// $Id: undoredochain.cpp,v 1.26 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <stack>
#include <iostream>
#include "errhandling.h"
#include "undoredochain.h"
#include "ui_vars.h"

UndoRedoChain::UndoRedoChain(DataPack& datapack,string fileName,UIInterface* ui)
    : defaultVars(datapack,fileName,ui)
{
}

UndoRedoChain::~UndoRedoChain()
{
    DeleteDoneItems();
    DeleteUndoneItems();
}

void UndoRedoChain::StartNewFrame()
{
    UIVars * nextVars = new UIVars(GetCurrentVars());
    done.push(nextVars);
}

void UndoRedoChain::RejectNewFrame()
{
    UIVars * clobberMe = done.top();
    done.pop();
    delete clobberMe;
}

void UndoRedoChain::AcceptNewFrame()
{
    DeleteUndoneItems();
}

void UndoRedoChain::Undo()
{
    if(CanUndo())
    {
        UIVars * vars = done.top();
        done.pop();
        undone.push(vars);
    }
    else
    {
        // well, we could throw an error here if
        // ever we try to undo when we can't,
        // but it seems reasonable to just do
        // nothing.
    }
}

void UndoRedoChain::Redo()
{
    if(CanRedo())
    {
        UIVars * vars = undone.top();
        undone.pop();
        done.push(vars);
    }
    else
    {
        // well, we could throw an error here if
        // ever we try to redo when we can't,
        // but it seems reasonable to just do
        // nothing.
    }
}

bool UndoRedoChain::CanUndo()
{
    return (!(done.empty()));
}

bool UndoRedoChain::CanRedo()
{
    return (!(undone.empty()));
}

std::string UndoRedoChain::GetUndoDescription()
{
    if(CanUndo())
    {
#if 0
        UndoRedo * action = done.top();
        return action->GetUndoDescription();
#endif
        return "UNDO LAST CHANGE";
    }
    else
    {
        throw UndoRedoCannotUndo();
    }
}

std::string UndoRedoChain::GetRedoDescription()
{
    if(CanRedo())
    {
#if 0
        UndoRedo * action = undone.top();
        return action->GetRedoDescription();
#endif
        return "REDO LAST CHANGE";
    }
    else
    {
        throw UndoRedoCannotRedo();
    }
}

void UndoRedoChain::DeleteDoneItems()
{
    while(!(done.empty()))
    {
        UIVars * clobberMe = done.top();
        done.pop();
        delete clobberMe;
    }
}

void UndoRedoChain::DeleteUndoneItems()
{
    while(!(undone.empty()))
    {
        UIVars * clobberMe = undone.top();
        undone.pop();
        delete clobberMe;
    }
}

UIVars & UndoRedoChain::GetCurrentVars()
{
    if(!(done.empty()))
        return *(done.top());
    else
        return defaultVars;
}

//____________________________________________________________________________________
