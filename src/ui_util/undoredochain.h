// $Id: undoredochain.h,v 1.20 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UNDOREDOCHAIN_H
#define UNDOREDOCHAIN_H

#include <stack>
#include <stdexcept>
#include <string>

#include "errhandling.h"
#include "ui_vars.h"

class DataPack;
class UIInterface;

/// stores stacks of done and undone changes
class UndoRedoChain
{
  private:
    UIVars defaultVars;           /// factory settings
    std::stack<UIVars*> done;     /// actions we've done
    std::stack<UIVars*> undone;   /// actions we've done & undone

    UndoRedoChain();

  protected:
    void DeleteUndoneItems();           /// remove items from undone stack
    /// and delete them
    void DeleteDoneItems();             /// remove items from done stack
    /// and delete them

  public:
    UndoRedoChain(DataPack& datapack,std::string fileName,UIInterface* ui);
    ~UndoRedoChain();
    void StartNewFrame();               /// start next undo-able action
    void AcceptNewFrame();              /// commit to action in last StartNewFrame()
    void RejectNewFrame();              /// back out of action in last StartNewFrame()
    void Undo();                        /// Undo last action done
    void Redo();                        /// Redo last action done
    bool CanUndo();
    bool CanRedo();
    std::string GetUndoDescription();
    std::string GetRedoDescription();
    UIVars & GetCurrentVars();
};

class UndoRedoCannotUndo : public std::exception
{
  private:
    std::string _what;
  public:
    UndoRedoCannotUndo(): _what ("Nothing to undo") {};
    virtual ~UndoRedoCannotUndo() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
};

class UndoRedoCannotRedo : public std::exception
{
  private:
    std::string _what;
  public:
    UndoRedoCannotRedo(): _what ("Nothing to redo") {};
    virtual ~UndoRedoCannotRedo() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
};

#endif  // UNDOREDOCHAIN_H

//____________________________________________________________________________________
