// $Id: gc_individual_err.cpp,v 1.7 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_individual.h"
#include "gc_individual_err.h"
#include "gc_strings_individual.h"

gc_individual_err::gc_individual_err(wxString msg) throw ()
    : gc_ex(msg)
{
}

gc_individual_err::~gc_individual_err() throw () {}

gc_phase_locus_repeat::gc_phase_locus_repeat(wxString indName, wxString locusName) throw()
    : gc_individual_err(wxString::Format(gcerr_ind::phaseLocusRepeat,indName.c_str(),locusName.c_str()))
{
}

gc_phase_locus_repeat::~gc_phase_locus_repeat() throw () {}

gc_sample_locus_repeat::gc_sample_locus_repeat(wxString sampleName, wxString locusName) throw()
    : gc_individual_err(wxString::Format(gcerr_ind::sampleLocusRepeat,sampleName.c_str(),locusName.c_str()))
{
}

gc_sample_locus_repeat::~gc_sample_locus_repeat() throw () {}

gc_sample_missing_locus_data::gc_sample_missing_locus_data(wxString sampleName, wxString locusName) throw()
    : gc_individual_err(wxString::Format(gcerr_ind::sampleMissingLocusData,sampleName.c_str(),locusName.c_str()))
{
}

gc_sample_missing_locus_data::~gc_sample_missing_locus_data() throw () {}

gc_ind_missing_phase_for_locus::gc_ind_missing_phase_for_locus(wxString indName, wxString locusName) throw()
    : gc_individual_err(wxString::Format(gcerr_ind::missingPhaseForLocus,indName.c_str(),locusName.c_str()))
{
}

gc_ind_missing_phase_for_locus::~gc_ind_missing_phase_for_locus() throw () {}

gc_ind_wrong_sample_count::gc_ind_wrong_sample_count(const GCIndividual& ind) throw()
    : gc_individual_err(wxString::Format(gcerr_ind::wrongSampleCount,ind.GetName().c_str()))
{
}

gc_ind_wrong_sample_count::~gc_ind_wrong_sample_count() throw () {}

//____________________________________________________________________________________
