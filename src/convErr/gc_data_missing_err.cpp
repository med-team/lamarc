// $Id: gc_data_missing_err.cpp,v 1.5 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_data_missing_err.h"
#include "gc_strings_data.h"

gc_data_missing_err::gc_data_missing_err(wxString msg) throw()
    : gc_ex(msg)
{
}

gc_data_missing_err::~gc_data_missing_err() throw() {}

gc_data_missing_pop_locus::gc_data_missing_pop_locus(wxString popName, wxString locusName) throw()
    : gc_data_missing_err(wxString::Format(gcerr_data::missingPopLocus,popName.c_str(),locusName.c_str()))
{
}

gc_data_missing_pop_locus::~gc_data_missing_pop_locus() throw() {}

gc_data_missing_pop_region::gc_data_missing_pop_region(wxString popName, wxString regionName) throw()
    : gc_data_missing_err(wxString::Format(gcerr_data::missingPopRegion,popName.c_str(),regionName.c_str()))
{
}

gc_data_missing_pop_region::~gc_data_missing_pop_region() throw() {}

//____________________________________________________________________________________
