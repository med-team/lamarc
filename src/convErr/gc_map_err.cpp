// $Id: gc_map_err.cpp,v 1.6 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_map_err.h"
#include "gc_strings_map.h"

gc_map_err::gc_map_err(wxString msg) throw()
    :   gc_ex(msg)
{
}

gc_map_err::~gc_map_err() throw() {};

gc_map_file_missing::gc_map_file_missing(wxString fileName) throw()
    : gc_map_err(wxString::Format(gcerr_map::fileMissing,fileName.c_str()))
{
}

gc_map_file_missing::~gc_map_file_missing() throw() {}

gc_map_file_read_err::gc_map_file_read_err(wxString fileName) throw()
    : gc_map_err(wxString::Format(gcerr_map::fileReadErr,fileName.c_str()))
{
}

gc_map_file_read_err::~gc_map_file_read_err() throw() {}

gc_map_file_empty::gc_map_file_empty(wxString fileName) throw()
    : gc_map_err(wxString::Format(gcerr_map::fileEmpty,fileName.c_str()))
{
}

gc_map_file_empty::~gc_map_file_empty() throw() {}

//____________________________________________________________________________________
