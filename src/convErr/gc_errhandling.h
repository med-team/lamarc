// $Id: gc_errhandling.h,v 1.12 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef GC_ERRHANDLING_H
#define GC_ERRHANDLING_H

#include <stdexcept>
#include "wx/string.h"

//------------------------------------------------------------------------------------

class gc_ex : public std::exception
{
  private:
    gc_ex();    // undefined
  protected:
    wxString    m_what;
    size_t      m_row;
    wxString    m_fileName;
  public:
    gc_ex(wxString) throw();
    virtual ~gc_ex() throw();

    bool    hasRow() const throw();
    size_t  getRow() const throw();
    void    setRow(size_t row) throw();
    void    setRow(int row) throw();

    bool        hasFile()   const throw();
    wxString    getFile()   const throw();
    void        setFile(const wxString & s) throw();

    virtual const char* what () const throw();
    virtual const wxString & wxWhat() const throw();

};

class gc_data_error : public gc_ex
{
  public:
    gc_data_error(const wxString & wh);
    virtual ~gc_data_error() throw();
};

class gc_implementation_error : public gc_ex
{
  public:
    gc_implementation_error(const wxString & wh);
    virtual ~gc_implementation_error() throw();
};

class gui_error : public gc_ex
{
  public:
    gui_error(const wxString & wh);
    virtual ~gui_error() throw();
};

class gc_abandon_export : public gc_ex
{
  public:
    gc_abandon_export() throw() ;
    virtual ~gc_abandon_export() throw();
};

class gc_fatal_error : public gc_ex
{
  public:
    gc_fatal_error();
    virtual ~gc_fatal_error() throw();
};

#endif  // GC_ERRHANDLING_H

//____________________________________________________________________________________
