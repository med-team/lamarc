// $Id: gc_locus_err.h,v 1.16 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOCUS_ERR_H
#define GC_LOCUS_ERR_H

#include "gc_errhandling.h"
#include "gc_types.h"

class gc_locus_err : public gc_data_error
{
  public:
    gc_locus_err(wxString msg) throw();
    virtual ~gc_locus_err() throw () ;
};

class gc_locus_site_count_mismatch : public gc_locus_err
{
  public:
    gc_locus_site_count_mismatch(wxString,wxString,size_t,size_t) throw ();
    virtual ~gc_locus_site_count_mismatch() throw () ;
};

class gc_locus_offset_mismatch : public gc_locus_err
{
  public:
    gc_locus_offset_mismatch(wxString,wxString,long,long) throw ();
    virtual ~gc_locus_offset_mismatch() throw () ;
};

class gc_locus_map_position_mismatch : public gc_locus_err
{
  public:
    gc_locus_map_position_mismatch(wxString,wxString,long,long) throw ();
    virtual ~gc_locus_map_position_mismatch() throw () ;
};

class gc_locus_user_linked_mismatch : public gc_locus_err
{
  public:
    gc_locus_user_linked_mismatch(wxString,wxString,bool,bool) throw ();
    virtual ~gc_locus_user_linked_mismatch() throw () ;
};

class gc_missing_locus : public gc_locus_err
{
    // setting number of locations that differs from number of sites
  public:
    gc_missing_locus(const wxString& locusName) throw();
    virtual ~gc_missing_locus() throw() ;
};

class gc_set_locations_err : public gc_locus_err
{
    // setting number of locations that differs from number of sites
  public:
    gc_set_locations_err(const wxString& name, const wxString& newlocs, size_t oldSize, size_t newSize) throw();
    virtual ~gc_set_locations_err() throw() ;
};

class gc_locus_length_mismatch : public gc_locus_err
{
  public:
    gc_locus_length_mismatch(wxString name1, wxString name2, size_t pLength, size_t length) throw();
    virtual ~gc_locus_length_mismatch() throw() ;
};

class gc_locus_user_data_type_mismatch : public gc_locus_err
{
  public:
    gc_locus_user_data_type_mismatch(wxString name1, wxString name2, gcSpecificDataType, gcSpecificDataType) throw();
    virtual ~gc_locus_user_data_type_mismatch() throw() ;
};

class gc_locus_types_mismatch : public gc_locus_err
{
  public:
    gc_locus_types_mismatch(wxString name1, wxString name2, wxString types1, wxString types2) throw();
    virtual ~gc_locus_types_mismatch() throw() ;
};

class gc_locus_overlap : public gc_locus_err
{
  public:
    gc_locus_overlap(wxString,long,long,wxString,long,long) throw ();
    virtual ~gc_locus_overlap() throw () ;
};

class gc_num_markers_zero : public gc_locus_err
{
  public:
    gc_num_markers_zero() throw();
    virtual ~gc_num_markers_zero() throw() ;
};

class gc_locus_without_length : public gc_locus_err
{
  public:
    gc_locus_without_length(const wxString& locusName) throw();
    virtual ~gc_locus_without_length() throw() ;
};

class gc_unlinked_nuc : public gc_locus_err
{
  public:
    gc_unlinked_nuc() throw();
    virtual ~gc_unlinked_nuc() throw() ;
};

class gc_locations_out_of_order : public gc_locus_err
{
  public:
    gc_locations_out_of_order(const wxString& locusName, long prevLoc, long newLoc) throw();
    virtual ~gc_locations_out_of_order() throw();
};

class gc_location_too_small : public gc_locus_err
{
  public:
    gc_location_too_small(long loc, long smallest, const wxString& locusName) throw();
    virtual ~gc_location_too_small() throw();
};

class gc_location_too_large : public gc_locus_err
{
  public:
    gc_location_too_large(long loc, long largest, const wxString& locusName) throw();
    virtual ~gc_location_too_large() throw();
};

class gc_wrong_location_count : public gc_locus_err
{
  public:
    gc_wrong_location_count(long numLoc, long numSite, const wxString& locusName) throw();
    virtual ~gc_wrong_location_count() throw();
};

#endif  // GC_LOCUS_ERR_H

//____________________________________________________________________________________
