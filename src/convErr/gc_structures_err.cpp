// $Id: gc_structures_err.cpp,v 1.14 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_structures_err.h"
#include "gc_strings.h" // EWFIX.P4 refactor out
#include "gc_strings_structures.h"

gc_structures_err::gc_structures_err(const wxString & wh) throw()
    :   gc_data_error(wh)
{
}

gc_structures_err::~gc_structures_err() throw() {};

//------------------------------------------------------------------------------------

duplicate_file_base_name_error::duplicate_file_base_name_error(const wxString & wh) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::duplicateFileBaseName,wh.c_str()))
{
}

duplicate_file_base_name_error::~duplicate_file_base_name_error() throw() {};

duplicate_file_error::duplicate_file_error(const wxString & wh) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::duplicateFileName,wh.c_str()))
{
}

duplicate_file_error::~duplicate_file_error() throw() {};

duplicate_name_error::duplicate_name_error(const wxString& name, const wxString & where) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::duplicateName,name.c_str(),where.c_str()))
{
}

duplicate_name_error::~duplicate_name_error() throw() {};

empty_name_error::empty_name_error(const wxString& where) throw()
    : gc_structures_err(wxString::Format(gcerr::emptyName,where.c_str()))
{
}

empty_name_error::~empty_name_error() throw() {};

incompatible_pops::incompatible_pops(const wxString& wh) throw()
    : gc_structures_err(wh)
{
}

incompatible_pops::~incompatible_pops() throw() {};

missing_file_error::missing_file_error(const wxString & wh) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingFile,wh.c_str()))
{
}

missing_file_error::~missing_file_error() throw() {};

missing_name_error::missing_name_error(const wxString& name, const wxString & where) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingName,name.c_str(),where.c_str()))
{
}

missing_migration::missing_migration(const wxString & migrationName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingMigration,migrationName.c_str()))
{
}

missing_migration::missing_migration(const wxString & toName, const wxString & fromName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::migrationNotDefined, fromName.c_str(), toName.c_str()))
{
}

missing_migration::~missing_migration() throw() {};

missing_migration_id::missing_migration_id(const wxString & idString) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingMigrationId,idString.c_str()))
{
}

missing_migration_id::~missing_migration_id() throw() {};

missing_name_error::~missing_name_error() throw() {};

unparsable_file_error::unparsable_file_error(const wxString & wh) throw()
    : gc_structures_err((wxString::Format(gcerr_structures::unparsableFile,wh.c_str())))
{
}

unparsable_file_error::~unparsable_file_error() throw() {};

gc_missing_population::gc_missing_population(const wxString & popName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingPopulation,popName.c_str()))
{
}

gc_missing_population::~gc_missing_population() throw() {};

missing_panel::missing_panel(const wxString & panelName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingPanel,panelName.c_str()))
{
}

missing_panel::missing_panel(const wxString & regionName, const wxString & popName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::panelNotDefined,regionName.c_str(), popName.c_str()))
{
}

missing_panel::~missing_panel() throw() {};

missing_panel_id::missing_panel_id(const wxString & idString) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingPanelId,idString.c_str()))
{
}

missing_panel_id::~missing_panel_id() throw() {};

panel_size_clash::panel_size_clash(const wxString & popName, const wxString & region1Name, const wxString & region2Name) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::panelSizeClash, popName.c_str(), region1Name.c_str(), region2Name.c_str()))
{
}

panel_size_clash::~panel_size_clash() throw() {};

panel_blessed_error::panel_blessed_error(const wxString & regionName, const wxString & popName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::panelBlessedError,regionName.c_str(), popName.c_str()))
{
}

panel_blessed_error::~panel_blessed_error() throw() {};

missing_region::missing_region(const wxString & regName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingRegion,regName.c_str()))
{
}

missing_parent::missing_parent(const wxString & parentName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingParent,parentName.c_str()))
{
}

missing_parent::~missing_parent() throw() {};

missing_parent_id::missing_parent_id(const wxString & idString) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingParentId,idString.c_str()))
{
}

missing_parent_id::~missing_parent_id() throw() {};

missing_region::~missing_region() throw() {};

missing_trait::missing_trait(const wxString & traitName) throw()
    : gc_structures_err(wxString::Format(gcerr_structures::missingTrait,traitName.c_str()))
{
}

missing_trait::~missing_trait() throw() {};

effective_pop_size_clash::effective_pop_size_clash(double size1, double size2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::regionEffPopSizeClash,size1,size2))
{
}

effective_pop_size_clash::~effective_pop_size_clash() throw() {};

gc_name_repeat_allele::gc_name_repeat_allele(wxString & name, int row1, int row2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::nameRepeatAllele,name.c_str(),row1))
{
    setRow(row2);
}

gc_name_repeat_allele::~gc_name_repeat_allele() throw() {};

gc_name_repeat_locus::gc_name_repeat_locus(wxString & name, int row1, int row2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::nameRepeatLocus,name.c_str(),row1))
{
    setRow(row2);
}

gc_name_repeat_locus::~gc_name_repeat_locus() throw() {};

gc_name_repeat_pop::gc_name_repeat_pop(wxString & name, int row1, int row2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::nameRepeatPop,name.c_str(),row1))
{
    setRow(row2);
}

gc_name_repeat_pop::~gc_name_repeat_pop() throw() {};

gc_name_repeat_region::gc_name_repeat_region(wxString & name, int row1, int row2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::nameRepeatRegion,name.c_str(),row1))
{
    setRow(row2);
}

gc_name_repeat_region::~gc_name_repeat_region() throw() {};

gc_name_repeat_trait::gc_name_repeat_trait(wxString & name, int row1, int row2) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::nameRepeatTrait,name.c_str(),row1))
{
    setRow(row2);
}

gc_name_repeat_trait::~gc_name_repeat_trait() throw() {};

gc_allele_trait_mismatch::gc_allele_trait_mismatch(wxString name, wxString newTraitName, wxString oldTraitName) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::mismatchAlleleTrait,name.c_str(),newTraitName.c_str(),oldTraitName.c_str()))
{
}

gc_allele_trait_mismatch::~gc_allele_trait_mismatch() throw() {};

gc_locus_region_mismatch::gc_locus_region_mismatch(wxString name, wxString newRegionName, wxString oldRegionName) throw()
    :   gc_structures_err(wxString::Format(gcerr_structures::mismatchLocusRegion,name.c_str(),newRegionName.c_str(),oldRegionName.c_str()))
{
}

gc_locus_region_mismatch::~gc_locus_region_mismatch() throw() {};


gc_wrong_divergence_error::gc_wrong_divergence_error() throw()
    :   gc_structures_err(gcerr::wrongDivergenceCount)
{
}

gc_wrong_divergence_error::~gc_wrong_divergence_error() throw() {};


gc_divergence_bad_id_error::gc_divergence_bad_id_error(wxString parent, wxString child) throw()
    :   gc_structures_err(wxString::Format(gcerr::badDivergenceId,parent.c_str(), child.c_str()))
{
}

gc_divergence_bad_id_error::~gc_divergence_bad_id_error() throw() {};

gc_rate_too_small_error::gc_rate_too_small_error() throw()
    :   gc_structures_err(gcerr::migrationRateTooSmall)
{
}

gc_rate_too_small_error::~gc_rate_too_small_error() throw() {};

gc_bad_name_error::gc_bad_name_error(wxString name) throw()
    :   gc_structures_err(wxString::Format(gcerr::badName, name.c_str()))
{
}

gc_bad_name_error::~gc_bad_name_error() throw() {};

//____________________________________________________________________________________
