// $Id: gc_data_missing_err.h,v 1.6 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_DATA_MISSING_ERR
#define GC_DATA_MISSING_ERR

#include "gc_errhandling.h"

class gc_data_missing_err : public gc_ex
{
  public:
    gc_data_missing_err(wxString msg) throw();
    virtual ~gc_data_missing_err() throw() ;
};

//------------------------------------------------------------------------------------

class gc_data_missing_pop_locus : public gc_data_missing_err
{
  public:
    gc_data_missing_pop_locus(wxString popName, wxString locusName) throw();
    virtual ~gc_data_missing_pop_locus() throw() ;
};

//------------------------------------------------------------------------------------

class gc_data_missing_pop_region : public gc_data_missing_err
{
  public:
    gc_data_missing_pop_region(wxString popName, wxString regionName) throw();
    virtual ~gc_data_missing_pop_region() throw() ;
};

#endif  // GC_DATA_MISSING_ERR

//____________________________________________________________________________________
