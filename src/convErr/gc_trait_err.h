// $Id: gc_trait_err.h,v 1.10 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_TRAIT_ERR
#define GC_TRAIT_ERR

#include "gc_errhandling.h"
#include "gc_phenotype.h"
#include "gc_trait.h"
#include "gc_trait_allele.h"

class gc_trait_err : public gc_ex
{
  public:
    gc_trait_err(wxString msg) throw();
    virtual ~gc_trait_err() throw();
};

//------------------------------------------------------------------------------------

class gc_haplotype_probability_negative : public gc_trait_err
{
  public:
    gc_haplotype_probability_negative(double probability) throw();
    virtual ~gc_haplotype_probability_negative() throw() ;
};

//------------------------------------------------------------------------------------

class gc_trait_allele_name_reuse : public gc_trait_err
{
  public:
    gc_trait_allele_name_reuse(wxString alleleName) throw();
    virtual ~gc_trait_allele_name_reuse() throw() ;
};

//------------------------------------------------------------------------------------

class gc_trait_phenotype_name_reuse : public gc_trait_err
{
  public:
    gc_trait_phenotype_name_reuse(wxString phenotypeName, wxString traitName) throw();
    virtual ~gc_trait_phenotype_name_reuse() throw() ;
};

//------------------------------------------------------------------------------------

class gc_missing_phenotype : public gc_trait_err
{
  public:
    gc_missing_phenotype(wxString phenotypeName) throw();
    virtual ~gc_missing_phenotype() throw() ;
};

//------------------------------------------------------------------------------------

class gc_missing_allele : public gc_trait_err
{
  public:
    gc_missing_allele(wxString alleleName) throw();
    virtual ~gc_missing_allele() throw() ;
};

//------------------------------------------------------------------------------------

class gc_trait_allele_name_spaces : public gc_trait_err
{
  public:
    gc_trait_allele_name_spaces(wxString alleleName) throw();
    virtual ~gc_trait_allele_name_spaces() throw() ;
};

//------------------------------------------------------------------------------------

class gc_pheno_trait_mismatch : public gc_trait_err
{
  public:
    gc_pheno_trait_mismatch(const gcTraitInfo& outer, const gcTraitInfo& inner, const gcPhenotype &, size_t lineNo) throw();
    virtual ~gc_pheno_trait_mismatch() throw() ;
};

#endif  // GC_TRAIT_ERR

//____________________________________________________________________________________
