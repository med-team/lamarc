// $Id: gc_cmdfile_err.h,v 1.9 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_CMDFILE_ERR
#define GC_CMDFILE_ERR

#include "gc_errhandling.h"

class gc_cmdfile_err : public gc_ex
{
  public:
    gc_cmdfile_err(const wxString & msg) throw();
    virtual ~gc_cmdfile_err() throw() ;
};

//------------------------------------------------------------------------------------

class gc_bad_yes_no : public gc_cmdfile_err
{
  public:
    gc_bad_yes_no(const wxString & token) throw();
    virtual ~gc_bad_yes_no() throw() ;
};

class gc_bad_proximity : public gc_cmdfile_err
{
  public:
    gc_bad_proximity(const wxString & token) throw();
    virtual ~gc_bad_proximity() throw() ;
};

class gc_bad_file_format : public gc_cmdfile_err
{
  public:
    gc_bad_file_format(const wxString & token) throw();
    virtual ~gc_bad_file_format() throw() ;
};

class gc_bad_interleaving : public gc_cmdfile_err
{
  public:
    gc_bad_interleaving(const wxString & token) throw();
    virtual ~gc_bad_interleaving() throw() ;
};

class gc_bad_general_data_type : public gc_cmdfile_err
{
  public:
    gc_bad_general_data_type(const wxString & token) throw();
    virtual ~gc_bad_general_data_type() throw() ;
};

class gc_bad_specific_data_type : public gc_cmdfile_err
{
  public:
    gc_bad_specific_data_type(const wxString & token) throw();
    virtual ~gc_bad_specific_data_type() throw() ;
};

//------------------------------------------------------------------------------------

class gc_locus_match_byname_not_empty : public gc_cmdfile_err
{
  public:
    gc_locus_match_byname_not_empty() throw ();
    virtual ~gc_locus_match_byname_not_empty() throw() ;
};

class gc_locus_match_single_empty : public gc_cmdfile_err
{
  public:
    gc_locus_match_single_empty() throw ();
    virtual ~gc_locus_match_single_empty() throw() ;
};

class gc_locus_match_unknown : public gc_cmdfile_err
{
  public:
    gc_locus_match_unknown(const wxString & locusMatchType) throw ();
    virtual ~gc_locus_match_unknown() throw() ;
};

class gc_pop_match_byname_not_empty : public gc_cmdfile_err
{
  public:
    gc_pop_match_byname_not_empty() throw ();
    virtual ~gc_pop_match_byname_not_empty() throw() ;
};

class gc_pop_match_single_empty : public gc_cmdfile_err
{
  public:
    gc_pop_match_single_empty() throw ();
    virtual ~gc_pop_match_single_empty() throw() ;
};

class gc_pop_match_unknown : public gc_cmdfile_err
{
  public:
    gc_pop_match_unknown(const wxString & popType) throw ();
    virtual ~gc_pop_match_unknown() throw() ;
};

#endif  // GC_CMDFILE_ERR

//____________________________________________________________________________________
