// $Id: gc_locus_err.cpp,v 1.17 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_data.h"
#include "gc_locus_err.h"
#include "gc_strings_locus.h"

gc_locus_err::gc_locus_err(wxString msg) throw()
    : gc_data_error(msg)
{
}

gc_locus_err::~gc_locus_err() throw() {}

gc_missing_locus::gc_missing_locus(const wxString & locusName) throw()
    : gc_locus_err(wxString::Format(gcerr_locus::missing,locusName.c_str()))
{
}

gc_missing_locus::~gc_missing_locus() throw() {}

gc_set_locations_err::gc_set_locations_err(const wxString & locusName,
                                           const wxString & newLocations,
                                           size_t oldCount,
                                           size_t newCount) throw()
    : gc_locus_err(wxString::Format(gcerr_locus::setLocs,locusName.c_str(),newLocations.c_str(),(long)oldCount,(long)newCount))
{
}

gc_set_locations_err::~gc_set_locations_err() throw() {}

gc_locus_length_mismatch::gc_locus_length_mismatch(wxString name1, wxString name2, size_t pLength, size_t length) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::lengthMismatch,name1.c_str(),name2.c_str(),(int)pLength,(int)length))
{
}

gc_locus_length_mismatch::~gc_locus_length_mismatch() throw() {}

gc_locus_map_position_mismatch::gc_locus_map_position_mismatch(wxString name1, wxString name2, long mapPos1, long mapPos2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::mapPositionMismatch,name1.c_str(),name2.c_str(),mapPos1,mapPos2))
{
}

gc_locus_map_position_mismatch::~gc_locus_map_position_mismatch() throw() {}

gc_locus_offset_mismatch::gc_locus_offset_mismatch(wxString name1, wxString name2, long offset1, long offset2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::offsetMismatch,name1.c_str(),name2.c_str(),offset1,offset2))
{
}

gc_locus_offset_mismatch::~gc_locus_offset_mismatch() throw() {}

gc_locus_site_count_mismatch::gc_locus_site_count_mismatch(wxString name1, wxString name2, size_t sites1, size_t sites2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::siteCountMismatch,(int)sites1,(int)sites2,name1.c_str(),name2.c_str()))
{
}

gc_locus_site_count_mismatch::~gc_locus_site_count_mismatch() throw() {}

gc_locus_types_mismatch::gc_locus_types_mismatch(wxString name1, wxString name2, wxString pType, wxString type) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::typeMismatch,name1.c_str(),name2.c_str(),pType.c_str(),type.c_str()))
{
}

gc_locus_types_mismatch::~gc_locus_types_mismatch() throw() {}

gc_locus_user_data_type_mismatch::gc_locus_user_data_type_mismatch(wxString name1, wxString name2, gcSpecificDataType t1, gcSpecificDataType t2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::userDataTypeMismatch,name1.c_str(),name2.c_str(),ToWxString(t1).c_str(),ToWxString(t2).c_str()))
{
}

gc_locus_user_data_type_mismatch::~gc_locus_user_data_type_mismatch() throw() {}

gc_locus_user_linked_mismatch::gc_locus_user_linked_mismatch(wxString name1, wxString name2, bool l1, bool l2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::typeMismatch,name1.c_str(),name2.c_str(),ToWxStringLinked(l1).c_str(),ToWxStringLinked(l2).c_str()))
{
}

gc_locus_user_linked_mismatch::~gc_locus_user_linked_mismatch() throw() {}

gc_locus_overlap::gc_locus_overlap(wxString name1, long start1, long end1, wxString name2, long start2, long end2) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::overlap,name1.c_str(),start1,end1,name2.c_str(),start2,end2))
{
}

gc_locus_overlap::~gc_locus_overlap() throw() {}

gc_num_markers_zero::gc_num_markers_zero() throw ()
    : gc_locus_err(gcerr_locus::numMarkersZero)
{
}

gc_num_markers_zero::~gc_num_markers_zero() throw() {}

gc_locus_without_length::gc_locus_without_length(const wxString & locusName) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::lengthMissing,locusName.c_str()))
{
}

gc_locus_without_length::~gc_locus_without_length() throw() {}

gc_unlinked_nuc::gc_unlinked_nuc() throw ()
    : gc_locus_err(gcerr_locus::unlinkedNuc)
{
}

gc_unlinked_nuc::~gc_unlinked_nuc() throw() {}

//------------------------------------------------------------------------------------

gc_locations_out_of_order::gc_locations_out_of_order(const wxString& locusName, long prevLoc, long newLoc) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::locationsOutOfOrder,locusName.c_str(),newLoc,prevLoc))
{
}

gc_locations_out_of_order::~gc_locations_out_of_order() throw () {};

//------------------------------------------------------------------------------------

gc_location_too_small::gc_location_too_small(long loc, long smallest, const wxString& locusName) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::locationTooSmall,loc,locusName.c_str(),smallest))
{
}

gc_location_too_small::~gc_location_too_small() throw () {};

//------------------------------------------------------------------------------------

gc_location_too_large::gc_location_too_large(long loc, long largest, const wxString& locusName) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::locationTooLarge,loc,locusName.c_str(),largest))
{
}

gc_location_too_large::~gc_location_too_large() throw () {};

//------------------------------------------------------------------------------------

gc_wrong_location_count::gc_wrong_location_count(long numLoc, long numSites, const wxString& locusName) throw ()
    : gc_locus_err(wxString::Format(gcerr_locus::wrongLocationCount,numLoc,locusName.c_str(),numSites))
{
}

gc_wrong_location_count::~gc_wrong_location_count() throw () {};

//____________________________________________________________________________________
