// $Id: gc_phase_err.h,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PHASE_ERR_H
#define GC_PHASE_ERR_H

#include "gc_errhandling.h"

class gcPhaseRecord;

class gc_phase_err : public gc_data_error
{
  public:
    gc_phase_err(wxString msg) throw();
    virtual ~gc_phase_err() throw();
};

class gc_no_individual_for_sample : public gc_phase_err
{
  private:
    gc_no_individual_for_sample();      // undefined
  public:
    gc_no_individual_for_sample(wxString sampleName) throw();
    virtual ~gc_no_individual_for_sample() throw();
};

class gc_no_sample_for_individual : public gc_phase_err
{
  private:
    gc_no_sample_for_individual();      // undefined
  public:
    gc_no_sample_for_individual(wxString individualName) throw();
    virtual ~gc_no_sample_for_individual() throw() ;
};

class gc_both_individual_and_sample : public gc_phase_err
{
  private:
    gc_both_individual_and_sample();      // undefined
  public:
    gc_both_individual_and_sample(const wxString &, const gcPhaseRecord&,const gcPhaseRecord&) throw();
    virtual ~gc_both_individual_and_sample() throw() ;
};

class gc_adjacent_phase_resolution_for_multisample_input : public gc_phase_err
{
  private:
    gc_adjacent_phase_resolution_for_multisample_input();  // undefined
  public:
    gc_adjacent_phase_resolution_for_multisample_input(wxString fname) throw();
    virtual ~gc_adjacent_phase_resolution_for_multisample_input() throw() ;
};

class gc_bad_ind_match_adjacency_value : public gc_phase_err
{
  private:
    gc_bad_ind_match_adjacency_value();  // undefined
  public:
    gc_bad_ind_match_adjacency_value(const wxString& numSamplesText) throw();
    virtual ~gc_bad_ind_match_adjacency_value() throw() ;
};

class gc_bad_ind_match : public gc_phase_err
{
  private:
    gc_bad_ind_match();  // undefined
  public:
    gc_bad_ind_match(const wxString&) throw();
    virtual ~gc_bad_ind_match() throw() ;
};

class gc_phase_mismatch : public gc_phase_err
{
  private:
    gc_phase_mismatch();  // undefined
  public:
    gc_phase_mismatch(const gcPhaseRecord&, const gcPhaseRecord&) throw();
    virtual ~gc_phase_mismatch() throw() ;
};

class gc_individual_sample_adj_mismatch : public gc_phase_err
{
  private:
    gc_individual_sample_adj_mismatch();  // undefined
  public:
    gc_individual_sample_adj_mismatch(size_t lineNum, wxString fileName, size_t numSamples, size_t numAdj) throw();
    virtual ~gc_individual_sample_adj_mismatch() throw() ;
};

class gc_phase_matching_confusion : public gc_phase_err
{
  private:
    gc_phase_matching_confusion();      // undefined
  public:
    gc_phase_matching_confusion(const wxString& regionName, const wxString& label1, const wxString& label2) throw();
    virtual ~gc_phase_matching_confusion() throw() ;
};

class gc_phase_marker_not_legal : public gc_phase_err
{
  public:
    gc_phase_marker_not_legal(wxString posString) throw();
    virtual ~gc_phase_marker_not_legal() throw() ;
};

class gc_phenotype_name_repeat_for_individual : public gc_phase_err
{
  public:
    gc_phenotype_name_repeat_for_individual(wxString pheno, wxString ind) throw();
    virtual ~gc_phenotype_name_repeat_for_individual() throw() ;
};

class gc_phase_too_small : public gc_phase_err
{
  public:
    gc_phase_too_small(long phase, wxString indName, long offset, wxString locusName) throw();
    virtual ~gc_phase_too_small() throw() ;
};

class gc_phase_too_large : public gc_phase_err
{
  public:
    gc_phase_too_large(long phase, wxString indName, long maxVal, wxString locusName) throw();
    virtual ~gc_phase_too_large() throw() ;
};

class gc_phase_not_location : public gc_phase_err
{
  public:
    gc_phase_not_location(long phase, wxString locusName) throw();
    virtual ~gc_phase_not_location() throw() ;
};

#endif  // GC_PHASE_ERR_H

//____________________________________________________________________________________
