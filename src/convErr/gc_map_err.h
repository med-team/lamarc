// $Id: gc_map_err.h,v 1.7 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_MAP_ERR
#define GC_MAP_ERR

#include "gc_errhandling.h"

class gc_map_err : public gc_ex
{
  public:
    gc_map_err(wxString msg) throw();
    virtual ~gc_map_err() throw();
};

class gc_map_file_missing : public gc_map_err
{
  public:
    gc_map_file_missing(wxString fileName) throw();
    virtual ~gc_map_file_missing() throw() ;
};

class gc_map_file_read_err : public gc_map_err
{
  public:
    gc_map_file_read_err(wxString fileName) throw();
    virtual ~gc_map_file_read_err() throw() ;
};

class gc_map_file_empty : public gc_map_err
{
  public:
    gc_map_file_empty(wxString fileName) throw();
    virtual ~gc_map_file_empty() throw() ;
};

#endif  // GC_MAP_ERR

//____________________________________________________________________________________
