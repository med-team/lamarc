// $Id: gc_infile_err.h,v 1.14 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_INFILE_ERR
#define GC_INFILE_ERR

#include "gc_errhandling.h"
#include "gc_types.h"

class gc_infile_err : public gc_ex
{
  public:
    gc_infile_err(wxString msg) throw();
    virtual ~gc_infile_err() throw() ;
};

class gc_extra_file_data : public gc_infile_err
{
  public:
    gc_extra_file_data() throw();
    virtual ~gc_extra_file_data() throw() ;
};

class gc_illegal_dna_character : public gc_infile_err
{
  public:
    gc_illegal_dna_character(char character, size_t position, const wxString & wh) throw();
    virtual ~gc_illegal_dna_character() throw() ;
};

class gc_illegal_msat : public gc_infile_err
{
  public:
    gc_illegal_msat(const wxString& msat) throw();
    virtual ~gc_illegal_msat() throw() ;
};

class gc_premature_end_of_file : public gc_infile_err
{
  public:
    gc_premature_end_of_file(const wxString & fileName) throw();
    virtual ~gc_premature_end_of_file() throw() ;
};

class gc_token_count_mismatch : public gc_infile_err
{
  public:
    gc_token_count_mismatch(const wxString & delimiter,
                            const wxString & thisToken,
                            const wxString & sampleName,
                            size_t receivedCount,
                            size_t expectedCount) throw ();
    virtual ~gc_token_count_mismatch() throw() ;
};

class gc_too_few_markers : public gc_infile_err
{
  public:
    gc_too_few_markers(wxString oldMsg) throw ();
    virtual ~gc_too_few_markers() throw() ;
};

class gc_too_many_markers : public gc_infile_err
{
  public:
    gc_too_many_markers(size_t haveCount, size_t wantCount) throw ();
    virtual ~gc_too_many_markers() throw() ;
};

class gc_migrate_bad_pop_count : public gc_infile_err
{
  public:
    gc_migrate_bad_pop_count(const wxString &) throw();
    virtual ~gc_migrate_bad_pop_count() throw() ;
};

class gc_migrate_bad_locus_count : public gc_infile_err
{
  public:
    gc_migrate_bad_locus_count(const wxString &) throw();
    virtual ~gc_migrate_bad_locus_count() throw() ;
};

class gc_migrate_locus_length_not_positive : public gc_infile_err
{
  public:
    gc_migrate_locus_length_not_positive(const wxString& token) throw();
    virtual ~gc_migrate_locus_length_not_positive() throw() ;
};

class gc_migrate_bad_sequence_count : public gc_infile_err
{
  public:
    gc_migrate_bad_sequence_count(const wxString& token) throw();
    virtual ~gc_migrate_bad_sequence_count() throw() ;
};

class gc_migrate_missing_sequence_count : public gc_infile_err
{
  public:
    gc_migrate_missing_sequence_count(const wxString& token) throw();
    virtual ~gc_migrate_missing_sequence_count() throw() ;
};

class gc_migrate_too_few_sequence_lengths : public gc_infile_err
{
  public:
    gc_migrate_too_few_sequence_lengths(size_t count, const wxString& line) throw();
    virtual ~gc_migrate_too_few_sequence_lengths() throw() ;
};

class gc_migrate_bad_delimiter : public gc_infile_err
{
  public:
    gc_migrate_bad_delimiter(const wxString& token) throw();
    virtual ~gc_migrate_bad_delimiter() throw() ;
};

class gc_migrate_missing_msat_delimiter : public gc_infile_err
{
  public:
    gc_migrate_missing_msat_delimiter(const wxString& fileName) throw();
    virtual ~gc_migrate_missing_msat_delimiter() throw();
};

class gc_migrate_parse_err : public gc_infile_err
{
  public:
    gc_migrate_parse_err(size_t lineNum, wxString fileName, const char * msg) throw();
    virtual ~gc_migrate_parse_err() throw();
};

class gc_phylip_first_token : public gc_infile_err
{
  public:
    gc_phylip_first_token(const wxString& token) throw();
    virtual ~gc_phylip_first_token() throw() ;
};

class gc_phylip_second_token : public gc_infile_err
{
  public:
    gc_phylip_second_token(const wxString& token) throw();
    virtual ~gc_phylip_second_token() throw() ;
};

class gc_parse_data_type_spec_mismatch : public gc_infile_err
{
  public:
    gc_parse_data_type_spec_mismatch(gcSpecificDataType,gcGeneralDataType) throw ();
    virtual ~gc_parse_data_type_spec_mismatch() throw () ;
};

class gc_parse_missing_err : public gc_infile_err
{
  public:
    gc_parse_missing_err(wxString fileName) throw ();
    virtual ~gc_parse_missing_err() throw () ;
};

#endif  // GC_INFILE_ERR

//____________________________________________________________________________________
