// $Id: gc_phase_err.cpp,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_phase_err.h"
#include "gc_phase_info.h"
#include "gc_strings_phase.h"

//------------------------------------------------------------------------------------

gc_phase_err::gc_phase_err(wxString msg) throw ()
    :   gc_data_error(msg)
{
}

gc_phase_err::~gc_phase_err() throw () {};

//------------------------------------------------------------------------------------

gc_no_individual_for_sample::gc_no_individual_for_sample(wxString sampleName) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::noIndividualForSample,sampleName.c_str()))
{
}

gc_no_individual_for_sample::~gc_no_individual_for_sample() throw () {};

//------------------------------------------------------------------------------------

gc_no_sample_for_individual::gc_no_sample_for_individual(wxString individualName) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::noSampleForIndividual,individualName.c_str()))
{
}

gc_no_sample_for_individual::~gc_no_sample_for_individual() throw () {};

//------------------------------------------------------------------------------------

gc_both_individual_and_sample::gc_both_individual_and_sample(const wxString & name, const gcPhaseRecord& rec1,const gcPhaseRecord& rec2) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::bothIndividualAndSample,name.c_str()))
      // EWFIX.P4.BUG.564 -- use all arguments
{
}

gc_both_individual_and_sample::~gc_both_individual_and_sample() throw () {};

//------------------------------------------------------------------------------------

gc_adjacent_phase_resolution_for_multisample_input::gc_adjacent_phase_resolution_for_multisample_input(wxString fname) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::adjacentPhaseForMultiSample,fname.c_str()))
{
}

gc_adjacent_phase_resolution_for_multisample_input::~gc_adjacent_phase_resolution_for_multisample_input() throw () {};

//------------------------------------------------------------------------------------

gc_bad_ind_match_adjacency_value::gc_bad_ind_match_adjacency_value(const wxString& numSamplesText) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::badIndMatchAdjacencyValue,numSamplesText.c_str()))
{
}

gc_bad_ind_match_adjacency_value::~gc_bad_ind_match_adjacency_value() throw () {};

//------------------------------------------------------------------------------------

gc_bad_ind_match::gc_bad_ind_match(const wxString& matchString) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::badIndMatchType,matchString.c_str()))
{
}

gc_bad_ind_match::~gc_bad_ind_match() throw () {};

//------------------------------------------------------------------------------------

gc_phase_mismatch::gc_phase_mismatch(const gcPhaseRecord& rec1,const gcPhaseRecord& rec2) throw()
    : gc_phase_err("")
{
    // EWFIX.BUG.722 -- needs lots better information here, including sample names
    m_what=wxString::Format(gcerr_phase::mergeMismatch,rec1.GetDescriptiveName().c_str(),rec2.GetDescriptiveName().c_str());
}

gc_phase_mismatch::~gc_phase_mismatch() throw () {};

//------------------------------------------------------------------------------------

gc_individual_sample_adj_mismatch::gc_individual_sample_adj_mismatch(size_t lineNum, wxString fileName, size_t numSamples, size_t numAdj) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::unevenAdjDivisor,(int)numSamples,(int)numAdj))
      // EWFIX.P4.BUG.564 -- use all arguments
{
}

gc_individual_sample_adj_mismatch::~gc_individual_sample_adj_mismatch() throw () {};

//------------------------------------------------------------------------------------

gc_phase_matching_confusion::gc_phase_matching_confusion(const wxString& regionName, const wxString& label1, const wxString& label2) throw()
    : gc_phase_err(wxString::Format(gcerr_phase::matchingConfusion,regionName.c_str(),label1.c_str(),label2.c_str()))
{
}

gc_phase_matching_confusion::~gc_phase_matching_confusion() throw () {};

//------------------------------------------------------------------------------------

gc_phase_marker_not_legal::gc_phase_marker_not_legal(wxString posString) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::markerNotLegal,posString.c_str()))
{
}

gc_phase_marker_not_legal::~gc_phase_marker_not_legal() throw () {};

//------------------------------------------------------------------------------------

gc_phenotype_name_repeat_for_individual::gc_phenotype_name_repeat_for_individual(wxString pheno, wxString ind) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::individualPhenotypeNameRepeat,pheno.c_str(),ind.c_str()))
{
}

gc_phenotype_name_repeat_for_individual::~gc_phenotype_name_repeat_for_individual() throw () {};

//------------------------------------------------------------------------------------

gc_phase_too_small::gc_phase_too_small(long phase, wxString indName,long smallest, wxString locName) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::tooSmall,phase,indName.c_str(),locName.c_str(),smallest))
{
}

gc_phase_too_small::~gc_phase_too_small() throw () {};

//------------------------------------------------------------------------------------

gc_phase_too_large::gc_phase_too_large(long phase, wxString indName,long largest, wxString locName) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::tooLarge,phase,indName.c_str(),locName.c_str(),largest))
{
}

gc_phase_too_large::~gc_phase_too_large() throw () {};

//------------------------------------------------------------------------------------

gc_phase_not_location::gc_phase_not_location(long phase, wxString locName) throw ()
    : gc_phase_err(wxString::Format(gcerr_phase::notLocation,phase,locName.c_str()))
{
}

gc_phase_not_location::~gc_phase_not_location() throw () {};

//____________________________________________________________________________________
