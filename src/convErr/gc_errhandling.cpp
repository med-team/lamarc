// $Id: gc_errhandling.cpp,v 1.14 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_errhandling.h"
#include "gc_strings.h"

//------------------------------------------------------------------------------------

gc_ex::gc_ex(wxString string) throw()
    :   m_what(string),
        m_row(0),
        m_fileName(wxEmptyString)
{
}

gc_ex::~gc_ex() throw()
{
}

bool
gc_ex::hasRow() const throw()
{
    return m_row > 0;
}

size_t
gc_ex::getRow() const throw()
{
    return m_row;
}

void
gc_ex::setRow(size_t row) throw ()
{
    m_row = row;
}

void
gc_ex::setRow(int row) throw ()
{
    assert(row >= 0);
    m_row = (size_t)row;
}

bool
gc_ex::hasFile() const throw()
{
    return !m_fileName.IsEmpty();
}

wxString
gc_ex::getFile() const throw()
{
    return m_fileName;
}

void
gc_ex::setFile(const wxString& fileName) throw ()
{
    m_fileName = fileName;
}

const char *
gc_ex::what() const throw()
{
    return m_what.c_str();
}

const wxString &
gc_ex::wxWhat() const throw()
{
    return m_what;
}

//------------------------------------------------------------------------------------

gc_data_error::gc_data_error(const wxString & wh)
    :
    gc_ex(wh)
{
}

gc_data_error::~gc_data_error() throw()
{
}

//------------------------------------------------------------------------------------

gc_implementation_error::gc_implementation_error(const wxString & wh)
    :
    gc_ex(wh)
{
}

gc_implementation_error::~gc_implementation_error() throw()
{
}

//------------------------------------------------------------------------------------

gui_error::gui_error(const wxString & wh)
    :
    gc_ex(wh)
{
}

gui_error::~gui_error() throw()
{
}

//------------------------------------------------------------------------------------

gc_abandon_export::gc_abandon_export() throw()
    :
    gc_ex(gcerr::abandonExport)
{
}

gc_abandon_export::~gc_abandon_export() throw()
{
}

//------------------------------------------------------------------------------------

gc_fatal_error::gc_fatal_error()
    :
    gc_ex(wxEmptyString)
{
}

gc_fatal_error::~gc_fatal_error() throw()
{
}

//____________________________________________________________________________________
