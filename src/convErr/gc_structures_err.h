// $Id: gc_structures_err.h,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef GC_STRUCTURES_ERR_H
#define GC_STRUCTURES_ERR_H

#include "gc_errhandling.h"

class gc_structures_err : public gc_data_error
{
  public:
    gc_structures_err(const wxString & wh) throw();
    virtual ~gc_structures_err() throw() ;
};

class duplicate_file_base_name_error : public gc_structures_err
{
  public:
    duplicate_file_base_name_error(const wxString & wh) throw();
    virtual ~duplicate_file_base_name_error() throw() ;
};

class duplicate_file_error : public gc_structures_err
{
  public:
    duplicate_file_error(const wxString & wh) throw();
    virtual ~duplicate_file_error() throw() ;
};

class unparsable_file_error : public gc_structures_err
{
  public:
    unparsable_file_error(const wxString & wh) throw();
    virtual ~unparsable_file_error() throw() ;
};

class gc_missing_population : public gc_structures_err
{
  public:
    gc_missing_population(const wxString & popName) throw() ;
    virtual ~gc_missing_population() throw() ;
};

class missing_panel : public gc_structures_err
{
  public:
    missing_panel(const wxString & panelName) throw() ;
    missing_panel(const wxString & regionName, const wxString & popName) throw() ;
    virtual ~missing_panel() throw() ;
};

class missing_panel_id : public gc_structures_err
{
  public:
    missing_panel_id(const wxString & idString) throw() ;
    virtual ~missing_panel_id() throw() ;
};

class missing_parent : public gc_structures_err
{
  public:
    missing_parent(const wxString & parentName) throw() ;
    virtual ~missing_parent() throw() ;
};

class missing_parent_id : public gc_structures_err
{
  public:
    missing_parent_id(const wxString & idString) throw() ;
    virtual ~missing_parent_id() throw() ;
};

class missing_migration : public gc_structures_err
{
  public:
    missing_migration(const wxString & migrationName) throw() ;
    missing_migration(const wxString & fromName, const wxString & toName) throw() ;
    virtual ~missing_migration() throw() ;
};

class missing_migration_id : public gc_structures_err
{
  public:
    missing_migration_id(const wxString & idString) throw() ;
    virtual ~missing_migration_id() throw() ;
};

class panel_size_clash : public gc_structures_err
{
  public:
    panel_size_clash(const wxString & popName, const wxString & region1Name, const wxString & region2Name) throw() ;
    virtual ~panel_size_clash() throw() ;
};

class panel_blessed_error : public gc_structures_err
{
  public:
    panel_blessed_error(const wxString & regionName, const wxString & popName) throw() ;
    virtual ~panel_blessed_error() throw() ;
};

class missing_region : public gc_structures_err
{
  public:
    missing_region(const wxString& wh) throw() ;
    virtual ~missing_region() throw() ;
};

class missing_trait : public gc_structures_err
{
  public:
    missing_trait(const wxString& wh) throw();
    virtual ~missing_trait() throw() ;
};

class duplicate_name_error : public gc_structures_err
{
  public:
    duplicate_name_error(const wxString & name, const wxString & why) throw();
    virtual ~duplicate_name_error() throw() ;
};

class empty_name_error : public gc_structures_err
{
  public:
    empty_name_error(const wxString& why) throw();
    virtual ~empty_name_error() throw() ;
};

class incompatible_pops : public gc_structures_err
{
  public:
    incompatible_pops(const wxString & wh) throw();
    virtual ~incompatible_pops() throw() ;
};

class missing_file_error : public gc_structures_err
{
  public:
    missing_file_error(const wxString & filename) throw();
    virtual ~missing_file_error() throw() ;
};

class missing_name_error : public gc_structures_err
{
  public:
    missing_name_error(const wxString& name, const wxString & why) throw();
    virtual ~missing_name_error() throw() ;
};

class effective_pop_size_clash : public gc_structures_err
{
  public:
    effective_pop_size_clash(double size1, double size2) throw();
    virtual ~effective_pop_size_clash() throw() ;
};

class gc_name_repeat_allele : public gc_structures_err
{
  public:
    gc_name_repeat_allele(wxString & name, int oldRow, int newRow) throw();
    virtual ~gc_name_repeat_allele() throw ();
};

class gc_name_repeat_locus : public gc_structures_err
{
  public:
    gc_name_repeat_locus(wxString & name, int oldRow, int newRow) throw();
    virtual ~gc_name_repeat_locus() throw ();
};

class gc_name_repeat_pop : public gc_structures_err
{
  public:
    gc_name_repeat_pop(wxString & name, int oldRow, int newRow) throw();
    virtual ~gc_name_repeat_pop() throw ();
};

class gc_name_repeat_region : public gc_structures_err
{
  public:
    gc_name_repeat_region(wxString & name, int oldRow, int newRow) throw();
    virtual ~gc_name_repeat_region() throw ();
};

class gc_name_repeat_trait : public gc_structures_err
{
  public:
    gc_name_repeat_trait(wxString & name, int oldRow, int newRow) throw();
    virtual ~gc_name_repeat_trait() throw ();
};

class gc_allele_trait_mismatch : public gc_structures_err
{
  public:
    gc_allele_trait_mismatch(wxString alleleName, wxString newTraitName, wxString oldTraitName) throw();
    virtual ~gc_allele_trait_mismatch() throw ();
};

class gc_locus_region_mismatch : public gc_structures_err
{
  public:
    gc_locus_region_mismatch(wxString locusName, wxString newRegionName, wxString oldRegionName) throw();
    virtual ~gc_locus_region_mismatch() throw ();
};

class gc_wrong_divergence_error : public gc_structures_err
{
  public:
    gc_wrong_divergence_error() throw();
    virtual ~gc_wrong_divergence_error() throw();
};

class gc_divergence_bad_id_error : public gc_structures_err
{
  public:
    gc_divergence_bad_id_error(wxString parent, wxString child) throw();
    virtual ~gc_divergence_bad_id_error() throw();
};

class gc_rate_too_small_error : public gc_structures_err
{
  public:
    gc_rate_too_small_error() throw();
    virtual ~gc_rate_too_small_error() throw();
};

class gc_bad_name_error : public gc_structures_err
{
  public:
    gc_bad_name_error(wxString name) throw();
    virtual ~gc_bad_name_error() throw();
};

#endif  // GC_STRUCTURES_ERR_H

//____________________________________________________________________________________
