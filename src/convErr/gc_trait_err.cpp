// $Id: gc_trait_err.cpp,v 1.10 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_trait_err.h"
#include "gc_strings_trait.h"

gc_trait_err::gc_trait_err(wxString msg) throw()
    : gc_ex(msg)
{
}
gc_trait_err::~gc_trait_err() throw() {}

gc_haplotype_probability_negative::gc_haplotype_probability_negative(double prob) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::hapProbabilityNegative,prob))
{
}
gc_haplotype_probability_negative::~gc_haplotype_probability_negative() throw() {}

gc_trait_allele_name_reuse::gc_trait_allele_name_reuse(wxString alleleName) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::alleleNameReuse,alleleName.c_str()))
{
}
gc_trait_allele_name_reuse::~gc_trait_allele_name_reuse() throw() {}

gc_trait_phenotype_name_reuse::gc_trait_phenotype_name_reuse(wxString phenotypeName, wxString traitName) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::phenotypeNameReuse,phenotypeName.c_str(),traitName.c_str()))
{
}
gc_trait_phenotype_name_reuse::~gc_trait_phenotype_name_reuse() throw() {}

gc_missing_phenotype::gc_missing_phenotype(wxString phenotypeName) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::phenotypeMissing,phenotypeName.c_str()))
{
}
gc_missing_phenotype::~gc_missing_phenotype() throw() {}

gc_missing_allele::gc_missing_allele(wxString alleleName) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::alleleMissing,alleleName.c_str()))
{
}
gc_missing_allele::~gc_missing_allele() throw() {}

#if 0

gc_allele_trait_mismatch::gc_allele_trait_mismatch(const gcTraitAllele & allele,
                                                   const gcTraitInfo & trait,
                                                   const gcPhenotype & pheno,
                                                   size_t lineNo) throw ()
    : gc_trait_err(wxString::Format(gcerr_trait::alleleTraitMismatch,
                                    allele.GetName().c_str(),
                                    pheno.GetName().c_str(),
                                    trait.GetName().c_str()))
{
    setRow(lineNo);
}

gc_allele_trait_mismatch::~gc_allele_trait_mismatch() throw() {}

#endif

gc_pheno_trait_mismatch::gc_pheno_trait_mismatch(const gcTraitInfo & outer,
                                                 const gcTraitInfo & inner,
                                                 const gcPhenotype & pheno,
                                                 size_t lineNo) throw ()
    : gc_trait_err(wxString::Format(gcerr_trait::phenoTraitMismatch,
                                    pheno.GetName().c_str(),
                                    inner.GetName().c_str(),
                                    outer.GetName().c_str()))
{
    setRow(lineNo);
}

gc_pheno_trait_mismatch::~gc_pheno_trait_mismatch() throw() {}

gc_trait_allele_name_spaces::gc_trait_allele_name_spaces(wxString alleleName) throw()
    : gc_trait_err(wxString::Format(gcerr_trait::alleleNameSpaces,
                                    alleleName.c_str()))
{
}

gc_trait_allele_name_spaces::~gc_trait_allele_name_spaces() throw() {}

//____________________________________________________________________________________
