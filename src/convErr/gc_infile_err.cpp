// $Id: gc_infile_err.cpp,v 1.13 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_data.h"
#include "gc_infile_err.h"
#include "gc_strings_infile.h"

gc_infile_err::gc_infile_err(wxString msg) throw ()
    : gc_ex(msg)
{
}

gc_infile_err::~gc_infile_err() throw ()
{
}

//------------------------------------------------------------------------------------

gc_extra_file_data::gc_extra_file_data() throw ()
    : gc_infile_err(gcerr_infile::extraFileData)
{
}

gc_extra_file_data::~gc_extra_file_data() throw ()
{
}

//------------------------------------------------------------------------------------

gc_illegal_dna_character::gc_illegal_dna_character(char character, size_t position, const wxString & data) throw()
    : gc_infile_err(wxString::Format(gcerr_infile::illegalDna,character,position,data.c_str()))
{
}

gc_illegal_dna_character::~gc_illegal_dna_character() throw ()
{
}

//------------------------------------------------------------------------------------

gc_illegal_msat::gc_illegal_msat(const wxString & data) throw()
    : gc_infile_err(wxString::Format(gcerr_infile::illegalMsat,data.c_str()))
{
}

gc_illegal_msat::~gc_illegal_msat() throw ()
{
}

//------------------------------------------------------------------------------------

gc_premature_end_of_file::gc_premature_end_of_file(const wxString & fileName) throw()
    : gc_infile_err(wxString::Format(gcerr_infile::prematureEndOfFile,fileName.c_str()))
{
}

gc_premature_end_of_file::~gc_premature_end_of_file() throw ()
{
}

//------------------------------------------------------------------------------------

gc_too_few_markers::gc_too_few_markers(wxString oldMsg) throw()
    : gc_infile_err(wxString::Format(gcerr_infile::tooFewMarkersInSample,oldMsg.c_str()))
{
}

gc_too_few_markers::~gc_too_few_markers() throw ()
{
}

//------------------------------------------------------------------------------------

gc_too_many_markers::gc_too_many_markers(size_t have, size_t want) throw()
    : gc_infile_err(wxString::Format(gcerr_infile::tooManyMarkersInSample,(int)have,(int)want))
{
}

gc_too_many_markers::~gc_too_many_markers() throw ()
{
}

//------------------------------------------------------------------------------------

gc_token_count_mismatch::gc_token_count_mismatch(const wxString & delimiter,
                                                 const wxString & thisToken,
                                                 const wxString & sampleName,
                                                 size_t receivedCount,
                                                 size_t expectedCount) throw ()
    : gc_infile_err(wxString::Format(gcerr_infile::tokenCountMismatch,
                                     delimiter.c_str(),
                                     thisToken.c_str(),
                                     sampleName.c_str(),
                                     (int)receivedCount,
                                     (int)expectedCount))
{
}

gc_token_count_mismatch::~gc_token_count_mismatch() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_bad_pop_count::gc_migrate_bad_pop_count(const wxString& token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::badPopCount,token.c_str()))
{
}

gc_migrate_bad_pop_count::~gc_migrate_bad_pop_count() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_bad_locus_count::gc_migrate_bad_locus_count(const wxString& token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::badLocusCount,token.c_str()))
{
}

gc_migrate_bad_locus_count::~gc_migrate_bad_locus_count() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_locus_length_not_positive::gc_migrate_locus_length_not_positive(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::locusLengthNotPositive,token.c_str()))
{
}

gc_migrate_locus_length_not_positive::~gc_migrate_locus_length_not_positive() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_bad_sequence_count::gc_migrate_bad_sequence_count(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::badSequenceCount,token.c_str()))
{
}

gc_migrate_bad_sequence_count::~gc_migrate_bad_sequence_count() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_missing_sequence_count::gc_migrate_missing_sequence_count(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::missingSequenceCount,token.c_str()))
{
}

gc_migrate_missing_sequence_count::~gc_migrate_missing_sequence_count() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_too_few_sequence_lengths::gc_migrate_too_few_sequence_lengths(size_t count, const wxString & line) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::tooFewSequenceLengths,(int)count,line.c_str()))
{
}

gc_migrate_too_few_sequence_lengths::~gc_migrate_too_few_sequence_lengths() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_bad_delimiter::gc_migrate_bad_delimiter(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::badDelimiter,token.c_str()))
{
}

gc_migrate_bad_delimiter::~gc_migrate_bad_delimiter() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_missing_msat_delimiter::gc_migrate_missing_msat_delimiter(const wxString & fileName) throw()
    : gc_infile_err(wxString::Format(gcerr_migrate::missingMsatDelimiter,fileName.c_str()))
{
}

gc_migrate_missing_msat_delimiter::~gc_migrate_missing_msat_delimiter() throw ()
{
}

//------------------------------------------------------------------------------------

gc_migrate_parse_err::gc_migrate_parse_err(size_t lineNum, wxString fileName, const char * msg) throw ()
    : gc_infile_err(wxString::Format(gcerr_migrate::parseErr,(int)lineNum,fileName.c_str(),msg))
{
}

gc_migrate_parse_err::~gc_migrate_parse_err() throw ()
{
}

//------------------------------------------------------------------------------------

gc_phylip_first_token::gc_phylip_first_token(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_phylip::badFirstToken,token.c_str()))
{
}

gc_phylip_first_token::~gc_phylip_first_token() throw ()
{
}

//------------------------------------------------------------------------------------

gc_phylip_second_token::gc_phylip_second_token(const wxString & token) throw()
    : gc_infile_err(wxString::Format(gcerr_phylip::badSecondToken,token.c_str()))
{
}

gc_phylip_second_token::~gc_phylip_second_token() throw ()
{
}

//------------------------------------------------------------------------------------

gc_parse_data_type_spec_mismatch::gc_parse_data_type_spec_mismatch(gcSpecificDataType stype,gcGeneralDataType dtype) throw ()
    : gc_infile_err(wxString::Format(gcerr_infile::parseDataTypeSpecMismatch,ToWxString(stype).c_str(),ToWxString(dtype).c_str()))
{
}

gc_parse_data_type_spec_mismatch::~gc_parse_data_type_spec_mismatch() throw ()
{
}

//------------------------------------------------------------------------------------

gc_parse_missing_err::gc_parse_missing_err(wxString fileName) throw ()
    : gc_infile_err(wxString::Format(gcerr_infile::parseMissingErr,fileName.c_str()))
{
}

gc_parse_missing_err::~gc_parse_missing_err() throw ()
{
}

//____________________________________________________________________________________
