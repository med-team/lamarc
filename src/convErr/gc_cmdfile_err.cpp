// $Id: gc_cmdfile_err.cpp,v 1.8 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_cmdfile_err.h"
#include "gc_strings_cmdfile.h"

//------------------------------------------------------------------------------------

gc_cmdfile_err::gc_cmdfile_err(const wxString & msg) throw ()
    : gc_ex(msg)
{
}

gc_cmdfile_err::~gc_cmdfile_err() throw() {};

//------------------------------------------------------------------------------------

gc_bad_yes_no::gc_bad_yes_no(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badYesNo,string.c_str()))
{
}

gc_bad_yes_no::~gc_bad_yes_no() throw() {};

//------------------------------------------------------------------------------------

gc_bad_proximity::gc_bad_proximity(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badProximity,string.c_str()))
{
}

gc_bad_proximity::~gc_bad_proximity() throw() {};

//------------------------------------------------------------------------------------

gc_bad_file_format::gc_bad_file_format(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badFileFormat,string.c_str()))
{
}

gc_bad_file_format::~gc_bad_file_format() throw() {};

//------------------------------------------------------------------------------------

gc_bad_interleaving::gc_bad_interleaving(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badInterleaving,string.c_str()))
{
}

gc_bad_interleaving::~gc_bad_interleaving() throw() {};

//------------------------------------------------------------------------------------

gc_bad_general_data_type::gc_bad_general_data_type(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badGeneralDataType,string.c_str()))
{
}

gc_bad_general_data_type::~gc_bad_general_data_type() throw() {};

//------------------------------------------------------------------------------------

gc_bad_specific_data_type::gc_bad_specific_data_type(const wxString & string) throw()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::badSpecificDataType,string.c_str()))
{
}

gc_bad_specific_data_type::~gc_bad_specific_data_type() throw() {};

//------------------------------------------------------------------------------------

gc_locus_match_byname_not_empty::gc_locus_match_byname_not_empty() throw ()
    : gc_cmdfile_err(gcerr_cmdfile::locusMatchByNameNotEmpty)
{
}

gc_locus_match_byname_not_empty::~gc_locus_match_byname_not_empty() throw() {};

//------------------------------------------------------------------------------------

gc_locus_match_single_empty::gc_locus_match_single_empty() throw ()
    : gc_cmdfile_err(gcerr_cmdfile::locusMatchSingleEmpty)
{
}

gc_locus_match_single_empty::~gc_locus_match_single_empty() throw() {};

//------------------------------------------------------------------------------------

gc_locus_match_unknown::gc_locus_match_unknown(const wxString & matchType) throw ()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::locusMatchUnknown,matchType.c_str()))
{
}

gc_locus_match_unknown::~gc_locus_match_unknown() throw() {};

//------------------------------------------------------------------------------------

gc_pop_match_byname_not_empty::gc_pop_match_byname_not_empty() throw ()
    : gc_cmdfile_err(gcerr_cmdfile::popMatchByNameNotEmpty)
{
}

gc_pop_match_byname_not_empty::~gc_pop_match_byname_not_empty() throw() {};

//------------------------------------------------------------------------------------

gc_pop_match_single_empty::gc_pop_match_single_empty() throw ()
    : gc_cmdfile_err(gcerr_cmdfile::popMatchSingleEmpty)
{
}

gc_pop_match_single_empty::~gc_pop_match_single_empty() throw() {};

//------------------------------------------------------------------------------------

gc_pop_match_unknown::gc_pop_match_unknown(const wxString & matchType) throw ()
    : gc_cmdfile_err(wxString::Format(gcerr_cmdfile::popMatchUnknown,matchType.c_str()))
{
}

gc_pop_match_unknown::~gc_pop_match_unknown() throw() {};

//____________________________________________________________________________________
