// $Id: gc_individual_err.h,v 1.8 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_INDIVIDUAL_ERR
#define GC_INDIVIDUAL_ERR

#include "gc_errhandling.h"

class GCIndividual;
class GCParseSample;

class gc_individual_err : public gc_ex
{
  public:
    gc_individual_err(wxString msg) throw() ;
    virtual ~gc_individual_err() throw() ;
};

//------------------------------------------------------------------------------------

class gc_phase_locus_repeat : public gc_individual_err
{
  public:
    gc_phase_locus_repeat(wxString indName, wxString locusName) throw();
    virtual ~gc_phase_locus_repeat() throw() ;
};

//------------------------------------------------------------------------------------

class gc_sample_locus_repeat : public gc_individual_err
{
  public:
    gc_sample_locus_repeat(wxString sampleName, wxString locusName) throw();
    virtual ~gc_sample_locus_repeat() throw() ;
};

//------------------------------------------------------------------------------------

class gc_sample_missing_locus_data : public gc_individual_err
{
  public:
    gc_sample_missing_locus_data(wxString sampleName, wxString locusName) throw();
    virtual ~gc_sample_missing_locus_data() throw() ;
};

//------------------------------------------------------------------------------------

class gc_ind_missing_phase_for_locus : public gc_individual_err
{
  public:
    gc_ind_missing_phase_for_locus(wxString indName, wxString locusName) throw();
    virtual ~gc_ind_missing_phase_for_locus() throw() ;
};

//------------------------------------------------------------------------------------

class gc_ind_wrong_sample_count : public gc_individual_err
{
  public:
    gc_ind_wrong_sample_count(const GCIndividual&) throw();
    virtual ~gc_ind_wrong_sample_count() throw() ;
};

#endif  // GC_INDIVIDUAL_ERR

//____________________________________________________________________________________
