// $Id: timesize.h,v 1.7 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TIMESIZE_H
#define TIMESIZE_H

#include "constants.h"
#include "vectorx.h"
#include "forceparam.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

class TimeSize
{
  public:
    // we accept the compiler-generated ctor, copy-ctor, and operator=
    virtual ~TimeSize() {};
    virtual TimeSize* Clone() const = 0;

    virtual DoubleVec1d PartitionSizeAt(force_type, const ForceParameters& fp,
                                        double etime) const = 0;
    virtual DoubleVec1d SizeAt(const ForceParameters& fp, double etime) const = 0;

}; // TimeSize

//------------------------------------------------------------------------------------

class GrowTimeSize : public TimeSize
{
  public:
    // we accept the compiler-generated ctor, copy-ctor, and operator=
    virtual ~GrowTimeSize() {};
    virtual TimeSize* Clone() const { return new GrowTimeSize(*this); };

    virtual DoubleVec1d PartitionSizeAt(force_type partforce,
                                        const ForceParameters& fp, double etime) const;
    virtual DoubleVec1d SizeAt(const ForceParameters& fp, double etime) const;

}; // GrowTimeSize

//------------------------------------------------------------------------------------

class StaticTimeSize : public TimeSize
{
  public:
    // we accept the compiler-generated ctor, copy-ctor, and operator=
    virtual ~StaticTimeSize() {};
    virtual TimeSize* Clone() const { return new StaticTimeSize(*this); };

    virtual DoubleVec1d PartitionSizeAt(force_type partforce,
                                        const ForceParameters& fp, double etime) const;
    virtual DoubleVec1d SizeAt(const ForceParameters& fp, double etime) const;

}; // StaticTimeSize

//------------------------------------------------------------------------------------

class LogisticSelectionTimeSize : public TimeSize
{
  public:
    LogisticSelectionTimeSize() {};
    virtual ~LogisticSelectionTimeSize() {};
    virtual TimeSize* Clone() const { return new LogisticSelectionTimeSize(*this); };

    virtual DoubleVec1d PartitionSizeAt(force_type partforce,
                                        const ForceParameters& fp, double etime) const;
    virtual DoubleVec1d SizeAt(const ForceParameters& fp, double etime) const;

}; // LogisticSelectionTimeSize

#endif // TIMESIZE_H

//____________________________________________________________________________________
