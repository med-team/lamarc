// $Id: epoch.cpp,v 1.5 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


//------------------------------------------------------------------------------------

#include "epoch.h"
#include "vectorx.h"

//------------------------------------------------------------------------------------

Epoch::Epoch(const LongVec1d& here, const LongVec1d& departing, long arriving)
    : m_here(here),
      m_departing(departing),
      m_arriving(arriving)
{
    // deliberately blank
} // Epoch ctor

//____________________________________________________________________________________
