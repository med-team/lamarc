// $Id: stair.h,v 1.5 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef STAIR_H
#define STAIR_H

#include<list>
#include "vectorx.h"

//------------------------------------------------------------------------------------

class StairRiser
{
  public:
    StairRiser(const DoubleVec1d& thetas, double tiptime, double roottime);
    // we accept the default copy ctor, operator= and dtor.
    bool operator<(const StairRiser& other) const;

    void SetThetas(const DoubleVec1d& thetas);
    void SetTipendTime(double newtime);
    void SetRootendTime(double newtime);

    DoubleVec1d GetThetas() const;
    double GetTipendTime() const;
    double GetRootendTime() const;

  private:
    DoubleVec1d m_thetas;
    double m_tiptime, m_roottime; // times at the tipward and rootward edge of
    // a riser.

    StairRiser();                 // the default ctor is disabled.

};

//------------------------------------------------------------------------------------

typedef std::list<StairRiser>     stair;

#endif // STAIR_H

//____________________________________________________________________________________
