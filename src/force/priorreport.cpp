// $Id: priorreport.cpp,v 1.8 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>
#include <iostream>
#include <algorithm>                    // for std::find

#include "priorreport.h"
#include "force.h"
#include "parameter.h"
#include "prior.h"

//------------------------------------------------------------------------------------

PriorReport::PriorReport(const Force& theforce) :
    m_forcename(theforce.GetFullName())
{
    const vector<Parameter>& params(theforce.GetParameters());

    vector<Parameter>::const_iterator param;
    for(param = params.begin(); param != params.end(); ++param)
    {
        if (!(param->IsValidParameter())) continue;
        if (param->GetStatus().Status() == pstat_constant) continue;

        vector<Prior>::iterator found(std::find(m_priors.begin(), m_priors.end(), param->GetPrior()));
        if (found != m_priors.end())
            m_whichparams[std::distance(m_priors.begin(),found)].
                push_back(param->GetUserName());
        else
        {
            StringVec1d newname(1UL,param->GetUserName());
            m_whichparams.push_back(newname);
            m_priors.push_back(param->GetPrior());
        }
    }
} // PriorReport ctor

//------------------------------------------------------------------------------------

void PriorReport::WriteTo(std::ofstream& outf) const
{
    assert(m_whichparams.size() == m_priors.size());

    outf << m_forcename << " Priors" << std::endl;
    outf << "parameter(s)   type   bounds" << std::endl;
    unsigned long ind, nind(m_whichparams.size());
    for(ind = 0; ind < nind; ++ind)
    {
        string line(m_whichparams[ind][0]);
        line += " | " + ToString(m_priors[ind].GetPriorType());
        line += " | " + ToString(m_priors[ind].GetLowerBound());
        line += " to " + ToString(m_priors[ind].GetUpperBound());
        outf << line << std::endl;
        StringVec1d::const_iterator addtlname(m_whichparams[ind].begin());
        for(++addtlname; addtlname != m_whichparams[ind].end(); ++addtlname)
            outf << *addtlname << " -- as above" << std::endl;
        outf << std::endl;
    }
} // PriorReport::WriteTo

//____________________________________________________________________________________
