// $Id: stair.cpp,v 1.4 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include "stair.h"

//------------------------------------------------------------------------------------

StairRiser::StairRiser(const DoubleVec1d& thetas, double tiptime,
                       double roottime) : m_thetas(thetas), m_tiptime(tiptime),
                                          m_roottime(roottime)
{
    // deliberately blank
} // StairRiser ctor

//------------------------------------------------------------------------------------

bool StairRiser::operator<(const StairRiser& other) const
{
    return (m_tiptime < other.m_tiptime);
} // StairRiser::operator<

//------------------------------------------------------------------------------------

void StairRiser::SetThetas(const DoubleVec1d& thetas)
{
    m_thetas = thetas;
} // StairRiser::SetThetas

//------------------------------------------------------------------------------------

void StairRiser::SetTipendTime(double newtime)
{
    m_tiptime = newtime;
} // StairRiser::SetTipendTime

//------------------------------------------------------------------------------------

void StairRiser::SetRootendTime(double newtime)
{
    m_roottime = newtime;
} // StairRiser::SetRootendTime

//------------------------------------------------------------------------------------

DoubleVec1d StairRiser::GetThetas() const
{
    return m_thetas;
} // StairRiser::GetThetas

//------------------------------------------------------------------------------------

double StairRiser::GetTipendTime() const
{
    return m_tiptime;
} // StairRiser::GetTipendTime

//------------------------------------------------------------------------------------

double StairRiser::GetRootendTime() const
{
    return m_roottime;
} // StairRiser::GetRootendTime

//____________________________________________________________________________________
