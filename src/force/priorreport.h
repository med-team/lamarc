// $Id: priorreport.h,v 1.4 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/********************************************************************
 PriorReport is a collection class that creates an output report on
 the priors used during a Lamarc run.

 Currently the report takes the form of a StringVec1d, dimension is
 the number of lines of character output.

 Written by Jon Yamato
********************************************************************/

#ifndef PRIORREPORT_H
#define PRIORREPORT_H

#include "vectorx.h"
#include "constants.h"
#include "defaults.h"

class Force;
class Prior;

class PriorReport
{
  private:
    PriorReport(); //undefined

    std::string m_forcename;

    // all dim: # unique priors for force
    StringVec2d m_whichparams;
    vector<Prior> m_priors;

  public:
    // we accept default copy-ctor and operator=
    PriorReport(const Force& theforce);

    void WriteTo(std::ofstream& out) const;
};

#endif // PRIORREPORT_H

//____________________________________________________________________________________
