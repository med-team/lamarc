// $Id: newmenu.h,v 1.24 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef NEWMENU_H
#define NEWMENU_H

#include <string>

#include "menudefs.h"
#include "menuinteraction.h"
#include "menuitem.h"
#include "menutypedefs.h"
#include "vectorx.h"

class Display;
class UIInterface;

class NewMenu : public MenuInteraction
{
  protected:
    UIInterface & ui;
    const std::string title;
    const std::string info;
    MenuDisplayQuantaVec afterContent;
    MenuDisplayQuantaVec beforeContent;
    MenuDisplayQuantaVec myContent;
    virtual void AddMenuItem(MenuDisplayQuanta*);
    void fillStandardContent(bool atTop);
  public:
    NewMenu(UIInterface & myui, const std::string myTitle, bool atTop=false);
    NewMenu(UIInterface & myui, const std::string myTitle, const std::string myInfo, bool atTop=false);
    virtual ~NewMenu();
    virtual bool Handles(std::string input);
    virtual MenuInteraction_ptr GetHandler(std::string input);
    virtual menu_return_type InvokeMe(Display&);
    virtual std::string Title();
    virtual std::vector<std::string> Warnings();
    virtual void AddWarning(std::string warning);
    virtual std::string Info();
    virtual MenuDisplayQuantaVec MenuItems();
    virtual StringVec1d GetInconsistencies();
};

class NewMenuCreator
{
  public:
    NewMenuCreator();
    virtual ~NewMenuCreator();
    //        virtual NewMenu * Create() = 0;
    virtual NewMenu_ptr Create() = 0;
};

#endif // NEWMENU_H

//____________________________________________________________________________________
