// $Id: matrixitem.h,v 1.12 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef MATRIXITEM_H
#define MATRIXITEM_H

#include <string>
#include "menuinteraction.h"
#include "newmenu.h"
#include "setmenuitem.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "vectorx.h"

class LowerLevelMenuItemGroup : public SetMenuItemGroup
{
  protected:
    std::string legalIdsMenuKey;
    std::string visibilityGuard;
    UIId intoId;
    virtual UIId translateLocalToGlobal(UIId localId);
    virtual UIId translateGlobalToLocal(UIId globalId);
  public:
    LowerLevelMenuItemGroup(UIInterface & myui,
                            std::string myMenuKey,
                            std::string myLegalIdsMenuKey,
                            std::string myVisibilityGuard,
                            UIId myId);
    virtual ~LowerLevelMenuItemGroup();
    virtual std::string GetKey(UIId id);
    virtual UIIdVec1d GetVisibleIds();
};

class LowerLevelMenu : public NewMenu
{
  public:
    LowerLevelMenu(UIInterface & myui,
                   std::string myMenuKey,
                   std::string lowerLevelMenuKey,
                   std::string myLegalIdsMenuKey,
                   std::string myVisibilityGuard,
                   UIId myId);
    virtual ~LowerLevelMenu();
};

class MatrixSetMenuItem : public MenuDisplayGroupBaseImplementation
{
  protected:
    std::string lowerLevelMenuKey;
    std::string legalIdsMenuKey;
    std::string visibilityGuard;
    UIId        forceId;
    // returns sub-menu
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
  public:
    MatrixSetMenuItem(UIInterface & myui,
                      std::string myMenuKey,
                      std::string myLowerLevelMenuKey,
                      std::string myLegalIdsMenuKey,
                      std::string myVisibilityGuard,
                      force_type ftype);
    virtual ~MatrixSetMenuItem();
    UIIdVec1d GetVisibleIds();
};

#endif  // MATRIXITEM_H

//____________________________________________________________________________________
