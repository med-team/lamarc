// $Id: menuerror.h,v 1.8 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef MENUERROR_H
#define MENUERROR_H

#include <stdexcept>
#include <string>

class DialogProtocolError : public std::exception
{
  private:
    std::string _what;
  public:
    DialogProtocolError(const std::string& wh): _what (wh) { };
    virtual ~DialogProtocolError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
};

#endif  // MENUERROR_H

//____________________________________________________________________________________
