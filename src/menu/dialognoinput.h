// $Id: dialognoinput.h,v 1.14 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DIALOGNOINPUT_H
#define DIALOGNOINPUT_H

#include <string>

#include "dialog.h"
#include "menudefs.h"
#include "vectorx.h"

class Display;

class DialogNoInput : public Dialog
{
  protected:
    virtual std::string outputString() = 0;
    virtual void performAction();
  public:
    DialogNoInput();
    virtual ~DialogNoInput();
    virtual menu_return_type InvokeMe(Display&);
};

class DialogAcknowledge : public DialogNoInput
{
  public:
    DialogAcknowledge();
    virtual ~DialogAcknowledge();
    virtual menu_return_type InvokeMe(Display&);
};

class DialogChideInconsistencies : public DialogAcknowledge
{
  protected:
    StringVec1d & inconsistencies;
    virtual std::string outputString();
  public:
    DialogChideInconsistencies(StringVec1d & inconsistencies);
    virtual ~DialogChideInconsistencies();
};

#endif // DIALOGNOINPUT_H

//____________________________________________________________________________________
