// $Id: dialognoinput.cpp,v 1.15 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>

#include "dialog.h"
#include "dialognoinput.h"
#include "display.h"
#include "menudefs.h"
#include "menu_strings.h"
#include "stringx.h"

using std::string;

DialogNoInput::DialogNoInput() : Dialog()
{
}

DialogNoInput::~DialogNoInput()
{
}

void DialogNoInput::performAction()
{
    // default is to do nothing at all
}

menu_return_type DialogNoInput::InvokeMe(Display & display)
{
    display.DisplayDialogOutput(outputString());
    performAction();
    return menu_REDISPLAY;
}

//------------------------------------------------------------------------------------

DialogAcknowledge::DialogAcknowledge() : DialogNoInput()
{
}

DialogAcknowledge::~DialogAcknowledge()
{
}

menu_return_type DialogAcknowledge::InvokeMe(Display & display)
{
    display.DisplayDialogOutput(outputString());
    display.DisplayDialogOutput(menustr::acknowledge);
    display.GetUserInput();
    return menu_REDISPLAY;
}

//------------------------------------------------------------------------------------

std::string DialogChideInconsistencies::outputString()
{
    std::string allInconsistencies = ToString(inconsistencies);
    std::string wholeMessage = menustr::inconsistencies + allInconsistencies;
    return wholeMessage;
}

DialogChideInconsistencies::DialogChideInconsistencies(StringVec1d & myInconsistencies)
    : inconsistencies(myInconsistencies)
{
}

DialogChideInconsistencies::~DialogChideInconsistencies()
{
}

//____________________________________________________________________________________
