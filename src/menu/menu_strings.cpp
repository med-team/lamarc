// $Id: menu_strings.cpp,v 1.15 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "menu_strings.h"

using std::string;

const string key::dot   = ".";
const string key::Q     = "Q";
const string key::R     = "R";

const string menustr::acknowledge      = "\nType <Return> to continue\n";
const string menustr::bottomLine       = "<Return> = Go Up | . = Run | Q = Quit";
const string menustr::bottomLineAtTop  = ". = Run | Q = Quit";
const string menustr::carriageReturn   = "\n";
const string menustr::divider          = "----------";
const string menustr::emptyString      = "";
const string menustr::inconsistencies  = "\n\nYou must fix inconsistencies with the following menu\nitems before exiting the current menu:\n\t";
const string menustr::space            = " ";
const string menustr::initial          = "Initial";
const string menustr::final            = "Final";
const string menustr::chains           = "Number of chains";
const string menustr::discard          = "Number of genealogies to discard";
const string menustr::interval         = "Sampling interval";
const string menustr::samples          = "Number of samples";

//____________________________________________________________________________________
