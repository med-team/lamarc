// $Id: twodtable.h,v 1.7 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TWODTABLE_H
#define TWODTABLE_H

#include <string>
#include "menuitem.h"

class Display;
class UIInterface;

class TwoDTable : public MenuDisplayTable
{
  protected:
    UIInterface & ui;
    virtual long ColCount(UIInterface & ui) = 0;
    virtual long RowCount(UIInterface & ui) = 0;
    virtual std::string Title(UIInterface & ui) = 0;
    virtual std::string RowHeader(UIInterface & ui);
    virtual std::string ColLabel(UIInterface & ui, long index) = 0;
    virtual std::string RowLabel(UIInterface & ui, long index) = 0;
    virtual std::string Cell(UIInterface & ui, long rowIndex, long colIndex) = 0;
    std::string CreateDisplayString(UIInterface & myui);

  public:
    TwoDTable(UIInterface & myui);
    virtual ~TwoDTable();
    virtual bool IsVisible();
    std::string CreateDisplayString();
};

#endif  // TWODTABLE_H

//____________________________________________________________________________________
