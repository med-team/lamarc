// $Id: menutypedefs.h,v 1.5 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef MENUTYPEDEFS_H
#define MENUTYPEDEFS_H

#include <memory>

class NewMenu;
//typedef boost::shared_ptr<NewMenu> NewMenu_ptr;

typedef std::auto_ptr<NewMenu> NewMenu_ptr;

class MenuInteraction;

//typedef boost::shared_ptr<MenuInteraction> MenuInteraction_ptr;

typedef std::auto_ptr<MenuInteraction> MenuInteraction_ptr;

#endif // MENUTYPEDEFS_H

//____________________________________________________________________________________
