// $Id: setmenuitem.cpp,v 1.28 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>

#include "setmenuitem.h"
#include "ui_strings.h"
#include "constants.h"
#include "stringx.h"
#include "ui_constants.h"
#include "newmenuitems.h"

using std::string;

//------------------------------------------------------------------------------------

string SetDialog::inLoopOutputString()
{
    if (getMin() == getMax())
    {
        assert (getMin() == ToString(FLAGDOUBLE));
        return "Enter " + getText() + "\n";
    }
    else
    {
        return "Enter " + getText() + " (between " + getMin() + " and "
            + getMax() + ").\n";
    }
}

SetMenuItem::SetMenuItem(std::string myKey, UIInterface & myui, std::string mk )
    : KeyedMenuItem(myKey), ui(myui) , menuKey(mk)
{
}

SetMenuItem::~SetMenuItem()
{
}

string SetMenuItem::GetText()
{
    return ui.doGetDescription(menuKey,GetId());
}

std::string SetMenuItem::GetVariableText()
{
    return ui.doGetPrintString(menuKey,GetId());
}

MenuInteraction_ptr SetMenuItem::GetHandler(std::string input)
{
    assert(Handles(input));
    return MenuInteraction_ptr(new SetDialog(ui,menuKey,GetId()));
}

bool SetMenuItem::IsVisible()
{
    return true;
}

bool SetMenuItem::IsConsistent()
{
    return ui.doGetConsistency(menuKey,GetId());
}

//------------------------------------------------------------------------------------

SetMenuItemNoId::SetMenuItemNoId(std::string myKey, UIInterface & myui, std::string myMenuKey)
    : SetMenuItem(myKey,myui,myMenuKey)
{
}

SetMenuItemNoId::~SetMenuItemNoId()
{
}

UIId SetMenuItemNoId::GetId()
{
    return NO_ID();
}

//------------------------------------------------------------------------------------

SetMenuItemId::SetMenuItemId(std::string myKey, UIInterface & myui, std::string myMenuKey, UIId myId)
    : SetMenuItem(myKey,myui,myMenuKey) , id(myId)
{
}

SetMenuItemId::~SetMenuItemId()
{
}

UIId SetMenuItemId::GetId()
{
    return id;
}

///// MenuDisplayGroupBaseImplementation

MenuDisplayGroupBaseImplementation::MenuDisplayGroupBaseImplementation(UIInterface & myui, string myMenuKey)
    : ui(myui) , menuKey(myMenuKey)
{
}

MenuDisplayGroupBaseImplementation::~MenuDisplayGroupBaseImplementation()
{
}

string MenuDisplayGroupBaseImplementation::GetKey(UIId id)
{
    return indexToKey(id.GetIndex1()); // EWFIX.P3 ERRORCHECKING : what if GetIndex1 not unique?
}

string MenuDisplayGroupBaseImplementation::GetText(UIId id)
{
    return ui.doGetDescription(menuKey,id);
}

string MenuDisplayGroupBaseImplementation::GetVariableText(UIId id)
{
    return ui.doGetPrintString(menuKey,id);
}

bool MenuDisplayGroupBaseImplementation::Handles(string input)
{
    // kinda wasteful, but avoids some unpleasant to
    // code error checking
    vector<UIId> visibles = GetVisibleIds();
    vector<UIId>::iterator i;
    for(i = visibles.begin(); i != visibles.end(); i++)
    {
        UIId& visibleId = *i;
        if(CaselessStrCmp(GetKey(visibleId),input))
        {
            return true;
        }
    }
    return false;
}

MenuInteraction_ptr MenuDisplayGroupBaseImplementation::GetHandler(string input)
{
    // kinda wasteful, but avoids some unpleasant to
    // code error checking
    vector<UIId> visibles = GetVisibleIds();
    vector<UIId>::iterator i;
    for(i = visibles.begin(); i != visibles.end(); i++)
    {
        UIId& visibleId = *i;
        if(CaselessStrCmp(GetKey(visibleId),input))
        {
            return MakeOneHandler(visibleId);
        }
    }
    assert(false); //A group with nothing in it?
    return MenuInteraction_ptr(new DoNothingHandler());
}

///// SetMenuItemGroup

SetMenuItemGroup::SetMenuItemGroup(UIInterface & myui, string myMenuKey)
    : MenuDisplayGroupBaseImplementation(myui,myMenuKey)
{
}

SetMenuItemGroup::~SetMenuItemGroup()
{
}

MenuInteraction_ptr SetMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new SetDialog(ui,menuKey,id));
}

///// ToggleMenuItemGroup

ToggleMenuItemGroup::ToggleMenuItemGroup(UIInterface & myui, string myMenuKey)
    : MenuDisplayGroupBaseImplementation(myui,myMenuKey)
{
}

ToggleMenuItemGroup::~ToggleMenuItemGroup()
{
}

MenuInteraction_ptr ToggleMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new NewToggleHandler(ui,menuKey,id));
}

//____________________________________________________________________________________
