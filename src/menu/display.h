// $Id: display.h,v 1.24 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DISPLAY_H
#define DISPLAY_H

#include <string>
#include <vector>

#include "menudefs.h" // for menu_return_type
#include "vectorx.h"

class Dialog;
class NewMenu;
class MenuDisplayQuantaVec;
class MenuDisplayGroup;
class ToggleMenuItem2dGroup;
class MenuDisplayLine;
class MenuDisplayTable;

class Display
{
  public:
    Display() {};
    virtual ~Display() {};
    virtual menu_return_type DisplayNewMenu(NewMenu&) = 0;
    virtual menu_return_type DisplayRaw(StringVec1d) = 0;
    //
    virtual void ShowMenuDisplayGroup(MenuDisplayGroup&) = 0;
    virtual void ShowMenuDisplay2dGroup(ToggleMenuItem2dGroup&) = 0;
    virtual void ShowMenuDisplayLine(MenuDisplayLine&) = 0;
    virtual void ShowMenuDisplayTable(MenuDisplayTable&) = 0;
    //
    virtual void DisplayDialogOutput(std::string) = 0;
    virtual std::string GetUserInput() = 0;
};

class ScrollingDisplay : public Display
{
  protected:
    void ShowMenuDisplayQuantaVec(MenuDisplayQuantaVec);
    void Prompt();
    void Title(const std::string & title, const std::string & info);
    void DisplayOneLine(std::string);
    void ShowOneMenuLine(std::string,std::string,std::string);
    void ShowExtraMenuLines(std::vector<string> storedMenuText, std::vector<string> generatedMenuText);
    std::vector<std::string> BreakAtCarriageReturns(std::string);
    std::vector<std::string> MakeDisplayableUnits(std::string, long int width);
    void ChideOnInconsistencies(StringVec1d inconsistencies);
  public:
    ScrollingDisplay();
    ~ScrollingDisplay();
    //
    virtual menu_return_type DisplayNewMenu(NewMenu&);
    virtual menu_return_type DisplayRaw(StringVec1d);
    //
    virtual void ShowMenuDisplayGroup(MenuDisplayGroup&);
    virtual void ShowMenuDisplay2dGroup(ToggleMenuItem2dGroup&);
    virtual void ShowMenuDisplayLine(MenuDisplayLine&);
    virtual void ShowMenuDisplayTable(MenuDisplayTable&);
    //
    virtual void DisplayDialogOutput(std::string);
    virtual std::string GetUserInput();
    //
    void Warn(std::vector<std::string> warnings);
};

class NoDisplay : public Display
{
  public:
    NoDisplay() ;
    ~NoDisplay();
    virtual menu_return_type DisplayNewMenu(NewMenu&);
    virtual menu_return_type DisplayRaw(StringVec1d);
    //
    virtual void ShowMenuDisplayGroup(MenuDisplayGroup&);
    virtual void ShowMenuDisplay2dGroup(ToggleMenuItem2dGroup&);
    virtual void ShowMenuDisplayLine(MenuDisplayLine&);
    virtual void ShowMenuDisplayTable(MenuDisplayTable&);
    //
    virtual void DisplayDialogOutput(std::string);
    virtual std::string GetUserInput();
};

#endif  // DISPLAY_H

//____________________________________________________________________________________
