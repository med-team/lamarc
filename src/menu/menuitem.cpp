// $Id: menuitem.cpp,v 1.27 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// menuitem.cpp
// functions to control the menuitem (a single line in a menu)
//
// Peter Beerli

#include <vector>

#include "display.h"
#include "errhandling.h"
#include "menuinteraction.h"
#include "menuitem.h"
#include "menutypedefs.h"
#include "stringx.h"
#include "ui_constants.h"

void
MenuDisplayQuantaVec::NukeContents()
{
    MenuDisplayQuantaVec::iterator iter;
    for(iter = begin(); iter != end(); iter++)
    {
        delete(*iter);
    }
}

MenuInteraction_ptr
MenuDisplayQuantaWithHandler::GetHandler(string)
{
    return MenuInteraction_ptr(NULL);
}

void MenuDisplayLine::DisplayItemOn(Display & display)
{
    display.ShowMenuDisplayLine(*this);
}

UIId& MenuDisplayLine::GetId()
{
    return NO_ID();
}

bool MenuDisplayLine::IsVisible()
{
    return true;
}

bool MenuDisplayLine::IsConsistent()
{
    return true;
}

void MenuDisplayGroup::DisplayItemOn(Display & display)
{
    display.ShowMenuDisplayGroup(*this);
}

std::vector<UIId> MenuDisplayGroup::GetInconsistentIds()
{
    std::vector<UIId> badIds;   // empty vector => no bad ids!
    return badIds;
}

void MenuDisplayTable::DisplayItemOn(Display & display)
{
    display.ShowMenuDisplayTable(*this);
}

DisplayOnlyGroupWrapper::DisplayOnlyGroupWrapper(MenuDisplayGroup * group)
    : m_group(group)
{
}

DisplayOnlyGroupWrapper::~DisplayOnlyGroupWrapper()
{
    delete m_group;
}

vector<UIId>
DisplayOnlyGroupWrapper::GetVisibleIds()
{
    return m_group->GetVisibleIds();
}

vector<UIId>
DisplayOnlyGroupWrapper::GetInconsistentIds()
{
    vector<UIId> emptyVector;
    return emptyVector;
}

string
DisplayOnlyGroupWrapper::GetKey(UIId)
{
    return menustr::emptyString;
}

string
DisplayOnlyGroupWrapper::GetText(UIId id)
{
    return m_group->GetText(id);
}

string
DisplayOnlyGroupWrapper::GetVariableText(UIId id)
{
    return m_group->GetVariableText(id);
}

KeyedMenuItem::KeyedMenuItem(const string& myKey)
    : MenuDisplayLine(), key(myKey)
{
}

KeyedMenuItem::~KeyedMenuItem()
{
}

string KeyedMenuItem::GetKey()
{
    return key;
}

bool KeyedMenuItem::Handles(std::string inputKey)
{
    if(IsVisible())
    {
        return CaselessStrCmp(GetKey(),inputKey);
    }
    return false;
}

//____________________________________________________________________________________
