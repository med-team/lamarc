// $Id: menu_strings.h,v 1.17 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef MENU_STRINGS_H
#define MENU_STRINGS_H

#include <string>

class key
{
  public:
    static const std::string dot;
    static const std::string R;
    static const std::string Q;
};

class menustr
{
  public:
    static const std::string acknowledge;
    static const std::string bottomLine;
    static const std::string bottomLineAtTop;
    static const std::string divider;
    static const std::string carriageReturn;
    static const std::string emptyString;
    static const std::string inconsistencies;
    static const std::string space;
    static const std::string initial;
    static const std::string final;
    static const std::string chains;
    static const std::string discard;
    static const std::string interval;
    static const std::string samples;
};

#endif // MENU_STRINGS_H

//____________________________________________________________________________________
