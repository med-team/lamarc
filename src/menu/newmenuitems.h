// $Id: newmenuitems.h,v 1.34 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef NEWMENUITEM_H
#define NEWMENUITEM_H

#include <string>
#include "dialognoinput.h"
#include "dialogrepeat.h"
#include "menudefs.h"
#include "menuitem.h"
#include "menuinteraction.h"
#include "menu_strings.h"
#include "newmenu.h"
#include "ui_constants.h"

class UIInterface;

// puts text into the menu but cannot be entered
class OutputOnlyMenuItem : public MenuDisplayLine
{
  protected:
    std::string displayString;
  public:
    OutputOnlyMenuItem(std::string toDisplay) {displayString = toDisplay;};
    virtual ~OutputOnlyMenuItem() {};
    virtual bool Handles(std::string) { return false;};
    virtual MenuInteraction_ptr GetHandler(std::string) {return MenuInteraction_ptr(NULL);};
    virtual std::string GetKey() { return menustr::emptyString;};
    virtual std::string GetText() {return displayString;};
    virtual std::string GetVariableText() {return "";};
    virtual bool IsVisible() {return true;};
};

// outputs a blank line
class BlankMenuItem : public OutputOnlyMenuItem
{
  public:
    BlankMenuItem() : OutputOnlyMenuItem(menustr::space) {};
    virtual ~BlankMenuItem() {};
};

// outputs a dashed line
class DividerMenuItem : public OutputOnlyMenuItem
{
  public:
    DividerMenuItem() : OutputOnlyMenuItem(menustr::divider) {};
    virtual ~DividerMenuItem() {};
};

// for menu items that result in the same menu being re-displayed
class SameLevelMenuItem : public KeyedMenuItem
{
  protected:
    UIInterface & ui;
    std::string menuKey;
  public:
    SameLevelMenuItem(std::string myKey, UIInterface & myUi, std::string myMenuKey);
    virtual ~SameLevelMenuItem();
    // ---- MenuItem methods
    virtual std::string GetVariableText();
};

// ideally, this item would go in the lamarcmenus directory
// and we would have a "LamarcMenu" class (inheriting from "NewMenu")
// into which we'd put this. Unfortunately, that is stymied by
// the fact that we have class "LowerLevelMenu" inheriting from
// NewMenu as well.
class FileDumpMenuItem : public SameLevelMenuItem
{
  public:
    FileDumpMenuItem(UIInterface & myUi);
    virtual ~FileDumpMenuItem();
    // ---- MenuItem methods
    virtual MenuInteraction_ptr GetHandler(std::string);
    virtual std::string GetText();

};

class FileDumpHandler : public DialogRepeat
{
  protected:
    UIInterface & ui;
    long maxTries() {return 3;};
    std::string beforeLoopOutputString();
    std::string inLoopOutputString();
    std::string inLoopFailureOutputString();
    std::string afterLoopSuccessOutputString();
    std::string afterLoopFailureOutputString();
    bool        handleInput(std::string inputString);
    void        doFailure() {};
  public:
    FileDumpHandler(UIInterface & myui) : ui(myui) {};
    virtual ~FileDumpHandler() {};
};

class UndoMenuItem : public SameLevelMenuItem
{
  public:
    UndoMenuItem(UIInterface & myUi);
    virtual ~UndoMenuItem();
    // ---- MenuItem methods
    virtual std::string GetText();
    virtual bool IsVisible();
    virtual MenuInteraction_ptr GetHandler(std::string);

};

class UndoHandler : public MenuInteraction
{
  protected:
    UIInterface & ui;
  public:
    UndoHandler(UIInterface & myui) : ui(myui) {};
    virtual ~UndoHandler() {};
    virtual menu_return_type InvokeMe(Display & display);
};

class RedoMenuItem : public SameLevelMenuItem
{
  public:
    RedoMenuItem(UIInterface & myUi);
    virtual ~RedoMenuItem();
    // ---- MenuItem methods
    virtual std::string GetText();
    virtual bool IsVisible();
    virtual MenuInteraction_ptr GetHandler(std::string);

};

class RedoHandler : public MenuInteraction
{
  protected:
    UIInterface & ui;
  public:
    RedoHandler(UIInterface & myui) : ui(myui) {};
    virtual ~RedoHandler() {};
    virtual menu_return_type InvokeMe(Display & display);
};

class BottomLineMenuItem : public MenuDisplayLine
{
  protected:
    UIInterface& m_ui;
  public:
    BottomLineMenuItem(UIInterface& ui);
    virtual ~BottomLineMenuItem();
    virtual std::string GetKey() { return menustr::emptyString;};

#if 0
    virtual long GetKeyid() { return -1;};
#endif

    virtual std::string GetText() { return menustr::bottomLine;};
    virtual bool Handles(std::string inputKey);
    virtual std::string GetVariableText() { return menustr::emptyString;};
    virtual MenuInteraction_ptr GetHandler(std::string inputKey);
};

class BottomLineMenuItemAtTop : public BottomLineMenuItem
{
  public:
    BottomLineMenuItemAtTop(UIInterface& ui);
    virtual ~BottomLineMenuItemAtTop();
    virtual std::string GetText() { return menustr::bottomLineAtTop;};
};

class DoNothingHandler : public MenuInteraction
{
  public:
    DoNothingHandler() {};
    virtual ~DoNothingHandler() {};
    virtual menu_return_type InvokeMe(Display & display);
};

class RunHandler : public DialogAcknowledge
{
  protected:
    UIInterface& m_ui;
    virtual std::string outputString();
  public:
    RunHandler(UIInterface& ui): m_ui(ui) {};
    virtual ~RunHandler() {};
    virtual menu_return_type InvokeMe(Display & display);
};

class QuitHandler : public MenuInteraction
{
  public:
    QuitHandler() {};
    virtual ~QuitHandler() {};
    virtual menu_return_type InvokeMe(Display &)
    { return menu_QUIT;};
};

class GoUpHandler : public MenuInteraction
{
  public:
    GoUpHandler() {};
    virtual ~GoUpHandler() {};
    virtual menu_return_type InvokeMe(Display &)
    { return menu_GO_UP;};
};

class SubMenuItem : public KeyedMenuItem
{
  protected:
    UIInterface & ui;
    NewMenuCreator * creator;
  public:
    SubMenuItem(std::string myKey,UIInterface & myui, NewMenuCreator * myCreator);
    virtual ~SubMenuItem();
    // ---- MenuItem methods
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string inputKey) ;
};

#endif  // NEWMENUITEM_H

//____________________________________________________________________________________
