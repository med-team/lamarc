// $Id: dialogrepeat.h,v 1.12 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DIALOGREPEAT_H
#define DIALOGREPEAT_H

#include <string>
#include "dialog.h"

class Display;

class DialogRepeat : public Dialog
{
  protected:
    virtual long maxTries() = 0;
    virtual std::string beforeLoopOutputString() = 0;
    virtual std::string inLoopOutputString() = 0;
    virtual std::string inLoopFailureOutputString() = 0;
    virtual std::string afterLoopSuccessOutputString() = 0;
    virtual std::string afterLoopFailureOutputString() = 0;
    virtual bool handleInput(std::string input) = 0;
    virtual void doFailure() = 0;
  public:
    DialogRepeat();
    virtual ~DialogRepeat();
    virtual menu_return_type InvokeMe(Display&);
};

#endif // DIALOGREPEAT_H

//____________________________________________________________________________________
