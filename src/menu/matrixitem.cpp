// $Id: matrixitem.cpp,v 1.14 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "matrixitem.h"
#include "menuinteraction.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "ui_id.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"
#include "vectorx.h"

UIId LowerLevelMenuItemGroup::translateLocalToGlobal(UIId localId)
{
    return UIId(localId.GetForceType(), intoId.GetIndex1() * ui.doGetLong(legalIdsMenuKey) + localId.GetIndex1());
}

UIId LowerLevelMenuItemGroup::translateGlobalToLocal(UIId globalId)
{
    return UIId(globalId.GetForceType(), globalId.GetIndex1() - (intoId.GetIndex1() * ui.doGetLong(legalIdsMenuKey)));
}

LowerLevelMenuItemGroup::LowerLevelMenuItemGroup(
    UIInterface & myui,
    string myMenuKey,
    string myLegalIdsMenuKey,
    string myVisibilityGuard,
    UIId myId)
    : SetMenuItemGroup(myui,myMenuKey),
      legalIdsMenuKey(myLegalIdsMenuKey),
      visibilityGuard(myVisibilityGuard),
      intoId(myId)
{
}

LowerLevelMenuItemGroup::~LowerLevelMenuItemGroup()
{
}

string LowerLevelMenuItemGroup::GetKey(UIId globalId)
{
    return SetMenuItemGroup::GetKey(translateGlobalToLocal(globalId));
}

UIIdVec1d LowerLevelMenuItemGroup::GetVisibleIds()
{
    UIIdVec1d visibles;
    std::set<long> grouplist;
    if(ui.doGetBool(visibilityGuard))
    {
        long partitionCount = ui.doGetLong(legalIdsMenuKey);
        for(long fromId= 0; fromId < partitionCount; fromId++)
        {
            if(fromId != intoId.GetIndex1())
            {
                UIId thisId(intoId.GetForceType(), fromId);
                thisId = translateLocalToGlobal(thisId);
                long gindex = ui.GetCurrentVars().forces.ParamInGroup(thisId.GetForceType(), thisId.GetIndex1());
                if ((gindex == FLAGLONG) || (grouplist.find(gindex) == grouplist.end()))
                {
                    visibles.push_back(thisId);
                    grouplist.insert(gindex);
                }
            }
        }
    }
    return visibles;
}

//------------------------------------------------------------------------------------

LowerLevelMenu::LowerLevelMenu(
    UIInterface & myui,
    string myMenuKey,
    string lowerLevelMenuKey,
    string myLegalIdsMenuKey,
    string myVisibilityGuard,
    UIId myId)
    : NewMenu(myui,myui.doGetDescription(myMenuKey,myId))
{
    AddMenuItem(new LowerLevelMenuItemGroup(
                    myui,lowerLevelMenuKey,myLegalIdsMenuKey,myVisibilityGuard,myId));
}

LowerLevelMenu::~LowerLevelMenu()
{
}

//------------------------------------------------------------------------------------

MenuInteraction_ptr MatrixSetMenuItem::MakeOneHandler(UIId id)
{
    UIId newId(forceId.GetForceType(), id.GetIndex1());
    return MenuInteraction_ptr(new LowerLevelMenu
                               (ui,menuKey,lowerLevelMenuKey,legalIdsMenuKey,
                                visibilityGuard,newId));
}

MatrixSetMenuItem::MatrixSetMenuItem(
    UIInterface & myui,
    string myMenuKey,
    string myLowerLevelMenuKey,
    string myLegalIdsMenuKey,
    string myVisibilityGuard,
    force_type ftype)
    : MenuDisplayGroupBaseImplementation(myui,myMenuKey),
      lowerLevelMenuKey(myLowerLevelMenuKey),
      legalIdsMenuKey(myLegalIdsMenuKey),
      visibilityGuard(myVisibilityGuard),
      forceId(ftype)
{
}

#if 0
UIId MatrixSetMenuItem::GetIdFromKey(string key)
{
    return UIId(forceId.GetForceType(), keyToIndex(key));
}
#endif

MatrixSetMenuItem::~MatrixSetMenuItem()
{
}

UIIdVec1d MatrixSetMenuItem::GetVisibleIds()
{
    UIIdVec1d visibles;
    if(ui.doGetBool(visibilityGuard))
    {
        long partitionCount = ui.doGetLong(legalIdsMenuKey);
        for(long i= 0; i < partitionCount; i++)
        {
            visibles.push_back(UIId(forceId.GetForceType(),i));
        }
    }
    return visibles;
}

//____________________________________________________________________________________
