// $Id: gc_trait.h,v 1.13 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_TRAIT_H
#define GC_TRAIT_H

#include <map>
#include <set>
#include "gc_quantum.h"
#include "gc_set_util.h"
#include "wx/string.h"

class GCGenotypeResolution;
class gcPhenotype;
class GCStructures;
class gcTraitAllele;

class gcTraitInfo : public GCQuantum
{
    friend class GCStructures;

  private:
    bool                    m_hasRegion;
    size_t                  m_regionId;

    gcIdSet                 m_alleleIds;
    gcIdSet                 m_phenotypeIds;

    void SetRegionId(size_t id);
    void UnsetRegionId();

    void AddAllele(const gcTraitAllele &);
    void RemoveAllele(const gcTraitAllele &);

    void AddPhenotype(const gcPhenotype&);
    void RemovePhenotype(const gcPhenotype&);

  public:
    gcTraitInfo();
    ~gcTraitInfo();

    const gcIdSet & GetAlleleIds() const;
    bool            HasAlleleId(size_t alleleId) const;

    const gcIdSet & GetPhenotypeIds() const;
    bool            HasPhenotype(const gcPhenotype &)const;

    size_t      GetRegionId()       const   ;
    bool        HasRegionId()       const   ;

    void        DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_TRAIT_H

//____________________________________________________________________________________
