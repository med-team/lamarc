// $Id: gc_file.h,v 1.37 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FILE_H
#define GC_FILE_H

#include "gc_parse.h"
#include "gc_quantum.h"
#include "gc_structure_maps.h"
#include "gc_types.h"
#include "wx/string.h"

class GCDataStore;
class GCFileLines;

class GCFile : public GCQuantum
{
    friend class GCDataStore;   // EWFIX.P3 -- think about making a
    // separate object that has access
    // to the SetParses method -- it
    // should only be necessary at GCFile
    // creation time
  private:
    GCDataStore &       m_dataStore;
    wxString            m_fullPathFileName;
    GCParseVec *        m_parses;   // we own this

    GCFile();       // undefined
    GCFile( GCDataStore & dataStoreRef, wxString name);

  protected:
    void SetParses(GCParseVec * parses);
    static std::map<wxString,wxString> s_fileNameMap;

  public:
    virtual ~GCFile();

    wxString            GetDataTypeString()     const;
    wxString            GetFormatString()       const;
    wxString            GetInterleavingString() const;
    wxString            GetName()               const;
    bool                GetNeedsSettings()      const;
    wxString            GetShortName()          const;
    const GCParse &     GetParse(size_t choice) const;
    const GCParse &     GetParse(GCFileFormat,gcGeneralDataType,GCInterleaving) const;
    size_t              GetParseCount()         const;

    void DebugDump(wxString prefix=wxEmptyString) const;

    wxArrayString   PossibleSettings() const;

    gcIdSet         IdsOfAllParses() const;
    gcIdSet         IdsOfAllBlocks() const;

    GCFileFormat        GetFormat()             const;
    gcGeneralDataType   GetGeneralDataType()    const;
    GCInterleaving      GetInterleaving()       const;
};

struct GCFileCompare
{
    bool operator()(const GCFile*,const GCFile*);
};

typedef std::set<GCFile*,GCFileCompare>         dataFileSet;
typedef std::set<const GCFile*,GCFileCompare>   constDataFileSet;

#endif  // GC_FILE_H

//____________________________________________________________________________________
