// $Id: gc_creation_info.h,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_CREATION_INFO_H
#define GC_CREATION_INFO_H

#include "wx/string.h"

enum gcCreationType
{
    created_UNKNOWN,
    created_CMDFILE,
    created_DATAFILE,
    created_GUI
};

class gcCreationInfo
{
  private:

  protected:
    gcCreationType      m_creationType;

    bool                m_hasLineNumber;
    size_t              m_lineNumber;

    // EWFIX.P3.BUG.766 -- consider refering to actual file object
    bool                m_hasFileName;
    wxString            m_fileName;

    void    SetLineNumber(size_t lineNumber);
    void    SetFileName(wxString fileName);

  public:
    gcCreationInfo();
    ~gcCreationInfo();

    bool        HasLineNumber() const;
    size_t      GetLineNumber() const;

    bool        HasFileName() const;
    wxString    GetFileName() const;

    wxString    GetDescriptiveName() const;

    static gcCreationInfo MakeGuiCreationInfo();
    static gcCreationInfo MakeDataFileCreationInfo(size_t lineNumber,wxString fileName);
    static gcCreationInfo MakeCmdFileCreationInfo(size_t lineNumber,wxString fileName);

};

#endif  // GC_CREATION_INFO_H

//____________________________________________________________________________________
