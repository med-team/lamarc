// $Id: gc_parent.h,v 1.4 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARENT_H
#define GC_PARENT_H

#include "wx/string.h"
#include "gc_creation_info.h"
#include "gc_phase.h"
#include "gc_quantum.h"
#include "gc_types.h"

class GCStructures;

class gcParent : public GCQuantum
{
    friend class GCStructures;

  private:
    bool                        m_blessed;
    int                         m_level;      // for lam_conv display layout
    int                         m_index;      // for lam_conv display layout
    int                         m_length;     // for lam_conv display layout
    bool                        m_hasChild1;
    size_t                      m_child1Id;
    bool                        m_hasChild2;
    size_t                      m_child2Id;
    bool                        m_hasParent;
    size_t                      m_parentId;

    gcCreationInfo              m_creationInfo;  //JRM this doing anything?

    void   SetCreationInfo(const gcCreationInfo &);

  public:

    gcParent();
    ~gcParent();

    void        SetBlessed(bool blessed);
    bool        GetBlessed()            const;

    int         GetDispLevel()          const;
    void        SetDispLevel(int level);
    int         GetDispIndex()          const;
    void        SetDispIndex(int index);
    int         GetDispLength()         const;
    void        SetDispLength(int length);

    void        SetChild1Id(size_t id);
    void        ClearChild1Id();
    bool        HasChild1()             const;
    size_t      GetChild1Id()           const;
    size_t      GetChild1Id();
    wxString    GetChild1IdString()     const;

    void        SetChild2Id(size_t id);
    void        ClearChild2Id();
    bool        HasChild2()             const;
    size_t      GetChild2Id()           const;
    size_t      GetChild2Id();
    wxString    GetChild2IdString()     const;

    void        SetParentId(size_t id);
    void        ClearParentId();
    bool        HasParent()             const;
    size_t      GetParentId()           const;
    size_t      GetParentId();
    wxString    GetParentIdString()     const;

    void        DebugDump(wxString prefix=wxEmptyString) const;

};

#endif  // GC_PARENT_H

//____________________________________________________________________________________
