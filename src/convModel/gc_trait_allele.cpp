// $Id: gc_trait_allele.cpp,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_trait_allele.h"
#include "gc_trait_err.h"

//------------------------------------------------------------------------------------

gcTraitAllele::gcTraitAllele()
    :
    m_hasTraitId(false),
    m_traitId(gcdefault::badIndex)
{
}

gcTraitAllele::~gcTraitAllele()
{
}

bool
gcTraitAllele::HasTraitId() const
{
    return m_hasTraitId;
}

size_t
gcTraitAllele::GetTraitId() const
{
    assert(HasTraitId());
    return m_traitId;
}

void
gcTraitAllele::SetTraitId(size_t traitId)
{
    m_hasTraitId = true;
    m_traitId = traitId;
}

void
gcTraitAllele::UnsetTraitId()
{
    m_hasTraitId = false;
}

void
gcTraitAllele::SetName(wxString alleleName)
{
    if(alleleName.Find(' ') != wxNOT_FOUND)
    {
        throw gc_trait_allele_name_spaces(alleleName);
    }
    GCQuantum::SetName(alleleName);
}

//____________________________________________________________________________________
