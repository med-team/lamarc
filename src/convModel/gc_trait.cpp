// $Id: gc_trait.cpp,v 1.13 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_genotype_resolution.h"
#include "gc_phenotype.h"
#include "gc_strings_trait.h"
#include "gc_trait.h"
#include "gc_trait_allele.h"
#include "gc_trait_err.h"
#include "wx/log.h"

//------------------------------------------------------------------------------------

gcTraitInfo::gcTraitInfo()
    :
    m_hasRegion(false),
    m_regionId(gcdefault::badIndex)
{
}

gcTraitInfo::~gcTraitInfo()
{
}

void
gcTraitInfo::AddAllele(const gcTraitAllele& allele)
{
    if(m_alleleIds.find(allele.GetId()) != m_alleleIds.end())
    {
        throw gc_trait_allele_name_reuse(allele.GetName());
    }
    m_alleleIds.insert(allele.GetId());
}

void
gcTraitInfo::RemoveAllele(const gcTraitAllele& allele)
{
    gcIdSet::iterator iter = m_alleleIds.find(allele.GetId());
    assert(iter != m_alleleIds.end());
    m_alleleIds.erase(iter);
}

bool
gcTraitInfo::HasAlleleId(size_t alleleId) const
{
    return (!(m_alleleIds.find(alleleId) == m_alleleIds.end()));
}

void
gcTraitInfo::AddPhenotype(const gcPhenotype& pheno)
{
    if(m_phenotypeIds.find(pheno.GetId()) != m_phenotypeIds.end())
    {
        throw gc_trait_phenotype_name_reuse(pheno.GetName(),GetName());
    }
    m_phenotypeIds.insert(pheno.GetId());
}

void
gcTraitInfo::RemovePhenotype(const gcPhenotype& pheno)
{
    gcIdSet::iterator iter = m_phenotypeIds.find(pheno.GetId());
    assert(iter != m_phenotypeIds.end());
    m_phenotypeIds.erase(iter);
}

bool
gcTraitInfo::HasPhenotype(const gcPhenotype & phenotype) const
{
    return (!(m_phenotypeIds.find(phenotype.GetId()) == m_phenotypeIds.end()));
}

const gcIdSet &
gcTraitInfo::GetAlleleIds() const
{
    return m_alleleIds;
}

const gcIdSet &
gcTraitInfo::GetPhenotypeIds() const
{
    return m_phenotypeIds;
}

size_t
gcTraitInfo::GetRegionId() const
{
    return m_regionId;
}

bool
gcTraitInfo::HasRegionId() const
{
    return m_hasRegion;
}

void
gcTraitInfo::SetRegionId(size_t id)
{
    m_hasRegion = true;
    m_regionId = id;
}

void
gcTraitInfo::UnsetRegionId()
{
    m_hasRegion = false;
}

void
gcTraitInfo::DebugDump(wxString prefix) const
{
    wxLogDebug("%strait \"%s\" has alleles: %s, phenotypes: %s",    // EWDUMPOK
               prefix.c_str(),
               GetName().c_str(),
               m_alleleIds.AsString().c_str(),
               m_phenotypeIds.AsString().c_str());
}

//____________________________________________________________________________________
