// $Id: gc_phenotype.cpp,v 1.7 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_phenotype.h"
#include "gc_strings.h"
#include "gc_trait_err.h"
#include "wx/log.h"
#include "wx/string.h"

//------------------------------------------------------------------------------------

gcHapProbability::gcHapProbability()
    :   m_hasPenetrance(false),
        m_penetrance(gcdefault::penetrance)
{
}

gcHapProbability::~gcHapProbability()
{
}

bool
gcHapProbability::HasPenetrance() const
{
    return m_hasPenetrance;
}

double
gcHapProbability::GetPenetrance() const
{
    assert(HasPenetrance());
    return m_penetrance;
}

void
gcHapProbability::SetPenetrance(double penetrance)
{
    if(penetrance < 0)
    {
        throw gc_haplotype_probability_negative(penetrance);
    }
    m_hasPenetrance = true;
    m_penetrance = penetrance;
}

void
gcHapProbability::UnsetPenetrance()
{
    m_hasPenetrance = false;
}

void
gcHapProbability::AddAlleleId(size_t alleleId)
{
    m_alleleIds.push_back(alleleId);
}

const gcIdVec &
gcHapProbability::GetAlleleIds() const
{
    return m_alleleIds;
}

void
gcHapProbability::DebugDump(wxString prefix) const
{
    wxLogDebug("%shap probability %lf for alleles %s",   // EWDUMPOK
               prefix.c_str(),
               GetPenetrance(),
               GetAlleleIds().AsString().c_str());
}

//------------------------------------------------------------------------------------

gcPhenotype::gcPhenotype()
    :   m_hasTraitId(false),
        m_traitId(gcdefault::badIndex),
        m_hasExplicitName(false)
{
}

gcPhenotype::~gcPhenotype()
{
}

void
gcPhenotype::AddHapProbability(const gcHapProbability & hp)
{
    m_hapProbabilities.push_back(hp);
}

const std::vector<gcHapProbability> &
gcPhenotype::GetHapProbabilities() const
{
    return m_hapProbabilities;
}

bool
gcPhenotype::HasTraitId() const
{
    return m_hasTraitId;
}

size_t
gcPhenotype::GetTraitId() const
{
    assert(HasTraitId());
    return m_traitId;
}

void
gcPhenotype::SetTraitId(size_t traitId)
{
    m_hasTraitId = true;
    m_traitId = traitId;
}

bool
gcPhenotype::HasExplicitName() const
{
    return m_hasExplicitName;
}

void
gcPhenotype::SetHasExplicitName()
{
    m_hasExplicitName = true;
}

void
gcPhenotype::DebugDump(wxString prefix) const
{
    wxLogDebug("%sphenotype %s of trait %d",   // EWDUMPOK
               prefix.c_str(),
               GetName().c_str(),
               (int)GetTraitId());
    for(size_t i = 0; i < m_hapProbabilities.size(); i++)
    {
        const gcHapProbability & hp = m_hapProbabilities[i];
        hp.DebugDump(prefix+gcstr::indent);
    }
}

//____________________________________________________________________________________
