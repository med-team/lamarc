// $Id: gc_exportable.cpp,v 1.8 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_errhandling.h"
#include "gc_exportable.h"
#include "gc_individual.h"
#include "gc_population.h"
#include "gc_region.h"
#include "gc_strings.h"
#include "wx/log.h"
#include "wx/string.h"

gcPopRegionPair::gcPopRegionPair(const gcPopulation * p, const gcRegion * r)
    :
    std::pair<const gcPopulation *,const gcRegion *>(p,r)
{
}

gcPopRegionPair::~gcPopRegionPair()
{
}

void
gcPopRegionPair::DebugDump(wxString prefix) const
{
    wxString pname = (*first).GetName();
    wxString rname = (*second).GetName();
    wxLogDebug("%spop/region:(%s,%s)",prefix.c_str(),pname.c_str(),rname.c_str());  // EWDUMPOK
}

//------------------------------------------------------------------------------------

gcNameResolvedInfo::gcNameResolvedInfo(const gcPopulation & pop, const gcRegion & reg)
    :
    m_populationRef(pop),
    m_regionRef(reg)
{
}

gcNameResolvedInfo::~gcNameResolvedInfo()
{
    for(std::vector<GCIndividual*>::iterator i = m_individuals.begin();
        i != m_individuals.end(); i++)
    {
        delete *i;
    }
}

void
gcNameResolvedInfo::AddIndividual(GCIndividual * ind)
{
    m_individuals.push_back(ind);
}

const gcPopulation &
gcNameResolvedInfo::GetPopRef() const
{
    return m_populationRef;
}

const gcRegion &
gcNameResolvedInfo::GetRegionRef() const
{
    return m_regionRef;
}

std::vector<const GCIndividual*>
gcNameResolvedInfo::GetIndividuals() const
{
    std::vector<const GCIndividual*> inds;
    for(size_t index=0; index < m_individuals.size(); index++)
    {
        inds.push_back(m_individuals[index]);
    }
    return inds;
}

void
gcNameResolvedInfo::DebugDump(wxString prefix) const
{
    wxLogDebug("%svector of %d individuals",prefix.c_str(),(int)m_individuals.size());  // EWDUMPOK
}

//------------------------------------------------------------------------------------

gcExportable::gcExportable()
{
}

gcExportable::~gcExportable()
{
    for(iterator i=begin(); i != end(); i++)
    {
        delete (*i).second;
    }
}

const gcNameResolvedInfo &
gcExportable::GetInfo(const gcPopulation & pop, const gcRegion & region) const
{
    gcPopRegionPair pair(&pop,&region);
    const_iterator i = find(pair);
    if(i == end())
    {
        wxString popName = pop.GetName();
        wxString regionName = region.GetName();
        gui_error g(wxString::Format(gcerr::nameResolutionPairMissing,popName.c_str(),regionName.c_str()));
        throw g;
    }
    return *((*i).second);
}

void
gcExportable::DebugDump(wxString prefix) const
{
    wxLogDebug("%sgcExportable:",prefix.c_str());   // EWDUMPOK
    for(const_iterator i=begin(); i!=end(); i++)
    {
        gcPopRegionPair pair = (*i).first;
        const gcNameResolvedInfo * info = (*i).second;
        pair.DebugDump(wxString::Format("%s    ",prefix.c_str()));
        info->DebugDump(wxString::Format("%s    ",prefix.c_str()));
    }
}

//____________________________________________________________________________________
