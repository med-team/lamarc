// $Id: gc_phenotype.h,v 1.7 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PHENOTYPE_H
#define GC_PHENOTYPE_H

#include <vector>

#include "gc_quantum.h"
#include "gc_set_util.h"
#include "wx/string.h"

class GCStructures;

class gcHapProbability
{
  private:
    bool            m_hasPenetrance;
    double          m_penetrance;
    gcIdVec         m_alleleIds;

  public:
    gcHapProbability();
    virtual ~gcHapProbability();

    bool    HasPenetrance() const;
    double  GetPenetrance() const;
    void    SetPenetrance(double penetrance);
    void    UnsetPenetrance();

    void    AddAlleleId(size_t alleleId);
    const gcIdVec & GetAlleleIds() const;

    void    DebugDump(wxString prefix=wxEmptyString) const;
};

class gcPhenotype : public GCQuantum
{
    friend class GCStructures;

  private:
    bool                            m_hasTraitId;
    size_t                          m_traitId;
    bool                            m_hasExplicitName;

    std::vector<gcHapProbability>   m_hapProbabilities;

    void    SetTraitId(size_t traitId);
    void    UnsetTraitId();

  public:
    gcPhenotype();
    virtual ~gcPhenotype();

    void    AddHapProbability(const gcHapProbability &);

    bool    HasExplicitName() const;

    const std::vector<gcHapProbability> & GetHapProbabilities() const;
    bool    HasTraitId() const;
    size_t  GetTraitId() const;

    void    SetHasExplicitName();

    void    DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_PHENOTYPE_H

//____________________________________________________________________________________
