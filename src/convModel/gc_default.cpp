// $Id: gc_default.cpp,v 1.13 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_default.h"
#include "wx/string.h"

const long       gcdefault::badDisplayIndex = -999;
const size_t     gcdefault::badIndex        = 999;
const size_t     gcdefault::badLength       = 9999;
const long       gcdefault::badLocation     = -9999;
const long       gcdefault::badMapPosition  = -9999;
const wxString   gcdefault::createdIndividualPrefix = "createdIndividual";
const wxString   gcdefault::delimiter       = ".";
const wxString   gcdefault::emptyBlock      = "<empty>";
const wxString   gcdefault::locusName       = "<unnamed segment>";
const size_t     gcdefault::migrateSequenceNameLength   = 10;
const double     gcdefault::penetrance      = 0.0;
const size_t     gcdefault::numSites        = 0;
const wxString   gcdefault::popName         = "<unnamed population>";
const wxString   gcdefault::regionName      = "<unnamed region>";
const wxString   gcdefault::unnamedObject   = "<unnamed object>";

//____________________________________________________________________________________
