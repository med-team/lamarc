// $Id: gc_dictionary.h,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_DICTIONARY_H
#define GC_DICTIONARY_H

#include "wx/string.h"
#include <set>

class gcDictionary : private std::set<wxString>
{
  private:

  protected:
    // tells you what types of things are in this dictionary
    virtual const wxString &    infoString() const = 0;

  public:
    gcDictionary();
    virtual ~gcDictionary();

    bool        HasName     (const wxString & name) const;
    void        FreeName    (const wxString & name);
    void        ReserveName (const wxString & name);
    wxString    ReserveOrMakeName(  wxString useIfNotEmpty,
                                    wxString prefixIfCreatingName=wxEmptyString);
};

#endif  // GC_DICTIONARY_H

//____________________________________________________________________________________
