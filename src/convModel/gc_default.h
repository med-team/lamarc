// $Id: gc_default.h,v 1.13 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_DEFAULT_H
#define GC_DEFAULT_H

#include "gc_types.h"
#include "wx/string.h"

class gcdefault
{
  public:
    static const long       badDisplayIndex;
    static const size_t     badIndex;
    static const size_t     badLength;
    static const long       badLocation;
    static const long       badMapPosition;
    static const wxString   createdIndividualPrefix;
    static const wxString   delimiter;
    static const wxString   emptyBlock;
    static const wxString   locusName;
    static const double     penetrance;
    static const size_t     migrateSequenceNameLength;
    static const size_t     numSites;
    static const wxString   popName;
    static const wxString   regionName;
    static const wxString   unnamedObject;
};

#endif  // GC_DEFAULT_H

//____________________________________________________________________________________
