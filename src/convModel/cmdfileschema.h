// $Id: cmdfileschema.h,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CMDFILESCHEMA_H
#define CMDFILESCHEMA_H

#include "parsetreeschema.h"
#include "wx/string.h"

class CmdFileSchema : public ParseTreeSchema
// specialization of ParseTreeSchema to apply to command
// files used for batch version of converter
{
  private:

  protected:
    // these methods allow us to pass wxStrings in as
    // arguments instead of std::string
    void AddTag(wxString tag);
    void AddAttribute(bool required,
                      wxString tagName,
                      wxString attrName);
    void AddSubtag(bool required, bool onlyOne,
                   wxString parentTag,
                   wxString childTag);
  public:
    CmdFileSchema();            // modify this method to update schema
    virtual ~CmdFileSchema();
};

#endif // CMDFILESCHEMA_H

//____________________________________________________________________________________
