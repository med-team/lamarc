// $Id: gc_structure_maps.cpp,v 1.20 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_data.h"
#include "gc_default.h"
#include "gc_region.h"
#include "gc_population.h"
#include "gc_locus.h"
#include "gc_panel.h"
#include "gc_strings.h"
#include "gc_structure_maps.h"
#include "gc_trait.h"
#include "wx/log.h"
#include "wx/string.h"

//------------------------------------------------------------------------------------

gcDisplayOrder::gcDisplayOrder()
{
}

gcDisplayOrder::~gcDisplayOrder()
{
}

wxString
gcDisplayOrder::AsString() const
{
    wxString ids="";
    for(const_iterator i=begin(); i != end(); i++)
    {
        size_t blockId = *i;
        ids += wxString::Format("%d ",(int)blockId);
    }
    return ids;
}

void
gcDisplayOrder::DebugDump(wxString prefix) const
{
    wxLogDebug("%s%s", prefix.c_str(), AsString().c_str()); // EWDUMPOK
}

//------------------------------------------------------------------------------------

gcPopLocusIdPair::gcPopLocusIdPair()
{
}

gcPopLocusIdPair::gcPopLocusIdPair(size_t popId, size_t locId)
    : std::pair<size_t,size_t>(popId,locId)
{
}

gcPopLocusIdPair::~gcPopLocusIdPair()
{
}

void
gcPopLocusIdPair::DebugDump(wxString prefix) const
{
    wxLogDebug("%s<%d,%d>", // EWDUMPOK
               prefix.c_str(),
               (int)first,
               (int)second);
}

//------------------------------------------------------------------------------------

gcBlockSetMap::gcBlockSetMap()
{
}

gcBlockSetMap::~gcBlockSetMap()
{
}

void
gcBlockSetMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%sblock from:",prefix.c_str()); // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        gcPopLocusIdPair idP = (*i).first;
        gcIdSet set = (*i).second;
        wxLogDebug("%s<%d,%d> -> %s",   // EWDUMPOK
                   (prefix+gcstr::indent).c_str(),
                   (int)(idP.first),
                   (int)(idP.second),
                   set.AsString().c_str());
    }
}

//------------------------------------------------------------------------------------

gcRegionMap::gcRegionMap()
{
}

gcRegionMap::~gcRegionMap()
{
}

void
gcRegionMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%sregions:",prefix.c_str()); // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcRegion & region = (*i).second;
        region.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcLocusMap::gcLocusMap()
{
}

gcLocusMap::~gcLocusMap()
{
}

void
gcLocusMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%ssegments:",prefix.c_str());   // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcLocus & locus = (*i).second;
        locus.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------


gcRegionPopIdPair::gcRegionPopIdPair()
{
}

gcRegionPopIdPair::gcRegionPopIdPair(size_t regionId, size_t popId)
    : std::pair<size_t,size_t>(regionId,popId)
{
}

gcRegionPopIdPair::~gcRegionPopIdPair()
{
}

void
gcRegionPopIdPair::DebugDump(wxString prefix) const
{
    wxLogDebug("%s<%d,%d>", // JMDUMPOK
               prefix.c_str(),
               (int)first,
               (int)second);
}

//------------------------------------------------------------------------------------

gcPanelMap::gcPanelMap()
{
}

gcPanelMap::~gcPanelMap()
{
}

void
gcPanelMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%ssegments:",prefix.c_str());   // JMDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcPanel & panel = (*i).second;
        panel.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcParentMap::gcParentMap()
{
}

gcParentMap::~gcParentMap()
{
}

void
gcParentMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%ssegments:",prefix.c_str());   // JMDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcParent & parent = (*i).second;
        parent.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcPopMap::gcPopMap()
{
}

gcPopMap::~gcPopMap()
{
}

void
gcPopMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%spopulations:",prefix.c_str());    // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcPopulation & pop = (*i).second;
        pop.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcPopMultiMap::gcPopMultiMap()
{
}

gcPopMultiMap::~gcPopMultiMap()
{
}

void
gcPopMultiMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%spop correspondence:",prefix.c_str()); // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        wxLogDebug("%spop %d file %d parsePop %d",  // EWDUMPOK
                   (prefix+gcstr::indent).c_str(),(int)((*i).first),(int)((*i).second.first),(int)((*i).second.second));
    }
}

//------------------------------------------------------------------------------------

gcTraitMap::gcTraitMap()
{
}

gcTraitMap::~gcTraitMap()
{
}

void
gcTraitMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%strait classes:",prefix.c_str());  // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcTraitInfo & trait = (*i).second;
        trait.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcAlleleMap::gcAlleleMap()
{
}

gcAlleleMap::~gcAlleleMap()
{
}

void
gcAlleleMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%salleles:",prefix.c_str());  // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcTraitAllele & allele = (*i).second;
        allele.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcPhenoMap::gcPhenoMap()
{
}

gcPhenoMap::~gcPhenoMap()
{
}

void
gcPhenoMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%sphenotypes:",prefix.c_str());  // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcPhenotype & pheno = (*i).second;
        pheno.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

gcStringMap::gcStringMap()
{
}

gcStringMap::~gcStringMap()
{
}

void
gcStringMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%sstring map:",prefix.c_str()); // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        wxLogDebug("%s%d:%s",   // EWDUMPOK
                   (prefix+gcstr::indent).c_str(),
                   (int)((*i).first),
                   (*i).second.c_str());
    }
}
//------------------------------------------------------------------------------------


gcMigrationPair::gcMigrationPair()
{
}

gcMigrationPair::gcMigrationPair(size_t toId, size_t fromId)
    : std::pair<size_t,size_t>(toId,fromId)
{
}

gcMigrationPair::~gcMigrationPair()
{
}

void
gcMigrationPair::DebugDump(wxString prefix) const
{
    wxLogDebug("%s<%d,%d>", // JMDUMPOK
               prefix.c_str(),
               (int)first,
               (int)second);
}

//------------------------------------------------------------------------------------

gcMigrationMap::gcMigrationMap()
{
}

gcMigrationMap::~gcMigrationMap()
{
}

void
gcMigrationMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%ssegments:",prefix.c_str());   // JMDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        const gcMigration & mig = (*i).second;
        mig.DebugDump(prefix+gcstr::indent);
    }
}

//------------------------------------------------------------------------------------

constBlockVector::constBlockVector()
{
}

constBlockVector::~constBlockVector()
{
}

//------------------------------------------------------------------------------------

popVector::popVector()
{
}

popVector::~popVector()
{
}

//------------------------------------------------------------------------------------

locVector::locVector()
{
}

locVector::~locVector()
{
}

//____________________________________________________________________________________
