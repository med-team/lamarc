// $Id: gc_mapfile.h,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_MAPFILE_H
#define GC_MAPFILE_H

#if 0  // Potentially DEAD CODE (bobgian, Feb 2010)

#include "wx/string.h"

class GCLocus;
class GCLinkageGroup;
class GCSequentialLocus;
class TiXmlElement;
class XmlParser;

class GCMapFile
{
  private:
    static size_t       s_mapFileCount;

    const size_t        m_mapFileId;
    wxString            m_mapFileName;

    XmlParser *         m_xmlParserPointer;

    linkageGroupSet     m_linkageGroups;

    GCMapFile();        // undefined

  protected:
    GCLinkageGroup &    AddLinkageGroup(size_t numHaps);

    //long                GetOffsetFromTiXml(TiXmlElement * blockElement);
    //size_t              GetNumSitesFromTiXml(TiXmlElement * blockElement);
    //wxString            GetLocationsFromTiXml(TiXmlElement * blockElement);
    //GCDataType          GetDataTypeFromTiXml(TiXmlElement * blockElement);
    //long                GetMapPositionFromTiXml(TiXmlElement * blockElement);

    GCSequentialLocus & BuildLocus(TiXmlElement * blockElement);

  public:
    GCMapFile(wxString mapFileName);
    ~GCMapFile();

    size_t      GetId() const;
    wxString    GetShortName() const;
    wxString    GetName() const    {return m_mapFileName;};

    void        DebugDump(wxString prefix=wxEmptyString) const;

    static long     ExtractLongValue(TiXmlElement * element, std::string tag, bool required);
};

#endif

#endif  // GC_MAPFILE_H

//____________________________________________________________________________________
