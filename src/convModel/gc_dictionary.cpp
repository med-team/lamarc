// $Id: gc_dictionary.cpp,v 1.4 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_dictionary.h"
#include "gc_strings.h" // EWFIX.P4 -- move to own file ?
#include "gc_structures_err.h"

//------------------------------------------------------------------------------------

gcDictionary::gcDictionary()
{
}

gcDictionary::~gcDictionary()
{
}

bool
gcDictionary::HasName(const wxString & name) const
{
    return (! (find(name) == end()) );
}

void
gcDictionary::ReserveName(const wxString & name)
{
    if(!name.IsEmpty())
    {
        if(HasName(name))
        {
            duplicate_name_error e(name,infoString());
            throw e;
        }
        insert(name);
    }
    else
    {
        empty_name_error e(infoString());
        throw e;
    }
}

void
gcDictionary::FreeName(const wxString & name)
{
    if(!name.IsEmpty())
    {
        iterator nameIter = find(name);
        if(nameIter == end())
        {
            missing_name_error e(name,infoString());
            throw e;
        }
        erase(nameIter);
    }
    else
    {
        empty_name_error e(infoString());
        throw e;
    }
}

wxString
gcDictionary::ReserveOrMakeName(wxString name,
                                wxString prefixToUseIfNameEmpty)
{
    if(!name.IsEmpty())
    {
        ReserveName(name);
        return name;
    }
    wxString prefix = prefixToUseIfNameEmpty;
    if(prefix.IsEmpty())
    {
        prefix = gcstr::object;
    }
    for(int i=0; true; i++)
    {
        wxString nameCandidate = wxString::Format(gcstr::nameCandidate,prefix.c_str(),i);
        try
        {
            ReserveName(nameCandidate);
            return nameCandidate;
        }
        catch(duplicate_name_error& e)
        {
            // do nothing -- we just keep trying
        }
        catch(empty_name_error& f)
        {
            // do nothing -- we just keep trying
        }
    }
    assert(false);
    return gcstr::badName;
}

//____________________________________________________________________________________
