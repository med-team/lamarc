// $Id: gc_file_info.cpp,v 1.6 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_file_info.h"
#include "gc_strings.h"
#include "wx/log.h"
#include "wx/string.h"

//------------------------------------------------------------------------------------

gcFileInfo::gcFileInfo()
    :
    m_selected(false),
    m_hasParse(false),
    m_parseId(gcdefault::badIndex),
    m_hasAdjacentHapAssignment(false),
    m_adjacentHapAssignment(gcdefault::badIndex),
    m_hasGenoFile(false),
    m_genoFileId(gcdefault::badIndex)
{
}

gcFileInfo::~gcFileInfo()
{
}

bool
gcFileInfo::GetSelected() const
{
    return m_selected;
}

void
gcFileInfo::SetSelected(bool selected)
{
    m_selected = selected;
}

//------------------------------------------------------------------------------------

bool
gcFileInfo::HasParse() const
{
    return m_hasParse;
}

size_t
gcFileInfo::GetParse() const
{
#if 0
    assert(HasParse());
#endif

    if(!HasParse())
    {
        assert(false);
    }

    return m_parseId;
}

void
gcFileInfo::SetParse(size_t parseId)
{
    m_hasParse = true;
    m_parseId = parseId;
}

void
gcFileInfo::UnsetParse()
{
    m_hasParse = false;
}

//------------------------------------------------------------------------------------

bool
gcFileInfo::HasAdjacentHapAssignment() const
{
    return m_hasAdjacentHapAssignment;
}

size_t
gcFileInfo::GetAdjacentHapAssignment() const
{
    assert(HasAdjacentHapAssignment());
    return  m_adjacentHapAssignment;
}

void
gcFileInfo::SetAdjacentHapAssignment(size_t numAdj)
{
    m_hasAdjacentHapAssignment = true;
    m_adjacentHapAssignment = numAdj;
}

void
gcFileInfo::UnsetAdjacentHapAssignment()
{
    m_hasAdjacentHapAssignment = false;
    m_adjacentHapAssignment = gcdefault::badIndex;
}

//------------------------------------------------------------------------------------

void
gcFileInfo::DebugDump(wxString prefix) const
{
    if(m_selected)
    {
        wxLogDebug("%sselected",prefix.c_str());   // EWDUMPOK
    }
    else
    {
        wxLogDebug("%sun selected",prefix.c_str());// EWDUMPOK
    }

    if(m_hasParse)
    {
        wxLogDebug("%sparse id %d",                 // EWDUMPOK
                   prefix.c_str(),
                   (int)m_parseId);
    }
    else
    {
        wxLogDebug("%sno parse",                   // EWDUMPOK
                   prefix.c_str());
    }

    if(HasAdjacentHapAssignment())
    {
        wxLogDebug("%sadjacent haps: %d",                   // EWDUMPOK
                   prefix.c_str(),
                   (int)GetAdjacentHapAssignment());
    }
}

//------------------------------------------------------------------------------------

bool
gcFileInfo::HasGenoFile() const
{
    return m_hasGenoFile;
}

void
gcFileInfo::UnsetGenoFile()
{
    m_hasGenoFile = false;
}

//------------------------------------------------------------------------------------

const GCPopMatcher &
gcFileInfo::GetPopMatcher() const
{
    return m_popMatch;
}

void
gcFileInfo::SetPopMatcher(const GCPopMatcher & p)
{
    m_popMatch = p;
}

//------------------------------------------------------------------------------------

const GCLocusMatcher &
gcFileInfo::GetLocMatcher() const
{
    return m_locMatch;
}

void
gcFileInfo::SetLocMatcher(const GCLocusMatcher & p)
{
    m_locMatch = p;
}

//------------------------------------------------------------------------------------

gcFileMap::gcFileMap()
{
}

gcFileMap::~gcFileMap()
{
}

void
gcFileMap::DebugDump(wxString prefix) const
{
    wxLogDebug("%sfile map:",prefix.c_str()); // EWDUMPOK
    for(const_iterator i=begin(); i != end(); i++)
    {
        wxLogDebug("%s%sfor id %d",             // EWDUMPOK
                   prefix.c_str(),
                   gcstr::indent.c_str(),
                   (int)((*i).first));
        const gcFileInfo & info = (*i).second;
        info.DebugDump(prefix+gcstr::indent+gcstr::indent);
    }
}

//____________________________________________________________________________________
