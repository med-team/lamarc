// $Id: gc_structure_maps.h,v 1.16 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRUCTURE_MAPS_H
#define GC_STRUCTURE_MAPS_H

#include <list>
#include <map>
#include <set>

#include "gc_locus.h"
#include "gc_migration.h"
#include "gc_panel.h"
#include "gc_parent.h"
#include "gc_population.h"
#include "gc_quantum.h"
#include "gc_region.h"
#include "gc_parent.h"
#include "gc_phenotype.h"
#include "gc_set_util.h"
#include "gc_trait.h"
#include "gc_trait_allele.h"

#include "gc_loci_match.h"
#include "gc_pop_match.h"

#include "wx/string.h"

class GCParseBlock;

class gcDisplayOrder :  public std::list<size_t>
{
  public:
    gcDisplayOrder() ;
    ~gcDisplayOrder() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
    wxString AsString() const;
};

class gcPopLocusIdPair : public std::pair<size_t, size_t>
{
  public:
    gcPopLocusIdPair() ;
    gcPopLocusIdPair(size_t i,size_t j);
    ~gcPopLocusIdPair() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcBlockSetMap : public std::map<gcPopLocusIdPair,gcIdSet>
{
  public:
    gcBlockSetMap() ;
    ~gcBlockSetMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcRegionMap : public std::map<size_t,gcRegion>
{
  public:
    gcRegionMap() ;
    virtual ~gcRegionMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcPopMap : public std::map<size_t,gcPopulation>
{
  public:
    gcPopMap() ;
    virtual ~gcPopMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcPopMultiMap : public std::multimap<size_t, std::pair< size_t, size_t> >
{
  public:
    gcPopMultiMap() ;
    virtual ~gcPopMultiMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcLocusMap : public std::map<size_t,gcLocus>
{
  public:
    gcLocusMap() ;
    virtual ~gcLocusMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};


class gcRegionPopIdPair : public std::pair<size_t, size_t>
{
  public:
    gcRegionPopIdPair() ;
    gcRegionPopIdPair(size_t i,size_t j);
    ~gcRegionPopIdPair() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcPanelMap : public std::map<gcRegionPopIdPair,gcPanel>
{
  public:
    gcPanelMap() ;
    virtual ~gcPanelMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcParentMap : public std::map<size_t,gcParent>
{
  public:
    gcParentMap() ;
    virtual ~gcParentMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcTraitMap : public std::map<size_t,gcTraitInfo>
{
  public:
    gcTraitMap() ;
    virtual ~gcTraitMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcAlleleMap : public std::map<size_t,gcTraitAllele>
{
  public:
    gcAlleleMap() ;
    virtual ~gcAlleleMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcPhenoMap : public std::map<size_t,gcPhenotype>
{
  public:
    gcPhenoMap() ;
    virtual ~gcPhenoMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcStringMap : public std::map<size_t,wxString>
{
  public:
    gcStringMap() ;
    virtual ~gcStringMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcMigrationPair : public std::pair<size_t, size_t>
{
  public:
    gcMigrationPair() ;
    gcMigrationPair(size_t i,size_t j);
    virtual ~gcMigrationPair() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcMigrationMap : public std::map<gcMigrationPair,gcMigration>
{
  public:
    gcMigrationMap() ;
    virtual ~gcMigrationMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

typedef std::vector<GCQuantum *>        objVector;
typedef std::vector<const GCQuantum *>  constObjVector;

class constBlockVector : public std::vector<const GCParseBlock *>
{
  public:
    constBlockVector() ;
    virtual ~constBlockVector() ;
    //void DebugDump(wxString prefix=wxEmptyString) const;
};

class popVector : public std::vector<gcPopulation *>
{
  public:
    popVector() ;
    virtual ~popVector() ;
};

class locVector : public std::vector<gcLocus *>
{
  public:
    locVector() ;
    virtual ~locVector() ;
};

#endif  // GC_STRUCTURE_MAPS_H

//____________________________________________________________________________________
