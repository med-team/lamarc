// $Id: gc_panel.cpp,v 1.3 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_creation_info.h"           // JRM this doing anything?
#include "gc_errhandling.h"
#include "gc_data.h"                    // for ToWxString
#include "gc_default.h"
#include "gc_panel.h"
#include "gc_strings.h"
#include "gc_structures_err.h"
#include "wx/log.h"
#include "wx/string.h"
#include "wx/tokenzr.h"

//------------------------------------------------------------------------------------

gcPanel::gcPanel()
    :
    m_blessed(false),
    m_hasRegion(false),
    m_regionId(gcdefault::badIndex),
    m_hasPop(false),
    m_popId(gcdefault::badIndex),
    m_nPanels(0)
{
}

gcPanel::~gcPanel()
{
}

void
gcPanel::SetBlessed(bool blessed)
{
    m_blessed = blessed;
}

bool
gcPanel::GetBlessed() const
{
    return m_blessed;
}

void
gcPanel::DebugDump(wxString prefix) const
{
#if 0
    wxLogDebug("%spanel %s (panel id %ld)", // JMDUMPOK
               prefix.c_str(),
               GetName().c_str(),
               (long)GetId());
    wxLogDebug("%sregion %s",(prefix+gcstr::indent).c_str(),ToWxString(GetRegionID()).c_str());  // JMDUMPOK
    wxLogDebug("%s%s population",(prefix+gcstr::indent).c_str(),GetPopulation().c_str());  // JMDUMPOK
    wxLogDebug("%s%s number of panels",(prefix+gcstr::indent).c_str(),GetNumPanels.c_str());   // JMDUMPOK
#endif

    wxLogVerbose("%spanel %s (panel id %ld)", // JMDUMPOK
                 prefix.c_str(),
                 GetName().c_str(),
                 (long)GetId());
    wxLogVerbose("%sregion %s",(prefix+gcstr::indent).c_str(),GetRegionIdString().c_str());  // JMDUMPOK
    wxLogVerbose("%s%s population",(prefix+gcstr::indent).c_str(),GetPopIdString().c_str());  // JMDUMPOK
    wxLogVerbose("%s%s number of panels",(prefix+gcstr::indent).c_str(),GetNumPanelsString().c_str());   // JMDUMPOK
}

void
gcPanel::SetRegionId(size_t id)
{
    m_regionId  = id;
    m_hasRegion = true;
}

void
gcPanel::UnsetRegionId()
{
    m_regionId  = gcdefault::badIndex;
    m_hasRegion = false;
}

bool
gcPanel::HasRegion() const
{
    return m_hasRegion;
}

size_t
gcPanel::GetRegionId() const
{
    if(!HasRegion())
    {
        wxString msg = wxString::Format(gcerr::unsetRegionId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_regionId;
}

wxString
gcPanel::GetRegionIdString() const
{
    if(HasRegion())
    {
        return wxString::Format("%d",(int)GetRegionId());
    }
    return gcstr::unknown;
}

void
gcPanel::SetPopId(size_t id)
{
    m_popId  = id;
    m_hasPop = true;
}

void
gcPanel::UnsetPopId()
{
    m_popId  = gcdefault::badIndex;
    m_hasPop = false;
}

bool
gcPanel::HasPop() const
{
    return m_hasPop;
}

size_t
gcPanel::GetPopId() const
{
    if(!HasPop())
    {
        wxString msg = wxString::Format(gcerr::unsetPopId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_popId;
}

wxString
gcPanel::GetPopIdString() const
{
    if(HasPop())
    {
        return wxString::Format("%d",(int)GetPopId());
    }
    return gcstr::unknown;
}

void
gcPanel::SetNumPanels(long val)
{
    m_nPanels = val;
}

long
gcPanel::GetNumPanels() const
{
    return m_nPanels;
}

wxString
gcPanel::GetNumPanelsString() const
{
    return wxString::Format("%d",(int)GetNumPanels());
}
//____________________________________________________________________________________
