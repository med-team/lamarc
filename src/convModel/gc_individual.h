// $Id: gc_individual.h,v 1.14 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_INDIVIDUAL_H
#define GC_INDIVIDUAL_H

#include <map>
#include <vector>

#include "gc_phase.h"
#include "gc_phenotype.h"
#include "gc_set_util.h"
#include "wx/string.h"

class gcLocus;
class gcPhenotype;
class gcRegion;
class GCSequentialData;

class gcSample
{
  private:
    wxString                                            m_label;
    std::map<const gcLocus*, const GCSequentialData *>  m_data;

  protected:

  public:
    gcSample(wxString label);
    virtual ~gcSample();

    void        AddLocusData(const gcLocus *, const GCSequentialData *);

    const GCSequentialData &    GetData(const gcLocus *)    const;
    wxString                    GetLabel()                  const;
};

class GCIndividual
{
  private:
    wxString                                            m_name;
    const gcRegion &                                    m_regionRef;
    std::vector<gcSample*>                              m_samples;
    std::map<const gcLocus*, gcUnphasedMarkers>         m_phaseInfo;

    gcIdSet                                             m_phenotypeIds;

    GCIndividual();                 // undefined

  protected:
    gcSample *                      GetSample(wxString label);

  public:
    GCIndividual(wxString name, const gcRegion &);
    ~GCIndividual();

    void    AddPhase(const gcLocus &, const gcUnphasedMarkers &);
    void    AddPhenotype(const gcPhenotype&);
    void    AddSample(wxString label, const gcLocus&,const GCSequentialData *);

    const gcIdSet &             GetPhenotypeIds()               const;
    wxString                    GetName()                       const;
    size_t                      GetNumSamples()                 const;
    const gcRegion &            GetRegion()                     const;
    const gcSample *            GetSample(size_t hapIndex)      const;
    const gcUnphasedMarkers  *  GetUnphased(const gcLocus &)    const;

    void    DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_INDIVIDUAL_H

//____________________________________________________________________________________
