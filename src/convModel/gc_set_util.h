// $Id: gc_set_util.h,v 1.6 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_SET_UTIL_H
#define GC_SET_UTIL_H

#include <set>
#include <vector>

#include "wx/string.h"

class gcIdSet : public std::set<size_t>
{
  public:
    gcIdSet() ;
    ~gcIdSet() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
    wxString AsString() const;
};

class gcIdVec : public std::vector<size_t>
{
  public:
    gcIdVec() ;
    ~gcIdVec() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
    wxString AsString() const;
};

#endif  // GC_SET_UTIL_H

//____________________________________________________________________________________
