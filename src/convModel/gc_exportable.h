// $Id: gc_exportable.h,v 1.8 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_EXPORTABLE_H
#define GC_EXPORTABLE_H

#include <map>
#include <vector>

class GCIndividual;
class gcPopulation;
class gcRegion;

class gcPopRegionPair : public std::pair<const gcPopulation *, const gcRegion *>
{
  public:
    gcPopRegionPair(const gcPopulation *, const gcRegion *);
    virtual ~gcPopRegionPair();
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcNameResolvedInfo
{
  private:
    const gcPopulation &        m_populationRef;
    const gcRegion &            m_regionRef;
    std::vector<GCIndividual*>  m_individuals;

  public:
    gcNameResolvedInfo(const gcPopulation &, const gcRegion &);
    virtual ~gcNameResolvedInfo();

    void                                AddIndividual(GCIndividual*);
    const gcPopulation &                GetPopRef() const;
    const gcRegion &                    GetRegionRef() const;
    std::vector<const GCIndividual*>    GetIndividuals() const;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class gcExportable : public std::map<gcPopRegionPair,gcNameResolvedInfo *>
{
  public:
    gcExportable();
    ~gcExportable();
    const gcNameResolvedInfo &  GetInfo(const gcPopulation &, const gcRegion &) const;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_EXPORTABLE_H

//____________________________________________________________________________________
