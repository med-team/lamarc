// $Id: gc_types.cpp,v 1.10 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_types.h"

gcGeneralDataType::gcGeneralDataType()
    : std::set<gcSpecificDataType> ()
{
}

gcGeneralDataType::~gcGeneralDataType()
{
}

bool
gcGeneralDataType::HasAllelic() const
{
    if(find(sdatatype_MICROSAT) != end()) return true;
    if(find(sdatatype_KALLELE) != end()) return true;
    return false;
}

bool
gcGeneralDataType::HasNucleic() const
{
    if(find(sdatatype_DNA) != end()) return true;
    if(find(sdatatype_SNP) != end()) return true;
    return false;
}

bool
gcGeneralDataType:: CompatibleWith(const gcGeneralDataType& dtype) const
{

    for(const_iterator i = begin(); i != end() ; i++ )
    {
        gcSpecificDataType s1 = *i;
        for(const_iterator j = dtype.begin(); j != dtype.end() ; j++ )
        {
            gcSpecificDataType s2 = *j;

            if(s1 == s2) return true;

        }
    }

    return false;
}

void
gcGeneralDataType::Intersect(const gcGeneralDataType & dtype)
{
    gcGeneralDataType::iterator i = begin();
    while(i != end())
    {
        if(dtype.find(*i) == dtype.end())
        {
            erase(i++);
        }
        else
        {
            i++;
        }
    }
}

void
gcGeneralDataType::Disallow(gcSpecificDataType stype)
{
    gcGeneralDataType::iterator i = find(stype);
    if(i != end())
    {
        erase(i);
    }
}

void
gcGeneralDataType::Union(const gcGeneralDataType & dtype)
{
    for(gcGeneralDataType::const_iterator i = dtype.begin(); i != dtype.end(); i++)
    {
        Union(*i);
    }
}

void
gcGeneralDataType::Union(const gcSpecificDataType dtype)
{
    insert(dtype);
}

gcGeneralDataType &
gcGeneralDataType::operator=(const gcSpecificDataType dtype)
{
    clear();
    Union(dtype);
    return *this;
}

//____________________________________________________________________________________
