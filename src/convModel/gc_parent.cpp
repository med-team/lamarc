// $Id: gc_parent.cpp,v 1.4 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_creation_info.h"           // JRM this doing anything?
#include "gc_errhandling.h"
#include "gc_data.h"                    // for ToWxString
#include "gc_default.h"
#include "gc_parent.h"
#include "gc_strings.h"
#include "gc_structures_err.h"
#include "wx/log.h"
#include "wx/string.h"
#include "wx/tokenzr.h"

//------------------------------------------------------------------------------------

gcParent::gcParent()
    :
    m_blessed(false),
    m_level(0),
    m_index(0),
    m_length(0),
    m_hasChild1(false),
    m_child1Id(gcdefault::badIndex),
    m_hasChild2(false),
    m_child2Id(gcdefault::badIndex),
    m_hasParent(false),
    m_parentId(gcdefault::badIndex)
{
}

gcParent::~gcParent()
{
}

void
gcParent::SetBlessed(bool blessed)
{
    m_blessed = blessed;
}

bool
gcParent::GetBlessed() const
{
    return m_blessed;
}

void
gcParent::SetDispLevel(int level)
{
    m_level = level;
}

int
gcParent::GetDispLevel() const
{
    return m_level;
}

void
gcParent::SetDispIndex(int index)
{
    m_index = index;
}

int
gcParent::GetDispIndex() const
{
    return m_index;
}

void
gcParent::SetDispLength(int length)
{
    m_length = length;
}

int
gcParent::GetDispLength() const
{
    return m_length;
}

void
gcParent::DebugDump(wxString prefix) const
{
    wxLogVerbose("%sparent %s (panel id %ld)", // JMDUMPOK
                 prefix.c_str(),
                 GetName().c_str(),
                 (long)GetId());
    wxLogVerbose("%sparent %s",(prefix+gcstr::indent).c_str(),GetParentIdString().c_str());  // JMDUMPOK
    wxLogVerbose("%schild1 %s",(prefix+gcstr::indent).c_str(),GetChild1IdString().c_str());  // JMDUMPOK
    wxLogVerbose("%schild2 %s",(prefix+gcstr::indent).c_str(),GetChild2IdString().c_str());   // JMDUMPOK
}

void
gcParent::SetChild1Id(size_t id)
{
    m_child1Id  = id;
    m_hasChild1 = true;
}

void
gcParent::ClearChild1Id()
{
    m_child1Id  = gcdefault::badIndex;
    m_hasChild1 = false;
}

bool
gcParent::HasChild1() const
{
    return m_hasChild1;
}

size_t
gcParent::GetChild1Id() const
{
    if(!HasChild1())
    {
        wxString msg = wxString::Format(gcerr::unsetChild1Id,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_child1Id;
}

size_t
gcParent::GetChild1Id()
{
    if(!HasChild1())
    {
        wxString msg = wxString::Format(gcerr::unsetChild1Id,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_child1Id;
}

wxString
gcParent::GetChild1IdString() const
{
    if(HasChild1())
    {
        return wxString::Format("%d",(int)GetChild1Id());
    }
    return gcstr::unknown;
}

void
gcParent::SetChild2Id(size_t id)
{
    m_child2Id  = id;
    m_hasChild2 = true;
}

void
gcParent::ClearChild2Id()
{
    m_child2Id  = gcdefault::badIndex;
    m_hasChild2 = false;
}

bool
gcParent::HasChild2() const
{
    return m_hasChild2;
}

size_t
gcParent::GetChild2Id() const
{
    if(!HasChild2())
    {
        wxString msg = wxString::Format(gcerr::unsetChild2Id,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_child2Id;
}

size_t
gcParent::GetChild2Id()
{
    if(!HasChild2())
    {
        wxString msg = wxString::Format(gcerr::unsetChild2Id,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_child2Id;
}

wxString
gcParent::GetChild2IdString() const
{
    if(HasChild2())
    {
        return wxString::Format("%d",(int)GetChild2Id());
    }
    return gcstr::unknown;
}

void
gcParent::SetParentId(size_t id)
{
    m_parentId  = id;
    m_hasParent = true;
}

void
gcParent::ClearParentId()
{
    m_parentId  = gcdefault::badIndex;
    m_hasParent = false;
}

bool
gcParent::HasParent() const
{
    return m_hasParent;
}

size_t
gcParent::GetParentId() const
{
#if 0
    // shut off for debugging  JRMHACK
    if(!HasParent())
    {
        wxString msg = wxString::Format(gcerr::unsetParentId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
#endif

    return m_parentId;
}

size_t
gcParent::GetParentId()
{
#if 0
    // shut off for debugging  JRMHACK
    if(!HasParent())
    {
        wxString msg = wxString::Format(gcerr::unsetParentId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
#endif

    return m_parentId;
}

wxString
gcParent::GetParentIdString() const
{
    if(HasParent())
    {
        return wxString::Format("%d",(int)GetParentId());
    }
    return gcstr::unknown;
}
//____________________________________________________________________________________
