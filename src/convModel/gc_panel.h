// $Id: gc_panel.h,v 1.3 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PANEL_H
#define GC_PANEL_H

#include "wx/string.h"
#include "gc_creation_info.h"
#include "gc_phase.h"
#include "gc_quantum.h"
#include "gc_types.h"

class GCStructures;

class gcPanel : public GCQuantum
{
    friend class GCStructures;

  private:
    bool                        m_blessed;  // true if user has edited m_nPanels

    bool                        m_hasRegion;
    size_t                      m_regionId;

    bool                        m_hasPop;
    size_t                      m_popId;

    long                        m_nPanels;

    gcCreationInfo              m_creationInfo;  //JRM this doing anything?

    void   SetCreationInfo(const gcCreationInfo &);

  public:

    gcPanel();
    ~gcPanel();

    void        SetBlessed(bool blessed);
    bool        GetBlessed()            const;

    bool        HasRegion()             const;
    void        SetRegionId(size_t id);
    void        UnsetRegionId();
    size_t      GetRegionId()           const;
    wxString    GetRegionIdString()     const;

    bool        HasPop()                const;
    void        SetPopId(size_t id);
    void        UnsetPopId();
    size_t      GetPopId()              const;
    wxString    GetPopIdString()        const;

    void        SetNumPanels(long val);
    long        GetNumPanels()          const;
    wxString    GetNumPanelsString()    const;


    void        DebugDump(wxString prefix=wxEmptyString) const;

};

#endif  // GC_PANEL_H

//____________________________________________________________________________________
