// $Id: gc_migration.cpp,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_creation_info.h"           // JRM this doing anything?
#include "gc_errhandling.h"
#include "gc_data.h"                    // for ToWxString
#include "gc_default.h"
#include "gc_migration.h"
#include "gc_strings.h"
#include "gc_strings_mig.h"
#include "gc_structures_err.h"
#include "wx/log.h"
#include "wx/string.h"
#include "wx/tokenzr.h"

//------------------------------------------------------------------------------------

gcMigration::gcMigration()
    :
    m_blessed(false),
    m_hasFrom(false),
    m_fromId(gcdefault::badIndex),
    m_hasTo(false),
    m_toId(gcdefault::badIndex),
    m_startValue(50.0),
    m_method(migmethod_USER),
    m_profile(migprofile_NONE),
    m_constraint(migconstraint_UNCONSTRAINED)
{
    SetName(wxString::Format(gcstr_mig::internalName,(long)GetId()));
}

gcMigration::~gcMigration()
{
}

void
gcMigration::SetBlessed(bool blessed)
{
    m_blessed = blessed;
}

bool
gcMigration::GetBlessed() const
{
    return m_blessed;
}

void
gcMigration::DebugDump(wxString prefix) const
{
#if 0
    wxLogDebug("%spanel %s (panel id %ld)", // JMDUMPOK
               prefix.c_str(),
               GetName().c_str(),
               (long)GetId());
    wxLogDebug("%sregion %s",(prefix+gcstr::indent).c_str(),ToWxString(GetFromID()).c_str());  // JMDUMPOK
    wxLogDebug("%s%s population",(prefix+gcstr::indent).c_str(),GetToulation().c_str());  // JMDUMPOK
    wxLogDebug("%s%s number of panels",(prefix+gcstr::indent).c_str(),GetStartValue.c_str());   // JMDUMPOK
#endif

    wxLogVerbose("%smatrix %s (matrix id %ld)", // JMDUMPOK
                 prefix.c_str(),
                 GetName().c_str(),
                 (long)GetId());
    wxLogVerbose("%sFrom %s",(prefix+gcstr::indent).c_str(),GetFromIdString().c_str());  // JMDUMPOK
    wxLogVerbose("%sTo %s",(prefix+gcstr::indent).c_str(),GetToIdString().c_str());  // JMDUMPOK
    wxLogVerbose("%s%s start value",(prefix+gcstr::indent).c_str(),GetStartValueString().c_str());   // JMDUMPOK
    wxLogVerbose("%s%s migration method",(prefix+gcstr::indent).c_str(),GetMethodString().c_str());   // JMDUMPOK
    wxLogVerbose("%s%s migration profile",(prefix+gcstr::indent).c_str(),GetProfileAsString().c_str());   // JMDUMPOK
    wxLogVerbose("%s%s migration constraint",(prefix+gcstr::indent).c_str(),GetConstraintString().c_str());   // JMDUMPOK
}

void
gcMigration::SetFromId(size_t id)
{
    m_fromId  = id;
    m_hasFrom = true;
}

bool
gcMigration::HasFrom() const
{
    return m_hasFrom;
}

size_t
gcMigration::GetFromId() const
{
    if(!HasFrom())
    {
        wxString msg = wxString::Format(gcerr::unsetFromId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_fromId;
}

wxString
gcMigration::GetFromIdString() const
{
    if(HasFrom())
    {
        return wxString::Format("%d",(int)GetFromId());
    }
    return gcstr::unknown;
}

void
gcMigration::SetToId(size_t id)
{
    m_toId  = id;
    m_hasTo = true;
}

bool
gcMigration::HasTo() const
{
    return m_hasTo;
}

size_t
gcMigration::GetToId() const
{
    if(!HasTo())
    {
        wxString msg = wxString::Format(gcerr::unsetToId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return m_toId;
}

wxString
gcMigration::GetToIdString() const
{
    if(HasTo())
    {
        return wxString::Format("%d",(int)GetToId());
    }
    return gcstr::unknown;
}

void
gcMigration::SetStartValue(double val)
{
    m_startValue = val;
}

double
gcMigration::GetStartValue() const
{
    return m_startValue;
}

wxString
gcMigration::GetStartValueString() const
{
    return wxString::Format("%f",GetStartValue());
}

void
gcMigration::SetMethod(migration_method val)
{
    m_method = val;
}

migration_method
gcMigration::GetMethod() const
{
    return m_method;
}

wxString
gcMigration::GetMethodString() const
{
    switch (m_method)
    {
        case migmethod_USER:
            return gcstr_mig::migmethodUser;
        case migmethod_FST:
            return gcstr_mig::migmethodFST;
        default:
            return wxString::Format("Unknown Migration Method");
    }
}

void
gcMigration::SetProfile(migration_profile val)
{
    m_profile = val;
}

migration_profile
gcMigration::GetProfile() const
{
    return m_profile;
}

wxString
gcMigration::GetProfileAsString() const
{
    switch (m_profile)
    {
        case migprofile_NONE:
            return gcstr_mig::migprofileNone;
        case migprofile_FIXED:
            return gcstr_mig::migprofileFixed;
        case migprofile_PERCENTILE:
            return gcstr_mig::migprofilePercentile;
        default:
            return wxString::Format("Unknown Migration Profile");
    }
}

void
gcMigration::SetConstraint(migration_constraint val)
{
    m_constraint = val;
}

migration_constraint
gcMigration::GetConstraint() const
{
    return m_constraint;
}

wxString
gcMigration::GetConstraintString() const
{
    switch (m_constraint)
    {
        case migconstraint_INVALID:
            return gcstr_mig::migconstraintInvalid;
        case migconstraint_CONSTANT:
            return gcstr_mig::migconstraintConstant;
        case migconstraint_SYMMETRIC:
            return gcstr_mig::migconstraintSymmetric;
        case migconstraint_UNCONSTRAINED:
            return gcstr_mig::migconstraintUnconstained;
        default:
            return wxString::Format("Unknown Migration Constraint");
    }
}
//____________________________________________________________________________________
