// $Id: gc_file_info.h,v 1.5 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FILE_INFO_H
#define GC_FILE_INFO_H

#include <list>
#include <map>
#include <set>

#include "gc_locus.h"
#include "gc_population.h"
#include "gc_quantum.h"
#include "gc_region.h"
#include "gc_trait.h"

#include "gc_loci_match.h"
#include "gc_pop_match.h"

#include "wx/string.h"

class gcFileInfo
{
  private:
    bool            m_selected;
    bool            m_hasParse;
    size_t          m_parseId;
    GCPopMatcher    m_popMatch;
    GCLocusMatcher  m_locMatch;
    bool            m_hasAdjacentHapAssignment;
    size_t          m_adjacentHapAssignment;
    bool            m_hasGenoFile;
    size_t          m_genoFileId;

  public:
    gcFileInfo();
    virtual ~gcFileInfo()               ;
    bool    GetSelected() const         ;
    bool    HasParse() const            ;
    size_t  GetParse() const            ;
    void    SetSelected(bool selected)  ;
    void    SetParse(size_t parseId)    ;
    void    UnsetParse()                ;
    void    DebugDump(wxString prefix=wxEmptyString) const;

    bool    HasAdjacentHapAssignment()  const ;
    size_t  GetAdjacentHapAssignment()  const;
    void    SetAdjacentHapAssignment(size_t numAdj);
    void    UnsetAdjacentHapAssignment();

    bool    HasGenoFile() const;
    void    UnsetGenoFile();

    const GCPopMatcher &    GetPopMatcher() const;
    const GCLocusMatcher &  GetLocMatcher() const;
    void    SetPopMatcher(const GCPopMatcher & p);
    void    SetLocMatcher(const GCLocusMatcher & l);
};

class gcFileMap : public std::map<size_t,gcFileInfo>
{
  public:
    gcFileMap() ;
    virtual ~gcFileMap() ;
    void DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_FILE_INFO_H

//____________________________________________________________________________________
