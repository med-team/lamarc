// $Id: cmdfileschema.cpp,v 1.24 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <iostream>

#include "cmdfileschema.h"
#include "cnv_strings.h"

CmdFileSchema::CmdFileSchema()
{
    const bool required = true;
    const bool optional = false;
    const bool onlyone = true;
    const bool many = false;

    AddTag(cnvstr::TAG_CONVERTER_CMD);
    AddAttribute(optional,cnvstr::TAG_CONVERTER_CMD,cnvstr::ATTR_VERSION);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_TRAITS);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_REGIONS);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_POPULATIONS);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_PANELS);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_INDIVIDUALS);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_INFILES);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_OUTFILE);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_ADDCOMMENT);
    AddSubtag(optional, onlyone,cnvstr::TAG_CONVERTER_CMD,cnvstr::TAG_DIVERGENCES);

    AddSubtag(optional, many,   cnvstr::TAG_TRAITS,cnvstr::TAG_TRAIT_INFO);
    AddSubtag(required, onlyone,cnvstr::TAG_TRAIT_INFO,cnvstr::TAG_NAME);
    AddSubtag(required, many,   cnvstr::TAG_TRAIT_INFO,cnvstr::TAG_ALLELE);
    AddSubtag(optional, many,   cnvstr::TAG_TRAITS,cnvstr::TAG_PHENOTYPE);
    AddSubtag(required, onlyone,cnvstr::TAG_PHENOTYPE,cnvstr::TAG_NAME);
    AddSubtag(required, many   ,cnvstr::TAG_PHENOTYPE,cnvstr::TAG_GENO_RESOLUTIONS);
    AddSubtag(required, onlyone,cnvstr::TAG_GENO_RESOLUTIONS,cnvstr::TAG_TRAIT_NAME);
    AddSubtag(required, many,   cnvstr::TAG_GENO_RESOLUTIONS,cnvstr::TAG_HAPLOTYPES);
    AddSubtag(required, onlyone,cnvstr::TAG_HAPLOTYPES,cnvstr::TAG_ALLELES);
    AddSubtag(required, onlyone,cnvstr::TAG_HAPLOTYPES,cnvstr::TAG_PENETRANCE);

    AddSubtag(required, many,cnvstr::TAG_REGIONS,cnvstr::TAG_REGION);

    AddSubtag(required, onlyone,cnvstr::TAG_REGION,cnvstr::TAG_NAME);
    AddSubtag(optional, onlyone,cnvstr::TAG_REGION,cnvstr::TAG_EFFECTIVE_POPSIZE);
    AddSubtag(required, onlyone,cnvstr::TAG_REGION,cnvstr::TAG_SEGMENTS);

    AddSubtag(optional, many,   cnvstr::TAG_REGION,cnvstr::TAG_TRAIT_LOCATION);
    AddSubtag(required, onlyone,cnvstr::TAG_TRAIT_LOCATION,cnvstr::TAG_TRAIT_NAME);

    AddSubtag(required, many,cnvstr::TAG_SEGMENTS,cnvstr::TAG_SEGMENT);

    AddAttribute(required,cnvstr::TAG_SEGMENT,cnvstr::ATTR_DATATYPE);
    AddAttribute(optional,cnvstr::TAG_SEGMENT,cnvstr::ATTR_PROXIMITY);
    AddSubtag(required, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_NAME);
    AddSubtag(required, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_MARKERS);
    AddSubtag(optional, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_MAP_POSITION);

    //For SNP data:
    AddSubtag(optional, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_SCANNED_LENGTH);
    AddSubtag(optional, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_SCANNED_DATA_POSITIONS);
    AddSubtag(optional, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_FIRST_POSITION_SCANNED);
    AddSubtag(optional, onlyone,cnvstr::TAG_SEGMENT,cnvstr::TAG_UNRESOLVED_MARKERS);

    AddSubtag(optional, many,cnvstr::TAG_PANELS,cnvstr::TAG_PANEL);
    AddSubtag(required, onlyone,cnvstr::TAG_PANEL,cnvstr::TAG_PANEL_REGION);
    AddSubtag(required, onlyone,cnvstr::TAG_PANEL,cnvstr::TAG_PANEL_POP);
    AddSubtag(optional, onlyone,cnvstr::TAG_PANEL,cnvstr::TAG_PANEL_NAME);
    AddSubtag(required, onlyone,cnvstr::TAG_PANEL,cnvstr::TAG_PANEL_SIZE);

    AddSubtag(required, many,cnvstr::TAG_POPULATIONS,cnvstr::TAG_POPULATION);

    AddSubtag(optional, many    ,cnvstr::TAG_INDIVIDUALS,cnvstr::TAG_INDIVIDUAL);
    AddSubtag(required, onlyone ,cnvstr::TAG_INDIVIDUAL,cnvstr::TAG_NAME);
    AddSubtag(required, many    ,cnvstr::TAG_INDIVIDUAL,cnvstr::TAG_SAMPLE);
    AddSubtag(required, onlyone ,cnvstr::TAG_SAMPLE,cnvstr::TAG_NAME);
    AddSubtag(optional, many    ,cnvstr::TAG_INDIVIDUAL,cnvstr::TAG_PHASE);
    AddSubtag(required, onlyone ,cnvstr::TAG_PHASE,cnvstr::TAG_SEGMENT_NAME);
    AddSubtag(required, onlyone ,cnvstr::TAG_PHASE,cnvstr::TAG_UNRESOLVED_MARKERS);
    AddSubtag(optional, many    ,cnvstr::TAG_INDIVIDUAL,cnvstr::TAG_HAS_PHENOTYPE);
    AddSubtag(optional, many    ,cnvstr::TAG_INDIVIDUAL,cnvstr::TAG_GENO_RESOLUTIONS);

    AddSubtag(required, many,cnvstr::TAG_INFILES,cnvstr::TAG_INFILE);
    AddAttribute(required,cnvstr::TAG_INFILE,cnvstr::ATTR_DATATYPE);
    AddAttribute(required,cnvstr::TAG_INFILE,cnvstr::ATTR_FORMAT);
    AddAttribute(required,cnvstr::TAG_INFILE,cnvstr::ATTR_SEQUENCEALIGNMENT);
    AddSubtag(required, onlyone,cnvstr::TAG_INFILE,cnvstr::TAG_NAME);
    AddSubtag(required, onlyone,cnvstr::TAG_INFILE,cnvstr::TAG_SEGMENTS_MATCHING);
    AddSubtag(required, onlyone,cnvstr::TAG_INFILE,cnvstr::TAG_POP_MATCHING);
    AddSubtag(optional, onlyone,cnvstr::TAG_INFILE,cnvstr::TAG_INDIVIDUALS_FROM_SAMPLES);
    AddAttribute(required,cnvstr::TAG_INDIVIDUALS_FROM_SAMPLES,cnvstr::ATTR_TYPE);

    AddAttribute(required,cnvstr::TAG_SEGMENTS_MATCHING,cnvstr::ATTR_TYPE);
    AddAttribute(required,cnvstr::TAG_POP_MATCHING,cnvstr::ATTR_TYPE);

    AddSubtag(optional, many,cnvstr::TAG_SEGMENTS_MATCHING,cnvstr::TAG_SEGMENT_NAME);
    AddSubtag(optional, many,cnvstr::TAG_POP_MATCHING,cnvstr::TAG_POP_NAME);

    AddSubtag(optional, many,cnvstr::TAG_DIVERGENCES,cnvstr::TAG_DIVERGENCE);
    AddSubtag(optional, onlyone,cnvstr::TAG_DIVERGENCE,cnvstr::TAG_DIV_ANCESTOR);
    AddSubtag(required, onlyone,cnvstr::TAG_DIVERGENCE,cnvstr::TAG_DIV_CHILD1);
    AddSubtag(required, onlyone,cnvstr::TAG_DIVERGENCE,cnvstr::TAG_DIV_CHILD2);
}

CmdFileSchema::~CmdFileSchema()
{
}

void
CmdFileSchema::AddAttribute(bool required, wxString tagName, wxString attrName)
{
    //ParseTreeSchema::AddAttribute(required,tagName.c_str(),attrName.c_str());
    ParseTreeSchema::AddAttribute(required, (const char *)tagName.mb_str(), (const char *)attrName.mb_str());// JRM hack
}

void
CmdFileSchema::AddTag(wxString tag)
{
    //ParseTreeSchema::AddTag(tag.c_str());
    ParseTreeSchema::AddTag((const char *)tag.mb_str());// JRM hack
}

void
CmdFileSchema::AddSubtag(bool required, bool onlyOne, wxString parentTag, wxString childTag)
{
    //ParseTreeSchema::AddSubtag(required,onlyOne,parentTag.c_str(),childTag.c_str());
    ParseTreeSchema::AddSubtag(required,onlyOne, (const char *)parentTag.mb_str(), (const char *)childTag.mb_str());// JRM hack
}

//____________________________________________________________________________________
