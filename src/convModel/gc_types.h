// $Id: gc_types.h,v 1.31 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_TYPES_H
#define GC_TYPES_H

#include <map>
#include <list>
#include <set>
#include <string>
#include "Converter_types.h"

enum GCFileFormat
{
    // should include each type of file converter can read
    format_NONE_SET,
    format_PHYLIP,
    format_MIGRATE
};

enum gcSpecificDataType
{
    //  data types as used in lamarc
    sdatatype_NONE_SET,
    sdatatype_DNA,
    sdatatype_SNP,
    sdatatype_KALLELE,
    sdatatype_MICROSAT
};

class gcGeneralDataType : public std::set<gcSpecificDataType>
{
  public:
    gcGeneralDataType();
    virtual ~gcGeneralDataType();
    bool HasAllelic() const;
    bool HasNucleic() const;
    bool CompatibleWith(const gcGeneralDataType&) const;
    void Disallow(const gcSpecificDataType);
    void Intersect(const gcGeneralDataType&);
    void Union(const gcGeneralDataType&);
    void Union(const gcSpecificDataType);
    gcGeneralDataType & operator=(const gcSpecificDataType);
};

enum GCInterleaving
{
    // are data sequences presented all at once or for interleaved comparison
    interleaving_NONE_SET,
    interleaving_SEQUENTIAL,
    interleaving_INTERLEAVED,
    interleaving_MOOT
};

enum gcPhaseSource
{
    phaseSource_NONE_SET,
    phaseSource_PHASE_FILE,
    phaseSource_MULTI_PHASE_SAMPLE,
    phaseSource_FILE_ADJACENCY,
    phaseSource_COUNT
};

enum loc_match
{
    locmatch_DEFAULT,
    locmatch_SINGLE,
    locmatch_LINKED,
    locmatch_VECTOR
};

enum pop_match
{
    popmatch_DEFAULT,
    popmatch_NAME,
    popmatch_SINGLE,
    popmatch_VECTOR
};

enum matrix_type
{
    matrixtype_MIGRATION,
    matrixtype_DIVERGENCE
};

enum matrix_cell_type
{
    matrixcelltype_EMPTY,
    matrixcelltype_INVALID,
    matrixcelltype_CORNER,
    matrixcelltype_LABEL,
    matrixcelltype_VALUE
};

enum matrix_cell_source
{
    matrixcellsource_NONE,
    matrixcellsource_POP,
    matrixcellsource_PARENT
};

enum migration_method
{
    migmethod_USER,
    migmethod_FST
};

enum migration_profile
{
    migprofile_NONE,
    migprofile_FIXED,
    migprofile_PERCENTILE
};

enum migration_constraint
{
    migconstraint_INVALID,
    migconstraint_CONSTANT,
    migconstraint_SYMMETRIC,
    migconstraint_UNCONSTRAINED
};

#endif  // GC_TYPES_H

//____________________________________________________________________________________
