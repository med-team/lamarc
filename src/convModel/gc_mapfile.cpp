// $Id: gc_mapfile.cpp,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#if 0  // Potentially DEAD CODE (bobgian, Feb 2010)

#include "gc_data.h"
#include "gc_region.h"
#include "gc_mapfile.h"
#include "gc_locus.h"
#include "tixml_util.h"
#include "xml.h"
#include "xml_strings.h"

#include "wx/filename.h"
#include "wx/log.h"

size_t GCMapFile::s_mapFileCount = 0;

GCMapFile::GCMapFile(wxString mapFileName)
    :
    m_mapFileId(s_mapFileCount++),
    m_mapFileName(mapFileName),
    m_xmlParserPointer( new XmlParser())

{
    m_xmlParserPointer->ParseFileData(std::string(mapFileName.c_str()));

    TiXmlElement * topElem = m_xmlParserPointer->GetRootElement();

    vector<TiXmlElement*> regionVec = ti_optionalChildren(topElem,xmlstr::XML_TAG_REGION);
    for(size_t i=0; i < regionVec.size(); i++)
    {
        TiXmlElement * regionElem = regionVec[i];

        size_t numHaps = ti_size_t_from_text(
            ti_optionalChild(regionElem,xmlstr::XML_TAG_HAP_COUNT));

        gcRegion & linkageGroup = AddLinkageGroup(numHaps);

        TiXmlElement * spacingElem
            = ti_optionalChild(regionElem,xmlstr::XML_TAG_SPACING);
        vector<TiXmlElement*> blockVec
            = ti_optionalChildren(spacingElem,xmlstr::XML_TAG_BLOCK);
        for(size_t j=0; j < blockVec.size(); j++)
        {
            TiXmlElement * blockElem = blockVec[j];

            wxString dataTypeString = ti_attributeValue(blockElem,xmlstr::XML_ATTRTYPE_TYPE);
            GCDataType dataType = ProduceGCDataTypeOrBarf(dataTypeString);
            size_t numSites = ti_size_t_from_text(
                ti_optionalChild(blockElem,xmlstr::XML_TAG_NUM_SITES));
            long offset = ti_long_from_text(
                ti_optionalChild(blockElem,xmlstr::XML_TAG_OFFSET));
            long mapPosition = ti_long_from_text(
                ti_optionalChild(blockElem,xmlstr::XML_TAG_MAP_POSITION));
            size_t length = ti_size_t_from_text(
                ti_optionalChild(blockElem,xmlstr::XML_TAG_LENGTH));
            wxString locationsString = ti_nodeText(
                ti_optionalChild(blockElem,xmlstr::XML_TAG_LOCATIONS));

            gcLocus & locus
                = linkageGroup.AddLocus(dataType,numSites);
            locus.SetOffset(offset);
            locus.SetMapPosition(mapPosition);
            locus.SetLength(length);
            locus.SetLocations(locationsString);
        }
    }
}

GCMapFile::~GCMapFile()
{
}

gcRegion &
GCMapFile::AddLinkageGroup(size_t numHaps)
{
    gcRegion * group = new gcRegion(*this,numHaps);
    m_linkageGroups.insert(group);
    return *group;
}

size_t
GCMapFile::GetId() const
{
    return m_mapFileId;
}

wxString
GCMapFile::GetShortName() const
{
    wxFileName fname(GetName());
    return fname.GetFullName(); // Full name == file name + extension
}

void
GCMapFile::DebugDump(wxString prefix) const
{
    wxLogDebug("%sMap File: %s",prefix.c_str(),m_mapFileName.c_str());  // EWDUMPOK
    for(linkageGroupSet::const_iterator i=m_linkageGroups.begin(); i != m_linkageGroups.end(); i++)
    {
        const gcRegion & group = **i;
        group.DebugDump(prefix+gcstr::indent);
    }
    wxLogDebug("%s*********",prefix.c_str());   // EWDUMPOK
}

long
GCMapFile::ExtractLongValue(TiXmlElement * ancestor, std::string tag, bool required)
{
    return -1;
}

#endif

//____________________________________________________________________________________
