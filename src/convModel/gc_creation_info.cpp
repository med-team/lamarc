// $Id: gc_creation_info.cpp,v 1.4 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "wx/string.h"
#include "gc_creation_info.h"
#include "gc_default.h"
#include "gc_strings_creation.h"

//------------------------------------------------------------------------------------

gcCreationInfo::gcCreationInfo()
    :   m_creationType(created_UNKNOWN),
        m_hasLineNumber(false),
        m_lineNumber(gcdefault::badIndex),
        m_hasFileName(false),
        m_fileName(wxEmptyString)
{
}

gcCreationInfo::~gcCreationInfo()
{
}

void
gcCreationInfo::SetLineNumber(size_t lineNumber)
{
    m_hasLineNumber = true;
    m_lineNumber = lineNumber;
}

void
gcCreationInfo::SetFileName(wxString fileName)
{
    m_hasFileName = true;
    m_fileName = fileName;
}

bool
gcCreationInfo::HasLineNumber() const
{
    return m_hasLineNumber;
}

size_t
gcCreationInfo::GetLineNumber() const
{
    assert(HasLineNumber());
    return m_lineNumber;
}

bool
gcCreationInfo::HasFileName() const
{
    return m_hasFileName;
}

wxString
gcCreationInfo::GetFileName() const
{
    assert(HasFileName());
    return m_fileName;
}

wxString
gcCreationInfo::GetDescriptiveName() const
{
    switch(m_creationType)
    {
        case created_UNKNOWN:
            return wxEmptyString;
            break;
        case created_CMDFILE:
            assert(HasLineNumber());
            assert(HasFileName());
            return wxString::Format(gcstr_creation::cmdfile,(long)m_lineNumber,m_fileName.c_str());
            break;
        case created_DATAFILE:
            assert(HasLineNumber());
            assert(HasFileName());
            return wxString::Format(gcstr_creation::datafile,(long)m_lineNumber,m_fileName.c_str());
            break;
        case created_GUI:
            return gcstr_creation::gui;
            break;
        default:
            assert(false);
            return wxEmptyString;
            break;
    }
}

gcCreationInfo
gcCreationInfo::MakeGuiCreationInfo()
{
    gcCreationInfo info;
    info.m_creationType = created_GUI;
    return info;
}

gcCreationInfo
gcCreationInfo::MakeDataFileCreationInfo(size_t lineNumber, wxString fileName)
{
    gcCreationInfo info;
    info.m_creationType = created_DATAFILE;
    info.SetLineNumber(lineNumber);
    info.SetFileName(fileName);
    return info;
}

gcCreationInfo
gcCreationInfo::MakeCmdFileCreationInfo(size_t lineNumber, wxString fileName)
{
    gcCreationInfo info;
    info.m_creationType = created_CMDFILE;
    info.SetLineNumber(lineNumber);
    info.SetFileName(fileName);
    return info;
}

//____________________________________________________________________________________
