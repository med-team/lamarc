// $Id: gc_set_util.cpp,v 1.4 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_set_util.h"
#include "wx/log.h"

gcIdSet::gcIdSet()
{
}

gcIdSet::~gcIdSet()
{
}

void
gcIdSet::DebugDump(wxString prefix) const
{
    wxString ids="";
    for(const_iterator i=begin(); i != end(); i++)
    {
        size_t id = *i;
        ids += wxString::Format("%d ",(int)id);
    }
    wxLogDebug("%s%s", prefix.c_str(), ids.c_str());    // EWDUMPOK
}

wxString
gcIdSet::AsString() const
{
    wxString ids="";
    for(const_iterator i=begin(); i != end(); i++)
    {
        size_t blockId = *i;
        ids += wxString::Format("%d ",(int)blockId);
    }
    return ids;
}

gcIdVec::gcIdVec()
{
}

gcIdVec::~gcIdVec()
{
}

void
gcIdVec::DebugDump(wxString prefix) const
{
    wxString ids="";
    for(const_iterator i=begin(); i != end(); i++)
    {
        size_t id = *i;
        ids += wxString::Format("%d ",(int)id);
    }
    wxLogDebug("%s%s", prefix.c_str(), ids.c_str());    // EWDUMPOK
}

wxString
gcIdVec::AsString() const
{
    wxString ids="";
    for(const_iterator i=begin(); i != end(); i++)
    {
        size_t blockId = *i;
        ids += wxString::Format("%d ",(int)blockId);
    }
    return ids;
}

//____________________________________________________________________________________
