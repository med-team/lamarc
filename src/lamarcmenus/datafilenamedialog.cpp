// $Id: datafilenamedialog.cpp,v 1.15 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include <iostream>

#include "dialogrepeat.h"
#include "datafilenamedialog.h"
#include "errhandling.h"
#include "menudefs.h"
#include "stringx.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "xml.h"

DataFileNameDialog::DataFileNameDialog(XmlParser & parser)
    : DialogRepeat() , m_dataFileName(parser.GetFileName()), m_parser(parser)
{
}

DataFileNameDialog::~DataFileNameDialog()
{
}

long DataFileNameDialog::maxTries()
{
    return 3;
}

std::string DataFileNameDialog::displayFileName()
{
    std::string displayFileName;
    if(m_dataFileName == "")
    {
        displayFileName = "[No default set]\n";
    }
    else
    {
        displayFileName = "[Default: "+ m_dataFileName + "]\n";
    }
    return displayFileName;
}

std::string DataFileNameDialog::beforeLoopOutputString()
{
    return "";
}

std::string DataFileNameDialog::inLoopOutputString()
{
    return "Enter the location of the data file\n"+displayFileName()+"\n";
}

std::string DataFileNameDialog::inLoopFailureOutputString()
{
    return " \n \n" + m_errmsg + "\n";
}

std::string DataFileNameDialog::afterLoopSuccessOutputString()
{
    std::string message = "Data file was read successfully\n\n";
    message += "Calculating starting values; please be patient";
    return message;
}

std::string DataFileNameDialog::afterLoopFailureOutputString()
{
    return "Unable to read or find your file in "+ToString(maxTries())
        + " attempts.\n";
}

bool DataFileNameDialog::handleInput(std::string input)
{
    if (input.size () != 0)
    {
        m_dataFileName = input;
    }
    try
    {
        m_parser.ParseFileData(m_dataFileName);
    }
    catch (const data_error& e)
    {
        m_errmsg = e.whatString();
        return false;
    }
    return true;
}

void DataFileNameDialog::doFailure()
{
    throw data_error("To create a LAMARC input file, please run the converter (lam_conv).");
}

//____________________________________________________________________________________
