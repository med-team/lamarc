// $Id: diseasemenus.h,v 1.15 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DISEASEMENUS_H
#define DISEASEMENUS_H

#include <string>
#include <vector>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class SetAllDiseaseRatesMenuItem : public SetMenuItemId
{
  public:
    SetAllDiseaseRatesMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetAllDiseaseRatesMenuItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

// Specializes SetMenuItemNoId to be visible only when the
// disease force is turned on
class DiseaseMaxEventsMenuItem : public SetMenuItemNoId
{
  public:
    DiseaseMaxEventsMenuItem(std::string myKey, UIInterface & myui);
    virtual ~DiseaseMaxEventsMenuItem();
    virtual bool IsVisible();
};

// Specializes SetMenuItemNoId to be visible only when the
// disease force is turned on
class DiseaseLocationMenuItem : public SetMenuItemNoId
{
  public:
    DiseaseLocationMenuItem(std::string myKey, UIInterface & myui);
    virtual ~DiseaseLocationMenuItem();
    virtual bool IsVisible();
};

class DiseaseMenu : public NewMenu
{
  public:
    DiseaseMenu(UIInterface & myui);
    virtual ~DiseaseMenu();
};

class DiseaseMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    DiseaseMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~DiseaseMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new DiseaseMenu(ui));};
};

#endif  // DISEASEMENUS_H

//____________________________________________________________________________________
