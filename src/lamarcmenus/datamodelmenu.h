// $Id: datamodelmenu.h,v 1.29 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DATAMODELMENU_H
#define DATAMODELMENU_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"
#include "ui_constants.h"
#include "ui_regid.h"

class UIInterface;

class AssignToGlobalMenuItem : public ToggleMenuItemId
{
  public:
    AssignToGlobalMenuItem(std::string , UIInterface &, UIId );
    virtual ~AssignToGlobalMenuItem();
    virtual bool IsVisible();
};

class TTRatioMenuItem : public SetMenuItemId
{
  public:
    TTRatioMenuItem(std::string , UIInterface &, UIId );
    virtual ~TTRatioMenuItem();
    virtual bool IsVisible();
};

class DataUncertaintyMenuItem : public SetMenuItemId
{
  public:
    DataUncertaintyMenuItem(std::string , UIInterface &, UIId );
    virtual ~DataUncertaintyMenuItem();
    virtual bool IsVisible();
};

class AlphaMenuItem : public SetMenuItemId
{
  public:
    AlphaMenuItem(std::string , UIInterface &, UIId );
    virtual ~AlphaMenuItem();
    virtual bool IsVisible();
};

class OptimizeAlphaMenuItem : public ToggleMenuItemId
{
  public:
    OptimizeAlphaMenuItem(std::string , UIInterface &, UIId );
    virtual ~OptimizeAlphaMenuItem();
    virtual bool IsVisible();
};

class GTRRatesMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    GTRRatesMenu(UIInterface&,UIId);
    virtual ~GTRRatesMenu();
    virtual std::string Title();
};

class GTRRatesSubMenuItem : public SubMenuItem
{
  protected:
    UIId id;
  public:
    GTRRatesSubMenuItem(std::string , UIInterface &, UIId );
    virtual ~GTRRatesSubMenuItem();
    virtual bool IsVisible();
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string);
};

class FreqsFromDataMenuItem : public ToggleMenuItemId
{
  public:
    FreqsFromDataMenuItem(std::string , UIInterface &, UIId);
    virtual ~FreqsFromDataMenuItem();
    virtual bool IsVisible();
};

class SetFrequencyMenuItemId : public SetMenuItemId
{
  public:
    SetFrequencyMenuItemId(std::string, UIInterface&, std::string, UIId);
    virtual ~SetFrequencyMenuItemId();
    virtual std::string GetText();
    virtual bool IsVisible();
};

class BaseFrequenciesMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    BaseFrequenciesMenu(UIInterface&,UIId);
    virtual ~BaseFrequenciesMenu();
    virtual std::string Title();
};

class BaseFrequenciesSubMenuItem : public SubMenuItem
{
  protected:
    UIId id;
  public:
    BaseFrequenciesSubMenuItem(std::string , UIInterface &, UIId);
    virtual ~BaseFrequenciesSubMenuItem();
    virtual bool IsVisible();
    virtual string GetText();
    virtual string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string);
};

class CategoryMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    CategoryMenu(UIInterface&,UIId);
    ~CategoryMenu();
    virtual std::string Title();
};

class CategoriesMenuGroup : public MenuDisplayGroupBaseImplementation
{
  protected:
    UIId id;
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
  public:
    CategoriesMenuGroup(UIInterface & myui,UIId id);
    virtual ~CategoriesMenuGroup();
    virtual vector<UIId> GetVisibleIds();
    virtual std::string GetKey(UIId id);
    virtual std::string GetText(UIId id);
    virtual std::string GetVariableText(UIId id);
};
class CategoriesMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    CategoriesMenu(UIInterface&,UIId);
    virtual ~CategoriesMenu();
    virtual std::string Title();
};

class CategoriesSubMenuItem : public SubMenuItem
{
  protected:
    UIId id;
  public:
    CategoriesSubMenuItem(std::string , UIInterface &, UIId);
    virtual ~CategoriesSubMenuItem();
    virtual string GetText();
    virtual string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string);
};

class RegionDataModelMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    RegionDataModelMenu(UIInterface & myui, UIId myid);
    virtual ~RegionDataModelMenu();
    virtual std::string Title();
    virtual std::string Info();
};

class RegionDataModelMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
    UIId id;
  public:
    RegionDataModelMenuCreator( UIInterface & myui, UIId myid)
        : ui(myui), id(myid) {};
    virtual ~RegionDataModelMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new RegionDataModelMenu(ui,id));};
};

class RegionDataModels : public MenuDisplayGroupBaseImplementation
{
  protected:
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
  public:
    RegionDataModels(UIInterface & myui);
    virtual ~RegionDataModels();
    //
    virtual vector<UIId> GetVisibleIds();
    virtual std::string GetText(UIId id);
    virtual std::string GetVariableText(UIId id);
};

class LocusDataModelMenu : public NewMenu
{
  protected:
    UIId id;
  public:
    LocusDataModelMenu(UIInterface & myui, UIId myid);
    virtual ~LocusDataModelMenu();
    virtual std::string Title();
    virtual std::string Info();
};

class LocusDataModelMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
    UIId id;
  public:
    LocusDataModelMenuCreator( UIInterface & myui, UIId myid)
        : ui(myui), id(myid) {};
    virtual ~LocusDataModelMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new LocusDataModelMenu(ui,id));};
};

class LocusDataModels : public MenuDisplayGroupBaseImplementation
{
  protected:
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
    UIId id;
  public:
    LocusDataModels(UIInterface & myui,UIId id);
    virtual ~LocusDataModels();
    //
    virtual std::string GetKey(UIId id);
    virtual vector<UIId> GetVisibleIds();
    virtual std::string GetText(UIId id);
    virtual std::string GetVariableText(UIId id);
};

class NewDataModelMenu : public NewMenu
{
  public:
    NewDataModelMenu(UIInterface & myui);
    ~NewDataModelMenu();
};

class NewDataModelMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    NewDataModelMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~NewDataModelMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new NewDataModelMenu(ui));};
};

class GlobalDataModelNuc : public SubMenuItem
{
  public:
    GlobalDataModelNuc(std::string , UIInterface &);
    virtual ~GlobalDataModelNuc();
    virtual bool IsVisible();
};

class GlobalDataModelMsat : public SubMenuItem
{
  public:
    GlobalDataModelMsat(std::string , UIInterface &);
    virtual ~GlobalDataModelMsat();
    virtual bool IsVisible();
};

class GlobalDataModelKAllele : public SubMenuItem
{
  public:
    GlobalDataModelKAllele(std::string , UIInterface &);
    virtual ~GlobalDataModelKAllele();
    virtual bool IsVisible();
};

class MenuDisplayDataType : public MenuDisplayLine
{
  private:
    MenuDisplayDataType();
    UIRegId m_regId;
  protected:
    UIId& GetId();
  public:
    MenuDisplayDataType(UIInterface& ui, UIId id);
    virtual ~MenuDisplayDataType();
    virtual std::string GetKey();
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual bool IsVisible();
    virtual bool Handles(std::string) {return false;};
};

class UseOldSeedMenuItem : public ToggleMenuItemNoId
{
  private:
    UseOldSeedMenuItem();
  public:
    UseOldSeedMenuItem(std::string, UIInterface& ui);
    ~UseOldSeedMenuItem();
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual bool IsVisible();
};

class EffectivePopSizeSubMenuItem : public SubMenuItem
{
  private:
    EffectivePopSizeSubMenuItem();
  public:
    EffectivePopSizeSubMenuItem(std::string, UIInterface& ui);
    ~EffectivePopSizeSubMenuItem();
    virtual bool IsVisible();
};

#endif  // DATAMODELMENU_H

//____________________________________________________________________________________
