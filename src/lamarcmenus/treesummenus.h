// $Id: treesummenus.h,v 1.14 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TREESUMMENUS_H
#define TREESUMMENUS_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class SetMenuItemTreeSumInFileName : public SetMenuItemNoId
{
  public:
    SetMenuItemTreeSumInFileName(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemTreeSumInFileName();
    virtual bool IsVisible();
};

class SetMenuItemTreeSumOutFileName : public SetMenuItemNoId
{
  public:
    SetMenuItemTreeSumOutFileName(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemTreeSumOutFileName();
    virtual bool IsVisible();
};

class TreeSumInMenu : public NewMenu
{
  public:
    TreeSumInMenu(UIInterface & myui);
    ~TreeSumInMenu();
};

class TreeSumOutMenu : public NewMenu
{
  public:
    TreeSumOutMenu(UIInterface & myui);
    ~TreeSumOutMenu();
};

class TreeSumInMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    TreeSumInMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~TreeSumInMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new TreeSumInMenu(ui));};
};

class TreeSumOutMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    TreeSumOutMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~TreeSumOutMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new TreeSumOutMenu(ui));};
};

class TreeSumInSubMenuItem : public SubMenuItem
{
  public:
    TreeSumInSubMenuItem(std::string myKey, UIInterface & myUI);
    virtual std::string GetVariableText();
};

class TreeSumOutSubMenuItem : public SubMenuItem
{
  public:
    TreeSumOutSubMenuItem(std::string myKey, UIInterface & myUI);
    virtual std::string GetVariableText();
};

#endif  // TREESUMMENUS_H

//____________________________________________________________________________________
