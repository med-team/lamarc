// $Id: diseasemenus.cpp,v 1.18 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "diseasemenus.h"
#include "lamarc_strings.h"
#include "matrixitem.h"
#include "menu_strings.h"
#include "newmenuitems.h"
#include "overviewmenus.h"
#include "priormenus.h"
#include "profilemenus.h"
#include "setmenuitem.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "ui_strings.h"

using std::string;

//------------------------------------------------------------------------------------

SetAllDiseaseRatesMenuItem::SetAllDiseaseRatesMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemId(myKey,myui,uistr::globalDisease, UIId(force_DISEASE, uiconst::GLOBAL_ID))
{
}

SetAllDiseaseRatesMenuItem::~SetAllDiseaseRatesMenuItem()
{
}

bool SetAllDiseaseRatesMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::disease);
}

string SetAllDiseaseRatesMenuItem::GetVariableText()
{
    return menustr::emptyString;
}

/////////

DiseaseMaxEventsMenuItem::DiseaseMaxEventsMenuItem(string myKey,UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::diseaseMaxEvents)
{
}

DiseaseMaxEventsMenuItem::~DiseaseMaxEventsMenuItem()
{
}

bool DiseaseMaxEventsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::disease);
}

/////////

DiseaseLocationMenuItem::DiseaseLocationMenuItem(string myKey,UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::diseaseLocation)
{
}

DiseaseLocationMenuItem::~DiseaseLocationMenuItem()
{
}

bool DiseaseLocationMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::disease);
}

///

DiseaseMenu::DiseaseMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::diseaseTitle,lamarcmenu::diseaseInfo)
{
    // EWFIX.P5 DISEASE -- replace display only with commented item below
    // once we resolve how to adjust menu settings when turning
    // disease on/off changes number of cross partitions
    AddMenuItem(new DisplayOnlyMenuItem(uistr::disease,ui));
    //  AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::disease));
    UIId id(force_DISEASE);
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new SetAllDiseaseRatesMenuItem("G",ui));
    AddMenuItem(new MatrixSetMenuItem(ui,
                                      uistr::diseaseInto,
                                      uistr::diseaseByID,
                                      uistr::diseasePartitionCount,
                                      uistr::disease,
                                      force_DISEASE));
    AddMenuItem(new DiseaseMaxEventsMenuItem("M",ui));
    AddMenuItem(new DiseaseLocationMenuItem("L",ui));
}

DiseaseMenu::~DiseaseMenu ()
{
}

//____________________________________________________________________________________
