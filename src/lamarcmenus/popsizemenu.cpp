// $Id: popsizemenu.cpp,v 1.6 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "lamarc_strings.h"
#include "popsizemenu.h"
#include "ui_interface.h"
#include "ui_strings.h"

using std::string;

EffectivePopSizeMenu::EffectivePopSizeMenu(UIInterface & myui)
    : NewMenu(myui,lamarcmenu::effectivePopSizeTitle,
              lamarcmenu::effectivePopSizeInfo)
{
    AddMenuItem(new EffectivePopSizeGroup(ui));
}

EffectivePopSizeMenu::~EffectivePopSizeMenu()
{
}

EffectivePopSizeGroup::EffectivePopSizeGroup(UIInterface& myui)
    : SetMenuItemGroup(myui,uistr::effectivePopSize)
{
}

EffectivePopSizeGroup::~EffectivePopSizeGroup()
{
}

UIIdVec1d
EffectivePopSizeGroup::GetVisibleIds()
{
    vector<UIId> visibleIds;
    LongVec1d regionNumbers = ui.doGetLongVec1d(uistr::regionNumbers);
    LongVec1d::iterator i;
    for(i=regionNumbers.begin(); i != regionNumbers.end(); i++)
    {
        visibleIds.push_back(UIId(*i));
    }
    return visibleIds;
}

string EffectivePopSizeGroup::GetText(UIId id)
{
    return lamarcmenu::effectivePopSizeFor + ui.doGetString(uistr::regionName,id);
}

//____________________________________________________________________________________
