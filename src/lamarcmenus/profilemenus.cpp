// $Id: profilemenus.cpp,v 1.16 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>

#include <string>
#include "lamarc_strings.h"
#include "profilemenus.h"
#include "ui_interface.h"
#include "ui_strings.h"

using std::string;

ProfileMenu::ProfileMenu(UIInterface & myui)
    : NewMenu(myui,lamarcmenu::profileTitle, lamarcmenu::profileInfo)
{
    AddMenuItem(new ToggleMenuItemNoId("A",ui,uistr::allProfilesOn));
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::allProfilesOff));
    AddMenuItem(new ToggleMenuItemNoId("P",ui,uistr::allProfilesPercentile));
    AddMenuItem(new ToggleMenuItemNoId("F",ui,uistr::allProfilesFixed));
    AddMenuItem(new ProfileByForceMenuItemGroup(ui));
}

ProfileMenu::~ProfileMenu()
{
}

ProfileByForceMenuItemGroup::ProfileByForceMenuItemGroup(UIInterface& ui)
    : MenuDisplayGroupBaseImplementation(ui,uistr::profileByForce)
{
}

ProfileByForceMenuItemGroup::~ProfileByForceMenuItemGroup()
{
}

MenuInteraction_ptr
ProfileByForceMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new ProfileMenuForOneForce(ui,id));
}

UIIdVec1d
ProfileByForceMenuItemGroup::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validForces);
}

string
ProfileByForceMenuItemGroup::GetKey(UIId id)
{
    // MDEBUG:  is the absence of the Divergence family a problem here?
    // Use testing seems to say it's okay (you get Migration menu entries for DivMig)
    // but this hasn't been extensively tested.
    switch(id.GetForceType())
    {
        case force_COAL:
            return "T";
            break;
        case force_DISEASE:
            return "D";
            break;
        case force_GROW:
            return "G";
            break;
        case force_MIG:
            return "M";
            break;
        case force_REC:
            return "R";
            break;
        case force_REGION_GAMMA:
            return "L";
            break;
        case force_EXPGROWSTICK:
            return "ES";
            break;
        case force_LOGISTICSELECTION:
            return "S";
            break;
        case force_LOGSELECTSTICK:
            return "LS";
            break;
        default:
            assert(false);              //uncaught force type.
    }
    throw implementation_error("force_type enum missing case in ProfileByForceMenuItemGroup::GetKey");
}

ProfileMenuForOneForce::ProfileMenuForOneForce(UIInterface& ui,UIId id)
    : NewMenu(ui,lamarcmenu::forceProfileTitle+ToString(id.GetForceType()),
              lamarcmenu::forceProfileInfo)
{
    AddMenuItem(new ToggleMenuItemId("A",ui,uistr::oneForceProfilesOn,id));
    AddMenuItem(new ToggleMenuItemId("X",ui,uistr::oneForceProfilesOff,id));
    AddMenuItem(new ToggleMenuItemId("P",ui,uistr::oneForceProfileType,id));
    AddMenuItem(new ToggleMenuItemGroupProfiles(ui,id));
}

ProfileMenuForOneForce::~ProfileMenuForOneForce()
{
}

SubMenuProfileForOneForce::SubMenuProfileForOneForce(std::string key, UIInterface& ui, UIId id)
    : ForceSubMenuItem(key, ui, new ProfileMenuForOneForceCreator(ui, id), id)
{
}

SubMenuProfileForOneForce::~SubMenuProfileForOneForce()
{
}

ToggleMenuItemGroupProfiles::ToggleMenuItemGroupProfiles(UIInterface & ui,UIId id)
    : ToggleMenuItemGroup(ui,uistr::profileByID), m_id(id)
{
}

ToggleMenuItemGroupProfiles::~ToggleMenuItemGroupProfiles()
{
}

UIIdVec1d
ToggleMenuItemGroupProfiles::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validParamsForForce,m_id);
}

//____________________________________________________________________________________
