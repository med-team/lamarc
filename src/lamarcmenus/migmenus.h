// $Id: migmenus.h,v 1.14 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef MIGMENUS_H
#define MIGMENUS_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class SetAllMigrationsMenuItem : public SetMenuItemId
{
  public:
    SetAllMigrationsMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetAllMigrationsMenuItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

class SetMigrationsFstMenuItem : public ToggleMenuItemNoId
{
  public:
    SetMigrationsFstMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetMigrationsFstMenuItem();
    virtual bool IsVisible();
};

class MigrationMaxEventsMenuItem : public SetMenuItemNoId
{
  public:
    MigrationMaxEventsMenuItem(std::string myKey, UIInterface & myui);
    virtual ~MigrationMaxEventsMenuItem();
    virtual bool IsVisible();
};

class MigrationMenu : public NewMenu
{
  public:
    MigrationMenu(UIInterface & myui);
    virtual ~MigrationMenu();
};

class MigrationMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    MigrationMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~MigrationMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new MigrationMenu(ui));};
};

#endif  // MIGMENUS_H

//____________________________________________________________________________________
