// $Id: lamarcmenuitems.h,v 1.25 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef LAMARCMENUITEMS_H_
#define LAMARCMENUITEMS_H_

#include "newmenu.h"
#include "setmenuitem.h"
#include "ui_id.h"
#include "ui_strings.h"

class UIInterface;

class SetMenuItemTempInterval : public SetMenuItemNoId
{
  public:
    SetMenuItemTempInterval(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemTempInterval();
    virtual bool IsVisible();
};

class SetMenuItemHapArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemHapArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemHapArranger();
    virtual bool IsVisible();
};

class SetMenuItemProbHapArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemProbHapArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemProbHapArranger();
    virtual bool IsVisible();
};

class SetMenuItemBayesArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemBayesArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemBayesArranger();
    virtual bool IsVisible();
};

class SetMenuItemEpochArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemEpochArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemEpochArranger();
    virtual bool IsVisible();
};

class SetMenuItemLocusArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemLocusArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemLocusArranger();
    virtual bool IsVisible();
};

class SetMenuItemZilchArranger : public SetMenuItemNoId
{
  public:
    SetMenuItemZilchArranger(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemZilchArranger();
    virtual bool IsVisible();
};

class SetMenuItemCurveFilePrefix : public SetMenuItemNoId
{
  public:
    SetMenuItemCurveFilePrefix(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemCurveFilePrefix();
    virtual bool IsVisible();
};

class SetMenuItemMapFilePrefix : public SetMenuItemNoId
{
  public:
    SetMenuItemMapFilePrefix(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemMapFilePrefix();
    virtual bool IsVisible();
};

class SetMenuItemReclocFiles : public ToggleMenuItemNoId
{
  public:
    SetMenuItemReclocFiles(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemReclocFiles();
    virtual bool IsVisible();
};

class SetMenuItemReclocFilePrefix : public SetMenuItemNoId
{
  public:
    SetMenuItemReclocFilePrefix(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemReclocFilePrefix();
    virtual bool IsVisible();
};

class SetMenuItemTraceFilePrefix : public SetMenuItemNoId
{
  public:
    SetMenuItemTraceFilePrefix(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemTraceFilePrefix();
    virtual bool IsVisible();
};

class SetMenuItemNewickTreeFilePrefix : public SetMenuItemNoId
{
  public:
    SetMenuItemNewickTreeFilePrefix(std::string myKey, UIInterface & myui);
    virtual ~SetMenuItemNewickTreeFilePrefix();
    virtual bool IsVisible();
};

class ToggleMenuItemTempAdapt : public ToggleMenuItemNoId
{
  public:
    ToggleMenuItemTempAdapt(std::string myKey, UIInterface & myui);
    virtual ~ToggleMenuItemTempAdapt();
    virtual bool IsVisible();
};

class ToggleMenuItemUseCurveFiles : public ToggleMenuItemNoId
{
  public:
    ToggleMenuItemUseCurveFiles(std::string myKey, UIInterface & myui);
    virtual ~ToggleMenuItemUseCurveFiles();
    virtual bool IsVisible();
};

class ChainTemperatureMenuItemGroup : public SetMenuItemGroup
{
  public:
    ChainTemperatureMenuItemGroup(UIInterface&);
    ~ChainTemperatureMenuItemGroup();
    virtual vector<UIId> GetVisibleIds();
    virtual string GetText(UIId id);
};

class HeatingMenu : public NewMenu
{
  public:
    HeatingMenu(UIInterface & myui);
    ~HeatingMenu();
};

class HeatingSubMenuItem : public SubMenuItem
{
  public:
    HeatingSubMenuItem(std::string myKey, UIInterface& myui);
    virtual ~HeatingSubMenuItem();
    virtual std::string GetVariableText();
};

class HeatingMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    HeatingMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~HeatingMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new HeatingMenu(ui));};
};

class PriorSubMenuItem : public SubMenuItem
{
  private:
    PriorSubMenuItem(); //undefined.
  public:
    PriorSubMenuItem(std::string myKey, UIInterface& myui);
    virtual ~PriorSubMenuItem();
    virtual bool IsVisible();
};

class RearrangeMenu : public NewMenu
{
  public:
    RearrangeMenu(UIInterface & myui);
    ~RearrangeMenu();
};

class RearrangeMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    RearrangeMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~RearrangeMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new RearrangeMenu(ui));};
};

class SearchMenu : public NewMenu
{
  public:
    SearchMenu(UIInterface & myui);
    virtual ~SearchMenu();
};

class SearchMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    SearchMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~SearchMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new SearchMenu(ui));};
};

class StrategyMenu : public NewMenu
{
  public:
    StrategyMenu(UIInterface & myui);
    ~StrategyMenu();
};

class StrategyMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    StrategyMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~StrategyMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new StrategyMenu(ui));};
};

class ResultMenu : public NewMenu
{
  public:
    ResultMenu(UIInterface & myui);
    ~ResultMenu();
};

class ResultMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    ResultMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~ResultMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new ResultMenu(ui));};
};

class OverviewMenu : public NewMenu
{
  public:
    OverviewMenu(UIInterface & myui);
    ~OverviewMenu();
};

class OverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    OverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~OverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new OverviewMenu(ui));};
};

class SubMenuItemBayesPriors: public SubMenuItem
{
  public:
    SubMenuItemBayesPriors(string myKey, UIInterface& myui);
    ~SubMenuItemBayesPriors();
    virtual bool IsVisible();
};

class SubMenuItemTraitMappingOverview: public SubMenuItem
{
  public:
    SubMenuItemTraitMappingOverview(string myKey, UIInterface& myui);
    ~SubMenuItemTraitMappingOverview();
    virtual bool IsVisible();
};

class SetMenuItemNumSamples: public SetMenuItemNoId
{
  public:
    SetMenuItemNumSamples(string myKey, UIInterface& myui, string myMenuKey);
    ~SetMenuItemNumSamples();
    virtual string GetText();
};

#endif // LAMARCMENUITEMS_H_

//____________________________________________________________________________________
