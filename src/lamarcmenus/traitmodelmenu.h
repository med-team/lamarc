// $Id: traitmodelmenu.h,v 1.6 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TRAITMODELMENU_H
#define TRAITMODELMENU_H

#include "newmenuitems.h"
#include "setmenuitem.h"
#include "ui_regid.h"

class TraitModelItem : public SubMenuItem
{
  public:
    TraitModelItem(string key, UIInterface& ui);
    ~TraitModelItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

class TraitModelsMenu : public NewMenu
{
  public:
    TraitModelsMenu(UIInterface & myui);
    ~TraitModelsMenu();
};

class TraitModelsMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    TraitModelsMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~TraitModelsMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new TraitModelsMenu(ui));};
};

class SubMenuItemsTraitModels : public MenuDisplayGroupBaseImplementation
{
  public:
    SubMenuItemsTraitModels(UIInterface & myui);
    virtual ~SubMenuItemsTraitModels();
    virtual vector<UIId> GetVisibleIds();
    virtual MenuInteraction_ptr GetHandler(std::string input);
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
    virtual string GetText(UIId id);
    virtual string GetKey(UIId id);
    virtual string GetVariableText(UIId id);
};

class SingleTraitModelMenu : public NewMenu
{
  private:
    UIId m_id;
  public:
    SingleTraitModelMenu(UIInterface& ui, UIId id);
    virtual ~SingleTraitModelMenu();
    string Title();
};

class MenuDisplayTraitAnalysisType : public MenuDisplayLine
{
  private:
    MenuDisplayTraitAnalysisType();
    UIRegId m_regId;
    UIInterface & m_ui;
  protected:
    UIId& GetId();
  public:
    MenuDisplayTraitAnalysisType(UIInterface& ui, UIId id);
    virtual ~MenuDisplayTraitAnalysisType();
    virtual std::string GetKey();
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual bool Handles(std::string) {return false;};
};

class MenuDisplayTraitRange : public MenuDisplayLine
{
  private:
    MenuDisplayTraitRange();
    UIRegId m_regId;
    UIInterface & m_ui;
  protected:
    UIId& GetId();
  public:
    MenuDisplayTraitRange(UIInterface& ui, UIId id);
    virtual ~MenuDisplayTraitRange();
    virtual std::string GetKey();
    virtual std::string GetText();
    virtual std::string GetVariableText();
    virtual bool Handles(std::string) {return false;};
};

class AddRangeToTraitModel : public SetMenuItemId
{
  public:
    AddRangeToTraitModel(string key, UIInterface& ui, UIId id);
    virtual ~AddRangeToTraitModel();
    virtual string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string key);
};

class SetAddRangeDialog  : public SetDialog
{
  public:
    SetAddRangeDialog(UIInterface& ui, string key, UIId id);
    virtual ~SetAddRangeDialog();
    virtual string inLoopOutputString();
};

class RemoveRangeFromTraitModel : public SetMenuItemId
{
  public:
    RemoveRangeFromTraitModel(string key, UIInterface& ui, UIId id);
    virtual ~RemoveRangeFromTraitModel();
    virtual string GetVariableText();
    virtual MenuInteraction_ptr GetHandler(std::string key);
};

class TraitModelRangeToPointMenu : public SetMenuItemId
{
  public:
    TraitModelRangeToPointMenu(string key, UIInterface& ui, UIId id);
    virtual ~TraitModelRangeToPointMenu();
    virtual string GetVariableText();
};

class SetRemoveRangeDialog  : public SetDialog
{
  public:
    SetRemoveRangeDialog(UIInterface& ui, string key, UIId id);
    virtual ~SetRemoveRangeDialog();
    virtual string inLoopOutputString();
};

#endif  // TRAITMODELMENU_H

//____________________________________________________________________________________
