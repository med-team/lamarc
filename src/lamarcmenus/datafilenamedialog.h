// $Id: datafilenamedialog.h,v 1.9 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DATAFILENAMEDIALOG_H
#define DATAFILENAMEDIALOG_H

#include <string>
#include "dialogrepeat.h"

class XmlParser;

class DataFileNameDialog : public DialogRepeat
{
  private:
    std::string    m_dataFileName;
    XmlParser &    m_parser;
    std::string    m_errmsg;

  protected:
    virtual long maxTries();
    virtual std::string beforeLoopOutputString();
    virtual std::string inLoopOutputString();
    virtual std::string inLoopFailureOutputString();
    virtual std::string afterLoopSuccessOutputString();
    virtual std::string afterLoopFailureOutputString();
    virtual bool handleInput(std::string input);
    virtual std::string displayFileName();
    virtual void doFailure();

  public:
    DataFileNameDialog(XmlParser &);
    virtual ~DataFileNameDialog();
};

#endif // DATAFILENAMEDIALOG_H

//____________________________________________________________________________________
