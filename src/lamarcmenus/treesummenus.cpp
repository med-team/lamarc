// $Id: treesummenus.cpp,v 1.14 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <fstream>
#include <string>
#include "menu_strings.h"
#include "newmenuitems.h"
#include "togglemenuitem.h"
#include "treesummenus.h"
#include "ui_interface.h"
#include "ui_strings.h"

using std::string;

SetMenuItemTreeSumInFileName::SetMenuItemTreeSumInFileName(string myKey, UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::treeSumInFileName)
{
}

SetMenuItemTreeSumInFileName::~SetMenuItemTreeSumInFileName()
{
}

SetMenuItemTreeSumOutFileName::SetMenuItemTreeSumOutFileName(string myKey, UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::treeSumOutFileName)
{
}

SetMenuItemTreeSumOutFileName::~SetMenuItemTreeSumOutFileName()
{
}

bool SetMenuItemTreeSumInFileName::IsVisible()
{
    return ui.doGetBool(uistr::treeSumInFileEnabled);
}

bool SetMenuItemTreeSumOutFileName::IsVisible()
{
    return ui.doGetBool(uistr::treeSumOutFileEnabled);
}

TreeSumInMenu::TreeSumInMenu(UIInterface & myui)
    : NewMenu(myui,uistr::treeSumInFileEnabled,menustr::emptyString)
{
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::treeSumInFileEnabled));
    AddMenuItem(new SetMenuItemTreeSumInFileName("N",ui));
}

TreeSumInMenu::~TreeSumInMenu()
{
}

TreeSumOutMenu::TreeSumOutMenu(UIInterface & myui)
    : NewMenu(myui,uistr::treeSumOutFileEnabled,menustr::emptyString)
{
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::treeSumOutFileEnabled));
    AddMenuItem(new SetMenuItemTreeSumOutFileName("N",ui));
}

TreeSumOutMenu::~TreeSumOutMenu()
{
}

TreeSumInSubMenuItem::TreeSumInSubMenuItem(string myKey, UIInterface & myui)
    : SubMenuItem(myKey, myui, new TreeSumInMenuCreator(myui))
{
}

TreeSumOutSubMenuItem::TreeSumOutSubMenuItem(string myKey, UIInterface & myui)
    : SubMenuItem(myKey, myui, new TreeSumOutMenuCreator(myui))
{
}

string TreeSumInSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::treeSumInFileEnabled,GetId());
}

string TreeSumOutSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::treeSumOutFileEnabled,GetId());
}

//____________________________________________________________________________________
