// $Id: outfilemenus.cpp,v 1.22 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "lamarc_strings.h"
#include "newmenuitems.h"
#include "outfilemenus.h"
#include "profilemenus.h"
#include "ui_interface.h"
#include "ui_strings.h"

using std::string;

OutfileMenu::OutfileMenu(UIInterface & myui)
    : NewMenu(myui,lamarcmenu::outfileTitle)
{
    AddMenuItem(new SetMenuItemNoId("N",ui,uistr::resultsFileName));
    AddMenuItem(new ToggleMenuItemNoId("V",ui,uistr::verbosity));
    AddMenuItem(new SubMenuItem("P", ui,new ProfileMenuCreator(ui)));
}

OutfileMenu::~OutfileMenu()
{
}

OutfileSubMenuItem::OutfileSubMenuItem(std::string myKey, UIInterface & myui)
    : SubMenuItem(myKey, myui,new OutfileMenuCreator(myui))
{
}

std::string OutfileSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::resultsFileName,GetId());
}

//____________________________________________________________________________________
