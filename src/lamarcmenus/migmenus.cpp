// $Id: migmenus.cpp,v 1.14 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "matrixitem.h"
#include "migmenus.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "setmenuitem.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "overviewmenus.h"
#include "profilemenus.h"

using std::string;

//------------------------------------------------------------------------------------

SetAllMigrationsMenuItem::SetAllMigrationsMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemId(myKey,myui,uistr::globalMigration, UIId(force_MIG, uiconst::GLOBAL_ID))
{
}

SetAllMigrationsMenuItem::~SetAllMigrationsMenuItem()
{
}

bool SetAllMigrationsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::migration);
}

string SetAllMigrationsMenuItem::GetVariableText()
{
    return "";
}

//------------------------------------------------------------------------------------

SetMigrationsFstMenuItem::SetMigrationsFstMenuItem(string key,UIInterface & ui)
    : ToggleMenuItemNoId(key,ui,uistr::fstSetMigration)
{
}

SetMigrationsFstMenuItem::~SetMigrationsFstMenuItem()
{
}

bool SetMigrationsFstMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::migration);
}

//------------------------------------------------------------------------------------

MigrationMaxEventsMenuItem::MigrationMaxEventsMenuItem(string myKey,UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::migrationMaxEvents)
{
}

MigrationMaxEventsMenuItem::~MigrationMaxEventsMenuItem()
{
}

bool MigrationMaxEventsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::migration);
}

///

MigrationMenu::MigrationMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::migTitle,lamarcmenu::migInfo)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::migration, ui));
    UIId id(force_MIG);
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new SetAllMigrationsMenuItem("G",ui));
    AddMenuItem(new SetMigrationsFstMenuItem("F",ui));
    AddMenuItem(new MatrixSetMenuItem(ui,
                                      uistr::migrationInto,
                                      uistr::migrationUser,
                                      uistr::migrationPartitionCount,
                                      uistr::migration,
                                      force_MIG));

    AddMenuItem(new MigrationMaxEventsMenuItem("M",ui));
}

MigrationMenu::~MigrationMenu ()
{
}

//____________________________________________________________________________________
