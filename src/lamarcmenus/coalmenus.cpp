// $Id: coalmenus.cpp,v 1.16 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "coalmenus.h"
#include "forcesummary.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "priormenus.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "profilemenus.h"
using std::string;

//------------------------------------------------------------------------------------

SetAllThetasMenuItem::SetAllThetasMenuItem(string myKey, UIInterface & ui)
    : SetMenuItemId(myKey,ui,uistr::globalTheta, UIId(force_COAL, uiconst::GLOBAL_ID))
{
}

SetAllThetasMenuItem::~SetAllThetasMenuItem()
{
}

bool SetAllThetasMenuItem::IsVisible()
{
    return (ui.doGetLong(uistr::crossPartitionCount) > 1);
}

string SetAllThetasMenuItem::GetVariableText()
{
    return "";
}

/////////

SetThetasFstMenuItem::SetThetasFstMenuItem(string key,UIInterface & ui)
    : ToggleMenuItemNoId(key,ui,uistr::fstSetTheta)
{
}

SetThetasFstMenuItem::~SetThetasFstMenuItem()
{
}

bool SetThetasFstMenuItem::IsVisible()
{
    return (ui.doGetLong(uistr::crossPartitionCount) > 1);
}

/////////

SetMenuItemThetas::SetMenuItemThetas(UIInterface & myui)
    : SetMenuItemGroup(myui,uistr::userSetTheta)
{
}

SetMenuItemThetas::~SetMenuItemThetas()
{
}

vector<UIId> SetMenuItemThetas::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validParamsForForce,UIId(force_COAL));
}

/////////

CoalescenceMenu::CoalescenceMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::coalTitle,lamarcmenu::coalInfo)
{
    UIId id(force_COAL);
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new SetAllThetasMenuItem("G",ui));
    AddMenuItem(new SetThetasFstMenuItem("F",ui));
    AddMenuItem(new ToggleMenuItemNoId("W",ui,uistr::wattersonSetTheta));
    AddMenuItem(new SetMenuItemThetas(ui));
}

CoalescenceMenu::~CoalescenceMenu ()
{
}

//____________________________________________________________________________________
