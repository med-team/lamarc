// $Id: lamarcheaderdialog.cpp,v 1.18 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>

// Defines various symbols used to control debugging of experimental code blocks.
#include "local_build.h"

#include "lamarcheaderdialog.h"
#include "runreport.h"           // Added only for DebuggingOptionsXxxxxx() functions below.

using std::string;

LamarcHeaderDialog::LamarcHeaderDialog()
    : DialogNoInput()
{
}

LamarcHeaderDialog::~LamarcHeaderDialog()
{
}

string LamarcHeaderDialog::outputString()
{
    string msg(string("       L A M A R C      Likelihood Analysis with               \n")
               + string("                        Metropolis-Hastings Algorithms         \n")
               + string("                        using Random Coalescences              \n")
               + string("                        Version  ") + VERSION
#ifndef NDEBUG
               + string(" (Debug)")
#endif
               + string("\n")
               + string("                        Release Date: ") + RELEASE_DATE + string("\n")
               + string("---------------------------------------------------------------------------\n"));

    // Appends indication of any debugging options which may different from the "usual" configuration.
    if (DebuggingOptionsRunning())
    {
        // This variable exists only to give DebuggingOptionsString() something to increment (by reference),
        // since that function needs to update a linecount when called in the context of printing an output file.
        unsigned long int current_linecount = 0;
        msg += DebuggingOptionsString(current_linecount);
    }

    return msg;
}

//------------------------------------------------------------------------------------
// These are tools for marking output (both header printed on screen at LAMARC startup
// and beginning of output file "outfile.txt") to indicate unambiguously whether any
// "unusual" debugging options are activated.  If all the relevant pre-processor flags
// are all in their usual states, no extra strings are printed.
//
// "Unusual" options include:
//     Final Coalescence Optimization is disabled (for comparison with "enabled" mode).
//       Final Coalescence being enabled is the "normal" state, indicated by no string of its own
//       and a false boolean (unless some other "unusual" state is active).
//     STATIONARIES is defined (data are ignored and trees are generated "from scratch").
//     DENOVO is defined (trees are generated fresh each time rather than being re-arranged).
//     ALL_ARRANGERS_DENOVO is defined (all arrangers work "de novo"; if OFF, only Bayesian arrangers do so).
//       This is indicated only in the string output.  The boolean output is already TRUE if DENOVO
//       is defined, and ALL_ARRANGERS_DENOVO is relevant only when DENOVO is defined.
//     DEBUG build.
//
// These options can be activated/deactivated via the "configure" program in the build process.
// They can also be set/unset directly in the file "local_build.h" in the config subdirectory
// of the directory in which LAMARC is being built.
//
// If you edit either function, be sure to make parallel changes in the other.
//
// Prototypes for these two functions are in "runreport.h" simply because it is already included
// by almost all files in which these functions appear.

// Returns a TRUE indication if any "unusual" debugging option is activated (logical OR of "unusualness").
// If all conditions are in their USUAL state, output is FALSE.
//
bool DebuggingOptionsRunning()
{
    bool unusual_flags(false);

#ifndef NDEBUG
    unusual_flags = true;
#endif

#if ! FINAL_COALESCENCE_ON
    unusual_flags = true;
#endif

#ifdef STATIONARIES
    unusual_flags = true;
#endif

#ifdef DENOVO
    unusual_flags = true;
#endif

    return unusual_flags;
}

// Returns a string describing "unusual" debugging options.
// The string is multi-line (each terminated by a newline), with one line for each "unusual" condition.
// The ALL_ARRANGERS_DENOVO string is conditionally present only if the DENOVO state is also active.
// If NO unusual states are active, the output string is the NULL string.
//
string DebuggingOptionsString(unsigned long int & current_linecount)
{
    // We want a blank line before the output appears (this function is called only if output WILL appear).
    // But for some reason, the leading Newline does not appear unless the first line returned is non-empty.
    string msg(" \n");
    ++current_linecount;

#ifndef NDEBUG
    msg += "DEBUG mode is ON.\n";
    ++current_linecount;
#endif

#if ! FINAL_COALESCENCE_ON
    msg += "Final Coalescence optimization is OFF.\n";
    ++current_linecount;
#endif

#ifdef STATIONARIES
    msg += "STATIONARIES is ON.\n";
    ++current_linecount;
#endif

#ifdef DENOVO
    msg += "DENOVO is ON.\n";
    ++current_linecount;
#ifdef ALL_ARRANGERS_DENOVO
    msg += "ALL_ARRANGERS_DENOVO is ON.\n";
    ++current_linecount;
#endif
#endif

    return msg;
}

//____________________________________________________________________________________
