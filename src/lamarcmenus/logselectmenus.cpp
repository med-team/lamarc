// $Id: logselectmenus.cpp,v 1.5 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "lamarc_strings.h"
#include "newmenuitems.h"
#include "forcesmenus.h"
#include "logselectmenus.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "profilemenus.h"

LogisticSelectionCoefficientMenuItem::LogisticSelectionCoefficientMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemId(myKey,myui,uistr::logisticSelectionCoefficient, UIId(force_LOGISTICSELECTION, uiconst::GLOBAL_ID))
{
}

LogisticSelectionCoefficientMenuItem::~LogisticSelectionCoefficientMenuItem()
{
}

bool LogisticSelectionCoefficientMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::logisticSelection);
}

LogisticSelectionMenu::LogisticSelectionMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::logisticSelectionTitle,lamarcmenu::logisticSelectionInfo)
{
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::logisticSelection));
    UIId id(force_LOGISTICSELECTION);
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new LogisticSelectionCoefficientMenuItem("S",ui));
}

LogisticSelectionMenu::~LogisticSelectionMenu ()
{
}

//____________________________________________________________________________________
