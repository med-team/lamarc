// $Id: nomenufilereaddialog.cpp,v 1.5 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>

#include "defaults.h"
#include "nomenufilereaddialog.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "xml.h"

using std::string;

NoMenuFileReadDialog::NoMenuFileReadDialog(XmlParser & parser)
    : DialogNoInput(), m_parser(parser)
{
}

NoMenuFileReadDialog::~NoMenuFileReadDialog()
{
}

string NoMenuFileReadDialog::outputString()
{
    return
        string("---------------------------------------------------------------------------\n")
        +string("** Menu-less version generated with \"configure --disable-menu\"\n")
        +string("** For menu re-run \"configure --enable-menu\" and recompile\n")
        +string("---------------------------------------------------------------------------\n")
        +string("** Reading data from \"")
        +m_parser.GetFileName()
        +string("\" and writing to its default outfile\n")
        +string("---------------------------------------------------------------------------\n");

}

void
NoMenuFileReadDialog::performAction()
{
    m_parser.ParseFileData(m_parser.GetFileName());
}

//____________________________________________________________________________________
