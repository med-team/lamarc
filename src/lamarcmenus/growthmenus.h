// $Id: growthmenus.h,v 1.17 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef GROWTHMENUS_H
#define GROWTHMENUS_H

#include <string>
#include <vector>
#include "newmenu.h"
#include "setmenuitem.h"
#include "newmenuitems.h"

class UIInterface;

class SetAllGrowthMenuItem : public SetMenuItemId
{
  public:
    SetAllGrowthMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetAllGrowthMenuItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

class SetMenuItemGrowths : public SetMenuItemGroup
{
  public:
    SetMenuItemGrowths(UIInterface & ui);
    virtual ~SetMenuItemGrowths();
    virtual std::vector<UIId> GetVisibleIds();
};

class ToggleMenuItemGrowthScheme : public ToggleMenuItemNoId
{
  public:
    ToggleMenuItemGrowthScheme(std::string myKey, UIInterface& myui);
    virtual ~ToggleMenuItemGrowthScheme();
    virtual bool IsVisible();
};

class ToggleMenuItemGrowthType : public ToggleMenuItemNoId
{
  public:
    ToggleMenuItemGrowthType(std::string myKey, UIInterface& myui);
    virtual ~ToggleMenuItemGrowthType();
    virtual bool IsVisible();
};

class GrowthMenu : public NewMenu
{
  public:
    GrowthMenu(UIInterface & myui);
    virtual ~GrowthMenu();
};

class GrowthMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    GrowthMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~GrowthMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new GrowthMenu(ui));};
};

#endif  // GROWTHMENUS_H

//____________________________________________________________________________________
