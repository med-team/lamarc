// $Id: overviewmenus.h,v 1.17 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef OVERVIEWMENUS_H
#define OVERVIEWMENUS_H

#include <string>
#include "menudefs.h"
#include "newmenu.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "twodtable.h"

class Display;
class UIInterface;

class ChainTable : public TwoDTable
{
  protected:
    virtual long ColCount(UIInterface & ui);
    virtual long RowCount(UIInterface & ui);
    virtual std::string Title(UIInterface & ui);
    virtual std::string ColLabel(UIInterface & ui, long index);
    virtual std::string RowLabel(UIInterface & ui, long index);
    virtual std::string Cell(UIInterface & ui, long rowIndex, long colIndex);
  public:
    ChainTable(UIInterface &);
    virtual ~ChainTable();
};

class ForceEventTable : public TwoDTable
{
  protected:
    virtual const std::string forceName(long);
    virtual const std::string forceMaxEvents(long);
    virtual long getForceIndex(UIInterface & ui, long index);
    virtual long ColCount(UIInterface & ui);
    virtual long RowCount(UIInterface & ui);
    virtual std::string Title(UIInterface & ui);
    virtual std::string ColLabel(UIInterface & ui, long index);
    virtual std::string RowLabel(UIInterface & ui, long index);
    virtual std::string Cell(UIInterface & ui, long rowIndex, long colIndex);
  public:
    ForceEventTable(UIInterface &);
    virtual ~ForceEventTable();
};

class StartParamTable : public TwoDTable
{
  protected:
    virtual long ColCount(UIInterface & ui);
    virtual long RowCount(UIInterface & ui);
    virtual std::string Title(UIInterface & ui);
    virtual std::string RowHeader(UIInterface & ui);
    virtual std::string ColLabel(UIInterface & ui, long index);
    virtual std::string RowLabel(UIInterface & ui, long index);
    virtual std::string Cell(UIInterface & ui, long rowIndex, long colIndex);
  public:
    StartParamTable(UIInterface &);
    virtual ~StartParamTable();
};

class TemperatureTable : public TwoDTable
{
  protected:
    virtual long ColCount(UIInterface & ui);
    virtual long RowCount(UIInterface & ui);
    virtual std::string Title(UIInterface & ui);
    virtual std::string ColLabel(UIInterface & ui, long index);
    virtual std::string RowLabel(UIInterface & ui, long index);
    virtual std::string Cell(UIInterface & ui, long rowIndex, long colIndex);
  public:
    TemperatureTable(UIInterface &);
    virtual ~TemperatureTable();
    virtual bool IsVisible();
};

class DisplayOnlyMenuItem : public OutputOnlyMenuItem
{
  protected:
    UIInterface & ui;
    UIId keyId;
  public:
    DisplayOnlyMenuItem(const std::string &,UIInterface&);
    DisplayOnlyMenuItem(const std::string &,UIInterface&,UIId);
    virtual ~DisplayOnlyMenuItem();
    virtual UIId GetKeyid();
    virtual std::string GetText();
    virtual std::string GetVariableText();
};

class DisplayOnlyMenuItemIfTrue : public DisplayOnlyMenuItem
{
  protected:
    std::string guard;
  public:
    DisplayOnlyMenuItemIfTrue(
        const std::string &,
        const std::string &,
        UIInterface &);
    DisplayOnlyMenuItemIfTrue(
        const std::string &,
        const std::string &,
        UIInterface &,
        UIId);
    virtual bool IsVisible();
    virtual ~DisplayOnlyMenuItemIfTrue();
};

class DisplayOnlyMenuItemIfNonZero : public DisplayOnlyMenuItem
{
  public:
    DisplayOnlyMenuItemIfNonZero(
        const std::string &,
        UIInterface &);
    virtual ~DisplayOnlyMenuItemIfNonZero();
    virtual bool IsVisible();
};

class DisplayOnlyMenu : public NewMenu
{
  public:
    DisplayOnlyMenu(UIInterface & myui, const std::string &,const std::string &);
    virtual ~DisplayOnlyMenu();
};

class FileOverviewMenu : public DisplayOnlyMenu
{
  public:
    FileOverviewMenu(UIInterface & myui);
    virtual ~FileOverviewMenu();
};

class FileOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    FileOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~FileOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new FileOverviewMenu(ui));};
};

class DataOverviewMenuItem : public OutputOnlyMenuItem
{
  protected:
    UIInterface & ui;
    UIId            m_id;
  public:
    DataOverviewMenuItem(UIInterface & myui,UIId id);
    virtual ~DataOverviewMenuItem();
    virtual std::string GetText();
};

class RegionDataOverviewMenu : public DisplayOnlyMenu
{
  protected:
    UIId    m_id;
  public:
    RegionDataOverviewMenu(UIInterface & ui,UIId id);
    virtual ~RegionDataOverviewMenu();
};

class DataOverviewMenuItemGroup :   public MenuDisplayGroupBaseImplementation
{
  protected:
    MenuInteraction_ptr MakeOneHandler(UIId id);
  public:
    DataOverviewMenuItemGroup(UIInterface & ui);
    ~DataOverviewMenuItemGroup();
    vector<UIId> GetVisibleIds();
    std::string GetText(UIId id);
    std::string GetVariableText(UIId id);
};

class DataOverviewMenu : public DisplayOnlyMenu
{
  public:
    DataOverviewMenu(UIInterface & ui);
    virtual ~DataOverviewMenu();
};

class DataOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    DataOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~DataOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new DataOverviewMenu(ui));};
};

class ForceOverviewMenu : public DisplayOnlyMenu
{
  public:
    ForceOverviewMenu(UIInterface & myui);
    virtual ~ForceOverviewMenu();
};

class ForceOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    ForceOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~ForceOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new ForceOverviewMenu(ui));};
};

class ReplicatesMenuItemIfMoreThanOne : public DisplayOnlyMenuItem
{
  public:
    ReplicatesMenuItemIfMoreThanOne(UIInterface&);
    virtual ~ReplicatesMenuItemIfMoreThanOne();
    virtual bool IsVisible();
};

class SearchOverviewMenu : public DisplayOnlyMenu
{
  public:
    SearchOverviewMenu(UIInterface & myui);
    virtual ~SearchOverviewMenu();
};

class SearchOverviewMenuCreator : public NewMenuCreator {
  protected:
    UIInterface & ui;
  public:
    SearchOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~SearchOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new SearchOverviewMenu(ui));};
};

class ResultsOverviewMenu : public DisplayOnlyMenu
{
  public:
    ResultsOverviewMenu(UIInterface & myui);
    virtual ~ResultsOverviewMenu();
};

class ResultsOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    ResultsOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~ResultsOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new ResultsOverviewMenu(ui));};
};

class BayesianPriorsOverviewMenu : public DisplayOnlyMenu
{
  public:
    BayesianPriorsOverviewMenu(UIInterface & myui);
    ~BayesianPriorsOverviewMenu();
};

class BayesianPriorsOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    BayesianPriorsOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~BayesianPriorsOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new BayesianPriorsOverviewMenu(ui));};
};

//------------------------------------------------------------------------------------

class BayesianPriorsOverviewMenuItemGroup : public MenuDisplayGroupBaseImplementation
{
  public:
    BayesianPriorsOverviewMenuItemGroup(UIInterface& ui);
    virtual ~BayesianPriorsOverviewMenuItemGroup();
    virtual vector<UIId> GetVisibleIds();
    virtual string GetText(UIId id);
    virtual string GetVariableText(UIId id);
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
};

//------------------------------------------------------------------------------------

class TraitMappingOverviewMenu : public DisplayOnlyMenu
{
  public:
    TraitMappingOverviewMenu(UIInterface & myui);
    ~TraitMappingOverviewMenu();
};

class TraitMappingOverviewMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    TraitMappingOverviewMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~TraitMappingOverviewMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new TraitMappingOverviewMenu(ui));};
};

//------------------------------------------------------------------------------------

class TraitMappingOverviewMenuItemGroup : public MenuDisplayGroupBaseImplementation
{
  public:
    TraitMappingOverviewMenuItemGroup(UIInterface& ui);
    virtual ~TraitMappingOverviewMenuItemGroup();
    virtual vector<UIId> GetVisibleIds();
    virtual string GetText(UIId id);
    virtual string GetVariableText(UIId id);
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
};

#endif  // OVERVIEWMENUS_H

//____________________________________________________________________________________
