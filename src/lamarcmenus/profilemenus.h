// $Id: profilemenus.h,v 1.5 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PROFILEMENUS_H
#define PROFILEMENUS_H

#include <string>
#include "forcesmenus.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

// choices for profile type and on/off for individual quantities
class ProfileMenu : public NewMenu
{
  public:
    ProfileMenu(UIInterface & myui);
    ~ProfileMenu();
};

class ProfileMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    ProfileMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~ProfileMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new ProfileMenu(ui));};
};

class ProfileByForceMenuItemGroup : public MenuDisplayGroupBaseImplementation
{
  protected:
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
  public:
    ProfileByForceMenuItemGroup(UIInterface& ui);
    virtual ~ProfileByForceMenuItemGroup();
    virtual UIIdVec1d GetVisibleIds();
    virtual std::string GetKey(UIId id);
};

class ProfileMenuForOneForce : public NewMenu
{
  public:
    ProfileMenuForOneForce(UIInterface &ui,UIId id);
    virtual ~ProfileMenuForOneForce();
};

//These two classes are used for the individual forces' menus.
class SubMenuProfileForOneForce : public ForceSubMenuItem
{
  private:
    SubMenuProfileForOneForce(); //undefined
    UIId id;
  public:
    SubMenuProfileForOneForce(std::string key, UIInterface& ui, UIId id);
    virtual ~SubMenuProfileForOneForce();
};

class ProfileMenuForOneForceCreator : public NewMenuCreator
{
  private:
    ProfileMenuForOneForceCreator(); //undefined
    UIInterface& ui;
    UIId id;
  public:
    ProfileMenuForOneForceCreator(UIInterface& myui, UIId myid) :
        ui(myui), id(myid) {};
    virtual ~ProfileMenuForOneForceCreator() {};
    virtual NewMenu_ptr Create() { return NewMenu_ptr(new ProfileMenuForOneForce(ui, id));};
};

class ToggleMenuItemGroupProfiles : public ToggleMenuItemGroup
{
  private:
    UIId    m_id;
  public:
    ToggleMenuItemGroupProfiles(UIInterface & ui,UIId id);
    virtual ~ToggleMenuItemGroupProfiles();
    virtual UIIdVec1d GetVisibleIds();
};

#endif  // PROFILEMENUS_H

//____________________________________________________________________________________
