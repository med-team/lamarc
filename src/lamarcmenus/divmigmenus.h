// $Id: divmigmenus.h,v 1.2 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DIVMIGMENUS_H
#define DIVMIGMENUS_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class SetAllDivMigsMenuItem : public SetMenuItemId
{
  public:
    SetAllDivMigsMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetAllDivMigsMenuItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

class SetDivMigsFstMenuItem : public ToggleMenuItemNoId
{
  public:
    SetDivMigsFstMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetDivMigsFstMenuItem();
    virtual bool IsVisible();
};

class DivMigMaxEventsMenuItem : public SetMenuItemNoId
{
  public:
    DivMigMaxEventsMenuItem(std::string myKey, UIInterface & myui);
    virtual ~DivMigMaxEventsMenuItem();
    virtual bool IsVisible();
};

class DivMigMenu : public NewMenu
{
  public:
    DivMigMenu(UIInterface & myui);
    virtual ~DivMigMenu();
};

class DivMigMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    DivMigMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~DivMigMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new DivMigMenu(ui));};
};

#endif  // DIVMIGMENUS_H

//____________________________________________________________________________________
