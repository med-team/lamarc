// $Id: priormenus.cpp,v 1.14 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING

#include "lamarc_strings.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "togglemenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"

using std::string;

//------------------------------------------------------------------------------------

PriorMenu::PriorMenu(UIInterface & myui)
    : NewMenu(myui,lamarcmenu::priorTitle)
{
    AddMenuItem(new PriorByForceMenuItemGroup(ui));
}

PriorMenu::~PriorMenu()
{
}

PriorByForceMenuItemGroup::PriorByForceMenuItemGroup(UIInterface& ui)
    : MenuDisplayGroupBaseImplementation(ui,uistr::priorByForce)
{
}

PriorByForceMenuItemGroup::~PriorByForceMenuItemGroup()
{
}

MenuInteraction_ptr
PriorByForceMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new PriorMenuForOneForce(ui,id));
}

UIIdVec1d
PriorByForceMenuItemGroup::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validForces);
}

string
PriorByForceMenuItemGroup::GetKey(UIId id)
{
    switch(id.GetForceType())
    {
        case force_COAL:
            return "T";
            break;
        case force_DISEASE:
            return "D";
            break;
        case force_GROW:
            return "G";
            break;
        case force_MIG:
        case force_DIVMIG:
            return "M";
            break;
        case force_REC:
            return "R";
            break;
        case force_REGION_GAMMA:
            return "L";
            break;
        case force_EXPGROWSTICK:
            return "X";
            break;
        case force_LOGISTICSELECTION:
            return "S";
            break;
        case force_LOGSELECTSTICK:
            return "H";
            break;
        case force_DIVERGENCE:
            return "V";
            break;
        default:
            assert(false);              //uncaught force type.
    }
    throw implementation_error("force_type enum missing case in PriorByForceMenuItemGroup::GetKey");
}

PriorMenuForOneForce::PriorMenuForOneForce(UIInterface& ui,UIId id)
    : NewMenu(ui,lamarcmenu::priorTitle + lamarcmenu::wordFor
              +ToString(id.GetForceType()), lamarcmenu::priorInfoForForce)
{
    AddMenuItem(new ToggleMenuItemId("U",ui,uistr::useDefaultPriorsForForce,id));
    AddMenuItem(new DefaultPriorForForce("D", ui, id));
    AddMenuItem(new PriorByParameterMenuItemGroup(ui,id));
}

PriorMenuForOneForce::~PriorMenuForOneForce()
{
}

SubMenuPriorForOneForce::SubMenuPriorForOneForce(std::string key, UIInterface& ui, UIId id)
    : ForceSubMenuItem(key, ui, new PriorMenuForOneForceCreator(ui, id), id)
{
}

SubMenuPriorForOneForce::~SubMenuPriorForOneForce()
{
}

bool SubMenuPriorForOneForce::IsVisible()
{
    return (ForceSubMenuItem::IsVisible() && ui.doGetBool(uistr::bayesian));
}

PriorByParameterMenuItemGroup::PriorByParameterMenuItemGroup(UIInterface & ui,UIId id)
    : MenuDisplayGroupBaseImplementation(ui,uistr::priorByID),
      m_id(id)
{
}

PriorByParameterMenuItemGroup::~PriorByParameterMenuItemGroup()
{
}

UIIdVec1d
PriorByParameterMenuItemGroup::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validParamsForForce,m_id);
}

MenuInteraction_ptr
PriorByParameterMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new PriorMenuForOneParameter(ui,id));
}

DefaultPriorForForce::DefaultPriorForForce(string myKey, UIInterface& myUI,
                                           UIId myId)
    : SubMenuItem(myKey, myUI, new PriorMenuForOneParameterCreator
                  (myUI, UIId(myId.GetForceType(), uiconst::GLOBAL_ID))),
      m_id(myId)
{
}

DefaultPriorForForce::~DefaultPriorForForce()
{
}

string DefaultPriorForForce::GetVariableText()
{
    return ui.GetCurrentVars().forces.GetPriorTypeSummaryDescription(m_id.GetForceType(), uiconst::GLOBAL_ID);
}

//One possible parameter is the 'default' parameter for a particular force.
PriorMenuForOneParameter::PriorMenuForOneParameter(UIInterface& ui, UIId id)
    : NewMenu(ui, lamarcmenu::priorTitle, lamarcmenu::priorInfoForParam),
      m_id(id)
{
    if (id.GetIndex1() != uiconst::GLOBAL_ID)
    {
        AddMenuItem( new ToggleMenuItemId("D",ui,uistr::priorUseDefault,id));
    }
    AddMenuItem(new ToggleMenuItemId("S",ui,uistr::priorType,id));
    AddMenuItem(new SetMenuItemId("U",ui,uistr::priorUpperBound,id));
    AddMenuItem(new SetMenuItemId("L",ui,uistr::priorLowerBound,id));
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    AddMenuItem(new SetMenuItemId("R",ui,uistr::relativeSampleRate,id));
#endif
}

PriorMenuForOneParameter::~PriorMenuForOneParameter()
{
}

string PriorMenuForOneParameter::Title()
{
    if (m_id.GetIndex1() == uiconst::GLOBAL_ID)
    {
        return lamarcmenu::defaultForParamsFor + ToString(m_id.GetForceType());
    }
    else
    {
        return lamarcmenu::priorTitle + lamarcmenu::wordFor +
            ui.GetCurrentVars().GetParamNameWithConstraint(m_id.GetForceType(), m_id.GetIndex1());
    }
}

//____________________________________________________________________________________
