// $Id: divmenus.h,v 1.3 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DIVMENUS_H
#define DIVMENUS_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class SetMenuItemEpochs : public SetMenuItemGroup
{
  public:
    SetMenuItemEpochs(UIInterface & myui);
    virtual ~SetMenuItemEpochs();
    virtual std::vector<UIId> GetVisibleIds();
    virtual bool HasMultiLineItems() {return true;};
    std::vector<std::string> GetExtraText(UIId id);
    std::vector<std::string> GetExtraVariableText(UIId id);
};

class DivergenceMenu : public NewMenu
{
  public:
    DivergenceMenu(UIInterface & myui);
    virtual ~DivergenceMenu();
};

class DivergenceMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    DivergenceMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~DivergenceMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new DivergenceMenu(ui));};
};

#endif  // DIVMENUS_H

//____________________________________________________________________________________
