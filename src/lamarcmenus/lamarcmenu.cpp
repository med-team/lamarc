// $Id: lamarcmenu.cpp,v 1.23 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// the base menu is the NON-Interactive "menu"
// derived menus handele the different windowing systems
//  (- Curses)
//  - Scrolling ASCII console
//  (-graphical GUI etc)
// Peter Beerli

#include <string>
#include "datamodelmenu.h"
#include "forcesmenus.h"
#include "lamarcmenu.h"
#include "lamarcmenuitems.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "newmenu.h"
#include "newmenuitems.h"
#include "overviewmenus.h"
#include "ui_interface.h"

//------------------------------------------------------------------------------------

LamarcMainMenu::LamarcMainMenu (UIInterface & myui)
    : NewMenu (myui,lamarcmenu::mainTitle,menustr::emptyString,true)
{
    AddMenuItem(new SubMenuItem(string("D"),ui,new NewDataModelMenuCreator(ui)));
    AddMenuItem(new SubMenuItem(string("A"),ui,new ForcesMenuCreator(ui)));
    AddMenuItem(new SubMenuItem(string("S"),ui,new StrategyMenuCreator(ui)));
    AddMenuItem(new SubMenuItem(string("I"),ui,new ResultMenuCreator(ui)));
    AddMenuItem(new SubMenuItem(string("O"),ui,new OverviewMenuCreator(ui)));

}

LamarcMainMenu::~LamarcMainMenu ()
{
}

//____________________________________________________________________________________
