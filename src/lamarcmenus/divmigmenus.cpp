// $Id: divmigmenus.cpp,v 1.3 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "matrixitem.h"
#include "divmigmenus.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "setmenuitem.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "overviewmenus.h"
#include "profilemenus.h"

using std::string;

//------------------------------------------------------------------------------------

SetAllDivMigsMenuItem::SetAllDivMigsMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemId(myKey,myui,uistr::globalDivMigration, UIId(force_DIVMIG, uiconst::GLOBAL_ID))
{
}

SetAllDivMigsMenuItem::~SetAllDivMigsMenuItem()
{
}

bool SetAllDivMigsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::divmigration);
}

string SetAllDivMigsMenuItem::GetVariableText()
{
    return "";
}
//------------------------------------------------------------------------------------
// don't know how to do this with divergence yet

#if 0

SetDivMigsFstMenuItem::SetDivMigsFstMenuItem(string key,UIInterface & ui)
    : ToggleMenuItemNoId(key,ui,uistr::fstSetMigration)
{
}

SetDivMigsFstMenuItem::~SetDivMigsFstMenuItem()
{
}

bool SetDivMigsFstMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::divmigration);
}

#endif

//------------------------------------------------------------------------------------

DivMigMaxEventsMenuItem::DivMigMaxEventsMenuItem(string myKey,UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::divmigrationMaxEvents)
{
}

DivMigMaxEventsMenuItem::~DivMigMaxEventsMenuItem()
{
}

bool DivMigMaxEventsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::divmigration);
}

///

DivMigMenu::DivMigMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::divMigTitle,lamarcmenu::divMigInfo)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::divmigration, ui));
    UIId id(force_DIVMIG);

    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new SetAllDivMigsMenuItem("G",ui));
    //AddMenuItem(new SetDivMigsFstMenuItem("F",ui)); // not currently functional

    AddMenuItem(new MatrixSetMenuItem(ui,
                                      uistr::divmigrationInto,
                                      uistr::divmigrationUser,
                                      uistr::divmigrationPartitionCount,
                                      uistr::divmigration,
                                      force_DIVMIG));

    AddMenuItem(new DivMigMaxEventsMenuItem("M",ui));


}

DivMigMenu::~DivMigMenu ()
{
}

//____________________________________________________________________________________
