// $Id: divmenus.cpp,v 1.7 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "matrixitem.h"
#include "divmenus.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "setmenuitem.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "overviewmenus.h"
#include "profilemenus.h"

using std::string;
//------------------------------------------------------------------------------------

SetMenuItemEpochs::SetMenuItemEpochs(UIInterface & myui)
    : SetMenuItemGroup(myui,uistr::divergenceEpochBoundaryTime)
{
}

SetMenuItemEpochs::~SetMenuItemEpochs()
{
}

vector<UIId> SetMenuItemEpochs::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validParamsForForce,UIId(force_DIVERGENCE));
}

std::vector<std::string> SetMenuItemEpochs::GetExtraText(UIId id)
{
    std::vector<std::string> s;
    string ancestor = "  ";
    ancestor += ui.doGetDescription(uistr::divergenceEpochAncestor,id);
    s.push_back(ancestor);
    string decendents = "  ";
    decendents += ui.doGetDescription(uistr::divergenceEpochDescendents,id);
    s.push_back(decendents);
    return s;
}

std::vector<std::string> SetMenuItemEpochs::GetExtraVariableText(UIId id)
{
    std::vector<std::string> s;
    s.push_back(ui.doGetPrintString(uistr::divergenceEpochAncestor,id));
    s.push_back(ui.doGetPrintString(uistr::divergenceEpochDescendents,id));
    return s;
}


//------------------------------------------------------------------------------------

DivergenceMenu::DivergenceMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::divTitle,lamarcmenu::divInfo)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::divergence, ui));
    UIId id(force_DIVERGENCE);

    AddMenuItem(new SetMenuItemEpochs(ui));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
}

DivergenceMenu::~DivergenceMenu ()
{
}

//____________________________________________________________________________________
