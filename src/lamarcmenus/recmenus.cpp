// $Id: recmenus.cpp,v 1.12 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "lamarc_strings.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "recmenus.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "profilemenus.h"

RecombineMaxEventsMenuItem::RecombineMaxEventsMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemNoId(myKey,myui,uistr::recombinationMaxEvents)
{
}

RecombineMaxEventsMenuItem::~RecombineMaxEventsMenuItem()
{
}

bool RecombineMaxEventsMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::recombination);
}

RecombineRateMenuItem::RecombineRateMenuItem(string myKey, UIInterface & myui)
    : SetMenuItemId(myKey,myui,uistr::recombinationRate, UIId(force_REC, uiconst::GLOBAL_ID))
{
}

RecombineRateMenuItem::~RecombineRateMenuItem()
{
}

bool RecombineRateMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::recombination);
}

RecombinationMenu::RecombinationMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::recTitle,lamarcmenu::recInfo)
{
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::recombination));
    UIId id(force_REC);
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new RecombineRateMenuItem("S",ui));
    AddMenuItem(new RecombineMaxEventsMenuItem("M",ui));
}

RecombinationMenu::~RecombinationMenu ()
{
}

//____________________________________________________________________________________
