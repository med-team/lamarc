// $Id: overviewmenus.cpp,v 1.35 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>
#include <iostream>

#include "display.h"
#include "lamarc_strings.h"
#include "menuitem.h"
#include "menu_strings.h"
#include "overviewmenus.h"
#include "profilemenus.h"
#include "ui_regid.h"
#include "stringx.h"
#include "twodtable.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"

using std::string;

//------------------------------------------------------------------------------------

string ChainTable::Title(UIInterface &)
{
    return "Chain parameters:";
}

long ChainTable::ColCount(UIInterface &)
{
    return 3;
}

long ChainTable::RowCount(UIInterface &)
{
    return 4;
}

string ChainTable::ColLabel(UIInterface &, long index)
{
    if (index == 0) return menustr::emptyString;
    if (index == 1) return menustr::initial;
    if (index == 2) return menustr::final;
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

string ChainTable::RowLabel(UIInterface &, long index)
{
    if(index == 0) return menustr::chains;
    if(index == 1) return menustr::discard;
    if(index == 2) return menustr::interval;
    if(index == 3) return menustr::samples;
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

string ChainTable::Cell(UIInterface &, long rowIndex, long colIndex)
{
    if (colIndex == 0) return menustr::emptyString;
    if (colIndex == 1)
    {
        if(rowIndex == 0) return ui.doGetPrintString(uistr::initialChains);
        if(rowIndex == 1) return ui.doGetPrintString(uistr::initialDiscard);
        if(rowIndex == 2) return ui.doGetPrintString(uistr::initialInterval);
        if(rowIndex == 3) return ui.doGetPrintString(uistr::initialSamples);
        assert(false); //Uncaught index.
    }
    if (colIndex==2)
    {
        if(rowIndex == 0) return ui.doGetPrintString(uistr::finalChains);
        if(rowIndex == 1) return ui.doGetPrintString(uistr::finalDiscard);
        if(rowIndex == 2) return ui.doGetPrintString(uistr::finalInterval);
        if(rowIndex == 3) return ui.doGetPrintString(uistr::finalSamples);
        assert(false); //Uncaught index.
    }
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

ChainTable::ChainTable(UIInterface & myui)
    : TwoDTable(myui)
{
}

ChainTable::~ChainTable()
{
}

//------------------------------------------------------------------------------------

const string ForceEventTable::forceName(long i)
{
    static const string forceNames[6] = {
        uistr::coalescence,
        uistr::disease,
        uistr::growth,
        uistr::migration,
        uistr::recombination,
        uistr::logisticSelection
    };
    if(i < 0) return menustr::emptyString;
    if(i > 5) return menustr::emptyString;
    return forceNames[i];
}

const string ForceEventTable::forceMaxEvents(long i)
{
    static const string forceMaxEventCount[6] = {
        uistr::coalescenceMaxEvents,
        uistr::diseaseMaxEvents,
        uistr::growthMaxEvents,
        uistr::migrationMaxEvents,
        uistr::recombinationMaxEvents,
        uistr::logisticSelectionMaxEvents
    };
    if(i < 0) return menustr::emptyString;
    if(i > 5) return menustr::emptyString;
    return forceMaxEventCount[i];
}

string ForceEventTable::Title(UIInterface &)
{
    return "Enabled Forces:";
}

long ForceEventTable::ColCount(UIInterface &)
{
    return 1;
}

long ForceEventTable::RowCount(UIInterface &)
{
    return (ui.GetCurrentVars().forces.GetActiveForces().size());
}

string ForceEventTable::ColLabel(UIInterface &, long index)
{
    if(index == 0) return uistr::maxEvents;
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

long ForceEventTable::getForceIndex(UIInterface & ui,long index)
{
    long countDown = index;
    for(long i=0; i < 6; i++)
    {
        if(ui.doGetBool(forceName(i))) countDown--;
        if(countDown < 0) return i;
    }
    return -1;
}

string ForceEventTable::RowLabel(UIInterface & ui, long index)
{
    long forceIndex = getForceIndex(ui,index);
    if(forceIndex < 0) return menustr::emptyString;
    if(forceIndex > 5) return menustr::emptyString;
    return ui.doGetDescription(forceName(forceIndex));
}

string ForceEventTable::Cell(UIInterface & ui, long rowIndex, long colIndex)
{
    if(colIndex != 0) return menustr::emptyString;
    long forceIndex = getForceIndex(ui,rowIndex);
    if(forceIndex < 0) return menustr::emptyString;
    if(forceIndex > 5) return menustr::emptyString;
    return ui.doGetPrintString(forceMaxEvents(forceIndex));
}

ForceEventTable::ForceEventTable(UIInterface & myui)
    : TwoDTable(myui)
{
}

ForceEventTable::~ForceEventTable()
{
}

//------------------------------------------------------------------------------------
// assuming only one disease present

string StartParamTable::Title(UIInterface &)
{
    return "Start parameters";
}

string StartParamTable::RowHeader(UIInterface &)
{
    return "Population";
}

long StartParamTable::ColCount(UIInterface &)
{
    return 6;
}

long StartParamTable::RowCount(UIInterface & ui)
{
    return ui.doGetLong(uistr::crossPartitionCount);
}

string StartParamTable::ColLabel(UIInterface & ui, long index)
{
    if(index == 0)
    {
        if(ui.doGetBool(uistr::disease))
        {
            return "disease status";
        }
    }
    if(index == 1) return "Theta";
    if(index == 2)
    {
        if(ui.doGetBool(uistr::growth))
        {
            return "Growth";
        }
    }
    if(index == 3)
    {
        if(ui.doGetBool(uistr::migration) && (RowCount(ui) > 0))
        {
            return "M=m/mu";
        }
    }
    if(index == 4)
    {
        if(ui.doGetBool(uistr::disease))
        {
            return "disease Mu rate";
        }
    }
    return menustr::emptyString;
}

string StartParamTable::RowLabel(UIInterface & ui, long index)
{
    long diseaseDivisor = ui.doGetLong(uistr::diseasePartitionCount);
    if (diseaseDivisor == 0) diseaseDivisor = 1;
    if(index % diseaseDivisor == 0)
    {
        return ui.doGetPrintString(uistr::migrationPartitionName,index / diseaseDivisor);
    }
    return menustr::emptyString;
}

string StartParamTable::Cell(UIInterface & ui, long rowIndex, long colIndex)
{
    if(colIndex == 0)
    {
        if(ui.doGetBool(uistr::disease))
        {
            long disIndex = rowIndex % ui.doGetLong(uistr::diseasePartitionCount);
            return ui.doGetString(uistr::diseasePartitionName,disIndex);
        }
    }
    if(colIndex == 1)
    {
        return Pretty(ui.doGetDouble(uistr::userSetTheta,rowIndex), 8);
    }
    if((colIndex == 2) && ui.doGetBool(uistr::growth))
    {
        return ui.doGetPrintString(uistr::growthByID,rowIndex);
    }
    if((colIndex == 3) && ui.doGetBool(uistr::migration))
    {
        long diseaseDivisor = ui.doGetLong(uistr::diseasePartitionCount);
        if (diseaseDivisor == 0) diseaseDivisor = 1;
        if(rowIndex % diseaseDivisor == 0)
        {
            return ui.doGetPrintString(uistr::migrationInto,rowIndex / diseaseDivisor);
        }
    }
    if((colIndex == 4) && ui.doGetBool(uistr::disease))
    {
        if(rowIndex < ui.doGetLong(uistr::diseasePartitionCount))
        {
            return ui.doGetPrintString(uistr::diseaseInto,rowIndex);
        }
    }
    return menustr::emptyString;
}

StartParamTable::StartParamTable(UIInterface & myui)
    : TwoDTable(myui)
{
}

StartParamTable::~StartParamTable()
{
}

//------------------------------------------------------------------------------------

long TemperatureTable::ColCount(UIInterface &)
{
    return 1;
}

long TemperatureTable::RowCount(UIInterface &)
{
    return 4;
}

string TemperatureTable::Title(UIInterface &)
{
    return "Parallel Markov Chain info:";
}

string TemperatureTable::ColLabel(UIInterface &, long)
{
    return menustr::emptyString;
}

string TemperatureTable::RowLabel(UIInterface & ui, long index)
{
    if(index == 0) return ui.doGetDescription(uistr::heatedChainCount);
    if(index == 1) return ui.doGetDescription(uistr::heatedChains);
    if(index == 2) return ui.doGetDescription(uistr::tempInterval);
    if(index == 3) return ui.doGetDescription(uistr::tempAdapt);
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

string TemperatureTable::Cell(UIInterface & ui, long rowIndex, long colIndex)
{
    if(colIndex != 0) return menustr::emptyString;
    if(rowIndex == 0) return ui.doGetPrintString(uistr::heatedChainCount);
    if(rowIndex == 1)
    {
        string retstring = ui.doGetPrintString(uistr::heatedChains);
        while (retstring.find(" ") == 0)
        {
            retstring.erase(0,1);
        }
        return retstring;
    }
    if(rowIndex == 2) return ui.doGetPrintString(uistr::tempInterval);
    if(rowIndex == 3) return ui.doGetPrintString(uistr::tempAdapt);
    assert(false); //Uncaught index.
    return menustr::emptyString;
}

TemperatureTable::TemperatureTable(UIInterface & myui)
    : TwoDTable(myui)
{
}

TemperatureTable::~TemperatureTable()
{
}

bool TemperatureTable::IsVisible()
{
    return (ui.doGetLong(uistr::heatedChainCount) > 1);
}

//------------------------------------------------------------------------------------

DisplayOnlyMenuItem::DisplayOnlyMenuItem(const string & uiVariable,UIInterface & myui)
    : OutputOnlyMenuItem(uiVariable),ui(myui),keyId(NO_ID())
{
}

DisplayOnlyMenuItem::DisplayOnlyMenuItem(const string & uiVariable,UIInterface & myui, UIId myKeyId)
    : OutputOnlyMenuItem(uiVariable),ui(myui),keyId(myKeyId)
{
}

DisplayOnlyMenuItem::~DisplayOnlyMenuItem()
{
}

UIId DisplayOnlyMenuItem::GetKeyid()
{
    return keyId;
}

string DisplayOnlyMenuItem::GetText()
{
    return ui.doGetDescription(displayString,GetKeyid());
}

string DisplayOnlyMenuItem::GetVariableText()
{
    return ui.doGetPrintString(displayString,GetKeyid());
}

//------------------------------------------------------------------------------------

DisplayOnlyMenuItemIfTrue::DisplayOnlyMenuItemIfTrue(
    const string & uiVariable,
    const string & myguard,
    UIInterface & myui, UIId myKeyId)
    : DisplayOnlyMenuItem(uiVariable,myui,myKeyId), guard(myguard)
{
}

DisplayOnlyMenuItemIfTrue::DisplayOnlyMenuItemIfTrue(
    const string & uiVariable,
    const string & myguard,
    UIInterface & myui)
    : DisplayOnlyMenuItem(uiVariable,myui), guard(myguard)
{
}

DisplayOnlyMenuItemIfTrue::~DisplayOnlyMenuItemIfTrue()
{
}

bool DisplayOnlyMenuItemIfTrue::IsVisible()
{
    return ui.doGetBool(guard);
}

//------------------------------------------------------------------------------------

DisplayOnlyMenuItemIfNonZero::DisplayOnlyMenuItemIfNonZero(
    const string & uiVariable,
    UIInterface & myui)
    : DisplayOnlyMenuItem(uiVariable,myui)
{
}

DisplayOnlyMenuItemIfNonZero::~DisplayOnlyMenuItemIfNonZero()
{
}

bool DisplayOnlyMenuItemIfNonZero::IsVisible()
{
    return (ui.doGetDouble(displayString) != 0.0);
}

//------------------------------------------------------------------------------------

DisplayOnlyMenu::DisplayOnlyMenu(
    UIInterface & myui, const string & myTitle, const string & myInfo)
    : NewMenu(myui,myTitle,string(lamarcmenu::overviewInfo+myInfo))
{
}

DisplayOnlyMenu::~DisplayOnlyMenu()
{
}

//------------------------------------------------------------------------------------

FileOverviewMenu::FileOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::fileOverviewTitle,
        lamarcmenu::resultTitle)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::dataFileName,myui));
    AddMenuItem(new DisplayOnlyMenuItem(uistr::resultsFileName,myui));
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::treeSumInFileName,uistr::treeSumInFileEnabled, myui));
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::treeSumOutFileName,uistr::treeSumOutFileEnabled, myui));
    AddMenuItem(new DisplayOnlyMenuItem(uistr::xmlOutFileName,myui));
    if (ui.doGetBool(uistr::bayesian))
    {
        AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::curveFilePrefix,uistr::useCurveFiles,myui));
    }
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::reclocFilePrefix,uistr::useReclocFiles,myui));
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::traceFilePrefix,uistr::useTraceFiles,myui));
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(uistr::newickTreeFilePrefix,uistr::useNewickTreeFiles,myui));

}

FileOverviewMenu::~FileOverviewMenu()
{
}

//------------------------------------------------------------------------------------

RegionDataOverviewMenu::RegionDataOverviewMenu(UIInterface& ui, UIId id)
    : DisplayOnlyMenu(ui,lamarcmenu::dataOverviewTitle,lamarcmenu::dataTitle), m_id(id)
{
    AddMenuItem(new DataOverviewMenuItem(ui,m_id));
}

RegionDataOverviewMenu::~RegionDataOverviewMenu()
{
}

MenuInteraction_ptr
DataOverviewMenuItemGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new RegionDataOverviewMenu(ui,id));
}

DataOverviewMenuItemGroup::DataOverviewMenuItemGroup(UIInterface & ui)
    : MenuDisplayGroupBaseImplementation(ui,menustr::emptyString)
{
}

DataOverviewMenuItemGroup::~DataOverviewMenuItemGroup()
{
}

vector<UIId>
DataOverviewMenuItemGroup::GetVisibleIds()
{
    vector<UIId> visibleIds;
    LongVec1d regionNumbers = ui.doGetLongVec1d(uistr::regionNumbers);
    LongVec1d::iterator i;
    for(i=regionNumbers.begin(); i != regionNumbers.end(); i++)
    {
        visibleIds.push_back(UIId(*i));
    }
    return visibleIds;
}

string
DataOverviewMenuItemGroup::GetText(UIId id)
{
    string retString("Model and parameters for ");
    retString += ui.doGetString(uistr::regionName,id.GetIndex1());
    return retString;
}

string
DataOverviewMenuItemGroup::GetVariableText(UIId)
{
    //return ToString(ui.doGetModelType(uistr::dataModel,id));
    return menustr::emptyString;
}

string DataOverviewMenuItem::GetText()
{
    string strings;

    long regionNumber = m_id.GetIndex1();
    string regionName = ui.doGetString(uistr::regionName,regionNumber);

    LongVec1d lociNumbers = ui.doGetLongVec1d(uistr::lociNumbers,regionNumber);
    LongVec1d::iterator j;
    for(j=lociNumbers.begin(); j != lociNumbers.end(); j++)
    {

        UIId thisId(regionNumber,*j);
        model_type dataModel = ui.doGetModelType(uistr::dataModel,thisId);
        string locusName = ui.doGetString(uistr::locusName,thisId);
        strings += "Parameters of a ";
        strings += ToString(dataModel);
        strings += " model for segment ";
        strings += locusName;
        strings += "\n";
        StringVec1d report =
            ui.doGetStringVec1d(uistr::dataModelReport,thisId);
        StringVec1d::iterator i;
        for(i=report.begin(); i != report.end(); i++)
        {
            strings += "    " + *i + "\n";
        }
    }
    return strings;
}

DataOverviewMenuItem::DataOverviewMenuItem(UIInterface & myui,UIId id)
    : OutputOnlyMenuItem(menustr::emptyString), ui(myui), m_id(id)
{
}

DataOverviewMenuItem::~DataOverviewMenuItem()
{
}

DataOverviewMenu::DataOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::dataOverviewTitle,
        lamarcmenu::dataTitle)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::randomSeed, myui));
    AddMenuItem(new DataOverviewMenuItemGroup(ui));
}

DataOverviewMenu::~DataOverviewMenu()
{
}

//------------------------------------------------------------------------------------

ForceOverviewMenu::ForceOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::forcesOverviewTitle,
        lamarcmenu::forcesTitle)
{
    AddMenuItem(new ForceEventTable(myui));
    AddMenuItem(new BlankMenuItem());
    AddMenuItem(new StartParamTable(myui));
    AddMenuItem(new BlankMenuItem());
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(
                    uistr::recombinationRate,uistr::recombination,
                    myui, UIId(force_REC, uiconst::GLOBAL_ID)));
    AddMenuItem(new DisplayOnlyMenuItemIfTrue(
                    uistr::regionGammaShape,uistr::regionGamma,
                    myui, UIId(force_REGION_GAMMA, uiconst::GLOBAL_ID)));

}

ForceOverviewMenu::~ForceOverviewMenu()
{
}

//------------------------------------------------------------------------------------

ReplicatesMenuItemIfMoreThanOne::ReplicatesMenuItemIfMoreThanOne(UIInterface & myui)
    : DisplayOnlyMenuItem(uistr::replicates,myui)
{
}

ReplicatesMenuItemIfMoreThanOne::~ReplicatesMenuItemIfMoreThanOne()
{
}

bool ReplicatesMenuItemIfMoreThanOne::IsVisible()
{
    return (ui.doGetLong(uistr::replicates) > 1);
}

SearchOverviewMenu::SearchOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::strategyOverviewTitle,
        lamarcmenu::strategyTitle)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::bayesian, myui));
    AddMenuItem(new DisplayOnlyMenuItemIfNonZero(uistr::dropArranger, myui));
    AddMenuItem(new DisplayOnlyMenuItemIfNonZero(uistr::sizeArranger, myui));
    AddMenuItem(new DisplayOnlyMenuItemIfNonZero(uistr::hapArranger, myui));
    AddMenuItem(new DisplayOnlyMenuItemIfNonZero(uistr::bayesArranger, myui));
    AddMenuItem(new ReplicatesMenuItemIfMoreThanOne(myui));
    AddMenuItem(new BlankMenuItem());
    AddMenuItem(new ChainTable(myui));
    AddMenuItem(new BlankMenuItem());
    AddMenuItem(new TemperatureTable(myui));

}

SearchOverviewMenu::~SearchOverviewMenu()
{
}

//------------------------------------------------------------------------------------

ResultsOverviewMenu::ResultsOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::resultOverviewTitle,
        lamarcmenu::resultTitle)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::progress,myui));
    AddMenuItem(new DisplayOnlyMenuItem(uistr::verbosity,myui));
    AddMenuItem(new DisplayOnlyMenuItem(uistr::treeSumInFileEnabled, myui));
    AddMenuItem(new DisplayOnlyMenuItem(uistr::treeSumOutFileEnabled, myui));
    AddMenuItem(new DisplayOnlyGroupWrapper(new ProfileByForceMenuItemGroup(myui)));

}

ResultsOverviewMenu::~ResultsOverviewMenu()
{
}

BayesianPriorsOverviewMenu::BayesianPriorsOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::bayesianPriorsOverviewTitle,
        lamarcmenu::strategyTitle)
{
    AddMenuItem(new DisplayOnlyGroupWrapper(new BayesianPriorsOverviewMenuItemGroup(myui)));
}

BayesianPriorsOverviewMenu::~BayesianPriorsOverviewMenu()
{
}

//------------------------------------------------------------------------------------

BayesianPriorsOverviewMenuItemGroup::BayesianPriorsOverviewMenuItemGroup(UIInterface & ui)
    : MenuDisplayGroupBaseImplementation(ui,menustr::emptyString)
{
}

BayesianPriorsOverviewMenuItemGroup::~BayesianPriorsOverviewMenuItemGroup()
{
}

vector<UIId>
BayesianPriorsOverviewMenuItemGroup::GetVisibleIds()
{
    vector<UIId> allParams;
    vector<UIId> forceIds = ui.doGetUIIdVec1d(uistr::validForces);
    for (vector<UIId>::iterator force  = forceIds.begin(); force != forceIds.end(); force++)
    {
        vector<UIId> paramIds = ui.doGetUIIdVec1d(uistr::validParamsForForce,*force);
        allParams.insert(allParams.end(), paramIds.begin(), paramIds.end());
    }
    return allParams;
}

string
BayesianPriorsOverviewMenuItemGroup::GetText(UIId id)
{
    return ui.GetCurrentVars().GetParamNameWithConstraint(id.GetForceType(),id.GetIndex1());
}

string
BayesianPriorsOverviewMenuItemGroup::GetVariableText(UIId id)
{
    return ui.GetCurrentVars().forces.GetPriorTypeSummaryDescription(id.GetForceType(),id.GetIndex1(), false);
}

MenuInteraction_ptr BayesianPriorsOverviewMenuItemGroup::MakeOneHandler(UIId)
{
    return MenuInteraction_ptr(new DoNothingHandler());
}

//------------------------------------------------------------------------------------

TraitMappingOverviewMenu::TraitMappingOverviewMenu(UIInterface & myui)
    : DisplayOnlyMenu(
        myui,
        lamarcmenu::traitMappingOverviewTitle,
        lamarcmenu::forcesTitle)
{
    AddMenuItem(new DisplayOnlyGroupWrapper(new TraitMappingOverviewMenuItemGroup(myui)));
}

TraitMappingOverviewMenu::~TraitMappingOverviewMenu()
{
}

TraitMappingOverviewMenuItemGroup::TraitMappingOverviewMenuItemGroup(UIInterface & ui)
    : MenuDisplayGroupBaseImplementation(ui,menustr::emptyString)
{
}

TraitMappingOverviewMenuItemGroup::~TraitMappingOverviewMenuItemGroup()
{
}

vector<UIId>
TraitMappingOverviewMenuItemGroup::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validMovingLoci,UIId());
}

string
TraitMappingOverviewMenuItemGroup::GetText(UIId id)
{
    UIRegId regId(id, ui.GetCurrentVars());
    return lamarcmenu::traitModel1 + ui.doGetString(uistr::locusName, id)
        + lamarcmenu::traitModel2
        + ToString(ui.GetCurrentVars().traitmodels.GetRange(regId));
    ;
}

string
TraitMappingOverviewMenuItemGroup::GetVariableText(UIId id)
{
    UIRegId regId(id, ui.GetCurrentVars());
    return ToString(ui.GetCurrentVars().traitmodels.GetAnalysisType(regId));
}

MenuInteraction_ptr TraitMappingOverviewMenuItemGroup::MakeOneHandler(UIId)
{
    return MenuInteraction_ptr(new DoNothingHandler());
}

//____________________________________________________________________________________
