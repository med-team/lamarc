// $Id: forcesmenus.cpp,v 1.31 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>

#include "coalmenus.h"
#include "constraintmenus.h"
#include "diseasemenus.h"
#include "forcesmenus.h"
#include "growthmenus.h"
#include "lamarc_strings.h"
#include "logselectmenus.h"
#include "menu_strings.h"
#include "migmenus.h"
#include "divmenus.h"
#include "divmigmenus.h"
#include "newmenuitems.h"
#include "recmenus.h"
#include "regiongammamenus.h"
#include "togglemenuitem.h"
#include "traitmodelmenu.h"
#include "ui_interface.h"
#include "ui_strings.h"

//------------------------------------------------------------------------------------

ForcesSubMenuItem::ForcesSubMenuItem(
    std::string myKey,
    UIInterface & myui,
    std::string myForce,
    std::string myForceLegal,
    NewMenuCreator * mySubMenuCreator)
    :   SubMenuItem(myKey,myui,mySubMenuCreator),
        force(myForce),
        forceLegal(myForceLegal)
{
}

ForcesSubMenuItem::~ForcesSubMenuItem()
{
}

std::string ForcesSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(force);
}

bool ForcesSubMenuItem::IsVisible()
{
    return ui.doGetBool(forceLegal);
}

ForcesMenu::ForcesMenu (UIInterface & myui)
    : NewMenu (myui,lamarcmenu::forcesTitle,menustr::emptyString)
{
    AddMenuItem(new ForcesSubMenuItem(string("T"),
                                      ui,
                                      uistr::coalescence,
                                      uistr::coalescenceLegal,
                                      new CoalescenceMenuCreator(ui)));
    AddMenuItem(new ForcesSubMenuItem(string("G"),
                                      ui,
                                      uistr::growth,
                                      uistr::growthLegal,
                                      new GrowthMenuCreator(ui)));
    AddMenuItem(new ForcesSubMenuItem(string("M"),
                                      ui,
                                      uistr::migration,
                                      uistr::migrationLegal,
                                      new MigrationMenuCreator(ui)));

    AddMenuItem(new ForcesSubMenuItem(string("I"),
                                      ui,
                                      uistr::divergence,
                                      uistr::divergenceLegal,
                                      new DivergenceMenuCreator(ui)));

    AddMenuItem(new ForcesSubMenuItem(string("E"),
                                      ui,
                                      uistr::divmigration,
                                      uistr::divmigrationLegal,
                                      new DivMigMenuCreator(ui)));

    AddMenuItem(new ForcesSubMenuItem(string("R"),
                                      ui,
                                      uistr::recombination,
                                      uistr::recombinationLegal,
                                      new RecombinationMenuCreator(ui)));
    AddMenuItem(new ForcesSubMenuItem(string("D"),
                                      ui,
                                      uistr::disease,
                                      uistr::diseaseLegal,
                                      new DiseaseMenuCreator(ui)));
    AddMenuItem(new ForcesSubMenuItem(string("V"),
                                      ui,
                                      uistr::regionGamma,
                                      uistr::regionGammaLegal,
                                      new RegionGammaMenuCreator(ui)));
#ifndef NDEBUG
    AddMenuItem(new ForcesSubMenuItem(string("S"),
                                      ui,
                                      uistr::logisticSelection,
                                      uistr::logisticSelectionLegal,
                                      new LogisticSelectionMenuCreator(ui)));
#endif
#if 0
    AddMenuItem(new ForcesSubMenuItem(string("Z"),
                                      ui,
                                      uistr::logSelectStick,
                                      uistr::logisticSelectionLegal,
                                      new StochasticSelectionMenuCreator(ui)));
#endif
    AddMenuItem(new TraitModelItem(string("A"),ui));
}

ForcesMenu::~ForcesMenu ()
{
}

ForceSubMenuItem::ForceSubMenuItem(std::string myKey,UIInterface & myui, NewMenuCreator * myCreator, UIId myId)
    : SubMenuItem(myKey, myui, myCreator),
      m_id(myId)
{
}

ForceSubMenuItem::~ForceSubMenuItem()
{
}

bool ForceSubMenuItem::IsVisible()
{
    switch(m_id.GetForceType())
    {
        case force_COAL:
            return true;
            break;
        case force_MIG:
            return ui.doGetBool(uistr::migration);
            break;
        case force_DISEASE:
            return ui.doGetBool(uistr::disease);
            break;
        case force_REC:
            return ui.doGetBool(uistr::recombination);
            break;
        case force_GROW:
            return ui.doGetBool(uistr::growth);
            break;
        case force_LOGISTICSELECTION:
            return ui.doGetBool(uistr::logisticSelection);
            break;
        case force_REGION_GAMMA:
            return ui.doGetBool(uistr::regionGamma);
            break;
        case force_EXPGROWSTICK:
            return ui.doGetBool(uistr::expGrowStick);
            break;
        case force_LOGSELECTSTICK:
            return ui.doGetBool(uistr::logSelectStick);
            break;
        case force_DIVERGENCE:
            return ui.doGetBool(uistr::divergence);
            break;
        case force_DIVMIG:
            return ui.doGetBool(uistr::divmigration);
            break;
        default:
#if 0
            string err = "ForceSubMenuItem::IsVisible() did not find force " + m_id.GetForceType();
            implementation_error e(err);
            throw e;
#endif
            assert(false);              //uncaught force type.
    }
    return true;
}

//____________________________________________________________________________________
