// $Id: priormenus.h,v 1.4 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PRIORMENUS_H
#define PRIORMENUS_H

#include <string>
#include "forcesmenus.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class PriorMenu : public NewMenu
{
  private:
    PriorMenu(); //undefined
  public:
    PriorMenu(UIInterface& ui);
    virtual ~PriorMenu();
};

class PriorMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    PriorMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~PriorMenuCreator() {};
    virtual NewMenu_ptr Create() { return NewMenu_ptr(new PriorMenu(ui));};
};

class PriorByForceMenuItemGroup : public MenuDisplayGroupBaseImplementation
{
  private:
    PriorByForceMenuItemGroup(); //undefined
  public:
    PriorByForceMenuItemGroup(UIInterface& ui);
    virtual ~PriorByForceMenuItemGroup();
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
    virtual UIIdVec1d           GetVisibleIds();
    virtual string              GetKey(UIId id);
};

class PriorMenuForOneForce : public NewMenu
{
  private:
    PriorMenuForOneForce(); //undefined
  public:
    PriorMenuForOneForce(UIInterface& ui, UIId id);
    virtual ~PriorMenuForOneForce();
};

//These two classes are used for the individual forces' menus.
class SubMenuPriorForOneForce : public ForceSubMenuItem
{
  private:
    SubMenuPriorForOneForce(); //undefined
    UIId id;
  public:
    SubMenuPriorForOneForce(std::string key, UIInterface& ui, UIId id);
    virtual ~SubMenuPriorForOneForce();
    virtual bool IsVisible();
};

class PriorMenuForOneForceCreator : public NewMenuCreator
{
  private:
    PriorMenuForOneForceCreator(); //undefined
    UIInterface& ui;
    UIId id;
  public:
    PriorMenuForOneForceCreator(UIInterface& myui, UIId myid) :
        ui(myui), id(myid) {};
    virtual ~PriorMenuForOneForceCreator() {};
    virtual NewMenu_ptr Create() { return NewMenu_ptr(new PriorMenuForOneForce(ui, id));};
};

class PriorByParameterMenuItemGroup : public MenuDisplayGroupBaseImplementation
{
  private:
    PriorByParameterMenuItemGroup(); //undefined
    UIId m_id;
  public:
    PriorByParameterMenuItemGroup(UIInterface& ui, UIId id);
    virtual ~PriorByParameterMenuItemGroup();
    virtual UIIdVec1d  GetVisibleIds();
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
};

class DefaultPriorForForce: public SubMenuItem
{
  private:
    DefaultPriorForForce();
    UIId m_id;
  public:
    DefaultPriorForForce(string myKey, UIInterface& myUI, UIId myId);
    virtual ~DefaultPriorForForce();
    virtual string GetVariableText();
};

class PriorMenuForOneParameter : public NewMenu
{
  private:
    PriorMenuForOneParameter();
    UIId m_id;
  public:
    PriorMenuForOneParameter(UIInterface& ui, UIId id);
    virtual ~PriorMenuForOneParameter();
    virtual string Title();
};

class PriorMenuForOneParameterCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
    UIId id;
  public:
    PriorMenuForOneParameterCreator(UIInterface & myui, UIId myid) :
        ui(myui), id(myid) {};
    virtual ~PriorMenuForOneParameterCreator() {};
    virtual NewMenu_ptr Create() { return NewMenu_ptr(new PriorMenuForOneParameter(ui, id));};
};

#endif  // PRIORMENUS_H

//____________________________________________________________________________________
