// $Id: lamarc_strings.h,v 1.33 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef LAMARC_STRINGS_H
#define LAMARC_STRINGS_H

#include <string>

class lamarcmenu
{
  public:
    static const std::string addRangeDialog;
    static const std::string bayesianPriorsOverviewTitle;
    static const std::string baseFrequenciesTitle;
    static const std::string calcPerLocus;
    static const std::string calculated;
    static const std::string categoriesTitle;
    static const std::string categoriesInfo;
    static const std::string categoryTitle;
    static const std::string coalInfo;
    static const std::string coalTitle;
    static const std::string dataTitle;
    static const std::string dataModelTitle;
    static const std::string dataOverviewTitle;
    static const std::string defaultForParamsFor;
    static const std::string diseaseInfo;
    static const std::string diseaseTitle;
    static const std::string divInfo;
    static const std::string divTitle;
    static const std::string divMigInfo;
    static const std::string divMigTitle;
    static const std::string effectivePopSizeFor;
    static const std::string effectivePopSizeTitle;
    static const std::string effectivePopSizeInfo;
    static const std::string fileOverviewTitle;
    static const std::string forGlobalNuc;
    static const std::string forGlobalMsat;
    static const std::string forGlobalKAllele;
    static const std::string forcesTitle;
    static const std::string forcesOverviewTitle;
    static const std::string forceProfileTitle;
    static const std::string forceProfileInfo;
    static const std::string forceConstraintTitle;
    static const std::string forceConstraintInfo;
    static const std::string regionGammaInfo;
    static const std::string regionGammaTitle;
    static const std::string globalModel;
    static const std::string globalDataModelNuc;
    static const std::string globalDataModelMsat;
    static const std::string globalDataModelKAllele;
    static const std::string growInfo;
    static const std::string growTitle;
    static const std::string gtrRatesTitle;
    static const std::string heatingInfo;
    static const std::string heatingTitle;
    static const std::string logisticSelectionInfo;
    static const std::string logisticSelectionTitle;
    static const std::string mainTitle;
    static const std::string migInfo;
    static const std::string migTitle;
    static const std::string multiLocusCount_0;
    static const std::string multiLocusCount_1;
    static const std::string outfileTitle;
    static const std::string overviewInfo;
    static const std::string overviewTitle;
    static const std::string parenLeft;
    static const std::string parenRight;
    static const std::string profileTitle;
    static const std::string profileInfo;
    static const std::string priorTitle;
    static const std::string priorInfo;
    static const std::string priorInfoForForce;
    static const std::string priorInfoForParam;
    static const std::string individualProfileTitle;
    static const std::string rearrangeTitle;
    static const std::string rearrangeInfo;
    static const std::string recInfo;
    static const std::string recTitle;
    static const std::string regionDataModelTitle;
    static const std::string removeRangeDialog;
    static const std::string resultOverviewTitle;
    static const std::string resultTitle;
    static const std::string searchTitle;
    static const std::string singleTraitModelInfo;
    static const std::string strategyOverviewTitle;
    static const std::string strategyTitle;
    static const std::string traitAnalysisType;
    static const std::string traitsTitle;
    static const std::string traitMappingOverviewTitle;
    static const std::string traitModel1;
    static const std::string traitModel2;
    static const std::string traitRange;
    static const std::string traitsInfo;
    static const std::string wordFor;
};

#endif // LAMARC_STRINGS_H

//____________________________________________________________________________________
