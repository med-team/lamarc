// $Id: forcesmenus.h,v 1.12 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef FORCESMENUS_H
#define FORCESMENUS_H

#include <string>
#include "newmenuitems.h"

class UIInterface;

class ForcesSubMenuItem : public SubMenuItem
{
  protected:
    std::string force;
    std::string forceLegal;
  public:
    ForcesSubMenuItem(std::string myKey,
                      UIInterface & myui,
                      std::string myForce,
                      std::string myForceLegal,
                      NewMenuCreator * mySubMenuCreator);
    virtual ~ForcesSubMenuItem();
    virtual std::string GetVariableText();
    virtual bool IsVisible();
};

class ForcesMenu : public NewMenu
{
  public:
    ForcesMenu(UIInterface & myui);
    ~ForcesMenu();
};

class ForcesMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    ForcesMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~ForcesMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new ForcesMenu(ui));};
};

class ForceSubMenuItem : public SubMenuItem
{
  private:
    ForceSubMenuItem(); //undefined
    UIId m_id;
  public:
    ForceSubMenuItem(std::string myKey,UIInterface & myui,
                     NewMenuCreator * myCreator, UIId id);
    virtual ~ForceSubMenuItem();
    virtual bool IsVisible();
};

#endif  // FORCESMENUS_H

//____________________________________________________________________________________
