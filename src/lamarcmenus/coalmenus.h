// $Id: coalmenus.h,v 1.16 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef COALMENUS_H
#define COALMENUS_H

#include <string>
#include <vector>
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"
#include "menutypedefs.h"

class UIInterface;

class SetAllThetasMenuItem : public SetMenuItemId
{
  public:
    SetAllThetasMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetAllThetasMenuItem();
    virtual bool IsVisible();
    virtual std::string GetVariableText();
};

class SetThetasFstMenuItem : public ToggleMenuItemNoId
{
  public:
    SetThetasFstMenuItem(std::string myKey, UIInterface & myui);
    virtual ~SetThetasFstMenuItem();
    virtual bool IsVisible();
};

class SetMenuItemThetas : public SetMenuItemGroup
{
  public:
    SetMenuItemThetas(UIInterface & ui);
    virtual ~SetMenuItemThetas();
    virtual std::vector<UIId> GetVisibleIds();
};

class CoalescenceMenu : public NewMenu
{
  public:
    CoalescenceMenu(UIInterface & myui);
    virtual ~CoalescenceMenu();
};

class CoalescenceMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    CoalescenceMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~CoalescenceMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new CoalescenceMenu(ui));};
};

#endif  // COALMENUS_H

//____________________________________________________________________________________
