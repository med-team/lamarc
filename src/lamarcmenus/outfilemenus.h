// $Id: outfilemenus.h,v 1.16 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef OUTFILEMENUS_H
#define OUTFILEMENUS_H

#include <string>
#include "newmenuitems.h"

class UIInterface;

class OutfileMenu : public NewMenu
{
  public:
    OutfileMenu(UIInterface & myui);
    ~OutfileMenu();
};

class OutfileMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    OutfileMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~OutfileMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new OutfileMenu(ui));};
};

// menu line giving access to OutfileMenu
class OutfileSubMenuItem : public SubMenuItem
{
  public:
    OutfileSubMenuItem(std::string myKey,UIInterface & myUI);
    virtual std::string GetVariableText();
};

#endif  // OUTFILEMENUS_H

//____________________________________________________________________________________
