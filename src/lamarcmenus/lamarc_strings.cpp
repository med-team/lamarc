// $Id: lamarc_strings.cpp,v 1.48 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "lamarc_strings.h"

using std::string;

const string lamarcmenu::addRangeDialog  = "Enter a site or a range of sites this trait might map to.  If entering a\n range of sites, use the format \"X:X\" (e.g. 23:57):";
const string lamarcmenu::baseFrequenciesTitle  = "Base Frequncies for ";
const string lamarcmenu::bayesianPriorsOverviewTitle = "Bayesian Priors Overview";
const string lamarcmenu::calcPerLocus     = "<calculate per segment>";
const string lamarcmenu::calculated       = " <calc>";
const string lamarcmenu::categoriesTitle  = "Mutation Rate Categories for ";
const string lamarcmenu::categoriesInfo   =
    "Each category should have a unique mutation rate--identical rates do "
    "nothing and waste considerable processing time.";
const string lamarcmenu::categoryTitle  = "Mutation Rate Category ";
const string lamarcmenu::coalInfo         =
    "Values of theta equal the number of inheritable copies of the data in your population times 2mu.  If you have nuclear non-sex chromosomal DNA in your data, this works out to 4Nmu, where N is the effective population size, and mu the mutation rate.  Data from X chromosomes is 3Nmu, and data from Y chromosomes and mitochondrial DNA is 1Nmu.";
const string lamarcmenu::coalTitle        = "Theta (Effective pop. size * mutation rate)";
const string lamarcmenu::dataOverviewTitle= "Data Options Overview";
const string lamarcmenu::dataTitle        = "Data options (tell us about your data)";
const string lamarcmenu::dataModelTitle   = "Edit data model for ";
const string lamarcmenu::defaultForParamsFor ="Default prior for ";
const string lamarcmenu::diseaseInfo      =
    "Values of D equal d/mu, and thus can be converted to be in terms of "
    "population size by multiplying by the appropriate Theta.\n"
    "e.g. if D is 100 and Theta is 0.01 for 4N data, this results in 4Nd=1\n";
const string lamarcmenu::diseaseTitle     = "Disease parameters and model";
const string lamarcmenu::divInfo          = "Edit Epoch Boundary Times";
const string lamarcmenu::divTitle         = "Divergence parameters and model";
const string lamarcmenu::divMigInfo          =
    "[information about Divergence Migration here]";
const string lamarcmenu::divMigTitle         = "Divergence Migration parameters and model";
const string lamarcmenu::effectivePopSizeTitle = "Effective population size menu";
const string lamarcmenu::effectivePopSizeInfo =
    "The effective population size per region is a way to scale the different "
    "regions so that the theta estimates can be combined.  Whatever region is set "
    "to 1.0 becomes the reference region, and the theta for all other regions is "
    "scaled appropriately.  So if, for example, you have both mitochondrial data "
    "and nuclear-chromosome data, if you set the mitochondrial region to 1 and the "
    "nuclear-chromosome region(s) to 4, the reported thetas will be scaled to the "
    "mitochondrial theta.  If you set the mitochondrial region to 0.25 and the "
    "nuclear-chromosome region(s) to 1, the reported thetas will be scaled to the "
    "nuclear-chromosome theta.";
const string lamarcmenu::effectivePopSizeFor = "Effective population size for ";
const string lamarcmenu::fileOverviewTitle= "File Name Overview";
const string lamarcmenu::forGlobalNuc     = "the default model for nucleotide data";
const string lamarcmenu::forGlobalMsat    = "the default model for microsatellite data";
const string lamarcmenu::forGlobalKAllele = "the default model for K-Allele data";
const string lamarcmenu::forceProfileTitle = "Profiling for force: ";
const string lamarcmenu::forceProfileInfo =
    "Profiling gives you information about the accuracy of your parameter "
    "estimates and the shape of the likelihood curve.  In a likelihood "
    "analysis, 'Fixed' profiling is "
    "faster but less informative, while 'percentile' profiling is slower but "
    "more informative.  In a bayesian analysis, both types of profiling take "
    "essentially no extra time to compute.  In either case, constant and invalid "
    "parameters cannot be profiled.";
const string lamarcmenu::forceConstraintTitle = "Constraints for force: ";
const string lamarcmenu::forceConstraintInfo   =
    "Select a parameter index to change its constraint type.  "
    "'Unconstrained' parameters vary freely, 'constant' parameters are held at "
    "their starting value, and 'invalid' parameters are set to zero and ignored "
    "in future output.  Grouped parameters are set to the same (averaged) starting "
    "value, then vary as above.  'Identical' grouped parameters are constrained to "
    "be always be equal to each other.";
const string lamarcmenu::forcesOverviewTitle="Analysis Overview";
const string lamarcmenu::forcesTitle      = "Analysis (tell us what you want to know)";
const string lamarcmenu::globalModel      = " <default>";
const string lamarcmenu::globalDataModelNuc = "Edit default data model for all Nucleotide data";
const string lamarcmenu::globalDataModelMsat = "Edit default data model for all Microsat data";
const string lamarcmenu::globalDataModelKAllele = "Edit default data model for all K-Allele data";
const string lamarcmenu::growInfo         =
    "Values of G equal g/mu, where the population size at time t in the past equals the the present population size times exp(-t*g).\n"
    "A shrinking population might have G = -10, while a rapidly growing population might have G = 200\n";
const string lamarcmenu::growTitle        = "Growth rate";
const string lamarcmenu::gtrRatesTitle    = "GTR Rates for ";
const string lamarcmenu::heatingInfo      =
    "Higher-temperature searches investigate more possible solutions "
    "but spend proportionately more time evaluating poor candidates.\n"
    "Temperatures will be scaled so the lowest is 1.0";
const string lamarcmenu::heatingTitle     = "Multiple simultaneous searches with heating";
const string lamarcmenu::logisticSelectionTitle = "Logistic selection coefficient";
const string lamarcmenu::logisticSelectionInfo =
    "This force models natural selection at a diallelic marker by "
    "assuming the subpopulation with favored allele \"A\" grows "
    "at the expense of the subpopulation with allele \"a\" "
    "at a logistic rate determined by a selection coefficient \"s.\"  "
    "Values of s > 0.1 indicate strong selection favoring \"A;\" "
    "negative values of s (down to a minimum value of -1) indicate "
    "selection favoring allele \"a.\"";
const string lamarcmenu::mainTitle        = "Main Menu";
const string lamarcmenu::migInfo          =
    "Values of M equal m/mu, and thus can be converted to be in terms of "
    "population size by multiplying by the appropriate Theta.\n"
    "e.g. if M is 100 and Theta is 0.01 for 4N data, this results in 4Nm=1";
const string lamarcmenu::migTitle         = "Migration parameters and model";
const string lamarcmenu::multiLocusCount_0= "<";
const string lamarcmenu::multiLocusCount_1= " segments>";
const string lamarcmenu::outfileTitle     = "Output file options";
const string lamarcmenu::overviewInfo     = "no changes allowed from this menu\nTo make changes use the menu: ";
const string lamarcmenu::overviewTitle    = "Overview of current settings (what you told us)";
const string lamarcmenu::parenLeft        = " ( ";
const string lamarcmenu::parenRight       = " ) ";
const string lamarcmenu::profileTitle     = "Profile likelihood settings";
const string lamarcmenu::profileInfo      =
    "Profiling provides information about the support intervals for your data.  "
    "In a likelihood run, percentile profiling is most informative, but takes "
    "longest to compute, while fixed profiling is less informative, but is "
    "faster.  In a bayesian run, both take a minimal amount of time.";
const string lamarcmenu::priorTitle       = "Bayesian Priors Menu";
const string lamarcmenu::priorInfo = "Bayesian priors can be set on a per-force or per-parameter basis.  The type of prior can be set (linear or logarithmic) as well as the allowable bounds.";
const string lamarcmenu::priorInfoForForce = "Select 'D' to change the default prior for all parameters for this force, or select the particular parameter whose prior you wish to change.  'Constant' and 'invalid' parameters do not use any prior.";
const string lamarcmenu::priorInfoForParam = "Priors may be either linear or logarithmic.  The lower bound for logarithmic priors must be above zero, in addition to any other constraints an evolutionary force might have.";
const string lamarcmenu::individualProfileTitle     = "Individual parameter profiling options";
const string lamarcmenu::rearrangeTitle   = "Rearrangers Menu";
const string lamarcmenu::rearrangeInfo    =
    "The values set for each arranger indicate the relative amount of time "
    "spent performing each type of arrangement.  The Tree-size frequency should "
    "probably be lower than the the Topology frequency, and in "
    "Bayesian runs with multiple parameters, the Bayesian frequency will "
    "probably need to be increased.";
const string lamarcmenu::recInfo          =
    "Values of r equal C/mu, and thus can be converted to be in terms of "
    "population size by multiplying by the appropriate Theta.\n"
    "e.g. if r is 0.1 and Theta is 0.01 for 4N data, this results in 4NC=0.001\n"
    "C is the recombination rate per generation per site (*not* per locus!).";
const string lamarcmenu::recTitle         = "Recombination parameters";
const string lamarcmenu::regionDataModelTitle   = "Edit data model(s) for ";
const string lamarcmenu::regionGammaTitle = "Gamma (background mutation rate varies over genomic regions)";
const string lamarcmenu::regionGammaInfo =
    "This \"force\" allows the background mutation rate to vary from region\n"
    "to region, modeled using the gamma distribution.  The \"scale\"\n"
    "parameter (\"beta\") gets set to the reciprocal of the \"shape\"\n"
    "parameter (\"alpha\") so that the average background mutation rate is 1.\n"
    "A low alpha value like 0.5 implies the data is nearly invariable\n"
    "in most genomic regions, and large at maybe one or two regions.\n"
    "A medium value like 1.1 implies a spread of mutation rates, most with\n"
    "low values.  A high value like 15 implies the level of variation is\n"
    "sizeable, but roughly the same for each genomic region.\n";
const string lamarcmenu::removeRangeDialog   = "Enter a site or a range of sites you know this trait cannot map.  If\n entering a range of sites, use the format \"X:X\" (e.g. 23:57):";
const string lamarcmenu::resultOverviewTitle = "Results and Progress Option Overview";
const string lamarcmenu::resultTitle      = "Input and Output (tell us where to keep it)";
const string lamarcmenu::searchTitle      = "Sampling strategy (chains and replicates)";
//LS DEBUG MAPPING: change after implementation
const string lamarcmenu::singleTraitModelInfo =
    "Your data contains information about the following trait.  This trait "
#ifdef NDEBUG
    "can be mapped either while collecting trees ('jump'), or after collecting "
    "trees ('float').";
#else
"can be either mapped to the region from which it came, used as data "
"to better determine other population parameters, or used to partition "
"your data into sub-populations.";
#endif
const string lamarcmenu::strategyOverviewTitle    = "Search Strategy Overview";
const string lamarcmenu::strategyTitle    = "Search Strategy menu (tell us how to do it)";
const string lamarcmenu::traitMappingOverviewTitle = "Mapping and Traits Overview";
const string lamarcmenu::traitModel1  = "Trait model for ";
const string lamarcmenu::traitModel2  = ".  Range: ";
const string lamarcmenu::traitRange   = "Range of allowable sites ";
const string lamarcmenu::traitAnalysisType   = "Type of Analysis ";
//const string lamarcmenu::traitsTitle = "Trait Data analysis (mapping or partitioning)";
const string lamarcmenu::traitsTitle = "Trait Data Mapping Analysis";
const string lamarcmenu::traitsInfo =
    "Your data contains information about the following traits.  Each trait "
    "will be mapped within a single region.";
;
const string lamarcmenu::wordFor          = " for ";

//____________________________________________________________________________________
