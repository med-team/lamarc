// $Id: constraintmenus.h,v 1.10 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CONSTRAINTMENUS_H
#define CONSTRAINTMENUS_H

#include <string>
#include "forcesmenus.h"
#include "newmenuitems.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"

class UIInterface;

class ConstraintMenuForOneForce : public NewMenu
{
  public:
    ConstraintMenuForOneForce(UIInterface &ui,UIId id);
    virtual ~ConstraintMenuForOneForce();
};

//These two classes are used for the individual forces' menus.
class SubMenuConstraintsForOneForce : public ForceSubMenuItem
{
  private:
    SubMenuConstraintsForOneForce(); //undefined
    UIId id;
  public:
    SubMenuConstraintsForOneForce(std::string key, UIInterface& ui, UIId id);
    virtual ~SubMenuConstraintsForOneForce();
};

class ConstraintsMenuForOneForceCreator : public NewMenuCreator
{
  private:
    ConstraintsMenuForOneForceCreator(); //undefined
    UIInterface& ui;
    UIId id;
  public:
    ConstraintsMenuForOneForceCreator(UIInterface& myui, UIId myid) :
        ui(myui), id(myid) {};
    virtual ~ConstraintsMenuForOneForceCreator() {};
    virtual NewMenu_ptr Create() { return NewMenu_ptr(new ConstraintMenuForOneForce(ui, id));};
};

class AddToGroupedConstraintsMenuItem : public SetMenuItemId
{
  public:
    AddToGroupedConstraintsMenuItem(std::string key, UIInterface& ui,
                                    UIId id);
    virtual ~AddToGroupedConstraintsMenuItem();
    virtual MenuInteraction_ptr GetHandler(std::string input);
};

class GetIdAndAddDialog : public SetDialog
{
  private:
    UIId outputId;
  protected:
    virtual void stuffValIntoUI(std::string val);
    virtual void stuffVal2IntoUI(std::string val);
  public:
    GetIdAndAddDialog(UIInterface& ui, string menuKey, UIId id);
    virtual ~GetIdAndAddDialog();
    virtual menu_return_type InvokeMe(Display& display);
    virtual bool handleInput2(std::string input);
    virtual string inLoopOutputString();
    virtual string inLoopOutputString2();

};

class RemoveFromGroupedConstraintsMenuItem : public SetMenuItemId
{
  public:
    RemoveFromGroupedConstraintsMenuItem(std::string key, UIInterface& ui,
                                         UIId id);
    virtual ~RemoveFromGroupedConstraintsMenuItem();
    virtual MenuInteraction_ptr GetHandler(std::string input);
};

class GetIdAndRemoveDialog : public SetDialog
{
  protected:
    virtual void stuffValIntoUI(std::string val);
  public:
    GetIdAndRemoveDialog(UIInterface& ui, string menuKey, UIId id);
    virtual ~GetIdAndRemoveDialog();
    virtual string inLoopOutputString();

};

class ToggleMenuItemUngroupedConstraints : public ToggleMenuItemGroup
{
  private:
    UIId    m_id;
  public:
    ToggleMenuItemUngroupedConstraints(UIInterface & ui,UIId id);
    virtual ~ToggleMenuItemUngroupedConstraints();
    virtual std::string GetGroupDescription();
    virtual std::string GetEmptyDescription();
    virtual UIIdVec1d GetVisibleIds();
};

class ToggleMenuItem2dGroup : public MenuDisplayQuantaWithHandler
{
  private:
    ToggleMenuItem2dGroup(); //undefined
  protected:
    UIInterface & ui;
    std::string menuKey;
    virtual UIId GetIdFromKey(std::string key);
    virtual MenuInteraction_ptr MakeOneHandler(UIId id);
    virtual UIId GetGroupIdFromLocalId(UIId localId);
  public:
    ToggleMenuItem2dGroup(UIInterface& ui, string menuKey);
    virtual ~ToggleMenuItem2dGroup() {};
    virtual UIIdVec2d GetVisibleIds() = 0;
    // single alphanumeric to invoke this line
    virtual std::string GetKey(UIId id);
    // description of this item
    virtual std::string GetText(UIId id);
    // current value of this item if any
    virtual std::string GetVariableText(UIId id);
    virtual std::string GetGroupDescription() {return "\n";};
    virtual std::string GetGroupDescription(long groupId);
    virtual std::string GetEmptyDescription() {return "\n";};
    virtual std::string GetEmptyDescription(long groupId);
    virtual bool Handles(std::string input);
    virtual MenuInteraction_ptr GetHandler(std::string input);
    // not virtual because we're assuming all inheritors should
    // use the canned routine given in class Display
    void DisplayItemOn(Display & display);

    // stuff for multi-line items
    virtual bool HasMultiLineItems() {return false;};
    virtual std::vector<std::string> GetExtraText(UIId) {return std::vector<std::string>();};
    virtual std::vector<std::string> GetExtraVariableText(UIId) {return std::vector<std::string>();};

};

class ToggleMenuItemGroupedConstraints : public ToggleMenuItem2dGroup
{
  private:
    UIId    m_id;
  public:
    ToggleMenuItemGroupedConstraints(UIInterface & ui,UIId id);
    virtual ~ToggleMenuItemGroupedConstraints();
    virtual UIIdVec2d GetVisibleIds();
    // description of the group
    virtual std::string GetGroupDescription(long groupId);
    virtual std::string GetEmptyDescription(long groupId);
};

#endif  // CONSTRAINTMENUS_H

//____________________________________________________________________________________
