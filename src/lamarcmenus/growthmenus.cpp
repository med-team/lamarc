// $Id: growthmenus.cpp,v 1.17 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <string>
#include "constants.h"
#include "constraintmenus.h"
#include "forcesummary.h"
#include "growthmenus.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "newmenuitems.h"
#include "priormenus.h"
#include "setmenuitem.h"
#include "togglemenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "profilemenus.h"

using std::string;

SetAllGrowthMenuItem::SetAllGrowthMenuItem(string myKey, UIInterface & ui)
    : SetMenuItemId(myKey,ui,uistr::globalGrowth, UIId(force_GROW, uiconst::GLOBAL_ID))
{
}

SetAllGrowthMenuItem::~SetAllGrowthMenuItem()
{
}

bool SetAllGrowthMenuItem::IsVisible()
{
    return ui.doGetBool(uistr::growth);
}

std::string SetAllGrowthMenuItem::GetVariableText()
{
    return "";
}

//------------------------------------------------------------------------------------

ToggleMenuItemGrowthScheme::ToggleMenuItemGrowthScheme(string k, UIInterface & myui)
    : ToggleMenuItemNoId(k,myui,uistr::growthScheme)
{
}

ToggleMenuItemGrowthScheme::~ToggleMenuItemGrowthScheme()
{
}

bool ToggleMenuItemGrowthScheme::IsVisible()
{
#ifdef NDEBUG
    return false;
#else
    return ui.doGetBool(uistr::growth);
#endif
}

//------------------------------------------------------------------------------------

ToggleMenuItemGrowthType::ToggleMenuItemGrowthType(string k, UIInterface & myui)
    : ToggleMenuItemNoId(k,myui,uistr::growthType)
{
}

ToggleMenuItemGrowthType::~ToggleMenuItemGrowthType()
{
}

bool ToggleMenuItemGrowthType::IsVisible()
{
#ifdef NDEBUG
    return false;
#else
    return ui.doGetBool(uistr::growth);
#endif
}

//------------------------------------------------------------------------------------

SetMenuItemGrowths::SetMenuItemGrowths(UIInterface & myui)
    : SetMenuItemGroup(myui,uistr::growthByID)
{
}

SetMenuItemGrowths::~SetMenuItemGrowths()
{
}

vector<UIId> SetMenuItemGrowths::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validParamsForForce,UIId(force_GROW));
}

//------------------------------------------------------------------------------------

GrowthMenu::GrowthMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::growTitle,lamarcmenu::growInfo)
{
    AddMenuItem(new ToggleMenuItemNoId("X",ui,uistr::growth));
    UIId id(force_GROW);
    AddMenuItem(new ToggleMenuItemGrowthScheme("S",ui));
    AddMenuItem(new ToggleMenuItemGrowthType("I",ui));
    AddMenuItem(new SubMenuConstraintsForOneForce("C",ui,id));
    AddMenuItem(new SubMenuProfileForOneForce("P",ui,id));
    AddMenuItem(new SubMenuPriorForOneForce("B",ui,id));
    AddMenuItem(new SetAllGrowthMenuItem("G",ui));
    AddMenuItem(new SetMenuItemGrowths(ui));
}

GrowthMenu::~GrowthMenu ()
{
}

//____________________________________________________________________________________
