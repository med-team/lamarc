// $Id: traitmodelmenu.cpp,v 1.12 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>

#include "lamarc_strings.h"
#include "overviewmenus.h"
#include "traitmodelmenu.h"
#include "ui_interface.h"
#include "ui_vars.h"

//------------------------------------------------------------------------------------

TraitModelItem::TraitModelItem(string key, UIInterface& ui)
    : SubMenuItem(key, ui, new TraitModelsMenuCreator(ui))
{
}

TraitModelItem::~TraitModelItem()
{
}

bool TraitModelItem::IsVisible()
{
    return (ui.GetCurrentVars().traitmodels.GetNumMovableLoci() > 0);
}

string TraitModelItem::GetVariableText()
{
    //If the trait menu is on at all, it's enabled.
    return "Enabled";
}

//------------------------------------------------------------------------------------

TraitModelsMenu::TraitModelsMenu (UIInterface & myui )
    : NewMenu (myui,lamarcmenu::traitsTitle,lamarcmenu::traitsInfo)
{
    AddMenuItem(new SubMenuItemsTraitModels(ui));
}

TraitModelsMenu::~TraitModelsMenu ()
{
}

//------------------------------------------------------------------------------------

SubMenuItemsTraitModels::SubMenuItemsTraitModels(UIInterface & myui)
    : MenuDisplayGroupBaseImplementation(myui,menustr::emptyString)
{
}

SubMenuItemsTraitModels::~SubMenuItemsTraitModels()
{
}

vector<UIId> SubMenuItemsTraitModels::GetVisibleIds()
{
    return ui.doGetUIIdVec1d(uistr::validMovingLoci,UIId());
}

MenuInteraction_ptr SubMenuItemsTraitModels::GetHandler(std::string input)
{
    vector<UIId> ids = ui.doGetUIIdVec1d(uistr::validMovingLoci, UIId());
    long idno = ProduceLongOrBarf(input);
    idno--;
    assert(idno < static_cast<long>(ids.size()));
    return MakeOneHandler(ids[idno]);
}

MenuInteraction_ptr SubMenuItemsTraitModels::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new SingleTraitModelMenu(ui, id));
}

string SubMenuItemsTraitModels::GetText(UIId id)
{
    return lamarcmenu::traitModel1 + ui.doGetString(uistr::regionName, id);
}

string SubMenuItemsTraitModels::GetVariableText(UIId id)
{
    return ui.doGetString(uistr::traitModelName, id);
}

string SubMenuItemsTraitModels::GetKey(UIId id)
{
    vector<UIId> ids = ui.doGetUIIdVec1d(uistr::validMovingLoci, UIId());
    for (unsigned long idno=0; idno<ids.size(); idno++)
    {
        if (ids[idno] == id) return ToString(idno+1);
    }
    assert(false);
    return ToString(0);
}

//------------------------------------------------------------------------------------

SingleTraitModelMenu::SingleTraitModelMenu(UIInterface& ui, UIId id)
    : NewMenu(ui, menustr::emptyString, lamarcmenu::singleTraitModelInfo),
      m_id(id)
{
    AddMenuItem(new DisplayOnlyMenuItem(uistr::traitModelName, ui, m_id));
    AddMenuItem(new MenuDisplayTraitAnalysisType(ui, id));
    AddMenuItem(new ToggleMenuItemId("F", ui, uistr::traitAnalysisFloat, m_id));
    AddMenuItem(new ToggleMenuItemId("J", ui, uistr::traitAnalysisJump, m_id));

#if 0
    //LS DEBUG MAPPING: change after implementation
    AddMenuItem(new ToggleMenuItemId("U", ui, uistr::traitAnalysisData, m_id));
    AddMenuItem(new ToggleMenuItemId("P", ui, uistr::traitAnalysisPartition, m_id));
#endif

    AddMenuItem(new MenuDisplayTraitRange(ui, id));
    AddMenuItem(new AddRangeToTraitModel("A", ui, m_id));
    AddMenuItem(new RemoveRangeFromTraitModel("D", ui, m_id));

#if 0
    AddMenuItem(new TraitModelRangeToPointMenu("S", ui, m_id));
#endif
}

SingleTraitModelMenu::~SingleTraitModelMenu()
{
}

string SingleTraitModelMenu::Title()
{
    return lamarcmenu::traitModel1 + ui.doGetString(uistr::locusName, m_id);
}

//------------------------------------------------------------------------------------

MenuDisplayTraitAnalysisType::MenuDisplayTraitAnalysisType(UIInterface& ui, UIId id)
    : MenuDisplayLine(),
      m_regId(id, ui.GetCurrentVars()),
      m_ui(ui)
{
}

MenuDisplayTraitAnalysisType::~MenuDisplayTraitAnalysisType()
{
}

string MenuDisplayTraitAnalysisType::GetKey()
{
    return "";
}

string MenuDisplayTraitAnalysisType::GetText()
{
    return lamarcmenu::traitAnalysisType;
}

string MenuDisplayTraitAnalysisType::GetVariableText()
{
    return ToString(m_ui.GetCurrentVars().traitmodels.GetAnalysisType(m_regId));
}

//------------------------------------------------------------------------------------

MenuDisplayTraitRange::MenuDisplayTraitRange(UIInterface& ui, UIId id)
    : MenuDisplayLine(),
      m_regId(id, ui.GetCurrentVars()),
      m_ui(ui)
{
}

MenuDisplayTraitRange::~MenuDisplayTraitRange()
{
}

string MenuDisplayTraitRange::GetKey()
{
    return "";
}

string MenuDisplayTraitRange::GetText()
{
    return lamarcmenu::traitRange;
}

string MenuDisplayTraitRange::GetVariableText()
{
    return ToString(m_ui.GetCurrentVars().traitmodels.GetRange(m_regId));
}

//------------------------------------------------------------------------------------

AddRangeToTraitModel::AddRangeToTraitModel(string key, UIInterface& ui, UIId id)
    : SetMenuItemId(key, ui, uistr::addRangeForTraitModel, id)
{
}

AddRangeToTraitModel::~AddRangeToTraitModel()
{
}

string AddRangeToTraitModel::GetVariableText()
{
    return "";
}

MenuInteraction_ptr AddRangeToTraitModel::GetHandler(std::string input)
{
    assert(Handles(input));
    return MenuInteraction_ptr(new SetAddRangeDialog(ui,menuKey,GetId()));
}

//------------------------------------------------------------------------------------

SetAddRangeDialog::SetAddRangeDialog(UIInterface& ui, string key, UIId id)
    : SetDialog(ui, key, id)
{
}

SetAddRangeDialog::~SetAddRangeDialog()
{
}

string SetAddRangeDialog::inLoopOutputString()
{
    return lamarcmenu::addRangeDialog;
}

//------------------------------------------------------------------------------------

RemoveRangeFromTraitModel::RemoveRangeFromTraitModel(string key, UIInterface& ui, UIId id)
    : SetMenuItemId(key, ui, uistr::removeRangeForTraitModel, id)
{
}

RemoveRangeFromTraitModel::~RemoveRangeFromTraitModel()
{
}

string RemoveRangeFromTraitModel::GetVariableText()
{
    return "";
}

MenuInteraction_ptr RemoveRangeFromTraitModel::GetHandler(std::string input)
{
    assert(Handles(input));
    return MenuInteraction_ptr(new SetRemoveRangeDialog(ui,menuKey,GetId()));
}

//------------------------------------------------------------------------------------

TraitModelRangeToPointMenu::TraitModelRangeToPointMenu(string key, UIInterface& ui, UIId id)
    : SetMenuItemId(key, ui, uistr::traitModelRangeToPoint, id)
{
}

TraitModelRangeToPointMenu::~TraitModelRangeToPointMenu()
{
}

string TraitModelRangeToPointMenu::GetVariableText()
{
    return "";
}

//------------------------------------------------------------------------------------

SetRemoveRangeDialog::SetRemoveRangeDialog(UIInterface& ui, string key, UIId id)
    : SetDialog(ui, key, id)
{
}

SetRemoveRangeDialog::~SetRemoveRangeDialog()
{
}

string SetRemoveRangeDialog::inLoopOutputString()
{
    return lamarcmenu::removeRangeDialog;
}

//____________________________________________________________________________________
