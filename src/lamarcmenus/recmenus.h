// $Id: recmenus.h,v 1.13 2018/01/03 21:33:01 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef RECMENUS_H
#define RECMENUS_H

#include <string>
#include "newmenuitems.h"
#include "setmenuitem.h"

class UIInterface;

class RecombineMaxEventsMenuItem : public SetMenuItemNoId
{
  public:
    RecombineMaxEventsMenuItem(std::string myKey, UIInterface & myui);
    ~RecombineMaxEventsMenuItem();
    bool IsVisible();
};

class RecombineRateMenuItem : public SetMenuItemId
{
  public:
    RecombineRateMenuItem(std::string myKey, UIInterface & myui);
    ~RecombineRateMenuItem();
    bool IsVisible();
};

class RecombinationMenu : public NewMenu
{
  public:
    RecombinationMenu(UIInterface & myui);
    virtual ~RecombinationMenu();
};

class RecombinationMenuCreator : public NewMenuCreator
{
  protected:
    UIInterface & ui;
  public:
    RecombinationMenuCreator(UIInterface & myui) : ui(myui) {};
    virtual ~RecombinationMenuCreator() {};
    NewMenu_ptr Create() { return NewMenu_ptr(new RecombinationMenu(ui));};
};

#endif  // RECMENUS_H

//____________________________________________________________________________________
