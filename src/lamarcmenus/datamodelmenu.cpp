// $Id: datamodelmenu.cpp,v 1.44 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>

#include "datamodelmenu.h"
#include "lamarc_strings.h"
#include "menu_strings.h"
#include "menuitem.h"
#include "overviewmenus.h"
#include "popsizemenu.h"
#include "setmenuitem.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"

using std::string;

//------------------------------------------------------------------------------------

AssignToGlobalMenuItem::AssignToGlobalMenuItem(
    string myKey,
    UIInterface & myui,
    UIId myid)
    : ToggleMenuItemId(myKey,myui,uistr::useGlobalDataModelForOne,myid)
{
}

AssignToGlobalMenuItem::~AssignToGlobalMenuItem()
{
}

bool AssignToGlobalMenuItem::IsVisible()
{
    long index = GetId().GetIndex1();
    return (index != uiconst::GLOBAL_DATAMODEL_NUC_ID &&
            index != uiconst::GLOBAL_DATAMODEL_MSAT_ID &&
            index != uiconst::GLOBAL_DATAMODEL_KALLELE_ID);
}

//------------------------------------------------------------------------------------

TTRatioMenuItem::TTRatioMenuItem(
    string myKey,
    UIInterface & myui,
    UIId myid)
    : SetMenuItemId(myKey,myui,uistr::TTRatio,myid)
{
}

TTRatioMenuItem::~TTRatioMenuItem()
{
}

bool TTRatioMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return mt == F84;
}

//------------------------------------------------------------------------------------

DataUncertaintyMenuItem::DataUncertaintyMenuItem(
    string myKey,
    UIInterface & myui,
    UIId myid)
    : SetMenuItemId(myKey,myui,uistr::perBaseErrorRate,myid)
{
}

DataUncertaintyMenuItem::~DataUncertaintyMenuItem()
{
}

bool DataUncertaintyMenuItem::IsVisible()
{
    // yes if is nuc model
    data_type dt = ui.doGetDataType(uistr::dataType,id);
    return (dt == dtype_DNA || dt == dtype_SNP);
}

//------------------------------------------------------------------------------------

AlphaMenuItem::AlphaMenuItem(
    string myKey,
    UIInterface & myui,
    UIId myid)
    : SetMenuItemId(myKey,myui,uistr::alpha,myid)
{
}

AlphaMenuItem::~AlphaMenuItem()
{
}

bool AlphaMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return mt == MixedKS;
}

//------------------------------------------------------------------------------------

OptimizeAlphaMenuItem::OptimizeAlphaMenuItem(
    string myKey,
    UIInterface & myui,
    UIId myid)
    : ToggleMenuItemId(myKey,myui,uistr::optimizeAlpha,myid)
{
}

OptimizeAlphaMenuItem::~OptimizeAlphaMenuItem()
{
}

bool OptimizeAlphaMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return mt == MixedKS;
}

//------------------------------------------------------------------------------------

GTRRatesMenu::GTRRatesMenu(UIInterface& myui, UIId myid)
    : NewMenu(myui,menustr::emptyString) , id(myid)
{
    AddMenuItem(new SetMenuItemId("1",ui,uistr::gtrRateAC,id));
    AddMenuItem(new SetMenuItemId("2",ui,uistr::gtrRateAG,id));
    AddMenuItem(new SetMenuItemId("3",ui,uistr::gtrRateAT,id));
    AddMenuItem(new SetMenuItemId("4",ui,uistr::gtrRateCG,id));
    AddMenuItem(new SetMenuItemId("5",ui,uistr::gtrRateCT,id));
    AddMenuItem(new SetMenuItemId("6",ui,uistr::gtrRateGT,id));
}

GTRRatesMenu::~GTRRatesMenu()
{
}

string GTRRatesMenu::Title()
{
    string title = lamarcmenu::gtrRatesTitle;
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        return title + lamarcmenu::forGlobalNuc;
    }
    assert (id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_MSAT_ID);
    assert (id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_KALLELE_ID);

    return title + ui.doGetString(uistr::regionName,id);
}

GTRRatesSubMenuItem::GTRRatesSubMenuItem(string myKey, UIInterface & myui, UIId myid)
    : SubMenuItem(myKey,myui,NULL), id(myid)
{
}

GTRRatesSubMenuItem::~GTRRatesSubMenuItem()
{
}

bool GTRRatesSubMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return (mt == GTR);
}

string GTRRatesSubMenuItem::GetText()
{
    return uistr::gtrRates;
}

string GTRRatesSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::gtrRates,id);
}

MenuInteraction_ptr GTRRatesSubMenuItem::GetHandler(std::string)
{
    NewMenu * gtrRateMenu = new GTRRatesMenu(ui,id);
    return MenuInteraction_ptr(gtrRateMenu);
}

//------------------------------------------------------------------------------------

FreqsFromDataMenuItem::FreqsFromDataMenuItem(string myKey, UIInterface & myui, UIId myid)
    : ToggleMenuItemId(myKey,myui,uistr::freqsFromData,myid)
{
}

FreqsFromDataMenuItem::~FreqsFromDataMenuItem()
{
}

bool FreqsFromDataMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return (mt == F84);
    //If the model is GTR, we require that you set the frequencies by hand
}

//------------------------------------------------------------------------------------

SetFrequencyMenuItemId::SetFrequencyMenuItemId(string myKey,
                                               UIInterface& myui,
                                               std::string myVariable,
                                               UIId myid)
    : SetMenuItemId(myKey,myui,myVariable,myid)
{
}

SetFrequencyMenuItemId::~SetFrequencyMenuItemId()
{
}

string
SetFrequencyMenuItemId::GetText()
{
    string text = SetMenuItemId::GetText();
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        text += lamarcmenu::wordFor + lamarcmenu::forGlobalNuc;
    }
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_MSAT_ID);
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_KALLELE_ID);
    return text;
}

bool
SetFrequencyMenuItemId::IsVisible()
{
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        return !(ui.doGetBool(uistr::freqsFromData,id));
    }
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_MSAT_ID);
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_KALLELE_ID);
    return true;
}

BaseFrequenciesMenu::BaseFrequenciesMenu(UIInterface& myui, UIId myid)
    : NewMenu(myui,menustr::emptyString) , id(myid)
{
    // Calculate base frequencies for F84
    AddMenuItem(new FreqsFromDataMenuItem("F",ui,id));
    AddMenuItem(new SetFrequencyMenuItemId("A",ui,uistr::baseFrequencyA,id));
    AddMenuItem(new SetFrequencyMenuItemId("C",ui,uistr::baseFrequencyC,id));
    AddMenuItem(new SetFrequencyMenuItemId("G",ui,uistr::baseFrequencyG,id));
    AddMenuItem(new SetFrequencyMenuItemId("T",ui,uistr::baseFrequencyT,id));
}

BaseFrequenciesMenu::~BaseFrequenciesMenu()
{
}

string BaseFrequenciesMenu::Title()
{
    string title = lamarcmenu::baseFrequenciesTitle;
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        return title + lamarcmenu::forGlobalNuc;
    }
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_MSAT_ID);
    assert(id.GetIndex1() != uiconst::GLOBAL_DATAMODEL_KALLELE_ID);
    return title + ui.doGetString(uistr::regionName,id);
}

BaseFrequenciesSubMenuItem::BaseFrequenciesSubMenuItem(string myKey, UIInterface & myui, UIId myid)
    : SubMenuItem(myKey,myui,NULL), id(myid)
{
}

BaseFrequenciesSubMenuItem::~BaseFrequenciesSubMenuItem()
{
}

bool BaseFrequenciesSubMenuItem::IsVisible()
{
    model_type mt = ui.doGetModelType(uistr::dataModel,id);
    return (mt == F84 || mt == GTR);
}

string BaseFrequenciesSubMenuItem::GetText()
{
    return uistr::baseFrequencies;
}

string BaseFrequenciesSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::baseFrequencies,id);
}

MenuInteraction_ptr BaseFrequenciesSubMenuItem::GetHandler(std::string)
{
    NewMenu * baseFreqMenu = new BaseFrequenciesMenu(ui,id);
    return MenuInteraction_ptr(baseFreqMenu);
}

//------------------------------------------------------------------------------------

CategoryMenu::CategoryMenu(UIInterface& myui, UIId myid)
    : NewMenu(myui,menustr::emptyString), id(myid)
{
    AddMenuItem(new SetMenuItemId("R",myui,uistr::categoryRate,myid));
    AddMenuItem(new SetMenuItemId("P",myui,uistr::categoryProbability,myid));
}

CategoryMenu::~CategoryMenu()
{
}

string CategoryMenu::Title()
{
    string titleString = lamarcmenu::categoryTitle
        + ToString(indexToKey(id.GetIndex3()))
        + lamarcmenu::wordFor;
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        titleString += lamarcmenu::forGlobalNuc;
    }
    else if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_MSAT_ID)
    {
        titleString += lamarcmenu::forGlobalMsat;
    }
    else if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_KALLELE_ID)
    {
        titleString += lamarcmenu::forGlobalKAllele;
    }
    else
    {
        titleString += ui.doGetString(uistr::regionName,id);
    }
    return titleString;
}

MenuInteraction_ptr
CategoriesMenuGroup::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new CategoryMenu(ui,id));
}

CategoriesMenuGroup::CategoriesMenuGroup(UIInterface& myui, UIId myid)
    : MenuDisplayGroupBaseImplementation(myui,""), id(myid)
{
}

CategoriesMenuGroup::~CategoriesMenuGroup()
{
}

string
CategoriesMenuGroup::GetKey(UIId id)
{
    return indexToKey(id.GetIndex3());
}

string
CategoriesMenuGroup::GetText(UIId)
{
    return string("Mutation Rate pair: (rate, probability)");
}

string
CategoriesMenuGroup::GetVariableText(UIId id)
{
    string rate = ui.doGetPrintString(uistr::categoryRate,id);
    string prob = ui.doGetPrintString(uistr::categoryProbability,id);
    return string("(")+ToString(rate)+","+ToString(prob)+")";
}

vector<UIId>
CategoriesMenuGroup::GetVisibleIds()
{
    vector<UIId> visibleIds;
    long numCats = ui.doGetLong(uistr::categoryCount,id);
    for(long i=0; i < numCats; i++)
    {
        visibleIds.push_back(UIId(id.GetIndex1(),id.GetIndex2(),i));
    }
    return visibleIds;
}

CategoriesMenu::CategoriesMenu(UIInterface& myui, UIId myid)
    : NewMenu(myui,menustr::emptyString, lamarcmenu::categoriesInfo) , id(myid)
{
    AddMenuItem(new SetMenuItemId("N",myui,uistr::categoryCount,myid));
    AddMenuItem(new CategoriesMenuGroup(ui,id));
}

CategoriesMenu::~CategoriesMenu()
{
}

string CategoriesMenu::Title()
{
    string title = lamarcmenu::categoriesTitle;
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        return title + lamarcmenu::forGlobalNuc;
    }
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_MSAT_ID)
    {
        return title + lamarcmenu::forGlobalMsat;
    }
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_KALLELE_ID)
    {
        return title + lamarcmenu::forGlobalKAllele;
    }
    return title + ui.doGetString(uistr::regionName,id);
}

CategoriesSubMenuItem::CategoriesSubMenuItem(string myKey, UIInterface & myui, UIId myid)
    : SubMenuItem(myKey,myui,NULL), id(myid)
{
}

CategoriesSubMenuItem::~CategoriesSubMenuItem()
{
}

string CategoriesSubMenuItem::GetText()
{
    return ui.doGetDescription(uistr::categoryCount,id);
}

string CategoriesSubMenuItem::GetVariableText()
{
    return ui.doGetPrintString(uistr::categoryCount,id);
}

MenuInteraction_ptr CategoriesSubMenuItem::GetHandler(std::string)
{
    NewMenu * catsFreqMenu = new CategoriesMenu(ui,id);
    return MenuInteraction_ptr(catsFreqMenu);
}

//------------------------------------------------------------------------------------

RegionDataModelMenu::RegionDataModelMenu(
    UIInterface & myui,
    UIId myid)
    : NewMenu(myui,menustr::emptyString) , id(myid)
{
    AddMenuItem(new LocusDataModels(ui,id));
}

RegionDataModelMenu::~RegionDataModelMenu()
{
}

string RegionDataModelMenu::Title()
{
    return lamarcmenu::regionDataModelTitle + ui.doGetString(uistr::regionName,id);
}

string RegionDataModelMenu::Info()
{
    return menustr::emptyString;
}

//------------------------------------------------------------------------------------

RegionDataModels::RegionDataModels(UIInterface & myui)
    : MenuDisplayGroupBaseImplementation(myui,menustr::emptyString)
{
}

RegionDataModels::~RegionDataModels()
{
}

vector<UIId> RegionDataModels::GetVisibleIds()
{
    vector<UIId> visibleIds;
    LongVec1d regionNumbers = ui.doGetLongVec1d(uistr::regionNumbers);
    LongVec1d::iterator i;
    for(i=regionNumbers.begin(); i != regionNumbers.end(); i++)
    {
        visibleIds.push_back(UIId(*i));
    }
    return visibleIds;
}

MenuInteraction_ptr RegionDataModels::MakeOneHandler(UIId id)
{
    // if there is only one locus for the region, skip directly
    // to the locus menu
    long numLoci = ui.doGetLong(uistr::lociCount,id);
    if(numLoci > 1)
    {
        return MenuInteraction_ptr(new RegionDataModelMenu(ui,id));
    }
    else
    {
        UIId locusId(id.GetIndex1(),0);
        return MenuInteraction_ptr(new LocusDataModelMenu(ui,locusId));
    }
}

string RegionDataModels::GetText(UIId id)
{
    return lamarcmenu::regionDataModelTitle + ui.doGetString(uistr::regionName,id);
}

string RegionDataModels::GetVariableText(UIId id)
{
    long lociCount = ui.doGetLong(uistr::lociCount,id);
    if(lociCount == 1)
    {
        UIId idForFirstLocus = UIId(id.GetIndex1(),0);
        string text = ui.doGetPrintString(uistr::dataModel,idForFirstLocus);
        if(ui.doGetBool(uistr::useGlobalDataModelForOne,idForFirstLocus))
        {
            text += lamarcmenu::globalModel;
        }
        return text;
    }
    else
    {
        return lamarcmenu::multiLocusCount_0
            +ToString(lociCount)
            +lamarcmenu::multiLocusCount_1;
    }
}

//------------------------------------------------------------------------------------

LocusDataModelMenu::LocusDataModelMenu(
    UIInterface & myui,
    UIId myid)
    : NewMenu(myui,menustr::emptyString) , id(myid)
{
    AddMenuItem(new MenuDisplayDataType(ui, id));
    AddMenuItem(new ToggleMenuItemId("M",ui,uistr::dataModel,id));
    AddMenuItem(new AssignToGlobalMenuItem("D",ui,id));
    // rate categories & probs for F84, GTR, stepwise, brownian
    AddMenuItem(new CategoriesSubMenuItem("C",ui,id));
    // autocorrelation for F84, GTR, stepwise, brownian
    AddMenuItem(new SetMenuItemId("A",ui,uistr::autoCorrelation,id));
    // tt ratio for F84
    AddMenuItem(new TTRatioMenuItem("T",ui,id));
    // GTR rates for GTR
    AddMenuItem(new GTRRatesSubMenuItem("G",ui,id));
    // base frequencies for F84, GTR
    AddMenuItem(new BaseFrequenciesSubMenuItem("B",ui,id));
    // per base error rate
    AddMenuItem(new DataUncertaintyMenuItem("E",ui,id));
    // for MixedKS, alpha
    AddMenuItem(new AlphaMenuItem("L",ui,id));
    AddMenuItem(new OptimizeAlphaMenuItem("O",ui,id));
    //We need to let people set the relative mu rate because it can be
    // overridden with inopportune use of using the global data model.
    // Ultimately, we probably want to take the relative mu rate *out* of
    // the data model, and put it somewhere else so it can be constant.
    AddMenuItem(new SetMenuItemId("R",ui,uistr::relativeMuRate, id));
    //LS DEBUG!  Figure out what to do with this for sure before release.
#ifndef NDEBUG
    if (id.GetIndex1() >= 0 && id.GetIndex2() >= 0)
    {
        //Not a global region and/or locus menu
        AddMenuItem(new ToggleMenuItemId("S",ui,uistr::simulateData,id));
    }
#endif
}

LocusDataModelMenu::~LocusDataModelMenu()
{
}

string LocusDataModelMenu::Title()
{
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        return lamarcmenu::globalDataModelNuc;
    }
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_MSAT_ID)
    {
        return lamarcmenu::globalDataModelMsat;
    }
    if(id.GetIndex1() == uiconst::GLOBAL_DATAMODEL_KALLELE_ID)
    {
        return lamarcmenu::globalDataModelKAllele;
    }
    return lamarcmenu::dataModelTitle + ui.doGetString(uistr::locusName,id);
}

string LocusDataModelMenu::Info()
{
    return menustr::emptyString;
}

//------------------------------------------------------------------------------------

LocusDataModels::LocusDataModels(UIInterface & myui, UIId myid)
    : MenuDisplayGroupBaseImplementation(myui,menustr::emptyString), id(myid)
{
}

LocusDataModels::~LocusDataModels()
{
}

string
LocusDataModels::GetKey(UIId id)
{
    return indexToKey(id.GetIndex2());
}

vector<UIId> LocusDataModels::GetVisibleIds()
{
    vector<UIId> visibleIds;
    LongVec1d lociNumbers = ui.doGetLongVec1d(uistr::lociNumbers,id);
    LongVec1d::iterator i;
    for(i=lociNumbers.begin(); i != lociNumbers.end(); i++)
    {
        visibleIds.push_back(UIId(id.GetIndex1(),*i));
    }
    return visibleIds;
}

MenuInteraction_ptr LocusDataModels::MakeOneHandler(UIId id)
{
    return MenuInteraction_ptr(new LocusDataModelMenu(ui,id));
}

string LocusDataModels::GetText(UIId id)
{
    return lamarcmenu::dataModelTitle + ui.doGetString(uistr::locusName,id);
}

string LocusDataModels::GetVariableText(UIId id)
{
    string text = ui.doGetPrintString(uistr::dataModel,id);
    if(ui.doGetBool(uistr::useGlobalDataModelForOne,id))
    {
        text += lamarcmenu::globalModel;
    }
    return text;
}

//------------------------------------------------------------------------------------

NewDataModelMenu::NewDataModelMenu(UIInterface & myui)
    : NewMenu(myui,lamarcmenu::dataTitle,menustr::emptyString)
{
    AddMenuItem(new ToggleMenuItemNoId("C", ui,uistr::systemClock));
    AddMenuItem(new UseOldSeedMenuItem("R", ui));
    AddMenuItem(new SetMenuItemNoId(   "S", ui,uistr::randomSeed));
    AddMenuItem(new EffectivePopSizeSubMenuItem("E", ui));
    AddMenuItem(new GlobalDataModelNuc("N", ui));
    AddMenuItem(new GlobalDataModelMsat("M", ui));
    AddMenuItem(new GlobalDataModelKAllele("K", ui));
    AddMenuItem(new ToggleMenuItemNoId("D", ui,uistr::useGlobalDataModelForAll));
    AddMenuItem(new RegionDataModels(ui));
}

NewDataModelMenu::~NewDataModelMenu()
{
}

GlobalDataModelNuc::GlobalDataModelNuc(string myKey, UIInterface& myUI)
    : SubMenuItem(myKey, myUI, new LocusDataModelMenuCreator
                  (myUI, UIId(uiconst::GLOBAL_DATAMODEL_NUC_ID,0)))
{
}

GlobalDataModelNuc::~GlobalDataModelNuc()
{
}

bool GlobalDataModelNuc::IsVisible()
{
    return ui.GetCurrentVars().datapackplus.HasSomeNucleotideData();
}

GlobalDataModelMsat::GlobalDataModelMsat(string myKey, UIInterface& myUI)
    : SubMenuItem(myKey, myUI, new LocusDataModelMenuCreator(myUI, UIId(uiconst::GLOBAL_DATAMODEL_MSAT_ID,0)))
{
}

GlobalDataModelMsat::~GlobalDataModelMsat()
{
}

bool GlobalDataModelMsat::IsVisible()
{
    return ui.GetCurrentVars().datapackplus.HasSomeMsatData();
}

GlobalDataModelKAllele::GlobalDataModelKAllele(string myKey, UIInterface& myUI)
    : SubMenuItem(myKey, myUI, new LocusDataModelMenuCreator(myUI, UIId(uiconst::GLOBAL_DATAMODEL_KALLELE_ID,0)))
{
}

GlobalDataModelKAllele::~GlobalDataModelKAllele()
{
}

bool GlobalDataModelKAllele::IsVisible()
{
    return ui.GetCurrentVars().datapackplus.HasSomeKAlleleData();
}

//------------------------------------------------------------------------------------

MenuDisplayDataType::MenuDisplayDataType(UIInterface& ui, UIId id)
    : MenuDisplayLine(),
      m_regId(id, ui.GetCurrentVars())
{
}

MenuDisplayDataType::~MenuDisplayDataType()
{
}

string MenuDisplayDataType::GetKey()
{
    return "";
}

string MenuDisplayDataType::GetText()
{
    return uistr::dataType;
}

string MenuDisplayDataType::GetVariableText()
{
    return ToString(m_regId.GetDataType());
}

bool MenuDisplayDataType::IsVisible()
{
    return (m_regId.GetRegion() != uiconst::GLOBAL_ID);
}

//------------------------------------------------------------------------------------

EffectivePopSizeSubMenuItem::EffectivePopSizeSubMenuItem(string myKey, UIInterface& ui)
    : SubMenuItem(myKey, ui, new EffectivePopSizeMenuCreator(ui))
{
}

EffectivePopSizeSubMenuItem::~EffectivePopSizeSubMenuItem()
{
}

bool EffectivePopSizeSubMenuItem::IsVisible()
{
    return (ui.GetCurrentVars().datapackplus.GetNumRegions() > 1);
}

//------------------------------------------------------------------------------------

UseOldSeedMenuItem::UseOldSeedMenuItem(string myKey, UIInterface& ui)
    : ToggleMenuItemNoId(myKey, ui, uistr::useOldSeedFromClock)
{
}

UseOldSeedMenuItem::~UseOldSeedMenuItem()
{
}

bool UseOldSeedMenuItem::IsVisible()
{
    return (ui.GetCurrentVars().userparams.GetHasOldClockSeed());
}

std::string UseOldSeedMenuItem::GetText()
{
    std::string text =  ToggleMenuItemNoId::GetText();
    long oldSeed = ui.GetCurrentVars().userparams.GetOldClockSeed();
    text += lamarcmenu::parenLeft + ToString(oldSeed) + lamarcmenu::parenRight;
    return text;
}

std::string UseOldSeedMenuItem::GetVariableText()
{
    return menustr::emptyString;
}

//____________________________________________________________________________________
