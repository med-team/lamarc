// $Id: constraint_interface.cpp,v 1.10 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <algorithm>    // for std::min()
#include "constraint_interface.h"
#include "defaults.h"
#include "force.h"
#include "ui_constants.h"
#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"
#include "paramstat.h"

using std::string;

//------------------------------------------------------------------------------------

uiParameterStatus::uiParameterStatus()
    : SetGetIndividualParamstatus(uistr::constraintType)
{
}

uiParameterStatus::~uiParameterStatus()
{
}

ParamStatus uiParameterStatus::Get(UIVars& vars, UIId id)
{
    return vars.forces.GetParamstatus(id.GetForceType(), id.GetIndex1()).Status();
}

void uiParameterStatus::Set(UIVars& vars, UIId id, ParamStatus val)
{
    vars.forces.SetParamstatus(val, id.GetForceType(), id.GetIndex1());
}

string uiParameterStatus::Description(UIVars& vars, UIId id)
{
    // EWFIX.P5 DIMENSIONS -- will change if we divide up 2-d params into 2-D storage
    return vars.datapackplus.GetParamName(id.GetForceType(),id.GetIndex1());
}

//------------------------------------------------------------------------------------

uiAddParamToGroup::uiAddParamToGroup()
    : SetGetNoval(uistr::addParamToGroup)
{
}

uiAddParamToGroup::~uiAddParamToGroup()
{
}

void uiAddParamToGroup::Set(UIVars& vars, UIId id, noval val)
{
    vars.forces.AddParamToGroup(id.GetForceType(), id.GetIndex2(), id.GetIndex1());
}

//------------------------------------------------------------------------------------

uiAddParamToNewGroup::uiAddParamToNewGroup()
    : SetGetNoval(uistr::addParamToNewGroup)
{
}

uiAddParamToNewGroup::~uiAddParamToNewGroup()
{
}

void uiAddParamToNewGroup::Set(UIVars& vars, UIId id, noval val)
{
    vars.forces.AddParamToNewGroup(id.GetForceType(), id.GetIndex2());
}

//------------------------------------------------------------------------------------

uiRemoveParamFromGroup::uiRemoveParamFromGroup()
    : SetGetNoval(uistr::removeParamFromGroup)
{
}

uiRemoveParamFromGroup::~uiRemoveParamFromGroup()
{
}

void uiRemoveParamFromGroup::Set(UIVars& vars, UIId id, noval val)
{
    vars.forces.RemoveParamFromGroup(id.GetForceType(), id.GetIndex1());
}

//------------------------------------------------------------------------------------

uiGroupParameterStatus::uiGroupParameterStatus()
    : SetGetGroupParamstatus(uistr::groupConstraintType)
{
}

uiGroupParameterStatus::~uiGroupParameterStatus()
{
}

ParamStatus uiGroupParameterStatus::Get(UIVars& vars, UIId id)
{
    return vars.forces.GetGroupParamstatus(id.GetForceType(), id.GetIndex1());
    //Index1 is the group index.
}

void uiGroupParameterStatus::Set(UIVars& vars, UIId id, ParamStatus val)
{
    vars.forces.SetGroupParamstatus(val, id.GetForceType(), id.GetIndex1());
    //Index1 is the group index.
}

string uiGroupParameterStatus::Description(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetParamName(id.GetForceType(),id.GetIndex2());
    //Index2 is the parameter index.
}

//------------------------------------------------------------------------------------

uiGroupParameterList::uiGroupParameterList()
    : SetGetLongVec1d(uistr::groupParamList)
{
}

uiGroupParameterList::~uiGroupParameterList()
{
}

LongVec1d uiGroupParameterList::Get(UIVars& vars, UIId id)
{
    return vars.forces.GetGroupParamList(id.GetForceType(), id.GetIndex1());
}

void uiGroupParameterList::Set(UIVars& vars, UIId id, LongVec1d params)
{
    vars.forces.AddGroup(params, id.GetForceType(), id.GetIndex1());
}

//------------------------------------------------------------------------------------

uiUngroupedParamsForOneForce::uiUngroupedParamsForOneForce()
    : GetUIIdVec1d(uistr::ungroupedParamsForForce)
{
}

uiUngroupedParamsForOneForce::~uiUngroupedParamsForOneForce()
{
}

UIIdVec1d uiUngroupedParamsForOneForce::Get(UIVars& vars, UIId id)
{
    force_type thisForce = id.GetForceType();
    long numPossibleParams = vars.forces.GetNumParameters(thisForce);
    UIIdVec1d ungroupedParams;
    for(long localId=0; localId < numPossibleParams; localId++)
    {
        long gindex = vars.forces.ParamInGroup(thisForce,localId);
        if (gindex == FLAGLONG)
        {
            if (vars.forces.GetParamValid(thisForce,localId))
            {
                ungroupedParams.push_back(UIId(thisForce,localId));
            }
        }
    }
    return ungroupedParams;
}

//------------------------------------------------------------------------------------

uiGroupedParamsForOneForce::uiGroupedParamsForOneForce()
    : GetUIIdVec2d(uistr::groupedParamsForForce)
{
}

uiGroupedParamsForOneForce::~uiGroupedParamsForOneForce()
{
}

UIIdVec2d uiGroupedParamsForOneForce::Get(UIVars& vars, UIId id)
{
    force_type thisForce = id.GetForceType();
    long numPossibleParams = vars.forces.GetNumParameters(thisForce);
    UIIdVec2d groupedParams(vars.forces.GetNumGroups(thisForce));
    for(long localId=0; localId < numPossibleParams; localId++)
    {
        long gindex = vars.forces.ParamInGroup(thisForce,localId);
        if (gindex != FLAGLONG)
        {
            groupedParams[gindex].push_back(UIId(thisForce,gindex,localId));
        }
    }
    return groupedParams;
}

//____________________________________________________________________________________
