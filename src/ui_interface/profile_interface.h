// $Id: profile_interface.h,v 1.22 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PROFILE_INTERFACE_H
#define PROFILE_INTERFACE_H

#include <string>
#include "setget.h"

class UIVars;

class uiGlobalProfileOn : public SetGetNoval
{
  public:
    uiGlobalProfileOn();
    virtual ~uiGlobalProfileOn();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiGlobalProfileOff : public SetGetNoval
{
  public:
    uiGlobalProfileOff();
    virtual ~uiGlobalProfileOff();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiGlobalProfilePercentile : public SetGetNoval
{
  public:
    uiGlobalProfilePercentile();
    virtual ~uiGlobalProfilePercentile();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiGlobalProfileFixed : public SetGetNoval
{
  public:
    uiGlobalProfileFixed();
    virtual ~uiGlobalProfileFixed();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiProfileByForce : public SetGetProftype
{
  public:
    uiProfileByForce();
    virtual ~uiProfileByForce();
    virtual proftype Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,proftype val);
    virtual string Description(UIVars& vars, UIId id);
    virtual std::string GetPrintString(UIVars& vars, UIId id);
};

class uiProfileByID : public SetGetBool
{
  public:
    uiProfileByID();
    virtual ~uiProfileByID();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
    virtual string Description(UIVars& vars, UIId id);
    virtual std::string MakePrintString(UIVars& vars, bool val);
};

class uiValidParamsForOneForce : public GetUIIdVec1d
{
  public:
    uiValidParamsForOneForce();
    virtual ~uiValidParamsForOneForce();
    virtual UIIdVec1d Get(UIVars& vars, UIId id);
};

class uiValidForces : public GetUIIdVec1d
{
  public:
    uiValidForces();
    virtual ~uiValidForces();
    virtual UIIdVec1d Get(UIVars& vars, UIId id);
};

class uiProfileOnOffByID : public SetGetBool
{
  public:
    uiProfileOnOffByID();
    virtual ~uiProfileOnOffByID();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, bool val);
};

class uiForceProfilesOn : public SetGetNoval
{
  public:
    uiForceProfilesOn();
    virtual ~uiForceProfilesOn();
    virtual void Set(UIVars& vars, UIId id, noval val);
    virtual string Description(UIVars& vars, UIId id);
};

class uiForceProfilesOff : public SetGetNoval
{
  public:
    uiForceProfilesOff();
    virtual ~uiForceProfilesOff();
    virtual void Set(UIVars& vars, UIId id, noval val);
    virtual string Description(UIVars& vars, UIId id);
};

class uiForceProfileType : public SetGetProftype
{
  public:
    uiForceProfileType();
    virtual ~uiForceProfileType();
    virtual string Description(UIVars& vars, UIId id);
    virtual proftype Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,proftype val);
    virtual std::string NextToggleValue(UIVars& vars, UIId id);
};

#endif  // PROFILE_INTERFACE_H

//____________________________________________________________________________________
