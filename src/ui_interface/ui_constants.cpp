// $Id: ui_constants.cpp,v 1.10 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include "ui_constants.h"

const long uiconst::NO_ID               = -1;
const long uiconst::GLOBAL_ID           = -3;
const long uiconst::GLOBAL_DATAMODEL_NUC_ID       = -30;
const long uiconst::GLOBAL_DATAMODEL_MSAT_ID      = -31;
const long uiconst::GLOBAL_DATAMODEL_KALLELE_ID   = -32;
const long uiconst::diseaseColumns      = 3;
const long uiconst::migColumns          = 3;

//____________________________________________________________________________________
