// $Id: constraint_interface.h,v 1.6 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CONSTRAINT_INTERFACE_H
#define CONSTRAINT_INTERFACE_H

#include <string>
#include "setget.h"

class UIVars;

class uiParameterStatus : public SetGetIndividualParamstatus
{
  public:
    uiParameterStatus();
    virtual ~uiParameterStatus();
    virtual ParamStatus Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, ParamStatus val);
    virtual string Description(UIVars& vars, UIId id);
};

class uiAddParamToGroup : public SetGetNoval
{
  public:
    uiAddParamToGroup();
    virtual ~uiAddParamToGroup();
    virtual void   Set(UIVars& vars, UIId id, noval val);
};

class uiAddParamToNewGroup : public SetGetNoval
{
  public:
    uiAddParamToNewGroup();
    virtual ~uiAddParamToNewGroup();
    virtual void   Set(UIVars& vars, UIId id, noval val);
};

class uiRemoveParamFromGroup : public SetGetNoval
{
  public:
    uiRemoveParamFromGroup();
    virtual ~uiRemoveParamFromGroup();
    virtual void   Set(UIVars& vars, UIId id, noval val);
};

class uiGroupParameterStatus : public SetGetGroupParamstatus
{
  public:
    uiGroupParameterStatus();
    virtual ~uiGroupParameterStatus();
    virtual ParamStatus Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, ParamStatus val);
    virtual string Description(UIVars& vars, UIId id);
};

class uiGroupParameterList : public SetGetLongVec1d
{
  public:
    uiGroupParameterList();
    virtual ~uiGroupParameterList();
    virtual LongVec1d   Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, LongVec1d val);
};

class uiUngroupedParamsForOneForce : public GetUIIdVec1d
{
  public:
    uiUngroupedParamsForOneForce();
    virtual ~uiUngroupedParamsForOneForce();
    virtual UIIdVec1d Get(UIVars& vars, UIId id);
};

class uiGroupedParamsForOneForce : public GetUIIdVec2d
{
  public:
    uiGroupedParamsForOneForce();
    virtual ~uiGroupedParamsForOneForce();
    virtual UIIdVec2d Get(UIVars& vars, UIId id);
};

#endif  // CONSTRAINT_INTERFACE_H

//____________________________________________________________________________________
