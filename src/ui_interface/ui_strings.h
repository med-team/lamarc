// $Id: ui_strings.h,v 1.90 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_STRINGS_H
#define UI_STRINGS_H

#include <string>
#include "local_build.h"

using std::string;

// values for the static const strings below are set in ui_strings.cpp
class uistr
{
  public:
    static const string TTRatio;
    static const string addParamToGroup;
    static const string addParamToNewGroup;
    static const string addRangeForTraitModel;
    static const string allProfilesFixed;
    static const string allProfilesOff;
    static const string allProfilesOn;
    static const string allProfilesPercentile;
    static const string allStr;
    static const string alpha;
    static const string argFilePrefix;
    static const string autoCorrelation;
    static const string baseFrequencies;
    static const string baseFrequencyA;
    static const string baseFrequencyC;
    static const string baseFrequencyG;
    static const string baseFrequencyT;
    static const string bayesArranger;
    static const string bayesian;
    static const string canHapArrange;
    static const string categoryCount;
    static const string categoryPair;
    static const string categoryProbability;
    static const string categoryRate;
    static const string coalescence;
    static const string coalescenceLegal;
    static const string coalescenceMaxEvents;
    static const string coalescenceStartMethodByID;
    static const string constraintByForce;
    static const string constraintType;
    static const string crossPartitionCount;
    static const string curveFilePrefix;
    static const string dataFileName;
    static const string dataModel;
    static const string dataModelReport;
    static const string dataType;
    static const string defaultStr;
    static const string disease;
    static const string diseaseByID2;
    static const string diseaseByID;
    static const string diseaseInto;
    static const string diseaseLegal;
    static const string diseaseLocation;
    static const string diseaseMaxEvents;
    static const string diseasePartitionCount;
    static const string diseasePartitionName;
    static const string diseasePartitionNames;
    static const string divergence;
    static const string divergenceEpoch;
    static const string divergenceEpochAncestor;
    static const string divergenceEpochDescendent;
    static const string divergenceEpochDescendents;
    static const string divergenceEpochCount;
    static const string divergenceEpochName;
    static const string divergenceEpochNames;
    static const string divergenceEpochBoundaryTime;
    static const string divergenceLegal;
    static const string divmigration;
    static const string divmigrationByID2;
    static const string divmigrationByID;
    static const string divmigrationInto;
    static const string divmigrationLegal;
    static const string divmigrationMaxEvents;
    static const string divmigrationPartitionCount;
    static const string divmigrationPartitionName;
    static const string divmigrationPartitionNames;
    static const string divmigrationUser;
    static const string dropArranger;
    static const string effectivePopSize;
    static const string epochSizeArranger;
    static const string expGrowStick;
    static const string finalChains;
    static const string finalDiscard;
    static const string finalInterval;
    static const string finalSamples;
    static const string finalSamplesBayes;
    static const string forceLegal;
    static const string forceName;
    static const string forceOnOff;
    static const string forceVector;
    static const string freqsFromData;
    static const string fstSetMigration;
    static const string fstSetTheta;
    static const string globalDisease;
    static const string globalGrowth;
    static const string globalLogisticSelectionCoefficient;
    static const string globalMigration;
    static const string globalDivMigration;
    static const string globalTheta;
    static const string groupConstraintType;
    static const string groupParamList;
    static const string groupedParamsForForce;
    static const string growth;
    static const string growthByID;
    static const string growthLegal;
    static const string growthMaxEvents;
    static const string growthRate;
    static const string growthScheme;
    static const string growthType;
    static const string gtrRateAC;
    static const string gtrRateAG;
    static const string gtrRateAT;
    static const string gtrRateCG;
    static const string gtrRateCT;
    static const string gtrRateGT;
    static const string gtrRates;
    static const string hapArranger;
    static const string heatedChain;
    static const string heatedChainCount;
    static const string heatedChains;
    static const string initialChains;
    static const string initialDiscard;
    static const string initialInterval;
    static const string initialSamples;
    static const string initialSamplesBayes;
    static const string lociCount;
    static const string lociNumbers;
    static const string locusArranger;
    static const string locusName;
    static const string logSelectStick;
    static const string logisticSelection;
    static const string logisticSelectionCoefficient;
    static const string logisticSelectionLegal;
    static const string logisticSelectionMaxEvents;
    static const string manyArgFiles;
    static const string mapFilePrefix;
    static const string maxEvents;
    static const string migration;
    static const string migrationByID2;
    static const string migrationByID;
    static const string migrationInto;
    static const string migrationLegal;
    static const string migrationMaxEvents;
    static const string migrationPartitionCount;
    static const string migrationPartitionName;
    static const string migrationPartitionNames;
    static const string migrationUser;
    static const string muRate;
    static const string newickTreeFilePrefix;
    static const string normalization;
    static const string oneForceProfileType;
    static const string oneForceProfilesOff;
    static const string oneForceProfilesOn;
    static const string optimizeAlpha;
    static const string paramName;
    static const string paramVector;
    static const string perBaseErrorRate;
    static const string plotPost;
    static const string priorByForce;
    static const string priorByID;
    static const string priorLowerBound;
    static const string priorType;
    static const string priorUpperBound;
    static const string priorUseDefault;
    static const string probhapArranger;
    static const string profileByForce;
    static const string profileByID;
    static const string profileprefix;
    static const string profiles;
    static const string progress;
    static const string randomSeed;
    static const string rateCategories;
    static const string recRate;
    static const string reclocFilePrefix;
    static const string recombination;
    static const string recombinationLegal;
    static const string recombinationMaxEvents;
    static const string recombinationRate;
    static const string regGammaShape;
    static const string regionCount;
    static const string regionGamma;
    static const string regionGammaLegal;
    static const string regionGammaShape;
    static const string regionName;
    static const string regionNumbers;
    static const string relativeMuRate;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    static const string relativeSampleRate;
#endif
    static const string removeParamFromGroup;
    static const string removeRangeForTraitModel;
    static const string replicates;
    static const string resultsFileName;
    static const string selectType;
    static const string setOldSeedFromClock;
    static const string simulateData;
    static const string sizeArranger;
    static const string startValue;
    static const string startValueMethod;
    static const string stairArranger;
    static const string systemClock;
    static const string tempAdapt;
    static const string tempInterval;
    static const string theta;
    static const string traceFilePrefix;
    static const string traitAnalysisData;
    static const string traitAnalysisFloat;
    static const string traitAnalysisJump;
    static const string traitAnalysisPartition;
    static const string traitModelName;
    static const string traitModelRangeToPoint;
    static const string treeSumInFileEnabled;
    static const string treeSumInFileName;
    static const string treeSumOutFileEnabled;
    static const string treeSumOutFileName;
    static const string trueValue;
    static const string ungroupedParamsForForce;
    static const string useArgFiles;
    static const string useCurveFiles;
    static const string useDefaultPriorsForForce;
    static const string useGlobalDataModelForAll;
    static const string useGlobalDataModelForOne;
    static const string useNewickTreeFiles;
    static const string useOldSeedFromClock;
    static const string useReclocFiles;
    static const string useTraceFiles;
    static const string userSetTheta;
    static const string validForces;
    static const string validMovingLoci;
    static const string validParamsForForce;
    static const string verbosity;
    static const string wattersonSetTheta;
    static const string xmlOutFileName;
    static const string xmlReportFileName;
    static const string zilchArranger;
};

#endif // UI_STRINGS_H

//____________________________________________________________________________________
