// $Id: setgetmachine.cpp,v 1.96 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <string>

#include "local_build.h"

#include "chainparam_interface.h"
#include "constraint_interface.h"
#include "data_interface.h"
#include "datamodel_interface.h"
#include "errhandling.h"
#include "force_interface.h"
#include "profile_interface.h"
#include "prior_interface.h"
#include "setgetmachine.h"
#include "traitmodel_interface.h"
#include "userparam_interface.h"
#include "vectorx.h"

using namespace std;

//------------------------------------------------------------------------------------

SetGetMachine::SetGetMachine()
{
    Init();
}

SetGetMachine::~SetGetMachine()
{
    map<string,SetGet*>::iterator mapiter;
    for(mapiter=setters.begin(); mapiter != setters.end(); mapiter++)
    {
        delete (*mapiter).second;
    }

}

void SetGetMachine::Init()
{

    addSetter               (new uiAdaptiveTemp());
    addSetter               (new uiAddParamToGroup());
    addSetter               (new uiAddParamToNewGroup());
    addSetter               (new uiAddRangeForTraitModel());
    addSetter               (new uiAlpha());
    addSetter               (new uiAutoCorrelation());
    addSetter               (new uiBaseFrequencies());
    addSetter               (new uiBaseFrequencyA());
    addSetter               (new uiBaseFrequencyC());
    addSetter               (new uiBaseFrequencyG());
    addSetter               (new uiBaseFrequencyT());
    addSetter               (new uiBayesArranger());
    addSetter               (new uiBayesianAnalysis());
    addSetter               (new uiCanHapArrange());
    addSetter               (new uiCategoryCount());
    addSetter               (new uiCategoryProbability());
    addSetter               (new uiCategoryRate());
    addSetter               (new uiCoalescence());
    addSetter               (new uiCoalescenceLegal());
    addSetter               (new uiCoalescenceMaxEvents());
    addSetter               (new uiCrossPartitionCount());
    addSetter               (new uiCurveFileEnabled());
    addSetter               (new uiNewickTreeFileEnabled());
#ifdef LAMARC_QA_TREE_DUMP
    addSetter               (new uiArgFileEnabled());
    addSetter               (new uiManyArgFiles());
#endif // LAMARC_QA_TREE_DUMP
    addSetter               (new uiCurveFilePrefix());
    addSetter               (new uiMapFilePrefix());
    addSetter               (new uiReclocFilePrefix());
    addSetter               (new uiTraceFilePrefix());
    addSetter               (new uiNewickTreeFilePrefix());
#ifdef LAMARC_QA_TREE_DUMP
    addSetter               (new uiArgFilePrefix());
#endif // LAMARC_QA_TREE_DUMP
    addSetter               (new uiDataFileName());
    addSetter               (new uiDataModel());
    addSetter               (new uiDataModelReport());
    addSetter               (new uiDataType());
    addSetter               (new uiDisease());
    addSetter               (new uiDiseaseByID());
    addSetter               (new uiDiseaseGlobal());
    addSetter               (new uiDiseaseInto());
    addSetter               (new uiDiseaseLegal());
    addSetter               (new uiDiseaseLocation());
    addSetter               (new uiDiseaseMaxEvents());
    addSetter               (new uiDiseasePartitionCount());
    addSetter               (new uiDiseasePartitionName());
    addSetter               (new uiDivergence());
    addSetter               (new uiDivergenceLegal());
    addSetter               (new uiDivergenceEpochAncestor());
    addSetter               (new uiDivergenceEpochDescendents());
    addSetter               (new uiDivergenceEpochBoundaryTime());
    addSetter               (new uiDivergenceEpochCount());
    addSetter               (new uiDivergenceEpochName());
    addSetter               (new uiDivMigPartitionCount());
    addSetter               (new uiDivMigPartitionName());
    addSetter               (new uiDivMigration());
    addSetter               (new uiDivMigrationGlobal());
    addSetter               (new uiDivMigrationInto());
    addSetter               (new uiDivMigrationLegal());
    addSetter               (new uiDivMigrationMaxEvents());
    addSetter               (new uiDivMigrationUser());
    addSetter               (new uiDropArranger());
    addSetter               (new uiEpochSizeArranger());
    addSetter               (new uiFinalChains());
    addSetter               (new uiFinalDiscard());
    addSetter               (new uiFinalInterval());
    addSetter               (new uiFinalSamples());
    addSetter               (new uiForceLegal());
    addSetter               (new uiForceOnOff());
    addSetter               (new uiForceProfileType());
    addSetter               (new uiForceProfilesOff());
    addSetter               (new uiForceProfilesOn());
    addSetter               (new uiFreqsFromData());
    addSetter               (new uiFstTheta());
    addSetter               (new uiGTRRateAC());
    addSetter               (new uiGTRRateAG());
    addSetter               (new uiGTRRateAT());
    addSetter               (new uiGTRRateCG());
    addSetter               (new uiGTRRateCT());
    addSetter               (new uiGTRRateGT());
    addSetter               (new uiGTRRates());
    addSetter               (new uiGlobalGrowth());
    addSetter               (new uiGlobalProfileFixed());
    addSetter               (new uiGlobalProfileOff());
    addSetter               (new uiGlobalProfileOn());
    addSetter               (new uiGlobalProfilePercentile());
    addSetter               (new uiGlobalTheta());
    addSetter               (new uiGroupParameterList());
    addSetter               (new uiGroupParameterStatus());
    addSetter               (new uiGroupedParamsForOneForce());
    addSetter               (new uiGrowth());
    addSetter               (new uiGrowthLegal());
    addSetter               (new uiGrowthMaxEvents());
    addSetter               (new uiGrowthType());
    addSetter               (new uiGrowthScheme());
    addSetter               (new uiGrowthUser());
    addSetter               (new uiHapArranger());
    addSetter               (new uiHeatedChain());
    addSetter               (new uiHeatedChainCount());
    addSetter               (new uiHeatedChains());
    addSetter               (new uiInitialChains());
    addSetter               (new uiInitialDiscard());
    addSetter               (new uiInitialInterval());
    addSetter               (new uiInitialSamples());
    addSetter               (new uiLociCount());
    addSetter               (new uiLociNumbers());
    addSetter               (new uiLocusArranger());
    addSetter               (new uiLocusName());
    addSetter               (new uiLogisticSelection());
    addSetter               (new uiLogisticSelectionCoefficientUser());
    addSetter               (new uiLogisticSelectionLegal());
    addSetter               (new uiLogisticSelectionMaxEvents());
    addSetter               (new uiLogisticSelectionType());
    addSetter               (new uiMaxEvents());
    addSetter               (new uiMigPartitionCount());
    addSetter               (new uiMigPartitionName());
    addSetter               (new uiMigration());
    addSetter               (new uiMigrationFst());
    addSetter               (new uiMigrationGlobal());
    addSetter               (new uiMigrationInto());
    addSetter               (new uiMigrationLegal());
    addSetter               (new uiMigrationMaxEvents());
    addSetter               (new uiMigrationUser());
    addSetter               (new uiNormalization());
    addSetter               (new uiNumReps());
    addSetter               (new uiOptimizeAlpha());
    addSetter               (new uiParameterLowerBound());
    addSetter               (new uiParameterPriorType());
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    addSetter               (new uiParameterRelativeSampling());
#endif
    addSetter               (new uiParameterStatus());
    addSetter               (new uiParameterUpperBound());
    addSetter               (new uiParameterUseDefaultPrior());
    addSetter               (new uiPerBaseErrorRate());
    addSetter               (new uiPlotPost());
    addSetter               (new uiPriorByForce());
    addSetter               (new uiPriorById());
    addSetter               (new uiProbHapArranger());
    addSetter               (new uiProfileByForce());
    addSetter               (new uiProfileByID());
    addSetter               (new uiProfilePrefix());
    addSetter               (new uiProgress());
    addSetter               (new uiRandomSeed());
    addSetter               (new uiRecombinationMaxEvents());
    addSetter               (new uiRecombinationRate());
    addSetter               (new uiRecombine());
    addSetter               (new uiRecombineLegal());
    addSetter               (new uiRegionEffectivePopSize());
    addSetter               (new uiRegionName());
    addSetter               (new uiRegionGamma());
    addSetter               (new uiRegionGammaLegal());
    addSetter               (new uiRegionGammaShape());
    addSetter               (new uiRegionNumbers());
    addSetter               (new uiRelativeMuRate());
    addSetter               (new uiRemoveParamFromGroup());
    addSetter               (new uiRemoveRangeForTraitModel());
    addSetter               (new uiResultsFileName());
    addSetter               (new uiSetOldClockSeed());
    addSetter               (new uiSetTraitModelRangeToPoint());
    addSetter               (new uiSimulateData());
    addSetter               (new uiSizeArranger());
    addSetter               (new uiStairArranger());
    addSetter               (new uiStartValue());
    addSetter               (new uiStartValueMethod());
    addSetter               (new uiSystemClock());
    addSetter               (new uiTTRatio());
    addSetter               (new uiTempInterval());
    addSetter               (new uiReclocFileEnabled());
    addSetter               (new uiTraceFileEnabled());
    addSetter               (new uiTraitModelData());
    addSetter               (new uiTraitModelFloat());
    addSetter               (new uiTraitModelJump());
    addSetter               (new uiTraitModelName());
    addSetter               (new uiTraitModelPartition());
    addSetter               (new uiTreeSumInFileEnabled());
    addSetter               (new uiTreeSumInFileName());
    addSetter               (new uiTreeSumOutFileEnabled());
    addSetter               (new uiTreeSumOutFileName());
    addSetter               (new uiTrueValue());
    addSetter               (new uiUngroupedParamsForOneForce());
    addSetter               (new uiUseDefaultPriorsForForce());
    addSetter               (new uiUseGlobalDataModelForAll());
    addSetter               (new uiUseGlobalDataModelForOne());
    addSetter               (new uiUseOldClockSeed());
    addSetter               (new uiUserTheta());
    addSetter               (new uiValidForces());
    addSetter               (new uiValidMovingLoci());
    addSetter               (new uiValidParamsForOneForce());
    addSetter               (new uiVerbosity());
    addSetter               (new uiWattersonTheta());
    addSetter               (new uiXMLOutFileName());
    addSetter               (new uiXMLReportFileName());
    addSetter               (new uiZilchArranger());
}

void SetGetMachine::addSetter(SetGet * setter)
{
    string key = setter->UIKey();
    assert(setters.find(key) == setters.end()); // make sure we don't add it twice!
    setters[key] = setter;
}

bool SetGetMachine::doGetBool(string variable, UIVars& vars, UIId id)
{
    return doGet<bool>(variable,vars,id,setters);
}

data_type SetGetMachine::doGetDataType(string variable, UIVars& vars, UIId id)
{
    return doGet<data_type>(variable,vars,id,setters);
}

double SetGetMachine::doGetDouble(string variable, UIVars& vars, UIId id)
{
    return doGet<double>(variable,vars,id,setters);
}

DoubleVec1d SetGetMachine::doGetDoubleVec1d(string variable, UIVars& vars, UIId id)
{
    return doGet<DoubleVec1d>(variable,vars,id,setters);
}

force_type SetGetMachine::doGetForceType(string variable, UIVars& vars, UIId id)
{
    return doGet<force_type>(variable,vars,id,setters);
}

ForceTypeVec1d SetGetMachine::doGetForceTypeVec1d(string variable, UIVars& vars, UIId id)
{
    return doGet<ForceTypeVec1d>(variable,vars,id,setters);
}

long SetGetMachine::doGetLong(string variable, UIVars& vars, UIId id)
{
    return doGet<long>(variable,vars,id,setters);
}

LongVec1d SetGetMachine::doGetLongVec1d(string variable, UIVars& vars, UIId id)
{
    return doGet<LongVec1d>(variable,vars,id,setters);
}

method_type SetGetMachine::doGetMethodType(string variable, UIVars& vars, UIId id)
{
    return doGet<method_type>(variable,vars,id,setters);
}

model_type SetGetMachine::doGetModelType(string variable, UIVars& vars, UIId id)
{
    return doGet<model_type>(variable,vars,id,setters);
}

proftype SetGetMachine::doGetProftype(string variable, UIVars& vars, UIId id)
{
    return doGet<proftype>(variable,vars,id,setters);
}

string SetGetMachine::doGetString(string variable, UIVars& vars, UIId id)
{
    return doGet<string>(variable,vars,id,setters);
}

StringVec1d SetGetMachine::doGetStringVec1d(string variable, UIVars& vars, UIId id)
{
    return doGet<StringVec1d>(variable,vars,id,setters);
}

verbosity_type SetGetMachine::doGetVerbosityType(string variable, UIVars& vars, UIId id)
{
    return doGet<verbosity_type>(variable,vars,id,setters);
}

UIIdVec1d
SetGetMachine::doGetUIIdVec1d(string variable, UIVars& vars, UIId id)
{
    return doGet<UIIdVec1d>(variable,vars,id,setters);
}

UIIdVec2d
SetGetMachine::doGetUIIdVec2d(string variable, UIVars& vars, UIId id)
{
    return doGet<UIIdVec2d>(variable,vars,id,setters);
}

string SetGetMachine::doGetPrintString(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    return setter->GetPrintString(vars,id);
}

string SetGetMachine::doGetDescription(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    return setter->Description(vars,id);
}

bool SetGetMachine::doGetConsistency(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    return setter->IsConsistent(vars,id);
}

string SetGetMachine::doGetMin(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    return setter->Min(vars,id);
}

string SetGetMachine::doGetMax(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    return setter->Max(vars,id);
}

void SetGetMachine::doSet(string variable, UIVars& vars, UIId id, string value)
{
    SetGet * setter = setters[variable];

    // set variables
    setter->SetFromString(vars,id,value);

}

void SetGetMachine::doToggle(string variable, UIVars& vars, UIId id)
{
    SetGet * setter = setters[variable];
    string nextValue = setter->NextToggleValue(vars,id);
    doSet(variable,vars,id,nextValue);
}

//____________________________________________________________________________________
