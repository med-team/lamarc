// $Id: userparam_interface.cpp,v 1.36 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <iostream>

#include "local_build.h"

#include "ui_interface.h"
#include "ui_strings.h"
#include "ui_vars.h"
#include "userparam_interface.h"

//------------------------------------------------------------------------------------
// Data File
//------------------------------------------------------------------------------------

uiDataFileName::uiDataFileName()
    : GetString(uistr::dataFileName)
{
}

uiDataFileName::~uiDataFileName()
{
}

string uiDataFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetDataFileName();
}

//------------------------------------------------------------------------------------
// Posterior Plot
//------------------------------------------------------------------------------------

uiPlotPost::uiPlotPost()
    : SetGetBool(uistr::plotPost)
{
}

uiPlotPost::~uiPlotPost()
{
}

bool uiPlotPost::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetPlotPost();
}

void uiPlotPost::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetPlotPost(val);
}

//------------------------------------------------------------------------------------
// Progress
//------------------------------------------------------------------------------------

uiProgress::uiProgress()
    : SetGetVerbosityType(uistr::progress)
{
}

uiProgress::~uiProgress()
{
}

verbosity_type uiProgress::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetProgress();
}

void uiProgress::Set(UIVars& vars, UIId id, verbosity_type val)
{
    vars.userparams.SetProgress(val);
}

//------------------------------------------------------------------------------------
// Random Seed
//------------------------------------------------------------------------------------

uiRandomSeed::uiRandomSeed()
    : SetGetLong(uistr::randomSeed)
{
}

uiRandomSeed::~uiRandomSeed()
{
}

long uiRandomSeed::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetRandomSeed();
}

void uiRandomSeed::Set(UIVars& vars, UIId id, long val)
{
    vars.userparams.SetRandomSeed(val);
}

//------------------------------------------------------------------------------------
// Results File
//------------------------------------------------------------------------------------

uiResultsFileName::uiResultsFileName()
    : SetGetString(uistr::resultsFileName)
{
}

uiResultsFileName::~uiResultsFileName()
{
}

string uiResultsFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetResultsFileName();
}

void uiResultsFileName::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetResultsFileName(val);
}

//------------------------------------------------------------------------------------
// System Clock Random Seed
//------------------------------------------------------------------------------------

uiSystemClock::uiSystemClock()
    : SetGetBool(uistr::systemClock)
{
}

uiSystemClock::~uiSystemClock()
{
}

bool uiSystemClock::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetUseSystemClock();
}

void uiSystemClock::Set(UIVars& vars, UIId id, bool val)
{
    // Ignore "val" and always set true
    vars.userparams.SetUseSystemClock(true);
}

//------------------------------------------------------------------------------------
// Set Old Clock Random Seed
//------------------------------------------------------------------------------------

uiSetOldClockSeed::uiSetOldClockSeed()
    : SetGetLong(uistr::setOldSeedFromClock)
{
}

uiSetOldClockSeed::~uiSetOldClockSeed()
{
}

long uiSetOldClockSeed::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetOldClockSeed();
}

void uiSetOldClockSeed::Set(UIVars& vars, UIId id, long val)
{
    vars.userparams.SetOldClockSeed(val);
}

//------------------------------------------------------------------------------------
// Use Old Clock Random Seed
//------------------------------------------------------------------------------------

uiUseOldClockSeed::uiUseOldClockSeed()
    : SetGetNoval(uistr::useOldSeedFromClock)
{
}

uiUseOldClockSeed::~uiUseOldClockSeed()
{
}

void uiUseOldClockSeed::Set(UIVars& vars, UIId id, noval val)
{
    vars.userparams.SetUseOldClockSeed(true);
}

//------------------------------------------------------------------------------------
// TreeSumInFileEnabled
//------------------------------------------------------------------------------------

uiTreeSumInFileEnabled::uiTreeSumInFileEnabled()
    : SetGetBoolEnabled( uistr::treeSumInFileEnabled)
{
}

uiTreeSumInFileEnabled::~uiTreeSumInFileEnabled()
{
}

bool uiTreeSumInFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetReadSumFile();
}

void uiTreeSumInFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetReadSumFile(val);
}

//------------------------------------------------------------------------------------
// Tree Input Summary File
//------------------------------------------------------------------------------------

uiTreeSumInFileName::uiTreeSumInFileName()
    : SetGetString( uistr::treeSumInFileName)
{
}

uiTreeSumInFileName::~uiTreeSumInFileName()
{
}

string uiTreeSumInFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetTreeSumInFileName();
}

void uiTreeSumInFileName::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetTreeSumInFileName(val);
}

//------------------------------------------------------------------------------------
// TreeSumOutFileEnabled
//------------------------------------------------------------------------------------

uiTreeSumOutFileEnabled::uiTreeSumOutFileEnabled()
    : SetGetBoolEnabled( uistr::treeSumOutFileEnabled)
{
}

uiTreeSumOutFileEnabled::~uiTreeSumOutFileEnabled()
{
}

bool uiTreeSumOutFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteSumFile();
}

void uiTreeSumOutFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteSumFile(val);
}

//------------------------------------------------------------------------------------
// Tree Output Summary File
//------------------------------------------------------------------------------------

uiTreeSumOutFileName::uiTreeSumOutFileName()
    : SetGetString( uistr::treeSumOutFileName)
{
}

uiTreeSumOutFileName::~uiTreeSumOutFileName()
{
}

string uiTreeSumOutFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetTreeSumOutFileName();
}

void uiTreeSumOutFileName::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetTreeSumOutFileName(val);
}

//------------------------------------------------------------------------------------
// CurveFileEnabled
//------------------------------------------------------------------------------------

uiCurveFileEnabled::uiCurveFileEnabled()
    : SetGetBoolEnabled( uistr::useCurveFiles)
{
}

uiCurveFileEnabled::~uiCurveFileEnabled()
{
}

bool uiCurveFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteCurveFiles();
}

void uiCurveFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteCurveFiles(val);
}

//------------------------------------------------------------------------------------
// ReclocFileEnabled
//------------------------------------------------------------------------------------

uiReclocFileEnabled::uiReclocFileEnabled()
    : SetGetBoolEnabled( uistr::useReclocFiles)
{
}

uiReclocFileEnabled::~uiReclocFileEnabled()
{
}

bool uiReclocFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteReclocFiles();
}

void uiReclocFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteReclocFiles(val);
}

//------------------------------------------------------------------------------------
// TraceFileEnabled
//------------------------------------------------------------------------------------

uiTraceFileEnabled::uiTraceFileEnabled()
    : SetGetBoolEnabled( uistr::useTraceFiles)
{
}

uiTraceFileEnabled::~uiTraceFileEnabled()
{
}

bool uiTraceFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteTraceFiles();
}

void uiTraceFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteTraceFiles(val);
}

//------------------------------------------------------------------------------------
// NewickTreeFileEnabled
//------------------------------------------------------------------------------------

uiNewickTreeFileEnabled::uiNewickTreeFileEnabled()
    : SetGetBoolEnabled( uistr::useNewickTreeFiles)
{
}

uiNewickTreeFileEnabled::~uiNewickTreeFileEnabled()
{
}

bool uiNewickTreeFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteNewickTreeFiles();
}

void uiNewickTreeFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteNewickTreeFiles(val);
}

//------------------------------------------------------------------------------------
// ArgFileEnabled
//------------------------------------------------------------------------------------

#ifdef LAMARC_QA_TREE_DUMP

uiArgFileEnabled::uiArgFileEnabled()
    : SetGetBoolEnabled( uistr::useArgFiles)
{
}

uiArgFileEnabled::~uiArgFileEnabled()
{
}

bool uiArgFileEnabled::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteArgFiles();
}

void uiArgFileEnabled::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteArgFiles(val);
}

//------------------------------------------------------------------------------------
// ManyArgFiles
//------------------------------------------------------------------------------------

uiManyArgFiles::uiManyArgFiles()
    : SetGetBoolEnabled( uistr::manyArgFiles)
{
}

uiManyArgFiles::~uiManyArgFiles()
{
}

bool uiManyArgFiles::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetWriteManyArgs();
}

void uiManyArgFiles::Set(UIVars& vars, UIId id, bool val)
{
    vars.userparams.SetWriteManyArgs(val);
}

#endif // LAMARC_QA_TREE_DUMP

//------------------------------------------------------------------------------------
// Curve File Output Name's Prefix
//------------------------------------------------------------------------------------

uiCurveFilePrefix::uiCurveFilePrefix()
    : SetGetString( uistr::curveFilePrefix)
{
}

uiCurveFilePrefix::~uiCurveFilePrefix()
{
}

string uiCurveFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetCurveFilePrefix();
}

void uiCurveFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetCurveFilePrefix(val);
}

//------------------------------------------------------------------------------------
// Curve File Output Name's Prefix
//------------------------------------------------------------------------------------

uiMapFilePrefix::uiMapFilePrefix()
    : SetGetString( uistr::mapFilePrefix)
{
}

uiMapFilePrefix::~uiMapFilePrefix()
{
}

string uiMapFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetMapFilePrefix();
}

void uiMapFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetMapFilePrefix(val);
}

//------------------------------------------------------------------------------------
// Profile Output Name's Prefix
//------------------------------------------------------------------------------------

uiProfilePrefix::uiProfilePrefix()
    : SetGetString( uistr::profileprefix)
{
}

uiProfilePrefix::~uiProfilePrefix()
{
}

string uiProfilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetProfilePrefix();
}

void uiProfilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetProfilePrefix(val);
}

//------------------------------------------------------------------------------------
// Recloc File Output Name's Prefix
//------------------------------------------------------------------------------------

uiReclocFilePrefix::uiReclocFilePrefix()
    : SetGetString( uistr::reclocFilePrefix)
{
}

uiReclocFilePrefix::~uiReclocFilePrefix()
{
}

string uiReclocFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetReclocFilePrefix();
}

void uiReclocFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetReclocFilePrefix(val);
}

//------------------------------------------------------------------------------------
// Trace File Output Name's Prefix
//------------------------------------------------------------------------------------

uiTraceFilePrefix::uiTraceFilePrefix()
    : SetGetString( uistr::traceFilePrefix)
{
}

uiTraceFilePrefix::~uiTraceFilePrefix()
{
}

string uiTraceFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetTraceFilePrefix();
}

void uiTraceFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetTraceFilePrefix(val);
}

//------------------------------------------------------------------------------------
// NewickTree File Output Name's Prefix
//------------------------------------------------------------------------------------

uiNewickTreeFilePrefix::uiNewickTreeFilePrefix()
    : SetGetString( uistr::newickTreeFilePrefix)
{
}

uiNewickTreeFilePrefix::~uiNewickTreeFilePrefix()
{
}

string uiNewickTreeFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetNewickTreeFilePrefix();
}

void uiNewickTreeFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetNewickTreeFilePrefix(val);
}

//------------------------------------------------------------------------------------
// ARG File Output Name's Prefix
//------------------------------------------------------------------------------------

#ifdef LAMARC_QA_TREE_DUMP

uiArgFilePrefix::uiArgFilePrefix()
    : SetGetString( uistr::argFilePrefix)
{
}

uiArgFilePrefix::~uiArgFilePrefix()
{
}

string uiArgFilePrefix::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetArgFilePrefix();
}

void uiArgFilePrefix::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetArgFilePrefix(val);
}

#endif // LAMARC_QA_TREE_DUMP

//------------------------------------------------------------------------------------
// Verbosity
//------------------------------------------------------------------------------------

uiVerbosity::uiVerbosity()
    : SetGetVerbosityTypeNoNone(uistr::verbosity)
{
}

uiVerbosity::~uiVerbosity()
{
}

verbosity_type uiVerbosity::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetVerbosity();
}

void uiVerbosity::Set(UIVars& vars, UIId id, verbosity_type val)
{
    vars.userparams.SetVerbosity(val);
}

//------------------------------------------------------------------------------------
// XML Output File
//------------------------------------------------------------------------------------

uiXMLOutFileName::uiXMLOutFileName()
    : SetGetString(uistr::xmlOutFileName)
{
}

uiXMLOutFileName::~uiXMLOutFileName()
{
}

string uiXMLOutFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetXMLOutFileName();
}

void uiXMLOutFileName::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetXMLOutFileName(val);
}

//------------------------------------------------------------------------------------
// XML Output File
//------------------------------------------------------------------------------------

uiXMLReportFileName::uiXMLReportFileName()
    : SetGetString(uistr::xmlReportFileName)
{
}

uiXMLReportFileName::~uiXMLReportFileName()
{
}

string uiXMLReportFileName::Get(UIVars& vars, UIId id)
{
    return vars.userparams.GetXMLReportFileName();
}

void uiXMLReportFileName::Set(UIVars& vars, UIId id, string val)
{
    vars.userparams.SetXMLReportFileName(val);
}

//____________________________________________________________________________________
