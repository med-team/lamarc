// $Id: front_end_warnings.h,v 1.4 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef FRONT_END_WARNINGS_H
#define FRONT_END_WARNINGS_H

#include <string>
#include <vector>

class FrontEndWarnings
{
  private:
    std::vector<std::string>    m_warnings;

  public:
    FrontEndWarnings();
    virtual ~FrontEndWarnings();
    std::vector<std::string> GetAndClearWarnings();
    void      AddWarning(std::string warnmsg);

};

#endif  // FRONT_END_WARNINGS

//____________________________________________________________________________________
