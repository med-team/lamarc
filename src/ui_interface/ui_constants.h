// $Id: ui_constants.h,v 1.13 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_CONSTANTS_H
#define UI_CONSTANTS_H

enum ui_param_class { uipsingle, untouched, global };

class uiconst
{
  public:
    static const long NO_ID;
    static const long GLOBAL_ID;
    static const long GLOBAL_DATAMODEL_NUC_ID;
    static const long GLOBAL_DATAMODEL_MSAT_ID;
    static const long GLOBAL_DATAMODEL_KALLELE_ID;
    static const long diseaseColumns;
    static const long migColumns;
};

#endif // UI_CONSTANTS_H

//____________________________________________________________________________________
