// $Id: userparam_interface.h,v 1.39 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef USERPARAM_INTERFACE_H
#define USERPARAM_INTERFACE_H

#include <string>

#include "local_build.h"

#include "setget.h"

class UIVars;

class uiDataFileName : public GetString
{
  public:
    uiDataFileName();
    virtual ~uiDataFileName();
    string Get(UIVars& vars, UIId id);
};

class uiPlotPost : public SetGetBool
{
  public:
    uiPlotPost();
    virtual ~uiPlotPost();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiProgress : public SetGetVerbosityType
{
  public:
    uiProgress();
    virtual ~uiProgress();
    verbosity_type Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,verbosity_type val);
};

class uiRandomSeed : public SetGetLong
{
  public:
    uiRandomSeed();
    virtual ~uiRandomSeed();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiSetOldClockSeed : public SetGetLong
{
  public:
    uiSetOldClockSeed();
    virtual ~uiSetOldClockSeed();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiResultsFileName : public SetGetString
{
  public:
    uiResultsFileName();
    virtual ~uiResultsFileName();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

// actually, it doesn't behave like a SetGetBool
// because you set it with this, but unset it with
// uiRandomSeed or uiUseOldClockSeed
class uiSystemClock : public SetGetBool
{
  public:
    uiSystemClock();
    virtual ~uiSystemClock();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, bool val);
};

// actually, it doesn't behave like a SetGetBool
// because you set it with this, but unset it with
// uiRandomSeed or uiSystemClock
class uiUseOldClockSeed : public SetGetNoval
{
  public:
    uiUseOldClockSeed();
    virtual ~uiUseOldClockSeed();
    void Set(UIVars& vars, UIId id, noval val);
};

class uiTreeSumInFileEnabled : public SetGetBoolEnabled
{
  public:
    uiTreeSumInFileEnabled();
    virtual ~uiTreeSumInFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiTreeSumInFileName : public SetGetString
{
  public:
    uiTreeSumInFileName();
    virtual ~uiTreeSumInFileName();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

//Used to need this when you were either reading or writing,
// but now you can do both/either.

class uiTreeSumOutFileEnabled : public SetGetBoolEnabled
{
  public:
    uiTreeSumOutFileEnabled();
    virtual ~uiTreeSumOutFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiTreeSumOutFileName : public SetGetString
{
  public:
    uiTreeSumOutFileName();
    virtual ~uiTreeSumOutFileName();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiCurveFileEnabled : public SetGetBoolEnabled
{
  public:
    uiCurveFileEnabled();
    virtual ~uiCurveFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiReclocFileEnabled : public SetGetBoolEnabled
{
  public:
    uiReclocFileEnabled();
    virtual ~uiReclocFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiTraceFileEnabled : public SetGetBoolEnabled
{
  public:
    uiTraceFileEnabled();
    virtual ~uiTraceFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiNewickTreeFileEnabled : public SetGetBoolEnabled
{
  public:
    uiNewickTreeFileEnabled();
    virtual ~uiNewickTreeFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

#ifdef LAMARC_QA_TREE_DUMP

class uiArgFileEnabled : public SetGetBoolEnabled
{
  public:
    uiArgFileEnabled();
    virtual ~uiArgFileEnabled();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

class uiManyArgFiles : public SetGetBoolEnabled
{
  public:
    uiManyArgFiles();
    virtual ~uiManyArgFiles();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,bool val);
};

#endif // LAMARC_QA_TREE_DUMP

class uiCurveFilePrefix : public SetGetString
{
  public:
    uiCurveFilePrefix();
    virtual ~uiCurveFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiMapFilePrefix : public SetGetString
{
  public:
    uiMapFilePrefix();
    virtual ~uiMapFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiProfilePrefix : public SetGetString
{
  public:
    uiProfilePrefix();
    virtual ~uiProfilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiReclocFilePrefix : public SetGetString
{
  public:
    uiReclocFilePrefix();
    virtual ~uiReclocFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiTraceFilePrefix : public SetGetString
{
  public:
    uiTraceFilePrefix();
    virtual ~uiTraceFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiNewickTreeFilePrefix : public SetGetString
{
  public:
    uiNewickTreeFilePrefix();
    virtual ~uiNewickTreeFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

#ifdef LAMARC_QA_TREE_DUMP

class uiArgFilePrefix : public SetGetString
{
  public:
    uiArgFilePrefix();
    virtual ~uiArgFilePrefix();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

#endif // LAMARC_QA_TREE_DUMP

class uiVerbosity : public SetGetVerbosityTypeNoNone
{
  public:
    uiVerbosity();
    virtual ~uiVerbosity();
    verbosity_type Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,verbosity_type val);
};

class uiXMLOutFileName : public SetGetString
{
  public:
    uiXMLOutFileName();
    virtual ~uiXMLOutFileName();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

class uiXMLReportFileName : public SetGetString
{
  public:
    uiXMLReportFileName();
    virtual ~uiXMLReportFileName();
    string Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,string val);
};

#endif  // USERPARAM_INTERFACE_H

//____________________________________________________________________________________
