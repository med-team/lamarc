// $Id: data_interface.cpp,v 1.34 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <iostream>

#include "data_interface.h"
#include "setget.h"
#include "ui_vars.h"
#include "ui_strings.h"
#include "vectorx.h"

//------------------------------------------------------------------------------------

long uiCrossPartitionCount::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetNCrossPartitions();
}

uiCrossPartitionCount::uiCrossPartitionCount()
    : GetLong(uistr::crossPartitionCount)
{
}

uiCrossPartitionCount::~uiCrossPartitionCount()
{
}

//------------------------------------------------------------------------------------

long uiDivMigPartitionCount::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetNPartitionsByForceType(force_DIVMIG);
}

uiDivMigPartitionCount::uiDivMigPartitionCount()
    : GetLong(uistr::divmigrationPartitionCount)
{
}

uiDivMigPartitionCount::~uiDivMigPartitionCount()
{
}

//------------------------------------------------------------------------------------

string uiDivMigPartitionName::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetForcePartitionName(force_DIVMIG,id.GetIndex1());
}

uiDivMigPartitionName::uiDivMigPartitionName()
    : GetString(uistr::divmigrationPartitionName)
{
}

uiDivMigPartitionName::~uiDivMigPartitionName()
{
}

//------------------------------------------------------------------------------------

long uiMigPartitionCount::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetNPartitionsByForceType(force_MIG);
}

uiMigPartitionCount::uiMigPartitionCount()
    : GetLong(uistr::migrationPartitionCount)
{
}

uiMigPartitionCount::~uiMigPartitionCount()
{
}

//------------------------------------------------------------------------------------

string uiMigPartitionName::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetForcePartitionName(force_MIG,id.GetIndex1());
}

uiMigPartitionName::uiMigPartitionName()
    : GetString(uistr::migrationPartitionName)
{
}

uiMigPartitionName::~uiMigPartitionName()
{
}

//------------------------------------------------------------------------------------

long uiDiseasePartitionCount::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetNPartitionsByForceType(force_DISEASE);
}

uiDiseasePartitionCount::uiDiseasePartitionCount()
    : GetLong(uistr::diseasePartitionCount)
{
}

uiDiseasePartitionCount::~uiDiseasePartitionCount()
{
}

//------------------------------------------------------------------------------------

string uiDiseasePartitionName::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetForcePartitionName(force_DISEASE,id.GetIndex1());
}

uiDiseasePartitionName::uiDiseasePartitionName()
    : GetString(uistr::diseasePartitionName)
{
}

uiDiseasePartitionName::~uiDiseasePartitionName()
{
}

//------------------------------------------------------------------------------------

LongVec1d uiLociNumbers::Get(UIVars& vars, UIId id)
{
    long count = vars.datapackplus.GetNumLoci(id.GetIndex1());
    LongVec1d longVec;
    for(long i= 0; i< count; i++)
    {
        longVec.push_back(i);
    }
    return longVec;
}

uiLociNumbers::uiLociNumbers()
    : GetLongVec1d(uistr::lociNumbers)
{
}

uiLociNumbers::~uiLociNumbers()
{
}

//------------------------------------------------------------------------------------

LongVec1d uiRegionNumbers::Get(UIVars& vars, UIId id)
{
    long count = vars.datapackplus.GetNumRegions();
    LongVec1d longVec;
    for(long i= 0; i< count; i++)
    {
        longVec.push_back(i);
    }
    return longVec;
}

uiRegionNumbers::uiRegionNumbers()
    : GetLongVec1d(uistr::regionNumbers)
{
}

uiRegionNumbers::~uiRegionNumbers()
{
}

//------------------------------------------------------------------------------------

uiRegionEffectivePopSize::uiRegionEffectivePopSize()
    : SetGetDouble(uistr::effectivePopSize)
{
}

uiRegionEffectivePopSize::~uiRegionEffectivePopSize()
{
}

double uiRegionEffectivePopSize::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetEffectivePopSize(id.GetIndex1());
}

void uiRegionEffectivePopSize::Set(UIVars& vars, UIId id, double size)
{
    vars.datapackplus.SetEffectivePopSize(id.GetIndex1(), size);
}

//------------------------------------------------------------------------------------

uiSimulateData::uiSimulateData()
    : SetGetBool(uistr::simulateData)
{
}

uiSimulateData::~uiSimulateData()
{
}

bool uiSimulateData::Get(UIVars& vars, UIId id)
{
    return vars.datapackplus.GetSimulateData(id.GetIndex1(), id.GetIndex2());
}

void uiSimulateData::Set(UIVars& vars, UIId id, bool sim)
{
    vars.datapackplus.SetSimulateData(id.GetIndex1(), id.GetIndex2(), sim);
}

//____________________________________________________________________________________
