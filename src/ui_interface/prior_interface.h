// $Id: prior_interface.h,v 1.9 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PRIOR_INTERFACE_H
#define PRIOR_INTERFACE_H

#include <string>
#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
#include "setget.h"

class UIVars;

class uiParameterUseDefaultPrior : public SetGetBool
{
  public:
    uiParameterUseDefaultPrior();
    virtual ~uiParameterUseDefaultPrior();
    virtual bool     Get(UIVars& vars, UIId id);
    virtual void     Set(UIVars& vars, UIId id, bool val);
};

class uiParameterPriorType : public SetGetPriorType
{
  public:
    uiParameterPriorType();
    virtual ~uiParameterPriorType();
    virtual priortype   Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, priortype val);
};

//------------------------------------------------------------------------------------

class uiParameterLowerBound : public SetGetDouble
{
  public:
    uiParameterLowerBound();
    virtual ~uiParameterLowerBound();
    virtual double      Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, double val);
    virtual string      Min(UIVars& vars, UIId id);
    virtual string      Max(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiParameterUpperBound : public SetGetDouble
{
  public:
    uiParameterUpperBound();
    virtual ~uiParameterUpperBound();
    virtual double      Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, double val);
    virtual string      Min(UIVars& vars, UIId id);
    virtual string      Max(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
class uiParameterRelativeSampling : public SetGetLong
{
  public:
    uiParameterRelativeSampling();
    virtual ~uiParameterRelativeSampling();
    virtual long        Get(UIVars& vars, UIId id);
    virtual void        Set(UIVars& vars, UIId id, long val);
};
#endif

//------------------------------------------------------------------------------------

class uiPriorByForce : public SetGetNoval
{
  public:
    uiPriorByForce();
    virtual ~uiPriorByForce();
    string Description(UIVars& vars, UIId id);
    string GetPrintString(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, noval val) {};
};

class uiPriorById : public SetGetNoval
{
  public:
    uiPriorById();
    virtual ~uiPriorById();
    string Description(UIVars& vars, UIId id);
    string GetPrintString(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, noval val) {};
};

class uiUseDefaultPriorsForForce : public SetGetNoval
{
  public:
    uiUseDefaultPriorsForForce();
    virtual ~uiUseDefaultPriorsForForce();
    void Set(UIVars& vars, UIId id, noval val);
    string Description(UIVars& vars, UIId id);
};

#endif  // PRIOR_INTERFACE_H

//____________________________________________________________________________________
