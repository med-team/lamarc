// $Id: front_end_warnings.cpp,v 1.4 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "front_end_warnings.h"
#include "vectorx.h"

FrontEndWarnings::FrontEndWarnings()
{
}

FrontEndWarnings::~FrontEndWarnings()
{
}

StringVec1d
FrontEndWarnings::GetAndClearWarnings()
{
    StringVec1d warnings = m_warnings;
    m_warnings.clear();
    return warnings;
}

void
FrontEndWarnings::AddWarning(std::string warnmsg)
{
    for (unsigned long wnum=0; wnum<m_warnings.size(); wnum++)
    {
        if (m_warnings[wnum]==warnmsg) return;
    }
    m_warnings.push_back(warnmsg);
}

//____________________________________________________________________________________
