// $Id: chainparam_interface.cpp,v 1.37 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <iostream>
#include "arranger.h"
#include "chainparam.h"
#include "chainparam_interface.h"
#include "constants.h"
#include "stringx.h"
#include "ui_strings.h"
#include "ui_vars.h"

using namespace std;

/// uiFinalChains

uiFinalChains::uiFinalChains()
    : SetGetLong(uistr::finalChains)
{
}

uiFinalChains::~uiFinalChains()
{
}

long uiFinalChains::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetFinalNumberOfChains();
}

void uiFinalChains::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetFinalNumberOfChains(val);
}

/// uiFinalDiscard

uiFinalDiscard::uiFinalDiscard()
    : SetGetLong(uistr::finalDiscard)
{
}

uiFinalDiscard::~uiFinalDiscard()
{
}

long uiFinalDiscard::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetFinalNumberOfChainsToDiscard();
}

void uiFinalDiscard::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetFinalNumberOfChainsToDiscard(val);
}

/// uiFinalSamples

uiFinalSamples::uiFinalSamples()
    : SetGetLong(uistr::finalSamples)
{
}

uiFinalSamples::~uiFinalSamples()
{
}

long uiFinalSamples::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetFinalNumberOfSamples();
}

void uiFinalSamples::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetFinalNumberOfSamples(val);
}

/// uiFinalInterval

uiFinalInterval::uiFinalInterval()
    : SetGetLong(uistr::finalInterval)
{
}

uiFinalInterval::~uiFinalInterval()
{
}

long uiFinalInterval::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetFinalChainSamplingInterval();
}

void uiFinalInterval::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetFinalChainSamplingInterval(val);
}

/// uiInitialChains

uiInitialChains::uiInitialChains()
    : SetGetLong(uistr::initialChains)
{
}

uiInitialChains::~uiInitialChains()
{
}

long uiInitialChains::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetInitialNumberOfChains();
}

void uiInitialChains::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetInitialNumberOfChains(val);
}

/// uiInitialDiscard

uiInitialDiscard::uiInitialDiscard()
    : SetGetLong(uistr::initialDiscard)
{
}

uiInitialDiscard::~uiInitialDiscard()
{
}

long uiInitialDiscard::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetInitialNumberOfChainsToDiscard();
}

void uiInitialDiscard::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetInitialNumberOfChainsToDiscard(val);
}

/// uiInitialSamples

uiInitialSamples::uiInitialSamples()
    : SetGetLong(uistr::initialSamples)
{
}

uiInitialSamples::~uiInitialSamples()
{
}

long uiInitialSamples::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetInitialNumberOfSamples();
}

void uiInitialSamples::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetInitialNumberOfSamples(val);
}

/// uiInitialInterval

uiInitialInterval::uiInitialInterval()
    : SetGetLong(uistr::initialInterval)
{
}

uiInitialInterval::~uiInitialInterval()
{
}

long uiInitialInterval::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetInitialChainSamplingInterval();
}

void uiInitialInterval::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetInitialChainSamplingInterval(val);
}

/// uiHeatedChain

uiHeatedChain::uiHeatedChain()
    : SetGetDouble(uistr::heatedChain)
{
}

uiHeatedChain::~uiHeatedChain()
{
}

double uiHeatedChain::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetChainTemperature(id.GetIndex1());
}

void uiHeatedChain::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetChainTemperature(val,id.GetIndex1());
}

/// uiHeatedChains

uiHeatedChains::uiHeatedChains()
    : GetDoubleVec1d(uistr::heatedChains)
{
}

uiHeatedChains::~uiHeatedChains()
{
}

DoubleVec1d uiHeatedChains::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetChainTemperatures();
}

/// uiHeatedChainCount

uiHeatedChainCount::uiHeatedChainCount()
    : SetGetLong(uistr::heatedChainCount)
{
}

uiHeatedChainCount::~uiHeatedChainCount()
{
}

long uiHeatedChainCount::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetChainCount();
}

void uiHeatedChainCount::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetChainCount(val);
}

/// uiTempInterval

uiTempInterval::uiTempInterval()
    : SetGetLong(uistr::tempInterval)
{
}

uiTempInterval::~uiTempInterval()
{
}

long uiTempInterval::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetTemperatureInterval();
}

void uiTempInterval::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetTemperatureInterval(val);
}

/// uiAdaptiveTemp

uiAdaptiveTemp::uiAdaptiveTemp()
    : SetGetBool(uistr::tempAdapt)
{
}

uiAdaptiveTemp::~uiAdaptiveTemp()
{
}

bool uiAdaptiveTemp::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetAdaptiveTemperatures();
}

void uiAdaptiveTemp::Set(UIVars& vars, UIId id, bool val)
{
    vars.chains.SetAdaptiveTemperatures(val);
}

/// uiNumReps

uiNumReps::uiNumReps()
    : SetGetLong(uistr::replicates)
{
}

uiNumReps::~uiNumReps()
{
}

long uiNumReps::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetNumberOfReplicates();
}

void uiNumReps::Set(UIVars& vars, UIId id, long val)
{
    vars.chains.SetNumberOfReplicates(val);
}

/// uiDropArranger

uiDropArranger::uiDropArranger()
    : SetGetDouble(uistr::dropArranger)
{
}

uiDropArranger::~uiDropArranger()
{
}

double uiDropArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetDropArrangerRelativeTiming();
}

void uiDropArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetDropArrangerRelativeTiming(val);
}

/// uiSizeArranger

uiSizeArranger::uiSizeArranger()
    : SetGetDouble(uistr::sizeArranger)
{
}

uiSizeArranger::~uiSizeArranger()
{
}

double uiSizeArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetSizeArrangerRelativeTiming();
}

void uiSizeArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetSizeArrangerRelativeTiming(val);
}

/// uiBayesArranger

uiBayesArranger::uiBayesArranger()
    : SetGetDouble(uistr::bayesArranger)
{
}

uiBayesArranger::~uiBayesArranger()
{
}

double uiBayesArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetBayesianArrangerRelativeTiming();
}

void uiBayesArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetBayesianArrangerRelativeTiming(val);
}

/// uiLocusArranger

uiLocusArranger::uiLocusArranger()
    : SetGetDouble(uistr::locusArranger)
{
}

uiLocusArranger::~uiLocusArranger()
{
}

double uiLocusArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetLocusArrangerRelativeTiming();
}

void uiLocusArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetLocusArrangerRelativeTiming(val);
}

/// uiHapArranger

uiHapArranger::uiHapArranger()
    : SetGetDouble(uistr::hapArranger)
{
}

uiHapArranger::~uiHapArranger()
{
}

double uiHapArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetHaplotypeArrangerRelativeTiming();
}

void uiHapArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetHaplotypeArrangerRelativeTiming(val);
}

/// uiProbHapArranger

uiProbHapArranger::uiProbHapArranger()
    : SetGetDouble(uistr::probhapArranger)
{
}

uiProbHapArranger::~uiProbHapArranger()
{
}

double uiProbHapArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetProbHapArrangerRelativeTiming();
}

void uiProbHapArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetProbHapArrangerRelativeTiming(val);
}

/// uiZilchArranger

uiZilchArranger::uiZilchArranger()
    : SetGetDouble(uistr::zilchArranger)
{
}

uiZilchArranger::~uiZilchArranger()
{
}

double uiZilchArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetZilchArrangerRelativeTiming();
}

void uiZilchArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetZilchArrangerRelativeTiming(val);
}

/// uiStairArranger

uiStairArranger::uiStairArranger()
    : SetGetDouble(uistr::stairArranger)
{
}

uiStairArranger::~uiStairArranger()
{
}

double uiStairArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetStairArrangerRelativeTiming();
}

void uiStairArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetStairArrangerRelativeTiming(val);
}

/// uiEpochSizeArranger

uiEpochSizeArranger::uiEpochSizeArranger()
    : SetGetDouble(uistr::epochSizeArranger)
{
}

uiEpochSizeArranger::~uiEpochSizeArranger()
{
}

double uiEpochSizeArranger::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetEpochSizeArrangerRelativeTiming();
}

void uiEpochSizeArranger::Set(UIVars& vars, UIId id, double val)
{
    vars.chains.SetEpochSizeArrangerRelativeTiming(val);
}

/// uiCanHapArrange

uiCanHapArrange::uiCanHapArrange()
    : GetBool(uistr::canHapArrange)
{
}

uiCanHapArrange::~uiCanHapArrange()
{
}

bool uiCanHapArrange::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetHaplotypeArrangerPossible();
}

/// uiBayesianAnalysis

uiBayesianAnalysis::uiBayesianAnalysis()
    : SetGetBoolBayesLike(uistr::bayesian)
{
}

uiBayesianAnalysis::~uiBayesianAnalysis()
{
}

bool uiBayesianAnalysis::Get(UIVars& vars, UIId id)
{
    return vars.chains.GetDoBayesianAnalysis();
}

void uiBayesianAnalysis::Set(UIVars& vars, UIId id, bool val)
{
    vars.chains.SetDoBayesianAnalysis(val);
}

//____________________________________________________________________________________
