// $Id: data_interface.h,v 1.24 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DATA_INTERFACE_H
#define DATA_INTERFACE_H

#include "setget.h"
#include "vectorx.h"

class UIVars;

class uiCrossPartitionCount : public GetLong
{
  public:
    uiCrossPartitionCount();
    virtual ~uiCrossPartitionCount();
    virtual long Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiDivMigPartitionCount : public GetLong
{
  public:
    uiDivMigPartitionCount();
    virtual ~uiDivMigPartitionCount();
    virtual long Get(UIVars& vars, UIId id);
};

class uiDivMigPartitionName : public GetString
{
  public:
    uiDivMigPartitionName();
    virtual ~uiDivMigPartitionName();
    virtual std::string Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiMigPartitionCount : public GetLong
{
  public:
    uiMigPartitionCount();
    virtual ~uiMigPartitionCount();
    virtual long Get(UIVars& vars, UIId id);
};

class uiMigPartitionName : public GetString
{
  public:
    uiMigPartitionName();
    virtual ~uiMigPartitionName();
    virtual std::string Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiDiseasePartitionCount : public GetLong
{
  public:
    uiDiseasePartitionCount();
    virtual ~uiDiseasePartitionCount();
    virtual long Get(UIVars& vars, UIId id);
};

class uiDiseasePartitionName : public GetString
{
  public:
    uiDiseasePartitionName();
    virtual ~uiDiseasePartitionName();
    virtual std::string Get(UIVars& vars, UIId id);
};

class uiLociNumbers : public GetLongVec1d
{
  public:
    uiLociNumbers();
    virtual ~uiLociNumbers();
    virtual LongVec1d Get(UIVars& vars, UIId id);
};

class uiRegionNumbers : public GetLongVec1d
{
  public:
    uiRegionNumbers();
    virtual ~uiRegionNumbers();
    virtual LongVec1d Get(UIVars& vars, UIId id);
};

class uiRegionEffectivePopSize : public SetGetDouble
{
  public:
    uiRegionEffectivePopSize();
    virtual ~uiRegionEffectivePopSize();
    virtual double Get(UIVars& vars, UIId id);
    virtual void   Set(UIVars& vars, UIId id, double size);
};

class uiSimulateData : public SetGetBool
{
  public:
    uiSimulateData();
    virtual ~uiSimulateData();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, bool sim);
};

#endif // DATA_INTERFACE_H

//____________________________________________________________________________________
