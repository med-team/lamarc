// $Id: chainparam_interface.h,v 1.36 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CHAINPARAM_INTERFACE_H
#define CHAINPARAM_INTERFACE_H

#include <string>
#include "setget.h"

class UIVars;

class uiInitialChains : public SetGetLong
{
  public:
    uiInitialChains();
    virtual ~uiInitialChains();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiInitialDiscard : public SetGetLong
{
  public:
    uiInitialDiscard();
    virtual ~uiInitialDiscard();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val) ;
};

class uiInitialSamples : public SetGetLong
{
  public:
    uiInitialSamples();
    virtual ~uiInitialSamples();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiInitialInterval : public SetGetLong
{
  public:
    uiInitialInterval();
    virtual ~uiInitialInterval();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiFinalChains : public SetGetLong
{
  public:
    uiFinalChains();
    virtual ~uiFinalChains();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiFinalDiscard : public SetGetLong
{
  public:
    uiFinalDiscard();
    virtual ~uiFinalDiscard();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val) ;
};

class uiFinalSamples : public SetGetLong
{
  public:
    uiFinalSamples();
    virtual ~uiFinalSamples();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiFinalInterval : public SetGetLong
{
  public:
    uiFinalInterval();
    virtual ~uiFinalInterval();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val);
};

class uiHeatedChain: public SetGetDouble
{
  public:
    uiHeatedChain();
    virtual ~uiHeatedChain();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, double val);
};

class uiHeatedChains : public GetDoubleVec1d
{
  public:
    uiHeatedChains();
    virtual ~uiHeatedChains();
    DoubleVec1d Get(UIVars& vars, UIId id);
};

class uiHeatedChainCount : public SetGetLong
{
  public:
    uiHeatedChainCount();
    virtual ~uiHeatedChainCount();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, long val);
};

class uiTempInterval : public SetGetLong
{
  public:
    uiTempInterval();
    virtual ~uiTempInterval();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val) ;
};

class uiAdaptiveTemp : public SetGetBool
{
  public:
    uiAdaptiveTemp();
    virtual ~uiAdaptiveTemp();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, bool val);
};

class uiNumReps : public SetGetLong
{
  public:
    uiNumReps();
    virtual ~uiNumReps();
    long Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,long val) ;
};

class uiDropArranger : public SetGetDouble
{
  public:
    uiDropArranger();
    virtual ~uiDropArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiSizeArranger : public SetGetDouble
{
  public:
    uiSizeArranger();
    virtual ~uiSizeArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiBayesArranger : public SetGetDouble
{
  public:
    uiBayesArranger();
    virtual ~uiBayesArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, double val);
};

class uiLocusArranger : public SetGetDouble
{
  public:
    uiLocusArranger();
    virtual ~uiLocusArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, double val);
};

class uiHapArranger : public SetGetDouble
{
  public:
    uiHapArranger();
    virtual ~uiHapArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiProbHapArranger : public SetGetDouble
{
  public:
    uiProbHapArranger();
    virtual ~uiProbHapArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiZilchArranger : public SetGetDouble
{
  public:
    uiZilchArranger();
    virtual ~uiZilchArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiStairArranger : public SetGetDouble
{
  public:
    uiStairArranger();
    virtual ~uiStairArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiEpochSizeArranger : public SetGetDouble
{
  public:
    uiEpochSizeArranger();
    virtual ~uiEpochSizeArranger();
    double Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id,double val);
};

class uiCanHapArrange : public GetBool
{
  public:
    uiCanHapArrange();
    virtual ~uiCanHapArrange();
    bool Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------
// Refinement of SetGetBool which prints "bayesian" for
// a true value and "likelihood" for a false one
class SetGetBoolBayesLike : public SetGetBool
{
  public:
    SetGetBoolBayesLike(const std::string & key) : SetGetBool(key) {};
    virtual ~SetGetBoolBayesLike() {};
    virtual std::string MakePrintString(UIVars& vars, bool val)
    {
        if(val) return "bayesian";
        return "likelihood";
    };
};

class uiBayesianAnalysis : public SetGetBoolBayesLike
{
  public:
    uiBayesianAnalysis();
    virtual ~uiBayesianAnalysis();
    bool Get(UIVars& vars, UIId id);
    void Set(UIVars& vars, UIId id, bool val);
};

#endif  // CHAINPARAM_INTERFACE_H

//____________________________________________________________________________________
