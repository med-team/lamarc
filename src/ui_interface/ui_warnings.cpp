// $Id: ui_warnings.cpp,v 1.5 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <string>
#include "ui_warnings.h"

using std::string;

const string uiwarn::calcFST_0      = "Warning: calculating FST estimates for ";
const string uiwarn::calcFST_1      = "and their reciprocal rates is impossible due to the data for the populations involved.  "
    "If the FST method is invoked to obtain starting values for those parameters, defaults will be used instead.";

//____________________________________________________________________________________
