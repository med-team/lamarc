// $Id: ui_regid.cpp,v 1.8 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>

#include "ui_regid.h"
#include "ui_vars.h"
#include "ui_id.h"
#include "ui_constants.h"

//------------------------------------------------------------------------------------

UIRegId::UIRegId(long reg, long loc, const UIVars& uivars)
    : m_region(reg),
      m_locus(loc),
      m_dtype(uivars.datapackplus.GetDataType(m_region, m_locus))
{
}

UIRegId::UIRegId(data_type dtype)
    : m_region(uiconst::GLOBAL_ID),
      m_locus(uiconst::GLOBAL_ID),
      m_dtype(dtype)
{
}

UIRegId::UIRegId(UIId id, const UIVars& uivars)
{
    if (id.GetIndex1()==uiconst::GLOBAL_ID)
    {
        assert(false); //We should be using the DATAMODEL flag values, below.
        m_region = uiconst::GLOBAL_ID;
        m_locus = uiconst::GLOBAL_ID;
        m_dtype = dtype_DNA; //default value
    }
    else if (id.GetIndex1()==uiconst::GLOBAL_DATAMODEL_NUC_ID)
    {
        m_region = uiconst::GLOBAL_ID;
        m_locus = uiconst::GLOBAL_ID;
        m_dtype = dtype_DNA;
    }
    else if (id.GetIndex1()==uiconst::GLOBAL_DATAMODEL_MSAT_ID)
    {
        m_region = uiconst::GLOBAL_ID;
        m_locus = uiconst::GLOBAL_ID;
        m_dtype = dtype_msat;
    }
    else if (id.GetIndex1()==uiconst::GLOBAL_DATAMODEL_KALLELE_ID)
    {
        m_region = uiconst::GLOBAL_ID;
        m_locus = uiconst::GLOBAL_ID;
        m_dtype = dtype_kallele;
    }
    else
    {
        m_region = id.GetIndex1();
        m_locus = id.GetIndex2();
        m_dtype = uivars.datapackplus.GetDataType(m_region, m_locus);
    }
}

bool UIRegId::operator<(const UIRegId other) const
{
    if (m_region == other.m_region)
    {
        return (m_locus < other.m_locus);
    }
    return (m_region < other.m_region);
}

//____________________________________________________________________________________
