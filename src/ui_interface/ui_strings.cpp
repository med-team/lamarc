// $Id: ui_strings.cpp,v 1.104 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <string>

#include "local_build.h"

#include "ui_strings.h"

using std::string;

// Strings assigned here should be defined in ui_strings.h as
// public static const string members of class uistr.

const string uistr::TTRatio         = "TT Ratio";
const string uistr::addParamToGroup = "Add a parameter to an existing or new group";
const string uistr::addParamToNewGroup = "Add a parameter to a new group";
const string uistr::addRangeForTraitModel = "Allow a range of sites for this trait";
const string uistr::allProfilesOff  = "Turn OFF Profiling for ALL parameters";
const string uistr::allProfilesOn   = "Turn ON  Profiling for ALL parameters";
const string uistr::allProfilesPercentile = "Set all profiling to Percentile";
const string uistr::allProfilesFixed      = "Set all profiling to Fixed";
const string uistr::allStr          = "all";
const string uistr::alpha           = "Percentage stepwise mutations";
const string uistr::argFilePrefix   = "Prefix to use for all ARG output files";
const string uistr::autoCorrelation = "Auto-Correlation";
const string uistr::baseFrequencies = "Base Frequencies (A C G T)";
const string uistr::baseFrequencyA  = "Relative Base Frequency of Adenine ";
const string uistr::baseFrequencyC  = "Relative Base Frequency of Cytosine";
const string uistr::baseFrequencyG  = "Relative Base Frequency of Guanine ";
const string uistr::baseFrequencyT  = "Relative Base Frequency of Thymine ";
const string uistr::bayesArranger   = "Bayesian rearranger frequency";
const string uistr::bayesian        = "Perform Bayesian or Likelihood analysis";
const string uistr::canHapArrange   = "Haplotype Arrangement allowed";
const string uistr::categoryCount   = "Number of Categories";
const string uistr::categoryPair    = "Category (Probability, Rate)";
const string uistr::categoryProbability     = "Relative Category Probability";
const string uistr::categoryRate    = "Relative Category Rate";
const string uistr::coalescence     = "Coalescence";
const string uistr::coalescenceLegal  = "Coalescence legal for this data set ";
const string uistr::coalescenceMaxEvents = "Maximum number of coalescence events";
const string uistr::coalescenceStartMethodByID= "Method for calcualting starting coalescence for";
const string uistr::constraintByForce = "Constraint tables for ";
const string uistr::constraintType = "Constraint for this parameter";
const string uistr::crossPartitionCount= "Number of cross partitions";
const string uistr::curveFilePrefix = "Prefix to use for all Bayesian curvefiles";
const string uistr::dataFileName    = "Input data file name";
const string uistr::dataModel       = "Data Model";
const string uistr::dataModelReport = "Data Model Report";
const string uistr::dataType        = "Datatype for this region";
const string uistr::defaultStr      = "default";
const string uistr::disease         = "Disease";
const string uistr::diseaseByID     = "Disease mutation rate to ";
const string uistr::diseaseByID2    = " from ";
const string uistr::diseaseInto     = "Disease mutation rate into ";
const string uistr::diseaseLegal    = "Disease legal for this data set ";
const string uistr::diseaseLocation = "Disease site ";
const string uistr::diseaseMaxEvents= "Maximum number of disease events";
const string uistr::diseasePartitionCount= "Number of disease partitions ";
const string uistr::diseasePartitionName= "Name of disease partition ";
const string uistr::diseasePartitionNames= "Names of disease partitions ";
const string uistr::divergence      = "Divergence";
const string uistr::divergenceEpoch = "Epoch";
const string uistr::divergenceEpochAncestor = "Ancestor";
const string uistr::divergenceEpochDescendent = "Descendent";
const string uistr::divergenceEpochDescendents = "Descendents";
const string uistr::divergenceEpochCount= "Number of divergence epochs";
const string uistr::divergenceEpochName= "Name of divergence epoch";
const string uistr::divergenceEpochNames= "Names of divergence epochs";
const string uistr::divergenceEpochBoundaryTime= "Boundary Time";
const string uistr::divergenceLegal = "Divergence legal using this data set ";
const string uistr::divmigration       = "Migration (in the presence of divergence)";
const string uistr::divmigrationByID   = "Rate of migrants into ";
const string uistr::divmigrationByID2  = " originating from ";
const string uistr::divmigrationInto   = "Migrating into ";
const string uistr::divmigrationLegal  = "Migration legal using this data set ";
const string uistr::divmigrationMaxEvents = "Maximum number of migration event";
const string uistr::divmigrationPartitionCount= "Number of migration groups";
const string uistr::divmigrationPartitionName= "Name of migration group ";
const string uistr::divmigrationPartitionNames= "Names of migration groups ";
const string uistr::divmigrationUser="User never sees: uistr::divmigrationUser string";
const string uistr::dropArranger    = "Topology rearranger frequency";
const string uistr::effectivePopSize= "Effective population size";
const string uistr::epochSizeArranger= "Epoch size rearranger frequency";
const string uistr::expGrowStick    = "Exponential Growth via stick";
const string uistr::finalChains     = "Number of chains (final)";
const string uistr::finalDiscard    = "Number of samples to discard (final burn-in)";
const string uistr::finalInterval   = "Interval between recorded items (final)";
const string uistr::finalSamples    = "Number of recorded genealogies (final)";
const string uistr::finalSamplesBayes = "Number of recorded parameter sets (final)";
const string uistr::forceLegal      = "Is a force legal";
const string uistr::forceName       = "Name of force";
const string uistr::forceOnOff      = "Force turned on?";
const string uistr::forceVector     = "Force vector";
const string uistr::freqsFromData   = "Base frequencies computed from data";
const string uistr::fstSetMigration = "Use FST estimate for migration rates";
const string uistr::fstSetTheta     = "Use FST estimate for thetas";
const string uistr::globalDisease   = "Single disease starting estimate for all data";
const string uistr::globalGrowth    = "Single growth starting estimate for all data";
const string uistr::globalLogisticSelectionCoefficient = "Single log. sel. coeff. starting estimate for all data";
const string uistr::globalMigration = "Single migration starting estimate for all data";
const string uistr::globalDivMigration = "Single migration starting value for all data";
const string uistr::globalTheta     = "Single theta starting estimate for all data";
const string uistr::groupConstraintType = "Constraint type for this group.";
const string uistr::groupParamList  = "List of parameters to constrain together.";
const string uistr::groupedParamsForForce = "Ids of valid grouped params for force.";
const string uistr::growth          = "Growth estimation";
const string uistr::growthByID      = "Growth rate for";
const string uistr::growthLegal     = "Growth legal for this data set ";
const string uistr::growthMaxEvents = "Maximum number of growth events";
const string uistr::growthRate      = "Growth";
const string uistr::growthScheme    = "Type of Growth";
const string uistr::growthType      = "Growth implemented via";
const string uistr::gtrRateAC       = "GTR rates AC";
const string uistr::gtrRateAG       = "GTR rates AG";
const string uistr::gtrRateAT       = "GTR rates AT";
const string uistr::gtrRateCG       = "GTR rates CG";
const string uistr::gtrRateCT       = "GTR rates CT";
const string uistr::gtrRateGT       = "GTR rates GT";
const string uistr::gtrRates        = "GTR rates [AC AG AT CG CT GT]";
const string uistr::hapArranger     = "Haplotype rearranger frequency";
const string uistr::heatedChain     = "Relative temperature for search ";
const string uistr::heatedChainCount= "Number of Simultaneous Searches (Heating)";
const string uistr::heatedChains    = "Relative Temperatures of Simultaneous Searches (Heating)";
const string uistr::initialChains   = "Number of chains (initial)";
const string uistr::initialDiscard  = "Number of samples to discard (initial burn-in) ";
const string uistr::initialInterval = "Interval between recorded items (initial)";
const string uistr::initialSamples  = "Number of recorded genealogies (initial)";
const string uistr::initialSamplesBayes  = "Number of recorded parameter sets (initial)";
const string uistr::lociCount       = "Number of segments for region";
const string uistr::lociNumbers     = "List of segment numbers";
const string uistr::locusArranger   = "Trait Location rearranger frequency (for mapping)";
const string uistr::locusName       = "Segment name";
const string uistr::logSelectStick  = "Stochastic selection.";
const string uistr::logisticSelection = "Est. of logistic selection coeff.";
const string uistr::logisticSelectionCoefficient = "Starting selection coeff. (\"s\")";
const string uistr::logisticSelectionLegal = "Logistic selection legal for this data set";
const string uistr::logisticSelectionMaxEvents = "Maximum number of logistic selection events";
const string uistr::manyArgFiles    = "Write out ALL sampled ARGs from last chain";
const string uistr::mapFilePrefix   = "Prefix to use for all Mapping information files";
const string uistr::maxEvents       = "Maximum number of events";
const string uistr::migration       = "Migration";
const string uistr::migrationByID   = "Migration rate into ";
const string uistr::migrationByID2  = " from ";
const string uistr::migrationInto   = "Migration into ";
const string uistr::migrationLegal  = "Migration legal for this data set ";
const string uistr::migrationMaxEvents = "Maximum number of migration events";
const string uistr::migrationPartitionCount= "Number of migration partitions";
const string uistr::migrationPartitionName= "Name of migration partition ";
const string uistr::migrationPartitionNames= "Names of migration partitions";
const string uistr::migrationUser="User never sees: uistr::migrationUser string";
const string uistr::muRate          = "MuRate";
const string uistr::newickTreeFilePrefix = "Prefix to use for all Newick Tree files";
const string uistr::normalization   = "Normalization";
const string uistr::oneForceProfileType = "Profile type for ";
const string uistr::oneForceProfilesOff = "Turn OFF profiles for ";
const string uistr::oneForceProfilesOn  = "Turn ON  profiles for ";
const string uistr::optimizeAlpha   = "Optimize the multi-step:single-step ratio after each chain";
const string uistr::paramName       = "Name of parameter";
const string uistr::paramVector     = "Parameter vector";
const string uistr::perBaseErrorRate= "Per-base error rate";
const string uistr::plotPost        = "Show plots";
const string uistr::priorByForce    = "Bayesian priors for ";
const string uistr::priorByID       = "Prior settings for ";
const string uistr::priorLowerBound = "Lower bound of the prior";
const string uistr::priorType       = "Shape of the prior";
const string uistr::priorUpperBound = "Upper bound of the prior";
const string uistr::priorUseDefault = "Use the default prior for this force";
const string uistr::probhapArranger = "Trait haplotypes rearranger frequency";
const string uistr::profileByForce  = "Profile tables for ";
const string uistr::profileByID     = "Profile";
const string uistr::profileprefix   = "prefix for profile output (beta test)";
const string uistr::profiles        = "Calculate profile tables";
const string uistr::progress        = "Verbosity of progress reports";
const string uistr::randomSeed      = "Random seed (closest 4n+1)";
const string uistr::rateCategories  = "Rate Categories";
const string uistr::recRate         = "RecRate";
const string uistr::reclocFilePrefix= "Prefix to use for all Recloc files";
const string uistr::recombination   = "Recombination";
const string uistr::recombinationLegal     = "Recombination legal for this data set ";
const string uistr::recombinationMaxEvents = "Maximum number of recombination events";
const string uistr::recombinationRate   = "Starting recombination rate";
const string uistr::regGammaShape = "Scaled shape parameter (\"alpha\")";
const string uistr::regionCount     = "Number of regions";
const string uistr::regionGamma     = "Gamma (mu rate varies over regions)";
const string uistr::regionGammaLegal = "Gamma over regions legal for this data set ";
const string uistr::regionGammaShape= "Starting scaled shape parameter (\"alpha\")";
const string uistr::regionName      = "Region name";
const string uistr::regionNumbers   = "List of region numbers";
const string uistr::relativeMuRate  = "Relative mutation rate";
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
const string uistr::relativeSampleRate  = "Relative parameter change proposal rate";
#endif
const string uistr::removeParamFromGroup = "Remove a parameter from its group";
const string uistr::removeRangeForTraitModel = "Disallow a range of sites for this trait";
const string uistr::replicates      = "Number of replicates";
const string uistr::resultsFileName = "Name of output results file";
const string uistr::selectType      = "Selection implemented via";
const string uistr::setOldSeedFromClock= "Random seed from earlier run";
const string uistr::simulateData    = "Simulate new data for this segment (destroying provided data!)";
const string uistr::sizeArranger    = "Tree-size rearranger frequency";
const string uistr::startValue      = "Initial starting value";
const string uistr::startValueMethod = "Method for calculating starting value";
const string uistr::stairArranger    = "Stair arranger frequency";
const string uistr::systemClock     = "Use system clock to set random seed";
const string uistr::tempAdapt       = "Adjust temperatures automatically during run";
const string uistr::tempInterval    = "Swap interval for different temperatures";
const string uistr::theta           = "Theta";
const string uistr::traceFilePrefix = "Prefix to use for all Tracer files";
const string uistr::traitAnalysisData = "Use this trait as data only";
const string uistr::traitAnalysisFloat = "Map this trait after collecting trees ('float')";
const string uistr::traitAnalysisJump = "Map this trait while collecting trees ('jump')";
const string uistr::traitAnalysisPartition = "Partition your data into sub-populations using this trait";
const string uistr::traitModelName  = "Trait name";
const string uistr::traitModelRangeToPoint  = "Place this trait at a single site";
const string uistr::treeSumInFileEnabled  = "Reading of tree summary file";
const string uistr::treeSumInFileName     = "Name of input tree summary file";
const string uistr::treeSumOutFileEnabled = "Writing of tree summary file";
const string uistr::treeSumOutFileName    = "Name of output tree summary file";
const string uistr::trueValue       = "The TRUTH, string never seen by users";
const string uistr::ungroupedParamsForForce = "Ids of valid ungrouped params for force.";
const string uistr::useArgFiles     = "Write out ARG at end of last chain";
const string uistr::useCurveFiles   = "Write Bayesian results to individual files";
const string uistr::useDefaultPriorsForForce = "Use default priors for all parameters for ";
const string uistr::useGlobalDataModelForAll = "Use default data model for all regions/segments";
const string uistr::useGlobalDataModelForOne = "Use default data model for this region/segment";
const string uistr::useNewickTreeFiles = "Write the best Newick Trees to files";
const string uistr::useOldSeedFromClock= "Restore random seed from earlier run";
const string uistr::useReclocFiles  = "Write recombination locations to files";
const string uistr::useTraceFiles   = "Write Tracer output to files";
const string uistr::userSetTheta    = "Theta value for";
const string uistr::validForces     = "Ids of valid forces";
const string uistr::validMovingLoci = "Ids of valid moving segments";
const string uistr::validParamsForForce  = "Ids of valid parameters for force ";
const string uistr::verbosity       = "Verbosity level of output file";
const string uistr::wattersonSetTheta = "Use Watterson's estimate for thetas";
const string uistr::xmlOutFileName  = "Name of menu-modified version of input file";
const string uistr::xmlReportFileName="Name for XML output file. (Beta test)";
const string uistr::zilchArranger   = "Do-nothing rearranger frequency";

//____________________________________________________________________________________
