// $Id: ui_id.h,v 1.10 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UIID_H
#define UIID_H

#include "constants.h"       // for force_type
#include "ui_constants.h"    // for uiconst::NO_ID

class UIId
{
  private:
    // note -- would like to make these const members as
    // they really shouldn't change, but we're using
    // vectors of them (not vectors of pointers) so
    // we have to allow the assignment operator
    bool          m_hasForce;
    force_type    m_forceType;
    long          m_index1;
    long          m_index2;
    long          m_index3;
    bool IndexesAreOkay() const;

  public:
    // note -- we are accepting the default copy constructor
    UIId(long index1=uiconst::NO_ID, long index2=uiconst::NO_ID, long index3=uiconst::NO_ID);
    UIId(force_type force, long index1=uiconst::NO_ID, long index2=uiconst::NO_ID, long index3=uiconst::NO_ID);
    force_type GetForceType() const;
    long GetIndex1() const;
    long GetIndex2() const;
    long GetIndex3() const;

    bool operator==(const UIId& id) const;
    bool HasForce() const;
    bool HasIndex1() const;
    bool HasIndex2() const;
    bool HasIndex3() const;
};

UIId & NO_ID();
UIId & GLOBAL_ID();

#endif  // UIID_H

//____________________________________________________________________________________
