// $Id: ui_regid.h,v 1.6 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


//This is not much more than a struct right now, but has some complexity in
// the constructors.  If you give it a region, locus, and the UIVars, it uses
// the UIVars to look up the data type for that region and locus.  If you just
// give it a datatype, it uses the GLOBAL_ID value for the region and locus.
// If you give it a UIId and the UIVars, it grabs the region and locus from
// Index1 and Index2, then looks up the appropriate data type in UIVars.  If
// Index1 is one of three global variables (one for each data type), it sets
// the region and locus to GLOBAL_ID, and sets the appropriate datatype.
//  --Lucian

#ifndef UIREGID_H
#define UIREGID_H

#include "datatype.h"       // for data_type

class UIVars;
class UIId;

class UIRegId
{
  private:
    UIRegId(); //undefined
    long      m_region;
    long      m_locus;
    data_type m_dtype;

  public:
    UIRegId(long reg, long loc, const UIVars& uivars);
    UIRegId(data_type dtype);
    UIRegId(UIId id, const UIVars& uivars);
    ~UIRegId() {};
    bool operator<(const UIRegId other) const;
    long      GetRegion()   {return m_region;};
    long      GetLocus()    {return m_locus;};
    data_type GetDataType() {return m_dtype;};
};

#endif  // UIREGID_H

//____________________________________________________________________________________
