// $Id: ui_interface.h,v 1.44 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#include <string>
#include <deque>
#include "constants.h"
#include "setgetmachine.h"
#include "vectorx.h"

using std::string;

class DataPack;
class FrontEndWarnings;
class UIVars;
class UndoRedoChain;

enum undo_redo_mode { undoRedoMode_FILE, undoRedoMode_USER };

// This class should eventually provide the only interface to the back end
// via methods that take only longs, doubles and strings as arguments.
class UIInterface
{
  private:
    FrontEndWarnings &  m_warnings;
    UndoRedoChain * undoRedoChain;    // stores undo/redo
    SetGetMachine   setGetMachine;
    undo_redo_mode  undoRedoMode;   // cache each variable set, or
    // run them together
    UIInterface();                  // undefined

  protected:

  public:
    UIInterface(FrontEndWarnings & warnings, DataPack&,std::string fileName);
    virtual ~UIInterface();

    UIVars& GetCurrentVars();

    //////////////////////////////////////////////////
    void SetUndoRedoMode(undo_redo_mode);
    void Undo();
    void Redo();
    bool CanUndo();
    bool CanRedo();
    string GetUndoDescription();
    string GetRedoDescription();

    //////////////////////////////////////////////////
    bool        doGetBool(string varName,          const UIId id = NO_ID());
    data_type   doGetDataType(string varName,      const UIId id = NO_ID());
    double      doGetDouble(string varName,        const UIId id = NO_ID());
    DoubleVec1d doGetDoubleVec1d(string varName,   const UIId id = NO_ID());
    force_type  doGetForceType(string varName,     const UIId id = NO_ID());
    ForceTypeVec1d  doGetForceTypeVec1d(string varName, const UIId id = NO_ID());
    long        doGetLong(string varName,          const UIId id = NO_ID());
    LongVec1d   doGetLongVec1d(string varName,     const UIId id = NO_ID());
    method_type doGetMethodType(string varName,    const UIId id = NO_ID());
    model_type  doGetModelType(string varName,     const UIId id = NO_ID());
    proftype    doGetProftype(string varName,      const UIId id = NO_ID());
    string      doGetString(string varName,        const UIId id = NO_ID());
    StringVec1d doGetStringVec1d(string varName,   const UIId id = NO_ID());
    UIIdVec1d   doGetUIIdVec1d(string varName,     const UIId id = NO_ID());
    UIIdVec2d   doGetUIIdVec2d(string varName,     const UIId id = NO_ID());
    verbosity_type doGetVerbosityType(string varName, const UIId id = NO_ID());

    void doSet(string varName, string value,    const UIId id = NO_ID());
    void doToggle(string varName,               const UIId id = NO_ID());

    string        doGetPrintString(string varName, const UIId id = NO_ID());
    string        doGetDescription(string varName, const UIId id = NO_ID());
    bool          doGetConsistency(string varName, const UIId id = NO_ID());

    string        doGetMin(string varName, const UIId id = NO_ID());
    string        doGetMax(string varName, const UIId id = NO_ID());

    std::vector<std::string> GetAndClearWarnings();
    void      AddWarning(std::string warnmsg);

    bool          IsReadyToRun();
    std::string   WhatIsWrong();

};

#endif  // UI_INTERFACE_H

//____________________________________________________________________________________
