// $Id: datamodel_interface.h,v 1.30 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DATAMODEL_INTERFACE_H
#define DATAMODEL_INTERFACE_H

#include <string>
#include "setget.h"

class UIVars;

class uiLociCount : public GetLong
{
  public:
    uiLociCount();
    virtual ~uiLociCount();
    virtual long Get(UIVars& vars, UIId id);
};

class uiLocusName : public GetString
{
  public:
    uiLocusName();
    virtual ~uiLocusName();
    virtual std::string Get(UIVars& vars, UIId id);
};

class uiRegionName : public GetString
{
  public:
    uiRegionName();
    virtual ~uiRegionName();
    virtual std::string Get(UIVars& vars, UIId id);
};

class uiFreqsFromData : public SetGetBool
{
  public:
    uiFreqsFromData();
    virtual ~uiFreqsFromData();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiDataType : public GetDataType
{
  public:
    uiDataType();
    virtual ~uiDataType();
    virtual data_type Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiGTRRates : public GetDoubleVec1d
{
  public:
    uiGTRRates();
    virtual ~uiGTRRates();
    virtual DoubleVec1d Get(UIVars& vars, UIId id);
    virtual std::string GetPrintString(UIVars& vars, UIId id);
};

class uiGTRRateAC : public SetGetDouble
{
  public:
    uiGTRRateAC();
    virtual ~uiGTRRateAC();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGTRRateAG : public SetGetDouble
{
  public:
    uiGTRRateAG();
    virtual ~uiGTRRateAG();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGTRRateAT : public SetGetDouble
{
  public:
    uiGTRRateAT();
    virtual ~uiGTRRateAT();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGTRRateCG : public SetGetDouble
{
  public:
    uiGTRRateCG();
    virtual ~uiGTRRateCG();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGTRRateCT : public SetGetDouble
{
  public:
    uiGTRRateCT();
    virtual ~uiGTRRateCT();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGTRRateGT : public SetGetDouble
{
  public:
    uiGTRRateGT();
    virtual ~uiGTRRateGT();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiBaseFrequencies : public GetDoubleVec1d
{
  public:
    uiBaseFrequencies();
    virtual ~uiBaseFrequencies();
    virtual DoubleVec1d Get(UIVars& vars, UIId id);
    virtual std::string GetPrintString(UIVars& vars, UIId id);
};

class uiBaseFrequencyA : public SetGetDouble
{
  public:
    uiBaseFrequencyA();
    virtual ~uiBaseFrequencyA();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiBaseFrequencyC : public SetGetDouble
{
  public:
    uiBaseFrequencyC();
    virtual ~uiBaseFrequencyC();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiBaseFrequencyG : public SetGetDouble
{
  public:
    uiBaseFrequencyG();
    virtual ~uiBaseFrequencyG();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiBaseFrequencyT : public SetGetDouble
{
  public:
    uiBaseFrequencyT();
    virtual ~uiBaseFrequencyT();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiDataModel : public SetGetModelType
{
  public:
    uiDataModel();
    virtual ~uiDataModel();
    virtual model_type Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,model_type val);
    virtual std::string NextToggleValue(UIVars& vars, UIId id);
};

class uiDataModelReport : public GetStringVec1d
{
  public:
    uiDataModelReport();
    virtual ~uiDataModelReport();
    virtual StringVec1d Get(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

//LS NOTE:  Normalization is no longer in the menu, since it is turned on
// automatically if needed.
class uiNormalization : public SetGetBool
{
  public:
    uiNormalization();
    virtual ~uiNormalization();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, bool val);
};

//------------------------------------------------------------------------------------

class uiTTRatio : public SetGetDouble
{
  public:
    uiTTRatio();
    virtual ~uiTTRatio();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiPerBaseErrorRate : public SetGetDouble
{
  public:
    uiPerBaseErrorRate();
    virtual ~uiPerBaseErrorRate();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiAlpha: public SetGetDouble
{
  public:
    uiAlpha();
    virtual ~uiAlpha();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,double val);
    virtual string Min(UIVars& vars, UIId id);
    virtual string Max(UIVars& vars, UIId id);
};

class uiOptimizeAlpha: public SetGetBool
{
  public:
    uiOptimizeAlpha();
    virtual ~uiOptimizeAlpha();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

//------------------------------------------------------------------------------------

class uiAutoCorrelation : public SetGetDouble
{
  public:
    uiAutoCorrelation();
    virtual ~uiAutoCorrelation();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

// The uiCategoryCount menu item displays the number of categories
class uiCategoryCount : public SetGetLong
{
  public:
    uiCategoryCount();
    virtual ~uiCategoryCount();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
    virtual string Min(UIVars& vars, UIId id);
    virtual string Max(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiCategoryProbability : public SetGetDouble
{
  public:
    uiCategoryProbability();
    virtual ~uiCategoryProbability();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiCategoryRate : public SetGetDouble
{
  public:
    uiCategoryRate();
    virtual ~uiCategoryRate();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiRelativeMuRate : public SetGetDouble
{
  public:
    uiRelativeMuRate();
    virtual ~uiRelativeMuRate();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiUseGlobalDataModelForAll : public SetGetNoval
{
  public:
    uiUseGlobalDataModelForAll();
    virtual ~uiUseGlobalDataModelForAll();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

//------------------------------------------------------------------------------------

class uiUseGlobalDataModelForOne : public SetGetBool
{
  public:
    uiUseGlobalDataModelForOne();
    virtual ~uiUseGlobalDataModelForOne();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, bool val);
};

#endif  // DATAMODEL_INTERFACE_H

//____________________________________________________________________________________
