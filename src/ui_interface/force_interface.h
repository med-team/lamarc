// $Id: force_interface.h,v 1.40 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef FORCE_INTERFACE_H
#define FORCE_INTERFACE_H

#include <string>
#include "setget.h"

class UIVars;

class uiForceLegal : public GetBool
{
  public:
    uiForceLegal();
    virtual ~uiForceLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiForceOnOff : public SetGetBool
{
  public:
    uiForceOnOff();
    virtual ~uiForceOnOff();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, bool val);
};

class uiMaxEvents : public SetGetLong
{
  public:
    uiMaxEvents();
    virtual ~uiMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

//LS DEBUG:  All the classes that inherit from uiParameter could be
// collapsed into a single class that looked a lot like uiStartValue.  In
// fact, the code in uiStartValue is essentially duplicated code that's used
// in the XML reading, but nowhere else.
class uiParameter : public SetGetDouble
{
  private:
    uiParameter(); //undefined
    force_type m_ftype;
  public:
    uiParameter(const std::string& whichForceClass);
    virtual ~uiParameter();
    virtual string Min(UIVars& vars, UIId id);
    virtual string Max(UIVars& vars, UIId id);
    virtual string Description(UIVars& vars, UIId id);
};

class uiCoalescence : public SetGetBoolEnabled
{
  public:
    uiCoalescence();
    virtual ~uiCoalescence();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiCoalescenceLegal : public GetBool
{
  public:
    uiCoalescenceLegal();
    virtual ~uiCoalescenceLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiCoalescenceMaxEvents : public SetGetLong
{
  public:
    uiCoalescenceMaxEvents();
    virtual ~uiCoalescenceMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiFstTheta : public SetGetNoval
{
  public:
    uiFstTheta();
    virtual ~uiFstTheta();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiUserTheta : public uiParameter
{
  public:
    uiUserTheta();
    virtual ~uiUserTheta();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiWattersonTheta : public SetGetNoval
{
  public:
    uiWattersonTheta();
    virtual ~uiWattersonTheta();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiGlobalTheta : public uiParameter
{
  public:
    uiGlobalTheta();
    virtual ~uiGlobalTheta();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiGrowth : public SetGetBoolEnabled
{
  public:
    uiGrowth();
    virtual ~uiGrowth();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiGrowthLegal : public GetBool
{
  public:
    uiGrowthLegal();
    virtual ~uiGrowthLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiGrowthType : public SetGetGrowthType
{
  public:
    uiGrowthType();
    virtual ~uiGrowthType();
    virtual growth_type Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, growth_type gType);
    virtual std::string NextToggleValue(UIVars& vars, UIId id);
    virtual std::string MakePrintString(UIVars& vars, growth_type gType);
};

class uiGrowthScheme : public SetGetGrowthScheme
{
  public:
    uiGrowthScheme();
    virtual ~uiGrowthScheme();
    virtual growth_scheme Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, growth_scheme gScheme);
    virtual std::string NextToggleValue(UIVars& vars, UIId id);
};

class uiGlobalGrowth : public uiParameter
{
  public:
    uiGlobalGrowth();
    virtual ~uiGlobalGrowth();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiGrowthUser : public uiParameter
{
  public:
    uiGrowthUser();
    virtual ~uiGrowthUser();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);

};

class uiGrowthMaxEvents : public SetGetLong
{
  public:
    uiGrowthMaxEvents();
    virtual ~uiGrowthMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

//------------------------------------------------------------------------------------

class uiLogisticSelection : public SetGetBoolEnabled
{
  public:
    uiLogisticSelection();
    virtual ~uiLogisticSelection();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiLogisticSelectionLegal : public GetBool
{
  public:
    uiLogisticSelectionLegal();
    virtual ~uiLogisticSelectionLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiGlobalLogisticSelectionCoefficient : public uiParameter
{
  public:
    uiGlobalLogisticSelectionCoefficient();
    virtual ~uiGlobalLogisticSelectionCoefficient();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiLogisticSelectionCoefficientUser : public uiParameter
{
  public:
    uiLogisticSelectionCoefficientUser();
    virtual ~uiLogisticSelectionCoefficientUser();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);

};

class uiLogisticSelectionMaxEvents : public SetGetLong
{
  public:
    uiLogisticSelectionMaxEvents();
    virtual ~uiLogisticSelectionMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiLogisticSelectionType : public SetGetSelectionType
{
  public:
    uiLogisticSelectionType();
    virtual ~uiLogisticSelectionType();
    virtual selection_type Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, selection_type sType);
    virtual std::string NextToggleValue(UIVars& vars, UIId id);
    virtual std::string MakePrintString(UIVars& vars, selection_type sType);
};

//------------------------------------------------------------------------------------

class uiMigration : public SetGetBoolEnabled
{
  public:
    uiMigration();
    virtual ~uiMigration();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiMigrationLegal : public GetBool
{
  public:
    uiMigrationLegal();
    virtual ~uiMigrationLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiMigrationFst : public SetGetNoval
{
  public:
    uiMigrationFst();
    virtual ~uiMigrationFst();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiMigrationGlobal : public uiParameter
{
  public:
    uiMigrationGlobal();
    virtual ~uiMigrationGlobal();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiMigrationUser : public uiParameter
{
  public:
    uiMigrationUser();
    virtual ~uiMigrationUser();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiMigrationMaxEvents : public SetGetLong
{
  public:
    uiMigrationMaxEvents();
    virtual ~uiMigrationMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiMigrationInto: public GetString
{
  public:
    uiMigrationInto();
    virtual ~uiMigrationInto();
    virtual std::string Get(UIVars& vars, UIId id);
    virtual string Description(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiDivMigration : public SetGetBoolEnabled
{
  public:
    uiDivMigration();
    virtual ~uiDivMigration();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiDivMigrationLegal : public GetBool
{
  public:
    uiDivMigrationLegal();
    virtual ~uiDivMigrationLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiDivMigrationFst : public SetGetNoval
{
  public:
    uiDivMigrationFst();
    virtual ~uiDivMigrationFst();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiDivMigrationGlobal : public uiParameter
{
  public:
    uiDivMigrationGlobal();
    virtual ~uiDivMigrationGlobal();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiDivMigrationUser : public uiParameter
{
  public:
    uiDivMigrationUser();
    virtual ~uiDivMigrationUser();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiDivMigrationMaxEvents : public SetGetLong
{
  public:
    uiDivMigrationMaxEvents();
    virtual ~uiDivMigrationMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiDivMigrationInto: public GetString
{
  public:
    uiDivMigrationInto();
    virtual ~uiDivMigrationInto();
    virtual std::string Get(UIVars& vars, UIId id);
    virtual string Description(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiDivergence : public SetGetBoolEnabled
{
  public:
    uiDivergence();
    virtual ~uiDivergence();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiDivergenceEpochAncestor : public GetString
{
  public:
    uiDivergenceEpochAncestor();
    virtual ~uiDivergenceEpochAncestor();
    virtual std::string Get(UIVars& vars, UIId id);
};

class uiDivergenceEpochDescendents : public GetString
{
  public:
    uiDivergenceEpochDescendents();
    virtual ~uiDivergenceEpochDescendents();
    virtual std::string Get(UIVars& vars, UIId id);
};

class uiDivergenceLegal : public GetBool
{
  public:
    uiDivergenceLegal();
    virtual ~uiDivergenceLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiDivergenceEpochCount : public GetLong
{
  public:
    uiDivergenceEpochCount();
    virtual ~uiDivergenceEpochCount();
    virtual long Get(UIVars& vars, UIId id);
};

class uiDivergenceEpochName : public GetString
{
  public:
    uiDivergenceEpochName();
    virtual ~uiDivergenceEpochName();
    virtual std::string Get(UIVars& vars, UIId id);
};


class uiDivergenceEpochBoundaryTime :  public uiParameter
{
  public:
    uiDivergenceEpochBoundaryTime();
    virtual ~uiDivergenceEpochBoundaryTime();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
    virtual string Description(UIVars& vars, UIId id);
};

//------------------------------------------------------------------------------------

class uiRecombine : public SetGetBoolEnabled
{
  public:
    uiRecombine();
    virtual ~uiRecombine();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiRecombineLegal : public GetBool
{
  public:
    uiRecombineLegal();
    virtual ~uiRecombineLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiRecombinationRate : public uiParameter
{
  public:
    uiRecombinationRate();
    virtual ~uiRecombinationRate();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiRecombinationMaxEvents : public SetGetLong
{
  public:
    uiRecombinationMaxEvents();
    virtual ~uiRecombinationMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

//------------------------------------------------------------------------------------

class uiRegionGamma : public SetGetBoolEnabled
{
  public:
    uiRegionGamma();
    virtual ~uiRegionGamma();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiRegionGammaLegal : public GetBool
{
  public:
    uiRegionGammaLegal();
    virtual ~uiRegionGammaLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiRegionGammaShape : public uiParameter
{
  public:
    uiRegionGammaShape();
    virtual ~uiRegionGammaShape();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

//------------------------------------------------------------------------------------

class uiDisease : public SetGetBoolEnabled
{
  public:
    uiDisease();
    virtual ~uiDisease();
    virtual bool Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id,bool val);
};

class uiDiseaseLegal : public GetBool
{
  public:
    uiDiseaseLegal();
    virtual ~uiDiseaseLegal();
    virtual bool Get(UIVars& vars, UIId id);
};

class uiDiseaseMaxEvents : public SetGetLong
{
  public:
    uiDiseaseMaxEvents();
    virtual ~uiDiseaseMaxEvents();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiDiseaseGlobal : public uiParameter
{
  public:
    uiDiseaseGlobal();
    virtual ~uiDiseaseGlobal();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiDiseaseByID : public uiParameter
{
  public:
    uiDiseaseByID();
    virtual ~uiDiseaseByID();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiDiseaseInto: public GetString
{
  public:
    uiDiseaseInto();
    virtual ~uiDiseaseInto();
    virtual std::string Get(UIVars& vars, UIId id);
    virtual string Description(UIVars& vars, UIId id);
};

class uiDiseaseLocation : public SetGetLong
{
  public:
    uiDiseaseLocation();
    virtual ~uiDiseaseLocation();
    virtual long Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, long val);
};

class uiStartValue : public SetGetDouble
{
  public:
    uiStartValue();
    virtual ~uiStartValue();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

class uiStartValueMethod : public SetGetMethodType
{
  public:
    uiStartValueMethod();
    virtual ~uiStartValueMethod();
    virtual method_type Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, method_type val);
};

class uiTrueValue : public SetGetDouble
{
  public:
    uiTrueValue();
    virtual ~uiTrueValue();
    virtual double Get(UIVars& vars, UIId id);
    virtual void Set(UIVars& vars, UIId id, double val);
};

#endif  // FORCE_INTERFACE_H

//____________________________________________________________________________________
