// $Id: traitmodel_interface.h,v 1.6 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TRAITMODEL_INTERFACE_H
#define TRAITMODEL_INTERFACE_H

#include <string>
#include "setget.h"
#include "rangex.h"

class UIVars;

class uiTraitModelName : public GetString
{
  public:
    uiTraitModelName();
    virtual ~uiTraitModelName();
    virtual std::string Get(UIVars& vars, UIId id);
};

class SetRangepair : public SetGetTemplate<rangepair>
{
  public:
    SetRangepair(const string& key);
    virtual ~SetRangepair();
    virtual rangepair Get(UIVars& vars, UIId id); //throws
    virtual rangepair GetValFromString(UIVars& vars, string val);
};

class uiAddRangeForTraitModel : public SetRangepair
{
  public:
    uiAddRangeForTraitModel();
    virtual ~uiAddRangeForTraitModel();
    virtual void Set(UIVars& vars, UIId id, rangepair val);
};

class uiRemoveRangeForTraitModel : public SetRangepair
{
  public:
    uiRemoveRangeForTraitModel();
    virtual ~uiRemoveRangeForTraitModel();
    virtual void Set(UIVars& vars, UIId id, rangepair val);
};

class uiSetTraitModelRangeToPoint : public SetGetLong
{
  public:
    uiSetTraitModelRangeToPoint();
    virtual ~uiSetTraitModelRangeToPoint();
    virtual void Set(UIVars& vars, UIId id, long val);
    virtual long Get(UIVars& vars, UIId id); //throws!
};

class uiValidMovingLoci : public GetUIIdVec1d
{
  public:
    uiValidMovingLoci();
    virtual ~uiValidMovingLoci();
    virtual UIIdVec1d Get(UIVars& vars, UIId id);
};

class uiTraitModelFloat : public SetGetNoval
{
  public:
    uiTraitModelFloat();
    virtual ~uiTraitModelFloat();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiTraitModelJump : public SetGetNoval
{
  public:
    uiTraitModelJump();
    virtual ~uiTraitModelJump();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiTraitModelData : public SetGetNoval
{
  public:
    uiTraitModelData();
    virtual ~uiTraitModelData();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

class uiTraitModelPartition : public SetGetNoval
{
  public:
    uiTraitModelPartition();
    virtual ~uiTraitModelPartition();
    virtual void Set(UIVars& vars, UIId id, noval val);
};

#endif  // DATAMODEL_INTERFACE_H

//____________________________________________________________________________________
