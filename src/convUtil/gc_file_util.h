// $Id: gc_file_util.h,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FILE_UTIL_H
#define GC_FILE_UTIL_H

#include "gc_errhandling.h"
#include "wx/string.h"

class wxFileInputStream;
class wxTextInputStream;

class gc_file_error : public gc_ex
{
  public:
    gc_file_error(const wxString &) throw();
    virtual ~gc_file_error() throw();
};

class gc_eof : public gc_file_error
{
  public:
    gc_eof() throw();
    virtual ~gc_eof() throw();
};

class gc_file_missing_error : public gc_file_error
{
  public:
    gc_file_missing_error(const wxString & fileName) throw();
    virtual ~gc_file_missing_error() throw();
};

class gc_file_read_error : public gc_file_error
{
  public:
    gc_file_read_error() throw();
    gc_file_read_error(const wxString & fileName) throw();
    virtual ~gc_file_read_error() throw();
};

wxString
ReadLineSafely(wxFileInputStream * fStream, wxTextInputStream * tStream);

wxString
ReadWordSafely(wxFileInputStream & fStream, wxTextInputStream & tStream);

#endif  // GC_FILE_UTIL_H

//____________________________________________________________________________________
