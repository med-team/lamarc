// $Id: gc_file_util.cpp,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_errhandling.h"
#include "gc_file_util.h"
#include "gc_strings_io.h"
#include "wx/log.h"
#include "wx/txtstrm.h"
#include "wx/wfstream.h"

//------------------------------------------------------------------------------------

gc_file_error::gc_file_error(const wxString & msg) throw()
    : gc_ex(msg)
{
}

gc_file_error::~gc_file_error() throw()
{}

//------------------------------------------------------------------------------------

gc_eof::gc_eof() throw()
    : gc_file_error(gc_io::eof)
{
}

gc_eof::~gc_eof() throw()
{}

//------------------------------------------------------------------------------------

gc_file_missing_error::gc_file_missing_error(const wxString & fileName) throw()
    : gc_file_error(wxString::Format(gc_io::fileMissing,fileName.c_str()))
{
}

gc_file_missing_error::~gc_file_missing_error() throw()
{}

//------------------------------------------------------------------------------------

gc_file_read_error::gc_file_read_error() throw()
    : gc_file_error(gc_io::fileReadError)
{
}

gc_file_read_error::gc_file_read_error(const wxString & fileName) throw()
    : gc_file_error(wxString::Format(gc_io::fileReadErrorWithName,fileName.c_str()))
{
}

gc_file_read_error::~gc_file_read_error() throw()
{}

//------------------------------------------------------------------------------------

wxString ReadLineSafely(wxFileInputStream * fStream,
                        wxTextInputStream * tStream)
{
    assert(fStream != NULL);
    assert(tStream != NULL);
    wxString line  = tStream->ReadLine();
    wxStreamError lastError = fStream->GetLastError();
    if(lastError == wxSTREAM_EOF)
        // EWFIX.BUG.698 -- ok to get eof if this is end of file
    {
        if(!line.IsEmpty())
        {
            return line;
        }
        else
        {
            throw gc_eof();
        }
    }
    if(lastError != wxSTREAM_NO_ERROR)
    {
        throw gc_file_read_error();
    }
    return line;
}

//------------------------------------------------------------------------------------

wxString ReadWordSafely(wxFileInputStream & fStream,
                        wxTextInputStream & tStream)
{
    wxString line  = tStream.ReadWord();
    wxStreamError lastError = fStream.GetLastError();
    if(lastError == wxSTREAM_EOF)
    {
        throw gc_eof();
    }
    if(lastError != wxSTREAM_NO_ERROR)
    {
        throw gc_file_read_error();
    }
    return line;
}

//____________________________________________________________________________________
