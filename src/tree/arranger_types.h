// $Id: arranger_types.h,v 1.16 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef ARRANGER_TYPES_H
#define ARRANGER_TYPES_H

#include <string>

class arrangerstrings
{
  public:
    static const std::string BASE   ;
    static const std::string RESIM  ;
    static const std::string DROP   ;
    static const std::string HAP    ;
    static const std::string BAYES  ;
    static const std::string DENO   ;
    static const std::string RECSITE;
    static const std::string PROBHAP;
    static const std::string TREESIZE;
    static const std::string LOCUS  ;
    static const std::string ZILCH  ;
    static const std::string STICK  ;
    static const std::string EPOCHSIZE;
    static const std::string EPOCHNUDGE;

};

#endif  // ARRANGER_TYPES_H

//____________________________________________________________________________________
