// $Id: branchtag.cpp,v 1.7 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>

#ifdef DMALLOC_FUNC_CHECK
#include <dmalloc.h>
#endif

#include "branchtag.h"

//------------------------------------------------------------------------------------

BranchTag::BranchTag()
{
    assert(!NextID().empty());
    m_id = NextID().front();
    NextID().pop_front();
    assert(m_id < RefCount().size());
    RefCount()[m_id] = 1;
} // BranchTag::ctor

//------------------------------------------------------------------------------------

BranchTag::BranchTag(const BranchTag& src)
{
    m_id = src.m_id;
    RefCount()[m_id]++;
}

//------------------------------------------------------------------------------------

BranchTag& BranchTag::operator=(const BranchTag& src)
{
    DecrementID(m_id);
    m_id = src.m_id;
    RefCount()[m_id]++;
    return *this;
}

//------------------------------------------------------------------------------------

void BranchTag::DecrementID(long oldid)
{
    assert(RefCount()[oldid] > 0);
    if (--RefCount()[oldid])
        return;

    NextID().push_back(oldid);
}

//------------------------------------------------------------------------------------

BranchTag::~BranchTag()
{
    DecrementID(m_id);
}

//------------------------------------------------------------------------------------

void BranchTag::BeginBranchIDs(long nids)
{
    assert(RefCount().empty());
    RefCount().assign(nids, 0L);
    assert(RefCount().size()==static_cast<LongVec1d::size_type>(nids));
    assert(NextID().empty());
    long i;
    for(i = 0; i < nids; ++i)
        NextID().push_back(i);
}

//------------------------------------------------------------------------------------

bool BranchTag::operator==(const BranchTag& src) const
{
    return m_id == src.m_id;
}

//------------------------------------------------------------------------------------

long BranchTag::ID() const
{
    return m_id;
}

//------------------------------------------------------------------------------------
// See FAQ 16.15 for an explanation of this opaque code....
//------------------------------------------------------------------------------------

LongVec1d& BranchTag::RefCount()
{
    static LongVec1d* refcount = new LongVec1d();
    return *refcount;
}

//------------------------------------------------------------------------------------

std::deque<long>& BranchTag::NextID()
{
    // clearly it is not behaving as I expected when I wrote it!
    static std::deque<long> * nextid = new std::deque<long>();
    return *nextid;
}

//____________________________________________________________________________________
