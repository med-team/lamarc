// $Id: argtree.h,v 1.4 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Jim McGill, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef ARGTREE_H
#define ARGTREE_H

//#include <cassert>  // May be needed for inline definitions.
#include <string>
#include <vector>

#include "branch.h" // for Branch_ptr declaration

using std::string;

//------------------------------------------------------------------------------------

class Tree;

//------------------------------------------------------------------------------------
// This is used to collect the ARG information from the input XML
// so it is available in phase 2 after the tree tips are created.

class ARGEdge
{
  private:
    // edge information
    long   m_target;
    long   m_source;
    Branch_ptr m_targetptr;             // for transfer of tip to new tree
    string m_partitions;
    string m_livesites;
    string m_transmittedsites;

    // target information
    long   m_targetid;                  // used internally in ParseTreeToData::DoARGtree()
                                        // when hooking the edges to the nodes
                                        // equals m_target when done
    string m_type;
    double m_time;
    string m_label;
    long   m_recloc;

    // we accept the default dtor, copy-ctor, and operator=

  public:

    ARGEdge(long target, long source);

    // Setter functions
    void SetTargetId(long targetid)                     {m_targetid = targetid;};
    void SetTarget(long target)                         {m_target = target;};
    void SetTargetPtr(Branch_ptr targetptr)             {m_targetptr = targetptr;};
    void SetType(string type)                           {m_type = type;};
    void SetTime(double time)                           {m_time = time;};
    void SetLabel(string label)                         {m_label = label;};
    void SetRecLoc(long recloc)                         {m_recloc = recloc;};
    void SetPartitions(string partitions)               {m_partitions = partitions;};
    void SetLiveSites(string livesites)                 {m_livesites = livesites;};
    void SetTransmittedSites(string transmittedsites)   {m_transmittedsites = transmittedsites;};

    //Getter functions
    long        GetTargetId()           const {return m_targetid;};
    string      GetType()               const {return m_type;};
    double      GetTime()               const {return m_time;};
    string      GetLabel()              const {return m_label;};
    long        GetRecLoc()             const {return m_recloc;};
    long        GetTarget()             const {return m_target;};
    Branch_ptr  GetTargetPtr()          const {return m_targetptr;};
    long        GetSource()             const {return m_source;};
    string      GetPartitions()         const {return m_partitions;};
    string      GetLiveSites()          const {return m_livesites;};
    string      GetTransmittedSites()   const {return m_transmittedsites;};

};

//------------------------------------------------------------------------------------

class ARGTree
{
  private:
    long m_totalSites;  // total sites, same for all nodes
    void TransferEdgeData(Branch_ptr bptr, ARGEdge edge);
    bool ProcessForce(Branch_ptr bptr, string forcestr);

  public:
    void ToLamarcTree(Tree& stump, vector<ARGEdge> argedges);
};

//------------------------------------------------------------------------------------

#endif // ARGTREE_H

//____________________________________________________________________________________
