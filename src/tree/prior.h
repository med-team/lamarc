// $Id: prior.h,v 1.10 2018/01/03 21:33:04 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


//A prior is a shape.  Currently, we have two possible shapes:  a rectangle
// in linear space, and a rectangle in log space.  Those are simple enough
// that we don't really need a class for them, but if we want more complex
// priors in the future (say, a gaussian curve) we will probably want this.
// RandomDraw() encapsulates the usefulness of a prior--it picks a random
// number within its shape, with the chance based on volume.

//------------------------------------------------------------------------------------

#ifndef PRIOR_H
#define PRIOR_H

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
#include "constants.h"

class UIVarsPrior;
class ParamStatus;

//------------------------------------------------------------------------------------

class Prior
{
  private:
    Prior();                            // undefined
    priortype  m_priortype;
    double     m_lowerbound;
    double     m_upperbound;
    double     m_lnlower;               // speed optimization--precalculated.
    double     m_lnupper;
    double     m_binwidth;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    long int   m_samplingRate;
#endif

  public:
    Prior(UIVarsPrior uiprior);
    Prior(ParamStatus shouldBeInvalid);
    //Use the default copy constructor.
    virtual ~Prior();
    bool      operator==(const Prior src) const; // compare priors

    virtual priortype GetPriorType()  const { return m_priortype; };
    virtual double    GetLowerBound() const { return m_lowerbound; };
    virtual double    GetUpperBound() const { return m_upperbound; };
    virtual double    GetBinwidth()   const { return m_binwidth; };
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual long      GetSamplingRate()   const { return m_samplingRate; };
#endif

    virtual std::pair<double, double> RandomDraw() const;
    virtual std::pair<double,double> RandomDrawWithReflection(double current, double windowpercent) const;
    virtual std::pair<double, double> RandomDrawWithinBounds(double lower, double upper) const;
};

#endif // PRIOR_H

//____________________________________________________________________________________
