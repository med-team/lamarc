// $Id: collmanager.h,v 1.19 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// The CollectionManager class contains all the Collectors and hands out
// information about them.

// Mary 2003/12/17

#ifndef COLLMANAGER_H
#define COLLMANAGER_H

#include <vector>
#include <utility>    // for pair
#include "vectorx.h"
#include "arranger.h"
#include "collector.h"
#include "forcesummary.h"
#include "chainstate.h"
#include "local_build.h"

class ArrangerVec;

//------------------------------------------------------------------------------------
// Collection manager
//------------------------------------------------------------------------------------

// typedef std::vector<std::pair<ForceSummary, long> > ParamSummary;
// ForceSummary object is supposed to be a Singleton.  Commenting typedef
// out to make sure it's not being used.  Jon 2012/05/21

class CollectionManager
{
  private:
    // This class is a singleton and is not to be copied.
    CollectionManager();                                    // not defined
    CollectionManager(const CollectionManager&);            // not defined
    CollectionManager& operator=(const CollectionManager&); // not defined

    // Dimensions of these are regions x replicates.
    std::vector<std::vector<TreeCollector*> > m_treecoll;
    std::vector<std::vector<ParamCollector*> > m_paramcoll;
    std::vector<std::vector<MapCollector*> > m_mapcoll;

    long m_region;
    long m_replicate;
    bool m_sample_trees;
    bool m_sample_params;
    bool m_sample_maps;

  public:
    CollectionManager(long nregions, long nreplicates);
    ~CollectionManager();

    // Sample
    void Collect(ChainState& chstate, long initialOrFinal, bool lastchain); // EWFIX.CHAINTYPE
    void WriteTraceFile(ChainState& chstate, long initialOrFinal);          // EWFIX.CHAINTYPE
    void WriteNewickTreeFile(ChainState& chstate);
    void WriteReclocsFile(ChainState& chstate, bool lastchain);             // EWFIX.CHAINTYPE

#ifdef LAMARC_QA_TREE_DUMP
    void WriteArgFile(ChainState& chstate, bool lastchain);
#endif

    void CorrectForFatalAttraction(long region);
    void StartChain(long reg, long rep, bool lastchain);
    void WriteThisChainsCollections(std::ofstream* out) const;
    void WriteLastSummaries() const;
    void WriteAllSummaries(long reg, long rep, std::ofstream& out) const;

    // Fetch packages of samples.
    TreeCollector* GetTreeColl(long region, long rep) const;
    std::vector<TreeCollector*> GetTreeColl(long region) const;
    std::vector<std::vector<TreeCollector*> > GetTreeColl() const;

    ParamCollector* GetParamColl(long region, long rep) const;
    std::vector<ParamCollector*> GetParamColl(long region) const;
    std::vector<std::vector<ParamCollector*> > GetParamColl() const;

    MapCollector* GetMapColl(long region, long rep) const;
    std::vector<MapCollector*> GetMapColl(long region) const;
    std::vector<std::vector<MapCollector*> > GetMapColl() const;

    // Inform Arrangers of changes
    void UpdateArrangers(ArrangerVec& arrangers, ChainState& chstate) const;

    bool GetSampleTrees() const { return m_sample_trees; };
};

#endif  // COLLMANAGER_H

//____________________________________________________________________________________
