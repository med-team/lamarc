// $Id: vectorx.cpp,v 1.15 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <cmath>

#include "vectorx.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

using namespace std;

//------------------------------------------------------------------------------------
// maintenance functions

// forward declarations if helper functions of the maintenace functions
double logsave(const double value);
double log0(const double value);

void LogVec0(const vector<double> &in, vector<double> &out)
{
    assert(in.size() == out.size());
    transform(in.begin(),in.end(),out.begin(),log0);
}

// helper function that keeps the structural zeros intact
// used in LogVec0() in  PostLike::Setup0()
double log0(const double value)
{
    return ((value > 0.0) ? log(value) : 0.0);
}

long LongSquareRootOfLong(long shouldBeASquare)
{
    // this is a funny-looking way to take a square root,
    // necessitated by the fact that the C library only provides
    // floating-point square root, and we don't want to deal with
    // the consequences of rounding error.

    long i;

    for (i = 1; i <= long(shouldBeASquare/2); ++i)
    {
        if (i * i == shouldBeASquare)
        {
            return i;
        }
    }
    throw ("Attempt to take long square root of non-square");
}

//____________________________________________________________________________________

