// $Id: vector_constants.h,v 1.12 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// modelled after ui_interface/ui_strings.h

#ifndef VECTOR_CONSTS_H
#define VECTOR_CONSTS_H

#include <vector>
#include "vectorx.h"

using std::vector;

// values for the static const vectors below are set in vectro_consts.cpp
class vecconst
{
  public:
    static const DoubleVec1d percentiles;
    static  DoubleVec1d multipliers;
    static  DoubleVec1d growthmultipliers;
    static const DoubleVec1d growthfixed;
    static  DoubleVec1d logisticselectionmultipliers;
    static const DoubleVec1d logisticselectionfixed;
    static const DoubleVec1d percentiles_short;
    static const DoubleVec1d multipliers_short;
};

#endif // VECTOR_CONSTS_H

//____________________________________________________________________________________
