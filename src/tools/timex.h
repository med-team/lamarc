// $Id: timex.h,v 1.8 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TIMEX_H
#define TIMEX_H

#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "vectorx.h"
#include "stringx.h"

/*****************************************************************
 These functions handle time information.  GetTime retrieves the
 current time as a time_t (seconds since the epoch).  PrintTime
 turns a time_t into a formatted string.
 Mary Kuhner (based on code of Peter Beerli) October 2000
*****************************************************************/

string PrintTime(const time_t mytime, const string format = "%H:%M:%S");

time_t GetTime();

#endif // TIMEX_H

//____________________________________________________________________________________
