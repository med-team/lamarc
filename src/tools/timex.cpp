// $Id: timex.cpp,v 1.7 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include "timex.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

/*===============================================
  timer utilities <Peter Beerli>

  ts = "%c" -> time + full date (see man strftime)
  = "%H:%M:%S" -> time hours:minutes:seconds

  This is C masquerading as C++, since it has to interface
  with C utilities.  --Mary

  WARNING!  Make sure that time_t.h exists before calling these!
*/

string PrintTime (const time_t mytime, const string format)
{
    const int arraylength = 80;

#ifdef NOTIME_FUNC
    // The system does not provide a clock so we return blanks.
    string tempstring;
    tempstring.assign(format.size,' ');
    return(tempstring);
#endif

    struct tm *nowstruct;
    char temparray[arraylength];
    if (mytime != (time_t) - 1)  // invalid time marked as -1
    {
        nowstruct = localtime (&mytime);
        strftime (temparray, arraylength, format.c_str(), nowstruct);
        return string(temparray);
    }
    else
    {                                   // time returned is invalid
        string tempstring;              // so we return blanks
        tempstring.assign(format.size(),' ');
        return(tempstring);
    }
} // PrintTime

//------------------------------------------------------------------------------------

time_t GetTime ()
{
#ifdef NOTIME_FUNC
    // The system does not provide a clock.
    return((time_t)-1);
#else
    // Return the "real" time.
    return(time(NULL));
#endif
} // GetTime

//____________________________________________________________________________________
