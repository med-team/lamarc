// $Id: vector_constants.cpp,v 1.18 2018/01/03 21:33:03 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// vector constants, used for profile tables
// Peter Beerli December 2002 modelled after Elizabeth Walkup's ui_string.[h][cpp] class

#include "vector_constants.h"

using std::vector;

// vectors assigned here should be defined in vector_consts.h as
// public static const DoubleVec1d (etc.)  members of class Vecconst
// data file menu

const long P = 10;
const long GF = 8;
const long LSF = 7; // logistic selection
const long SHORT = 2;

//PERC must be in order, and must go from lowest to highest.  It should also
// not include 0.5, since that means nothing to profiling--it assumes a
// symmetric likelihood surface.
const double PERC[P] = {0.005, 0.025, 0.05, 0.125, 0.25, 0.75, 0.875, 0.95, 0.975, 0.995};
const double SHORTPERC[SHORT] = {0.025, 0.975};
const double SHORTMULT[SHORT] = {0.1, 10.0};
const double GFIXED[GF] = {-100.0, -10.0, 0.0, 10.0, 25.0, 100.0, 250.0, 1000.0};
const double LSFIXED[LSF] = {-0.05, -0.005, 0.0, 0.005, 0.01, 0.1, 1.0};

#ifdef NDEBUG
const long M = 8;
const long G = 4;
const long LS = 4;
const double MULT[M] = {0.1, 0.2, 0.5, 0.75, 2.0, 5.0, 7.5, 10.0};
const double GMULT[G] = {0.5, 0.66666666667, 1.5, 2.0};
const double LSMULT[LS] = {0.5, 0.66666666667, 1.5, 2.0};
#else //add '1.0' to the multiplier list.
const long M = 9;
const long G = 5;
const long LS = 5;
const double MULT[M] = {0.1, 0.2, 0.5, 0.75, 1.0, 2.0, 5.0, 7.5, 10.0};
const double GMULT[G] = {0.5, 0.66666666667, 1.0, 1.5, 2.0};
const double LSMULT[LS] = {0.5, 0.66666666667, 1.0, 1.5, 2.0};
#endif

const vector <double> vecconst::percentiles(PERC, PERC + P);
vector <double> vecconst::multipliers(MULT, MULT + M);
vector <double> vecconst::growthmultipliers(GMULT, GMULT + G);
const vector <double> vecconst::growthfixed(GFIXED, GFIXED + GF);

vector <double> vecconst::logisticselectionmultipliers(LSMULT, LSMULT + LS);
const vector <double> vecconst::logisticselectionfixed(LSFIXED, LSFIXED + LSF);

const vector <double> vecconst::percentiles_short(SHORTPERC, SHORTPERC + SHORT);
const vector <double> vecconst::multipliers_short(SHORTMULT, SHORTMULT + SHORT);

// The various _short vectors are used instead of the long ones in
// force/forcesummary.cpp if CONCISE output file report is indicated by the
// user.  This prevents excess calculations by the profiler, and ensures that
// the reporter works, too.

//____________________________________________________________________________________
