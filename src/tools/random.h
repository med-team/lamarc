// $Id: random.h,v 1.18 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef RANDOM_H
#define RANDOM_H

#include <string>
#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>

#define MAX_RANDOM  4294967296.0        // 2^32

class Random
{
    Random(const Random&);          // undefined
    Random operator=(Random);       // undefined

    long num0, num1, num2, num3;
    long n0, n1, n2, n3;
    long m0, m1, m2, m3;

    boost::mt19937 m_rng;

  public:
    // The default ctor seeds the RNG from the system clock.  Warning:  if you
    // call it twice in quick succession the answer will probably be the same!
    // It is using *seconds*, not milliseconds.
    Random();
    Random(long seed);
    void Seed(long seed);
    void Read(char*);
    void Write(char*);
    long Long(long range);
    double Float();
    char Base();
    bool Bool();
    std::string Name();
    double Normal();
};

#endif // RANDOM_H

//____________________________________________________________________________________
