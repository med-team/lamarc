// $Id: xml_report.h,v 1.8 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peeter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef XML_REPORT_H
#define XML_REPORT_H

#include <string>
#include "tinyxml.h"

class ChainManager;
class Parameter;

/******************************************************************
This class produces an XML version of the outfile.txt
At initial writing, it is not a complete version, just
what I need to evaluate simulations of the new lamarc
data uncertainty model

Elizabeth Walkup  December 2008

******************************************************************/

class ChainOut;

class XMLReport
{
  private:
    std::string     m_filename;
    TiXmlDocument   m_doc;

  protected:
    void            AddSlicesTo(TiXmlElement *,
                                const Parameter&,
                                force_type,
                                long regNo,
                                bool doOverall);
    TiXmlElement *  MakeParameter(const Parameter&, const force_type);
    TiXmlElement *  MakeParameters();
    TiXmlElement *  MakeChainReport(const   ChainOut&,
                                    size_t  regNo,
                                    size_t  repNo,
                                    size_t  chainNo);
    TiXmlElement *  MakeWarnings();

    void            AddRangeElements(TiXmlElement *, rangeset sites, std::string label);

  public:
    XMLReport(std::string filename);
    virtual ~XMLReport();
    TiXmlElement * Write(const ChainManager&);
};

#endif // XML_REPORT_H

//____________________________________________________________________________________
