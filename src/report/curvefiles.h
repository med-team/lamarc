// $Id: curvefiles.h,v 1.3 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peeter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CURVEFILES_H
#define CURVEFILES_H

#include <fstream>
#include <iostream>
#include <string>
#include "vectorx.h"

class BayesAnalyzer_1D;
class Force;
class Parameter;

void writeCurveHeader(  std::ofstream &     curvefileStream,
                        std::string         pname,
                        long                paramnum,
                        long                regions,
                        long                replicates,
                        BayesAnalyzer_1D &  ba);

double getMinIndex(BayesAnalyzer_1D&,long,long);

void WriteOneConsolidatedCurveFile( std::string         filePrefix,
                                    BayesAnalyzer_1D &  bayesAnalyzer,
                                    const Parameter &   param,
                                    long                paramnum);
void WriteConsolidatedCurveFiles(   std::string         filePrefix,
                                    BayesAnalyzer_1D &  bayesAnalyzer);

#endif // CURVEFILES_H

//____________________________________________________________________________________
