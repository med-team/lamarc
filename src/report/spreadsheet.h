// $Id: spreadsheet.h,v 1.3 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peeter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef SPREADSHEET_H
#define SPREADSHEET_H

#include <string>
#include "vectorx.h"

class Force;
class Parameter;
class TiXmlElement;

/******************************************************************
Classes for writing spreadsheet-ready versions of data from
outfile.txt

Elizabeth Walkup  September 2009

******************************************************************/

std::string makeFileName(   std::string     filePrefix,
                            std::string     paramName);
size_t      getNumSlices(   TiXmlElement *  estimateElem);
void        addFirstCol(    StringVec2d &   table,
                            TiXmlElement *  esitmateElem,
                            std::string &   profileTypeOut,
                            std::string &   analysisTypeOut);
void        addEstimate(    StringVec2d &   table,
                            size_t          estimateIndex,
                            TiXmlElement *  esitmateElem,
                            std::string     paramName,
                            std::string     regionName,
                            std::string     expectedProfileType,
                            std::string     expectedAnalysisType);

void        reorderEstimates(   std::vector<TiXmlElement*> &    estimates);

void WriteOneProfileSpread( std::string     filePrefix,
                            TiXmlElement *  forceElem,
                            TiXmlElement *  paramElem);

void WriteProfileSpreads(   std::string     filePrefix,
                            TiXmlElement *  reportTop);

#endif // SPREADSHEET_H

//____________________________________________________________________________________
