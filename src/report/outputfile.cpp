// $Id: outputfile.cpp,v 1.22 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <exception>
#include <iostream>

#include "outputfile.h"
#include "errhandling.h"
#include "registry.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

using namespace std;

//------------------------------------------------------------------------------------

OutputFile::OutputFile(const string& fname)
{

    m_outf.open(fname.c_str());

    if (!m_outf)
    {
        file_error e("Unable to open output file\n");
        throw e ;
    }

} // OutputFile::ctor

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

ResultsFile::ResultsFile()
    : OutputFile(registry.GetUserParameters().GetResultsFileName())
{

} // ResultsFile

//------------------------------------------------------------------------------------

void ResultsFile::AddReport(ReportPage &report)
{

    m_reports.push_back(&report);

} // AddReport

//------------------------------------------------------------------------------------

void ResultsFile::ShowReports()
{
    vector<ReportPage *>::iterator rpit;
    for(rpit = m_reports.begin(); rpit != m_reports.end(); ++rpit)
        (*rpit)->Show();

} // ShowReports

//------------------------------------------------------------------------------------

void ResultsFile::Display()
{
    verbosity_type verbosity = registry.GetUserParameters().GetVerbosity();

    MlePage estimatepage(m_outf);
    if (registry.GetChainParameters().IsBayesian())
    {
        estimatepage.Setup("Most Probable Estimates (MPEs) of Parameters");
    }
    else
    {
        estimatepage.Setup("Maximum Likelihood Estimates (MLEs) of Parameters");
    }

    MapPage mappingpage(m_outf);
    mappingpage.Setup("Mapping results");

    ProfPage profilepage(m_outf);
    profilepage.Setup("Profile Likelihoods");

    UsrPage userpage(m_outf);
    userpage.Setup("User Specified Options");

    DataPage echopage(m_outf);
    echopage.Setup("Data summary");

    RunPage runreportpage(m_outf);
    runreportpage.Setup("Run Reports by Region");

    AddReport(estimatepage);
    AddReport(profilepage);
    AddReport(mappingpage);
    AddReport(userpage);
    if (verbosity != CONCISE && verbosity != NONE)
    {
        AddReport(echopage);  //Echo data
        // We've taken the 'Echo data' option out of the menu, and just tied it
        // to the verbosity level of the output instead. --LS
        AddReport(runreportpage);
    }

    ShowReports();

} // Run

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

XMLOutfile::XMLOutfile() :
    OutputFile(registry.GetUserParameters().GetXMLOutFileName())
{
} // XMLOutfile::ctor

XMLOutfile::XMLOutfile(string outfileName) :
    OutputFile(outfileName)
{
} // XMLOutfile::ctor

//------------------------------------------------------------------------------------

// Any global model the user may have had will not be replicated
// by this code.  Instead every region contains it's own datamodel.
void XMLOutfile::Display()
{
    m_outf << "<lamarc version=\"" << VERSION << "\">" << endl;
    m_outf << "<!-- Created by the Lamarc program -->" << endl;

    unsigned long nindentspaces(INDENT_DEPTH);
    StringVec1d xmllines;

    StringVec1d chainxml(registry.GetChainParameters().ToXML(nindentspaces));
    xmllines.insert(xmllines.end(),chainxml.begin(),chainxml.end());

    StringVec1d userxml(registry.GetUserParameters().ToXML(nindentspaces));
    xmllines.insert(xmllines.end(),userxml.begin(),userxml.end());

    StringVec1d forcexml(registry.GetForceSummary().ToXML(nindentspaces));
    xmllines.insert(xmllines.end(),forcexml.begin(),forcexml.end());

    StringVec1d dataxml(registry.GetDataPack().ToXML(nindentspaces));
    xmllines.insert(xmllines.end(),dataxml.begin(),dataxml.end());

    StringVec1d::iterator line;
    for(line = xmllines.begin(); line != xmllines.end(); ++line)
    {
        m_outf << *line << endl;
    }

    m_outf << "</lamarc>" << endl;

} // XMLOutfile::Display

//____________________________________________________________________________________
