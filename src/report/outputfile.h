// $Id: outputfile.h,v 1.11 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef OUTPUTFILE_H
#define OUTPUTFILE_H

#include <fstream>
#include <string>
#include "stringx.h"
#include "reportpage.h"
#include "constants.h"

// Class OutputFile is the abstract base class for dealing with file i/o
// at the end of the program.
//
// This class is not default-constructible or copyable.
//
// The class ctor will throw a file_error if ofstream.open() fails
class OutputFile
{
  private:
    // these are deliberately never implemented
    OutputFile();
    OutputFile(const OutputFile &src);
    OutputFile &operator=(const OutputFile &src);

  protected:
    ofstream m_outf;

  public:
    OutputFile(const string& fname);
    virtual ~OutputFile() { m_outf.close(); };

    virtual void Display() = 0;
};

// Class ResultsFile manages the collection of report pages that make
// up the final file output for the program.  It is expected to be a
// singleton.
//
// This class is not default-constructible or copyable.
//
// The class ctor does not catch any exceptions thrown by the base class.
class ResultsFile : public OutputFile
{
  private:
    // these are deliberately never implemented
    ResultsFile(const ResultsFile &src);
    ResultsFile &operator=(const ResultsFile &src);

    std::vector<ReportPage *> m_reports; // this points at local variables of Display()

    void AddReport(ReportPage &report);
    void ShowReports();

  public:
    ResultsFile();
    virtual ~ResultsFile() {};

    virtual void Display();

};

// Class XMLOutfile manages the creation of a correct xml input file from the
// current data structures of the program.
class XMLOutfile : public OutputFile
{
  private:
    // these are deliberately never implemented
    XMLOutfile(const XMLOutfile &src);
    XMLOutfile &operator=(const XMLOutfile &src);

  public:
    XMLOutfile();
    XMLOutfile(std::string outfileName);
    virtual ~XMLOutfile() {};

    virtual void Display();
};

#endif // OUTPUTFILE_H

//____________________________________________________________________________________
