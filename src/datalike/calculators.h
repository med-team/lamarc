// $Id: calculators.h,v 1.14 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef CALCULATORS_H
#define CALCULATORS_H

#include <map>
#include <string>
#include <vector>
#include <deque>
#include "defaults.h"
#include "vectorx.h"

class DataModel;
class DataPack;
class Region;
class Locus;

using std::map;
using std::string;
using std::vector;

DoubleVec1d FrequenciesFromData(const Locus& locus, model_type);

DoubleVec1d FrequenciesFromData(long regionId, long locusId, model_type);

void MigFSTLocus(const DataPack& dpack,const Locus& loc, long nmigs,
                 DoubleVec1d& estimate, std::deque<bool>& isCalculated);

// EWFIX.P5 DIMENSIONS -- change to 2d  EWFIX.P5 LOCUS

void MigFSTMultiregion(const DataPack& dpack, const DoubleVec2d& regionalmuratios,
                       DoubleVec1d& perRegionMigsAccumulator, std::deque<bool>&); // EWFIX.P5 DIMENSIONS

void ThetaFSTMultiregion(const DataPack& dpack, const DoubleVec1d&
                         regionalthetascalars, const DoubleVec2d& regionalmuratios, DoubleVec1d&
                         estimates, std::deque<bool>&);

void ThetaFSTLocus(const DataPack& dpack, const Locus& locus, DoubleVec1d&
                   estimates, std::deque<bool>&);

void ThetaWattersonMultiregion(const DataPack& dpack, const DoubleVec1d&
                               regionalthetascalars, const DoubleVec2d& regionalmuratios,
                               DoubleVec1d& estimates, std::deque<bool>&);

void ThetaWattersonLocus(const DataPack&,const Locus&,long numThetas,
                         vector<map<force_type,string> > tipids,
                         DoubleVec1d&,
                         std::deque<bool>&);

void ThetaWattersonRegion(const DataPack&, const Region&, double thetascalar,
                          const DoubleVec1d& muratios, DoubleVec1d& estimates, std::deque<bool>&);

#endif // CALCULATORS_H

//____________________________________________________________________________________
