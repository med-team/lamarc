// $Id: cellmanager.h,v 1.4 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/******************************************************************

The CellManager maintains a private internal store of the arrays
used by class DLCell and its children.  This is a speedup; we avoid
allocating and deallocating these arrays and reuse them instead.
This functionality was originally static in DLCell but that design
suffered order-of-static-deallocation issues especially on
Macs.  Here it is a Singleton which is abandoned on the heap, producing
the appearance of a memory leak, but at least no crashes.  DO NOT
give this class a destructor, as destroying it will reintroduce the
order-of-deallocation bug!

After each Region you should call ClearStore to get rid of old
arrays; they are unlikely to be useful in the new Region and will
eat up space.

The CellManager code assumes that all of the data-likelihood
arrays are three-dimensional contiguous allocation using new[].  If you
write one that isn't, do not use this code to manage it.

********************************************************************/

#ifndef CELLMANAGER_H
#define CELLMANAGER_H

#include "types.h"  // for definition of cellarray and FreeStore
#include "dlcell.h" // for definition of triplet and DLCell

class CellManager
{
  private:
    FreeStore     m_store;  // multimap of triplet and cellarray
    cellarray     MakeArray();

    // disabled functionality follows
    ~CellManager();  // undefined to prevent destruction
    CellManager(const CellManager&);
    CellManager&  operator=(const CellManager&);

  public:
    CellManager() {};
    cellarray     GetArray(triplet dimensions, DLCell& requester);
    void          FreeArray(triplet dimensions, cellarray array);
    void          ClearStore();
};

#endif // CELLMANAGER_H

//____________________________________________________________________________________
