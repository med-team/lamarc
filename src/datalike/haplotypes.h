// $Id: haplotypes.h,v 1.11 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Lucian Smith, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// This files defines the class that stores "haplotype" specific
// information.

#ifndef HAPLOTYPES_H
#define HAPLOTYPES_H

#include <set>
#include "vectorx.h"

class LocusCell;

class Haplotypes
{
  private:
    Haplotypes(); //undefined
    long m_regionnum;
    string m_locusname;
    StringVec2d  m_haplotype_alleles; //Phase 1
    DoubleVec1d  m_penetrances;

    vector<vector<LocusCell> > m_haplotype_dlcells; //Phase 2
    long m_current_hapindex;

    void ConvertAllelesToDLCells();
    void CollapseHaplotypeDLs();
    StringVec2d SetToVecs(std::multiset<std::string> stringSet) const;

  public:
    Haplotypes(long regnum, string lname);
    Haplotypes(Haplotypes hap, bool clear);
    ~Haplotypes() {};
    //We accept the default for:
    //Haplotype& operator=(const Haplotype& src);
    //Haplotype(const Haplotype& src);

    void AddHaplotype(StringVec1d alleles, double penetrance);
    void AddHaplotype(std::multiset<std::string> alleles, double penetrance);
    vector<LocusCell> ChooseNewHaplotypes();
    vector<LocusCell> ChooseRandomHaplotypes();
    vector<LocusCell> ChooseFirstHaplotypes();
    vector<LocusCell> ChooseNextHaplotypes();
    StringVec1d GetAlleles() const; //phase 1
    string GetMarkerData() const; //phase 3 (output)
    StringVec1d GetHaplotypesXML(long nspaces) const; //menuinfile XML
    bool MultipleHaplotypes() const;

    // Debugging function.
    void PrintCellsAndAlleles() const;

};

#endif // HAPLOTYPES_H

//____________________________________________________________________________________
