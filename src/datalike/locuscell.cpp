// $Id: locuscell.cpp,v 1.14 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include "locuscell.h"

//------------------------------------------------------------------------------------

LocusCell::LocusCell(const vector<Cell_ptr>& src)
{
    DeepCopyCells(src);
} // LocusCell ctor

//------------------------------------------------------------------------------------

LocusCell::LocusCell(const LocusCell& src)
{
    DeepCopyCells(src.dlcells);
} // LocusCell copy ctor

//------------------------------------------------------------------------------------

LocusCell::LocusCell(const vector<LocusCell>& src)
{
    //For use when constructing a LocusCell from haplotypes, which come
    // in a marker-by-marker vector, and need to be concatenated.
    vector<Cell_ptr> src_ptrs;
    for (unsigned long marker=0; marker<src.size(); marker++)
    {
        assert(src.size() == 1);
        src_ptrs.push_back(src[marker][0]);
    }
    DeepCopyCells(src_ptrs);
} // LocusCell copy ctor

//------------------------------------------------------------------------------------

LocusCell& LocusCell::operator=(const LocusCell& src)
{
    DeepCopyCells(src.dlcells);
    return *this;
} // operator=

//------------------------------------------------------------------------------------

LocusCell& LocusCell::operator+=(const LocusCell& src)
{
    assert(dlcells.size() == src.dlcells.size());
    for (size_t cell=0; cell<dlcells.size(); cell++)
    {
        (*dlcells[cell]).AddTo(src.dlcells[cell]);
    }
    return *this;
} // operator=

//------------------------------------------------------------------------------------

LocusCell& LocusCell::operator*=(double mult)
{
    for (size_t cell=0; cell<dlcells.size(); cell++)
    {
        (*dlcells[cell]).MultiplyBy(mult);
    }
    return *this;
} // operator=

//------------------------------------------------------------------------------------

bool LocusCell::operator==(const LocusCell& src) const
{
    unsigned long cell;
    for (cell = 0; cell < dlcells.size(); ++cell)
    {
        if (dlcells[cell]->DiffersFrom(src.dlcells[cell]) != FLAGLONG)
            return false;
    }
    return true;
} // operator==

//------------------------------------------------------------------------------------

void LocusCell::DeepCopyCells(const vector<Cell_ptr>& src)
{
    dlcells.clear();
    unsigned long cell;
    for (cell = 0; cell < src.size(); ++cell)
    {
        dlcells.push_back(Cell_ptr(src[cell]->Copy()));
    }

} // DeepCopyCells

//------------------------------------------------------------------------------------

bool LocusCell::DiffersFrom(const LocusCell& other, long marker) const
{
    return !dlcells[0]->IsSameAs(other.dlcells[0],marker);
} // DiffersFrom

void LocusCell::SetAllCategoriesTo(DoubleVec1d& state, long marker)
{
    dlcells[0]->SetAllCategoriesTo(state, marker);
}

//____________________________________________________________________________________
