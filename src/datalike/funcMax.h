// $Id: funcMax.h,v 1.9 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef FUNCMAX_H
#define FUNCMAX_H

#include <iomanip>
#include <fstream>
#include <cmath>

#include "dlcalc.h"
#include "dlmodel.h"
#include "locus.h"
#include "tree.h"

class MixedKSModel;

typedef double (*dblpf) (double);

struct FMState
{
    int max_iters;
    double increment;
    double threshold;
    double leftLimit;
    double rightLimit;
    double initX;
    dblpf pf;

    FMState(int _max_iters,
            double _increment,
            double _threshold,
            double _leftLimit,
            double _rightLimit,
            double _initX
        );
};

class FuncMax
{
    int max_iters;
    double increment;
    double threshold;
    double leftLimit;
    double rightLimit;
    double initX;
    dblpf pf;

    MixedKSModel& mksModel;
    std::ofstream* fs;
    DLCalc_ptr dlcalc;
    Tree* tree;
    const Locus& locus;
    bool m_moving;

  public:
    FuncMax(MixedKSModel& _mksModel, Tree* tree, const Locus& locus);
    void setState(FMState fms);
    void run();
    FMState getCurState();
    double getCurX();

  private:
    double curX;
    double eval(double x);
    bool isInc(double x, double max);
};

#endif  // FUNCMAX_H

//____________________________________________________________________________________
