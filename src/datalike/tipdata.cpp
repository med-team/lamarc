// $Id: tipdata.cpp,v 1.15 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <functional>                   // for Locus::CountNNucleotides()

#include "tipdata.h"
#include "constants.h"
#include "registry.h"
#include "dlcalc.h"
#include "dlmodel.h"
#include "mathx.h"                      // for IsEven
#include "force.h"                      // for TipData::GetBranchPartitions()
#include "xml_strings.h"                // for TipData::ToXML()

using namespace std;

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

TipData::TipData()
    : partitions(),
      individual(FLAGLONG),
      m_locus(FLAGLONG),
      m_hap(FLAGLONG),
      m_nodata(false),
      label(""),
      m_popname(""),
      data()
{
    // intentionally blank
} // TipData::TipData

//------------------------------------------------------------------------------------

void TipData::Clear()
{
    partitions.erase(partitions.begin(),partitions.end());
    individual = FLAGLONG;
    m_locus = FLAGLONG;
    m_hap = FLAGLONG;
    data.clear();

} // Clear

//------------------------------------------------------------------------------------

bool TipData::BelongsTo(long ind) const
{
    return (individual == ind);
} // BelongsTo

//------------------------------------------------------------------------------------

bool TipData::IsInPopulation(const string& popname) const
{
    return (m_popname == popname);
} // IsInPopulation

//------------------------------------------------------------------------------------

string TipData::GetFormattedData(const string& dlm) const
{
    string result;

    unsigned long i;
    for (i = 0; i < data.size(); ++i)
    {
        result += data[i] + dlm;
    }

    return result;

} // GetFormattedData

//------------------------------------------------------------------------------------

long TipData::GetPartition(force_type partname) const
{
    string partitionname = partitions.find(partname)->second;
    return registry.GetDataPack().GetPartitionNumber(partname,partitionname);
} // TipData::GetPartition

//------------------------------------------------------------------------------------

LongVec1d TipData::GetBranchPartitions() const
{
    LongVec1d parts(partitions.size(),FLAGLONG);
    const ForceSummary& forcesum = registry.GetForceSummary();
    const DataPack& datapack = registry.GetDataPack();
    map<force_type,string>::const_iterator pits;
    for(pits = partitions.begin(); pits != partitions.end(); ++pits)
    {
        force_type forcename = pits->first;
        string partname = pits->second;

        PartitionForce* pforce = dynamic_cast<PartitionForce*>
            (*forcesum.GetForceByTag(forcename));

        parts[pforce->GetPartIndex()] =
            datapack.GetPartitionNumber(forcename,partname);
    }

    return parts;

} // TipData::GetBranchPartitions

//------------------------------------------------------------------------------------

void TipData::RemovePartition(force_type forcename)
{
    if (partitions.erase(forcename) == 0)
        assert(false); // failed to erase something that wasn't there!

} // TipData::RemovePartition

//------------------------------------------------------------------------------------

void TipData::AddPartition(pair<force_type,string> newpart)
{
    partitions.insert(newpart);
} // TipData::AddPartition

//------------------------------------------------------------------------------------

bool TipData::IsInCrossPartition(map<force_type,string> xpart) const
{
    return (xpart == partitions);
} // TipData::IsInCrossPartition

//------------------------------------------------------------------------------------

data_source TipData::GetDataSource() const
{
    return m_source;
}

//------------------------------------------------------------------------------------

void TipData::SetDataSource(const string tag)
{
    if (CaselessStrCmp(tag, lamarcstrings::PANEL))
    {
        m_source = dsource_panel;
    }
    else
    {
        m_source = dsource_study;
    }
} // TipData::SetDataSource

//____________________________________________________________________________________
