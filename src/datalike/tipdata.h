// $Id: tipdata.h,v 1.10 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/***************************************************************
 The TipData class holds the linked genetic data for a tip in a tree of
 haplotypes.  It also stores, for the tip, its partition membership,
 individual membership, and name.

 In general, TipData is a helper class for locus that stores much of
 the tip specific information in a manner that is useful for tree generation
 and rearrangement.  Each locus owns a container of TipData objects, which
 correspond to the tips in the tree that "belong" to that particular locus.

 TipData written by Jim Sloan, revised by Jon Yamato
 2002/01/03 changed Tipdata::data to a vector for generality--Mary Kuhner
 2004/09/15 split TipData out into its own file--Mary Kuhner
****************************************************************/

#ifndef TIPDATA_H
#define TIPDATA_H

#include <string>
#include <vector>
#include <map>
#include "constants.h"
#include "types.h"
#include "vectorx.h"
#include "datatype.h"    // for DataType_ptr
#include "defaults.h"    // for force_type
#include "toxml.h"       // for SampleXML construction in TipData::ToXML()

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

class TipData
{
  public:
    std::map<force_type,std::string> partitions; // forcename & partitionname
    long individual;
    long m_locus;
    long m_hap;
    bool m_nodata;         // Flag for loci where the data is all in haplotypes
    std::string label;     // sample name
    std::string m_popname; // we need to keep this because of migration's special
    // status in the (current) xml
    StringVec1d data;      // one string per marker
    data_source m_source;  // source of data

    data_source GetDataSource() const;
    void SetDataSource(const string tag);

    TipData();
    // we accept the default copy-ctor and operator=

    void Clear(); // return tipdata to newly constructed form
    // used by xmlreader, DataFile::DoSamples()

    bool BelongsTo(long ind) const;
    bool IsInPopulation(const string& popname) const;

    std::string GetFormattedData(const std::string& dlm) const;
    long GetPartition(force_type partname) const;
    LongVec1d GetBranchPartitions() const;

    void RemovePartition(force_type);
    void AddPartition(std::pair<force_type,std::string> newpart);

    bool IsInCrossPartition(std::map<force_type,std::string> xpart) const;
};

#endif // TIPDATA_H

//____________________________________________________________________________________
