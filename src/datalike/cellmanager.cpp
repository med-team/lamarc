// $Id: cellmanager.cpp,v 1.6 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// This file contains the implementation code for the manager of data-likelihood
// storage, CellManager

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

#include "cellmanager.h"

//------------------------------------------------------------------------------------

cellarray CellManager::GetArray(triplet id, DLCell& requester)
{
    FreeStore::iterator it = m_store.find(id);

    if (it != m_store.end())            // found one
    {
        cellarray result = it->second;
        m_store.erase(it);
        return result;
    }
    else
        return requester.MakeArray();

} // GetArray

//------------------------------------------------------------------------------------

void CellManager::FreeArray(triplet id, cellarray array)
{
    m_store.insert(std::make_pair(id, array));

} // FreeArray

//------------------------------------------------------------------------------------

void CellManager::ClearStore()
{
    // assumes all cells are 3-D contiguous storage gotten with new[]!
    cellarray ar;
    FreeStore::iterator it = m_store.begin();
    for ( ; it != m_store.end(); ++it)
    {
        ar = it->second;
        delete [] ar[0][0];
        delete [] ar[0];
        delete [] ar;
    }
    m_store.erase(m_store.begin(), m_store.end());
} // ClearStore

//____________________________________________________________________________________
