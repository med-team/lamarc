// $Id: locuscell.h,v 1.10 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/*************************************************************************
 This class is a managed container of Cells belonging to a single
 locus.  It needs to be managed because Cells generally have to be
 deep copied.

 Written by Mary Kuhner
*************************************************************************/

#ifndef LOCUSCELL_H
#define LOCUSCELL_H

#include <cassert>  // May be needed for inline definitions.
#include <vector>
#include "dlcell.h"

//------------------------------------------------------------------------------------

class LocusCell
{
  private:
    std::vector<Cell_ptr> dlcells;

    void DeepCopyCells(const std::vector<Cell_ptr>& src);

  public:
    LocusCell()                                   {};
    LocusCell(const std::vector<Cell_ptr>& src);
    LocusCell(const LocusCell& src);
    LocusCell(const vector<LocusCell>& src);
    ~LocusCell()                                  {};

    LocusCell&     operator=(const LocusCell& src);
    LocusCell&     operator+=(const LocusCell& src);
    LocusCell&     operator*=(double mult);
    bool           operator==(const LocusCell& src) const;
    unsigned long  size() const             { return dlcells.size(); };
    void           clear()                  { dlcells.clear(); };

    Cell_ptr       operator[](long ind)     { assert(static_cast<unsigned long>(ind) < dlcells.size());
        return dlcells[ind]; };

    const Cell_ptr operator[](long ind) const
    { assert(static_cast<unsigned long>(ind) < dlcells.size());
        return dlcells[ind]; };

    bool           DiffersFrom(const LocusCell& other, long marker) const;
    void           SetAllCategoriesTo(DoubleVec1d& state, long marker);
};

#endif // LOCUSCELL_H

//____________________________________________________________________________________
