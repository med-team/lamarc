// $Id: report_strings.cpp,v 1.8 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "report_strings.h"

using std::string;

const string reportstr::header              = "Parameters for model of type: ";

const string reportstr::categoryHeader      = " rate categories with correlated length ";
const string reportstr::categoryRate        = "Relative rate";
const string reportstr::categoryRelativeProb= "Relative probability";

const string reportstr::baseFreqs           = "Base frequencies: ";
const string reportstr::baseFreqSeparator   = ", ";

const string reportstr::TTratio             = "Transition/transversion ratio: ";

const string reportstr::GTRRates            = "Mutation parameters: ";
const string reportstr::GTRRateSeparator    = ", ";
const string reportstr::GTRRatesFromA       = "Between A and (C, G, T): ";
const string reportstr::GTRRatesFromC       = "Between C and (G, T): ";
const string reportstr::GTRRatesFromG       = "Between G and (T): ";

const string reportstr::numberOfBins        = "number of bins: ";

const string reportstr::brownian            = "<Brownian model has no additional parameters>";

const string reportstr::alpha               = "alpha: ";
const string reportstr::optimizeAlpha       = "Optimized value of Alpha will be reported for each chain";
const string reportstr::relativeMutationRate= "Relative marker mutation rate: ";

const string reportstr::perBaseErrorRate    = "Per-base error rate: ";

//____________________________________________________________________________________
