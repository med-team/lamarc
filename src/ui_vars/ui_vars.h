// $Id: ui_vars.h,v 1.16 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_VARS_H
#define UI_VARS_H

#include "ui_interface.h"
#include "ui_vars_chainparams.h"
#include "ui_vars_datamodel.h"
#include "ui_vars_forces.h"
#include "ui_vars_userparams.h"
#include "ui_vars_datapackplus.h"
#include "ui_vars_traitmodels.h"

class DataPack;

using std::string;

// EWFIX.P5 ENHANCEMENT -- all of the aggregate members of this class have
// a variety of Set methods for setting values. However, we're not
// actually checking that those are good values in most cases.
// we need to create a specialized exception that the menu can
// catch and query the user to try again

// variables that can be changed by the user
class UIVars
{
  private:
    UIVars();   // undefined
    UIVars& operator=(const UIVars& vars); // undefined
    UIInterface* ui;

  public:
    // one might argue that the constructors should have
    // restricted access since only the UndoRedoChain should
    // be creating these puppies.
    UIVars(DataPack& datapack,string fileName,UIInterface* myui);
    UIVars(const UIVars& vars);
    virtual ~UIVars();
    virtual string GetParamNameWithConstraint(force_type ftpye, long pindex,
                                              bool doLongName=true) const;
    virtual UIInterface* GetUI() const {return ui;}

    // public members because we want direct access to their
    // public methods
    UIVarsChainParameters       chains;
    UIVarsUserParameters        userparams;
    UIVarsDataPackPlus          datapackplus;
    UIVarsDataModels            datamodel;
    UIVarsTraitModels           traitmodels;
    UIVarsForces                forces;
};

#endif  // UI_VARS_H

//____________________________________________________________________________________
