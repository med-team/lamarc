// $Id: ui_vars_component.cpp,v 1.6 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <cstddef>              // for NULL
#include "ui_vars_component.h"

//------------------------------------------------------------------------------------

UIVarsComponent::UIVarsComponent(UIVars * myUIVars)
    : m_UIVars(myUIVars)
{
}

UIVarsComponent::~UIVarsComponent()
{
    m_UIVars = NULL;
}

const UIVars &
UIVarsComponent::GetConstUIVars() const
{
    assert(m_UIVars != NULL);
    return (*m_UIVars);
}

UIVars &
UIVarsComponent::GetUIVars()
{
    assert(m_UIVars != NULL);
    return (*m_UIVars);
}

//____________________________________________________________________________________
