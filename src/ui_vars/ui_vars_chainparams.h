// $Id: ui_vars_chainparams.h,v 1.18 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_VARS_CHAINPARAMS_H
#define UI_VARS_CHAINPARAMS_H

#include <string>
#include "vectorx.h"
#include "ui_vars_component.h"

using std::string;

// variables that can be changed by the user
class UIVarsChainParameters : public UIVarsComponent
{
  private:
    //
    UIVarsChainParameters();                                // undefined
    UIVarsChainParameters(const UIVarsChainParameters&);    // undefined

    // variables for the ChainParameters
    bool                    adaptiveTemperatures;
    DoubleVec1d             chainTemperatures;
    double                  dropArrangerRelativeTiming;
    double                  sizeArrangerRelativeTiming;
    double                  haplotypeArrangerRelativeTiming;
    double                  probhapArrangerRelativeTiming;
    double                  bayesianArrangerRelativeTiming;
    double                  locusArrangerRelativeTiming;
    double                  zilchArrangerRelativeTiming;
    double                  stairArrangerRelativeTiming;
    double                  epochSizeArrangerRelativeTiming;
    bool                    doBayesianAnalysis;
    long                    finalChainSamplingInterval;
    long                    finalNumberOfChains;
    long                    finalNumberOfChainsToDiscard;
    long                    finalNumberOfSamples;
    long                    initialChainSamplingInterval;
    long                    initialNumberOfChains;
    long                    initialNumberOfChainsToDiscard;
    long                    initialNumberOfSamples;
    long                    numberOfReplicates;
    long                    temperatureInterval;

  public:
    // one might argue that the constructors should have
    // restricted access since only UIVars should
    // be creating these puppies.
    UIVarsChainParameters(UIVars*);
    UIVarsChainParameters(UIVars*,const UIVarsChainParameters&);

    virtual ~UIVarsChainParameters();

    /////////////////////////////////////////////////////////////
    // Get Methods for chain parameter variables
    virtual bool           GetAdaptiveTemperatures() const;
    virtual bool           GetDoBayesianAnalysis() const;
    virtual long           GetChainCount() const;
    virtual double         GetChainTemperature(long chainId) const;
    virtual DoubleVec1d    GetChainTemperatures() const;
    virtual double         GetDropArrangerRelativeTiming() const;
    virtual double         GetSizeArrangerRelativeTiming() const;
    virtual double         GetBayesianArrangerRelativeTiming() const;
    virtual double         GetLocusArrangerRelativeTiming() const;
    virtual double         GetZilchArrangerRelativeTiming() const;
    virtual double         GetStairArrangerRelativeTiming() const;
    virtual double         GetEpochSizeArrangerRelativeTiming() const;
    virtual long           GetFinalChainSamplingInterval() const;
    virtual long           GetFinalNumberOfChains() const;
    virtual long           GetFinalNumberOfChainsToDiscard() const;
    virtual long           GetFinalNumberOfSamples() const;
    virtual bool           GetHaplotypeArrangerPossible() const;
    virtual double         GetHaplotypeArrangerRelativeTiming() const;
    virtual bool           GetProbHapArrangerPossible() const;
    virtual double         GetProbHapArrangerRelativeTiming() const;
    virtual long           GetInitialChainSamplingInterval() const;
    virtual long           GetInitialNumberOfChains() const;
    virtual long           GetInitialNumberOfChainsToDiscard() const;
    virtual long           GetInitialNumberOfSamples() const;
    virtual long           GetNumberOfReplicates() const;
    virtual long           GetTemperatureInterval() const;

    /////////////////////////////////////////////////////////////
    virtual void            SetAdaptiveTemperatures(bool);
    virtual void            SetDoBayesianAnalysis(bool);
    virtual void            SetChainCount(long count);
    virtual void            SetChainTemperature(double temp, long chainId);
    virtual void            RescaleDefaultSizeArranger();
    virtual void            SetBayesianArrangerRelativeTiming(double);
    virtual void            SetDropArrangerRelativeTiming(double);
    virtual void            SetSizeArrangerRelativeTiming(double);
    virtual void            SetLocusArrangerRelativeTiming(double);
    virtual void            SetHaplotypeArrangerRelativeTiming(double);
    virtual void            SetProbHapArrangerRelativeTiming(double);
    virtual void            SetZilchArrangerRelativeTiming(double);
    virtual void            SetStairArrangerRelativeTiming(double);
    virtual void            SetEpochSizeArrangerRelativeTiming(double);
    virtual void            SetFinalChainSamplingInterval(long);
    virtual void            SetFinalNumberOfChains(long);
    virtual void            SetFinalNumberOfChainsToDiscard(long);
    virtual void            SetFinalNumberOfSamples(long);
    virtual void            SetInitialChainSamplingInterval(long);
    virtual void            SetInitialNumberOfChains(long);
    virtual void            SetInitialNumberOfChainsToDiscard(long);
    virtual void            SetInitialNumberOfSamples(long);
    virtual void            SetNumberOfReplicates(long);
    virtual void            SetTemperatureInterval(long);
  private:
    virtual void            WarnIfNoSamples();
};

double MakeNextChainTemperature(const vector<double> temperatures);

#endif  // UI_VARS_CHAINPARAMS_H

//____________________________________________________________________________________
