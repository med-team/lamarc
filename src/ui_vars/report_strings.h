// $Id: report_strings.h,v 1.8 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef REPORT_STRINGS_H
#define REPORT_STRINGS_H

#include <string>

class reportstr
{
  public:
    static const std::string    header;

    static const std::string    categoryHeader;
    static const std::string    categoryRate;
    static const std::string    categoryRelativeProb;

    static const std::string    baseFreqs;
    static const std::string    baseFreqSeparator;

    static const std::string    TTratio;

    static const std::string    GTRRates;
    static const std::string    GTRRateSeparator;
    static const std::string    GTRRatesFromA;
    static const std::string    GTRRatesFromC;
    static const std::string    GTRRatesFromG;

    static const std::string    numberOfBins;

    static const std::string    brownian;

    static const std::string    alpha;
    static const std::string    optimizeAlpha;

    static const std::string    relativeMutationRate;

    static const std::string    perBaseErrorRate;
};

#endif // REPORT_STRINGS_H

//____________________________________________________________________________________
