// $Id: ui_vars_userparams.h,v 1.25 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_VARS_USERPARAMS_H
#define UI_VARS_USERPARAMS_H

#include <string>

#include "local_build.h"

#include "constants.h"          // for verbosity_type
#include "ui_vars_component.h"

class UIVars;

using std::string;

// variables that can be changed by the user
class UIVarsUserParameters : public UIVarsComponent
{
  protected:
    string                  m_curveFilePrefix;
    string                  m_mapFilePrefix;
    string                  m_reclocFilePrefix;
    string                  m_traceFilePrefix;
    string                  m_newickTreeFilePrefix;
#ifdef LAMARC_QA_TREE_DUMP
    string                  m_argFilePrefix;
#endif // LAMARC_QA_TREE_DUMP
    string                  m_dataFileName;
    bool                    m_plotPost;
    time_t                  m_programStartTime;
    verbosity_type          m_progress;
    bool                    m_hasOldClockSeed;
    long                    m_oldClockSeed;
    long                    m_randomSeed;
    bool                    m_readSumFile;
    string                  m_resultsFileName;
    string                  m_treeSumInFileName;
    string                  m_treeSumOutFileName;
    bool                    m_useSystemClock;
    verbosity_type          m_verbosity;
    bool                    m_writeCurveFiles;
    bool                    m_writeSumFile;
    bool                    m_writeReclocFiles;
    bool                    m_writeTraceFiles;
    bool                    m_writeNewickTreeFiles;
#ifdef LAMARC_QA_TREE_DUMP
    bool                    m_writeArgFiles;
    bool                    m_writeManyArgs;
#endif // LAMARC_QA_TREE_DUMP
    string                  m_xmlOutFileName;
    string                  m_xmlReportFileName;
    string                  m_profilePrefix;

    UIVarsUserParameters();                             // undefined
    UIVarsUserParameters(const UIVarsUserParameters&);  // undefined

  public:
    // one might argue that the constructors should have
    // restricted access since only UIVars should
    // be creating these puppies.
    UIVarsUserParameters(UIVars*,string fileName);
    UIVarsUserParameters(UIVars*,const UIVarsUserParameters&);
    virtual ~UIVarsUserParameters();

    /////////////////////////////////////////////////////////////
    virtual string         GetCurveFilePrefix()    const {return m_curveFilePrefix;};
    virtual bool           GetHasOldClockSeed()    const {return m_hasOldClockSeed;};
    virtual string         GetMapFilePrefix()      const {return m_mapFilePrefix;};
    virtual string         GetReclocFilePrefix()   const {return m_reclocFilePrefix;};
    virtual string         GetTraceFilePrefix()    const {return m_traceFilePrefix;};
    virtual string       GetNewickTreeFilePrefix() const {return m_newickTreeFilePrefix;};
#ifdef LAMARC_QA_TREE_DUMP
    virtual string         GetArgFilePrefix()      const {return m_argFilePrefix;};
#endif // LAMARC_QA_TREE_DUMP
    virtual string         GetDataFileName()       const {return m_dataFileName;};
    virtual long           GetOldClockSeed()       const ;
    virtual bool           GetPlotPost()           const {return m_plotPost;};
    virtual string         GetProfilePrefix()      const {return m_profilePrefix;};
    virtual time_t         GetProgramStartTime()   const {return m_programStartTime;};
    virtual verbosity_type GetProgress()           const {return m_progress;};
    virtual long           GetRandomSeed()         const {return m_randomSeed;};
    virtual string         GetResultsFileName()    const {return m_resultsFileName;};
    virtual string         GetTreeSumInFileName()  const {return m_treeSumInFileName;};
    virtual string         GetTreeSumOutFileName() const {return m_treeSumOutFileName;};
    virtual bool           GetUseSystemClock()     const {return m_useSystemClock;};
    virtual verbosity_type GetVerbosity()          const {return m_verbosity;};
    virtual bool           GetReadSumFile()        const;
    virtual bool           GetWriteSumFile()       const;
    virtual bool           GetWriteCurveFiles()    const {return m_writeCurveFiles;};
    virtual bool           GetWriteReclocFiles()   const {return m_writeReclocFiles;};
    virtual bool           GetWriteTraceFiles()    const {return m_writeTraceFiles;};
    virtual bool           GetWriteNewickTreeFiles() const;
#ifdef LAMARC_QA_TREE_DUMP
    virtual bool           GetWriteArgFiles()      const {return m_writeArgFiles;};
    virtual bool           GetWriteManyArgs()      const {return m_writeManyArgs;};
#endif // LAMARC_QA_TREE_DUMP
    virtual string         GetXMLOutFileName()     const {return m_xmlOutFileName;};
    virtual string         GetXMLReportFileName()  const {return m_xmlReportFileName;};

    /////////////////////////////////////////////////////////////
    virtual void SetCurveFilePrefix(string x);
    virtual void SetMapFilePrefix(string x);
    virtual void SetReclocFilePrefix(string x);
    virtual void SetTraceFilePrefix(string x);
    virtual void SetNewickTreeFilePrefix(string x);
#ifdef LAMARC_QA_TREE_DUMP
    virtual void SetArgFilePrefix(string x);
#endif // LAMARC_QA_TREE_DUMP
    virtual void SetDataFileName(string x);
    virtual void SetPlotPost(bool x)              {m_plotPost = x;};
    virtual void SetProfilePrefix(string x);
    virtual void SetProgress(verbosity_type x)    {m_progress = x;};
    virtual void SetResultsFileName(string x);
    virtual void SetTreeSumInFileName(string x);
    virtual void SetTreeSumOutFileName(string x);
    virtual void SetVerbosity(verbosity_type x)   {m_verbosity = x;};
    virtual void SetWriteCurveFiles(bool x);
    virtual void SetWriteReclocFiles(bool x);
    virtual void SetWriteTraceFiles(bool x);
    virtual void SetWriteNewickTreeFiles(bool x);
#ifdef LAMARC_QA_TREE_DUMP
    virtual void SetWriteArgFiles(bool x);
    virtual void SetWriteManyArgs(bool x);
#endif // LAMARC_QA_TREE_DUMP
    virtual void SetReadSumFile(bool x);
    virtual void SetWriteSumFile(bool x);
    virtual void SetXMLOutFileName(string x);
    virtual void SetXMLReportFileName(string x);
    // these Setters have more "interesting" logic. See individual
    // method implementations for details
    virtual void SetProgramStartTime();
    virtual void SetOldClockSeed(long x);
    virtual void SetRandomSeed(long x);
    virtual void SetUseOldClockSeed(bool x);
    virtual void SetUseSystemClock(bool x);

  private:
    void CheckReadSumFile();
    void CheckWriteSumFile();
    bool CheckBothSumFiles();
    void CheckNewickTreeFile();
#ifdef LAMARC_QA_TREE_DUMP
    void CheckArgFile();
#endif // LAMARC_QA_TREE_DUMP
    void CheckCurveFiles();
    void CheckMapFiles();
    void CheckReclocFiles();
    void CheckTraceFiles();
    void CheckPrefix(string x);
};

#endif  // UI_VARS_USERPARAMS_H

//____________________________________________________________________________________
