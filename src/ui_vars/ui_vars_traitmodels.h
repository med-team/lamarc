// $Id: ui_vars_traitmodels.h,v 1.12 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_VARS_TRAITMODELS_H
#define UI_VARS_TRAITMODELS_H

#include "datatype.h"           // for data_type
#include "phenotypes.h"
#include "rangex.h"
#include "ui_vars_component.h"

class UIRegId;

enum mloc_type {mloc_data, mloc_mapjump, mloc_mapfloat, mloc_partition};

string ToString(mloc_type);
string ToXMLString(mloc_type);
mloc_type ProduceMlocTypeOrBarf(const string& in);

class UIVarsSingleTraitModel
{
  private:
    UIVarsSingleTraitModel();    // undefined
    long m_region;
    long m_locus;
    data_type m_datatype;
    mloc_type m_type;
    rangeset m_range;
    rangepair m_fullrange;
    string m_name;
    Phenotypes m_phenotypes;
    long m_multihapnum;

  public:
    UIVarsSingleTraitModel(UIRegId regionId, string name, rangeset mrange,
                           rangepair fullrange, const Locus* locus,
                           long multihapnum);
    virtual ~UIVarsSingleTraitModel();
    //Setters
    void SetAnalysisType(mloc_type type);
    void SetRange(rangeset range);
    void AddPhenotype(StringVec1d& alleles, string name, double penetrance);

    //Getters
    mloc_type GetAnalysisType() const {return m_type;};
    rangeset  GetRange() const {return m_range;};
    rangepair GetFullRange() const {return m_fullrange;};
    string    GetName()  const {return m_name;};
    Phenotypes GetPhenotypes() const {return m_phenotypes;};
    long      GetMultiHapNum() const {return m_multihapnum;};
};

class UIVarsTraitModels : public UIVarsComponent
{
  private:
    UIVarsTraitModels();  // undefined
    UIVarsTraitModels(const UIVarsTraitModels&); // undefined
    std::map<UIRegId, UIVarsSingleTraitModel>   m_individualModels;

  public:
    UIVarsTraitModels(UIVars*);
    UIVarsTraitModels(UIVars*, const UIVarsTraitModels&);
    virtual ~UIVarsTraitModels();

    void AddTraitModel(UIRegId regID);

    //Setters
    void SetAnalysisType(UIRegId regID, mloc_type type);
    void AddRange(UIRegId regID, rangepair addpart);
    void RemoveRange(UIRegId regID, rangepair removepart);
    void SetRangeToPoint(UIRegId regID, long site);
    void AddPhenotype(UIRegId regID, StringVec1d& alleles, string name, double penetrance);
    //Note:  AddPhenotype is currently only called from the XML.

    //Getters
    long GetNumMovableLoci() const;
    mloc_type GetAnalysisType(UIRegId regID) const;
    rangeset GetRange(UIRegId regID) const;
    long GetInitialMapPosition(UIRegId regID) const;
    string   GetName(UIRegId regID) const;
    Phenotypes GetPhenotypes(UIRegId regID) const;
    vector<UIRegId> GetRegIDs() const;
    bool AnyJumpingAnalyses() const;
    bool AnyMappingAnalyses() const;
    void SetRange(UIRegId regID, rangeset rset);

  private:
    bool OneSite(rangeset rset);
};

#endif  // UI_VARS_TRAITMODELS_H

//____________________________________________________________________________________
