// $Id: ui_vars_prior.cpp,v 1.17 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING

#include "ui_vars_prior.h"
#include "ui_vars_forces.h"
#include "ui_vars.h"
#include "defaults.h"
#include "errhandling.h"
#include "stringx.h"

//------------------------------------------------------------------------------------

UIVarsPrior::UIVarsPrior(
    UIInterface * ui,
    force_type force,
    priortype type,
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    long rate,
#endif
    double lowerbound,
    double upperbound)
    : m_ui(ui),
      m_forcetype(force),
      m_priortype(type),
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
      m_relativeSampling(rate),
#endif
      m_lowerbound(lowerbound),
      m_upperbound(upperbound)
{
}

UIVarsPrior::UIVarsPrior(force_type shouldBeGamma)
    : m_ui(NULL),
      m_forcetype(shouldBeGamma),
      m_priortype(LINEAR),
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
      m_relativeSampling(1),
#endif
      m_lowerbound(0.0),
      m_upperbound(0.0)
{
    assert (shouldBeGamma == force_REGION_GAMMA);
}

UIVarsPrior::~UIVarsPrior()
{
}

void UIVarsPrior::SetPriorType(priortype type)
{
    m_priortype = type;
}

void UIVarsPrior::SetLowerBound(double bound)
{
    double defbound = 0;
    switch(m_forcetype)
    {
        case force_COAL:
            defbound = defaults::minboundTheta;
            break;
        case force_MIG:
            defbound = defaults::minboundMig;
            break;
        case force_DISEASE:
            defbound = defaults::minboundDisease;
            break;
        case force_REC:
            defbound = defaults::minboundRec;
            break;
        case force_EXPGROWSTICK:
        case force_GROW:
            defbound = defaults::minboundGrowth;
            break;
        case force_DIVMIG:
            defbound = defaults::minboundDivMig;
            break;
        case force_DIVERGENCE:
            defbound = defaults::minboundEpoch;
            break;
        case force_LOGSELECTSTICK:
        case force_LOGISTICSELECTION:
            defbound = defaults::minboundLSelect;
            break;
        case force_REGION_GAMMA:
            throw implementation_error("Method UIVarsPrior::SetLowerBound() was called for force_REGION_GAMMA; this is illegal.");
            break;
        default:
            assert(false);              //Uncaught force type.
    }
    if (bound < defbound)
    {
        m_lowerbound = defbound;
        string msg = "Warning:  the minimum lower bound for "
            + ToString(m_forcetype) + " priors is " + ToString(defbound)
            + ":  setting the lower bound there.";
        m_ui->AddWarning(msg);
        return;
    }
    if (bound > m_upperbound)
    {
        throw data_error("The lower bound of the prior must be less than the upper bound of the prior.");
    }
    if (bound == m_upperbound)
    {
        throw data_error("The lower bound of the prior must be less than the upper bound of the prior.\n"
                         "If you wish to hold this parameter constant, go to the constraints menu and\n"
                         "set it constant there.");
    }
    m_lowerbound = bound;
}

#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
void UIVarsPrior::SetRelativeSampling(long rate)
{
    if (rate < 1)
    {
        throw data_error("Relative sampling rates must be integers greater than or equal to 1.");
    }
    m_relativeSampling = rate;
}
#endif

void UIVarsPrior::SetUpperBound(double bound)
{
    double defbound = 0;
    switch(m_forcetype)
    {
        case force_COAL:
            defbound  = defaults::maxboundTheta;
            break;
        case force_MIG:
            defbound = defaults::maxboundMig;
            break;
        case force_DISEASE:
            defbound = defaults::maxboundDisease;
            break;
        case force_REC:
            defbound = defaults::maxboundRec;
            break;
        case force_DIVMIG:
            defbound = defaults::maxboundDivMig;
            break;
        case force_DIVERGENCE:
            defbound = defaults::maxboundEpoch;
            break;
        case force_EXPGROWSTICK:
        case force_GROW:
            defbound  = defaults::maxboundGrowth;
            break;
        case force_LOGSELECTSTICK:
        case force_LOGISTICSELECTION:
            defbound = defaults::maxboundLSelect;
            break;
        case force_REGION_GAMMA:
            throw implementation_error("Method UIVarsPrior::SetLowerBound() was called for force_REGION_GAMMA; this is illegal.");
            break;
        default:
            assert(false);              //Uncaught force type.
    }
    if (bound > defbound)
    {
        m_upperbound = defbound;
        string msg = "Warning:  the maximum upper bound for "
            + ToString(m_forcetype) + " priors is " + ToString(defbound)
            + ":  setting the upper bound there.";
        m_ui->AddWarning(msg);
        return;
    }
    if (bound < m_lowerbound)
    {
        throw data_error("The upper bound of the prior must be greater than the upper bound of the prior.");
    }
    if (bound == m_lowerbound)
    {
        throw data_error("The upper bound of the prior must be greater than the upper bound of the prior.\n"
                         "If you wish to hold this parameter constant, go to the constraints menu and\n"
                         "set it constant there.");
    }
    m_upperbound = bound;
}

double UIVarsPrior::GetBinwidth() const
{
    //The basic intent here is to have the binwidth in the fourth significant
    // digit.  For logarithmic priors, this is easy.
    if (m_priortype == LOGARITHMIC)
    {
        return 0.001;
    }

    //For linear priors, take the difference of the bounds and take the fourth
    // significant digit of that number.
    int exponent = static_cast<int>(log10(m_upperbound - m_lowerbound));
    return pow(10.0,exponent-4);
};

string UIVarsPrior::GetSummaryDescription() const
{
    string desc = "(" + ToString(m_priortype) + ") " + Pretty(m_lowerbound,6) + " - " + Pretty(m_upperbound,6);
    return desc;
}

//____________________________________________________________________________________
