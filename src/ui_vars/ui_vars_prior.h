// $Id: ui_vars_prior.h,v 1.10 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef UI_VARS_PRIOR_H
#define UI_VARS_PRIOR_H

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
#include "constants.h"                  // for priortype

class UIInterface;
class UIVarsForces;

//This class is nothing more than a struct right now, but I made it a class
// anyway in case we get more complicated priors in the future.  Also, the
// binwidth member variable may at some point be calculated based on the
// upper and lower bounds instead of being set directly.

class UIVarsPrior
{
  private:
    UIVarsPrior();    // undefined
    UIInterface *   m_ui;
    force_type m_forcetype; //Used for boundary checking.
    priortype  m_priortype;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    long       m_relativeSampling;
#endif
    double     m_lowerbound;
    double     m_upperbound;

  public:
    UIVarsPrior(UIInterface * ui,
                force_type force,
                priortype type,
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
                long rate,
#endif
                double lowerbound,
                double upperbound);
    UIVarsPrior(force_type shouldBeGamma);

    //Use the default copy constructor.
    virtual ~UIVarsPrior();

    virtual priortype GetPriorType()  const {return m_priortype;};
    virtual double    GetLowerBound() const {return m_lowerbound;};
    virtual double    GetUpperBound() const {return m_upperbound;};
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual long      GetRelativeSampling() const {return m_relativeSampling;};
#endif
    virtual double    GetBinwidth() const;

    virtual void SetPriorType(priortype type);
    virtual void SetLowerBound(double bound);
    virtual void SetUpperBound(double bound);
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual void SetRelativeSampling(long rate);
#endif
    //  virtual void SetBinwidth(double bin);
    //  The binwidth is a function of all the other settings, and cannot (now)
    //   be set on its own.  Perhaps this should be revisited in the future.

    virtual string GetSummaryDescription() const;
};

#endif  // UI_VARS_PRIOR_H

//____________________________________________________________________________________
