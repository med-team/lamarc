// $Id: ui_vars_forces.h,v 1.57 2018/01/03 21:33:05 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// This file contains two classes (one with subclasses) which are derived from UIVarsComponent.
// UIVarsSingleForce represents one force; it has a subtype UIVars2DForce for forces whose
// parameters form a 2D table (migration, disease, divmigration) and then concrete subclasses
// for each type of force.  UIVarsForces is analogous to ForceSummary; it contains stuff about all forces.

#ifndef UI_VARS_FORCES_H
#define UI_VARS_FORCES_H

#include <deque>
#include <map>
#include <vector>

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING

#include "constants.h"                  // for method_type
#include "defaults.h"                   // for force_type
#include "ui_strings.h"
#include "ui_vars_component.h"
#include "ui_vars_prior.h"
#include "vectorx.h"
#include "stringx.h"
#include "paramstat.h"

using std::deque;
using std::map;
using std::vector;

class UIVarsForces;

// virtual class with common methods across all force types
class UIVarsSingleForce : public UIVarsComponent
{
  private:
    // The number of parameters for this force is also the
    // number of start values, method types, and profiles
    //
    // The correct number of parameters for each force is:
    //      theta/coal       number of cross partitions
    //      disease         (number of disease states)  ^2
    //      migration       (number of populations)     ^2
    //      growth           number of cross partitions
    //      rec. rate        1
    //      gamma over regions "force"  1
    const long  m_numParameters;

    const double      m_defaultValue;   // parameter value if none set
    const method_type m_defaultMethodType;

  protected: //The gamma force needs to be able to change this.
    bool              m_canSetOnOff;    // can this force be turned on/off

  private:
    bool              m_onOff;          // is this force active?
    long              m_maxEvents; //maximum number of Events for this force
    proftype          m_profileType;// profiles of parameters for a single
    // force must have the same proftype

    // per-parameter data
    deque<bool>                     m_doProfile;
    DoubleVec1d                     m_userSuppliedStartValues;
    vector<method_type>             m_startValueMethodTypes;
    vector<ParamGroup>              m_groups;
    DoubleVec1d                     m_truevalues;
    vector<ParamStatus>             m_pstatusValues;

    // Bayesian information
    UIVarsPrior                     m_defaultPrior;
    vector<UIVarsPrior>             m_priors;
    deque<bool>                     m_useDefaultPrior;

    // Never want to create a UIVarsSingleForce without arguments
    UIVarsSingleForce();                            // undefined
    UIVarsSingleForce(const UIVarsSingleForce&);    // undefined
    UIVarsSingleForce& operator=(const UIVarsSingleForce&); // undefined

  protected:
    // member protected (rather than private) because sub-classes
    // have need to invalidate various parameters
    const force_type         m_ftype;

    // wanted this in private area but haven't figured out
    // the best way to get it there
    map<method_type,DoubleVec1d>    m_calculatedStartValues;

    void checkIndexValue(long index) const;
    virtual void AssertOnIllegalStartMethod(method_type);
    virtual string GetParamName(long pindex);

  public:
    UIVarsSingleForce(
        UIVars *,
        force_type ftype,
        long nparams,
        double defaultVal,
        method_type defaultMethod,
        bool canBeSetUnset,
        bool isOn,
        long eventMax,
        UIVarsPrior defaultPrior);
    UIVarsSingleForce(UIVars *, const UIVarsSingleForce&);
    virtual ~UIVarsSingleForce()        {};

    // pure virtual -- defined by subclasses
    virtual long        GetNumParameters()  const { return m_numParameters;};
    virtual bool        GetCanSetOnOff()    const { return m_canSetOnOff;};
    virtual bool        GetOnOff()          const { return m_onOff;};
    virtual bool        GetLegal()          const;
    virtual long        GetMaxEvents()      const { return m_maxEvents;};
    virtual force_type  GetPhase2Type(force_type orig) const { return orig; };

    // each parameter can have profiling turned ON or OFF (boolean)
    // but the type of profiling (fixed, percentile) is set at
    // the per-force level
    virtual bool            GetDoProfile(long index) const;
    virtual proftype        GetProfileType()  const;
    virtual proftype        GetProfileType(long index)  const;
    virtual string          GetProfileTypeSummaryDescription() const;
    virtual string          GetPriorTypeSummaryDescription() const;
    virtual paramlistcondition  GetParamListCondition() const;
    virtual void            SetDoProfile(bool doIt, long index);
    virtual void            SetDoProfile(bool doIt);
    virtual void            SetProfileType(proftype x);

    virtual ParamStatus     GetParamstatus(long index) const;
    virtual void   SetParamstatus(const ParamStatus& mystatus, long index);

    virtual double          GetStartValue(long index)   const;
    virtual DoubleVec1d     GetStartValues()  const;
    virtual double          GetMinStartValue(long pindex) const;
    virtual double          GetMaxStartValue(long pindex) const;
    virtual void            SetUserStartValue(double startVal, long index);
    virtual void            SetUserStartValues(double startVal);

    virtual method_type     GetStartMethod(long index)  const;

    virtual double          GetTrueValue(long index) const;
    virtual void            SetTrueValue(double trueval, long index);

    //Getters and setters for Bayesian information
    virtual const UIVarsPrior& GetPrior(long pindex) const;
    virtual priortype GetPriorType(long pindex) const;
    virtual double GetLowerBound(long pindex)   const;
    virtual double GetUpperBound(long pindex)   const;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual long   GetRelativeSampling(long pindex) const;
#endif

    virtual void SetPriorType(priortype ptype, long pindex);
    virtual void SetUngroupedPriorType(priortype ptype, long pindex);
    virtual void SetLowerBound(double bound, long pindex);
    virtual void SetUngroupedLowerBound(double bound, long pindex);
    virtual void SetUpperBound(double bound, long pindex);
    virtual void SetUngroupedUpperBound(double bound, long pindex);
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual void SetRelativeSampling(long rate, long pindex);
    virtual void SetUngroupedRelativeSampling(long rate, long pindex);
#endif

    virtual bool GetUseDefaultPrior(long pindex) const;
    virtual const UIVarsPrior& GetDefaultPrior() const;
    virtual priortype GetDefaultPriorType() const;
    virtual double GetDefaultLowerBound()   const;
    virtual double GetDefaultUpperBound()   const;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual long GetDefaultRelativeSampling() const;
#endif

    virtual void SetUseDefaultPrior(bool use, long pindex);
    virtual void SetUseAllDefaultPriors();
    virtual void SetDefaultPriorType(priortype ptype);
    virtual void SetDefaultLowerBound(double bound);
    virtual void SetDefaultUpperBound(double bound);
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual void SetDefaultRelativeSampling(long rate);
#endif

    virtual void SetStartMethods(method_type method);
    // override this method to allow only legal values of method_type
    virtual void SetStartMethod(method_type method, long index);

    virtual void        SetMaxEvents(long maxEvents);

    virtual void        SetOnOff(bool x);

    virtual bool        GetParamValid(long id) const;
    virtual bool        GetParamUnique(long id) const;

    virtual bool        AreZeroesValid() const = 0;
    virtual bool        SomeVariableParams() const;

    // Public interface with the groups:
    virtual void AddGroup(ParamStatus mystatus, LongVec1d indices);
    virtual void AddParamToGroup(long pindex, long gindex);
    virtual void AddParamToNewGroup(long pindex);
    virtual ParamStatus GetGroupParamstatus(long gindex) const;
    virtual LongVec1d   GetGroupParamList  (long gindex) const;
    virtual std::vector<ParamGroup> GetGroups() const {return m_groups;};
    virtual bool AreGroupsValid() const;
    virtual void FixGroups();
    virtual void SetGroupParamstatus(ParamStatus pstat, long gindex);
    virtual long ParamInGroup(long pindex) const;
    virtual long GetNumGroups() const {return m_groups.size();};
    virtual void RemoveParamIfInAGroup(long pindex);

  private:
    //Private interface with the groups:
    virtual void SetDoProfilesForGroup(bool doIt, long gindex);
    virtual void checkGIndexValue(long gindex) const;

    virtual double GetUngroupedStartValue(long index) const;
};

class UIVars2DForce : public UIVarsSingleForce
{
  private:
    UIVars2DForce();                                  // undefined
    UIVars2DForce(const UIVars2DForce&);              // undefined
    void UpdateReachList(long testRome, std::list<long>& unreached,
                         std::list<long>& reached) const;
    long PickANewRome(long oldRome, std::list<long> unreached,
                      std::list<long> reached) const;
    bool CanReach(long partfrom, long partto) const;
    UIVars2DForce& operator=(const UIVars2DForce&); // undefined
  protected:
    virtual void AssertOnIllegalStartMethod(method_type);
    long m_npartitions;
  public:
    UIVars2DForce(UIVars *,
                  force_type ftype,
                  long npartitions,
                  double defaultVal,
                  method_type defaultMethod,
                  bool canBeSetUnset,
                  bool isOn,
                  long eventMax,
                  UIVarsPrior defaultPrior);
    UIVars2DForce(UIVars *,const UIVars2DForce&);
    virtual ~UIVars2DForce()          {};
    virtual bool        GetParamValid(long id) const;
    virtual bool  AreZeroesValid() const;
};

class UIVarsCoalForce : public UIVarsSingleForce
{
  private:
    UIVarsCoalForce();                                  // undefined
    UIVarsCoalForce& operator=(const UIVarsCoalForce&); // undefined
    UIVarsCoalForce(const UIVarsCoalForce&);            // undefined
  protected:
    virtual void AssertOnIllegalStartMethod(method_type);
  public:
    UIVarsCoalForce(UIVars *,long numCrossPartitions);
    UIVarsCoalForce(UIVars *,const UIVarsCoalForce&);
    virtual ~UIVarsCoalForce()          {};
    virtual bool        AreZeroesValid() const;
    void FillCalculatedStartValues();

};

class UIVarsMigForce : public UIVars2DForce
{
  private:
    UIVarsMigForce();                                   // undefined
    UIVarsMigForce& operator=(const UIVarsMigForce&);   // undefined
    UIVarsMigForce(const UIVarsMigForce&);              // undefined
  public:
    UIVarsMigForce(UIVars*, long numPopulations,bool onOrOff);
    UIVarsMigForce(UIVars*, const UIVarsMigForce&);
    virtual ~UIVarsMigForce()          {};
    void FillCalculatedStartValues();
};

class UIVarsDivMigForce : public UIVars2DForce
{
  private:
    UIVarsDivMigForce();                                   // undefined
    UIVarsDivMigForce& operator=(const UIVarsMigForce&);   // undefined
    UIVarsDivMigForce(const UIVarsMigForce&);              // undefined
  protected:
    virtual void AssertOnIllegalStartMethod(method_type);
  public:
    UIVarsDivMigForce(UIVars*, long numPopulations,bool onOrOff);
    UIVarsDivMigForce(UIVars*, const UIVarsDivMigForce&);
    virtual ~UIVarsDivMigForce()          {};
    virtual bool  AreZeroesValid() const { return true; }; // zero always valid here
};

class UIVarsDivergenceForce : public UIVarsSingleForce
{
  private:
    UIVarsDivergenceForce();                                  // undefined
    UIVarsDivergenceForce& operator=(const UIVarsDivergenceForce&); // undefined
    UIVarsDivergenceForce(const UIVarsDivergenceForce&);            // undefined
    // in the following two vectors, each entry is an epoch
    std::vector<std::vector<std::string> > newpops;
    std::vector<std::string> ancestors;
  protected:
    virtual void AssertOnIllegalStartMethod(method_type);
  public:
    UIVarsDivergenceForce(UIVars *,long numDivPopulations);
    UIVarsDivergenceForce(UIVars *,const UIVarsDivergenceForce&);
    virtual ~UIVarsDivergenceForce()          {};
    virtual bool        AreZeroesValid() const;
    void AddNewPops(const std::vector<std::string>& newp);
    void AddAncestor(const std::string& anc);
    std::vector<std::vector<std::string> > GetNewPops() const;
    std::vector<std::string> GetAncestors() const;
    std::string GetAncestor(long index) const;

};

class UIVarsDiseaseForce : public UIVars2DForce
{
  private:
    long                location;
    //      string              diseaseName;
    // LS Note:  If we allow more than one disease/trait at once, and we want
    //  to model this using one disease force, we'll presumably need a
    //  vector of strings for the names instead of just the name.  Or we can
    //  keep doing what we're doing now, which is to use the disease state
    //  to refer to it ('rate from healthy to diseased', etc.).

    UIVarsDiseaseForce();                                       // undefined
    UIVarsDiseaseForce(const UIVarsDiseaseForce&);              // undefined
    UIVarsDiseaseForce& operator=(const UIVarsDiseaseForce&);   // undefined
  public:
    UIVarsDiseaseForce(UIVars*, long numDiseaseStates, bool canTurnOnOff);
    UIVarsDiseaseForce(UIVars*, const UIVarsDiseaseForce&);
    virtual ~UIVarsDiseaseForce()   {};

    virtual long        GetLocation()       const {return location;};

    virtual void    SetLocation(long x)             { location = x; };
};

class UIVarsRecForce : public UIVarsSingleForce
{
  private:
    UIVarsRecForce();                                   // undefined
    UIVarsRecForce(const UIVarsRecForce&);              // undefined
    UIVarsRecForce& operator=(const UIVarsRecForce&);   // undefined
  public:
    UIVarsRecForce(UIVars*, bool canTurnOn);
    UIVarsRecForce(UIVars*,const UIVarsRecForce&);
    virtual ~UIVarsRecForce()          {};
    virtual void        SetOnOff(bool onOffVal);
    virtual bool        GetOnOff()       const;
    virtual bool        AreZeroesValid() const;
};

class UIVarsRegionGammaForce : public UIVarsSingleForce
{
  private:
    UIVarsRegionGammaForce();
    UIVarsRegionGammaForce(const UIVarsRegionGammaForce&);
    UIVarsRegionGammaForce& operator=(const UIVarsRegionGammaForce&);
  public:
    UIVarsRegionGammaForce(UIVars*);
    UIVarsRegionGammaForce(UIVars*,const UIVarsRegionGammaForce&);
    virtual ~UIVarsRegionGammaForce()          {};
    virtual bool GetOnOff() const;
    virtual void SetOnOff(bool);
    virtual bool AreZeroesValid() const;
};

class UIVarsGrowForce : public UIVarsSingleForce
{
  private:
    UIVarsGrowForce();                                  // undefined
    UIVarsGrowForce(const UIVarsGrowForce&);            // undefined
    UIVarsGrowForce& operator=(const UIVarsGrowForce&); // undefined

    growth_type    growthType;

  public:
    UIVarsGrowForce(UIVars*, long numCrossPartitions);
    UIVarsGrowForce(UIVars*, const UIVarsGrowForce&);
    virtual ~UIVarsGrowForce()          {};
    virtual bool        AreZeroesValid() const;
    virtual growth_type GetGrowthType() const {return growthType;};
    virtual void        SetGrowthType(growth_type g) {growthType=g;};
    virtual growth_scheme GetGrowthScheme() const;
    virtual void        SetGrowthScheme(growth_scheme g);
    virtual force_type  GetPhase2Type(force_type f) const;
    virtual void        SetOnOff(bool);
};

class UIVarsLogisticSelectionForce : public UIVarsSingleForce
{
  private:
    UIVarsLogisticSelectionForce();
    UIVarsLogisticSelectionForce(const UIVarsLogisticSelectionForce&);
    UIVarsLogisticSelectionForce& operator=(const UIVarsLogisticSelectionForce&);

    // double m_observedMajorAlleleFrequency; // ??
    selection_type selectionType;

  public:
    UIVarsLogisticSelectionForce(UIVars*, long numCrossPartitions);
    UIVarsLogisticSelectionForce(UIVars*, const UIVarsLogisticSelectionForce&);
    virtual ~UIVarsLogisticSelectionForce()          {};
    virtual bool        AreZeroesValid() const;
    virtual selection_type GetSelectionType() const {return selectionType;};
    virtual void        SetSelectionType(selection_type s) {selectionType=s;};
    virtual force_type  GetPhase2Type(force_type f) const;
};

//------------------------------------------------------------------------------------

class UIVarsForces : public UIVarsComponent
{
  private:
    UIVarsForces();                             // undefined
    UIVarsForces(const UIVarsForces&);         // undefined

    class IsInactive : public std::unary_function<force_type,bool>
    {
      private:
        const UIVarsForces& m_vars_forces;
      public:
        IsInactive(const UIVarsForces&);
        ~IsInactive();
        bool operator()(force_type f) const;
    };
    class IsIllegal : public std::unary_function<force_type,bool>
    {
      private:
        const UIVarsForces& m_vars_forces;
      public:
        IsIllegal(const UIVarsForces&);
        ~IsIllegal();
        bool operator()(force_type f) const;
    };

    UIVarsCoalForce         coalForce;
    UIVarsDiseaseForce      diseaseForce;
    UIVarsGrowForce         growForce;
    UIVarsMigForce          migForce;
    UIVarsDivMigForce       divMigForce;
    UIVarsDivergenceForce   divForce;
    UIVarsRecForce          recForce;
    UIVarsRegionGammaForce  regionGammaForce;
    UIVarsLogisticSelectionForce logisticSelectionForce;
  protected:
    const UIVarsSingleForce &     getLegalForce(force_type) const;
    UIVarsSingleForce &     getLegalForce(force_type);
    const UIVarsSingleForce &     getForceRegardlessOfLegality(force_type) const;
  public:
    // one might argue that the constructors should have
    // restricted access since only UIVars should
    // be creating these puppies.
    UIVarsForces(UIVars *,long nCrossPartitions, long nMigPopulations, long nDivPopulations, long nDiseaseStates, bool canMeasureRecombination);
    UIVarsForces(UIVars *,const UIVarsForces&);
    virtual ~UIVarsForces();

    void FillCalculatedStartValues();

    // All public non-constructors should be legal to perform
    // from the menu. If not, put 'em in the protected section
    LongVec1d             GetForceSizes() const;
    vector<force_type>    GetActiveForces() const;
    // the following guarantees a unique tag for any legal Phase 2
    // force, eg Stick variants of existing forces.  Mary
    vector<force_type>    GetPhase2ActiveForces() const;
    vector<force_type>    GetLegalForces() const;
    vector<force_type>    GetPossibleForces() const;
    virtual long          GetNumGroups(force_type force) const;
    virtual long          ParamInGroup(force_type ftype, long pindex) const;

    bool            GetForceCanTurnOnOff(force_type force) const;
    bool            GetForceLegal(force_type force) const;
    bool            GetForceZeroesValidity(force_type force) const;
    void            FixGroups(force_type force);

    long            GetDiseaseLocation  () const;
    bool            GetDoProfile        (force_type force, long id) const;
    growth_type     GetGrowthType       () const;
    growth_scheme   GetGrowthScheme     () const;
    long            GetNumParameters    (force_type force) const;
    bool            GetParamValid       (force_type force, long id) const;
    bool            GetParamUnique      (force_type force, long id) const;
    bool            GetForceOnOff       (force_type force) const;
    long            GetMaxEvents        (force_type force) const;
    proftype        GetProfileType      (force_type force) const;
    proftype        GetProfileType      (force_type force, long id) const;
    method_type     GetStartMethod      (force_type force, long id) const;
    ParamStatus     GetParamstatus      (force_type force, long id) const;
    ParamStatus     GetGroupParamstatus (force_type force, long id) const;
    LongVec1d       GetGroupParamList   (force_type force, long id) const;
    // MFIX -- these functions need to be written; only stubs exist!
    std::vector<ParamGroup> GetIdentGroups   (force_type force) const;
    std::vector<ParamGroup> GetMultGroups   (force_type force) const;
    double          GetStartValue       (force_type force, long id) const;
    DoubleVec1d     GetStartValues      (force_type)  const;
    double          GetTrueValue        (force_type force, long id) const;
    force_type      GetPhase2Type       (force_type force) const;
    selection_type  GetSelectionType    () const;

    //Getters/setters for Bayesian information
    virtual bool GetUseDefaultPrior(force_type force, long pindex) const;
    virtual const UIVarsPrior& GetDefaultPrior (force_type force) const;
    virtual const UIVarsPrior& GetPrior     (force_type force, long pindex) const;
    virtual priortype GetPriorType (force_type force, long pindex) const;
    virtual double    GetLowerBound(force_type force, long pindex) const;
    virtual double    GetUpperBound(force_type force, long pindex) const;
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual long      GetRelativeSampling(force_type force, long pindex) const;
#endif

    virtual void SetUseDefaultPrior(bool use,force_type force, long pindex);
    virtual void SetUseDefaultPriorsForForce(force_type force);
    virtual void SetPriorType (priortype ptype, force_type force, long pindex);
    virtual void SetLowerBound(double bound, force_type force, long pindex);
    virtual void SetUpperBound(double bound, force_type force, long pindex);
#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    virtual void SetRelativeSampling(long rate, force_type force, long pindex);
#endif
    virtual string GetPriorTypeSummaryDescription(force_type force) const;
    virtual string GetPriorTypeSummaryDescription(force_type force, long pindex, bool sayDefault=true) const;

    // allows display such as "(some) fixed" for menu summary
    string  GetProfileTypeSummaryDescription(force_type force) const;

    void SetForceOnOff  (bool onOff,        force_type forceId);
    void SetMaxEvents   (long maxEvents,    force_type forceId);
    //
    void SetProfileType (proftype ptype);
    void SetProfileType (proftype ptype,    force_type forceId);
    //
    void SetDoProfile   (bool doProfile);
    void SetDoProfile   (bool doProfile,    force_type forceId);
    void SetDoProfile   (bool doProfile,    force_type forceId, long id);

    void SetUserStartValue  (double startValue, force_type forceId, long id);
    void SetStartMethod (method_type startMethod, force_type forceId, long id);
    void SetTrueValue  (double startValue, force_type forceId, long id);
    void SetParamstatus (const ParamStatus& mystatus, force_type force, long id);
    void SetGroupParamstatus (ParamStatus pstat, force_type force, long id);
    void AddGroup (LongVec1d params, force_type force, long id);
    void RemoveParamFromGroup (force_type force, long id);
    void AddParamToGroup (force_type force, long pindex, long gindex);
    void AddParamToNewGroup (force_type force, long pindex);

    void SetAllThetaStartValues             (double startValue);
    void SetAllThetaStartValuesFST          ();
    void SetAllThetaStartValuesWatterson    ();
    void SetThetaStartValue                 (double startValue, long id);

    void SetAllMigrationStartValues     (double startValue);
    void SetAllMigrationStartValuesFST  ();
    void SetMigrationStartValue         (double startValue, long id);

    void SetDivergenceEpochStartTime  (double startValue, long int id);

    void SetAllDivMigrationStartValues     (double startValue);
    void SetDivMigrationStartValue         (double startValue, long id);

    void SetAllDiseaseStartValues   (double startValue);
    void SetDiseaseStartValue       (double startValue, long id);
    void SetDiseaseLocation         (long loc);

    void SetAllGrowthStartValues            (double startValue);
    void SetGrowthStartValue                (double startValue, long id);
    void SetGrowthType                      (growth_type gType);
    void SetGrowthScheme                    (growth_scheme gScheme);

    void SetRecombinationStartValue         (double startValue);

    void SetRegionGammaStartValue         (double startValue);

    void SetLogisticSelectionCoefficientStartValue (double startValue);
    void SetSelectionType                    (selection_type sType);

    bool AreZeroesValid(force_type forceId);
    bool SomeVariableParams() const;

    // pass-through functions for Divergence force; JNOTE will be removed when this is
    // handled properly by SetGet machinery
    void AddNewPops(const std::vector<std::string>& newpops) {divForce.AddNewPops(newpops); };
    void AddAncestor(const std::string& ancestor) {divForce.AddAncestor(ancestor); };
    std::vector<std::vector<std::string> > GetNewPops() const { return divForce.GetNewPops(); };
    std::vector<std::string> GetAncestors() const { return divForce.GetAncestors(); };

    const UIVarsRegionGammaForce& GetUIVarsRegionGammaForce() const
    { return regionGammaForce; };

    std::string GetEpochAncestorName(long id) const;
    std::string GetEpochDescendentNames(long id) const;

};

#endif  // UI_VARS_FORCES_H

//____________________________________________________________________________________
