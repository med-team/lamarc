// $Id: gc_strings_map.h,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_MAP_H
#define GC_STRINGS_MAP_H

#include "wx/string.h"

class gcstr_map
{
  public:

#if 0
    static const wxString ATTR_ADJACENCY;
    static const wxString ATTR_DATATYPE;
    static const wxString ATTR_FORMAT;
    static const wxString ATTR_SEQUENCEALIGNMENT;
    static const wxString ATTR_TYPE;

    static const wxString ATTR_VAL_BYLIST;
    static const wxString ATTR_VAL_BYNAME;
    static const wxString ATTR_VAL_LINKED;
    static const wxString ATTR_VAL_SINGLE;
    static const wxString ATTR_VAL_UNLINKED;

    static const wxString TAG_ADDCOMMENT;
    static const wxString TAG_ALLELE;
    static const wxString TAG_ALLELES;
    static const wxString TAG_BLOCK;
    static const wxString TAG_CONVERTER_CMD;
    static const wxString TAG_EFFECTIVE_POPSIZE;
    static const wxString TAG_FIRST_POSITION_SCANNED;
    static const wxString TAG_GENO_RESOLUTIONS;
    static const wxString TAG_HAPLOTYPES;
    static const wxString TAG_INDIVIDUAL_MATCHING;
    static const wxString TAG_INFILE;
    static const wxString TAG_INFILES;
    static const wxString TAG_LOCI_MATCHING;
    static const wxString TAG_LOCUS_NAME;
    static const wxString TAG_MAP_POSITION;
    static const wxString TAG_NAME;
    static const wxString TAG_OUTFILE;
    static const wxString TAG_PHASE_FILE;
    static const wxString TAG_POPULATION;
    static const wxString TAG_POPULATIONS;
    static const wxString TAG_POP_MATCHING;
    static const wxString TAG_POP_NAME;
    static const wxString TAG_REGION;
    static const wxString TAG_REGIONS;
    static const wxString TAG_RELATIVE_PROB;
    static const wxString TAG_SAMPLES_PER_INDIVIDUAL;
    static const wxString TAG_SCANNED_DATA_POSITIONS;
    static const wxString TAG_SCANNED_LENGTH;
    static const wxString TAG_SITES;
    static const wxString TAG_SPACING;
    static const wxString TAG_TRAIT;
    static const wxString TAG_TRAIT_INFO;

    static const wxString ERR_BYNAME_POP_MATCHER_NO_VALUE;
    static const wxString ERR_DATA_LENGTH_REQUIRED;
    static const wxString ERR_DNA_LOCATIONS;
    static const wxString ERR_EMPTY_POP_NAME;
    static const wxString ERR_EXTRA_TAG;
    static const wxString ERR_HAP_DATA_SIZE_MISMATCH;
    static const wxString ERR_LOCATION_SITE_MISMATCH;
    static const wxString ERR_MISSING_FILE;
    static const wxString ERR_MISSING_TAG;
    static const wxString ERR_NAME_REPEAT;
    static const wxString ERR_NO_DATA_LENGTH_FOR_DNA;
    static const wxString ERR_NO_DATATYPE;
    static const wxString ERR_NO_FORMAT;
    static const wxString ERR_NO_INTERLEAVING;
    static const wxString ERR_NO_SUCH_POP_NAME;
    static const wxString ERR_NOT_DOUBLE;
    static const wxString ERR_NOT_LONG;
    static const wxString ERR_NOT_SIZE_T;
    static const wxString ERR_ROW_WRAP;
    static const wxString ERR_SHORT_DATA_LENGTH;
    static const wxString ERR_TRAIT_REPEAT;
    static const wxString ERR_UNKNOWN_LOCUS_MATCHER;
    static const wxString ERR_UNKNOWN_POP_MATCHER;
    static const wxString ERR_UNRECOGNIZED_TAG;

    static const wxString WARN_NO_LOCATIONS;
#endif

    static const wxString notXmlMapFileTryOldFmt;
};

class gcerr_map
{
  public:
    static const wxString ERR_BAD_TOP_TAG;
    static const wxString fileEmpty;
    static const wxString fileMissing;
    static const wxString fileReadErr;
};

#endif  // GC_STRINGS_MAP_H

//____________________________________________________________________________________
