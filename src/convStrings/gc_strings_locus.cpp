// $Id: gc_strings_locus.cpp,v 1.16 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_locus.h"
#include "wx/intl.h"

const wxString gcerr_locus::unsetLinkedUserValue =   "call to GetLinkedUserValue() without HaveLinkedUserValue()";
const wxString gcerr_locus::unsetOffset =   "call to GetOffset() without HaveOffset()";

const wxString gcerr_locus::dnaBigLengthNeedsLocations =wxTRANSLATE("Segment \"%s\" of type DNA requires locations since its total length is greater than its number of markers.");
const wxString gcerr_locus::lengthMismatch =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because they have lengths of %d and %d.");
const wxString gcerr_locus::lengthMissing =wxTRANSLATE("Segment \"%s\" needs a total length assignment.");
const wxString gcerr_locus::locationsOutOfOrder =wxTRANSLATE("In segment \"%s\": location %ld cannot appear after %ld, they must be strictly increasing.");
const wxString gcerr_locus::locationTooLarge =wxTRANSLATE("%d cannot be a location for segment \"%s\" since the largest possible value is %d.");
const wxString gcerr_locus::locationTooSmall =wxTRANSLATE("%d cannot be a location for segment \"%s\" since the smallest possible value is %d.");
const wxString gcerr_locus::mapPositionMismatch =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because they have map positions of %ld and %ld.");
const wxString gcerr_locus::mergeFailure   =wxTRANSLATE("Something unexpected happened while trying to merge segments.\n\nIt is possible your operation did not complete.");
const wxString gcerr_locus::missing        =wxTRANSLATE("Segment \"%s\" not defined");
const wxString gcerr_locus::numMarkersZero =wxTRANSLATE("Segment must have one or more markers.");
const wxString gcerr_locus::offsetMismatch =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because they have first position scanned of %ld and %ld.");
const wxString gcerr_locus::offsetMissingMultiSegment =wxTRANSLATE("Segment \"%s\" requires you to set the first position scanned since multiple segments appear in the same region.");
const wxString gcerr_locus::offsetMissingSnpLocations =wxTRANSLATE("Segment \"%s\" requires you to set the first position scanned because it is SNP data with locations.");
const wxString gcerr_locus::overlap =wxTRANSLATE("segment %s (%ld:%ld) overlaps segment %s (%ld:%ld)");
const wxString gcerr_locus::setLocs =wxTRANSLATE("Unable to set locations for segment \"%s\" to \"%s\" because they have different site counts of %ld and %ld.");
const wxString gcerr_locus::siteCountMismatch =wxTRANSLATE("Unable to merge data from the following segments because they have different site counts of %d and %d:\n\n\t%s\n\n\t%s");

//const wxString gcerr_structures::hapMismatch    =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because parsed data has ploidy of %d and segment has %d.");
const wxString gcerr_locus::typeMismatch   =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because they have incompatible data types of %s and %s.");
const wxString gcerr_locus::unlinkedNuc =wxTRANSLATE("Cannot set DNA or SNP data to be unlinked");
const wxString gcerr_locus::unsetNumMarkers=wxTRANSLATE("Num Markers not set for segment \"%s\".");
const wxString gcerr_locus::userDataTypeMismatch =wxTRANSLATE("Unable to merge data from segment \"%s\" with segment \"%s\" because they have different data types of %s and %s.");
const wxString gcerr_locus::wrongLocationCount =wxTRANSLATE("Cannot have %ld locations for segment \"%s\".\n\nThe number of locations must be %ld, the number of data markers.");

//____________________________________________________________________________________
