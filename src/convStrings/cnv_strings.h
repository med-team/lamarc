// $Id: cnv_strings.h,v 1.19 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef CNV_STR_H
#define CNV_STR_H

#include "wx/string.h"

class cnvstr
{
  public:
    static const wxString ATTR_DATATYPE;
    static const wxString ATTR_FORMAT;
    static const wxString ATTR_PROXIMITY;
    static const wxString ATTR_SEQUENCEALIGNMENT;
    static const wxString ATTR_TYPE;
    static const wxString ATTR_VERSION;

    static const wxString ATTR_VAL_BYADJACENCY;
    static const wxString ATTR_VAL_BYLIST;
    static const wxString ATTR_VAL_BYNAME;
    static const wxString ATTR_VAL_DEFAULT;
    static const wxString ATTR_VAL_KNOWN;
    static const wxString ATTR_VAL_LINKED;
    static const wxString ATTR_VAL_SINGLE;
    static const wxString ATTR_VAL_UNKNOWN;
    static const wxString ATTR_VAL_UNLINKED;

    static const wxString TAG_ADDCOMMENT;
    static const wxString TAG_ALLELE;
    static const wxString TAG_ALLELES;
    static const wxString TAG_CONVERTER_CMD;
    static const wxString TAG_DIVERGENCE;
    static const wxString TAG_DIVERGENCES;
    static const wxString TAG_DIV_ANCESTOR;
    static const wxString TAG_DIV_CHILD1;
    static const wxString TAG_DIV_CHILD2;
    static const wxString TAG_EFFECTIVE_POPSIZE;
    static const wxString TAG_FIRST_POSITION_SCANNED;
    static const wxString TAG_GENO_RESOLUTIONS;
    static const wxString TAG_HAPLOTYPES;
    static const wxString TAG_HAS_PHENOTYPE;
    static const wxString TAG_INDIVIDUAL;
    static const wxString TAG_INDIVIDUALS;
    static const wxString TAG_INDIVIDUALS_FROM_SAMPLES;
    static const wxString TAG_INFILE;
    static const wxString TAG_INFILES;
    static const wxString TAG_MAP_POSITION;
    static const wxString TAG_MARKERS;
    static const wxString TAG_NAME;
    static const wxString TAG_OUTFILE;
    static const wxString TAG_PANEL;
    static const wxString TAG_PANELS;
    static const wxString TAG_PANEL_NAME;
    static const wxString TAG_PANEL_POP;
    static const wxString TAG_PANEL_REGION;
    static const wxString TAG_PANEL_SIZE;
    static const wxString TAG_PENETRANCE;
    static const wxString TAG_PHASE;
    static const wxString TAG_PHENOTYPE;
    static const wxString TAG_POPULATION;
    static const wxString TAG_POPULATIONS;
    static const wxString TAG_POP_MATCHING;
    static const wxString TAG_POP_NAME;
    static const wxString TAG_REGION;
    static const wxString TAG_REGIONS;
    static const wxString TAG_SAMPLE;
    static const wxString TAG_SAMPLES_PER_INDIVIDUAL;
    static const wxString TAG_SCANNED_DATA_POSITIONS;
    static const wxString TAG_SCANNED_LENGTH;
    static const wxString TAG_SEGMENT;
    static const wxString TAG_SEGMENTS;
    static const wxString TAG_SEGMENTS_MATCHING;
    static const wxString TAG_SEGMENT_NAME;
    static const wxString TAG_TRAIT;
    static const wxString TAG_TRAIT_LOCATION;
    static const wxString TAG_TRAIT_NAME;
    static const wxString TAG_TRAITS;
    static const wxString TAG_TRAIT_INFO;
    static const wxString TAG_UNRESOLVED_MARKERS;

    static const wxString ERR_BAD_TOP_TAG;
    static const wxString ERR_BYNAME_POP_MATCHER_NO_VALUE;
    static const wxString ERR_DATA_LENGTH_REQUIRED;
    static const wxString ERR_DNA_LOCATIONS;
    static const wxString ERR_EMPTY_POP_NAME;
    static const wxString ERR_EXTRA_TAG;
    static const wxString ERR_HAP_DATA_SIZE_MISMATCH;
    static const wxString ERR_LENGTH_REQUIRED_WITH_LOCATIONS;
    static const wxString ERR_LOCATIONS_REQUIRED_WITH_LENGTH;
    static const wxString ERR_LOCATIONS_REQUIRE_OFFSET;
    static const wxString ERR_LOCATION_SITE_MISMATCH;
    static const wxString ERR_MAP_POSITION_REQUIRED;
    static const wxString ERR_MISSING_FILE;
    static const wxString ERR_MISSING_TAG;
    static const wxString ERR_NAME_REPEAT;
    static const wxString ERR_NOT_DOUBLE;
    static const wxString ERR_NOT_LONG;
    static const wxString ERR_NOT_SIZE_T;
    static const wxString ERR_NO_DATATYPE;
    static const wxString ERR_NO_FORMAT;
    static const wxString ERR_NO_INTERLEAVING;
    static const wxString ERR_NO_NUCLEOTIDES_UNLINKED;
    static const wxString ERR_NO_SUCH_POP_NAME;
    static const wxString ERR_NO_UNKNOWN_DATATYPES;
    static const wxString ERR_OFFSET_REQUIRED;
    static const wxString ERR_ROW_WRAP;
    static const wxString ERR_SHORT_DATA_LENGTH;
    static const wxString ERR_TRAIT_REPEAT;
    static const wxString ERR_UNKNOWN_SEGMENT_MATCHER;
    static const wxString ERR_UNKNOWN_POP_MATCHER;
    static const wxString ERR_UNRECOGNIZED_TAG;

    static const wxString WARN_NO_LENGTH;
    static const wxString WARN_NO_LOCATIONS;
};

#endif  // CNV_STR_H

//____________________________________________________________________________________
