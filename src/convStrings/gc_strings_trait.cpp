// $Id: gc_strings_trait.cpp,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_trait.h"
#include "wx/intl.h"

const wxString gcerr_trait::alleleMissing   =   wxTRANSLATE("cannot find trait allele with name %s.");
const wxString gcerr_trait::alleleNameReuse =   wxTRANSLATE("re-use of allele name %s not allowed");
const wxString gcerr_trait::alleleNameSpaces=   wxTRANSLATE("Allele name \"%s\" not allowed because it contains one or more spaces");
const wxString gcerr_trait::alleleTraitMismatch= wxTRANSLATE("Allele %s in phenotype %s does not belong to trait %s.");
const wxString gcerr_trait::hapProbabilityNegative = wxTRANSLATE("Relative penetrance of %f illegal -- must be a non-negative number");
const wxString gcerr_trait::phenoTraitMismatch= wxTRANSLATE("Phenotype %s trait %s differs from enclosing trait %s.");
const wxString gcerr_trait::phenotypeMissing   =   wxTRANSLATE("cannot find phenotype with name %s.");
const wxString gcerr_trait::phenotypeNameReuse =   wxTRANSLATE("re-use of phenotype name %s for trait %s not allowed");

const wxString gcstr_trait::alleleListMember=              ("%s ");
const wxString gcstr_trait::generatedName   =              ("trait_%ld");
const wxString gcstr_trait::internalName    =              ("internalTrait_%ld");

//____________________________________________________________________________________
