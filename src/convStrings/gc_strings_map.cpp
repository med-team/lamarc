// $Id: gc_strings_map.cpp,v 1.6 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_map.h"
#include "wx/intl.h"

const wxString gcstr_map::notXmlMapFileTryOldFmt    =wxTRANSLATE("%s. Trying to read %s as old map file format.");

const wxString gcerr_map::ERR_BAD_TOP_TAG   =wxTRANSLATE("expected top xml tag of <%s> but got <%s> instead.");
const wxString gcerr_map::fileEmpty         =wxTRANSLATE("File %s empty");
const wxString gcerr_map::fileMissing       =wxTRANSLATE("File %s missing");
const wxString gcerr_map::fileReadErr       =wxTRANSLATE("Unknown error while reading file %s");

//____________________________________________________________________________________
