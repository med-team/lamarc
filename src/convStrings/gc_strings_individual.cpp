// $Id: gc_strings_individual.cpp,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_individual.h"
#include "wx/intl.h"

const wxString gcerr_ind::missingPhaseForLocus  = wxTRANSLATE("Individual \"%s\" is missing phase data for segment \"%s\".\n\nDid you write a phase information file?");
const wxString gcerr_ind::phaseLocusRepeat      = wxTRANSLATE("Attempted to add phase data to individual \"%s\" from segment \"%s\" a second time.\n\nCheck to see if individual name is duplicated.");
const wxString gcerr_ind::sampleLocusRepeat     = wxTRANSLATE("Attempted to add data to sample \"%s\" from segment \"%s\" a second time.\n\nCheck to see if sample name is duplicated.");
const wxString gcerr_ind::sampleMissingLocusData= wxTRANSLATE("Cannot find data for sample \"%s\" of segment \"%s\".\n\nYou may need to provide individual/sample resolution information in a phase file.");
const wxString gcerr_ind::wrongSampleCount      = wxTRANSLATE("Individual \"%s\" has conflicting sample counts for data.");

//____________________________________________________________________________________
