// $Id: gc_strings_phase.cpp,v 1.12 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_phase.h"
#include "wx/intl.h"

const wxString gcerr_phase::adjacentPhaseForMultiSample = wxTRANSLATE("Cannot group adjacent samples from file %s into single individuals -- each sample is already a multi-phase sample");
const wxString gcerr_phase::badIndMatchAdjacencyValue = wxTRANSLATE("Cannot group each \"%s\" adjacent samples into single individual. Please use an integer greater or equal to 2.");
const wxString gcerr_phase::badIndMatchType  = wxTRANSLATE("Bad value \"%s\" for individual-matching type attribute. Current legal values are: byAdjacency");
const wxString gcerr_phase::badTopTag = wxTRANSLATE("expected top xml tag of <%s> but got <%s> instead");
const wxString gcerr_phase::bothIndividualAndSample = wxTRANSLATE("Name \"%s\" appears as both an individual name and a sample name. This is not legal.");
const wxString gcerr_phase::individualPhenotypeNameRepeat = wxTRANSLATE("Phenotype \"%s\" appears more once for individual \"%s\".");
const wxString gcerr_phase::markerNotLegal =wxTRANSLATE("unresolved phase position \"%s\" illegal -- must be an integer.");
const wxString gcerr_phase::matchingConfusion = wxTRANSLATE("Cannot determine how to match up all data samples in region %s.\n\nPlease write a converter command file specifying individual and sample relationships using the <individuals> tag.\n\nSamples that need information include \"%s\" and \"%s\".");
const wxString gcerr_phase::mergeMismatch = wxTRANSLATE("Cannot resolve the following individuals\n\n%s\n\n%s\n\nIf present, sample names must match and occur in the same order.");
const wxString gcerr_phase::noIndividualForSample = wxTRANSLATE("No information found relating sample %s to any individual");
const wxString gcerr_phase::noSampleForIndividual = wxTRANSLATE("No information found relating individual %s to any samples");
const wxString gcerr_phase::notLocation = wxTRANSLATE("Cannot assign unresolved phase to marker at position %ld in segment \"%s\" because the segment locations do not include it.");
const wxString gcerr_phase::tooLarge = wxTRANSLATE("Cannot assign unresolved phase to marker at position %ld for individual \"%s\" because position is too large. Segment \"%s\" has largest marker position of %ld.");
const wxString gcerr_phase::tooSmall = wxTRANSLATE("Cannot assign unresolved phase to marker at position %ld for individual \"%s\" because position is too small. Segment \"%s\" has smallest marker position of %ld.");
const wxString gcerr_phase::unevenAdjDivisor      = wxTRANSLATE("Cannot evenly allocate %d samples to individuals with %d samples each.");

const wxString gcstr_phase::adjacentHaps1     =   wxTRANSLATE("Group every ");
const wxString gcstr_phase::adjacentHaps2     =   wxTRANSLATE(" adjacent samples into one individual");
const wxString gcstr_phase::descFileAdjacency =   wxTRANSLATE("induced by adjacency near line %s of file %s, and containing samples %s");
const wxString gcstr_phase::descMultiPhase    =   wxTRANSLATE("induced by multiploid data near line %s of file %s, and containing samples %s");
const wxString gcstr_phase::descPhaseFile     =   wxTRANSLATE("%s defined near line %s of file %s, and containing samples %s");
const wxString gcstr_phase::known   =   wxTRANSLATE("known");
const wxString gcstr_phase::unknown =   wxTRANSLATE("unknown");

//____________________________________________________________________________________
