// $Id: cnv_strings.cpp,v 1.20 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "cnv_strings.h"
#include "wx/intl.h"

// attribute names -- we don't use wxTRANSLATE because these are input files
const wxString cnvstr::ATTR_DATATYPE                = "datatype";
const wxString cnvstr::ATTR_FORMAT                  = "format";
const wxString cnvstr::ATTR_PROXIMITY               = "marker-proximity";
const wxString cnvstr::ATTR_SEQUENCEALIGNMENT       = "sequence-alignment";
const wxString cnvstr::ATTR_TYPE                    = "type";
const wxString cnvstr::ATTR_VERSION                 = "version";

// attribute values -- we don't use wxTRANSLATE because these are input files
const wxString cnvstr::ATTR_VAL_BYADJACENCY         = "byAdjacency";
const wxString cnvstr::ATTR_VAL_BYLIST              = "byList";
const wxString cnvstr::ATTR_VAL_BYNAME              = "byName";
const wxString cnvstr::ATTR_VAL_DEFAULT             = "default";
const wxString cnvstr::ATTR_VAL_KNOWN               = "known";
const wxString cnvstr::ATTR_VAL_LINKED              = "linked";
const wxString cnvstr::ATTR_VAL_SINGLE              = "single";
const wxString cnvstr::ATTR_VAL_UNKNOWN             = "unknown";
const wxString cnvstr::ATTR_VAL_UNLINKED            = "unlinked";

// tag names -- we don't use wxTRANSLATE because these are input files
const wxString cnvstr::TAG_ADDCOMMENT               = "lamarc-header-comment";
const wxString cnvstr::TAG_ALLELE                   = "allele";
const wxString cnvstr::TAG_ALLELES                  = "alleles";
const wxString cnvstr::TAG_CONVERTER_CMD            = "lamarc-converter-cmd";
const wxString cnvstr::TAG_DIVERGENCE               = "divergence";
const wxString cnvstr::TAG_DIVERGENCES              = "divergences";
const wxString cnvstr::TAG_DIV_ANCESTOR             = "ancestor";
const wxString cnvstr::TAG_DIV_CHILD1               = "child1";
const wxString cnvstr::TAG_DIV_CHILD2               = "child2";
const wxString cnvstr::TAG_EFFECTIVE_POPSIZE        = "effective-popsize";
const wxString cnvstr::TAG_FIRST_POSITION_SCANNED   = "first-position-scanned";
const wxString cnvstr::TAG_GENO_RESOLUTIONS         = "genotype-resolutions";
const wxString cnvstr::TAG_HAPLOTYPES               = "haplotypes";
const wxString cnvstr::TAG_HAS_PHENOTYPE            = "has-phenotype";
const wxString cnvstr::TAG_INDIVIDUAL               = "individual";
const wxString cnvstr::TAG_INDIVIDUALS              = "individuals";
const wxString cnvstr::TAG_INDIVIDUALS_FROM_SAMPLES = "individuals-from-samples";
const wxString cnvstr::TAG_INFILE                   = "infile";
const wxString cnvstr::TAG_INFILES                  = "infiles";
const wxString cnvstr::TAG_MAP_POSITION             = "map-position";
const wxString cnvstr::TAG_MARKERS                  = "markers";
const wxString cnvstr::TAG_NAME                     = "name";
const wxString cnvstr::TAG_OUTFILE                  = "outfile";
const wxString cnvstr::TAG_PANEL                    = "panel";
const wxString cnvstr::TAG_PANELS                   = "panels";
const wxString cnvstr::TAG_PANEL_NAME               = "panel-name";
const wxString cnvstr::TAG_PANEL_POP                = "panel-pop";
const wxString cnvstr::TAG_PANEL_REGION             = "panel-region";
const wxString cnvstr::TAG_PANEL_SIZE               = "panel-size";
const wxString cnvstr::TAG_PENETRANCE               = "penetrance";
const wxString cnvstr::TAG_PHASE                    = "phase";
const wxString cnvstr::TAG_PHENOTYPE                = "phenotype";
const wxString cnvstr::TAG_POPULATION               = "population";
const wxString cnvstr::TAG_POPULATIONS              = "populations";
const wxString cnvstr::TAG_POP_MATCHING             = "population-matching";
const wxString cnvstr::TAG_POP_NAME                 = "population-name";
const wxString cnvstr::TAG_REGION                   = "region";
const wxString cnvstr::TAG_REGIONS                  = "regions";
const wxString cnvstr::TAG_SAMPLE                   = "sample";
const wxString cnvstr::TAG_SAMPLES_PER_INDIVIDUAL   = "samples-per-individual";
const wxString cnvstr::TAG_SCANNED_DATA_POSITIONS   = "locations";
const wxString cnvstr::TAG_SCANNED_LENGTH           = "length";
const wxString cnvstr::TAG_SEGMENT                  = "segment";
const wxString cnvstr::TAG_SEGMENTS                 = "segments";
const wxString cnvstr::TAG_SEGMENTS_MATCHING        = "segments-matching";
const wxString cnvstr::TAG_SEGMENT_NAME             = "segment-name";
const wxString cnvstr::TAG_TRAIT                    = "trait";
const wxString cnvstr::TAG_TRAIT_LOCATION           = "trait-location";
const wxString cnvstr::TAG_TRAIT_NAME               = "trait-name";
const wxString cnvstr::TAG_TRAITS                   = "traits";
const wxString cnvstr::TAG_TRAIT_INFO               = "trait-info";
const wxString cnvstr::TAG_UNRESOLVED_MARKERS       = "unresolved-markers";

// errors -- use wxTRANSLATE to enable internationalization
const wxString cnvstr::ERR_BAD_TOP_TAG      = wxTRANSLATE("Expected top level tag <lamarc-converter-cmd> but got \"%s\"");
const wxString cnvstr::ERR_BYNAME_POP_MATCHER_NO_VALUE = wxTRANSLATE("\"byName\" population matcher should have no text within tag");
const wxString cnvstr::ERR_DATA_LENGTH_REQUIRED = wxTRANSLATE("SNP data requires <length> tag");
const wxString cnvstr::ERR_DNA_LOCATIONS    = wxTRANSLATE("Can't set locations for segment %s (near line %d) because it is DNA data.");
const wxString cnvstr::ERR_EMPTY_POP_NAME   = wxTRANSLATE("Empty population name");
const wxString cnvstr::ERR_EXTRA_TAG        = wxTRANSLATE("extra xml tag \"%s\"");
const wxString cnvstr::ERR_HAP_DATA_SIZE_MISMATCH = wxTRANSLATE("Expected %d haplotypes but data has %d");
const wxString cnvstr::ERR_LENGTH_REQUIRED_WITH_LOCATIONS = wxTRANSLATE("If you provide a <locations> tag for this data type, you must provide a <length> tag as well.");
const wxString cnvstr::ERR_LOCATIONS_REQUIRED_WITH_LENGTH = wxTRANSLATE("If you provide a <length> tag to a <segment> for this data type, you must provide a <locations> tag as well.");
const wxString cnvstr::ERR_LOCATIONS_REQUIRE_OFFSET = wxTRANSLATE("if you specify locations for a segment, you must also specify the first position scanned.");
const wxString cnvstr::ERR_LOCATION_SITE_MISMATCH= wxTRANSLATE("Number of items in <location> tag at line %d should be %d, the number of sites in the segment");
const wxString cnvstr::ERR_MAP_POSITION_REQUIRED = wxTRANSLATE("<map-position> tag required for any segment in a multi-segment region");
const wxString cnvstr::ERR_MISSING_FILE     = wxTRANSLATE("File \"%s\" does not exist or is not readable.");
const wxString cnvstr::ERR_MISSING_TAG      = wxTRANSLATE("missing xml tag \"%s\"");
const wxString cnvstr::ERR_NAME_REPEAT      = wxTRANSLATE("Identifier \"%s\" in line %d conflicts with previous definition in line %d. All population, region, and segment names must be distinct.");
const wxString cnvstr::ERR_NOT_DOUBLE       = wxTRANSLATE("\"%s\" is not a real number");
const wxString cnvstr::ERR_NOT_LONG         = wxTRANSLATE("\"%s\" is not an integer");
const wxString cnvstr::ERR_NOT_SIZE_T       = wxTRANSLATE("\"%ld\" is not non-negative");
const wxString cnvstr::ERR_NO_DATATYPE      = wxTRANSLATE("<infile> tag at line %d requrires the datatype attribute.");
const wxString cnvstr::ERR_NO_FORMAT        = wxTRANSLATE("<infile> tag at line %d requrires the format attribute.");
const wxString cnvstr::ERR_NO_INTERLEAVING  = wxTRANSLATE("<infile> tag at line %d requrires sequence-alignment attribute");
const wxString cnvstr::ERR_NO_NUCLEOTIDES_UNLINKED = wxTRANSLATE("data type \"%s\" may not be set \"%s\"");
const wxString cnvstr::ERR_NO_SUCH_POP_NAME = wxTRANSLATE("Population \"%s\" not defined");
const wxString cnvstr::ERR_NO_UNKNOWN_DATATYPES = wxTRANSLATE("<%s> tag at line %d may not use the 'unknown' datatype \"%s\".");
const wxString cnvstr::ERR_OFFSET_REQUIRED = wxTRANSLATE("Segment tag at line %d requires the <first-position-scanned> tag when you are using <locations>.");
const wxString cnvstr::ERR_ROW_WRAP         = wxTRANSLATE("near line %d:%s");
const wxString cnvstr::ERR_SHORT_DATA_LENGTH= wxTRANSLATE("Data length for segment at line %d is shorter than number of sites");
const wxString cnvstr::ERR_TRAIT_REPEAT     = wxTRANSLATE("Region \"%s\" redefines trait \"%s\" in line %d");
const wxString cnvstr::ERR_UNKNOWN_SEGMENT_MATCHER  = wxTRANSLATE("Unknown segment matching type \"%s\"");
const wxString cnvstr::ERR_UNKNOWN_POP_MATCHER    = wxTRANSLATE("Unknown population matching type \"%s\" in line %d");
const wxString cnvstr::ERR_UNRECOGNIZED_TAG = wxTRANSLATE("Unrecognized tag: \"%s\" in line %d");

// warnings -- use wxTRANSLATE to enable internationalization
const wxString cnvstr::WARN_NO_LOCATIONS  = wxTRANSLATE("No location data set for segment %s. You will not be allowed to estimate recombination using this data.");
const wxString cnvstr::WARN_NO_LENGTH     = wxTRANSLATE("No length data set for segment %s. You will not be allowed to estimate recombination using this data.");

//____________________________________________________________________________________
