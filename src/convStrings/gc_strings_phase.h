// $Id: gc_strings_phase.h,v 1.13 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_PHASE_H
#define GC_STRINGS_PHASE_H

#include "wx/string.h"

class gcerr_phase
{
  public:
    static const wxString adjacentPhaseForMultiSample;
    static const wxString badIndMatchAdjacencyValue;
    static const wxString badIndMatchType;
    static const wxString badTopTag;
    static const wxString bothIndividualAndSample;
    static const wxString individualPhenotypeNameRepeat;
    static const wxString markerNotLegal;
    static const wxString matchingConfusion;
    static const wxString mergeMismatch;
    static const wxString noIndividualForSample;
    static const wxString noSampleForIndividual;
    static const wxString notLocation;
    static const wxString tooLarge;
    static const wxString tooSmall;
    static const wxString unevenAdjDivisor;
};

class gcstr_phase
{
  public:
    static const wxString adjacentHaps1;
    static const wxString adjacentHaps2;
    static const wxString descFileAdjacency;
    static const wxString descMultiPhase;
    static const wxString descPhaseFile;
    static const wxString known;
    static const wxString unknown;
};

#endif  // GC_STRINGS_PHASE_H

//____________________________________________________________________________________
