// $Id: gc_strings_mig.cpp,v 1.3 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_mig.h"
#include "wx/intl.h"

const wxString gcstr_mig::internalName = "internalMig_%ld";
const wxString gcstr_mig::migmethodUser     =   wxTRANSLATE("USER");
const wxString gcstr_mig::migmethodFST      =   wxTRANSLATE("FST");
const wxString gcstr_mig::migprofileNone        =   wxTRANSLATE("None");
const wxString gcstr_mig::migprofileFixed       =   wxTRANSLATE("Fixed");
const wxString gcstr_mig::migprofilePercentile  =   wxTRANSLATE("Percentile");
const wxString gcstr_mig::migconstraintInvalid      = wxTRANSLATE("Invalid");
const wxString gcstr_mig::migconstraintConstant     = wxTRANSLATE("Constant");
const wxString gcstr_mig::migconstraintSymmetric    = wxTRANSLATE("Symmetric");
const wxString gcstr_mig::migconstraintUnconstained = wxTRANSLATE("Unconstrained");

//____________________________________________________________________________________
