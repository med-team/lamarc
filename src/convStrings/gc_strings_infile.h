// $Id: gc_strings_infile.h,v 1.15 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_INFILE_H
#define GC_STRINGS_INFILE_H

#include "wx/string.h"

class gcerr_infile
{
  public:
    static const wxString extraFileData;
    static const wxString fileParseError;
    static const wxString illegalDna;
    static const wxString illegalMsat;
    static const wxString parseDataTypeSpecMismatch;
    static const wxString parseMissingErr;
    static const wxString prematureEndOfFile;
    static const wxString shortSampleName;
    static const wxString tokenCountMismatch;
    static const wxString tooFewMarkersInSample;
    static const wxString tooManyMarkersInSample;
    static const wxString unableToParseBecause;
};

class gcerr_migrate
{
  public:
    static const wxString badDelimiter;
    static const wxString badLocusCount;
    static const wxString badPopCount;
    static const wxString badSequenceCount;
    static const wxString firstToken;
    static const wxString locusLengthNotPositive;
    static const wxString missingMsatDelimiter;
    static const wxString missingSequenceCount;
    static const wxString parseErr;
    static const wxString tooFewSequenceLengths;
};

class gcstr_migrate
{
  public:
    static const char missingData;
};

class gcerr_phylip
{
  public:
    static const wxString badFirstToken;
    static const wxString badSecondToken;
};

#endif  // GC_STRINGS_INFILE_H

//____________________________________________________________________________________
