// $Id: gc_strings_structures.h,v 1.12 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_STRUCTURES_H
#define GC_STRINGS_STRUCTURES_H

#include "wx/string.h"

class gcstr_structures
{
  public:
    static const wxString objDict;
    static const wxString traitDict;
};

class gcerr_structures
{
  public:

    static const wxString duplicateFileBaseName;
    static const wxString duplicateFileName;
    static const wxString duplicateName;

#if 0
    static const wxString locusMergeFailure;
    static const wxString locusHapMismatch;
    static const wxString locusLengthMismatch;
    static const wxString locusTypeMismatch;
    static const wxString missingLocus;
#endif

    static const wxString migrationNotDefined;
    static const wxString mismatchAlleleTrait;
    static const wxString mismatchLocusRegion;

    static const wxString missingFile;
    static const wxString missingMigration;
    static const wxString missingMigrationId;
    static const wxString missingName;
    static const wxString missingPopulation;
    static const wxString missingPanel;
    static const wxString missingPanelId;
    static const wxString missingParent;
    static const wxString missingParentId;
    static const wxString missingRegion;
    static const wxString missingTrait;
    static const wxString nameRepeatAllele;
    static const wxString nameRepeatLocus;
    static const wxString nameRepeatPop;
    static const wxString nameRepeatRegion;
    static const wxString nameRepeatTrait;
    static const wxString panelBlessedError;
    static const wxString panelNotDefined;
    static const wxString panelSizeClash;
    static const wxString regionEffPopSizeClash;
    // static const wxString regionSamplesPerClash;
    static const wxString unparsableFile;
};

#endif  // GC_STRINGS_STRUCTURES_H

//____________________________________________________________________________________
