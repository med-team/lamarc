// $Id: gc_strings_err.cpp,v 1.9 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings.h"
#include "wx/intl.h"

const wxString gcerr::abandonExport          = "internal message -- abandoning export. if you're reading this, there's an error.";

const wxString gcerr::badDivergenceId    = wxTRANSLATE("Bad child id in \"%s\" Couldn't set Parent id for \"%s\".");
const wxString gcerr::badEffectivePopSize    = wxTRANSLATE("Couldn't set effective population size for region \"%s\" to %f. The value must be positive.");
const wxString gcerr::badHapFile    = wxTRANSLATE("Failed to read hap file \"%s\" because of the following error:\n%s\nIgnoring.");
const wxString gcerr::badMapFile    = wxTRANSLATE("Failed to read map file \"%s\" because of the following error:\n%s\nIgnoring.");
const wxString gcerr::badName = wxTRANSLATE("Name \"%s\" contains a space which will break the XML parser.");

// const wxString gcerr::badSamplesPer = wxTRANSLATE("Couldn't set samples per individual for %s to %d. The value must be positive");
const wxString gcerr::badSequentialDataCast = wxTRANSLATE("GCSequentialData wasn't allelic when it should have been guaranteed to be so.");
const wxString gcerr::corruptedDisplayableLociInMapOrder= wxTRANSLATE("map order of displayable segments incorrectly calculated");
const wxString gcerr::duplicateLocusInRegion= wxTRANSLATE("Segment \"%s\" added to region \"%s\" twice.");
const wxString gcerr::duplicateMapPosition= wxTRANSLATE("More than one segment of region \"%s\" has map position %ld");
const wxString gcerr::emptyGroupName    =   wxTRANSLATE("Empty region name not allowed. Ignoring your command.");
const wxString gcerr::emptyLocusName    =   wxTRANSLATE("Empty segment name not allowed. Ignoring your command.");
const wxString gcerr::emptyPopulationName = wxTRANSLATE("Empty population name not allowed. Ignoring your command.");
const wxString gcerr::emptyName         =   wxTRANSLATE("Empty name for population, region, segment, or trait");
const wxString gcerr::fatalError        =   wxTRANSLATE("Cannot recover. Exiting.");
const wxString gcerr::fileWithRow       =   wxTRANSLATE("Near row %d of file %s:\n%s");
const wxString gcerr::fileWithoutRow    =   wxTRANSLATE("In file %s:\n%s");
const wxString gcerr::hapFileIllegalDelimiter   =   wxTRANSLATE("Delimiter \"%s\" illegal -- must be a single character.");
const wxString gcerr::hapFilePositionNegative   =   wxTRANSLATE("In phase information file \"%s\": position %d cannot be negative");
const wxString gcerr::hapFilePositionNotLong    =   wxTRANSLATE("In phase information file \"%s\": position \"%s\" not a long value");
const wxString gcerr::hapFilePositionUnordered  =   wxTRANSLATE("In phase information file \"%s\": position %d comes after later position %d");
const wxString gcerr::hapFileReadErrUnknown     =   wxTRANSLATE("Unknown error occured while reading phase information file \"%s\"");
const wxString gcerr::incompatibleLocusLengths=   wxTRANSLATE("Incompatible segment lengths %d and %d");
const wxString gcerr::incompatibleLocusTypes  =   wxTRANSLATE("Incompatible data types %s and %s");
const wxString gcerr::incompatibleNumHaps=  wxTRANSLATE("Incompatible hap counts %d and %d");
const wxString gcerr::inIndividual      =   wxTRANSLATE("Error writing data for individual \"%s\": %s");
const wxString gcerr::lengthTooShort    =   wxTRANSLATE("Length %d too short for data with %d sites");
const wxString gcerr::locusOverlap  = wxTRANSLATE("Segment \"%s\", ending at position %ld overlaps segment \"%s\" starting at position %ld.");
const wxString gcerr::locusWithoutDataType  = wxTRANSLATE("Segment \"%s\" in region \"%s\" needs data type assignment.");
const wxString gcerr::locusWithoutLength  = wxTRANSLATE("Segment \"%s\" needs data length assignment.");
const wxString gcerr::locusWithoutMapPosition  = wxTRANSLATE("Segment \"%s\" needs map position assignment.");
const wxString gcerr::migrationRateTooSmall = wxTRANSLATE("The Migration Rate cannot be less than zero");
const wxString gcerr::missingDataTypeForLocus   = wxTRANSLATE("Segment \"%s\" needs a data type assignment");
const wxString gcerr::missingHapFileId  =   wxTRANSLATE("No hap file with id\"%d\" found");
const wxString gcerr::missingFileId     =   wxTRANSLATE("No file with id\"%d\" found");
const wxString gcerr::missingLengthForLocus   = wxTRANSLATE("Segment \"%s\" needs a total length assignment");
const wxString gcerr::missingParse      =   wxTRANSLATE("Could not find any parse data assigned to file \"%s\"");
const wxString gcerr::missingParseId    =   wxTRANSLATE("No parse with id\"%d\" found");

//const wxString gcerr::missingPhaseDataForLocus=   wxTRANSLATE("Individual \"%s\" is missing phase information for segment \"%s\". Did you write a phase info file?");
const wxString gcerr::missingRegion     =   wxTRANSLATE("Did not find region \"%s\"");

//const wxString gcerr::missingSampleDataForLocus=   wxTRANSLATE("Sample \"%s\" of individual \"%s\" is missing data for segment \"%s\".");
const wxString gcerr::missingTrait      =   wxTRANSLATE("Did not find trait \"%s\"");
const wxString gcerr::mungeParseDataTypeMismatch=   wxTRANSLATE("Tried to munge parses with incompatible data types");
const wxString gcerr::mungeParseFormatMismatch=   wxTRANSLATE("Tried to munge parses with incompatible formats");
const wxString gcerr::mungeParseNeedsTwo=   wxTRANSLATE("Tried to munge other than two parses");
const wxString gcerr::mungeParseNull    =   wxTRANSLATE("Tried to munge null parse");
const wxString gcerr::nameResolutionPairMissing =   wxTRANSLATE("Did not find (population, region) pair (%s,%s) in name resolution");
const wxString gcerr::noBlockForPopLocus=   wxTRANSLATE("Did not find a block for pop %d and segment %d");
const wxString gcerr::noDataFound       =   wxTRANSLATE("Did not find any exportable data. Have you added a data file?");
const wxString gcerr::noSuchParse       =   wxTRANSLATE("Unable to read %s as a %s file with %s data in the '%s' format.");
const wxString gcerr::notALocation      =   wxTRANSLATE("\"%s\" does not specify a location. It should be a long integer");
const wxString gcerr::provideDoDelete   =   wxTRANSLATE("Subclasses of gcUpdatingDialog must override virtual method ::DoDelete method if they include a button with wxID_DELETE");
const wxString gcerr::regionNoData =   wxTRANSLATE("No data found for region \"%s\".");
const wxString gcerr::regionNoEffectivePopSize =   wxTRANSLATE("Call to GetEffectivePopSize() for region id %d without HasEffectivePopSize()");
const wxString gcerr::regionNoPositionToGet =   wxTRANSLATE("Call to GetPosition() without HasPosition()");

// const wxString gcerr::regionNoSamplesPerIndividual =   wxTRANSLATE("Call to GetSamplesPerIndividual() for region id %d without HasSamplesPerIndividual()");
const wxString gcerr::regionNoSuchLocus =   wxTRANSLATE("Segment with id %d not associated with region id %d");
const wxString gcerr::regionTraitAlreadyAdded=  wxTRANSLATE("Attempt to re-add trait with id %d");
const wxString gcerr::regionNoSuchTrait =   wxTRANSLATE("Trait with id %d not associated with region id %d");
const wxString gcerr::requireCmdFile    =   wxTRANSLATE("Option -c <cmdfile> required for batch (non-gui) converter.");
const wxString gcerr::shortDna          =   wxTRANSLATE("Tried to add bogus nuclear data \"%s\" to sample \"%s\". Is the sample too short or does it have non-dna data?");
const wxString gcerr::tooBigDataIndex   =   wxTRANSLATE("Index %d request parse sample larger than actual hap count of %d");
const wxString gcerr::tooFewLociInSpec  =   wxTRANSLATE("Spec for segment doesn't allow segment with index %d");
const wxString gcerr::tooFewPopsInSpec  =   wxTRANSLATE("Spec for populations doesn't allow segment with index %d");
const wxString gcerr::tooManySites      =   wxTRANSLATE("Found more data sites than expected.");
const wxString gcerr::unableToExport    =   wxTRANSLATE("Unable to export file because:%s");
const wxString gcerr::uncaughtException =   wxTRANSLATE("Uncaught Exception:%s");
const wxString gcerr::unsetChild1Id     =   wxTRANSLATE("Call to GetChild1Id() for segment \"%s\" without checking HasChild1Id()");
const wxString gcerr::unsetChild2Id     =   wxTRANSLATE("Call to GetChild2Id() for segment \"%s\" without checking HasChild2Id()");
const wxString gcerr::unsetFromId       =   wxTRANSLATE("Call to GetFromId() for segment \"%s\" without checking HasFromId()");
const wxString gcerr::unsetLength       =   wxTRANSLATE("Call to GetLength() for segment \"%s\" without checking HasLength()");
const wxString gcerr::unsetMapPosition  =   wxTRANSLATE("Call to GetMapPosition() for segment \"%s\" without checking HasMapPosition()");
const wxString gcerr::unsetNumSites     =   wxTRANSLATE("Call to GetNumSites() for segment \"%s\" without checking HasNumSites()");
const wxString gcerr::unsetParentId     =   wxTRANSLATE("Call to GetParentId() for segment \"%s\" without checking HasParentId()");
const wxString gcerr::unsetPopId        =   wxTRANSLATE("Call to GetPopId() for panel \"%s\" without checking HasPopId()");
const wxString gcerr::unsetRegionId     =   wxTRANSLATE("Call to GetRegionId() for segment \"%s\" without checking HasRegionId()");
const wxString gcerr::unsetToId         =   wxTRANSLATE("Call to GetToId() for segment \"%s\" without checking HasToId()");
const wxString gcerr::wrongDivergenceCount     =   wxTRANSLATE("Only 2 children can be merged in Divergence");

//____________________________________________________________________________________
