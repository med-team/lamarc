// $Id: gc_strings.h,v 1.45 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_H
#define GC_STRINGS_H

#include "wx/string.h"

class gcerr
{
  public:
    static const wxString abandonExport;

    static const wxString badDivergenceId;
    static const wxString badEffectivePopSize;
    static const wxString badHapFile;
    static const wxString badMapFile;
    static const wxString badName;
    // static const wxString badSamplesPer;
    static const wxString badSequentialDataCast;

    static const wxString corruptedDisplayableLociInMapOrder;

    static const wxString duplicateLocusInRegion;
    static const wxString duplicateMapPosition;

    static const wxString emptyGroupName;
    static const wxString emptyLocusName;
    static const wxString emptyName;
    static const wxString emptyPopulationName;
    static const wxString emptyTraitName;

    static const wxString fatalError;
    static const wxString fileWithRow;
    static const wxString fileWithoutRow;

    static const wxString hapFileIllegalDelimiter;
    static const wxString hapFilePositionNegative;
    static const wxString hapFilePositionNotLong;
    static const wxString hapFilePositionUnordered;
    static const wxString hapFileReadErrUnknown;

    static const wxString incompatibleLocusLengths;
    static const wxString incompatibleLocusTypes;
    static const wxString incompatibleNumHaps;
    static const wxString inIndividual;

    static const wxString lengthTooShort;

    static const wxString locusOverlap;
    static const wxString locusWithoutDataType;
    static const wxString locusWithoutLength;
    static const wxString locusWithoutMapPosition;

    static const wxString mapFileError;

    static const wxString migrationRateTooSmall;

    static const wxString missingDataTypeForLocus;
    static const wxString missingHapFileId;
    static const wxString missingFileId;
    static const wxString missingLengthForLocus;
    static const wxString missingParse;
    static const wxString missingParseId;
    static const wxString missingRegion;
    static const wxString missingTrait;

    static const wxString mungeParseDataTypeMismatch;
    static const wxString mungeParseFormatMismatch;
    static const wxString mungeParseNeedsTwo;
    static const wxString mungeParseNull;

    static const wxString nameResolutionPairMissing;

    static const wxString noBlockForPopLocus;
    static const wxString noDataFound;
    static const wxString noSuchParse;
    static const wxString notALocation;

    static const wxString panelBlessedError;
    static const wxString panelSizeClash;

    static const wxString provideDoDelete;

    static const wxString regionNoData;
    static const wxString regionNoEffectivePopSize;
    static const wxString regionNoPositionToGet;
    // static const wxString regionNoSamplesPerIndividual;
    static const wxString regionNoSuchLocus;
    static const wxString regionNoSuchTrait;
    static const wxString regionTraitAlreadyAdded;

    static const wxString requireCmdFile;
    static const wxString shortDna;

    static const wxString tooBigDataIndex;
    static const wxString tooFewLociInSpec;
    static const wxString tooFewPopsInSpec;
    static const wxString tooManySites;

    static const wxString unableToExport;
    static const wxString uncaughtException;
    static const wxString unsetChild1Id;
    static const wxString unsetChild2Id;
    static const wxString unsetFromId;
    static const wxString unsetLength;
    static const wxString unsetMapPosition;
    static const wxString unsetNumSites;
    static const wxString unsetParentId;
    static const wxString unsetPopId;
    static const wxString unsetToId;
    static const wxString unsetRegionId;

    static const wxString wrongDivergenceCount;
};

class gcstr
{
  public:
    static const wxString abandonExport;
    static const wxString addHapFile;
    static const wxString addLocus;
    static const wxString addPanel;
    static const wxString addParent;
    static const wxString addPop;
    static const wxString addRegion;
    static const wxString adjacent;
    static const wxString all;
    static const wxString allelic;
    static const wxString allele;
    static const wxString allFiles;
    static const wxString assignTabTitle;
    static const wxString badLocusLength;
    static const wxString badLocusLength1;
    static const wxString badLocusLength2;
    static const wxString badLocusPosition;
    static const wxString badName;
    static const wxString badRegionLength1;
    static const wxString badRegionLength2;
    static const wxString batchFileDefault;
    static const wxString batchOutComment;
    static const wxString batchSafeFinish;
    static const wxString blockFromFiles;
    static const wxString blockInfo1;
    static const wxString blockInfo2;
    static const wxString blockFileInfo;
    static const wxString blockLocusChoice;
    static const wxString blockLocusIndexInFile;
    static const wxString blockPloidyInfo;
    static const wxString blockPloidyTitle;
    static const wxString blockPopChoice;
    static const wxString blockPopIndexInFile;
    static const wxString blocksFromFiles;
    static const wxString buttonHide;
    static const wxString buttonSelectAll;
    static const wxString buttonShow;
    static const wxString buttonUnselectAll;
    static const wxString byfile;
    static const wxString byprog;
    static const wxString byuser;
    static const wxString cancelString;
    static const wxString cannotWrite;
    static const wxString chooseDataType;
    static const wxString childPopsInstructions;
    static const wxString chooseFileType;
    static const wxString chooseHapResolution;
    static const wxString chooseOne;
    static const wxString chooseOneGroup;
    static const wxString chooseOneLocus;
    static const wxString chooseOneParse;
    static const wxString chooseOnePop;
    static const wxString continueExport;
    static const wxString continueString;
    static const wxString converterInfo;
    static const wxString converterTitle;
    static const wxString createNewRegion;
    static const wxString createParent2Child;
    static const wxString createParentFirst2Children;
    static const wxString createParentNext2Children;
    static const wxString dataBlocks;
    static const wxString dataFileBatchExport;
    static const wxString dataFileButtonAdd;
    static const wxString dataFileButtonAllSelect;
    static const wxString dataFileButtonAllUnselect;
    static const wxString dataFileButtonRemoveSelected;
    static const wxString dataFileExport;
    static const wxString dataFiles;
    static const wxString dataFilesInstructions;
    static const wxString dataFilesSelect;
    static const wxString dataFilesTitle;
    static const wxString dataType;
    static const wxString delMapFile;
    static const wxString divergence;
    static const wxString divergeInstructions;
    static const wxString divergenceSelect;
    static const wxString divMigMatrix;
    static const wxString dna;
    static const wxString doneThanks;
    static const wxString editApply;
    static const wxString editCancel;
    static const wxString editDelete;
    static const wxString editFileSettings;
    static const wxString editLocus;
    static const wxString editMigration;
    static const wxString editOK;
    static const wxString editParseBlock;
    static const wxString editPanel;
    static const wxString editParent;
    static const wxString editPop;
    static const wxString editRegion;
    static const wxString enterNewName;
    static const wxString error;
    static const wxString errorWrap;
    static const wxString errorWrapNoFile;
    static const wxString exportFileDefault;
    static const wxString exportFileGlob;
    static const wxString exportWarning;
    static const wxString falseVal;
    static const wxString fileAlreadyAdded;
    static const wxString fileDelete;
    static const wxString fileEmpty;
    static const wxString fileLabelDataType;
    static const wxString fileLabelFormat;
    static const wxString fileLabelInterleaving;
    static const wxString fileLabelName;
    static const wxString fileLabelRemove;
    // static const wxString fileLabelUnlinked;
    static const wxString fileSetting;
    static const wxString firstPositionScanned;
    static const wxString fragmentRegion;
    static const wxString fullPath;
    static const wxString genoFileDefault;
    static const wxString globAll;
    static const wxString hapFileBarf;
    static const wxString hapFileDefault;
    static const wxString hapFileEmptyFirstLine;
    static const wxString hapFileMissing;
    static const wxString hapFileParseFailed;
    static const wxString hapFileReuse;
    static const wxString hapFileSelect;
    static const wxString hapFileSelectAnother;
    static const wxString hapFileToken1;
    static const wxString hapFileToken2;
    static const wxString hapFileToken3;
    static const wxString hapFileToken3Missing;
    static const wxString hapFileUnSelect;
    static const wxString hapFilesSelect;
    static const wxString havePatience;
    static const wxString hiddenContent;
    static const wxString indent;
    static const wxString information;
    static const wxString instructionsMultipleDataTypes;
    static const wxString interleaved;
    static const wxString interleavedNoKalleleMsat;
    static const wxString kallele;
    static const wxString linkGCreateTitle;
    static const wxString linkGEnterNewName;
    static const wxString linkageCaption;
    static const wxString linkageNo;
    static const wxString linkageYes;
    static const wxString locations;
    static const wxString locationsForRecom;
    static const wxString lociTabTitle;
    static const wxString locus;
    static const wxString locusButtonAdd;
    static const wxString locusButtonMergeSelected;
    static const wxString locusButtonRemoveSelected;
    static const wxString locusCreate;
    static const wxString locusCreateTitle;
    static const wxString locusDialogMapPosition;
    static const wxString locusEditString;
    static const wxString locusEnterNewName;
    static const wxString locusExists;
    static const wxString locusForAll;
    static const wxString locusLabelDataType;
    static const wxString locusLabelLength;
    static const wxString locusLabelLinked;
    static const wxString locusLabelMapFile;
    static const wxString locusLabelMapPosition;
    static const wxString locusLabelName;
    static const wxString locusLabelOffset;
    static const wxString locusLabelRegionName;
    static const wxString locusLabelSites;
    static const wxString locusLength;
    static const wxString locusLengthIllegal;
    static const wxString locusLengthVsMarkers;
    static const wxString locusNameFromFile;
    static const wxString locusNewName;
    static const wxString locusOffsetIllegal;
    static const wxString locusOwnRegion;
    static const wxString locusRename;
    static const wxString locusRenameChoice;
    static const wxString locusSelect;
    static const wxString locusMarkerCount;
    static const wxString locusUnlinked;
    static const wxString locusUse;
    static const wxString logWindowHeader;
    static const wxString mapFileExportFailed;
    static const wxString mapFileLastPositionTooLate;
    static const wxString mapFileMarkerPositionMismatch;
    static const wxString mapFileOffsetAfterFirstPosition;
    static const wxString mapFileRegionMissing;
    static const wxString mapFileSelect;
    static const wxString mapFileSelectAnother;
    static const wxString mapFileUnSelect;
    static const wxString mapFilesSelect;
    static const wxString mapPositionUnset;
    static const wxString members;
    static const wxString mergeLinkGInstructions;
    static const wxString mergeLinkGTitle;
    static const wxString mergeLociInstructions;
    static const wxString mergeLociTitle;
    static const wxString mergePopsInstructions;
    static const wxString mergePopsTitle;
    static const wxString microsat;
    static const wxString migrate;
    static const wxString migrationMatrix;
    static const wxString migConstraint;
    static const wxString migMethod;
    static const wxString migProfile;
    static const wxString migLabelRate;
    static const wxString migLabelConstraint;
    static const wxString migLabelMethod;
    static const wxString migLabelProfile;
    static const wxString migRate;
    static const wxString moot;
    static const wxString moveLocus;
    static const wxString multiPhaseSample;
    static const wxString nameCandidate;
    static const wxString nearRow;
    static const wxString newName;
    static const wxString no;
    static const wxString noChoice;
    static const wxString noChoiceLocus;
    static const wxString noChoicePopulation;
    static const wxString noChoiceRegion;
    static const wxString notebookLabelDataTab;
    static const wxString notebookLabelLocusTab;
    static const wxString notebookLabelRegionTab;
    static const wxString notebookLabelUnitTab;
    static const wxString noWarningsFound;
    static const wxString nuc;
    static const wxString object;
    static const wxString off;
    static const wxString on;
    static const wxString panel;
    static const wxString panelLabelName;
    static const wxString panelMemberCount;
    static const wxString panelRename;
    static const wxString panelToggle;
    static const wxString parent;
    static const wxString parentLabelName;
    static const wxString parentRename;
    static const wxString parseAbort;
    static const wxString parseDataType;
    static const wxString parseFormat;
    static const wxString parseGood;
    static const wxString parseInfo;
    static const wxString parseInfoNone;
    static const wxString parseInterleaving;
    static const wxString parseSettings;
    static const wxString parseSettingsForFile;
    static const wxString parseWarning;
    static const wxString phaseFile;
    static const wxString phenotype;
    static const wxString phylip;
    static const wxString phylipNoKalleleMsat;
    static const wxString plainLong;
    static const wxString ploidy;
    static const wxString ploidy_1;
    static const wxString ploidy_2;
    static const wxString ploidy_3;
    static const wxString ploidy_4;
    static const wxString popEditButton;
    static const wxString population;
    static const wxString populationCreate;
    static const wxString popButtonAdd;
    static const wxString popButtonMergeSelected;
    static const wxString popButtonRemoveSelected;
    static const wxString popCreateTitle;
    static const wxString popEnterNewName;
    static const wxString popLabelName;
    static const wxString popTabTitle;
    static const wxString populationExists;
    static const wxString populationForAll;
    static const wxString populationNameFromFile;
    static const wxString populationNewName;
    static const wxString populationRename;
    static const wxString populationRenameChoice;
    static const wxString populationSelect;
    static const wxString populationUse;
    static const wxString questionHeader;
    static const wxString region;
    static const wxString regionChoice;
    static const wxString regionCreate;
    static const wxString regionEditString;
    static const wxString regionEffPopSize;
    static const wxString regionEnterNewName;
    static const wxString regionExists;
    static const wxString regionForAll;
    static const wxString regionLabelDataType;
    static const wxString regionLabelEffPopSize;
    static const wxString regionLabelLength;
    static const wxString regionLabelMapFile;
    static const wxString regionLabelMapPosition;
    static const wxString regionLabelName;
    static const wxString regionLabelOffset;
    static const wxString regionLabelSamples;
    static const wxString regionLabelSites;
    static const wxString regionLengthIllegal;
    static const wxString regionLengthVsMarkers;
    static const wxString regionNameFromFile;
    static const wxString regionNameUnlinked;
    static const wxString regionNewName;
    static const wxString regionOffsetIllegal;
    static const wxString regionPositionInfo;
    static const wxString regionRename;
    static const wxString regionRenameChoice;
    // static const wxString regionSamplesPer;
    static const wxString regionSelect;
    static const wxString regionUse;
    static const wxString removeFiles;
    static const wxString removeGroupsInstructions;
    static const wxString removeGroupsTitle;
    static const wxString removeLociInstructions;
    static const wxString removeLociTitle;
    static const wxString removePopsInstructions;
    static const wxString removePopsTitle;
    static const wxString renameLinkGTitle;
    static const wxString renameLocusTitle;
    static const wxString renamePopTitle;
    static const wxString renamePopTitleFor;
    static const wxString saveFileInstructionsForMac;
    static const wxString selectAll;
    static const wxString sequential;
    static const wxString setDataTypesAll;
    static const wxString setFormats;
    static const wxString setInterleaving;
    static const wxString snp;
    static const wxString structureDump;
    // static const wxString tooFewSamplesPerIndividual;
    // static const wxString tooManySamplesPerIndividual;
    static const wxString trait;
    static const wxString traitEnterNewName;
    static const wxString trueVal;
    static const wxString typeClashDialog;
    static const wxString unknown;
    static const wxString unrecognizedFileFormat;
    static const wxString unselectAll;
    static const wxString unsetValueLocations;
    static const wxString unsetValueLocusLength;
    static const wxString unsetValueLocusPosition;
    static const wxString unsetValueOffset;
    static const wxString unsetValueRegionEffectivePopulationSize;
    static const wxString unsetValueRegionSamples;
    static const wxString usageHeader;
    static const wxString userTypeOverride;

    static const wxString warning;
    static const wxString warningMissingPopRegPair;
    static const wxString warningNeedFile;
    static const wxString warningNeedFileDataType;
    static const wxString warningNeedFileDataTypes;
    static const wxString warningNeedFileFormat;
    static const wxString warningNeedFileFormats;
    static const wxString warningNeedFilesParsed;
    static const wxString warningStringsHeader;
    static const wxString warningUnsetLinkageGroup;
    static const wxString warningUnsetLocus;
    static const wxString warningUnsetPopulation;
    static const wxString warningUnsetRegion;
    static const wxString xmlFiles;
    static const wxString yes;

    // stuff for command line parsing
    static const wxString cmdBatch;
    static const wxString cmdBatchChar;
    static const wxString cmdBatchDescription;
    static const wxString cmdCommand;
    static const wxString cmdCommandChar;
    static const wxString cmdCommandDescription;
    static const wxString cmdDump;
    static const wxString cmdDumpChar;
    static const wxString cmdDumpDescription;
    static const wxString cmdFileFormat;
    static const wxString cmdFileFormatChar;
    static const wxString cmdFileFormatDescription;
    static const wxString cmdInput;
    static const wxString cmdInputChar;
    static const wxString cmdInputDescription;
    static const wxString cmdInterleaved;
    static const wxString cmdInterleavedChar;
    static const wxString cmdInterleavedDescription;
    static const wxString cmdMapFile;
    static const wxString cmdMapFileChar;
    static const wxString cmdMapFileDescription;
    static const wxString cmdOutput;
    static const wxString cmdOutputChar;
    static const wxString cmdOutputDescription;
    static const wxString cmdWriteBatch;
    static const wxString cmdWriteBatchChar;
    static const wxString cmdWriteBatchDescription;
};

class gcverbose
{
  public:
    static const wxString addedFile;
    static const wxString addedLocus;
    static const wxString addedPopulation;
    static const wxString addedRegion;
    static const wxString addedUnit;
    static const wxString exportSuccess;
    static const wxString exportTry;
    static const wxString firstPositionNotLong;
    static const wxString locationsNotIntegers;
    static const wxString locusLengthNotLong;
    static const wxString locusPositionNotLong;
    static const wxString noSetDataType;
    static const wxString noSetFileFormat;
    static const wxString noSetIsInterleaved;
    //static const wxString noSetIsUnlinked;
    static const wxString parseAttemptExiting;
    static const wxString parseAttemptFailed;
    static const wxString parseAttemptPossible;
    static const wxString parseAttemptSettings;
    static const wxString parseAttemptStarted;
    static const wxString parseAttemptSuccessful;
    static const wxString removedFile;
    static const wxString removedLocus;
    static const wxString removedPopulation;
    static const wxString removedRegion;
    static const wxString removedUnit;
    static const wxString setDataType;
    static const wxString setDataTypeAndFileFormat;
    static const wxString setFileFormat;
    static const wxString setIsInterleaved;
    //static const wxString setIsUnlinked;
};

#endif  // GC_STRINGS_H

//____________________________________________________________________________________
