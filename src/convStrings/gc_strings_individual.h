// $Id: gc_strings_individual.h,v 1.7 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_INDIVIDUAL_H
#define GC_STRINGS_INDIVIDUAL_H

#include "wx/string.h"

class gcerr_ind
{
  public:
    static const wxString missingPhaseForLocus;
    static const wxString phaseLocusRepeat;
    static const wxString sampleLocusRepeat;
    static const wxString sampleMissingLocusData;
    static const wxString wrongSampleCount;
};

#if 0
class gcstr_individual
{
  public:
};
#endif

#endif  // GC_STRINGS_INDIVIDUAL_H

//____________________________________________________________________________________
