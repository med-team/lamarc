// $Id: gc_strings_cmdfile.cpp,v 1.13 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_cmdfile.h"
#include "wx/intl.h"

const wxString gcerr_cmdfile::atRow                 =wxTRANSLATE("Near row %d ");

const wxString gcerr_cmdfile::badCmdFile            =wxTRANSLATE("Problem reading command file %s:%s");
const wxString gcerr_cmdfile::badYesNo              =wxTRANSLATE("Did not recognize \"%s\". Allowed strings are \"yes\" and \"no\"");
const wxString gcerr_cmdfile::badFileFormat         =wxTRANSLATE("Did not recognize file format \"%s\". Allowed formats are \"phylip\" and \"migrate\"");
const wxString gcerr_cmdfile::badGeneralDataType    =wxTRANSLATE("Did not recognize general data type \"%s\". Allowed data types are \"nucleotide\" and \"allelic\"");
const wxString gcerr_cmdfile::badInterleaving       =wxTRANSLATE("Did not recognize sequence alignment type \"%s\". Allowed formats are \"interleaved\" and \"sequential\"");
const wxString gcerr_cmdfile::badProximity          =wxTRANSLATE("Did not recognize marker proximity of \"%s\". Allowed strings are \"linked\" and \"unlinked\"");
const wxString gcerr_cmdfile::badSpecificDataType   =wxTRANSLATE("Did not recognize specific data type \"%s\". Allowed data types are \"dna\", \"snp\", \"kallele\" and \"microsat\"");

const wxString gcerr_cmdfile::deprecatedGeneralDataType =wxTRANSLATE("Use of general data type \"%s\" is disallowed for infile parsing. Please update to use one of \"dna\", \"snp\", \"kallele\" or \"microsat\"");

const wxString gcerr_cmdfile::inCmdFile             =wxTRANSLATE("While processing command file \"%s\", the following error occured:\n\n");
const wxString gcerr_cmdfile::inFile                =wxTRANSLATE("in file %s" );

const wxString gcerr_cmdfile::locusMatchByNameNotEmpty=wxTRANSLATE("<segments-matching type=\"byName\"> should have no text between tag open and close.");
const wxString gcerr_cmdfile::locusMatchSingleEmpty =wxTRANSLATE("<segments-matching type=\"single\"> requres segment name between tag open and close.");
const wxString gcerr_cmdfile::locusMatchUnknown     =wxTRANSLATE("<segments-matching type=\"%s\"> unrecognized.  Allowed types are \"byName\" or \"single\".");

const wxString gcerr_cmdfile::messageIs             =wxTRANSLATE(": %s");

const wxString gcerr_cmdfile::popMatchByNameNotEmpty=wxTRANSLATE("<population-matching type=\"byName\"> should have no text between tag open and close.");
const wxString gcerr_cmdfile::popMatchSingleEmpty   =wxTRANSLATE("<population-matching type=\"single\"> requires population name between tag open and close.");
const wxString gcerr_cmdfile::popMatchUnknown       =wxTRANSLATE("population-matching type \"%s\" unrecognized");

const wxString gcstr_cmdfile::cmdFilesSelect        =wxTRANSLATE("Select a converter command file");

//____________________________________________________________________________________
