// $Id: gc_strings_locus.h,v 1.15 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_LOCUS_H
#define GC_STRINGS_LOCUS_H

#include "wx/string.h"

class gcerr_locus
{
  public:
    static const wxString dnaBigLengthNeedsLocations;
    static const wxString lengthMismatch;
    static const wxString lengthMissing;
    static const wxString locationsOutOfOrder;
    static const wxString locationTooLarge;
    static const wxString locationTooSmall;
    static const wxString mapPositionMismatch;
    static const wxString mergeFailure;
    static const wxString missing;
    static const wxString numMarkersZero;
    static const wxString offsetMismatch;
    static const wxString offsetMissingMultiSegment;
    static const wxString offsetMissingSnpLocations;
    static const wxString overlap;
    static const wxString setLocs;
    static const wxString siteCountMismatch;
    static const wxString typeMismatch;
    static const wxString unlinkedNuc;
    static const wxString unsetLinkedUserValue;
    static const wxString unsetNumMarkers;
    static const wxString unsetOffset;
    static const wxString userDataTypeMismatch;
    static const wxString wrongLocationCount;
};

#endif  // GC_STRINGS_LOCUS_H

//____________________________________________________________________________________
