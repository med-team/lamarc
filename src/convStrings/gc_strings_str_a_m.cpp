// $Id: gc_strings_str_a_m.cpp,v 1.10 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings.h"
#include "wx/intl.h"

const wxString gcstr::abandonExport     =   wxTRANSLATE("Abort Export");
const wxString gcstr::addHapFile        =   wxTRANSLATE("Select New Haplotype/Phase Resolution File");
const wxString gcstr::addLocus          =   wxTRANSLATE("Adding New Segment");
const wxString gcstr::addPanel          =   wxTRANSLATE("Adding New Panel");
const wxString gcstr::addParent         =   wxTRANSLATE("Adding New Parent");
const wxString gcstr::addPop            =   wxTRANSLATE("Adding New Population");
const wxString gcstr::addRegion         =   wxTRANSLATE("Adding New Region");
const wxString gcstr::adjacent          =   "adjacent";
const wxString gcstr::all               =   "all";
const wxString gcstr::allele            =   wxTRANSLATE("allele");
const wxString gcstr::allelic           =   "Allelic";
const wxString gcstr::allFiles          =   "*";
const wxString gcstr::assignTabTitle    =   wxTRANSLATE("Data Partitions");

const wxString gcstr::badLocusLength    =   wxTRANSLATE("Attempted to set length of segment \"%s\" to illegal value \"%s\". All lengths must be positive integers. Ignoring.");
const wxString gcstr::badLocusLength1   =   wxTRANSLATE("The following segments have suspiciously short lengths:");
const wxString gcstr::badLocusLength2   =   wxTRANSLATE("You may wish to cancel this operation and edit segment lengths in the Genomic Segments Properties tab");
const wxString gcstr::badLocusPosition  =   wxTRANSLATE("Attempted to set position of segment \"%s\" to illegal value \"%s\". All map positions must be integers. Ignoring.");
const wxString gcstr::badRegionLength1  =   wxTRANSLATE("The following regions have suspiciously short lengths:");
const wxString gcstr::badRegionLength2  =   wxTRANSLATE("You may wish to cancel this operation and edit region lengths in the Genomic Region Properties tab");
const wxString gcstr::batchFileDefault  =   "exported_batch_commands.xml";
const wxString gcstr::batchOutComment   =   wxTRANSLATE("Exported from lamarc converter %s");
const wxString gcstr::batchSafeFinish   =   wxTRANSLATE("batch converter finished successfully");
const wxString gcstr::blockFromFiles    =   wxTRANSLATE("1 block from %d files");
const wxString gcstr::blockInfo1        =   wxTRANSLATE("%d samples of type %s");
const wxString gcstr::blockInfo2        =   wxTRANSLATE("from file %s");
const wxString gcstr::blockFileInfo     =   wxTRANSLATE("data file:");
const wxString gcstr::blockLocusChoice  =   wxTRANSLATE("Choose a segment for this block:");
const wxString gcstr::blockLocusIndexInFile  =   wxTRANSLATE("Locus order within file");
const wxString gcstr::blockPopChoice    =   wxTRANSLATE("Choose a population for this block:");
const wxString gcstr::blockPopIndexInFile  =   wxTRANSLATE("Population order within file");
const wxString gcstr::blockPloidyInfo   =   wxTRANSLATE("%d %s samples\n\nChange sample ploidy in Data File panel.");
const wxString gcstr::blockPloidyTitle  =   wxTRANSLATE("Samples and Sequences");
const wxString gcstr::blocksFromFiles   =   wxTRANSLATE("%d blocks from %d files");
const wxString gcstr::buttonHide        =   wxTRANSLATE("Hide Detail");
const wxString gcstr::buttonSelectAll   =   wxTRANSLATE("Select All");
const wxString gcstr::buttonUnselectAll =   wxTRANSLATE("Unselect All");
const wxString gcstr::buttonShow        =   wxTRANSLATE("Show Detail");
const wxString gcstr::byfile            =   "by file";
const wxString gcstr::byprog            =   "by program";
const wxString gcstr::byuser            =   "by user";
const wxString gcstr::cancelString      =   wxTRANSLATE("Cancel");
const wxString gcstr::childPopsInstructions =   wxTRANSLATE("Child Populations:");
const wxString gcstr::chooseHapResolution = wxTRANSLATE("Choose Haplotype Resolution");
const wxString gcstr::chooseOne         =   wxTRANSLATE("Choose one");
const wxString gcstr::chooseOneGroup    =   wxTRANSLATE("Choose one Region");
const wxString gcstr::chooseOneLocus    =   wxTRANSLATE("Choose one Segment");
const wxString gcstr::chooseOneParse    =   wxTRANSLATE("Choose one Parse");
const wxString gcstr::chooseOnePop      =   wxTRANSLATE("Choose one Population");
const wxString gcstr::continueExport    =   wxTRANSLATE("Export Without Locations");
const wxString gcstr::continueString    =   wxTRANSLATE("Export");
const wxString gcstr::cannotWrite       =   wxTRANSLATE("Unable to write file \"%s\"");
const wxString gcstr::chooseDataType    =   wxTRANSLATE("Choose");
const wxString gcstr::chooseFileType    =   wxTRANSLATE("Choose");
const wxString gcstr::converterInfo     =   wxTRANSLATE("A GUI version of the LAMARC converter.\n\nlamarc@u.washington.edu\n\nhttp://evolution.gs.washington.edu/lamarc/\n\nCopyright 2009 Mary K. Kuhner, Peter Beerli, Joe Felsenstein, Bob Giansiracusa, James R. McGill, Eric Rynes, Lucian Smith, Elizabeth A. Walkup, Jon Yamato");
const wxString gcstr::converterTitle    =   wxTRANSLATE("LAMARC File Converter");
const wxString gcstr::createNewRegion   =   wxTRANSLATE("place in its own region");
const wxString gcstr::createParent2Child =   wxTRANSLATE("Pick the 2 children of this parent");
const wxString gcstr::createParentFirst2Children = wxTRANSLATE("Create parent of first 2 children");
const wxString gcstr::createParentNext2Children = wxTRANSLATE("Create parent of next 2 children");
const wxString gcstr::dataBlocks        =   wxTRANSLATE("parsed data");
const wxString gcstr::dataFileBatchExport=  wxTRANSLATE("Select data file to write batch commands to");
const wxString gcstr::dataFileButtonAdd =   wxTRANSLATE("Add");
const wxString gcstr::dataFileButtonAllSelect =   wxTRANSLATE("Select All");
const wxString gcstr::dataFileButtonAllUnselect =   wxTRANSLATE("Unselect All");
const wxString gcstr::dataFileButtonRemoveSelected =   wxTRANSLATE("Remove Selected");
const wxString gcstr::dataFileExport    =   wxTRANSLATE("Select data file to write");
const wxString gcstr::dataFiles         =   "Phylip and Migrate files (*.phy;*.mig)|*.phy;*.mig|All Files (*)|*";
const wxString gcstr::dataFilesInstructions   =   wxTRANSLATE("Add a file using the button above.");
const wxString gcstr::dataFilesSelect   =   wxTRANSLATE("Select data files to read");
const wxString gcstr::dataFilesTitle    =   wxTRANSLATE("Information on %d Data Files");
const wxString gcstr::dataType          =   wxTRANSLATE("Data Type");
const wxString gcstr::divergence        =   wxTRANSLATE("Divergence");
const wxString gcstr::divergeInstructions =   wxTRANSLATE("Pick Children:");
const wxString gcstr::divergenceSelect  =   wxTRANSLATE("Select 2 child Populations");
const wxString gcstr::divMigMatrix      =   wxTRANSLATE("Diverge_Mig");
const wxString gcstr::dna               =   "DNA";
const wxString gcstr::doneThanks        =   wxTRANSLATE("Operation completed.");
const wxString gcstr::editApply         =   wxTRANSLATE("Apply");
const wxString gcstr::editCancel        =   wxTRANSLATE("Cancel");
const wxString gcstr::editDelete        =   wxTRANSLATE("Delete");
const wxString gcstr::editFileSettings  =   wxTRANSLATE("Edit/Review Settings for file \"%s\"");
const wxString gcstr::editLocus         =   wxTRANSLATE("Edit segment \"%s\"");
const wxString gcstr::editMigration     =   wxTRANSLATE("Edit migration into \"%s\" from \"%s\"");
const wxString gcstr::editOK            =   wxTRANSLATE("OK");
const wxString gcstr::editParseBlock    =   wxTRANSLATE("Edit parsed data properties");
const wxString gcstr::editPanel         =   wxTRANSLATE("Edit panel \"%s\"");
const wxString gcstr::editParent        =   wxTRANSLATE("Edit parent \"%s\"");
const wxString gcstr::editPop           =   wxTRANSLATE("Edit population \"%s\"");
const wxString gcstr::editRegion        =   wxTRANSLATE("Edit Region \"%s\"");
const wxString gcstr::enterNewName      =   wxTRANSLATE("Enter new name");
const wxString gcstr::error             =   wxTRANSLATE("ERROR");
const wxString gcstr::errorWrap         =   wxTRANSLATE("Error Type \"%s\" for file \"%s\":\n%s");
const wxString gcstr::errorWrapNoFile   =   wxTRANSLATE("Error Type \"%s\":\n%s");
const wxString gcstr::exportFileDefault =   "infile.xml";
const wxString gcstr::exportFileGlob    =   "Lamarc files (*.xml)|*.xml|All files (*)|*";
const wxString gcstr::exportWarning     =   wxTRANSLATE("Warning: export may not be correct");
const wxString gcstr::falseVal          =   wxTRANSLATE("false");
const wxString gcstr::fileAlreadyAdded      =   wxTRANSLATE("Cannot add file \"%s\". It is already added.");
const wxString gcstr::fileDelete            =   wxTRANSLATE("Delete");
const wxString gcstr::fileEmpty             =   wxTRANSLATE("File \"%s\" appears to be empty.");
const wxString gcstr::fileLabelDataType     =   wxTRANSLATE("Data Type");
const wxString gcstr::fileLabelFormat       =   wxTRANSLATE("File Format");
const wxString gcstr::fileLabelInterleaving =   wxTRANSLATE("Sequence Alignment");
const wxString gcstr::fileLabelName         =   wxTRANSLATE("File Name");
const wxString gcstr::fileLabelRemove       =   wxTRANSLATE("Remove File");

//const wxString gcstr::fileLabelUnlinked     =   wxTRANSLATE("Unlink Microsats");
const wxString gcstr::fileSetting           =   wxTRANSLATE("phase grouping set at file level");
const wxString gcstr::firstPositionScanned  =   wxTRANSLATE("First Position Scanned");
const wxString gcstr::fragmentRegion        =   wxTRANSLATE("Unlink all Segments in this Region");
const wxString gcstr::fullPath              =   wxTRANSLATE("Full Path to File:");
const wxString gcstr::genoFileDefault   =   wxTRANSLATE("No trait resolution set for this file");
const wxString gcstr::globAll               =   "*";
const wxString gcstr::hapFileEmptyFirstLine =   wxTRANSLATE("No data found in first line of phase information file \"%s\"");
const wxString gcstr::hapFileBarf       =   wxTRANSLATE("Implementation error reading phase information file \"%s\"");
const wxString gcstr::hapFileDefault    =   wxTRANSLATE("Use default haplotype and phase resolution");
const wxString gcstr::hapFileMissing    =   wxTRANSLATE("Phase information file \"%s\" missing or un-readable");
const wxString gcstr::hapFileParseFailed=   wxTRANSLATE("Parse of phase information file \"%s\" failed:%s");
const wxString gcstr::hapFileToken1     =   wxTRANSLATE("First token of phase information file \"%s\" was \"%s\" but expected an integer");
const wxString gcstr::hapFileToken2     =   wxTRANSLATE("Second token of phase information file \"%s\" was \"%s\" but expected \"adjacent\" or end of line");
const wxString gcstr::hapFileToken3     =   wxTRANSLATE("Third token of phase information file \"%s\" was \"%s\" but expected an integer");
const wxString gcstr::hapFileToken3Missing =   wxTRANSLATE("Third token of phase information file \"%s\" missing");
const wxString gcstr::hapFileReuse      =   wxTRANSLATE("Attempt to add phase information file \"%s\" a second time");
const wxString gcstr::hapFileSelect     =   wxTRANSLATE("select phase information file");
const wxString gcstr::hapFileSelectAnother  =   wxTRANSLATE("file not listed here");
const wxString gcstr::hapFileUnSelect   =   wxTRANSLATE("un-select current file");
const wxString gcstr::hapFilesSelect    =   wxTRANSLATE("Select and apply file(s) with haplotype correspondence information.");
const wxString gcstr::havePatience      =   wxTRANSLATE("About to start a lengthy operation. Please wait.");
const wxString gcstr::hiddenContent     =   wxTRANSLATE("IMPLEMENTATION ERROR -- this should be a hidden object");
const wxString gcstr::indent            =   "  ";
const wxString gcstr::information       =   wxTRANSLATE("Information");
const wxString gcstr::instructionsMultipleDataTypes=wxTRANSLATE("The \"datatype\" attribute for the above infile tag should be edited to list only one datatype.");
const wxString gcstr::interleaved       =   wxTRANSLATE("Interleaved");
const wxString gcstr::interleavedNoKalleleMsat  =   wxTRANSLATE("Interleaved format not compatible with Kallele or Microsat data (file \"%s\"");
const wxString gcstr::kallele           =   "Kallele";
const wxString gcstr::linkGCreateTitle  =   wxTRANSLATE("Create a New Region");
const wxString gcstr::linkGEnterNewName=   wxTRANSLATE("Enter the name of the new region");
const wxString gcstr::linkageCaption   =   wxTRANSLATE("Linkage:");
const wxString gcstr::linkageNo        =   wxTRANSLATE("unlinked");
const wxString gcstr::linkageYes       =   wxTRANSLATE("linked");
const wxString gcstr::locations         =   wxTRANSLATE("Locations of sampled markers");
const wxString gcstr::locationsForRecom =wxTRANSLATE("Segment \"%s\" will need its locations item set if you wish to estimate recombination in Lamarc.\n\nDo you wish to continue your file export?");

const wxString gcstr::lociTabTitle      =   wxTRANSLATE("Properties of %d Segments");
const wxString gcstr::locus             =   wxTRANSLATE("contiguous segment");
const wxString gcstr::locusButtonAdd    =   wxTRANSLATE("Add");
const wxString gcstr::locusButtonMergeSelected    =   wxTRANSLATE("Merge Selected");
const wxString gcstr::locusButtonRemoveSelected    =   wxTRANSLATE("Remove Selected");
const wxString gcstr::locusCreate       =   wxTRANSLATE("Create new Segment");
const wxString gcstr::locusCreateTitle  =   wxTRANSLATE("Create new Segment");
const wxString gcstr::locusDialogMapPosition=   wxTRANSLATE("Map Position in Region:");
const wxString gcstr::locusEditString  =   wxTRANSLATE("Edit Segments");
const wxString gcstr::locusEnterNewName=   wxTRANSLATE("Enter the name of the new segment");
const wxString gcstr::locusExists       =   wxTRANSLATE("Segment \"%s\" already exists.");
const wxString gcstr::locusForAll      =   wxTRANSLATE("Set All Units to One Segment");
const wxString gcstr::locusLabelDataType   =   wxTRANSLATE("data type:%s");
const wxString gcstr::locusLabelLength     =   wxTRANSLATE("total length:%s");
const wxString gcstr::locusLabelLinked     =   wxTRANSLATE("sites linked:%s");
const wxString gcstr::locusLabelMapFile    =   wxTRANSLATE("Map Position File (optional)");
const wxString gcstr::locusLabelMapPosition=   wxTRANSLATE("position in region:%s");
const wxString gcstr::locusLabelName       =   wxTRANSLATE("name:%s");
const wxString gcstr::locusLabelOffset     =   wxTRANSLATE("Offset");
const wxString gcstr::locusLabelRegionName =   wxTRANSLATE("region:%s");
const wxString gcstr::locusLabelSites      =   wxTRANSLATE("# sites:%s");
const wxString gcstr::locusLength          =   wxTRANSLATE("Total Length");
const wxString gcstr::locusLengthIllegal   =   wxTRANSLATE("Illegal value \"%s\" for length of segment \"%s\". Ignoring.");
const wxString gcstr::locusLengthVsMarkers =   wxTRANSLATE("Segment length (%ld) must be > number of markers (%ld). Ignoring.");
const wxString gcstr::locusNameFromFile    =   wxTRANSLATE("segment %d of %s");
const wxString gcstr::locusNewName =    wxTRANSLATE("Name for New Segment");
const wxString gcstr::locusOffsetIllegal   =   wxTRANSLATE("Illegal value \"%s\" for length of segment \"%s\". Ignoring.");
const wxString gcstr::locusOwnRegion    =   wxTRANSLATE("Place this segment in its own region");
const wxString gcstr::locusRename       =   wxTRANSLATE("Rename Segment");
const wxString gcstr::locusRenameChoice =   wxTRANSLATE("Segment to Rename");
const wxString gcstr::locusSelect       =   wxTRANSLATE("Select a Segment");
const wxString gcstr::locusMarkerCount  =   wxTRANSLATE("Number of Markers");
const wxString gcstr::locusUnlinked     =   wxTRANSLATE("unlinked markers");
const wxString gcstr::locusUse          =   wxTRANSLATE("Use this Segment");
const wxString gcstr::logWindowHeader   =   wxTRANSLATE("Log Window for Lamarc Converter. Use menu item \"View > Log Verbosely\" to change verbosity.");
const wxString gcstr::mapFileExportFailed   =   wxTRANSLATE("File export failed while reading data for region \"%s\". \n\nThe error was \"%s:%s\n\nPlease check your settings for that region and please report the error.");
const wxString gcstr::mapFileLastPositionTooLate =
    wxTRANSLATE("Map position file \"%s\" has last position of %ld, but region \"%s\" length (%ld) and first position sequenced (%ld) allow last position of %ld or less only");
const wxString gcstr::mapFileMarkerPositionMismatch =
    wxTRANSLATE("Map position file \"%s\" has %ld distinct positions, but region \"%s\" requires at least %ld");
const wxString gcstr::mapFileOffsetAfterFirstPosition =
    wxTRANSLATE("Map position file \"%s\" has first position %ld, which is before %ld, the start of sequencing in region \"%s\"");
const wxString gcstr::mapFileRegionMissing  =   wxTRANSLATE("Cannot export file because map position file \"%s\" has no data for region \"%s\"");
const wxString gcstr::mapFileSelect     =   wxTRANSLATE("select map position file");
const wxString gcstr::mapFileSelectAnother  =   wxTRANSLATE("file not listed here");
const wxString gcstr::mapFileUnSelect   =   wxTRANSLATE("un-select current file");
const wxString gcstr::mapFilesSelect    =   wxTRANSLATE("Select and apply file(s) with data sample spacing information.");
const wxString gcstr::mapPositionUnset  =   wxTRANSLATE("<unset map position>");
const wxString gcstr::members           =   wxTRANSLATE("members: %s");
const wxString gcstr::mergeLinkGTitle        =   wxTRANSLATE("Combine Regions");
const wxString gcstr::mergeLinkGInstructions =   wxTRANSLATE("Merge with selected Regions:");
const wxString gcstr::mergeLociTitle        =   wxTRANSLATE("Combine Segments");
const wxString gcstr::mergeLociInstructions =   wxTRANSLATE("Merge with selected Segments:");
const wxString gcstr::mergePopsTitle        =   wxTRANSLATE("Combine Populations");
const wxString gcstr::mergePopsInstructions =   wxTRANSLATE("Merge with selected Populations:");
const wxString gcstr::microsat          =   "Microsat";
const wxString gcstr::migrate           =   "Migrate";
const wxString gcstr::migrationMatrix   =   wxTRANSLATE("Migration Matrix");
const wxString gcstr::migConstraint     =   wxTRANSLATE("Constraint: %s");
const wxString gcstr::migMethod         =   wxTRANSLATE("Method: %s");
const wxString gcstr::migProfile        =   wxTRANSLATE("Profile: %s");
const wxString gcstr::migRate           =   wxTRANSLATE("Migration Rate: %s");
const wxString gcstr::migLabelConstraint =  wxTRANSLATE("Constraint");
const wxString gcstr::migLabelMethod    =  wxTRANSLATE("Method");
const wxString gcstr::migLabelProfile   =  wxTRANSLATE("Profile");
const wxString gcstr::migLabelRate      =  wxTRANSLATE("Migration Rate");
const wxString gcstr::moot              =   wxTRANSLATE("Moot");
const wxString gcstr::moveLocus         =   wxTRANSLATE("Moving segment %s out of region %s -- segments with unlinked data do not belong in multi-segment regions.");
const wxString gcstr::multiPhaseSample  =   wxTRANSLATE("induced by microsat/kallele file");

//____________________________________________________________________________________
