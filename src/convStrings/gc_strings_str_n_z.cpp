// $Id: gc_strings_str_n_z.cpp,v 1.8 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings.h"
#include "wx/intl.h"

const wxString gcstr::nameCandidate     =   "%s_%d";
const wxString gcstr::nearRow           =   wxTRANSLATE("Near row %d: %s");
const wxString gcstr::newName           =   wxTRANSLATE("New name");
const wxString gcstr::no                =   "no";
const wxString gcstr::noChoice          =   wxTRANSLATE("No other objects to select");
const wxString gcstr::noChoiceLocus     =   wxTRANSLATE("No other segments to select");
const wxString gcstr::noChoicePopulation=   wxTRANSLATE("No other populations to select");
const wxString gcstr::noChoiceRegion    =   wxTRANSLATE("No other regions to select");
const wxString gcstr::noWarningsFound   =   wxTRANSLATE("No warning messages were found. You should be able to export a lamarc file now");
const wxString gcstr::nuc               =   "Nucleotide";
const wxString gcstr::object            =   "object";
const wxString gcstr::off               =   wxTRANSLATE("Off");
const wxString gcstr::on                =   wxTRANSLATE("On");
const wxString gcstr::panel             =   "panel";
const wxString gcstr::panelLabelName    =   wxTRANSLATE("name: %s");
const wxString gcstr::panelMemberCount  =   wxTRANSLATE("Number of Members");
const wxString gcstr::panelRename       =   wxTRANSLATE("Rename Panel");
const wxString gcstr::panelToggle       =   wxTRANSLATE("Use Panels");
const wxString gcstr::parseAbort        =   wxTRANSLATE("Aborted    parse of %s(%s:%s,%s):%s");
const wxString gcstr::parseDataType     =   wxTRANSLATE("data type:%s");
const wxString gcstr::parseFormat       =   wxTRANSLATE("format:%s");
const wxString gcstr::parseGood         =   wxTRANSLATE("Successful parse of %s(%s:%s,%s)");
const wxString gcstr::parseInfo         =   wxTRANSLATE("Parsing Information from File:");
const wxString gcstr::parseInfoNone     =   wxTRANSLATE("Unable to parse file.\n\nPlease check you have a file in correct Phylip or Migrate format");
const wxString gcstr::parent            =   "parent";
const wxString gcstr::parentLabelName   =   wxTRANSLATE("name: %s");
const wxString gcstr::parentRename      =   wxTRANSLATE("Rename Parent");
const wxString gcstr::parseInterleaving =   wxTRANSLATE("interleaving:%s");
const wxString gcstr::parseSettings     =   wxTRANSLATE("%s;%s;%s");
const wxString gcstr::parseSettingsForFile= wxTRANSLATE("%s parsed with %s");
const wxString gcstr::parseWarning      =   wxTRANSLATE("Conflict while parsing file");
const wxString gcstr::phaseFile         =   wxTRANSLATE("phase grouping set with phase resolution file");
const wxString gcstr::phenotype         =   wxTRANSLATE("phenotype");
const wxString gcstr::phylip            =   "Phylip";
const wxString gcstr::phylipNoKalleleMsat       =   wxTRANSLATE("Phylip format not compatible with Kallele or Microsat data (file \"%s\"");
const wxString gcstr::plainLong         =   "%ld";
const wxString gcstr::ploidy            =   "%ld-ploid";
const wxString gcstr::ploidy_1          =   wxTRANSLATE("haploid");
const wxString gcstr::ploidy_2          =   wxTRANSLATE("diploid");
const wxString gcstr::ploidy_3          =   wxTRANSLATE("triploid");
const wxString gcstr::ploidy_4          =   wxTRANSLATE("tetraploid");
const wxString gcstr::popEditButton     =   wxTRANSLATE("Edit");
const wxString gcstr::population        =   wxTRANSLATE("population");
const wxString gcstr::populationExists  =   wxTRANSLATE("Population \"%s\" already exists.");
const wxString gcstr::populationCreate  =   wxTRANSLATE("Create new Population");
const wxString gcstr::popButtonAdd    =   wxTRANSLATE("Add");
const wxString gcstr::popButtonMergeSelected    =   wxTRANSLATE("Merge Selected");
const wxString gcstr::popButtonRemoveSelected    =   wxTRANSLATE("Remove Selected");
const wxString gcstr::popCreateTitle    =   wxTRANSLATE("Create a New Population");
const wxString gcstr::popEnterNewName   =   wxTRANSLATE("New name:");
const wxString gcstr::popLabelName      =   wxTRANSLATE("name: %s");
const wxString gcstr::popTabTitle       =   wxTRANSLATE("Properties of %d Populations");
const wxString gcstr::populationNameFromFile    =   wxTRANSLATE("pop %d of %s");
const wxString gcstr::populationNewName =   wxTRANSLATE("Name for New Population");
const wxString gcstr::populationRename          =   wxTRANSLATE("Rename Population");
const wxString gcstr::populationRenameChoice    =   wxTRANSLATE("Population to Rename");
const wxString gcstr::populationSelect  =   wxTRANSLATE("Select a Population");
const wxString gcstr::populationUse     =   wxTRANSLATE("Use this Population");
const wxString gcstr::populationForAll  =   wxTRANSLATE("Set All Units to One Population");
const wxString gcstr::questionHeader    =   wxTRANSLATE("Warning: possible problem detected");
const wxString gcstr::region            =   wxTRANSLATE("region");
const wxString gcstr::regionChoice      =   wxTRANSLATE("Assign to Region:");
const wxString gcstr::regionCreate      =   wxTRANSLATE("Create new Region");
const wxString gcstr::regionEditString  =   wxTRANSLATE("Edit Genomic Regions");
const wxString gcstr::regionEffPopSize  =   wxTRANSLATE("Effective Population Size:");
const wxString gcstr::regionEnterNewName=   wxTRANSLATE("Enter the name of the new region");
const wxString gcstr::regionExists      =   wxTRANSLATE("Region \"%s\" already exists.");
const wxString gcstr::regionForAll      =   wxTRANSLATE("Set All Units to One Genomic Region");
const wxString gcstr::regionLabelDataType   =   wxTRANSLATE("Data Type");
const wxString gcstr::regionLabelEffPopSize =   wxTRANSLATE("effective population size:");
const wxString gcstr::regionLabelLength     =   wxTRANSLATE("Length");
const wxString gcstr::regionLabelMapFile    =   wxTRANSLATE("Map Position File (optional)");
const wxString gcstr::regionLabelMapPosition=   wxTRANSLATE("Map Position");
const wxString gcstr::regionLabelName       =   wxTRANSLATE("name: %s");
const wxString gcstr::regionLabelOffset     =   wxTRANSLATE("First Position Sequenced");
const wxString gcstr::regionLabelSamples    =   wxTRANSLATE("samples per individual:");
const wxString gcstr::regionLabelSites      =   wxTRANSLATE("Sites");
const wxString gcstr::regionLengthIllegal   =   wxTRANSLATE("Illegal value \"%s\" for length of region \"%s\". Ignoring.");
const wxString gcstr::regionLengthVsMarkers =   wxTRANSLATE("Region length (%ld) must be > number of markers (%ld). Ignoring.");
const wxString gcstr::regionNameFromFile    =   wxTRANSLATE("from %s");
const wxString gcstr::regionNameUnlinked    =   wxTRANSLATE("%s_%08ld");
const wxString gcstr::regionNewName     =   wxTRANSLATE("Name for New Region");
const wxString gcstr::regionOffsetIllegal   =   wxTRANSLATE("Illegal value \"%s\" for length of region \"%s\". Ignoring.");
const wxString gcstr::regionPositionInfo    =   wxTRANSLATE("Position info for segments:");
const wxString gcstr::regionRename      =   wxTRANSLATE("Rename Region");
const wxString gcstr::regionRenameChoice=   wxTRANSLATE("Region to Rename");
const wxString gcstr::regionSelect      =   wxTRANSLATE("Select a Region");
const wxString gcstr::regionUse         =   wxTRANSLATE("Use this Region");
const wxString gcstr::removeFiles       =   wxTRANSLATE("Remove Files");
const wxString gcstr::removeGroupsTitle        =   wxTRANSLATE("Remove Regions");
const wxString gcstr::removeGroupsInstructions =   wxTRANSLATE("Choose one or more");
const wxString gcstr::removeLociTitle        =   wxTRANSLATE("Remove Segments");
const wxString gcstr::removeLociInstructions =   wxTRANSLATE("Choose one or more");
const wxString gcstr::removePopsTitle        =   wxTRANSLATE("Remove Populations");
const wxString gcstr::removePopsInstructions =   wxTRANSLATE("Choose one or more");
const wxString gcstr::renameLinkGTitle  =   wxTRANSLATE("Rename a Region");
const wxString gcstr::renameLocusTitle  =   wxTRANSLATE("Rename Segment");
const wxString gcstr::renamePopTitle    =   wxTRANSLATE("Rename Population");
const wxString gcstr::renamePopTitleFor =   wxTRANSLATE("Rename Population %s");
const wxString gcstr::saveFileInstructionsForMac    =   wxTRANSLATE(" (button to right of file name shows/hides directory information)");
const wxString gcstr::selectAll         =   wxTRANSLATE("Select All");
const wxString gcstr::sequential        =   wxTRANSLATE("Sequential");
const wxString gcstr::setDataTypesAll   =   wxTRANSLATE("Set Data Type for all Files");
const wxString gcstr::setFormats        =   wxTRANSLATE("Set Formats for Files");
const wxString gcstr::setInterleaving   =   wxTRANSLATE("Set Interleaving for Files");
const wxString gcstr::structureDump     =   wxTRANSLATE("%sStructures dump:");
const wxString gcstr::trait             =   wxTRANSLATE("trait");
const wxString gcstr::traitEnterNewName=   wxTRANSLATE("Enter the name of the new trait class");
const wxString gcstr::trueVal           =   wxTRANSLATE("true");
const wxString gcstr::typeClashDialog   =   wxTRANSLATE("File \"%s\" specifies data type \"%s\", but you have specified \"%s\".\n\nPlease specify which one you want.");
const wxString gcstr::unselectAll       =   wxTRANSLATE("Unselect All");
const wxString gcstr::unsetValueLocations       =   wxTRANSLATE("<enter an ordered list of integers>");
const wxString gcstr::unsetValueLocusLength     =   wxTRANSLATE("<enter a positive integer>");
const wxString gcstr::unsetValueLocusPosition   =   wxTRANSLATE("<enter an integer>");
const wxString gcstr::unsetValueOffset          =   wxTRANSLATE("<enter an integer>");
const wxString gcstr::unsetValueRegionEffectivePopulationSize   =   wxTRANSLATE("<enter a positive number>");
const wxString gcstr::unsetValueRegionSamples   =   wxTRANSLATE("<enter a positive integer>");
const wxString gcstr::snp                       =   "SNP";
const wxString gcstr::warning                   =   wxTRANSLATE("Warning");
const wxString gcstr::warningMissingPopRegPair  =   wxTRANSLATE("Missing data sample covering (%s,%s) pair");
const wxString gcstr::warningNeedFile           =   wxTRANSLATE("Add one or more files");
const wxString gcstr::warningNeedFileDataType   =   wxTRANSLATE("File \"%s\" needs its data type set");
const wxString gcstr::warningNeedFileDataTypes  =   wxTRANSLATE("At least one file needs its data type set");
const wxString gcstr::warningNeedFileFormat     =   wxTRANSLATE("File \"%s\" needs its file format set");
const wxString gcstr::warningNeedFileFormats    =   wxTRANSLATE("At least one file needs its file format set");
const wxString gcstr::warningNeedFilesParsed    =   wxTRANSLATE("At least one file has not been parsed.");
const wxString gcstr::warningStringsHeader      =   wxTRANSLATE("You will need to fix the following things before you can create a Lamarc output file:");
const wxString gcstr::warningUnsetLinkageGroup  =   wxTRANSLATE("At least one data unit has no region assigned");
const wxString gcstr::warningUnsetLocus         =   wxTRANSLATE("At least one data unit has no segment assigned");
const wxString gcstr::warningUnsetPopulation    =   wxTRANSLATE("At least one data unit has no population assigned");
const wxString gcstr::warningUnsetRegion        =   wxTRANSLATE("At least one data unit has no region assigned");
const wxString gcstr::unknown                   =      "???";
const wxString gcstr::unrecognizedFileFormat    =   wxTRANSLATE("Did not recognize file format \"%s\". Setting to \"none\"");
const wxString gcstr::usageHeader               =   wxTRANSLATE("**********************************************************************\nInput File Conversion Program for LAMARC Version %s\n**********************************************************************");
const wxString gcstr::userTypeOverride          =   wxTRANSLATE("Overriding type %s in file %s to user-specified choice of %s");
const wxString gcstr::xmlFiles                  =   "XML Files (*.xml)|*.xml|All Files (*)|*";
const wxString gcstr::yes                       =   "yes";

const wxString gcstr::cmdBatch                   =   "batchonly";
const wxString gcstr::cmdBatchChar               =   "b";
const wxString gcstr::cmdBatchDescription        =   wxTRANSLATE("run in batch mode and exit immediately.");
const wxString gcstr::cmdCommand                 =   "commandfile";
const wxString gcstr::cmdCommandChar             =   "c";
const wxString gcstr::cmdCommandDescription      =   wxTRANSLATE("command file for batch run; see documentation");
const wxString gcstr::cmdDump                    =   "dump";
const wxString gcstr::cmdDumpChar                =   "d";
const wxString gcstr::cmdDumpDescription         =   wxTRANSLATE("do debug dump just before exiting");
const wxString gcstr::cmdFileFormat              =   "format";
const wxString gcstr::cmdFileFormatChar          =   "f";
const wxString gcstr::cmdFileFormatDescription   =   wxTRANSLATE("input file format (MIGRATE or PHYLIP)");
const wxString gcstr::cmdInput                   =   "inputfile";
const wxString gcstr::cmdInputChar               =   "i";
const wxString gcstr::cmdInputDescription        =   wxTRANSLATE("input file");
const wxString gcstr::cmdInterleaved             =   "interleaved";
const wxString gcstr::cmdInterleavedChar         =   "n";
const wxString gcstr::cmdInterleavedDescription  =   wxTRANSLATE("input file is in interleaved format");
const wxString gcstr::cmdMapFile                 =   "mapfile";
const wxString gcstr::cmdMapFileChar             =   "m";
const wxString gcstr::cmdMapFileDescription      =   wxTRANSLATE("map file");
const wxString gcstr::cmdOutput                  =   "outputfile";
const wxString gcstr::cmdOutputChar              =   "o";
const wxString gcstr::cmdOutputDescription       =   wxTRANSLATE("output file; default is \"%s\"");
const wxString gcstr::cmdWriteBatch              =   "writebatch";
const wxString gcstr::cmdWriteBatchChar          =   "w";
const wxString gcstr::cmdWriteBatchDescription   =   wxTRANSLATE("write batch file for data state at end of run");

const wxString gcverbose::addedFile             = wxTRANSLATE("Added file \"%s\"");
const wxString gcverbose::addedLocus            = wxTRANSLATE("Added segment \"%s\"");
const wxString gcverbose::addedPopulation       = wxTRANSLATE("Added population \"%s\"");
const wxString gcverbose::addedRegion           = wxTRANSLATE("Added region \"%s\"");
const wxString gcverbose::addedUnit             = wxTRANSLATE("Added unit \"%s\"");
const wxString gcverbose::exportSuccess         = wxTRANSLATE("Successful export to file \"%s\"");
const wxString gcverbose::exportTry             = wxTRANSLATE("Attempting export to file \"%s\"");
const wxString gcverbose::firstPositionNotLong  = wxTRANSLATE("Ignoring first scanned position of \"%s\" for segment \"%s\". It should have been an integer.");
const wxString gcverbose::locationsNotIntegers  = wxTRANSLATE("Ignoring locations of \"%s\" for segment \"%s\". It should have been an ordered list of integers.");
const wxString gcverbose::locusLengthNotLong    = wxTRANSLATE("Ignoring length of \"%s\" for segment \"%s\". It should have been a positive integer.");
const wxString gcverbose::locusPositionNotLong  = wxTRANSLATE("Ignoring starting position of \"%s\" for segment \"%s\".  It should have been an integer.");
const wxString gcverbose::noSetDataType         = wxTRANSLATE("Failed to set data type of file \"%s\" to \"%s\"");
const wxString gcverbose::noSetFileFormat       = wxTRANSLATE("Failed to set format of file \"%s\" to \"%s\"");
const wxString gcverbose::noSetIsInterleaved    = wxTRANSLATE("Failed to set interleaving of file \"%s\" to \"%s\"");
const wxString gcverbose::parseAttemptExiting   = wxTRANSLATE("Exiting call to parse file \"%s\"");
const wxString gcverbose::parseAttemptFailed    = wxTRANSLATE("Failed to parse file \"%s\"");
const wxString gcverbose::parseAttemptPossible  = wxTRANSLATE("Entering call to parse file \"%s\"");
const wxString gcverbose::parseAttemptSettings  = wxTRANSLATE("Setting values following parse file \"%s\"");
const wxString gcverbose::parseAttemptStarted   = wxTRANSLATE("Starting to parse file \"%s\"");
const wxString gcverbose::parseAttemptSuccessful= wxTRANSLATE("Succeeded parsing file \"%s\"");
const wxString gcverbose::removedFile           = wxTRANSLATE("Removed file \"%s\"");
const wxString gcverbose::removedLocus          = wxTRANSLATE("Removed segment \"%s\"");
const wxString gcverbose::removedPopulation     = wxTRANSLATE("Removed population \"%s\"");
const wxString gcverbose::removedRegion         = wxTRANSLATE("Removed region \"%s\"");
const wxString gcverbose::removedUnit           = wxTRANSLATE("Removed unit \"%s\"");
const wxString gcverbose::setDataType           = wxTRANSLATE("Set data type of file \"%s\" to \"%s\"");
const wxString gcverbose::setDataTypeAndFileFormat  = wxTRANSLATE("Set data type of file \"%s\" to \"%s\" and therefore file format to \"%s\"");
const wxString gcverbose::setFileFormat         = wxTRANSLATE("Set format of file \"%s\" to \"%s\"");
const wxString gcverbose::setIsInterleaved      = wxTRANSLATE("Set interleaving of file \"%s\" to \"%s\"");

//____________________________________________________________________________________
