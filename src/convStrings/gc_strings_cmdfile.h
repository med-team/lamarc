// $Id: gc_strings_cmdfile.h,v 1.11 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_STRINGS_CMDFILE_H
#define GC_STRINGS_CMDFILE_H

#include "wx/string.h"

class gcerr_cmdfile
{
  public:
    static const wxString atRow;

    static const wxString badCmdFile;
    static const wxString badFileFormat;
    static const wxString badGeneralDataType;
    static const wxString badInterleaving;
    static const wxString badProximity;
    static const wxString badSpecificDataType;
    static const wxString badYesNo;

    static const wxString deprecatedGeneralDataType;

    static const wxString inCmdFile;
    static const wxString inFile;

    static const wxString locusMatchByNameNotEmpty;
    static const wxString locusMatchSingleEmpty;
    static const wxString locusMatchUnknown;

    static const wxString messageIs;

    static const wxString popMatchByNameNotEmpty;
    static const wxString popMatchSingleEmpty;
    static const wxString popMatchUnknown;
};

class gcstr_cmdfile
{
  public:
    static const wxString cmdFilesSelect;
};

#endif  // GC_STRINGS_CMDFILE_H

//____________________________________________________________________________________
