// $Id: gc_strings_infile.cpp,v 1.17 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_infile.h"
#include "wx/intl.h"

const wxString gcerr_infile::extraFileData   = wxTRANSLATE("File contains extra data");
const wxString gcerr_infile::fileParseError  = wxTRANSLATE("Near line %d: %s.");
const wxString gcerr_infile::illegalDna      = wxTRANSLATE("Illegal character \"%c\" at position %d in nucleotide sequence \"%s\".");
const wxString gcerr_infile::illegalMsat     = wxTRANSLATE("Illegal microsattelite \"%s\".");
const wxString gcerr_infile::parseDataTypeSpecMismatch=wxTRANSLATE("File specifies data of type %s during attempt to parse as %s data.");
const wxString gcerr_infile::parseMissingErr =wxTRANSLATE("File \"%s\" did not parse correctly. \n\nIt may be broken or may have conflicted with a command file. \n\nPlease remove and replace it before trying again.");
const wxString gcerr_infile::prematureEndOfFile=wxTRANSLATE("File \"%s\" ended prematurely.");
const wxString gcerr_infile::shortSampleName=wxTRANSLATE("File \"%s\" has at least one sample name containing an embedded space.\n\nThis is often a symptom that your input file was incorrectly formatted. (Phylip and Migrate use the first ten characters of a line for the sample name.)\n\nYou may wish to remove this file from the converter, edit it, and re-add it.");
const wxString gcerr_infile::tokenCountMismatch=wxTRANSLATE("While using delimiter \"%s\" to separate \"%s\" into tokens for sample \"%s\", saw %d tokens when we expected %d.");
const wxString gcerr_infile::tooFewMarkersInSample=wxTRANSLATE("%s\nIs the sequence on the previous line too short?");
const wxString gcerr_infile::tooManyMarkersInSample=wxTRANSLATE("Found %d data markers, but expected only %d.");
const wxString gcerr_infile::unableToParseBecause=wxTRANSLATE("Unable to parse file %s as a \"%s\" file of type\" %s\" and \"%s\" sequences because:\n\t%s");

const wxString gcerr_migrate::badDelimiter   = wxTRANSLATE("Delimiter \"%s\" illegal in migrate files. A better choice would be \".\"");
const wxString gcerr_migrate::badLocusCount  = wxTRANSLATE("Expected a segment count but got \"%s\" instead. Segment count should be a positive integer.");
const wxString gcerr_migrate::badPopCount    = wxTRANSLATE("Expected a population count but got \"%s\" instead. Population count should be a positive integer.");
const wxString gcerr_migrate::badSequenceCount=wxTRANSLATE("Migrate parser was expecting sequence count of 1 or greater but got \"%s\" instead.");
const wxString gcerr_migrate::firstToken  = wxTRANSLATE("Ignoring first token \"%s\" of migrate file. Expected data type indicator of \"e\", \"m\", \"n\", or \"s\", or population count.");
const wxString gcerr_migrate::locusLengthNotPositive=   wxTRANSLATE("token \"%s\" in segment lengths not a positive number");
const wxString gcerr_migrate::missingMsatDelimiter=wxTRANSLATE("File %s cannot be microsatellite data -- it is missing an appropriate delimiter in its first line");
const wxString gcerr_migrate::missingSequenceCount=wxTRANSLATE("Migrate parser was expecting sequence count but got \"%s\" instead.");
const wxString gcerr_migrate::parseErr      = wxTRANSLATE("Error while parsing near line %d of file %s: %s.");
const wxString gcerr_migrate::tooFewSequenceLengths = wxTRANSLATE("Found only %d sequence lengths in line \"%s\" of migrate file.");

const char gcstr_migrate::missingData =  '?';

const wxString gcerr_phylip::badFirstToken   =   wxTRANSLATE("Not a phylip file: first token \"%s\" should be a positive integer.");
const wxString gcerr_phylip::badSecondToken  =   wxTRANSLATE("Not a phylip file: second token \"%s\" should be a positive integer.");

//____________________________________________________________________________________
