// $Id: gc_strings_structures.cpp,v 1.15 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings_structures.h"
#include "wx/intl.h"

const wxString gcstr_structures::objDict            = wxTRANSLATE("population, segment, or region");
const wxString gcstr_structures::traitDict          = wxTRANSLATE("trait name, allele name, or phenotype");

const wxString gcerr_structures::duplicateFileBaseName = wxTRANSLATE("Attempt to add a second file with base name \"%s\". Ignoring.");
const wxString gcerr_structures::duplicateFileName  = wxTRANSLATE("Attempt to add file \"%s\" a second time. Ignoring.");
const wxString gcerr_structures::duplicateName      =   wxTRANSLATE("Re-use of name \"%s\" for %s.");
const wxString gcerr_structures::mismatchAlleleTrait=   wxTRANSLATE("Allele \"%s\" of trait \"%s\" is illegal. Allele already const assigned to trait \"%s\".");
const wxString gcerr_structures::migrationNotDefined=   wxTRANSLATE("Migration from \"%s\" to \"%s\" does not exist");
const wxString gcerr_structures::mismatchLocusRegion=   wxTRANSLATE("Segment \"%s\" of region \"%s\" is illegal. Segment already assigned to region \"%s\".");
const wxString gcerr_structures::missingFile        =   wxTRANSLATE("File \"%s\" missing or unreadable. Unable to complete your operation.");
const wxString gcerr_structures::missingMigration   =   wxTRANSLATE("Migration \"%s\" not defined");
const wxString gcerr_structures::missingMigrationId =   wxTRANSLATE("Migration Id \"%s\" not defined");
const wxString gcerr_structures::missingName        =   wxTRANSLATE("Did not find name \"%s\" for population, group, segment, or trait");
const wxString gcerr_structures::missingPanel       =   wxTRANSLATE("Panel \"%s\" not defined");
const wxString gcerr_structures::missingPanelId     =   wxTRANSLATE("Panel Id \"%s\" not defined");
const wxString gcerr_structures::missingParent      =   wxTRANSLATE("Parent \"%s\" not defined");
const wxString gcerr_structures::missingParentId    =   wxTRANSLATE("Parent Id \"%s\" not defined");
const wxString gcerr_structures::missingPopulation  =   wxTRANSLATE("Population \"%s\" not defined");
const wxString gcerr_structures::missingRegion      =   wxTRANSLATE("Region \"%s\" not defined");
const wxString gcerr_structures::missingTrait       =   wxTRANSLATE("Trait \"%s\" not defined");
const wxString gcerr_structures::panelBlessedError  =   wxTRANSLATE("Panel for region\"%s\" and population \"%s\" has a >0 value but is not blessed");
const wxString gcerr_structures::panelNotDefined    =   wxTRANSLATE("Panel for region\"%s\" and population \"%s\" does not exist");
const wxString gcerr_structures::panelSizeClash     =   wxTRANSLATE("Panels for population \"%s\" in regions \"%s\" and \"%s\" have different sizes");

#if 0
const wxString gcerr_structures::locusMergeFailure  =   wxTRANSLATE("Something unexpected happened while trying to merge segments.\n\nIt is possible your operation did not complete.");
const wxString gcerr_structures::missingLocus       =   wxTRANSLATE("Segment \"%s\" not defined");
const wxString gcerr_structures::plocusHapMismatch  =   wxTRANSLATE("Unable to assign parsed data from %s to segment \"%s\" because parsed data has ploidy of %d and segment has %d.");
const wxString gcerr_structures::plocusLengthMismatch=  wxTRANSLATE("Unable to assign parsed data from %s to segment \"%s\" because parsed data has length %d and segment has length %d.");
const wxString gcerr_structures::plocusTypeMismatch =   wxTRANSLATE("Unable to assign parsed data from %s to segment \"%s\" because they have incompatible data types.\n\nParsed data is type %s and segment has type %s.");
#endif

const wxString gcerr_structures::nameRepeatAllele=   wxTRANSLATE("Allele name \"%s\" occurs previously at line %d.");
const wxString gcerr_structures::nameRepeatLocus=   wxTRANSLATE("Segment name \"%s\" occurs previously at line %d.");
const wxString gcerr_structures::nameRepeatPop  =   wxTRANSLATE("Population name \"%s\" occurs previously at line %d.");
const wxString gcerr_structures::nameRepeatRegion=  wxTRANSLATE("Region name \"%s\" occurs previously at line %d.");
const wxString gcerr_structures::nameRepeatTrait=  wxTRANSLATE("Trait name \"%s\" occurs previously at line %d.");
const wxString gcerr_structures::regionEffPopSizeClash  =   wxTRANSLATE("Unable to merge regions because relative effective population sizes %f and %f are different");

// const wxString gcerr_structures::regionSamplesPerClash  =   wxTRANSLATE("Unable to merge regions because samples per individual of %ld and %ld are different");
const wxString gcerr_structures::unparsableFile     =   wxTRANSLATE("File \"%s\" unparsable. Unable to complete your operation. \n\nTry running program with \"--verbose\" option for more information.");

//____________________________________________________________________________________
