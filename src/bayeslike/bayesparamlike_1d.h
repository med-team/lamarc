// $Id: bayesparamlike_1d.h,v 1.11 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Lucian Smith, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef BAYES_PARAMLIKE_1D_H
#define BAYES_PARAMLIKE_1D_H

#include "bayescurve.h"

class Parameter;

class BayesParamLike_1D
{
  public:
    BayesParamLike_1D (Parameter param);
    ~BayesParamLike_1D ();
    void AddPoint(double value, long freq);
    //Initializes m_smoothed_curve
    void SmoothCurve();
    BayesCurve GetSmoothedCurve() {return m_smoothed_curve;}
    priortype GetPriorType() {return m_priortype;}
    double GetKernelWidth() {return m_kernelwidth;}
    long   GetNumUniquePoints();

  private:
    std::vector<std::pair<double, long> > m_raw_data;
    unsigned long m_numpoints;
    bool m_isvalid;

    BayesCurve m_smoothed_curve;

    double m_kernelwidth;

    //These values are set on a per-force basis.
    priortype m_priortype;
    double m_binwidth;

    //Functions.  Used by SmoothCurve(), generally.
    void InitializeCurve();
    double CalculateStdev(const double average); //used for CalcualateH()

}; // class BayesParamLike_1D

#endif // BAYES_PARAMLIKE_1DH

//____________________________________________________________________________________
