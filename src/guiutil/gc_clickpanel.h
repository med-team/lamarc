// $Id: gc_clickpanel.h,v 1.9 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_CLICKPANEL_H
#define GC_CLICKPANEL_H

#include "wx/sizer.h"
#include "wx/stattext.h"
#include "wx/wx.h"

class gcClickCell;

class gcClickPanel : public wxPanel
{
  private:
  protected:
    wxBoxSizer *        m_sizer;
  public:
    gcClickPanel(gcClickCell * parent);
    virtual ~gcClickPanel();

    void AddText(wxString text);
    void CenterText(wxString text);
    void OnMouse(wxMouseEvent & event);
    void RecursiveSetColour(wxColour c);

    void        FinishSizing();

    DECLARE_EVENT_TABLE()
};

class gcClickCell : public wxPanel
{
  private:
    gcClickCell();         // undefined
  protected:
    wxStaticBoxSizer *      m_sizer;
    gcClickPanel *          m_clickPanel;
    bool                    m_mouseInCell;
#ifdef LAMARC_COMPILE_MACOSX
    int                     m_clickCount;  // HACK.HACK.HACK - 10.7 counts things wrong apparently
#endif

    void        FinishSizing();
  public:
    gcClickCell(wxWindow * parent, wxString label);
    virtual ~gcClickCell();

    void OnMouse(wxMouseEvent & event);
    void RecursiveSetColour(wxColour c);

    virtual void NotifyEntering     ();
    virtual void NotifyLeaving      ();
    virtual void NotifyLeftDClick   ();
    virtual void NotifyLeftDown     ();
    virtual void NotifyLeftUp       ();

    void AddText(wxString text);
    void CenterText(wxString text);

    DECLARE_EVENT_TABLE()
};

class gcClickText : public wxStaticText
{
  private:
    gcClickText();       // undefined
  protected:
    gcClickPanel *     m_parent;
  public:
    gcClickText(gcClickPanel * parent,wxString label);
    virtual ~gcClickText();

    void OnMouse(wxMouseEvent & event);

    DECLARE_EVENT_TABLE()
};

#endif  // GC_CLICKPANEL_H

//____________________________________________________________________________________
