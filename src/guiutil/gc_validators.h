// $Id: gc_validators.h,v 1.7 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_VALIDATORS_H
#define GC_VALIDATORS_H

#include "wx/valtext.h"

class GCIntegerValidator : public wxTextValidator
{
  private:
    wxString                    *   m_stringVar;
  public:
    GCIntegerValidator();
    virtual ~GCIntegerValidator();
};

class GCIntegerListValidator : public wxTextValidator
{
  private:
    wxString                    *   m_stringVar;
  public:
    GCIntegerListValidator();
    virtual ~GCIntegerListValidator();
};

class GCNonNegativeIntegerValidator : public wxTextValidator
{
  private:
    wxString                    *   m_stringVar;
  public:
    GCNonNegativeIntegerValidator();
    virtual ~GCNonNegativeIntegerValidator();
};

class GCPositiveFloatValidator : public wxTextValidator
{
  private:
    wxString                    *   m_stringVar;
  public:
    GCPositiveFloatValidator();
    virtual ~GCPositiveFloatValidator();
};

#endif  // GC_VALIDATORS_H

//____________________________________________________________________________________
