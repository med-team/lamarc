// $Id: gc_gridpanel.h,v 1.24 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_GRIDPANEL_H
#define GC_GRIDPANEL_H

#include <map>
#include <vector>

#include "gc_event_ids.h"
#include "gc_layout.h"
#include "wx/scrolwin.h"
#include "wx/sizer.h"
#include "wx/stattext.h"
#include "wx/wx.h"

class GCLogic;
class wxSizer;

typedef std::pair<size_t,size_t>        cellIndices;
typedef std::map<cellIndices,wxWindow*> cellMap;
typedef std::vector<size_t>             objIdsByRow;

class gcGridPane : public wxPanel
{
  private:

  protected:
    wxFlexGridSizer *   m_sizer;
    size_t              m_rows;
    size_t              m_cols;
    cellMap             m_cells;
    objIdsByRow         m_objVec;

  public:

    gcGridPane(wxWindow * parent, size_t cols, size_t growCol);
    virtual ~gcGridPane();

    virtual void NotifyEntering     (size_t row, size_t col);
    virtual void NotifyLeaving      (size_t row, size_t col);
    virtual void NotifyLeftDClick   (size_t row, size_t col);
    virtual void NotifyLeftDown     (size_t row, size_t col);
    virtual void NotifyLeftUp       (size_t row, size_t col);

    // EWFIX.P4 -- move to protected and inherit
    void        AddRow(size_t objId, wxArrayString labels);

    void        Finish();
};

class gcGridCell : public wxPanel
{
  private:
    gcGridCell();                // undefined
  protected:
    size_t      m_rowIndex;
    size_t      m_colIndex;
  public:
    gcGridCell( gcGridPane *    parent,
                wxString        label,
                size_t          rowIndex,
                size_t          colIndex);
    virtual ~gcGridCell();

    void OnMouse(wxMouseEvent & event);

    DECLARE_EVENT_TABLE()
};

class gcGridText : public wxStaticText
{
  private:
    gcGridText();       // undefined
  protected:
  public:
    gcGridText(gcGridCell * parent,wxString label);
    virtual ~gcGridText();
    void OnMouse(wxMouseEvent & event);

    DECLARE_EVENT_TABLE()
};

class gcInfoPane : public wxPanel
{
  private:
    gcInfoPane();              // undefined
  protected:
    wxScrolledWindow *  m_scrolled;
    wxSizer *           m_contentSizer;
    wxPanel *           m_innerPanel;
    wxSizer *           m_topSizer;
    const wxString      m_panelLabelFmt;
    wxStaticText *      m_panelLabel;
    GCLogic &           m_logic;
    virtual wxPanel *   MakeContent() = 0;
  public:
    gcInfoPane(wxWindow * parent, GCLogic & logic, wxString title);
    virtual ~gcInfoPane();
    void UpdateUserCues();
    virtual wxString MakeLabel() = 0;
};

#endif  // GC_GRIDPANEL_H

//____________________________________________________________________________________
