// $Id: xml.h,v 1.51 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef XML_H
#define XML_H

#include <string>
#include "parsetreeschema.h"
#include "tinyxml.h"

using std::string;

/******************************************************************
 This class reads the data file, including both molecular data and
 option settings, and produces a Tiny XML parse tree of the contents.
 It does not process those contents. Instead they are read by
 classes ParseTreeToData and ParseTreeToSettings, which walk the
 TinyXML tree this class generates.

*********************************************************************/

class XmlErrorSupport
{
  protected:
    string m_fileName;
    string m_errorMessage;

    XmlErrorSupport();          // undefined

  public:
    XmlErrorSupport(string fileName);
    virtual ~XmlErrorSupport();

    string      GetFileName();
    void        ThrowDataError(const string& reason);
    void        ThrowDataError(const string& reason, long lineno);
    void        ThrowFileError(const string& reason);
    void        ThrowInternalXMLError(const string& reason);
    void        ThrowXMLError(const string& reason);
    void        ThrowXMLError(const string& reason, long lineno);
};

class FrontEndWarnings;

class XmlParser : public XmlErrorSupport
{
  private:
    XmlParser();        // undefined

    TiXmlDocument       m_document;
    ParseTreeSchema &   m_schema;
    FrontEndWarnings &  m_frontEndWarnings;

  protected:
    void checkAttrAllowed(TiXmlElement * elem, string attrName);
    void checkAttrPresent(TiXmlElement * elem, string attrName);
    void checkElemCount(TiXmlElement * elem, string childName);
    void checkElemPresent(TiXmlElement * elem, string elemName);
    void checkSchema(TiXmlElement *);
    void checkSchema();

  public:
    XmlParser(ParseTreeSchema & schema, FrontEndWarnings & warnings);
    virtual ~XmlParser();
    void ParseFileData(string fileName);   // builds TiXML structures
    TiXmlElement * GetRootElement();
};

#endif // XML_H

//____________________________________________________________________________________
