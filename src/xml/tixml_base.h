// $Id: tixml_base.h,v 1.6 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef TIXML_BASE_H
#define TIXML_BASE_H

#include <string>
#include <vector>

#include "errhandling.h"

class TiXmlElement;

class tibasestr
{
  public:
    static const std::string EXTRA_TAG_0 ;
    static const std::string EXTRA_TAG_1 ;
    static const std::string MISSING_TAG_0 ;
    static const std::string MISSING_TAG_1 ;
    static const std::string NOT_DOUBLE_0 ;
    static const std::string NOT_DOUBLE_1 ;
    static const std::string NOT_LONG_0 ;
    static const std::string NOT_LONG_1 ;
    static const std::string NOT_SIZE_T_0 ;
    static const std::string NOT_SIZE_T_1 ;
};

TiXmlElement *          ti_singleElement(TiXmlElement* ancestor, std::string nodeName,bool required);
TiXmlElement *          ti_optionalChild(TiXmlElement* ancestor, std::string nodeName);
TiXmlElement *          ti_requiredChild(TiXmlElement* ancestor, std::string nodeName);
std::string             ti_nodeText(TiXmlElement *);
std::string             ti_attributeValue(TiXmlElement*,std::string attributeName);
bool                    ti_hasAttribute(TiXmlElement*,std::string attributeName);

double                  ti_double_from_text(TiXmlElement *);
long                    ti_long_from_text(TiXmlElement *) throw (incorrect_xml);
size_t                  ti_size_t_from_text(TiXmlElement *) throw (incorrect_xml);

std::vector<TiXmlElement *>  ti_optionalChildren(TiXmlElement* ancestor, std::string nodeName);
std::vector<TiXmlElement *>  ti_requiredChildren(TiXmlElement* ancestor, std::string nodeName);

#endif  // TIXML_BASE_H

//____________________________________________________________________________________
