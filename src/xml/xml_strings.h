// $Id: xml_strings.h,v 1.81 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef XMLSTRINGS_H
#define XMLSTRINGS_H

#include <string>
#include "local_build.h"

class xmlstr
{
  public:

    static const std::string XML_COMMENT_SEED_FROM_CLOCK_0  ;
    static const std::string XML_COMMENT_SEED_FROM_CLOCK_1  ;

    static const std::string XML_STRING_UNKNOWN_FILE   ;
    static const std::string XML_STRING_COLON          ;
    static const std::string XML_STRING_DASH           ;
    static const std::string XML_STRING_USER           ;
    static const std::string XML_STRING_NEWICK         ;
    static const std::string XML_STRING_ARG            ;

    static const std::string XML_ERR_0                 ;
    static const std::string XML_ERR_1                 ;
    static const std::string XML_ERR_2                 ;
    static const std::string XML_ERR_3                 ;
    static const std::string XML_ERR_ANCESTOR_TRUEPOP_0    ;
    static const std::string XML_ERR_ANCESTOR_TRUEPOP_1    ;
    static const std::string XML_ERR_ATTR_MISSING_0    ;
    static const std::string XML_ERR_ATTR_MISSING_1    ;
    static const std::string XML_ERR_ATTR_MISSING_2    ;
    static const std::string XML_ERR_ELEM_MISSING_0    ;
    static const std::string XML_ERR_ELEM_MISSING_1    ;
    static const std::string XML_ERR_ELEM_MISSING_2    ;
    static const std::string XML_ERR_BAD_ANCESTOR_TREE     ;
    static const std::string XML_ERR_BOTH_SEED_TYPES_0 ;
    static const std::string XML_ERR_BOTH_SEED_TYPES_1 ;
    static const std::string XML_ERR_BOTH_SEED_TYPES_2 ;
    static const std::string XML_ERR_BOTH_SEED_TYPES_3 ;
    static const std::string XML_ERR_DATA_ERR_0        ;
    static const std::string XML_ERR_DATA_ERR_1        ;
    static const std::string XML_ERR_DATA_ERR_2        ;
    static const std::string XML_ERR_DATA_ERR_3        ;
    static const std::string XML_ERR_DUPLICATE_ANCESTOR_0 ;
    static const std::string XML_ERR_DUPLICATE_ANCESTOR_1 ;
    static const std::string XML_ERR_DUPLICATE_REGIONNAME_0;
    static const std::string XML_ERR_DUPLICATE_REGIONNAME_1;
    static const std::string XML_ERR_DUPLICATE_SAMPLENAME_0;
    static const std::string XML_ERR_DUPLICATE_SAMPLENAME_1;
    static const std::string XML_ERR_EMPTY_ANCESTOR    ;
    static const std::string XML_ERR_EXTRA_TAG_0       ;
    static const std::string XML_ERR_EXTRA_TAG_1       ;
    static const std::string XML_ERR_EXTRA_TAG_2       ;
    static const std::string XML_ERR_EXTRA_TAG_TOP_0   ;
    static const std::string XML_ERR_EXTRA_TAG_TOP_1   ;
    static const std::string XML_ERR_FILE_ERR          ;
    static const std::string XML_ERR_FILE_NOT_FOUND_0  ;
    static const std::string XML_ERR_FILE_NOT_FOUND_1  ;
    static const std::string XML_ERR_INCONSISTENT_REGION;
    static const std::string XML_ERR_INTERNAL          ;
    static const std::string XML_ERR_INVALID_METHOD_0  ;
    static const std::string XML_ERR_INVALID_METHOD_1  ;
    static const std::string XML_ERR_METHOD_TYPE_COUNT_0;
    static const std::string XML_ERR_METHOD_TYPE_COUNT_1;
    static const std::string XML_ERR_METHOD_TYPE_COUNT_2;
    static const std::string XML_ERR_METHOD_TYPE_COUNT_3;
    static const std::string XML_ERR_METHOD_USER_WITHOUT_VALUE_0;
    static const std::string XML_ERR_METHOD_USER_WITHOUT_VALUE_1;
    static const std::string XML_ERR_METHOD_USER_WITHOUT_VALUE_2;
    static const std::string XML_ERR_MISSING_CONTENT_0 ;
    static const std::string XML_ERR_MISSING_CONTENT_1 ;
    static const std::string XML_ERR_MISSING_NEWPOP_0 ;
    static const std::string XML_ERR_MISSING_NEWPOP_1 ;
    static const std::string XML_ERR_MISSING_TAG_0     ;
    static const std::string XML_ERR_MISSING_TAG_1     ;
    static const std::string XML_ERR_MISSING_TAG_HIER_0;
    static const std::string XML_ERR_MISSING_TAG_HIER_1;
    static const std::string XML_ERR_MISSING_TAG_HIER_2;
    static const std::string XML_ERR_MULTIPLE_ANCESTORS_0 ;
    static const std::string XML_ERR_MULTIPLE_ANCESTORS_1 ;
    static const std::string XML_ERR_MULTIPLE_ANCESTORS_2 ;
    static const std::string XML_ERR_MULTIPLE_ANCESTORS_3 ;
    static const std::string XML_ERR_NEST_0            ;
    static const std::string XML_ERR_NEST_1            ;
    static const std::string XML_ERR_NEWPOP            ;
    static const std::string XML_ERR_NO_SUBTAG_0       ;
    static const std::string XML_ERR_NO_SUBTAG_1       ;
    static const std::string XML_ERR_NO_SUBTAG_2       ;
    static const std::string XML_ERR_NO_TAG_0          ;
    static const std::string XML_ERR_NO_TAG_1          ;
    static const std::string XML_ERR_NOT_LAMARC        ;
    static const std::string XML_ERR_NOT_MAPFILE       ;
    static const std::string XML_ERR_NOT_XML           ;
    static const std::string XML_ERR_NO_XML_DATA       ;
    static const std::string XML_ERR_START_VALUE_COUNT_0;
    static const std::string XML_ERR_START_VALUE_COUNT_1;
    static const std::string XML_ERR_START_VALUE_COUNT_2;
    static const std::string XML_ERR_START_VALUE_COUNT_3;
    static const std::string XML_ERR_UNEXPECTED_ATTR_0 ;
    static const std::string XML_ERR_UNEXPECTED_ATTR_1 ;
    static const std::string XML_ERR_UNEXPECTED_ATTR_2 ;
    static const std::string XML_ERR_UNEXPECTED_TAG_0  ;
    static const std::string XML_ERR_UNEXPECTED_TAG_1  ;
    static const std::string XML_ERR_UNEXPECTED_TAG_2  ;

    static const std::string XML_IERR_DUP_ATTR_0       ;
    static const std::string XML_IERR_DUP_ATTR_1       ;
    static const std::string XML_IERR_DUP_ATTR_2       ;
    static const std::string XML_IERR_DUP_CHILD_0      ;
    static const std::string XML_IERR_DUP_CHILD_1      ;
    static const std::string XML_IERR_DUP_CHILD_2      ;
    static const std::string XML_IERR_DUP_TAG_0        ;
    static const std::string XML_IERR_DUP_TAG_1        ;
    static const std::string XML_IERR_NO_PARENT_TAG_0  ;
    static const std::string XML_IERR_NO_PARENT_TAG_1  ;
    static const std::string XML_IERR_NO_PARENT_TAG_2  ;
    static const std::string XML_IERR_NO_TAG_0         ;
    static const std::string XML_IERR_NO_TAG_1         ;
    static const std::string XML_IERR_NO_TAG_2         ;

    static const std::string XML_ATTRTYPE_ATTR_NAME    ;
    static const std::string XML_ATTRTYPE_ATTR_TYPE    ;
    static const std::string XML_ATTRTYPE_CONSTRAINT   ;
    static const std::string XML_ATTRTYPE_FOR          ;
    static const std::string XML_ATTRTYPE_EDGEDEFAULT  ;
    static const std::string XML_ATTRTYPE_ID           ;
    static const std::string XML_ATTRTYPE_KEY          ;
    static const std::string XML_ATTRTYPE_LOCUS_NAME   ;
    static const std::string XML_ATTRTYPE_NAME         ;
    static const std::string XML_ATTRTYPE_SOURCE       ;
    static const std::string XML_ATTRTYPE_TYPE         ;
    static const std::string XML_ATTRTYPE_TARGET       ;
    static const std::string XML_ATTRTYPE_VALUE        ;
    static const std::string XML_ATTRTYPE_VERSION      ;

    static const std::string XML_TAG_ALLELES           ;
    static const std::string XML_TAG_ALPHA             ;
    static const std::string XML_TAG_ANALYSIS          ;
    static const std::string XML_TAG_ANCESTOR          ;
    static const std::string XML_TAG_ARGDATA           ;
    static const std::string XML_TAG_ARGFILE_PREFIX    ;
    static const std::string XML_TAG_AUTOCORRELATION   ;
    static const std::string XML_TAG_BASE_FREQS        ;
    static const std::string XML_TAG_BAYESIAN          ;
    static const std::string XML_TAG_BAYESIAN_ANALYSIS ;
    static const std::string XML_TAG_BLOCK             ;
    static const std::string XML_TAG_CALCULATED        ;
    static const std::string XML_TAG_CATEGORIES        ;
    static const std::string XML_TAG_CHAINS            ;
    static const std::string XML_TAG_COALESCENCE       ;
    static const std::string XML_TAG_CONSTRAINTS       ;
    static const std::string XML_TAG_CONVERT_OUTPUT    ;
    static const std::string XML_TAG_CREATING          ;
    static const std::string XML_TAG_CURVEFILE_PREFIX  ;
    static const std::string XML_TAG_DATA              ;
    static const std::string XML_TAG_DATABLOCK         ;
    static const std::string XML_TAG_DISCARD           ;
    static const std::string XML_TAG_DISEASE           ;
    static const std::string XML_TAG_DISEASELOCATION   ;
    static const std::string XML_TAG_DISEASESTATUS     ;
    static const std::string XML_TAG_DIVERGENCE        ;
    static const std::string XML_TAG_DIVMIG            ;
    static const std::string XML_TAG_ECHO              ;
    static const std::string XML_TAG_EDGE              ;
    static const std::string XML_TAG_EDGEDEFAULT       ;
    static const std::string XML_TAG_EFFECTIVE_POPSIZE ;
    static const std::string XML_TAG_END               ;
    static const std::string XML_TAG_EPOCH_BOUNDARY    ;
    static const std::string XML_TAG_EPOCHSIZEARRANGER ;
    static const std::string XML_TAG_FINAL             ;
    static const std::string XML_TAG_FOR               ;
    static const std::string XML_TAG_FORCES            ;
    static const std::string XML_TAG_FORMAT            ;
    static const std::string XML_TAG_GENOTYPE          ;
    static const std::string XML_TAG_GENOTYPE_RESOLUTIONS;
    static const std::string XML_TAG_GRAPH             ;
    static const std::string XML_TAG_GROUP             ;
    static const std::string XML_TAG_GROWTH            ;
    static const std::string XML_TAG_GTRRATES          ;
    static const std::string XML_TAG_HAPLOTYPES        ;
    static const std::string XML_TAG_HAPLOTYPING       ;
    static const std::string XML_TAG_HAP_COUNT         ;
    static const std::string XML_TAG_HEATING           ;
    static const std::string XML_TAG_HEATING_STRATEGY  ;
    static const std::string XML_TAG_INDIVIDUAL        ;
    static const std::string XML_TAG_INITIAL           ;
    static const std::string XML_TAG_INTERVAL          ;
    static const std::string XML_TAG_IN_SUMMARY_FILE   ;
    static const std::string XML_TAG_ISOPT             ;
    static const std::string XML_TAG_KEY               ;
    static const std::string XML_TAG_LAMARC            ;
    static const std::string XML_TAG_LENGTH            ;
    static const std::string XML_TAG_LOCATIONS         ;
    static const std::string XML_TAG_LOCUSARRANGER     ;
    static const std::string XML_TAG_LOGISTICSELECTION ;
    static const std::string XML_TAG_STOCHASTICSELECTION;
    static const std::string XML_TAG_MANY_ARGFILES     ;
    static const std::string XML_TAG_MAPFILE           ;
    static const std::string XML_TAG_MAP_POSITION      ;
    static const std::string XML_TAG_MARKER_WEIGHTS    ;
    static const std::string XML_TAG_MATRIX_MEMBERS    ;
    static const std::string XML_TAG_MAX_EVENTS        ;
    static const std::string XML_TAG_MERGING           ;
    static const std::string XML_TAG_METHOD            ;
    static const std::string XML_TAG_MIGRATION         ;
    static const std::string XML_TAG_MODEL             ;
    static const std::string XML_TAG_MU                ;
    static const std::string XML_TAG_NAME              ;
    static const std::string XML_TAG_NEWICKTREEFILE_PREFIX;
    static const std::string XML_TAG_NEWPOP            ;
    static const std::string XML_TAG_NODE              ;
    static const std::string XML_TAG_NORMALIZE         ;
    static const std::string XML_TAG_NU                ;
    static const std::string XML_TAG_NUMBER            ;
    static const std::string XML_TAG_NUM_CATEGORIES    ;
    static const std::string XML_TAG_NUM_SITES         ;
    static const std::string XML_TAG_OFFSET            ;
    static const std::string XML_TAG_OLD_SUMMARY_FILE  ;
    static const std::string XML_TAG_OUT_SUMMARY_FILE  ;
    static const std::string XML_TAG_OUT_XML_FILE      ;
    static const std::string XML_TAG_PANEL             ;
    static const std::string XML_TAG_PANELSIZE         ;
    static const std::string XML_TAG_PARAMETER_FILE    ;
    static const std::string XML_TAG_PARAMINDEX        ;
    static const std::string XML_TAG_PENETRANCE        ;
    static const std::string XML_TAG_PER_BASE_ERROR_RATE;
    static const std::string XML_TAG_PHASE             ;
    static const std::string XML_TAG_PHENOTYPE         ;
    static const std::string XML_TAG_PHENOTYPES        ;
    static const std::string XML_TAG_PHENOTYPE_NAME    ;
    static const std::string XML_TAG_PLOTTING          ;
    static const std::string XML_TAG_POPULATION        ;
    static const std::string XML_TAG_POPTREE           ;
    static const std::string XML_TAG_POSSIBLE_LOCATIONS;
    static const std::string XML_TAG_POSTERIOR         ;
    static const std::string XML_TAG_PRIOR             ;
    static const std::string XML_TAG_PRIORLOWERBOUND   ;
    static const std::string XML_TAG_PRIORUPPERBOUND   ;
    static const std::string XML_TAG_PROBABILITIES     ;
    static const std::string XML_TAG_PROFILE           ;
    static const std::string XML_TAG_PROFILE_PREFIX    ;
    static const std::string XML_TAG_PROFILES          ;
    static const std::string XML_TAG_PROGRESS_REPORTS  ;
    static const std::string XML_TAG_RANGE             ;
    static const std::string XML_TAG_RATES             ;
    static const std::string XML_TAG_RECLOCFILE_PREFIX ;
    static const std::string XML_TAG_RECOMBINATION     ;
    static const std::string XML_TAG_REGION            ;
    static const std::string XML_TAG_REGION_GAMMA      ;
    static const std::string XML_TAG_RELATIVE_MURATE   ;

#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    static const std::string XML_TAG_RELATIVE_SAMPLE_RATE   ;
#endif

    static const std::string XML_TAG_REPLICATES        ;
    static const std::string XML_TAG_REPORT_XML_FILE   ;
    static const std::string XML_TAG_RESIMULATING      ;
    static const std::string XML_TAG_RESULTS_FILE      ;
    static const std::string XML_TAG_SAMPLE            ;
    static const std::string XML_TAG_SAMPLES           ;
    static const std::string XML_TAG_SEED              ;
    static const std::string XML_TAG_SEED_FROM_CLOCK   ;
    static const std::string XML_TAG_SPACING           ;
    static const std::string XML_TAG_START             ;
    static const std::string XML_TAG_START_VALUES      ;
    static const std::string XML_TAG_STATUS            ;
    static const std::string XML_TAG_STAIRARRANGER     ;
    static const std::string XML_TAG_STRATEGY          ;
    static const std::string XML_TAG_SWAP_INTERVAL     ;
    static const std::string XML_TAG_TEMPERATURES      ;
    static const std::string XML_TAG_TRACEFILE_PREFIX  ;
    static const std::string XML_TAG_TRAIT             ;
    static const std::string XML_TAG_TRAITS            ;
    static const std::string XML_TAG_TRAIT_NAME        ;
    static const std::string XML_TAG_TREE              ;
    static const std::string XML_TAG_TREESIZE          ;
    static const std::string XML_TAG_TRUEVALUE         ;
    static const std::string XML_TAG_TTRATIO           ;
    static const std::string XML_TAG_USE_ARGFILES      ;
    static const std::string XML_TAG_USE_CURVEFILES    ;
    static const std::string XML_TAG_USE_IN_SUMMARY    ;
    static const std::string XML_TAG_USE_NEWICKTREEFILE;
    static const std::string XML_TAG_USE_OUT_SUMMARY   ;
    static const std::string XML_TAG_USE_RECLOCFILE    ;
    static const std::string XML_TAG_USE_TRACEFILE     ;
    static const std::string XML_TAG_VERBOSITY         ;

    static const std::string XML_ATTRVALUE_ASITES      ;
    static const std::string XML_ATTRVALUE_CURVE       ;
    static const std::string XML_ATTRVALUE_DOUBLE      ;
    static const std::string XML_ATTRVALUE_KNOWN       ;
    static const std::string XML_ATTRVALUE_LIVE_SITES  ;
    static const std::string XML_ATTRVALUE_LONG        ;
    static const std::string XML_ATTRVALUE_NLABEL      ;
    static const std::string XML_ATTRVALUE_NODE_LABEL  ;
    static const std::string XML_ATTRVALUE_NODE_TIME   ;
    static const std::string XML_ATTRVALUE_NODE_TYPE   ;
    static const std::string XML_ATTRVALUE_NTIME       ;
    static const std::string XML_ATTRVALUE_NTYPE       ;
    static const std::string XML_ATTRVALUE_OFF         ;
    static const std::string XML_ATTRVALUE_PARTITIONS  ;
    static const std::string XML_ATTRVALUE_PTYPE       ;
    static const std::string XML_ATTRVALUE_REC_LOCATION;
    static const std::string XML_ATTRVALUE_RLOC        ;
    static const std::string XML_ATTRVALUE_STRING      ;
    static const std::string XML_ATTRVALUE_TRANS_SITES ;
    static const std::string XML_ATTRVALUE_UNKNOWN     ;

    static const std::string XML_ATTRVALUE_STICK       ;
    static const std::string XML_ATTRVALUE_STICKEXP    ;

    static const std::string XML_BRANCHTYPE_COAL   ;
    static const std::string XML_BRANCHTYPE_DISEASE;
    static const std::string XML_BRANCHTYPE_DIVMIG ;
    static const std::string XML_BRANCHTYPE_EPOCH  ;
    static const std::string XML_BRANCHTYPE_MIG    ;
    static const std::string XML_BRANCHTYPE_REC    ;
    static const std::string XML_BRANCHTYPE_TIP    ;

    static const std::string XML_WARN_DEPRECATED_TAG_0;
    static const std::string XML_WARN_DEPRECATED_TAG_1;
    static const std::string XML_WARN_DEPRECATED_TAG_2;

    static const std::string DATA_MODEL_F84;

    // static const std::string WHAT_DOES_VALGRIND_THINK;

};

#endif // XMLSTRINGS_H

//____________________________________________________________________________________
