// $Id: toxml.h,v 1.15 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// PopulationXML is a helper class used by Region::MakePopXML() to create an
// XML infile for lamarc identical (in execution) to the one used to begin
// the program run.  PopulationXML's chief job is to manage a container of
// IndividualXMLs and provide other population level parameters.
//
// IndividualXML is a helper class to PopulationXML used to manage the individual
// level parameters and a container of SampleXML to be used for creation of a
// lamarc xml input file.  IndividualXML's are expected to be made by
// Individual::ToXML().
//
// SampleXML is a helper class to IndividualXML providing expertise on the sample
// level parameters including the actual genetic data.  SampleXML's are expected
// to be made by TipData::ToXML().
//
// WARNING:  These classes all provide a default (no arguments) constructor to
// meet the contract for being placed within STL containers, but attempts to call
// any member functions on a default constructed object will cause an assert!
//
// Written by Jon Yamato

#ifndef TOXML_H
#define TOXML_H

#include <map>
#include <string>
#include <vector>

#include "constants.h"
#include "defaults.h"
#include "individual.h"
#include "types.h"
#include "vectorx.h"

//------------------------------------------------------------------------------------

class TipData;
class Region;
class Locus;

//------------------------------------------------------------------------------------

class SampleXML
{
  private:
    std::string m_name;
    DataSourceVec1d m_geneticdatasource;
    StringVec1d m_geneticdatatype;
    StringVec1d m_geneticdata;
    std::map<force_type,std::string> m_stati;

  public:
    // we accept the default ctor, copy ctor and operator=
    SampleXML(const Region& region, const std::vector<TipData>& loci);

    StringVec1d ToXML(unsigned long nspaces) const;
};

//------------------------------------------------------------------------------------

class IndividualXML
{
  private:
    std::string m_name;
    LongVec2d m_phases;    // dim: loci x markers
    std::vector<SampleXML> m_samples;
    Individual m_individual; //oddly, only useful for trait data

  public:
    // we accept the default ctor, copy ctor and operator=
    IndividualXML(const Region& region, const std::vector<TipData>& popdata, long whichind);

    StringVec1d ToXML(unsigned long nspaces) const;
};

//------------------------------------------------------------------------------------

class PopulationXML
{
  private:
    std::string m_name;
    std::vector<IndividualXML> m_individuals;
    std::string m_panelname;
    long m_panelcount;

    const TipData* IndividualIsIn(const vector<TipData>& pdata, long whichind) const;

  public:
    // we accept the default ctor, copy ctor and operator=
    PopulationXML(const Region& region, const std::string& name);

    StringVec1d ToXML(unsigned long nspaces) const;
};

#endif // TOXML_H

//____________________________________________________________________________________
