// $Id: parsetreewalker.h,v 1.11 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PARSETREEWALKER_H
#define PARSETREEWALKER_H

#include <string>
#include "vectorx.h"    // for StringVec1d
#include "xml.h"
#include "rangex.h"

class TiXmlElement;
class ParseTreeSchema;

using std::string;

// class implements methods shared by
//      ParseTreeToData     -- which parses everything under <data> tag
//      ParseTreeToSettings -- which parses everything else
// methods are utility methods for finding a child tag of a specific
// name, and getting attribute text or text between tags, etc.
class ParseTreeWalker
{
  private:
    ParseTreeWalker();      // undefined

  protected:
    XmlParser &     m_parser;

    TiXmlElement *  getSingleDescendantElement(TiXmlElement* ancestor, string nodeName,bool required);
    vector<TiXmlElement *> getAllOptionalDescendantElements(TiXmlElement* ancestor,
                                                            string nodeName);
    TiXmlElement *  singleOptionalChild(TiXmlElement* ancestor, string nodeName);
    TiXmlElement *  singleRequiredChild(TiXmlElement* ancestor, string nodeName);
    string          getNodeText(TiXmlElement *);
    string          getGraphMLNodeText(TiXmlElement *);
    StringVec1d     getNodeTextSplitOnWhitespace(TiXmlElement *);
    string          getNodeAttributeValue(TiXmlElement*,string attributeName);

    void            checkSchema(TiXmlElement * topElem, ParseTreeSchema&);

  public:
    ParseTreeWalker(XmlParser & parser);
    virtual ~ParseTreeWalker();
};

#endif // PARSETREEWALKER_H

//____________________________________________________________________________________
