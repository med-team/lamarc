// $Id: xml_strings.cpp,v 1.75 2018/01/03 21:33:06 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <string>

#include "local_build.h"                // for definition of LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
#include "xml_strings.h"

using std::string;

//------------------------------------------------------------------------------------
// xml tags for lamarc input file

const string xmlstr::XML_COMMENT_SEED_FROM_CLOCK_0 =
    "<!-- The tag below documents the seed used for this run. -->";
const string xmlstr::XML_COMMENT_SEED_FROM_CLOCK_1 =
    "<!-- It is ignored if you use this file as lamarc input -->";

const string xmlstr::XML_STRING_UNKNOWN_FILE    = "<unknown>";
const string xmlstr::XML_STRING_COLON           = ": ";
const string xmlstr::XML_STRING_DASH            = "-";
const string xmlstr::XML_STRING_USER            = "USER";
const string xmlstr::XML_STRING_NEWICK          = "newick";
const string xmlstr::XML_STRING_ARG             = "arg";

const string xmlstr::XML_ERR_0                  = "XML Error: ";
const string xmlstr::XML_ERR_1                  = "in file \"";
const string xmlstr::XML_ERR_2                  = "\"  near line ";
const string xmlstr::XML_ERR_3                  = ": ";
const string xmlstr::XML_ERR_ANCESTOR_TRUEPOP_0 = "Population \"";
const string xmlstr::XML_ERR_ANCESTOR_TRUEPOP_1 = "\" appears as internal node in divergence tree.";
const string xmlstr::XML_ERR_ATTR_MISSING_0     = "Couldn't find required attribute \"";
const string xmlstr::XML_ERR_ATTR_MISSING_1     = "\" for element <";
const string xmlstr::XML_ERR_ATTR_MISSING_2     = ">";
const string xmlstr::XML_ERR_BAD_ANCESTOR_TREE      = "Badly specified divergence tree.";
const string xmlstr::XML_ERR_BOTH_SEED_TYPES_0  = "Only one of the following tags can appear in a lamarc file: <seed> ( near line ";
const string xmlstr::XML_ERR_BOTH_SEED_TYPES_1  = ") and <seed-from-system-clock> ( near line ";
const string xmlstr::XML_ERR_BOTH_SEED_TYPES_2  = ") of file \"";
const string xmlstr::XML_ERR_BOTH_SEED_TYPES_3  = "\".";
const string xmlstr::XML_ERR_DATA_ERR_0         = "Data Error: ";
const string xmlstr::XML_ERR_DATA_ERR_1         = "in file \"";
const string xmlstr::XML_ERR_DATA_ERR_2         = "\"  near line ";
const string xmlstr::XML_ERR_DATA_ERR_3         = ": ";
const string xmlstr::XML_ERR_DUPLICATE_ANCESTOR_0 = "Population name \"";
const string xmlstr::XML_ERR_DUPLICATE_ANCESTOR_1 = "\" appears in more than one <ancestor> tag.";
const string xmlstr::XML_ERR_DUPLICATE_REGIONNAME_0 = "There is more than one region named '";
const string xmlstr::XML_ERR_DUPLICATE_REGIONNAME_1 = "' in the input file.";
const string xmlstr::XML_ERR_DUPLICATE_SAMPLENAME_0 = "There is more than one sample named '";
const string xmlstr::XML_ERR_DUPLICATE_SAMPLENAME_1 = "' in region ";
const string xmlstr::XML_ERR_ELEM_MISSING_0     = "Couldn't find required subtag <";
const string xmlstr::XML_ERR_ELEM_MISSING_1     = "> for element <";
const string xmlstr::XML_ERR_ELEM_MISSING_2     = ">";
const string xmlstr::XML_ERR_EMPTY_ANCESTOR     = "Tag <ancestor> appears to be empty";
const string xmlstr::XML_ERR_EXTRA_TAG_0        = "Found more <";
const string xmlstr::XML_ERR_EXTRA_TAG_1        = "> tags under tag <";
const string xmlstr::XML_ERR_EXTRA_TAG_2        = "> than expected";
const string xmlstr::XML_ERR_EXTRA_TAG_TOP_0    = "Extra tag \"<";
const string xmlstr::XML_ERR_EXTRA_TAG_TOP_1    = ">\" found in file";
const string xmlstr::XML_ERR_FILE_ERR           = "File Error: ";
const string xmlstr::XML_ERR_FILE_NOT_FOUND_0   = "File \"";
const string xmlstr::XML_ERR_FILE_NOT_FOUND_1   = "\" not found";
const string xmlstr::XML_ERR_INCONSISTENT_REGION = "Inconsistent specifications in region ";
const string xmlstr::XML_ERR_INTERNAL           = "Internal XML error. Is schema out of date?: ";
const string xmlstr::XML_ERR_INVALID_METHOD_0   = "Invalid method ";
const string xmlstr::XML_ERR_INVALID_METHOD_1   = " for force type ";
const string xmlstr::XML_ERR_METHOD_TYPE_COUNT_0= "Tag <method> should have had ";
const string xmlstr::XML_ERR_METHOD_TYPE_COUNT_1= " parameters for force ";
const string xmlstr::XML_ERR_METHOD_TYPE_COUNT_2= ", but found ";
const string xmlstr::XML_ERR_METHOD_TYPE_COUNT_3= " instead.";
const string xmlstr::XML_ERR_METHOD_USER_WITHOUT_VALUE_0="Tag <method> for force \"";
const string xmlstr::XML_ERR_METHOD_USER_WITHOUT_VALUE_1="\" has a parameter of type \"USER\" without a supplied value. Taking the program default value of ";
const string xmlstr::XML_ERR_METHOD_USER_WITHOUT_VALUE_2=" .";
const string xmlstr::XML_ERR_MISSING_CONTENT_0  = "Missing content between <";
const string xmlstr::XML_ERR_MISSING_CONTENT_1  = "> tags";
const string xmlstr::XML_ERR_MISSING_NEWPOP_0 = "Population \"";
const string xmlstr::XML_ERR_MISSING_NEWPOP_1 = "\" never appears in a <new-population> tag in divergence tree.";
const string xmlstr::XML_ERR_MISSING_TAG_0      = "Could not find a <";
const string xmlstr::XML_ERR_MISSING_TAG_1      = "> tag. Expected at least 1.";
const string xmlstr::XML_ERR_MISSING_TAG_HIER_0 = "Expected a <";
const string xmlstr::XML_ERR_MISSING_TAG_HIER_1 = "> tag Within the <";
const string xmlstr::XML_ERR_MISSING_TAG_HIER_2 = "> tag.";
const string xmlstr::XML_ERR_MULTIPLE_ANCESTORS_0 = "Population name \"";
const string xmlstr::XML_ERR_MULTIPLE_ANCESTORS_1 = "\" appears in multiple <new-populations> tags with <ancestor> ";
const string xmlstr::XML_ERR_MULTIPLE_ANCESTORS_2 = " and ";
const string xmlstr::XML_ERR_MULTIPLE_ANCESTORS_3 = " or more.";
const string xmlstr::XML_ERR_NEST_0             = "Nested <";
const string xmlstr::XML_ERR_NEST_1             = "> tags encountered";
const string xmlstr::XML_ERR_NEWPOP             = "Tag <new-populations> appears to contain other than two populations";
const string xmlstr::XML_ERR_NO_SUBTAG_0        = "No <";
const string xmlstr::XML_ERR_NO_SUBTAG_1        = "> tag found in <";
const string xmlstr::XML_ERR_NO_SUBTAG_2        = "> element";
const string xmlstr::XML_ERR_NO_TAG_0           = "tag <";
const string xmlstr::XML_ERR_NO_TAG_1           = "> not found";
const string xmlstr::XML_ERR_NOT_LAMARC         = "Input does not appear to be a Lamarc XML file";
const string xmlstr::XML_ERR_NOT_MAPFILE        = "Input does not appear to be a Lamarc map file";
const string xmlstr::XML_ERR_NOT_XML            = "Input does not appear to be an XML file";
const string xmlstr::XML_ERR_NO_XML_DATA        = "Input does not appear to have any XML tags";
const string xmlstr::XML_ERR_START_VALUE_COUNT_0= "Tag <start-values> should have had ";
const string xmlstr::XML_ERR_START_VALUE_COUNT_1= " parameters for force ";
const string xmlstr::XML_ERR_START_VALUE_COUNT_2= ", but we found ";
const string xmlstr::XML_ERR_START_VALUE_COUNT_3= " instead.";
const string xmlstr::XML_ERR_UNEXPECTED_ATTR_0  = "Attribute \"";
const string xmlstr::XML_ERR_UNEXPECTED_ATTR_1  = "\" undefined or not legal to assign for tag <";
const string xmlstr::XML_ERR_UNEXPECTED_ATTR_2  = ">";
const string xmlstr::XML_ERR_UNEXPECTED_TAG_0   = "Tag <";
const string xmlstr::XML_ERR_UNEXPECTED_TAG_1   = "> undefined or not legal to appear under tag <";
const string xmlstr::XML_ERR_UNEXPECTED_TAG_2   = ">";

const string xmlstr::XML_IERR_DUP_ATTR_0        = "tried to add attribute \"";
const string xmlstr::XML_IERR_DUP_ATTR_1        = "\" to element \"";
const string xmlstr::XML_IERR_DUP_ATTR_2        = "\" twice. Fix schema.";
const string xmlstr::XML_IERR_DUP_CHILD_0       = "tried to add child element <";
const string xmlstr::XML_IERR_DUP_CHILD_1       = "> to parent element <";
const string xmlstr::XML_IERR_DUP_CHILD_2       = "> twice. Fix schema.";
const string xmlstr::XML_IERR_DUP_TAG_0         = "So sorry. You tried to add tag <";
const string xmlstr::XML_IERR_DUP_TAG_1         = "> twice. Simple schema doesn't support this.";
const string xmlstr::XML_IERR_NO_PARENT_TAG_0   = "tried to add element <";
const string xmlstr::XML_IERR_NO_PARENT_TAG_1   = "> before parent <";
const string xmlstr::XML_IERR_NO_PARENT_TAG_2   = "> was added. Fix schema.";
const string xmlstr::XML_IERR_NO_TAG_0          = "Did not recognize tag <";
const string xmlstr::XML_IERR_NO_TAG_1          = "> near line ";

#ifdef NDEBUG
const string xmlstr::XML_IERR_NO_TAG_2          = ".";
#else
const string xmlstr::XML_IERR_NO_TAG_2          = ". Do you need to update the schema?";
#endif

const string xmlstr::XML_ATTRTYPE_ATTR_NAME     = "attr.name";
const string xmlstr::XML_ATTRTYPE_ATTR_TYPE     = "attr.type";
const string xmlstr::XML_ATTRTYPE_CONSTRAINT    = "constraint";
const string xmlstr::XML_ATTRTYPE_FOR           = "for";
const string xmlstr::XML_ATTRTYPE_EDGEDEFAULT   = "edgedefault";
const string xmlstr::XML_ATTRTYPE_ID            = "id";
const string xmlstr::XML_ATTRTYPE_KEY           = "key";
const string xmlstr::XML_ATTRTYPE_LOCUS_NAME    = "locus";
const string xmlstr::XML_ATTRTYPE_NAME          = "name";
const string xmlstr::XML_ATTRTYPE_SOURCE        = "source";
const string xmlstr::XML_ATTRTYPE_TARGET        = "target";
const string xmlstr::XML_ATTRTYPE_TYPE          = "type";
const string xmlstr::XML_ATTRTYPE_VALUE         = "value";
const string xmlstr::XML_ATTRTYPE_VERSION       = "version";

const string xmlstr::XML_TAG_ALLELES            = "alleles";
const string xmlstr::XML_TAG_ALPHA              = "alpha";
const string xmlstr::XML_TAG_ANALYSIS           = "analysis";
const string xmlstr::XML_TAG_ANCESTOR           = "ancestor";
const string xmlstr::XML_TAG_ARGDATA            = "argdata";

#ifdef LAMARC_QA_TREE_DUMP
const string xmlstr::XML_TAG_ARGFILE_PREFIX     = "argfile-prefix";
#endif // LAMARC_QA_TREE_DUMP

const string xmlstr::XML_TAG_AUTOCORRELATION    = "autocorrelation";
const string xmlstr::XML_TAG_BASE_FREQS         = "base-freqs";
const string xmlstr::XML_TAG_BAYESIAN           = "bayesian";
const string xmlstr::XML_TAG_BAYESIAN_ANALYSIS  = "bayesian-analysis";
const string xmlstr::XML_TAG_BLOCK              = "block";
const string xmlstr::XML_TAG_CALCULATED         = "calculated";
const string xmlstr::XML_TAG_CATEGORIES         = "categories";
const string xmlstr::XML_TAG_CHAINS             = "chains";
const string xmlstr::XML_TAG_COALESCENCE        = "coalescence";
const string xmlstr::XML_TAG_CONSTRAINTS        = "constraints";
const string xmlstr::XML_TAG_CONVERT_OUTPUT     = "convert-output-to-eliminate-zero";
const string xmlstr::XML_TAG_CREATING           = "creating";
const string xmlstr::XML_TAG_CURVEFILE_PREFIX   = "curvefile-prefix";
const string xmlstr::XML_TAG_DATA               = "data";
const string xmlstr::XML_TAG_DATABLOCK          = "datablock";
const string xmlstr::XML_TAG_DISCARD            = "discard";
const string xmlstr::XML_TAG_DISEASE            = "disease";
const string xmlstr::XML_TAG_DISEASELOCATION    = "location";
const string xmlstr::XML_TAG_DISEASESTATUS      = "disease-status";
const string xmlstr::XML_TAG_DIVERGENCE         = "divergence";
const string xmlstr::XML_TAG_DIVMIG             = "divergence-migration";
const string xmlstr::XML_TAG_ECHO               = "echo";
const string xmlstr::XML_TAG_EDGE               = "edge";
const string xmlstr::XML_TAG_EDGEDEFAULT        = "edgedefault";
const string xmlstr::XML_TAG_EFFECTIVE_POPSIZE  = "effective-popsize";
const string xmlstr::XML_TAG_END                = "end";
const string xmlstr::XML_TAG_EPOCH_BOUNDARY     = "epoch-boundary";
const string xmlstr::XML_TAG_EPOCHSIZEARRANGER  = "epoch-size";
const string xmlstr::XML_TAG_FINAL              = "final";
const string xmlstr::XML_TAG_FOR                = "for";
const string xmlstr::XML_TAG_FORCES             = "forces";
const string xmlstr::XML_TAG_FORMAT             = "format";
const string xmlstr::XML_TAG_GENOTYPE           = "genotype";
const string xmlstr::XML_TAG_GENOTYPE_RESOLUTIONS = "genotype-resolutions";
const string xmlstr::XML_TAG_GRAPH              = "graph";
const string xmlstr::XML_TAG_GROUP              = "group";
const string xmlstr::XML_TAG_GROWTH             = "growth";
const string xmlstr::XML_TAG_GTRRATES           = "gtr-rates";
const string xmlstr::XML_TAG_HAPLOTYPES         = "haplotypes";
const string xmlstr::XML_TAG_HAPLOTYPING        = "haplotyping";
const string xmlstr::XML_TAG_HAP_COUNT          = "samples-per-individual";
const string xmlstr::XML_TAG_HEATING            = "heating";
const string xmlstr::XML_TAG_HEATING_STRATEGY   = "adaptive";
const string xmlstr::XML_TAG_INDIVIDUAL         = "individual";
const string xmlstr::XML_TAG_INITIAL            = "initial";
const string xmlstr::XML_TAG_INTERVAL           = "interval";
const string xmlstr::XML_TAG_IN_SUMMARY_FILE    = "in-summary-file";
const string xmlstr::XML_TAG_ISOPT              = "optimize";
const string xmlstr::XML_TAG_KEY                = "key";
const string xmlstr::XML_TAG_LAMARC             = "lamarc";
const string xmlstr::XML_TAG_LENGTH             = "length";
const string xmlstr::XML_TAG_LOCATIONS          = "locations";
const string xmlstr::XML_TAG_LOCUSARRANGER      = "trait-arranger";
const string xmlstr::XML_TAG_LOGISTICSELECTION  = "logistic-selection-coefficient";
const string xmlstr::XML_TAG_STOCHASTICSELECTION = "stochastic-selection";
const string xmlstr::XML_TAG_MANY_ARGFILES      = "many-arg-files";
const string xmlstr::XML_TAG_MAPFILE            = "lamarc-map-file";
const string xmlstr::XML_TAG_MAP_POSITION       = "map-position";
const string xmlstr::XML_TAG_MARKER_WEIGHTS     = "marker-weights";
const string xmlstr::XML_TAG_MATRIX_MEMBERS     = "matrix-members";
const string xmlstr::XML_TAG_MAX_EVENTS         = "max-events";
const string xmlstr::XML_TAG_MERGING            = "merging";
const string xmlstr::XML_TAG_METHOD             = "method";
const string xmlstr::XML_TAG_MIGRATION          = "migration";
const string xmlstr::XML_TAG_MODEL              = "model";
const string xmlstr::XML_TAG_MU                 = "mu";
const string xmlstr::XML_TAG_NAME               = "name";
const string xmlstr::XML_TAG_NEWICKTREEFILE_PREFIX = "newicktreefile-prefix";
const string xmlstr::XML_TAG_NEWPOP             = "new-populations";
const string xmlstr::XML_TAG_NODE               = "node";
const string xmlstr::XML_TAG_NORMALIZE          = "normalize";
const string xmlstr::XML_TAG_NU                 = "nu";
const string xmlstr::XML_TAG_NUMBER             = "number";
const string xmlstr::XML_TAG_NUM_CATEGORIES     = "num-categories";
const string xmlstr::XML_TAG_NUM_SITES          = "sites";
const string xmlstr::XML_TAG_OFFSET             = "offset";
const string xmlstr::XML_TAG_OLD_SUMMARY_FILE   = "summary-file";
const string xmlstr::XML_TAG_OUT_SUMMARY_FILE   = "out-summary-file";
const string xmlstr::XML_TAG_OUT_XML_FILE       = "out-xml-file";
const string xmlstr::XML_TAG_PANEL              = "panel";
const string xmlstr::XML_TAG_PANELSIZE          = "panel-size";
const string xmlstr::XML_TAG_PARAMETER_FILE     = "parameter-file";
const string xmlstr::XML_TAG_PARAMINDEX         = "paramindex";
const string xmlstr::XML_TAG_PENETRANCE         = "penetrance";
const string xmlstr::XML_TAG_PER_BASE_ERROR_RATE= "per-base-error-rate";
const string xmlstr::XML_TAG_PHASE              = "phase";
const string xmlstr::XML_TAG_PHENOTYPE          = "phenotype";
const string xmlstr::XML_TAG_PHENOTYPES         = "phenotypes";
const string xmlstr::XML_TAG_PHENOTYPE_NAME     = "phenotype-name";
const string xmlstr::XML_TAG_PLOTTING           = "plotting";
const string xmlstr::XML_TAG_POPULATION         = "population";
const string xmlstr::XML_TAG_POPTREE            = "population-tree";
const string xmlstr::XML_TAG_POSSIBLE_LOCATIONS = "possible-locations";
const string xmlstr::XML_TAG_POSTERIOR          = "posterior";
const string xmlstr::XML_TAG_PRIOR              = "prior";
const string xmlstr::XML_TAG_PRIORLOWERBOUND    = "lower";
const string xmlstr::XML_TAG_PRIORUPPERBOUND    = "upper";
const string xmlstr::XML_TAG_PROBABILITIES      = "probabilities";
const string xmlstr::XML_TAG_PROFILE            = "profile";
const string xmlstr::XML_TAG_PROFILES           = "profiles";
const string xmlstr::XML_TAG_PROFILE_PREFIX     = "profile-prefix";
const string xmlstr::XML_TAG_PROGRESS_REPORTS   = "progress-reports";
const string xmlstr::XML_TAG_RANGE              = "range";
const string xmlstr::XML_TAG_RATES              = "rates";
const string xmlstr::XML_TAG_RECLOCFILE_PREFIX  = "reclocfile-prefix";
const string xmlstr::XML_TAG_RECOMBINATION      = "recombination";
const string xmlstr::XML_TAG_REGION             = "region";
const string xmlstr::XML_TAG_REGION_GAMMA       = "gamma-over-regions";
const string xmlstr::XML_TAG_RELATIVE_MURATE    = "relative-murate";

#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
const string xmlstr::XML_TAG_RELATIVE_SAMPLE_RATE    = "relative-sample-rate";
#endif

const string xmlstr::XML_TAG_REPLICATES         = "replicates";
const string xmlstr::XML_TAG_REPORT_XML_FILE    = "xml-report-file";
const string xmlstr::XML_TAG_RESIMULATING       = "resimulating";
const string xmlstr::XML_TAG_RESULTS_FILE       = "results-file";
const string xmlstr::XML_TAG_SAMPLE             = "sample";
const string xmlstr::XML_TAG_SAMPLES            = "samples";
const string xmlstr::XML_TAG_SEED               = "seed";
const string xmlstr::XML_TAG_SEED_FROM_CLOCK    = "seed-from-system-clock";
const string xmlstr::XML_TAG_SPACING            = "spacing";
const string xmlstr::XML_TAG_START              = "start";
const string xmlstr::XML_TAG_START_VALUES       = "start-values";
const string xmlstr::XML_TAG_STATUS             = "status";
const string xmlstr::XML_TAG_STAIRARRANGER      = "stick-arranger";
const string xmlstr::XML_TAG_STRATEGY           = "strategy";
const string xmlstr::XML_TAG_SWAP_INTERVAL      = "swap-interval";
const string xmlstr::XML_TAG_TEMPERATURES       = "temperatures";
const string xmlstr::XML_TAG_TRACEFILE_PREFIX   = "tracefile-prefix";
const string xmlstr::XML_TAG_TRAIT              = "trait";
const string xmlstr::XML_TAG_TRAITS             = "traits";
const string xmlstr::XML_TAG_TRAIT_NAME         = "trait-name";
const string xmlstr::XML_TAG_TREE               = "tree";
const string xmlstr::XML_TAG_TREESIZE           = "tree-size";
const string xmlstr::XML_TAG_TRUEVALUE          = "true-value";
const string xmlstr::XML_TAG_TTRATIO            = "ttratio";

#ifdef LAMARC_QA_TREE_DUMP
const string xmlstr::XML_TAG_USE_ARGFILES       = "use-argfiles";
#endif // LAMARC_QA_TREE_DUMP

const string xmlstr::XML_TAG_USE_CURVEFILES     = "use-curvefiles";
const string xmlstr::XML_TAG_USE_IN_SUMMARY     = "use-in-summary";
const string xmlstr::XML_TAG_USE_NEWICKTREEFILE = "use-newicktreefile";
const string xmlstr::XML_TAG_USE_OUT_SUMMARY    = "use-out-summary";
const string xmlstr::XML_TAG_USE_RECLOCFILE     = "use-reclocfile";
const string xmlstr::XML_TAG_USE_TRACEFILE      = "use-tracefile";
const string xmlstr::XML_TAG_VERBOSITY          = "verbosity";

const string xmlstr::XML_ATTRVALUE_ASITES        = "asites";
const string xmlstr::XML_ATTRVALUE_CURVE         = "curve";
const string xmlstr::XML_ATTRVALUE_DOUBLE        = "double";
const string xmlstr::XML_ATTRVALUE_KNOWN         = "known";
const string xmlstr::XML_ATTRVALUE_LIVE_SITES    = "live_sites";
const string xmlstr::XML_ATTRVALUE_LONG          = "long";
const string xmlstr::XML_ATTRVALUE_NLABEL        = "nlabel";
const string xmlstr::XML_ATTRVALUE_NODE_LABEL    = "node_label";
const string xmlstr::XML_ATTRVALUE_NODE_TIME     = "node_time";
const string xmlstr::XML_ATTRVALUE_NODE_TYPE     = "node_type";
const string xmlstr::XML_ATTRVALUE_NTIME         = "ntime";
const string xmlstr::XML_ATTRVALUE_NTYPE         = "ntype";
const string xmlstr::XML_ATTRVALUE_OFF           = "off";
const string xmlstr::XML_ATTRVALUE_PARTITIONS    = "partitions";
const string xmlstr::XML_ATTRVALUE_PTYPE         = "ptype";
const string xmlstr::XML_ATTRVALUE_REC_LOCATION  = "rec_location";
const string xmlstr::XML_ATTRVALUE_RLOC          = "rloc";
const string xmlstr::XML_ATTRVALUE_STRING        = "string";
const string xmlstr::XML_ATTRVALUE_TRANS_SITES   = "transmitted_sites";
const string xmlstr::XML_ATTRVALUE_UNKNOWN       = "unknown";

const string xmlstr::XML_BRANCHTYPE_COAL      = "Coal";
const string xmlstr::XML_BRANCHTYPE_DISEASE   = "Disease";
const string xmlstr::XML_BRANCHTYPE_DIVMIG    = "DivMig";
const string xmlstr::XML_BRANCHTYPE_EPOCH     = "Epoch";
const string xmlstr::XML_BRANCHTYPE_MIG       = "Mig";
const string xmlstr::XML_BRANCHTYPE_REC       = "Rec";
const string xmlstr::XML_BRANCHTYPE_TIP       = "Tip";

// ignoring case, xmlstr::XML_ATTRVALUE must be the same as
// lamarcstrings::shortStickExpName for use in
// stringx.cpp::StringMatchesGrowthType() used by
// stringx.cpp::ProduceGrowthTypeOrBarf()

const string xmlstr::XML_ATTRVALUE_STICKEXP = "stick-exp";
const string xmlstr::XML_ATTRVALUE_STICK    = "stick";

const string xmlstr::XML_WARN_DEPRECATED_TAG_0  = "WARNING: ignoring deprecated tag <";
const string xmlstr::XML_WARN_DEPRECATED_TAG_1  = "> appearing under tag <";
const string xmlstr::XML_WARN_DEPRECATED_TAG_2  = "> near line ";

const string xmlstr::DATA_MODEL_F84 = "F84";

// const string xmlstr::WHAT_DOES_VALGRIND_THINK = "as;dlfjas;ldfkj;sldkjf";

//____________________________________________________________________________________
