// $Id: gc_block_dialogs.h,v 1.7 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_BLOCK_DIALOGS_H
#define GC_BLOCK_DIALOGS_H

#include "gc_quantum.h"
#include "gc_dialog.h"

class gcBlockPopChoice : public gcChoiceObject
{
  private:
    gcBlockPopChoice();               // undefined
    size_t                          m_blockId;
    size_t                          m_popId;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcBlockPopChoice(size_t blockId, size_t popId);
    ~gcBlockPopChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;
    size_t      GetRelevantId();
};

class gcBlockLocusChoice : public gcChoiceObject
{
  private:
    gcBlockLocusChoice();           // undefined
    size_t                          m_blockId;
    size_t                          m_locusId;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcBlockLocusChoice(size_t blockId, size_t locusId);
    ~gcBlockLocusChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;
    size_t      GetRelevantId();
};

class gcBlockFileInfo : public gcPlainTextHelper
{
  private:
    gcBlockFileInfo();              // undefined
    size_t                          m_blockId;
  protected:
  public:
    gcBlockFileInfo(size_t blockId);
    virtual ~gcBlockFileInfo();
    wxString FromDataStore(GCDataStore&);
};

class gcBlockPopIndex : public gcPlainTextHelper
{
  private:
    gcBlockPopIndex();              // undefined
    size_t                          m_blockId;
  protected:
  public:
    gcBlockPopIndex(size_t blockId);
    virtual ~gcBlockPopIndex();
    wxString FromDataStore(GCDataStore&);
};

class gcBlockLocusIndex : public gcPlainTextHelper
{
  private:
    gcBlockLocusIndex();              // undefined
    size_t                          m_blockId;
  protected:
  public:
    gcBlockLocusIndex(size_t blockId);
    virtual ~gcBlockLocusIndex();
    wxString FromDataStore(GCDataStore&);
};

class gcBlockPloidyInfo : public gcPlainTextHelper
{
  private:
    gcBlockPloidyInfo();              // undefined
    size_t                          m_blockId;
  protected:
  public:
    gcBlockPloidyInfo(size_t blockId);
    virtual ~gcBlockPloidyInfo();
    wxString FromDataStore(GCDataStore&);
};

class gcBlockEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_blockId;
    void DoDelete();
  public:
    gcBlockEditDialog(wxWindow *    parentWindow,
                      GCDataStore &   dataStore,
                      size_t          blockId);
    virtual ~gcBlockEditDialog();
};

bool DoDialogEditBlock( wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          locusId);

class gcActor_BlockEdit : public gcEventActor
{
  private:
    gcActor_BlockEdit();     // undefined
    size_t                  m_blockId;
  public:
    gcActor_BlockEdit(size_t blockId) : m_blockId(blockId) {};
    virtual ~gcActor_BlockEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_BLOCK_DIALOGS_H

//____________________________________________________________________________________
