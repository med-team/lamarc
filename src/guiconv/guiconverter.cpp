// $Id: guiconverter.cpp,v 1.36 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


/* NOTE:  8/29/2014 Mary edited gc_migtab.cpp to reverse the sense
of "from" and "to" in the migration matrix, since it was producing
XML files that were backwards to what LAMARC assumes.  She did NOT
change variable names or anything else to match, so there are probably
misleading variable names in this part of the converter and in the
corresponding XML output routines.  Maintainers beware!  DEBUG */

#include "guiconverter.h"
#include "gc_cmdline.h"
#include "gc_errhandling.h"
#include "gc_event_ids.h"
#include "gc_event_publisher.h"
#include "gc_frame.h"
#include "gc_layout.h"
#include "gc_strings.h"

#include "giraffe32.xpm"

#include "tinyxml.h"

#include "wx/cmdline.h"
#include "wx/icon.h"
#include "wx/log.h"

#ifdef LAMARC_COMPILE_MSWINDOWS
#include "wincon.h"
#endif

GuiConverterApp::GuiConverterApp()
{
}

GuiConverterApp::~GuiConverterApp()
{
}

IMPLEMENT_APP(GuiConverterApp)

void
GuiConverterApp::OnInitCmdLine(wxCmdLineParser& parser)
{
    wxApp::OnInitCmdLine(parser);
    GCCmdLineManager::AddOptions(parser);
}

bool
GuiConverterApp::OnCmdLineParsed(wxCmdLineParser& parser)
{
    bool parentReturned = wxApp::OnCmdLineParsed(parser);
    GCCmdLineManager::ExtractValues(parser);
    return parentReturned;
}

bool
GuiConverterApp::OnInit()
{
    // use the parent's OnInit because it includes command line parsing
    if(wxApp::OnInit())
    {
        if(m_batchOnly)
        {
#ifdef LAMARC_COMPILE_MSWINDOWS
            AttachConsole(-1);
            // this should be better but mingw needs patching
            // AttachConsole(ATTACH_PARENT_PROCESS);
#else
            wxLog::SetActiveTarget(new wxLogStderr());
#endif
            GCCmdLineManager::ProcessCommandLineAndCommandFile(m_logic);
            GCCmdLineManager::DoExport(m_logic);
            wxExit();
            return false;
        }
        else
        {
            if(wxApp::OnInitGui())
            {
                int appWidth  = gclayout::appWidth;
                int appHeight = gclayout::appHeight;
                wxSize maxSize = wxGetDisplaySize();
                int maxWidth = maxSize.GetWidth()  * gclayout::appWidthPercent  / 100;
                int maxHeight= maxSize.GetHeight() * gclayout::appHeightPercent / 100;
                if(appWidth > maxWidth)
                {
                    appWidth = maxWidth;
                }
                if(appHeight > maxHeight)
                {
                    appHeight = maxHeight;
                }

                wxSize bestSize(appWidth,appHeight);

                GCFrame * m_mainFrame = new GCFrame(gcstr::converterTitle,m_logic);
                m_logic.SetDisplayParent(m_mainFrame);
                m_mainFrame->SetSize(bestSize);
                m_mainFrame->CentreOnScreen();
                m_mainFrame->Show(true);
                SetTopWindow(m_mainFrame);

                m_mainFrame->SetIcon( wxICON( giraffe32) );

                GCCmdLineManager::ProcessCommandLineAndCommandFile(m_logic);
                PublishDataEvent(m_mainFrame->GetEventHandler(),D2S_UserInteractionPhaseEnd);

                return true;
            }
        }
    }
    return false;
}

int
GuiConverterApp::OnRun()
{
    try
    {
        int result = wxApp::OnRun();
        return result;
    }
    catch(const gc_fatal_error& e)
    {
        wxLogError(wxString::Format(gcerr::fatalError));
        return 2;
    }
    catch(const std::exception& f)
    {
        wxLogError(wxString::Format(gcerr::uncaughtException,f.what()));
        return 2;
    }
    return 3;       // EWFIX.P3 -- what should this be?
}

int
GuiConverterApp::OnExit()
{
    if(m_doDebugDump)
    {
        m_logic.DebugDump();
    }
    if(! m_batchOutName.IsEmpty())
    {
        TiXmlDocument * doc = m_logic.ExportBatch();
        m_logic.WriteBatchFile(doc,m_batchOutName);
        delete doc;
    }

    m_logic.NukeContents();

    return wxApp::OnExit();
}

//____________________________________________________________________________________
