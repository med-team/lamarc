// $Id: gc_region_dialogs.h,v 1.15 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_REGION_DIALOGS_H
#define GC_REGION_DIALOGS_H

#include "gc_dialog.h"
#include "gc_quantum.h"
#include "gc_validators.h"

class GCDataStore;
class wxWindow;

//------------------------------------------------------------------------------------

class gcRegionRename : public gcTextHelper
{
  private:
    gcRegionRename();       // undefined
    size_t                  m_regionId;
  protected:
  public:
    gcRegionRename(size_t regionId);
    ~gcRegionRename();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

//------------------------------------------------------------------------------------

class gcRegionEffPopSize : public gcTextHelper
{
  private:
    gcRegionEffPopSize();       // undefined
    size_t                      m_regionId;
    GCPositiveFloatValidator    m_validator;
  protected:
  public:
    gcRegionEffPopSize(size_t regionId);
    ~gcRegionEffPopSize();

    wxString            FromDataStore(GCDataStore &);
    void                ToDataStore(GCDataStore &, wxString newText);
    const wxValidator & GetValidator();
    wxString            InitialString();
};

//------------------------------------------------------------------------------------

class gcRegionSamples : public gcTextHelper
{
  private:
    gcRegionSamples();       // undefined
    size_t                      m_regionId;
    GCPositiveFloatValidator    m_validator;
  protected:
  public:
    gcRegionSamples(size_t regionId);
    ~gcRegionSamples();

    wxString            FromDataStore(GCDataStore &);
    void                ToDataStore(GCDataStore &, wxString newText);
    const wxValidator & GetValidator();
    wxString            InitialString();
};

//------------------------------------------------------------------------------------

class gcRegionMergeChoice : public gcChoiceObject
{
  private:
    gcRegionMergeChoice();      // undefined
    size_t                      m_choiceRegionId;
    size_t                      m_dialogRegionId;
    wxCheckBox *                m_box;
  protected:
  public:
    gcRegionMergeChoice(size_t choiceRegionId, size_t dialogRegionId);
    ~gcRegionMergeChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

//------------------------------------------------------------------------------------

class gcRegionMerge : public gcUpdatingChooseMulti
{
  private:
    size_t          m_regionId;
  protected:
  public:
    gcRegionMerge(  wxWindow *                      parent,
                    size_t                          regionId,
                    std::vector<gcChoiceObject*>    choices);
    ~gcRegionMerge();

    void    DoFinalForMulti(GCDataStore & dataStore, gcIdVec selectedChoices);
    wxString    NoChoicesText() const;
};

//------------------------------------------------------------------------------------

class gcRegionFragmentChoice : public gcChoiceObject
{
  private:
    gcRegionFragmentChoice();
  protected:
    size_t                      m_regionId;
    wxCheckBox *                m_box;
  public:
    gcRegionFragmentChoice(size_t regionId);
    ~gcRegionFragmentChoice();

    void        UpdateDisplayInitial    (GCDataStore &);
    void        UpdateDisplayInterim    (GCDataStore &);
    void        UpdateDataInterim       (GCDataStore &);
    void        UpdateDataFinal         (GCDataStore &);

    wxWindow *  MakeWindow(wxWindow * parent);
    wxWindow *  FetchWindow();

    size_t      GetRelevantId();
};

class gcRegionFragmenter : public gcUpdatingChoose
{
  private:
    size_t          m_regionId;
  protected:
  public:
    gcRegionFragmenter( wxWindow *                      parent,
                        size_t                          regionId,
                        std::vector<gcChoiceObject*>    choices);
    ~gcRegionFragmenter();
};

//------------------------------------------------------------------------------------

class gcRegionEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_regionId;
    void DoDelete();
  public:
    gcRegionEditDialog( wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          regionId,
                        bool            forJustCreatedObj);
    virtual ~gcRegionEditDialog();
};

//------------------------------------------------------------------------------------

bool DoDialogEditRegion(wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          regionId,
                        bool            forJustCreatedObj);

//------------------------------------------------------------------------------------

class gcActor_RegionAdd : public gcEventActor
{
  public:
    gcActor_RegionAdd() {};
    virtual ~gcActor_RegionAdd() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

//------------------------------------------------------------------------------------

class gcActor_RegionEdit : public gcEventActor
{
  private:
    gcActor_RegionEdit();       // undefined
    size_t                      m_regionId;

  public:
    gcActor_RegionEdit(size_t regionId) : m_regionId(regionId) {};
    virtual ~gcActor_RegionEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_REGION_DIALOGS_H

//____________________________________________________________________________________
