// $Id: gc_panel_dialogs.h,v 1.3 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PANEL_DIALOGS_H
#define GC_PANEL_DIALOGS_H

#include "gc_dialog.h"
#include "gc_quantum.h"
#include "gc_validators.h"

class GCDataStore;
class wxWindow;

//------------------------------------------------------------------------------------

class gcPanelRename : public gcTextHelper
{
  private:
    gcPanelRename();       // undefined
    size_t                  m_panelId;
  protected:
  public:
    gcPanelRename(size_t panelId);
    ~gcPanelRename();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

//------------------------------------------------------------------------------------

class gcPanelMemberCount : public gcTextHelper
{
  private:
    gcPanelMemberCount();       // undefined
    size_t                      m_panelId;
    GCPositiveFloatValidator    m_validator;
  protected:
  public:
    gcPanelMemberCount(size_t panelId);
    ~gcPanelMemberCount();

    wxString            FromDataStore(GCDataStore &);
    void                ToDataStore(GCDataStore &, wxString newText);
    const wxValidator & GetValidator();
    wxString            InitialString();
};
//------------------------------------------------------------------------------------

class gcPanelEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_panelId;
  public:
    gcPanelEditDialog( wxWindow *      parentWindow,
                       GCDataStore &   dataStore,
                       size_t          panelId,
                       bool            forJustCreatedObj);
    virtual ~gcPanelEditDialog();
};

//------------------------------------------------------------------------------------

bool DoDialogEditPanel(wxWindow *      parentWindow,
                       GCDataStore &   dataStore,
                       size_t          regionId,
                       bool            forJustCreatedObj);



//------------------------------------------------------------------------------------

class gcActor_PanelEdit : public gcEventActor
{
  private:
    gcActor_PanelEdit();       // undefined
    size_t                      m_panelId;

  public:
    gcActor_PanelEdit(size_t panelId) : m_panelId(panelId) {};
    virtual ~gcActor_PanelEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_PANEL_DIALOGS_H
//____________________________________________________________________________________
