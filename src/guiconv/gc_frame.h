// $Id: gc_frame.h,v 1.57 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FRAME_H
#define GC_FRAME_H

#include "wx/wx.h"
#include "wx/notebook.h"

class wxSplitterWindow;
class GCLogic;
class gcInfoPane ;
class GCAssignmentTab ;
class gcMigTab;

class GCFrame : public wxFrame
// outer-most frame in the converter application
{
  private:
    GCLogic         &   m_logic;    // interface with back end storage

    wxPanel             * m_logPanel;
    wxTextCtrl          * m_logText;
    wxLog               * m_oldLog;
    wxPanel             * m_basePanel;
    wxBookCtrlBase      * m_bookCtrl;
    wxBoxSizer          * m_sizerFrame;

    gcInfoPane          * m_filePanel;
    GCAssignmentTab     * m_gridPanel;
    gcMigTab            * m_migPanel;

    wxWindow *m_topLogWin;
    wxWindow *m_botLogWin;

    wxMenu          *   m_fileMenu;
    wxMenu          *   m_insertMenu;
    wxMenu          *   m_viewMenu;

    wxMenuItem      *   m_verbose;

    void DispatchDataEvent              (wxCommandEvent& event);
    void DispatchMenuEvent              (wxCommandEvent& event);
    void DispatchScreenEvent            (wxCommandEvent& event);

    void SetUpMenus                     ();
    void EnableMenus                    ();

  protected:

  public:
    GCFrame();
    GCFrame(const wxString& title, GCLogic & logic);
    virtual ~GCFrame();

    void UpdateUserCues                 ();
    void OnNotebookCtrl(wxNotebookEvent& event);

    DECLARE_EVENT_TABLE()
};

// name for each notebook page
#define DATA_FILES        wxT("Data Files")
#define DATA_PARTITIONS   wxT("Data Partitions")
#define MIGRATION_MATRIX  wxT("Migration Matrix")
#define LOG_TEXT          wxT("Debug Log")

#endif  // GC_FRAME_H

//____________________________________________________________________________________
