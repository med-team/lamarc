// $Id: gc_unit_dialogs.cpp,v 1.9 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_strings.h"
#include "gc_unit_dialogs.h"
#include "wx/filedlg.h"

//------------------------------------------------------------------------------------

wxString DoDialogSelectHapFile(wxWindow * parentWindow,GCDataStore & dataStore)
{
    wxString hapFileName = wxEmptyString;
    wxFileDialog hapFileDialog( parentWindow,
                                gcstr::hapFileSelect,
                                wxEmptyString,  // default directory == current
                                wxEmptyString,  // default file = none
                                gcstr::globAll, // show all files
                                wxOPEN);
    if(hapFileDialog.ShowModal() == wxID_OK)
    {
        hapFileName = hapFileDialog.GetPath();
    }

    return hapFileName;
}

//____________________________________________________________________________________
