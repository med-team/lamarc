// $Id: gc_event_publisher.h,v 1.18 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_EVENT_PUBLISHER_H
#define GC_EVENT_PUBLISHER_H

#include <map>

#include "wx/event.h"       // for wxEventType

class GCQuantum;
class gcEventActor;

// defines events signalling a change in values in the
// datastore. These events are intended to be subscribed
// to by GUI elements which need to update their appearance
// when data changes.
extern const wxEventType DATA_2_SCREEN;

// event table entry capable of dispatching a DATA_2_SCREEN
// event to the method which handles it
#define EVT_D2S(ev,fn)                                  \
    DECLARE_EVENT_TABLE_ENTRY(                          \
        DATA_2_SCREEN, ev, wxID_ANY,                    \
        (wxObjectEventFunction)(wxEventFunction)&fn,    \
        (wxObject *) NULL                               \
        ),

// defines events signalling a change created from the GUI
extern const wxEventType SCREEN_2_DATA;
// event table entry capable of dispatching a SCREEN_2_DATA
// event to the method which handles it
#define EVT_S2D(ev,fn)                                  \
    DECLARE_EVENT_TABLE_ENTRY(                          \
        SCREEN_2_DATA, ev, wxID_ANY,                    \
        (wxObjectEventFunction)(wxEventFunction)&fn,    \
        (wxObject *) NULL                               \
        ),

void PublishDataEvent(wxEvtHandler*,int eventId);

void PublishScreenEvent(wxEvtHandler*, gcEventActor *);

#if 0
void PublishScreenEvent(wxEvtHandler*,int eventId);
void PublishScreenEvent(wxEvtHandler*,int eventId,int);
void PublishScreenEvent(wxEvtHandler*,int eventId,wxString);
void PublishScreenEvent(wxEvtHandler*,int eventId,const GCQuantum*);
void PublishScreenEvent(wxEvtHandler*,int eventId,const GCQuantum*,int);
void PublishScreenEvent(wxEvtHandler*,int eventId,const GCQuantum*,wxString);
#endif

// we need these so we can build a multimap to represent a
// wxEvtHandler that wants to be notified about events of
// a certain type and id. A complication is that the event
// id (an integer -- second member of the GCEventDescriptor
// pair) can legally be wxID_ANY -- meaning I want to know
// about *all* events of a given type
typedef std::pair<wxEventType,int> GCEventDescriptor;
typedef std::pair<GCEventDescriptor, wxEvtHandler *> GCSubscriberPair;
typedef std::multimap<GCEventDescriptor, wxEvtHandler *> GCSubscriberMap;
typedef GCSubscriberMap::iterator GCSubscriberMapIter;
typedef std::pair<GCSubscriberMapIter,GCSubscriberMapIter> GCSubscriberMapIterPair;

#endif  // GC_EVENT_PUBLISHER_H

//____________________________________________________________________________________
