// $Id: gc_parent_dialogs.h,v 1.3 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARENT_DIALOGS_H
#define GC_PARENT_DIALOGS_H

#include "gc_dialog.h"
#include "gc_quantum.h"
#include "gc_validators.h"
#include "gc_default.h"

class GCDataStore;
class wxWindow;

//------------------------------------------------------------------------------------
class gcUpdatingChooseTwo : public gcUpdatingChooseMulti
{
  private:
    size_t m_childParentId;
  public:
    gcUpdatingChooseTwo(  wxWindow *                      parent,
                          wxString                        instructions,
                          std::vector<gcChoiceObject*>    choices);
    virtual ~gcUpdatingChooseTwo();
    virtual void    BuildDisplay(GCDataStore &);
    virtual void    OnCheck(wxCommandEvent &);
    virtual void    UpdateDataFinal(GCDataStore &);
    virtual void    DoFinalForMulti(GCDataStore &, gcIdVec checkedIds);
    virtual void    OnUnselectAll(wxCommandEvent &);
    void    SetChildParentId(size_t childParentId);
    void    ParentLevelUp(GCDataStore & dataStore, size_t parentId);
    size_t  GetChildParentId ();
};

//------------------------------------------------------------------------------------

class gcParentRename : public gcTextHelper
{
  private:
    size_t m_parentId;
  public:
    gcParentRename(size_t parentId);
    ~gcParentRename();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

//------------------------------------------------------------------------------------

class gcParentChildChoice : public gcChoiceObject
{
  private:
    gcParentChildChoice();         // undefined
    size_t                      m_popId;
    size_t                      m_parId;
    wxCheckBox *                m_box;
  protected:
  public:
    gcParentChildChoice(size_t popId, size_t parentId);
    ~gcParentChildChoice();

    wxString    GetLabel(GCDataStore &);
    bool        GetEnabled(GCDataStore &);
    void        SetEnabled(GCDataStore &, bool value);
    bool        GetSelected(GCDataStore &);
    void        SetSelected(GCDataStore &, bool value);

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent);
    wxWindow *  FetchWindow();

    size_t      GetRelevantId();

};

//------------------------------------------------------------------------------------

class gcParentChild : public gcUpdatingChooseTwo
{
  public:
    gcParentChild(wxWindow *                      parent,
                  size_t                          parentId,
                  std::vector<gcChoiceObject*>    choices);
    ~gcParentChild();

    wxString    NoChoicesText() const;
};

//------------------------------------------------------------------------------------

class gcParentEditDialog : public gcUpdatingDialog //public wxDialog
{
  private:
  protected:
    size_t          m_parentId;
    GCDataStore &   m_dataStore;  //needed so it can decide whether to recurse or exit in OnButton
  public:
    gcParentEditDialog( wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          parentId,
                        wxString        title,
                        bool            forJustCreatedObj);
    virtual ~gcParentEditDialog();
    //virtual void    OnApply(wxCommandEvent & event);

    //DECLARE_EVENT_TABLE()
};

//------------------------------------------------------------------------------------

bool DoDialogEditParent(wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          regionId,
                        bool            forJustCreatedObj);



//------------------------------------------------------------------------------------

class gcActor_ParentNew : public gcEventActor
{
  private:

  public:
    gcActor_ParentNew() {};
    virtual ~gcActor_ParentNew() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

//------------------------------------------------------------------------------------

class gcActor_ParentEdit : public gcEventActor
{
  private:
    size_t                      m_parentId;

  public:
    gcActor_ParentEdit(size_t parentId) : m_parentId(parentId) {};
    //gcActor_ParentEdit(parentId) : m_parentId(parentId) {};
    virtual ~gcActor_ParentEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_PANEL_DIALOGS_H
//____________________________________________________________________________________
