// $Id: gc_event_publisher.cpp,v 1.21 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <set>

#include "gc_event_ids.h"
#include "gc_event_publisher.h"
#include "gc_quantum.h"
#include "wx/clntdata.h"
#include "wx/event.h"

const wxEventType DATA_2_SCREEN = wxNewEventType();
const wxEventType SCREEN_2_DATA = wxNewEventType();

void PublishDataEvent(wxEvtHandler* handler,int eventId)
{
    wxCommandEvent myEvent(DATA_2_SCREEN,eventId);
    wxPostEvent(handler,myEvent);
}

#if 0

void PublishDataEvent(wxEvtHandler* handler,int eventId, const GCQuantum* obj)
{
    wxCommandEvent myEvent(DATA_2_SCREEN,eventId);
    wxClientData * clientP = new GCClientData(obj);
    myEvent.SetClientObject(clientP);
    wxPostEvent(handler,myEvent);
}

void PublishScreenEvent(wxEvtHandler* handler,int eventId)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,eventId);
    wxPostEvent(handler,myEvent);
}

void PublishScreenEvent(wxEvtHandler* handler,int eventId,int intData)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,eventId);
    myEvent.SetInt(intData);
    wxPostEvent(handler,myEvent);
}

void PublishScreenEvent(wxEvtHandler* handler,int eventId,wxString stringData)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,eventId);
    myEvent.SetString(stringData);
    wxPostEvent(handler,myEvent);
}

#endif

void PublishScreenEvent(wxEvtHandler* handler, gcEventActor * obj)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,gcEvent_Generic);
    wxClientData * clientP = new GCClientData(obj);
    myEvent.SetClientObject(clientP);
    wxPostEvent(handler,myEvent);
}

#if 0

void PublishScreenEvent(wxEvtHandler* handler,int eventId, const GCQuantum* obj,int intData)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,eventId);
    wxClientData * clientP = new GCClientData(obj);
    myEvent.SetClientObject(clientP);
    myEvent.SetInt(intData);
    wxPostEvent(handler,myEvent);
}

void PublishScreenEvent(wxEvtHandler* handler,int eventId, const GCQuantum* obj,wxString stringData)
{
    wxCommandEvent myEvent(SCREEN_2_DATA,eventId);
    wxClientData * clientP = new GCClientData(obj);
    myEvent.SetClientObject(clientP);
    myEvent.SetString(stringData);
    wxPostEvent(handler,myEvent);
}

#endif

//____________________________________________________________________________________
