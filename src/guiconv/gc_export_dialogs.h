// $Id: gc_export_dialogs.h,v 1.7 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_EXPORT_DIALOGS_H
#define GC_EXPORT_DIALOGS_H

#include "wx/dialog.h"
#include "wx/string.h"

enum GCSuspiciousLocusChoice
{
    badlocus_CANCEL,
    badlocus_CONTINUE,
    badlocus_EDIT
};

enum GCSuspiciousRegionChoice
{
    badregion_CANCEL,
    badregion_CONTINUE,
    badregion_EDIT
};

class GCBadLocusLengthDialog : public wxDialog
{
  private:
    wxArrayString               m_regionNameList;
    GCSuspiciousLocusChoice     m_nextStep;
  public:
    GCBadLocusLengthDialog(  wxWindow   * parent, wxArrayString nameList);
    virtual ~GCBadLocusLengthDialog() {};
    void Init();

    GCSuspiciousLocusChoice GetChoice()  { return m_nextStep;};

    DECLARE_EVENT_TABLE()

    virtual void OnTakeContinue(wxCommandEvent& event);
    virtual void OnTakeCancel(wxCommandEvent& event);
    virtual void OnTakeEdit(wxCommandEvent& event);
};

#endif  // GC_EXPORT_DIALOGS_H

//____________________________________________________________________________________
