// $Id: gc_locitab.h,v 1.9 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOCITAB_H
#define GC_LOCITAB_H

#include "gc_gridpanel.h"

class wxWindow;

class gcLociPane : public gcGridPane
{
  private:
    gcLociPane();            // undefined
  protected:
  public:
    gcLociPane(wxWindow * parent);
    virtual ~gcLociPane();

    virtual void NotifyLeftDClick(size_t row, size_t col);
};

class gcLociTab : public gcInfoPane
{
  private:
    gcLociTab();        // undefined

  protected:
    wxPanel *   MakeContent();
    wxString    MakeLabel();

  public:
    gcLociTab(wxWindow * parent, GCLogic & logic);
    virtual ~gcLociTab();
};

#endif  // GC_LOCITAB_H

//____________________________________________________________________________________
