// $Id: gc_logic.cpp,v 1.59 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <vector>

#include "gc_dialog.h"
#include "gc_logic.h"
#include "gc_strings.h"

#include "wx/string.h"
#include "wx/utils.h"

GCLogic::GCLogic()
    : m_displayParent(NULL)
{
}

GCLogic::~GCLogic()
{
}

void
GCLogic::SetDisplayParent(wxWindow * win)
{
    m_displayParent = win;
}

void
GCLogic::GCFatalBatchWarnGUI(wxString msg) const
{
    if(m_displayParent == NULL)
    {
        GCFatal(msg);
    }
    else
    {
        GCWarning(msg);
    }
}

void
GCLogic::GCError(wxString msg) const
{
    if(m_displayParent != NULL)
    {
        wxMessageDialog dialog(m_displayParent,msg,gcstr::error,wxOK|wxICON_ERROR);
        dialog.ShowModal();
    }
    else
    {
        GCDataStore::GCError(msg);
    }
}

void
GCLogic::GCInfo(wxString msg) const
{
    if(m_displayParent != NULL)
    {
        wxMessageDialog dialog(m_displayParent,msg,gcstr::information,wxOK|wxICON_INFORMATION);
        dialog.ShowModal();
    }
    else
    {
        GCDataStore::GCInfo(msg);
    }
}

void
GCLogic::GCWarning(wxString msg) const
{
    if(m_displayParent != NULL)
    {
        wxMessageDialog dialog(m_displayParent,msg,gcstr::warning,wxOK|wxICON_EXCLAMATION);
        dialog.ShowModal();
    }
    else
    {
        GCDataStore::GCWarning(msg);
    }
}

void
GCLogic::batchFileRejectGuiLog(wxString msg, size_t lineNo) const
{
    warnLog(msg,lineNo);
}

bool
GCLogic::guiQuestionBatchLog(wxString msg, wxString stopButton, wxString continueButton) const
{
    if(m_displayParent != NULL)
    {
        wxMessageDialog md(m_displayParent,
                           msg,
                           gcstr::questionHeader,
                           wxYES_NO | wxNO_DEFAULT | wxICON_EXCLAMATION
            );
        int returnVal = md.ShowModal();
        return(returnVal == wxID_YES);

    }
    else
    {
        GCDataStore::guiQuestionBatchLog(msg,stopButton,continueButton);
    }
    return true;
}

void
GCLogic::GettingBusy(const wxString& msg) const
{
    GCDataStore::GettingBusy(msg);
    wxBeginBusyCursor();
}

void
GCLogic::LessBusy(const wxString& msg) const
{
    wxEndBusyCursor();
    GCDataStore::LessBusy(msg);
}

//____________________________________________________________________________________
