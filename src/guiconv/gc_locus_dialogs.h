// $Id: gc_locus_dialogs.h,v 1.19 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOCUS_DIALOGS_H
#define GC_LOCUS_DIALOGS_H

#include "gc_quantum.h"
#include "gc_dialog.h"
#include "gc_types.h"
#include "gc_validators.h"

class GCDataStore;
class gcLocus;
class wxWindow;

class gcLocusDataTypeChoice : public gcChoiceObject
{
  private:
    gcLocusDataTypeChoice();        // undefined
    size_t                          m_locusId;
    gcSpecificDataType              m_type;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcLocusDataTypeChoice(size_t forThisId, gcSpecificDataType type);
    ~gcLocusDataTypeChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

class gcLocusLength : public gcTextHelper
{
  private:
    gcLocusLength();                // undefined
    size_t                          m_locusId;
    GCNonNegativeIntegerValidator   m_validator;
  protected:
  public:
    gcLocusLength  (size_t locusId);
    virtual ~gcLocusLength();

    wxString                FromDataStore(GCDataStore &);
    void                    ToDataStore(GCDataStore &, wxString text);
    const wxValidator &     GetValidator();
    wxString                InitialString();
};

class gcLocusFirstPosition : public gcTextHelper
{
  private:
    gcLocusFirstPosition();              // undefined
    size_t                          m_locusId;
    GCIntegerValidator              m_validator;
  protected:
  public:
    gcLocusFirstPosition  (size_t locusId);
    virtual ~gcLocusFirstPosition();

    wxString                FromDataStore(GCDataStore &);
    void                    ToDataStore(GCDataStore &, wxString text);
    const wxValidator &     GetValidator();
    wxString                InitialString();
};

class gcLocusLocations : public gcTextHelper
{
  private:
    gcLocusLocations();             // undefined
    size_t                          m_locusId;
    GCIntegerListValidator          m_validator;
  protected:
  public:
    gcLocusLocations  (size_t locusId);
    virtual ~gcLocusLocations();

    wxString                FromDataStore(GCDataStore &);
    void                    ToDataStore(GCDataStore &, wxString text);
    const wxValidator &     GetValidator();
    wxString                InitialString();
};

class gcLocusLinkageChoice : public gcChoiceObject
{
  private:
    gcLocusLinkageChoice();         // undefined
    size_t                          m_locusId;
    bool                            m_linked;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcLocusLinkageChoice(size_t locusId, bool linked);
    ~gcLocusLinkageChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

class gcLocusMergeChoice : public gcChoiceObject
{
  private:
    gcLocusMergeChoice();        // undefined
    size_t                      m_choiceLocusId;
    size_t                      m_dialogLocusId;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcLocusMergeChoice(size_t choiceLocusId, size_t dialogLocusId);
    ~gcLocusMergeChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

class gcLocusMerge : public gcUpdatingChooseMulti
{
  private:
  protected:
    size_t          m_locusId;
  public:
    gcLocusMerge(   wxWindow *                      parent,
                    size_t                          locusId,
                    std::vector<gcChoiceObject*>    choices);
    virtual ~gcLocusMerge();

    void    DoFinalForMulti(GCDataStore & dataStore, gcIdVec selectedChoices);
    wxString    NoChoicesText() const;
};

class gcLocusPosition : public gcTextHelper
{
  private:
    gcLocusPosition();              // undefined
    size_t                          m_locusId;
    GCIntegerValidator              m_validator;
  protected:
  public:
    gcLocusPosition(size_t locusId);
    virtual ~gcLocusPosition();

    wxString                FromDataStore(GCDataStore &);
    void                    ToDataStore(GCDataStore &, wxString text);
    const wxValidator &     GetValidator();
    wxString                InitialString();
};

class gcLocusRegionChoice : public gcChoiceObject
{
  private:
    gcLocusRegionChoice();               // undefined
    size_t                          m_locusId;
    size_t                          m_regionId;
    bool                            m_newlyCreated;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcLocusRegionChoice(size_t locusId, size_t regionId, bool isNewlyCreatedParent=false);
    ~gcLocusRegionChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

class gcLocusOwnRegion : public gcChoiceObject
{
  private:
    gcLocusOwnRegion();               // undefined
    size_t                          m_locusId;
    bool                            m_newlyCreated;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcLocusOwnRegion(size_t locusId, bool isNewlyCreated=false);
    ~gcLocusOwnRegion();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

class gcLocusRename : public gcTextHelper
{
  private:
  protected:
    size_t          m_locusId;
  public:
    gcLocusRename(size_t locusId);
    ~gcLocusRename();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

class gcLocusMarkerCount : public gcPlainTextHelper
{
  private:
    gcLocusMarkerCount();             // undefined
    size_t                          m_locusId;
  protected:
  public:
    gcLocusMarkerCount(size_t locusId);
    virtual ~gcLocusMarkerCount();

    wxString FromDataStore(GCDataStore&);
};

class gcLocusEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_locusId;
    void DoDelete();
  public:
    gcLocusEditDialog(wxWindow *    parentWindow,
                      GCDataStore &   dataStore,
                      size_t          locusId,
                      bool            forJustCreatedObj);
    virtual ~gcLocusEditDialog();
};

bool DoDialogEditLocus( wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          locusId,
                        bool            forJustCreatedObj);

class gcActor_LocusAdd : public gcEventActor
{
  public:
    gcActor_LocusAdd() {};
    virtual ~gcActor_LocusAdd() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_LocusEdit : public gcEventActor
{
  private:
    gcActor_LocusEdit();     // undefined
    size_t                  m_locusId;
  public:
    gcActor_LocusEdit(size_t locusId) : m_locusId(locusId) {};
    virtual ~gcActor_LocusEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_LOCUS_DIALOGS_H

//____________________________________________________________________________________
