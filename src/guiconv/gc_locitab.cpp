// $Id: gc_locitab.cpp,v 1.11 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_event_publisher.h"
#include "gc_locitab.h"
#include "gc_locus_dialogs.h"
#include "gc_logic.h"
#include "gc_strings.h"
#include "gc_structures.h"

//------------------------------------------------------------------------------------

gcLociPane::gcLociPane(wxWindow * parent)
    :
    gcGridPane(parent,7,0)
{
}

gcLociPane::~gcLociPane()
{
}

void
gcLociPane::NotifyLeftDClick(size_t row, size_t col)
{
    assert(row < m_objVec.size());
    size_t locId = m_objVec[row];
    gcEventActor * locEditActor = new gcActor_LocusEdit(locId);
    PublishScreenEvent(GetEventHandler(),locEditActor);
}

//------------------------------------------------------------------------------------

gcLociTab::gcLociTab( wxWindow * parent, GCLogic & logic)
    :
    gcInfoPane(parent, logic, gcstr::lociTabTitle)
{
}

gcLociTab::~gcLociTab()
{
}

wxPanel *
gcLociTab::MakeContent()
{
    gcGridPane * pane = new gcLociPane(m_scrolled);

    objVector loci = m_logic.GetStructures().GetDisplayableLoci();
    for(objVector::iterator iter=loci.begin(); iter != loci.end(); iter++)
    {
        GCQuantum * quantumP = *iter;
        gcLocus * locusP = dynamic_cast<gcLocus*>(quantumP);
        assert(locusP != NULL);

        wxArrayString labels;

        labels.Add(locusP->GetName());
        labels.Add(wxString::Format(gcstr::locusLabelDataType,locusP->GetDataTypeString().c_str()));
        labels.Add(wxString::Format(gcstr::locusLabelSites,locusP->GetNumSitesString().c_str()));
        if(locusP->GetLinked())
        {
            gcRegion & regionRef = m_logic.GetStructures().GetRegion(locusP->GetRegionId());
            labels.Add(wxString::Format(gcstr::locusLabelLinked,gcstr::yes.c_str()));
            labels.Add(wxString::Format(gcstr::locusLabelLength,locusP->GetLengthString().c_str()));
            labels.Add(wxString::Format(gcstr::locusLabelRegionName,regionRef.GetName().c_str()));
            labels.Add(wxString::Format(gcstr::locusLabelMapPosition,locusP->GetMapPositionString().c_str()));
        }
        else
        {
            labels.Add(wxString::Format(gcstr::locusLabelLinked,gcstr::no.c_str()));
            labels.Add(wxEmptyString);
            labels.Add(wxEmptyString);
            labels.Add(wxEmptyString);
        }

        pane->AddRow(quantumP->GetId(),labels);
    }

    pane->Finish();
    return pane;

}

wxString
gcLociTab::MakeLabel()
{
    return wxString::Format(m_panelLabelFmt,(int)m_logic.GetStructures().GetDisplayableLocusIds().size());
}

//____________________________________________________________________________________
