// $Id: gc_data.h,v 1.33 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_DATA_H
#define GC_DATA_H

#include "gc_types.h"
#include "gc_strings_mig.h"
#include "wx/arrstr.h"
#include "wx/list.h"

class gcdata
{
  public:
    static const wxArrayString fileFormatChoices();
    static const wxArrayString specificDataTypeChoices();
    static const wxArrayString interleavingChoices();
    static const wxArrayString genericLocusChoices();
    static const wxArrayString genericPopulationChoices();
    static const wxArrayString genericRegionChoices();
    static const wxArrayString integerList();
    static const wxArrayString integerListWithSpaces();
    static const wxArrayString nonNegativeIntegerList();
    static const wxArrayString positiveFloatChars();

    static const wxArrayString migrationConstraints();
    static const wxArrayString migrationMethods();
    static const wxArrayString migrationProfiles();

    static const size_t defaultHapCount;

    static const long defaultMapPosition;
    static const long defaultOffset;
    static const long noLengthSet;
    static const long noMapPositionSet;
    static const long noMarkerCountSet;
    static const long noOffsetSet;
    static const long noStyle;

    static const GCFileFormat   defaultFileFormat;

    static const gcGeneralDataType  allDataTypes();
    static const gcGeneralDataType  allelicDataTypes();
    static const gcGeneralDataType  nucDataTypes();

    static wxString getPloidyString(size_t ploidy);
};

bool                 ProduceBoolFromProximityOrBarf(wxString string);
bool                 ProduceBoolFromYesNoOrBarf(wxString string);
GCFileFormat         ProduceGCFileFormatOrBarf(wxString string, bool allowUnknown=true);
gcGeneralDataType    ProduceGeneralDataTypeOrBarf(wxString string, bool allowUnknown=true);
gcSpecificDataType   ProduceSpecificDataTypeOrBarf(wxString string, bool allowUnknown=true);
GCInterleaving       ProduceGCInterleavingOrBarf(wxString string, bool allowUnknown=true);
migration_method     ProduceMigMethodOrBarf(wxString string);
migration_profile    ProduceMigProfileOrBarf(wxString string);
migration_constraint ProduceMigConstraintOrBarf(wxString string);

wxString        ToWxString(bool);
wxString        ToWxStringLinked(bool);
wxString        ToWxString(gcGeneralDataType);
wxString        ToWxString(gcSpecificDataType);
wxString        ToWxString(GCFileFormat);
wxString        ToWxString(GCInterleaving);
wxString        ToWxString(gcPhaseSource);
wxString        ToWxString(loc_match);
wxString        ToWxString(pop_match);
wxString        ToWxString(migration_method);
wxString        ToWxString(migration_profile);
wxString        ToWxString(migration_constraint);

#endif  // GC_DATA_H

//____________________________________________________________________________________
