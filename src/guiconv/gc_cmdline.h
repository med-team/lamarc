// $Id: gc_cmdline.h,v 1.14 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_CMDLINE_H
#define GC_CMDLINE_H

#include "wx/string.h"

class wxCmdLineParser;
class GCDataStore;

class GCCmdLineManager
{
  protected:
    wxString        m_commandFileName;
    wxString        m_outputFileName;
    wxArrayString   m_inputFileNames;
    bool            m_batchOnly;
    bool            m_doDebugDump;
    wxString        m_batchOutName;

    void AddOptions(wxCmdLineParser& parser);
    void ExtractValues(wxCmdLineParser& parser);
    int  ProcessCommandLineAndCommandFile(  GCDataStore& dataStore);
    int  DoExport(GCDataStore& dataStore);

  public:
    GCCmdLineManager();
    virtual ~GCCmdLineManager();
};

#endif  // GC_CMDLINE_H

//____________________________________________________________________________________
