// $Id: gc_data_display.h,v 1.10 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_DATA_DISPLAY_H
#define GC_DATA_DISPLAY_H

#include "wx/gbsizer.h"

class GCDataDisplaySizer : public wxGridBagSizer
{
  private:
    int m_offset;
  public:
    GCDataDisplaySizer();
    virtual ~GCDataDisplaySizer();
    void SetOffset(int offset);
    int  GetOffset();

    void AddPop(wxWindow * header,int rowIndex, int length);
    void AddRegion(wxWindow * header,size_t firstLocus, size_t lastLocus);
    void AddLocus(wxWindow * header, size_t locusIndex);
    void AddData(wxWindow * header, int rowIndex, int colIndex);
    void AddPanel(wxWindow * header, int rowIndex, int colIndex, int lastCol);
    void AddDivergenceToggleCell(wxWindow * header);
    void AddPanelsToggleCell(wxWindow * header);
    void AddParent(wxWindow * header,size_t parIndex, int parLevel, int parSpan);
};

#endif  // GC_DATA_DISPLAY_H

//____________________________________________________________________________________
