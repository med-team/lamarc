// $Id: gc_menu_actors.cpp,v 1.8 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_datastore.h"
#include "gc_event_ids.h"
#include "gc_file_dialogs.h"
#include "gc_locus_dialogs.h"
#include "gc_menu_actors.h"
#include "gc_population_dialogs.h"
#include "gc_region_dialogs.h"
#include "gc_strings.h"
#include "wx/log.h"

//------------------------------------------------------------------------------------

basicMenuActor::basicMenuActor(int eventId)
    :
    m_eventId(eventId)
{
}

basicMenuActor::~basicMenuActor()
{
}

bool
basicMenuActor::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    wxLogDebug("saw menu for %d",m_eventId);
    assert(false);
    return true;
}

//------------------------------------------------------------------------------------

bool
gcActor_About::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    dataStore.GCInfo(gcstr::converterInfo);
    return false;       // does not change database
}

//------------------------------------------------------------------------------------

bool
gcActor_DebugDump::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    dataStore.DebugDump();
    return false;       // does not change database
}

//------------------------------------------------------------------------------------

bool
gcActor_ExportBatchFile::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    DoDialogExportBatchFile(parent,dataStore);
    return true;        // doesn't change database now, but will if we store
                        // batch file name
}

//------------------------------------------------------------------------------------

bool
gcActor_ExportFile::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    DoDialogExportFile(parent,dataStore);
    return true;       // changes database -- outfile name
}

//------------------------------------------------------------------------------------

bool
gcActor_FileAdd::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    return DoDialogAddFiles(parent,dataStore);
}

//------------------------------------------------------------------------------------

bool
gcActor_CmdFileRead::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    return DoDialogReadCmdFile(parent,dataStore);
}

//------------------------------------------------------------------------------------

bool
gcActor_ToggleVerbose::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    bool cur = wxLog::GetVerbose();
    wxLog::SetVerbose(!cur);
    return false;       // does not change database
}

//------------------------------------------------------------------------------------

gcEventActor * MakeMenuActor(int eventId)
{
    gcEventActor * actor = NULL;
    switch(eventId)
    {
        case wxID_ABOUT:
            actor = new gcActor_About();
            break;
        case gcEvent_Batch_Export:
            actor = new gcActor_ExportBatchFile();
            break;
        case gcEvent_CmdFile_Read:
            actor = new gcActor_CmdFileRead();
            break;
        case gcEvent_Debug_Dump:
            actor = new gcActor_DebugDump();
            break;
        case gcEvent_File_Add:
            actor = new gcActor_FileAdd();
            break;
        case gcEvent_File_Export:
            actor = new gcActor_ExportFile();
            break;
        case gcEvent_LinkG_Add:
            actor = new gcActor_RegionAdd();
            break;
        case gcEvent_Locus_Add:
            actor = new gcActor_LocusAdd();
            break;
        case gcEvent_Pop_Add:
            actor = new gcActor_PopAdd();
            break;
        case gcEvent_ToggleVerbose:
            actor = new gcActor_ToggleVerbose();
            break;
        default:
            actor = new basicMenuActor(eventId);
    };
    assert(actor != NULL);
    return actor;
}

//____________________________________________________________________________________
