// $Id: gc_file_list.cpp,v 1.24 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_event_publisher.h"
#include "gc_file_dialogs.h"
#include "gc_file_list.h"
#include "gc_logic.h"
#include "gc_strings.h"

#include "wx/bitmap.h"
#include "wx/gdicmn.h"
#include "wx/image.h"
#include "wx/log.h"
#include "wx/statbmp.h"

#include "empty16.xpm"
#include "excl16.xpm"

//------------------------------------------------------------------------------------

const wxBitmap &
GCExclaimBitmap::emptyBitmap()
{
    //static wxBitmap emptyBitmap(wxBITMAP(empty16));
    static wxBitmap emptyBitmap = wxBITMAP(empty16);
    return emptyBitmap;
}

const wxBitmap &
GCExclaimBitmap::exclBitmap()
{
    static wxBitmap exclBitmap = wxBITMAP(excl16);
    return exclBitmap;
}

GCExclaimBitmap::GCExclaimBitmap(wxPanel * panel, bool hasExclaim)
    :   wxStaticBitmap(panel,-1,emptyBitmap(),wxDefaultPosition)
{
    if(hasExclaim)
    {
        SetBitmap(exclBitmap());
    }
}

GCExclaimBitmap::~GCExclaimBitmap()
{
}

//------------------------------------------------------------------------------------

gcFilePane::gcFilePane(wxWindow * parent)
    :
    gcGridPane(parent,4,0)
{
}

gcFilePane::~gcFilePane()
{
}

void
gcFilePane::NotifyLeftDClick(size_t row, size_t col)
{
    assert(row < m_objVec.size());
    size_t fileId = m_objVec[row];
    gcEventActor * fileEditActor = new gcActor_File_Edit(fileId);
    PublishScreenEvent(GetEventHandler(),fileEditActor);

}

//------------------------------------------------------------------------------------

GCFileList::GCFileList( wxWindow * parent, GCLogic & logic)
    :
    gcInfoPane(parent, logic, gcstr::dataFilesTitle)
{
}

GCFileList::~GCFileList()
{
}

wxPanel *
GCFileList::MakeContent()
{
    gcGridPane * pane = new gcFilePane(m_scrolled);
    pane->SetBackgroundColour(wxTheColourDatabase->Find("WHITE"));

    const dataFileSet & files = m_logic.GetDataFiles();
    int line = 0;
    int xval = 10;
    for(dataFileSet::iterator iter= files.begin(); iter != files.end(); iter++)
    {
        const GCFile & fileRef = *(*iter);
        line++;
        xval += 10;

        wxArrayString labels;
        labels.Add(fileRef.GetShortName());

        if(m_logic.HasParse(fileRef))
        {
            const GCParse & parseRef = m_logic.GetParse(fileRef);
            labels.Add(parseRef.GetFormatString());
            labels.Add(parseRef.GetDataTypeString());
            labels.Add(parseRef.GetInterleavingString());
        }
        else
        {
            labels.Add(fileRef.GetFormatString());
            labels.Add(fileRef.GetDataTypeString());
            labels.Add(fileRef.GetInterleavingString());
        }
        pane->AddRow(fileRef.GetId(),labels);
    }
    pane->Finish();
    return pane;
}

wxString
GCFileList::MakeLabel()
{
    return wxString::Format(m_panelLabelFmt,m_logic.GetDataFileCount());
}

//____________________________________________________________________________________
