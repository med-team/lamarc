// $Id: gc_panel_dialogs.cpp,v 1.3 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_datastore.h"
#include "gc_dialog.h"
#include "gc_errhandling.h"
#include "gc_layout.h"
#include "gc_panel_dialogs.h"
#include "gc_strings.h"

#include "wx/checkbox.h"
#include "wx/log.h"
#include "wx/sizer.h"
#include "wx/statbox.h"
#include "wx/statline.h"
#include "wx/textctrl.h"

//------------------------------------------------------------------------------------

gcPanelRename::gcPanelRename(size_t panelId)
    :
    m_panelId(panelId)
{
}

gcPanelRename::~gcPanelRename()
{
}

wxString
gcPanelRename::FromDataStore(GCDataStore & dataStore)
{
    gcPanel & panelRef = dataStore.GetStructures().GetPanel(m_panelId);
    return panelRef.GetName();
}

void
gcPanelRename::ToDataStore(GCDataStore & dataStore, wxString newText)
{
    gcPanel & panelRef = dataStore.GetStructures().GetPanel(m_panelId);
    dataStore.GetStructures().Rename(panelRef,newText);
}

//------------------------------------------------------------------------------------

gcPanelMemberCount::gcPanelMemberCount(size_t panelId)
    :
    m_panelId(panelId)
{
}

gcPanelMemberCount::~gcPanelMemberCount()
{
}

wxString
gcPanelMemberCount::FromDataStore(GCDataStore & dataStore)
{
    gcPanel & panelRef = dataStore.GetStructures().GetPanel(m_panelId);
    return wxString::Format("%i",(int)panelRef.GetNumPanels());
}

void
gcPanelMemberCount::ToDataStore(GCDataStore & dataStore, wxString newText)
{
    long longVal;
    bool gotLong = newText.ToLong(&longVal);
    if(gotLong)
    {
        gcPanel & panelRef = dataStore.GetStructures().GetPanel(m_panelId);
        panelRef.SetNumPanels(longVal);
        panelRef.SetBlessed(true);
    }
}

const wxValidator &
gcPanelMemberCount::GetValidator()
{
    return m_validator;
}

wxString
gcPanelMemberCount::InitialString()
{
    return wxString::Format("0");
}

//------------------------------------------------------------------------------------

gcPanelEditDialog::gcPanelEditDialog( wxWindow *      parent,
                                      GCDataStore &   dataStore,
                                      size_t          panelId,
                                      bool            forJustCreatedObj)
    :
    gcUpdatingDialog(   parent,
                        dataStore,
                        forJustCreatedObj
                        ?
                        gcstr::addPanel
                        :
                        wxString::Format(gcstr::editPanel,
                                         dataStore.GetStructures().GetPanel(panelId).GetName().c_str()),
                        forJustCreatedObj),
    m_panelId(panelId)
{
}

gcPanelEditDialog::~gcPanelEditDialog()
{
}

//------------------------------------------------------------------------------------

bool DoDialogEditPanel(wxWindow *      parent,
                       GCDataStore &   dataStore,
                       size_t          panelId,
                       bool            forJustCreatedObj)
{
    wxLogVerbose("DoDialogEditPanel: %i",(int)panelId);  // JMDBG
    gcPanelEditDialog dialog(parent,dataStore,panelId,forJustCreatedObj);

    gcDialogCreator creator;
    wxBoxSizer * contentSizer   = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer * leftSizer      = new wxBoxSizer(wxVERTICAL);

    // for renaming
    gcTextHelper * panelRenameHelp  = new gcPanelRename(panelId);
    gcUpdatingComponent * rename    = new gcUpdatingTextCtrl(&dialog,
                                                             gcstr::panelRename,
                                                             panelRenameHelp);

    // change count
    gcTextHelper * memberCountHelp = new gcPanelMemberCount(panelId);
    gcUpdatingComponent * memberCount    = new gcUpdatingTextCtrl(&dialog,
                                                                  gcstr::panelMemberCount,
                                                                  memberCountHelp);

    leftSizer->Add(rename,
                   0,
                   wxALL | wxALIGN_CENTER | wxEXPAND,
                   gclayout::borderSizeSmall);

    leftSizer->Add(memberCount,
                   0,
                   wxALL | wxALIGN_CENTER | wxEXPAND,
                   gclayout::borderSizeSmall);

    creator.AddComponent(dialog,rename);
    creator.AddComponent(dialog,memberCount);
    contentSizer->Add(leftSizer,
                      1,
                      wxALL | wxALIGN_CENTER | wxEXPAND,
                      gclayout::borderSizeSmall);

    creator.PlaceContent(dialog,contentSizer);
    return dialog.Go();
}
//------------------------------------------------------------------------------------

bool
gcActor_PanelEdit::OperateOn(wxWindow * parent, GCDataStore & dataStore)
{
    wxLogVerbose("gcActor_PanelEdit::OperateOn m_panelId: %i",(int)m_panelId);  // JMDBG
    return DoDialogEditPanel(parent,dataStore,m_panelId,false);
}

//____________________________________________________________________________________
