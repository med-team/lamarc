// $Id: gc_layout.cpp,v 1.30 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_layout.h"

const long      gclayout::appHeight                  = 800;
const long      gclayout::appHeightPercent           = 90;
const long      gclayout::appWidth                   = 800;
const long      gclayout::appWidthPercent            = 90;
const int       gclayout::borderSize                 = 10;
const int       gclayout::borderSizeNone             = 0;
const int       gclayout::borderSizeSmall            = 4;
const int       gclayout::boxBorderSize              = 2;
const long      gclayout::maxDataFiles               = 3;
const int       gclayout::tabIconHeight              = 32;
const int       gclayout::tabIconWidth               = 32;
const int       gclayout::warningImageNeedsInput     = 0;
const int       gclayout::warningImageOK             = -1;

//____________________________________________________________________________________
