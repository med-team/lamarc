// $Id: gc_logic.h,v 1.44 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOGIC_H
#define GC_LOGIC_H

#include <vector>
#include "wx/wx.h"
#include "gc_datastore.h"
#include "gc_types.h"

class GuiConverterApp;
class wxWindow;

class GCLogic   : public GCDataStore
{
    friend class GuiConverterApp;               // for SetDisplayParent()

  private:
    wxWindow        *   m_displayParent;    // for centering windows

  protected:

    void SetDisplayParent(wxWindow * win);

  public:
    GCLogic();
    ~GCLogic();

    void        GCFatalBatchWarnGUI(wxString msg) const;
    void        GCError  (wxString msg) const;
    void        GCInfo   (wxString msg) const;
    void        GCWarning(wxString msg) const;

    void    batchFileRejectGuiLog(wxString msg,size_t lineNo) const;
    bool    guiQuestionBatchLog(wxString msg,wxString stopButton, wxString continueButton) const;

    void    GettingBusy(const wxString& msg) const;
    void    LessBusy(const wxString& msg) const;
};

#endif  // GC_LOGIC_H

//____________________________________________________________________________________
