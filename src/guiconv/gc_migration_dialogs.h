// $Id: gc_migration_dialogs.h,v 1.4 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_MIGRATION_DIALOGS_H
#define GC_MIGRATION_DIALOGS_H

#include "gc_dialog.h"
#include "gc_quantum.h"
#include "gc_validators.h"

class GCDataStore;
class wxWindow;

//------------------------------------------------------------------------------------

class gcMigrationRate : public gcTextHelper
{
  private:
    gcMigrationRate();       // undefined
    size_t                  m_migrationId;
  protected:
  public:
    gcMigrationRate(size_t migrationId);
    ~gcMigrationRate();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

//------------------------------------------------------------------------------------

class gcMigrationMethodChoice : public gcChoiceObject
{
  private:
    gcMigrationMethodChoice();        // undefined
    size_t                          m_migrationId;
    migration_method                m_type;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcMigrationMethodChoice(size_t forThisId, migration_method type);
    ~gcMigrationMethodChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

//------------------------------------------------------------------------------------

class gcMigrationProfileChoice : public gcChoiceObject
{
  private:
    gcMigrationProfileChoice();        // undefined
    size_t                          m_migrationId;
    migration_profile               m_type;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcMigrationProfileChoice(size_t forThisId, migration_profile type);
    ~gcMigrationProfileChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

//------------------------------------------------------------------------------------

class gcMigrationConstraintChoice : public gcChoiceObject
{
  private:
    gcMigrationConstraintChoice();        // undefined
    size_t                          m_migrationId;
    migration_constraint            m_type;
    wxCheckBox *                    m_box;
  protected:
  public:
    gcMigrationConstraintChoice(size_t forThisId, migration_constraint type);
    ~gcMigrationConstraintChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId();
};

//------------------------------------------------------------------------------------

class gcMigrationEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_migrationId;
    void    DoDelete() {};  // satisfy compiler, now does the same as cancel
  public:
    gcMigrationEditDialog(  wxWindow *      parentWindow,
                            GCDataStore &   dataStore,
                            size_t          migrationId,
                            wxString        toName,
                            wxString        fromName,
                            bool            forJustCreatedObj);
    virtual ~gcMigrationEditDialog();
};

//------------------------------------------------------------------------------------

bool DoDialogEditMigration( wxWindow *      parentWindow,
                            GCDataStore &   dataStore,
                            size_t          migrationId,
                            bool            forJustCreatedObj);



//------------------------------------------------------------------------------------

class gcActor_MigrationEdit : public gcEventActor
{
  private:
    gcActor_MigrationEdit();       // undefined
    size_t                      m_migrationId;

  public:
    gcActor_MigrationEdit(size_t migrationId) : m_migrationId(migrationId) {};
    virtual ~gcActor_MigrationEdit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_MIGRATION_DIALOGS_H
//____________________________________________________________________________________
