// $Id: batchconverter.h,v 1.6 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef BATCHCONVERTER_H
#define BATCHCONVERTER_H

#include "wx/app.h"
#include "wx/wx.h"
#include "gc_cmdline.h"
#include "gc_datastore.h"

class wxCmdLineParser;

class BatchConverterApp: public wxAppConsole, public GCCmdLineManager
                         // main gui converter application
{
  private:
    GCDataStore     m_dataStore;

  public:
    BatchConverterApp();
    virtual ~BatchConverterApp();

    virtual bool    OnCmdLineParsed(wxCmdLineParser&);
    virtual int     OnExit();
    virtual bool    OnInit();
    virtual void    OnInitCmdLine(wxCmdLineParser&);
    virtual int     OnRun();

};

#endif  // BATCHCONVERTER_H

//____________________________________________________________________________________
