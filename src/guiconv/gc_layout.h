// $Id: gc_layout.h,v 1.24 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LAYOUT_H
#define GC_LAYOUT_H

#include "wx/gdicmn.h"

class gclayout
{
  public:
    static const long   appHeight;
    static const long   appHeightPercent;
    static const long   appWidth;
    static const long   appWidthPercent;
    static const int    borderSize;
    static const int    borderSizeNone;
    static const int    borderSizeSmall;
    static const int    boxBorderSize;
    static const long   maxDataFiles;
    static const int    tabIconHeight;
    static const int    tabIconWidth;
    static const int    warningImageNeedsInput;
    static const int    warningImageOK;
};

#endif  // GC_LAYOUT_H

//____________________________________________________________________________________
