// $Id: gc_file_dialogs.h,v 1.26 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FILE_DIALOGS_H
#define GC_FILE_DIALOGS_H

#include "gc_quantum.h"
#include "gc_dialog.h"

class wxWindow;
class GCFile;
class GCDataStore;

class gcFullPath : public gcPlainTextHelper
{
  private:
    gcFullPath();   // undefined

    size_t          m_fileId;
  protected:
  public:
    gcFullPath(size_t fileId);
    virtual ~gcFullPath();
    wxString    FromDataStore(GCDataStore &);
};

class gcParseInfoNone : public gcPlainTextHelper
{
  private:
  protected:
  public:
    gcParseInfoNone();
    virtual ~gcParseInfoNone();

    wxString    FromDataStore(GCDataStore &);
};

class gcParseInfoOne : public gcPlainTextHelper
{
  private:
    gcParseInfoOne();   // undefined
    size_t              m_fileId;
  protected:
  public:
    gcParseInfoOne(size_t fileId);
    virtual ~gcParseInfoOne();

    wxString    FromDataStore(GCDataStore &);
};

class gcParseChoice : public gcChoiceObject
{
  private:
    gcParseChoice();        // undefined
    size_t                  m_parseId;
    wxCheckBox *            m_box;
  protected:
  public:
    gcParseChoice(size_t parseId);
    virtual ~gcParseChoice();

#if 0
    wxString    GetLabel    (GCDataStore &);
    bool        GetEnabled  (GCDataStore &);
    bool        GetSelected (GCDataStore &);
    void        SetSelected (GCDataStore &, bool selected);
#endif

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent);
    wxWindow *  FetchWindow();

    size_t      GetRelevantId();

};

class gcHapDefaultChoice : public gcChoiceObject
{
  private:
    size_t                      m_fileId;
    gcHapDefaultChoice();       // undefined
  protected:
    wxCheckBox *        m_box;
  public:
    gcHapDefaultChoice(size_t fileId);
    virtual ~gcHapDefaultChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId()                         ;
};

class gcHapAdjacentChoice : public gcChoiceObject
{
  private:
    size_t                      m_fileId;
    gcHapAdjacentChoice();      // undefined
  protected:
    wxPanel *           m_panel;
    wxCheckBox *        m_box;
    wxTextCtrl *        m_text;
  public:
    gcHapAdjacentChoice(size_t fileId);
    virtual ~gcHapAdjacentChoice();

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent)           ;
    wxWindow *  FetchWindow()                           ;

    size_t      GetRelevantId()                         ;
};

class gcHapChoices : public gcUpdatingChoose
{
  private:
    size_t              m_fileId;
  protected:
  public:
    gcHapChoices(   wxWindow *                      parent,
                    GCDataStore &                   dataStore,
                    size_t                          fileId,
                    std::vector<gcChoiceObject*>    choices);
    virtual ~gcHapChoices();

    void    BuildDisplay(GCDataStore &);
};

class gcFileEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t      m_fileId;
    void DoDelete();
  public:
    gcFileEditDialog(   wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          fileId);
    virtual ~gcFileEditDialog();
};

bool DoDialogAddFiles(wxWindow * parent, GCDataStore & dataStore);
bool DoDialogEditFile(wxWindow * parent, GCDataStore & dataStore, size_t fileId);
bool DoDialogExportFile(wxWindow * parent, GCDataStore & dataStore);
bool DoDialogExportBatchFile(wxWindow * parent, GCDataStore & dataStore);
bool DoDialogReadCmdFile(wxWindow * parent, GCDataStore & dataStore);

class gcActor_File_Edit : public gcEventActor
{
  private:
    gcActor_File_Edit();    // undefined
    size_t                  m_fileId;
  public:
    gcActor_File_Edit(size_t fileId) : m_fileId(fileId) {};
    virtual ~gcActor_File_Edit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_FILE_DIALOGS_H

//____________________________________________________________________________________
