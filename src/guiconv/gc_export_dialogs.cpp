// $Id: gc_export_dialogs.cpp,v 1.8 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_event_ids.h"
#include "gc_export_dialogs.h"
#include "gc_layout.h"
#include "gc_strings.h"
#include "wx/sizer.h"
#include "wx/stattext.h"

BEGIN_EVENT_TABLE(GCBadLocusLengthDialog, wxDialog)
EVT_BUTTON( GC_ExportCancel,   GCBadLocusLengthDialog::OnTakeCancel)
EVT_BUTTON( GC_ExportContinue, GCBadLocusLengthDialog::OnTakeContinue)
EVT_BUTTON( GC_ExportEdit,     GCBadLocusLengthDialog::OnTakeEdit)
END_EVENT_TABLE()

GCBadLocusLengthDialog::GCBadLocusLengthDialog(   wxWindow    * parent,
                                                  wxArrayString nameList)
:
wxDialog(parent,-1,gcstr::exportWarning),
    m_regionNameList(nameList),
    m_nextStep(badlocus_CANCEL)
{
    Init();
}

void
GCBadLocusLengthDialog::Init()
{
    wxBoxSizer * topSizer      = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer * buttonSizer   = new wxBoxSizer(wxHORIZONTAL);

    wxString regionListString = gcstr::badLocusLength1;
    for(size_t index = 0; index < m_regionNameList.GetCount(); index++)
    {
        regionListString += "\n\t";
        regionListString += m_regionNameList[index];
    }
    regionListString += "\n\n";
    regionListString += gcstr::badLocusLength2;

    wxStaticText * message = new wxStaticText(this,-1,regionListString);

    wxButton    * continueButton = new wxButton(this,GC_ExportContinue,
                                                gcstr::continueString.c_str());
    wxButton    * editButton     = new wxButton(this,GC_ExportEdit,
                                                gcstr::regionEditString.c_str());
    wxButton    * cancelButton   = new wxButton(this,GC_ExportCancel,
                                                gcstr::cancelString.c_str());

    buttonSizer->Add(continueButton,0,wxALL,gclayout::borderSize);
    buttonSizer->Add(editButton,0,wxALL,gclayout::borderSize);
    buttonSizer->Add(cancelButton,0,wxALL,gclayout::borderSize);

    topSizer->Add(message,1,wxEXPAND | wxALL,gclayout::borderSize);
    topSizer->Add(buttonSizer,0,wxALIGN_CENTER);

    SetSizer(topSizer);
    topSizer->SetSizeHints( this);
}

void
GCBadLocusLengthDialog::OnTakeCancel(wxCommandEvent & event)
{
    m_nextStep = badlocus_CANCEL;
    OnOK(event);
}

void
GCBadLocusLengthDialog::OnTakeContinue(wxCommandEvent & event)
{
    m_nextStep = badlocus_CONTINUE;
    OnOK(event);
}

void
GCBadLocusLengthDialog::OnTakeEdit(wxCommandEvent & event)
{
    m_nextStep = badlocus_EDIT;
    OnOK(event);
}

//____________________________________________________________________________________
