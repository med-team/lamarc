// $Id: gc_regiontab.cpp,v 1.44 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_event_publisher.h"
#include "gc_logic.h"
#include "gc_regiontab.h"
#include "gc_region_dialogs.h"
#include "gc_strings.h"
#include "gc_strings_region.h"
#include "gc_structures.h"

//------------------------------------------------------------------------------------

gcRegionPane::gcRegionPane(wxWindow * parent)
    :
    gcGridPane(parent,3,0)  // EWFIX.P3 -- constant
{
}

gcRegionPane::~gcRegionPane()
{
}

void
gcRegionPane::NotifyLeftDClick(size_t row, size_t col)
{
    assert(row < m_objVec.size());
    size_t regionId = m_objVec[row];
    gcEventActor * regionEditActor = new gcActor_RegionEdit(regionId);
    PublishScreenEvent(GetEventHandler(),regionEditActor);
}

//------------------------------------------------------------------------------------

gcRegionTab::gcRegionTab( wxWindow * parent, GCLogic & logic)
    :
    gcInfoPane(parent, logic, gcstr_region::tabTitle)
{
}

gcRegionTab::~gcRegionTab()
{
}

wxPanel *
gcRegionTab::MakeContent()
{
    gcGridPane * pane = new gcRegionPane(m_scrolled);

    objVector regions = m_logic.GetStructures().GetDisplayableRegions();
    for(objVector::iterator iter=regions.begin(); iter != regions.end(); iter++)
    {
        GCQuantum * quantumP = *iter;
        gcRegion * regionP = dynamic_cast<gcRegion*>(quantumP);
        assert(regionP != NULL);

        wxArrayString labels;

        labels.Add(regionP->GetName());
        labels.Add(wxString::Format(gcstr_region::numLoci,(int)(regionP->GetLocusCount())));
        if(regionP->HasEffectivePopulationSize())
        {
            labels.Add(wxString::Format(gcstr_region::effPopSize,regionP->GetEffectivePopulationSize()));
        }
        else
        {
            labels.Add("");
        }

        pane->AddRow(quantumP->GetId(),labels);
    }

    pane->Finish();
    return pane;

}

wxString
gcRegionTab::MakeLabel()
{
    return wxString::Format(m_panelLabelFmt,(int)m_logic.GetStructures().GetDisplayableRegionIds().size());
}

//____________________________________________________________________________________
