// $Id: guiconverter.h,v 1.15 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GUICONVERTER_H
#define GUICONVERTER_H

/* NOTE:  8/29/2014 Mary edited gc_migtab.cpp to reverse the sense
of "from" and "to" in the migration matrix, since it was producing
XML files that were backwards to what LAMARC assumes.  She did NOT
change variable names or anything else to match, so there are probably
misleading variable names in this part of the converter and in the
corresponding XML output routines.  Maintainers beware!  DEBUG */

#include "wx/wx.h"
#include "gc_cmdline.h"
#include "gc_logic.h"

class wxCmdLineParser;
class GCFrame;

class GuiConverterApp: public wxApp, public GCCmdLineManager
                       // main gui converter application
{
  protected:
    GCLogic         m_logic;
    GCFrame     *   m_mainFrame;

  public:
    GuiConverterApp();
    virtual ~GuiConverterApp();

    virtual bool    OnCmdLineParsed(wxCmdLineParser&);
    virtual int     OnExit();
    virtual bool    OnInit();
    virtual void    OnInitCmdLine(wxCmdLineParser&);
    virtual int     OnRun();
};

#endif  // GUICONVERTER_H

//____________________________________________________________________________________
