// $Id: gc_quantum.h,v 1.16 2018/01/03 21:33:00 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_QUANTUM_H
#define GC_QUANTUM_H

#include "wx/clntdata.h"
#include "wx/string.h"

class GCQuantum
{
  private:
    static size_t       s_objCount;
  protected:
    size_t              m_objId;
    wxString            m_name;
    bool                m_selected;
  public:
    GCQuantum();

#if 0
    GCQuantum(wxString name);
#endif

    virtual ~GCQuantum();

    size_t      GetId()                         const;
    void        DebugDump(wxString prefix=wxEmptyString)   const;
    virtual wxString    GetName()                       const;
    virtual void        SetName(wxString name);

#if 0
    virtual bool        GetSelected()                   const;
    virtual void        SetSelected(bool selected);
#endif

    static  void        ReportMax();
};

class wxWindow;
class GCDataStore;

class gcEventActor
{
  public:
    gcEventActor() {};
    virtual ~gcEventActor() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & store) = 0;
};

class GCClientData : public wxClientData
{
  private:
    GCClientData();         // undefined
    gcEventActor *          m_myActor;
  public:
    GCClientData(gcEventActor * myActor);
    virtual ~GCClientData();
    gcEventActor *  GetActor() ;
};

#endif  // GC_QUANTUM_H

//____________________________________________________________________________________
