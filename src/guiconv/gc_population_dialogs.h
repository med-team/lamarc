// $Id: gc_population_dialogs.h,v 1.17 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_POPULATION_DIALOGS_H
#define GC_POPULATION_DIALOGS_H

#include "gc_quantum.h"
#include "gc_dialog.h"

class GCDataStore;
class gcPopulation;
class wxWindow;

class gcPopMergeChoice : public gcChoiceObject
{
  private:
    gcPopMergeChoice();         // undefined
    size_t                      m_popId;
    wxCheckBox *                m_box;
  protected:
  public:
    gcPopMergeChoice(size_t popId);
    ~gcPopMergeChoice();

#if 0
    wxString    GetLabel(GCDataStore &);
    bool        GetEnabled(GCDataStore &);
    bool        GetSelected(GCDataStore &);
    void        SetSelected(GCDataStore &, bool value);
#endif

    void        UpdateDisplayInitial    (GCDataStore &) ;
    void        UpdateDisplayInterim    (GCDataStore &) ;
    void        UpdateDataInterim       (GCDataStore &) ;
    void        UpdateDataFinal         (GCDataStore &) ;

    wxWindow *  MakeWindow(wxWindow * parent);
    wxWindow *  FetchWindow();

    size_t      GetRelevantId();

};

class gcPopMerge : public gcUpdatingChooseMulti
{
  protected:
    size_t          m_popId;
  public:
    gcPopMerge( wxWindow *                      parent,
                size_t                          popId,
                std::vector<gcChoiceObject*>    choices);
    virtual ~gcPopMerge();

    void DoFinalForMulti(GCDataStore & dataStore, gcIdVec chosenPops);
    wxString    NoChoicesText() const;
};

class gcPopRename : public gcTextHelper
{
  private:
  protected:
    size_t          m_popId;
  public:
    gcPopRename(size_t popId);
    ~gcPopRename();

    wxString    FromDataStore(GCDataStore &);
    void        ToDataStore(GCDataStore &, wxString newText);
};

class gcPopEditDialog : public gcUpdatingDialog
{
  private:
  protected:
    size_t          m_popId;
    void DoDelete();
  public:
    gcPopEditDialog(wxWindow *      parentWindow,
                    GCDataStore &   dataStore,
                    size_t          popId,
                    bool            forJustCreatedObj);
    virtual ~gcPopEditDialog();
};

bool DoDialogEditPop(   wxWindow *      parentWindow,
                        GCDataStore &   dataStore,
                        size_t          popId,
                        bool            forJustCreatedObj);

class gcActor_PopAdd : public gcEventActor
{
  public:
    gcActor_PopAdd() {};
    virtual ~gcActor_PopAdd() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_Pop_Edit : public gcEventActor
{
  private:
    gcActor_Pop_Edit();     // undefined
    size_t                  m_popId;
  public:
    gcActor_Pop_Edit(size_t popId) : m_popId(popId) {};
    virtual ~gcActor_Pop_Edit() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_POPULATION_DIALOGS_H

//____________________________________________________________________________________
