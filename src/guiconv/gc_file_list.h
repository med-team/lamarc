// $Id: gc_file_list.h,v 1.14 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_FILELIST_H
#define GC_FILELIST_H

#include "gc_gridpanel.h"
#include "wx/button.h"
#include "wx/checkbox.h"
#include "wx/choice.h"
#include "wx/radiobox.h"

class GCFile;
class GCParse;
class wxBitmap;
class wxWindow;

class GCExclaimBitmap : public wxStaticBitmap
{
  private:
    GCExclaimBitmap();    // undefined
  protected:
  public:
    static const wxBitmap & emptyBitmap();
    static const wxBitmap & exclBitmap();

    GCExclaimBitmap(wxPanel * panel, bool hasExclaim);
    virtual ~GCExclaimBitmap();
};

class gcFilePane : public gcGridPane
{
  private:
    gcFilePane();           // undefined
  protected:
  public:
    gcFilePane(wxWindow * parent);
    virtual ~gcFilePane();
    virtual void NotifyLeftDClick(size_t row, size_t col);

};

class GCFileList : public gcInfoPane
{
  private:
    GCFileList();        // undefined

  protected:
    wxPanel * MakeContent();
    wxString  MakeLabel();

  public:
    GCFileList(wxWindow * parent, GCLogic & logic);
    virtual ~GCFileList();
};

#endif  // GC_FILELIST_H

//____________________________________________________________________________________
