// $Id: gc_poptab.cpp,v 1.10 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_event_publisher.h"
#include "gc_poptab.h"
#include "gc_logic.h"
#include "gc_population_dialogs.h"
#include "gc_strings.h"

#include "wx/log.h"

//------------------------------------------------------------------------------------

gcPopPane::gcPopPane(wxWindow * parent)
    :
    gcGridPane(parent,1,0)
{
}

gcPopPane::~gcPopPane()
{
}

void
gcPopPane::NotifyLeftDClick(size_t row, size_t col)
{
    assert(row < m_objVec.size());
    size_t popId = m_objVec[row];
    gcEventActor * popEditActor = new gcActor_Pop_Edit(popId);
    PublishScreenEvent(GetEventHandler(),popEditActor);
}

//------------------------------------------------------------------------------------

gcPopTab::gcPopTab( wxWindow * parent, GCLogic & logic)
    :
    gcInfoPane(parent, logic, gcstr::popTabTitle)
{
}

gcPopTab::~gcPopTab()
{
}

wxPanel *
gcPopTab::MakeContent()
{
    gcGridPane * pane = new gcPopPane(m_scrolled);
    objVector pops = m_logic.GetStructures().GetDisplayablePops();
    for(objVector::iterator iter=pops.begin(); iter != pops.end(); iter++)
        // for each population
    {
        GCQuantum * quantumP = *iter;
        GCPopulation * popP = dynamic_cast<GCPopulation*>(quantumP);
        assert(popP != NULL);
        wxString popName = popP->GetName();
        wxArrayString labels;
        labels.Add(popName);
        pane->AddRow(quantumP->GetId(),labels);
    }
    pane->Finish();
    return pane;

}

wxString
gcPopTab::MakeLabel()
{
    return wxString::Format(m_panelLabelFmt,(int)m_logic.GetStructures().GetDisplayablePopIds().size());
}

//____________________________________________________________________________________
