// $Id: gc_data_display.cpp,v 1.11 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


//#include <iostream>
#include <string>

#include "gc_data_display.h"
#include "gc_layout.h"
#include "wx/log.h"

//using namespace std;

GCDataDisplaySizer::GCDataDisplaySizer()
    : wxGridBagSizer(gclayout::borderSize,gclayout::borderSize)
{
    m_offset = 0;
}

GCDataDisplaySizer::~GCDataDisplaySizer()
{
}

void
GCDataDisplaySizer::SetOffset(int offset)
{
    m_offset = offset;
}

int
GCDataDisplaySizer::GetOffset()
{
    return m_offset;
}

void
GCDataDisplaySizer::AddPop(wxWindow * header, int rowIndex, int length)
{
    int colIndex = m_offset;
    //wxLogVerbose("AddPop row: %i column: %i", rowIndex, colIndex);  // JMDBG
    Add(header,wxGBPosition(rowIndex,colIndex),wxGBSpan(length,1),wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddRegion(wxWindow * header, size_t firstLocus, size_t lastLocus)
{
    // always in row 0, fiirst column always skipped
    //wxLogVerbose(" Region row 0 col: %i len: %i",firstLocus+1+m_offset, lastLocus-firstLocus+1);  // JMDBG
    Add(header,wxGBPosition(0,firstLocus+1+m_offset),wxGBSpan(1,lastLocus-firstLocus+1),wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddLocus(wxWindow * header, size_t locusIndex)
{
    // always is row 1, first column always skipped
    //wxLogVerbose(" Locus row 1 col: %i" ,locusIndex+1+m_offset);  // JMDBG
    Add(header,wxGBPosition(1,locusIndex+1+m_offset),wxDefaultSpan,wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddData(wxWindow * header, int rowIndex, int colIndex)
{
    colIndex += m_offset;
    //wxLogVerbose("AddData row: %i column: %i", rowIndex, colIndex);
    Add(header,wxGBPosition(rowIndex,colIndex),wxDefaultSpan,wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddPanel(wxWindow * header, int rowIndex, int colIndex, int width)
{
    //wxLogVerbose("AddPanel row: %i column: %i length: %i", rowIndex,colIndex+m_offset, width);
    Add(header,wxGBPosition(rowIndex,colIndex+m_offset),wxGBSpan(1, width),wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddDivergenceToggleCell(wxWindow * header)
{
    //wxLogVerbose("AddDivergenceToggleCell row: 0 column: 0");  // JMDBG
    Add(header,wxGBPosition(0,0),wxGBSpan(1,1),wxALL | wxEXPAND);
}


void
GCDataDisplaySizer::AddPanelsToggleCell(wxWindow * header)
{
    //wxLogVerbose("AddPanelToggleCell row: 1 column: 0");  // JMDBG
    Add(header,wxGBPosition(1,0),wxGBSpan(1,1),wxALL | wxEXPAND);
}

void
GCDataDisplaySizer::AddParent(wxWindow * header, size_t parRow, int parCol, int parSpan)
{
    //wxLogVerbose("AddParent row: %i column: %i height: %i", parRow, parCol, parSpan);  // JMDBG
    Add(header,wxGBPosition(parRow,parCol),wxGBSpan(parSpan,1),wxALL | wxEXPAND);
}

//____________________________________________________________________________________
