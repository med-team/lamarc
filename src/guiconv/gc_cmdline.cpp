// $Id: gc_cmdline.cpp,v 1.24 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "wx/cmdline.h"
#include "wx/log.h"
#include "conf.h"
#include "gc_errhandling.h"
#include "gc_cmdline.h"
#include "gc_data.h"
#include "gc_datastore.h"
#include "gc_strings.h"
#include "tinyxml.h"

class GCFile;

GCCmdLineManager::GCCmdLineManager()
    :
    m_commandFileName   (wxEmptyString),
    m_outputFileName    (wxEmptyString),
    m_batchOnly         (false),
    m_doDebugDump       (false),
    m_batchOutName      (wxEmptyString)
{
    m_inputFileNames.Empty();
}

GCCmdLineManager::~GCCmdLineManager()
{
}

void
GCCmdLineManager::AddOptions(wxCmdLineParser& parser)
{
    wxString logoString = wxString::Format(gcstr::usageHeader,VERSION);
    parser.SetLogo(logoString);
    parser.AddSwitch(   gcstr::cmdBatchChar,
                        gcstr::cmdBatch,
                        gcstr::cmdBatchDescription);
    parser.AddSwitch(   gcstr::cmdDumpChar,
                        gcstr::cmdDump,
                        gcstr::cmdDumpDescription);
    parser.AddOption(   gcstr::cmdCommandChar,
                        gcstr::cmdCommand,
                        gcstr::cmdCommandDescription,
                        wxCMD_LINE_VAL_STRING);
    wxString cmdOutputDesc = wxString::Format(gcstr::cmdOutputDescription,
                                              gcstr::exportFileDefault.c_str());
    parser.AddOption(   gcstr::cmdOutputChar,
                        gcstr::cmdOutput,
                        cmdOutputDesc,
                        wxCMD_LINE_VAL_STRING);
    parser.AddOption(   gcstr::cmdWriteBatchChar,
                        gcstr::cmdWriteBatch,
                        gcstr::cmdWriteBatchDescription,
                        wxCMD_LINE_VAL_STRING);
    parser.AddParam (   gcstr::cmdInputDescription,
                        wxCMD_LINE_VAL_STRING,
                        wxCMD_LINE_PARAM_OPTIONAL |
                        wxCMD_LINE_PARAM_MULTIPLE);
}

void
GCCmdLineManager::ExtractValues(wxCmdLineParser& parser)
{
    m_batchOnly = parser.Found(gcstr::cmdBatch);
    m_doDebugDump = parser.Found(gcstr::cmdDump);
    parser.Found(gcstr::cmdCommand, &m_commandFileName);
    parser.Found(gcstr::cmdOutput,  &m_outputFileName);
    parser.Found(gcstr::cmdWriteBatch,  &m_batchOutName);

    size_t numFiles = parser.GetParamCount();
    for(size_t p = 0; p < numFiles; p++)
    {
        m_inputFileNames.Add(parser.GetParam(p));
    }
}

int
GCCmdLineManager::ProcessCommandLineAndCommandFile( GCDataStore &   dataStore)
{
    int exitCode = 0;
    for(size_t i=0; i < m_inputFileNames.Count(); i++)
    {
        try
        {
            dataStore.AddDataFile(m_inputFileNames[i]);
        }
        catch(const gc_ex& e)  // EWFIX.P4 -- make more specific type ??
        {
            dataStore.GCFatalBatchWarnGUI(e.what());
            exitCode = 1;
        }
    }
    if(!m_commandFileName.IsEmpty())
    {
        exitCode = dataStore.ProcessCmdFile(m_commandFileName);
    }

    if(!m_outputFileName.IsEmpty())
    {
        dataStore.SetOutfileName(m_outputFileName);
    }
    return exitCode;

}

int
GCCmdLineManager::DoExport(GCDataStore& dataStore)
{
    try
    {
        TiXmlDocument * doc = dataStore.ExportFile();
        dataStore.WriteExportedData(doc);
        wxLogVerbose(gcverbose::exportSuccess,dataStore.GetOutfileName().c_str());
        delete doc;
        return 0;
    }
    catch(const gc_ex& e)
    {
        dataStore.GCError(wxString::Format(gcerr::unableToExport,e.what()));
        return 1;
    }
}

//____________________________________________________________________________________
