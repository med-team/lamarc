// $Id: gc_event_ids.h,v 1.62 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_EVENT_IDS_H
#define GC_EVENT_IDS_H

// It's safer to put all these in a single header file,
// thus ensuring all event ids get a different value.
// However, it does mean that a single change in the
// GUI events means nearly everything needs to be recompiled.
// If this gets to be a problem, we should consider breaking
// this enum up. One likely possibility would be to put the
// GC_*, S2D_* and D2S_* events in different enums. At this
// writing (10/14/2004) S2D_* event id's are only generated
// by events of type SCREEN_2_DATA (and similar for D2S_* and
// DATA_2_SCREEN) so a number collision with other events would
// likely not cause confusion.

enum GCEventId
{
    gcEvent_Generic    = 1,

    gcEvent_Debug_Dump,

    gcEvent_CmdFile_Read,

    gcEvent_File_Add,
    gcEvent_File_Edit,
    gcEvent_File_Export,
    gcEvent_Batch_Export,

    gcEvent_LinkG_Add,

    gcEvent_Locus_Add,

    gcEvent_Pop_Add,
    gcEvent_Pop_Edit,

    gcEvent_ToggleVerbose,

    gcEvent_ViewToggle_InputFiles,
    gcEvent_ViewToggle_Partitions,

    //////////////////

    GC_EditCancel,
    GC_EditDelete,
    GC_EditOK,
    //
    //
    GC_LinkGMerge,
    GC_LinkGRemove,
    GC_LinkGRename,
    //
    //
    GC_TraitAdd,
    GC_TraitRemove,
    GC_TraitRename,
    //

    // injected into event space by GCFrame after dispatching any
    // S2D event. This event marks the end of a possible series
    // of other D2S events that cause screen updates to be cached
    D2S_UserInteractionPhaseEnd,

    // dialog buttons
    GC_MigParseTakeFile,
    GC_MigParseTakeUser,
    GC_ExportCancel,
    GC_ExportContinue,
    GC_ExportEdit,

    GC_SelectAll,
    GC_UnselectAll,

    // divergence button
    GC_Divergence
};

#endif  // GC_EVENT_IDS_H

//____________________________________________________________________________________
