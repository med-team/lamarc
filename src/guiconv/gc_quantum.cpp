// $Id: gc_quantum.cpp,v 1.16 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_default.h"
#include "gc_quantum.h"
#include "wx/log.h"

size_t GCQuantum::s_objCount = 0;

GCQuantum::GCQuantum()
    :
    m_objId(s_objCount++),
    m_name(gcdefault::unnamedObject),
    m_selected(false)
{
}

#if 0
GCQuantum::GCQuantum(wxString name)
    :
    m_objId(s_objCount++),
    m_name(name),
    m_selected(false)
{
}
#endif

GCQuantum::~GCQuantum()
{
}

size_t
GCQuantum::GetId() const
{
    return m_objId;
}

#if 0

bool
GCQuantum::GetSelected() const
{
    return m_selected;
}

void
GCQuantum::SetSelected(bool selected)
{
    m_selected=selected;
}

#endif

void
GCQuantum::DebugDump(wxString prefix) const
{
    wxLogDebug("%sid:%d",prefix.c_str(),(int)m_objId);  // EWDUMPOK
}

wxString
GCQuantum::GetName() const
{
    return m_name;
}

void
GCQuantum::SetName(wxString name)
{
    m_name = name;
}

void
GCQuantum::ReportMax()
{
    wxLogDebug("created %d numbered objects",(int)s_objCount);  // EWDUMPOK
}

//------------------------------------------------------------------------------------

GCClientData::GCClientData(gcEventActor * myActor)
    :   m_myActor(myActor)
{
}

GCClientData::~GCClientData()
{
    delete m_myActor;
}

gcEventActor *
GCClientData::GetActor()
{
    return m_myActor;
}

//____________________________________________________________________________________
