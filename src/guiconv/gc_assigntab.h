// $Id: gc_assigntab.h,v 1.17 2018/01/03 21:32:58 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_ASSIGNTAB_H
#define GC_ASSIGNTAB_H

#include "gc_clickpanel.h"
#include "gc_gridpanel.h"
#include "gc_quantum.h"

class wxPanel;
class wxWindow;
class constBlockVector;
class gcLocus;
class gcPanel;
class GCParseBlock;
class gcPopulation;
class gcRegion;
class gcParent;
class GCStructures;

class gcBlockCell : public gcClickCell
{
  private:
  protected:
    size_t      m_blockId;
  public:
    gcBlockCell(wxWindow * parent, const GCParseBlock &);
    virtual ~gcBlockCell();
    void NotifyLeftDClick();
};

class gcPopCell : public gcClickCell
{
  private:
  protected:
    size_t      m_popId;
  public:
    gcPopCell(wxWindow * parent, const gcPopulation &);
    virtual ~gcPopCell();

    void    NotifyLeftDClick();
};

class gcRegionCell : public gcClickCell
{
  private:
  protected:
    size_t      m_regionId;
  public:
    gcRegionCell(wxWindow * parent, const gcRegion &);
    virtual ~gcRegionCell();

    void    NotifyLeftDClick();
};

class gcLocusCell : public gcClickCell
{
  private:
  protected:
    size_t      m_locusId;
  public:
    gcLocusCell(wxWindow * parent, const gcLocus &);
    virtual ~gcLocusCell();

    void    NotifyLeftDClick();
};

class gcPanelCell : public gcClickCell
{
  private:
  protected:
    size_t      m_panelId;
  public:
    gcPanelCell(wxWindow * parent, const gcPanel &);
    virtual ~gcPanelCell();

    void    NotifyLeftDClick();
};

class gcEmptyCell : public wxPanel
{
  private:
  protected:
    wxStaticBoxSizer * m_sizer;

  public:
    gcEmptyCell(wxWindow * parent);
    virtual ~gcEmptyCell();
};

class gcDivergenceToggleCell : public gcClickCell
{
  private:
    bool m_divergenceOn;
  public:
    gcDivergenceToggleCell(wxWindow * parent, bool divergenceOn, int unused);
    virtual ~gcDivergenceToggleCell();

    void    NotifyLeftDClick();
};

class gcPanelsToggleCell : public gcClickCell
{
  private:
    bool m_panelsOn;
  public:
    gcPanelsToggleCell(wxWindow * parent, bool panelsOn);
    virtual ~gcPanelsToggleCell();

    void    NotifyLeftDClick();
};

class gcActor_PanelsToggle : public gcEventActor
{
  public:
    gcActor_PanelsToggle() {};
    virtual ~gcActor_PanelsToggle() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};


class gcParentCell : public gcClickCell
{
  private:
  protected:
    size_t      m_parentId;
  public:
    gcParentCell(wxWindow * parent, const gcParent &);
    virtual ~gcParentCell();

    void    NotifyLeftDClick();
};

class GCAssignmentTab : public gcInfoPane
{
  private:
    GCAssignmentTab();        // undefined

  protected:
    wxPanel *   MakeContent();
    wxString    MakeLabel();
    wxPanel *   blockControl(wxWindow *, constBlockVector &);

  public:
    GCAssignmentTab(wxWindow * parent, GCLogic & logic);
    //GCAssignmentTab(wxBookCtrlBase * parent, GCLogic & logic);
    virtual ~GCAssignmentTab();
};

#endif  // GC_ASSIGNTAB_H

//____________________________________________________________________________________
