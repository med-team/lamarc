// $Id: gc_menu_actors.h,v 1.8 2018/01/03 21:32:59 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_MENU_ACTORS_H
#define GC_MENU_ACTORS_H

#include "gc_quantum.h"

gcEventActor * MakeMenuActor(int eventId);

class basicMenuActor : public gcEventActor
{
  private:
    basicMenuActor();   // undefined
  protected:
    int m_eventId;
  public:
    basicMenuActor(int eventId);
    virtual ~basicMenuActor();
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_About : public gcEventActor
{
  public:
    gcActor_About() {};
    virtual ~gcActor_About() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_DebugDump : public gcEventActor
{
  public:
    gcActor_DebugDump() {};
    virtual ~gcActor_DebugDump() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_ExportBatchFile : public gcEventActor
{
  public:
    gcActor_ExportBatchFile() {};
    virtual ~gcActor_ExportBatchFile() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_ExportFile : public gcEventActor
{
  public:
    gcActor_ExportFile() {};
    virtual ~gcActor_ExportFile() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_FileAdd : public gcEventActor
{
  public:
    gcActor_FileAdd() {};
    virtual ~gcActor_FileAdd() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_LinkGAdd : public gcEventActor
{
  public:
    gcActor_LinkGAdd() {};
    virtual ~gcActor_LinkGAdd() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_CmdFileRead : public gcEventActor
{
  public:
    gcActor_CmdFileRead() {};
    virtual ~gcActor_CmdFileRead() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

class gcActor_ToggleVerbose : public gcEventActor
{
  public:
    gcActor_ToggleVerbose() {};
    virtual ~gcActor_ToggleVerbose() {};
    virtual bool OperateOn(wxWindow * parent, GCDataStore & dataStore);
};

#endif  // GC_MENU_ACTORS_H

//____________________________________________________________________________________
