// $Id: Converter_ParserUtil.h,v 1.10 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef Converter_ParserUtil_H
#define Converter_ParserUtil_H

#include <string>

using std::string;

//  This file is the base class for all the ParserUtil classes.

class ParserUtil
{
  private:

  protected:
    // No creation of just this class.
    ParserUtil();
    virtual ~ParserUtil();

    // Pulls chars until whitespace, newline, or digit is found.
    bool getWord (istream& is, string& buffer) const ;
    // Pulls chars until whitespace, newline is found.
    bool getName (istream& is, string& buffer) const ;
    // Pulls chars until a non digit is found
    bool getNumber (istream& is, string& buffer) const;
    // Pulls chars until a whitespace, newline, or tab is found
    bool getToken (istream& is, string& buffer) const ;
    // Pulls chars until a newline is found
    bool getLine (istream& is, string& buffer) const;
    // Pulls all spaces and newlines until next non whitespace is found
    bool skipWhiteSpace (istream& is) const;
    // Pulls the next N characters (newline, tabs skipped)
    bool getNextNChars (istream& is, string& buffer, const long& n) const;
    // Pulls the next N nonwhitespace characters (tab, newline, space)
    bool getNextNNonWhiteSpace (istream& is, string& buffer, const long& n) const;
    // Pulls chars until the given character is found.
    bool skipToChar (istream& is, int searchChar) const;

    // returns true if the first non-whitespace character is in the
    // search string, false otherwise
    bool isFirstChar(istream& is, const string& searchString) const;

  public:
};

// Checks against DOS vs Unix newlines
bool isnewline (int ch);

#endif // Converter_ParserUtil_H

//____________________________________________________________________________________
