// $Id: DataSourceException.cpp,v 1.4 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "Converter_DataSourceException.h"
#include "stringx.h"

MarkerLengthMismatchDataError::MarkerLengthMismatchDataError(long front, long back, long length)
    : InconsistentDataError("The input length ("
                            +   ToString(length)
                            +   ") is less than the length covered by the markers ("
                            +   ToString(back)
                            +   " - "
                            +   ToString(front)
                            +   " + 1 = "
                            +   ToString(back-front+1)
                            +   ")")
{
}

OffsetAfterFirstPositionDataError::OffsetAfterFirstPositionDataError(long offset, long firstPosition)
    :   InconsistentDataError("First position of sequence ("
                              +   ToString(firstPosition)
                              +   ") is before the sequence offset ("
                              +   ToString(offset)
                              +   ")")
{
}

RegionEndBeforeLastPositionDataError::RegionEndBeforeLastPositionDataError(long offset, long length, long lastPosition)
    :   InconsistentDataError("End of region (position "
                              +   ToString(offset+length-1)
                              +   " = offset ("
                              +   ToString(offset)
                              +   ") + length ("
                              +   ToString(length)
                              +   ") - 1) is before last position ("
                              +   ToString(lastPosition)
                              +   ")"
        )
{
}

MarkerPositionMismatchDataError::MarkerPositionMismatchDataError(long nmarkers, long npositions)
    : InconsistentDataError("There are "
                            +   ToString(nmarkers)
                            +   ", but only "
                            +   ToString(npositions)
                            +   " positions for them to occupy")
{
}

//____________________________________________________________________________________
