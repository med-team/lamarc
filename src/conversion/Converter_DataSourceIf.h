// $Id: Converter_DataSourceIf.h,v 1.9 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef Converter_DataSourceIf_H
#define Converter_DataSourceIf_H

#include <string>

using std::string;

//  This file is the base class for all the Datasource (DS) classes.
//  Its a pure virtual ensuring that each DS class will know how to write itself as XML

class DataSourceIf
{
  protected:
    virtual void addTabs (int numTabs, string& str) const;
  public:
    virtual ~DataSourceIf();
    virtual string getXML(unsigned int numTabs) const = 0;
};

#endif // Converter_DataSourceIf_H

//____________________________________________________________________________________
