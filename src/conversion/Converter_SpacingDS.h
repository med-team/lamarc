// $Id: Converter_SpacingDS.h,v 1.12 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


// SpacingDS is a representation of the spacing information for a
// region's genetic data.
//
// Each representation owns the following.
//   vector<long> positions;  // a sorted list of marker positions
//   long offset;             // the position of a region's start in the
//                            // user's numbering scheme
//   long map_position;       // the position of the region with respect
//                            // to other regions
//   long length;             // the region's length

// Limited validation is done on construction.  The member function:
// IsConsistentWith(const string& geneticdata) is provided for more
// rigorous validiation.

// Note: when expanding to loci, either add a vector/map layer to
// positions and offset and length, or this becomes a per locus
// describer and the regional map_position will need to move to
// the RegionDS.

#ifndef CONVERTER_SPACINGDS_H
#define CONVERTER_SPACINGDS_H

#include <vector>
#include "Converter_DataSourceIf.h"

using std::vector;

class SpacingDS : public DataSourceIf
{
  private:
    vector<long> m_positions;
    long m_length;
    long m_offset;
    long m_map_position;

    // this is a validator for SpacingDS
    void CheckConsistency(long nmarkers) const;

  public:
    //  Note.  Constructors may throw ConverterBaseError's
    SpacingDS();
    SpacingDS(long length,
              long nmarkers); // this ctor doesn't CheckConsistency as there
    // is no consistency to check!
    SpacingDS(const vector<long>& positions,
              long length,
              long nmarkers);
    SpacingDS(const vector<long>& positions,
              long length,
              long offset,
              long map_position,
              long nmarkers);

    // We'll accept the default copy ctor and operator=.

    virtual ~SpacingDS();

    std::string getXML(unsigned int numTabs) const;   //  From DataSourceIf
};

#endif // CONVERTER_SPACINGDS_H

//____________________________________________________________________________________
