// $Id: Converter_ConverterIf.h,v 1.17 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef Converter_ConverterIf_H
#define Converter_ConverterIf_H

#include <string>

#include "Converter_LamarcDS.h"
#include "Converter_ParserUtil.h"
#include "stringx.h"

using std::string;

class Random;

//  This file is the base class for all the Converter (DS) classes.
//  Its a pure virtual ensuring that each DS class will know how to
//  write itself as XML
//  Added ability to deal with phase unknown data, Jon 2002/02/12
//  Removed phase unknown to new HapConverter class, Jon 2002/03/28

class ConverterIf : public ParserUtil
{
  private:

  protected:

  public:
    ConverterIf();
    virtual ~ConverterIf();
    virtual void addConvertedLamarcDS(LamarcDS& lamarc) = 0;

    // A wrapper around the string to long converter, FromString(),
    // to check that the number was parsed correctly.  Throws a
    // a FileFormatError upon failure.
    long FlagCheck(const string& origstring, const string& msg) const;

    virtual string GetDataType() { return ""; };
    virtual void SetDataType(const string&) {};
    virtual void ProcessData() {};

};

#endif // Converter_ConverterIf_H

//____________________________________________________________________________________
