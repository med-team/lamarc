// $Id: Converter_types.h,v 1.7 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef CONVERTER_TYPES_H
#define CONVERTER_TYPES_H

#include <string>
#include <map>
#include <set>

#include "stringx.h"

using std::string;
using std::map;
using std::vector;

class PopulationDS;
class RegionDS;
class IndividualDS;

typedef map<string, PopulationDS, CIStringCompare> PopMap;
typedef map<string, RegionDS, CIStringCompare> RegionMap;
typedef map<IndividualDS, string> IndividualMap;

typedef PopMap::iterator popmapiterator;
typedef vector<popmapiterator> PopIterVec;
typedef map<string, PopIterVec, CIStringCompare> PopIterMap;
typedef map<string, PopIterMap, CIStringCompare> RegByPopMap;

typedef map<string, vector<long>, CIStringCompare> SpaceMap;

typedef map<string, long, CIStringCompare> MarkerMap;
typedef map<string, string, CIStringCompare> TypeMap;

typedef std::set<string, CIStringCompare> PopNameSet;
typedef map<string, PopNameSet, CIStringCompare> RegPopNameMap;

#endif // CONVERTER_TYPES_H

//____________________________________________________________________________________
