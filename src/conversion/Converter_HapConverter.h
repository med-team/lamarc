// $Id: Converter_HapConverter.h,v 1.13 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


// HapConverter reads the info from a prepared Converter-Haplotype
// format file and uses it to rearrange the innards of a RegionDS.
// Specifically, may consolidate multiple former individuals into a
// single individual as indicated by the read file.

#ifndef CONVERTER_HAPCONVERTER_H
#define CONVERTER_HAPCONVERTER_H

#include <fstream>

#include "Converter_ConverterIf.h"
#include "Converter_RegionDS.h"

using std::string;
using std::vector;

typedef vector<IndividualDS> IndDSVec;

class HapConverter : public ConverterIf
{
  private:
    RegionDS& m_region;
    Random& m_random;
    long m_nindividuals;
    vector<string> m_hapnames;

    HapConverter();                               // deliberately undefined
    HapConverter(const HapConverter&);            // deliberately undefined
    HapConverter& operator=(const HapConverter&); // deliberately undefined

    vector<long> ParsePhaseInfo(ifstream& input) const;
    vector<string> PopHapNames(long ntopop);
    string PopTilDelimiter(istream& input, const char& dlm) const;

    // helper function for ReadHapInfo. The returned container
    // will be empty if further processing is necessary.  May
    // throw a FileFormatError.
    IndDSVec ParseFirstLine(istringstream& firstline,
                            const string& filename,
                            ifstream& filestr);

  public:
    HapConverter(RegionDS& region, Random& m_random);
    virtual ~HapConverter();

    // This function can throw a FileFormatError.
    IndDSVec ReadHapInfo(const string& filename);

    void ReplaceIndividualsWith(IndDSVec& individuals);

    void addConvertedLamarcDS (LamarcDS& lamarc);

};

#endif // CONVERTER_HAPCONVERTER_H

//____________________________________________________________________________________
