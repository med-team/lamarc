// $Id: IndividualDS.cpp,v 1.18 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#include <cassert>
#include "Converter_IndividualDS.h"
#include "Converter_DataSourceException.h"
#include "stringx.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

using namespace std;

//------------------------------------------------------------------------------------

IndividualDS::IndividualDS (const string& name,
                            const Sequence& seq)
    : m_name(name)
{
    m_seq.push_back(seq);
}                               // IndividualDS::IndividualDS

//------------------------------------------------------------------------------------

IndividualDS::IndividualDS (const string& name,
                            const string& seq)
    : m_name(name)
{
    m_seq.push_back(Sequence(seq));
}                               // IndividualDS::IndividualDS

//------------------------------------------------------------------------------------

IndividualDS::IndividualDS (const string& name,
                            const string& seq,
                            const string& dataType)
    : m_name(name)
{
    m_seq.push_back(Sequence(seq, dataType, name));
}                               // IndividualDS::IndividualDS

//------------------------------------------------------------------------------------

IndividualDS::~IndividualDS ()
{
}                               // IndividualDS::~IndividualDS

//------------------------------------------------------------------------------------

string
IndividualDS::getSequence() const
{
    return m_seq[0].asString();
}

//------------------------------------------------------------------------------------

int
IndividualDS::getSequenceLength() const
{
    return m_seq[0].getSequenceLength();
}

//------------------------------------------------------------------------------------

vector<string> IndividualDS::GetAllSeqNames() const
{
    vector<string> seqnames;

    vector<Sequence>::const_iterator seq;
    for(seq = m_seq.begin(); seq != m_seq.end(); ++seq)
    {
        seqnames.push_back(seq->GetName());
    }

    return seqnames;

} // IndividualDS::GetAllSeqNames

//------------------------------------------------------------------------------------

vector<string> IndividualDS::GetHapNames() const
{
    return m_seqnames;
} // IndividualDS::GetHapNames

//------------------------------------------------------------------------------------

string IndividualDS::GetHapNamesForPrint() const
{
    string names;
    vector<string>::const_iterator name;
    for(name = m_seqnames.begin(); name != m_seqnames.end(); ++name)
        names += *name + ",";

    return names;
} // IndividualDS::GetHapNamesForPrint

//------------------------------------------------------------------------------------

bool IndividualDS::HasSequence(const string& seqname) const
{
    vector<Sequence>::const_iterator seq;
    for(seq = m_seq.begin(); seq != m_seq.end(); ++seq)
    {
        if (seq->IsNamed(seqname)) return true;
    }

    return false;

} // IndividualDS::HasSequence

//------------------------------------------------------------------------------------

bool IndividualDS::HasNoSequences() const
{
    return m_seq.empty();

} // IndividualDS::HasNoSequences

//------------------------------------------------------------------------------------

bool IndividualDS::HasNonContiguousData() const
{
    vector<Sequence>::const_iterator seq;
    for(seq = m_seq.begin(); seq != m_seq.end(); ++seq)
    {
        if (seq->HasNonContiguousData())
            return true;
    }

    return false;

} // IndividualDS::HasNonContiguousData

//------------------------------------------------------------------------------------

bool IndividualDS::HasSNPs() const
{
    vector<Sequence>::const_iterator seq;
    for(seq = m_seq.begin(); seq != m_seq.end(); ++seq)
    {
        if (seq->HasSNPs())
            return true;
    }

    return false;

} // IndividualDS::HasSNPs

//------------------------------------------------------------------------------------

Sequence IndividualDS::PopSequence(const string& seqname)
{
    vector<Sequence>::iterator seq;
    for(seq = m_seq.begin(); seq != m_seq.end(); ++seq)
    {
        if (seq->IsNamed(seqname))
        {
            Sequence sequence(*seq);
            m_seq.erase(seq);
            return sequence;
        }
    }

    assert(false); // Shouldn't be able to get here

    Sequence sequence(m_seq.front());
    return sequence;

} // IndividualDS::PopSequence

//------------------------------------------------------------------------------------

string
IndividualDS::getName() const
{
    return m_name;
}

//------------------------------------------------------------------------------------

void
IndividualDS::setName(const string& name)
{
    m_name = name;
}

//------------------------------------------------------------------------------------

string
IndividualDS::getXML(unsigned int numTabs) const
{
    string individualXML;

    addTabs(numTabs, individualXML);
    individualXML += "<individual name=\"" + m_name + "\">\n";

    ++numTabs;
    if (!m_unknownphase.empty())        // if we have phase unknown data
    {
        addTabs(numTabs, individualXML);
        individualXML += "<phase type=\"unknown\">";
        vector<long>::const_iterator marker;
        for(marker = m_unknownphase.begin();
            marker != m_unknownphase.end();
            ++marker)
        {
            individualXML += " " + ToString(*marker);
        }
        individualXML += " </phase>\n";
    }

    vector<Sequence>::const_iterator seqit;
    for(seqit = m_seq.begin(); seqit != m_seq.end(); ++seqit)
        individualXML += seqit->getXML(numTabs);

    --numTabs;
    addTabs(numTabs, individualXML);
    individualXML += "</individual>\n";

    return individualXML;
}

//____________________________________________________________________________________
