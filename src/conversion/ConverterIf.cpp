// $Id: ConverterIf.cpp,v 1.18 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#include <iostream>
#include <sstream>
#include <ctype.h>
#include <string>

#include "Converter_ConverterIf.h"
#include "Converter_DataSourceException.h"
#include "constants.h"
#include "stringx.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

using namespace std;

const long DEFAULTLONG = 0;

//----------------------------------------------------------------------

ConverterIf::ConverterIf()
    : ParserUtil()
{
}

//----------------------------------------------------------------------

ConverterIf::~ConverterIf()
{
}

//----------------------------------------------------------------------

long ConverterIf::FlagCheck(const string& origstring,
                            const string& msg) const
{
    long number;

    if (!FromString(origstring,number))
    {
        string emsg = "\nEncountered an illegal value for " + msg;
        emsg += ", " + origstring + ".\n";
        throw FileFormatError(emsg);
    }

    return number;

} // ConverterIf::FlagCheck

//____________________________________________________________________________________
