// $Id: Converter_ConverterUI.h,v 1.25 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef CONVERTER_UI_H
#define CONVERTER_UI_H

#include <string>
#include <vector>
#include <map>
#include <set>

#include "Converter_RegionDS.h"     // added 2003/11/28 by erynes to compile on Windows
#include "Converter_PopulationDS.h" // added 2003/11/28 by erynes to compile on Windows

#include "Converter_types.h"

using std::string;

class LamarcDS;
class PopulationDS;
class ConverterIf;
class UserFileUtil;

bool isInterleaved();
string getFileName(const UserFileUtil& fileutil);
string getHapFileName(const string& regionname, const UserFileUtil& fileutil);
string getFormat();
string getDataType();
string GetMapFileName();
long getLong(const string& name, bool mustbepositive = false);
long getRegionalMapInfo (const string& regionName, long& length, long& offset);
// SetRegionLength() is a helper function for SetMapInfo()
void SetRegionLength(RegionMap::iterator region);
void SetMapInfo(LamarcDS& dataStore, const string& mapfilename);
void SetHapInfo(LamarcDS& dataStore);

// used in Migrate file conversion, how to read microsats?
bool AreMicrosRegions();

// return true if answer is "yes" and false if answer is "no"
// defaultyes is true if the default, user just carriage returns,
// answer is "yes", false if "no"
bool GetYesOrNo(const string& query, bool defaultyes);

int main();

//------------------------------------------------------------------------------------

// This class stores information on one "unit" (a block of
// sequences from the same region and population).

class Unit
{
  public:
    long regionno;
    string region;
    string population;
    long tips;
    long markers;
    string filename;
    string datatype;
    PopMap::iterator m_pop;

    Unit(long no, const string& reg, const string& pop, long ti,
         long mar, const string& filen, const string& dtype,
         PopMap::iterator mypop)
        : regionno(no), region(reg), population(pop), tips(ti), markers(mar),
          filename(filen), datatype(dtype), m_pop(mypop) {}

    void DisplayUnit() const;

    // this operator allows sorting by region, then population
    bool operator<(const Unit& other) const;
    bool IsIn(const string& regionname, const string& popname) const;
};

//------------------------------------------------------------------------------------

// This class stores and manages a list of Units, allowing the
// user to assign and validate their population and region
// relationships

//------------------------------------------------------------------------------------

class PopRegionRelation
{
  private:
    std::vector<Unit> units;
    std::set<string> GetRegionNames() const;
    std::set<string> GetPopulationNames() const;

    // a pair of helper functions for OverallValid()
    bool RegionsValid() const;
    bool PopsValid() const;

    void WrapAndPrintToCout(const string& msg) const;

  public:
    void AddUnit(Unit unit) { units.push_back(unit); };
    // the following is non-const because it sorts the Units
    void DisplayPopRegionRelation();
    void ChangeRegions();
    void ChangePopulations();
    bool OverallValid() const;
    RegByPopMap GetRegionalByPopMap() const;
};

#endif // CONVERTER_UI_H

//____________________________________________________________________________________
