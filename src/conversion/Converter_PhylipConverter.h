// $Id: Converter_PhylipConverter.h,v 1.15 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


//  Phylip Converter will take an old style phylip file, and create a lamarcDS out of it.
//  Note that this can throw pretty much any ConverterBaseError.
//  Anytime this is used, one should catch and handle these errors.

#ifndef CONVERTER_PHYLIPCONVERTER_H
#define CONVERTER_PHYLIPCONVERTER_H

#include <fstream>

#include "Converter_ConverterIf.h"
#include "Converter_LamarcDS.h"

using std::string;

class Random;

//  Namespace here?

class PhylipConverter : public ConverterIf
{
  private:
    LamarcDS m_lamarc;
    string m_fileName;
    string m_datatype;
    bool m_interleaved;
    ifstream m_inFile;

  public:
    //  Note.  Constructors may throw ConverterBaseError's
    PhylipConverter(const string& fileName,
                    const bool interleaved);

    virtual ~PhylipConverter();

    void addConvertedLamarcDS(LamarcDS&);   //  From ConverterIf

    virtual void SetDataType(const string& dtype) { m_datatype = dtype; };
    virtual string GetDataType() { return m_datatype; };
    virtual void ProcessData();

};

#endif // CONVERTER_PHYLIPCONVERTER_H

//____________________________________________________________________________________
