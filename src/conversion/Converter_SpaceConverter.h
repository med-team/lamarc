// $Id: Converter_SpaceConverter.h,v 1.10 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


// SpaceConverter reads the info from a prepared Converter-Haplotype
// format file and modifies the regions of a LamarcDS.

#ifndef CONVERTER_SPACECONVERTER_H
#define CONVERTER_SPACECONVERTER_H

#include <fstream>
#include <map>

#include "Converter_ConverterIf.h"
#include "Converter_types.h"  // for map typedefs

using std::string;

class LamarcDS;

class SpaceConverter : public ConverterIf
{
  private:
    LamarcDS& m_lamarc;

    SpaceConverter();                                 // deliberately undefined
    SpaceConverter(const SpaceConverter&);            // deliberately undefined
    SpaceConverter& operator=(const SpaceConverter&); // deliberately undefined

    // May throw a FileFormatError.
    void ValidateSpaceInfo(const SpaceMap& spaces,
                           const string& filename) const;

  public:
    SpaceConverter(LamarcDS& lamarc);
    virtual ~SpaceConverter();

    // May throw a FileFormatError.
    SpaceMap ReadSpacingInfo(const string& filename) const;

    void addConvertedLamarcDS(LamarcDS& lamarc);

};

#endif // CONVERTER_SPACECONVERTER_H

//____________________________________________________________________________________
