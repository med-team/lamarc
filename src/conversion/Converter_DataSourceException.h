// $Id: Converter_DataSourceException.h,v 1.12 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef Converter_DataSourceException_H
#define Converter_DataSourceException_H

#include <exception>
#include <string>

using std::string;

//  This file contains the exceptions thrown by datasource classes
//  in the converter directory.  This holds exceptions relating to bad
//  data formats or constraint violations.
//  what() can be called to get a string describing the error.

class ConverterBaseError : public std::exception
{
  public:
    virtual const char* type () const = 0;
};

class ConverterTestError : public ConverterBaseError
{
  private:
    string _what;
  public:
    ConverterTestError(const string& wh): _what (wh) { };
    virtual ~ConverterTestError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "ConverterTestError"; };
};

class DataTypeNotFoundError : public ConverterBaseError
{
  private:
    string _what;
  public:
    DataTypeNotFoundError(const string& wh): _what (wh) { };
    virtual ~DataTypeNotFoundError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "DataTypeNotFoundError"; };
};

class InvalidNucleotideError : public ConverterBaseError
{
  private:
    string _what;
  public:
    InvalidNucleotideError(const string& wh): _what (wh) { };
    virtual ~InvalidNucleotideError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "InvalidNucleotideError"; };
};

class InvalidSequenceLengthError : public ConverterBaseError
{
  private:
    string _what;
  public:
    InvalidSequenceLengthError(const string& wh): _what (wh) { };
    virtual ~InvalidSequenceLengthError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "InvalidSequenceLengthError"; };
};

class InvalidFrequenciesError : public ConverterBaseError
{
  private:
    string _what;
  public:
    InvalidFrequenciesError(const string& wh): _what (wh) { };
    virtual ~InvalidFrequenciesError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "InvalidFrequenciesError"; };
};

class FileNotFoundError : public ConverterBaseError
{
  private:
    string _what;
  public:
    FileNotFoundError(const string& wh): _what (wh) { };
    virtual ~FileNotFoundError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "FileNotFoundError"; };
};

class FileFormatError : public ConverterBaseError
{
  private:
    string _what;
  public:
    FileFormatError(const string& wh): _what (wh) { };
    virtual ~FileFormatError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "FileFormatError"; };
};

class InconsistentDataError : public ConverterBaseError
{
  private:
    string _what;
  public:
    InconsistentDataError(const string& wh): _what (wh) { };
    virtual ~InconsistentDataError() throw() {};
    virtual const char* what () const throw() { return _what.c_str (); };
    virtual const char* type () const { return "InconsistentDataError"; };
};

class MarkerLengthMismatchDataError : public InconsistentDataError
{
  public:
    MarkerLengthMismatchDataError(long front, long back, long length);
    virtual ~MarkerLengthMismatchDataError() throw() {};
};

class OffsetAfterFirstPositionDataError : public InconsistentDataError
{
  public:
    OffsetAfterFirstPositionDataError(long offset, long firstPosition);
    virtual ~OffsetAfterFirstPositionDataError() throw() {};
};

class RegionEndBeforeLastPositionDataError : public InconsistentDataError
{
  public:
    RegionEndBeforeLastPositionDataError(long offset, long length, long lastPosition);
    virtual ~RegionEndBeforeLastPositionDataError() throw() {};
};

class MarkerPositionMismatchDataError : public InconsistentDataError
{
  public:
    MarkerPositionMismatchDataError(long nmarkers, long npositions);
    virtual ~MarkerPositionMismatchDataError() throw() {};
};

#endif // Converter_DataSourceException_H

//____________________________________________________________________________________
