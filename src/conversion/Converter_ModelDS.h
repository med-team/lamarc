// $Id: Converter_ModelDS.h,v 1.8 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


// ModelDS is a representation of the data on any one model.
// Each model owns the following.
// A set of Base Freqs.  These are four two digit frequencies that total 100.  I guess.
// A ttratio
// A rate
// A probabilies

// To be perfectly honest, I've no idea what some of these things are for.  But that
// shouldn't matter.

// Validation is done whenever new freqs are added.
// Model's default constructor defaults to Model F84.  I've no Idea if this is a good thing
// or not, but it seems okay.

#ifndef CONVERTER_MODELDS_H
#define CONVERTER_MODELDS_H

#include <map>

#include "Converter_DataSourceIf.h"

using std::string;

class ModelDS : public DataSourceIf
{
  private:
    string m_modelName;
    std::map<string, double> m_freqs;    // does not need case-insensitivity
    double m_ttRatio;
    unsigned int m_numCategories;
    double m_rates;
    double m_probabilities;

    // Validation for ModelDS
    void validateFreqs(double a, double c, double g, double t);

  public:
    //  Note.  Constructors may throw ConverterBaseError's
    ModelDS();
    ModelDS(const string& modelName,
            const double freqA,
            const double freqC,
            const double freqG,
            const double freqT,
            const double ttRatio);
    ModelDS(const string& modelName,
            const double freqA,
            const double freqC,
            const double freqG,
            const double freqT,
            const double ttRatio,
            const unsigned int numCategories,
            const double rates,
            const double probabilities);

    virtual ~ModelDS();

    // Use the Default Copy Constructor.

    //  get the Model name
    string getName() const;

    //  Allow setting of the model name
    void setName(const string& modelName);

    //  get the Model ttRatio
    double getTTRatio() const;

    //  Allow setting of the model ttRatio
    void setTTRatio(const double modelTTRatio);

    //  get the Model numCategories
    unsigned int getNumCategories() const;

    //  Allow setting of the model numCategories
    void setNumCategories(const unsigned int modelNumCategories);

    //  get the Model rates
    double getRates() const;

    //  Allow setting of the model rates
    void setRates(const double modelRates);

    //  get the Model probabilities
    double getProbabilities() const;

    //  Allow setting of the model probabilities
    void setProbabilities(const double modelProbabilities);

    //  Allow getting of the Freqs.
    double getAFreq();
    double getCFreq();
    double getGFreq();
    double getTFreq();

    //  Allow setting of the freqs.
    //  This may throw a ConverterBaseError
    void setFreqs(double a, double c, double g, double t);

    string getXML(unsigned int numTabs) const;   //  From DataSourceIf
};

#endif // CONVERTER_MODELDS_H

//____________________________________________________________________________________
