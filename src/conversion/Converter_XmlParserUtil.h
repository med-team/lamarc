// $Id: Converter_XmlParserUtil.h,v 1.8 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


#ifndef Converter_XmlParserUtil_H
#define Converter_XmlParserUtil_H

#include <string>
#include <vector>
#include <map>

#include "Converter_ParserUtil.h"

using std::string;
using std::map;

//  This file is the base class for all the ParserUtil classes.

class XmlParserUtil : public ParserUtil
{
  private:
    std::vector<string> m_tagStack;

    // returns true for a start tag.  false for a end tag.
    // Throws if theres an error.
    bool stripTag(string& tagString, map<string, string>& tagInfo) const;

  protected:
    // No creation of just this class.
    XmlParserUtil();

    // finds the next xml tag.  Returns the text of that tag.  Updates the tagStack.
    // The tagName will be in 'TagName'.  This is a dumb way to do it, but it should be okay.
    string getNextTag(istream& is, map<string, string>& tagInfo);
    // Gets the value of the current tag (the text between this and the current tags endtag)
    bool getTagValue(istream& is, string& buffer);
    // Returns the particular current dictionary in the xml tree.
    // Format of the string is dict <space> dict <space> etc...
    string getLocation() const;

    // Returns the top level TagName
    string getTopNodeName() const;

  public:
    virtual ~XmlParserUtil();
};

#endif // Converter_XmlParserUtil_H

//____________________________________________________________________________________
