// $Id: Converter_MigrateConverter.h,v 1.17 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


//  Migrate Converter will take an old style migrate file, and create a lamarcDS out of it.
//  Note that this can throw pretty much any ConverterBaseError.
//  Anytime this is used, one should catch and handle these errors.

#ifndef CONVERTER_MIGRATECONVERTER_H
#define CONVERTER_MIGRATECONVERTER_H

#include <fstream>

#include "Converter_ConverterIf.h"
#include "Converter_LamarcDS.h"

using std::string;

class Random;

//  Namespace here?

class MigrateConverter : public ConverterIf
{
  private:
    LamarcDS m_lamarc;
    string m_fileName;
    string m_datatype;
    bool m_interleaved;
    string m_firstLine;  // saves the first line for re-parsing
    ifstream m_inFile;
    bool m_markerstoregions;

    void getInterleavedSequences (ifstream& infile,
                                  const long numSequences,
                                  const long sequenceLength,
                                  const string& popName,
                                  const string& regionName,
                                  const string& datatype);

    void getNonInterleavedSequences (ifstream& infile,
                                     const long numSequences,
                                     const long sequenceLength,
                                     const string& popName,
                                     const string& regionName,
                                     const string& datatype);

    string getNewName(Random&) const;

    long GetNumPops(istringstream& linestream) const;
    long GetNumLoci(istringstream& linestream) const;
    std::vector<long> GetSeqLengths(istringstream& linestream, long numLoci) const;
    long GetNumSeqs(istringstream& linestream) const;
    string ReadDataType(istringstream& linestream) const;

    void GetMicroSatLoci (ifstream& infile,
                          const long numSequences,
                          const long numLoci,
                          const string& popName,
                          const string& regionName,
                          const string& datatype,
                          const string& delimiter);

  public:
    //  Note.  Constructors may throw ConverterBaseError's
    MigrateConverter(const string& fileName, bool interleaved);

    virtual ~MigrateConverter();

    void addConvertedLamarcDS(LamarcDS&);   //  From ConverterIf

    virtual void SetDataType(const string& dtype) { m_datatype = dtype; };
    virtual string GetDataType() { return m_datatype; };
    virtual void ProcessData();

    void SetMarkersToRegions(bool val);
};

#endif // CONVERTER_MIGRATECONVERTER_H

//____________________________________________________________________________________
