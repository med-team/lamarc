// $Id: Converter_Sequence.h,v 1.18 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
/* Authors: Patrick Colacurcio, Peter Beerli, Mary Kuhner, Jon Yamato 
and Joseph Felsenstein */


// Sequence is a representation of a Nucleotide sequence of any length.
// Each Sequence owns a string that represents the DNA sequence.
// Validation is done on the DNA sequence upon construction.  If any
// invalid nucleotides or symbols are found, an InvalidNucleotide exception
// is thrown.

// The only constructor I'm giving initially is from a string.
// The getLength() method returns the length of the sequence as an int.
// The operator string is not defined, but I'm giving the asString() method.
// This method returns the sequence as a string..

// Note:  This IS a DataSource.  The naming is unfortunate.  I plan on changing that.

#ifndef CONVERTER_SEQUENCE_H
#define CONVERTER_SEQUENCE_H

#include <string>
#include <vector>

#include "Converter_DataSourceIf.h"
#include "constants.h" // for definition of DNA literal.

using std::string;
using std::vector;

class Sequence : public DataSourceIf
{
  private:
    vector<string> m_sequence;
    string m_dataType;
    string m_name;

    Sequence();                                // undefined
    void validate(const string &src) const;    // does the actual validation
    void trimSequence();          // Trims the leading and trailing
    // whitespace from the sequence

    vector<string> AsStringVec(const string& src) const;

  public:
    Sequence(const string& sequenceString);
    Sequence(const string& sequenceString,
             const string& dataType,
             const string& name);
    Sequence(const vector<string>& sequenceString,
             const string& dataType,
             const string& name);

    virtual ~Sequence();

    // Use the Default Copy Constructor and operator=

    long getSequenceLength() const;
    string getDataType() const { return m_dataType; };
    string GetName() const { return m_name; };
    string asString() const;
    void setDataType( const string& );
    void setName( const string& name ) { m_name = name; };
    bool IsNamed( const string& name ) const { return (m_name == name); };
    bool HasNonContiguousData() const { return m_dataType != lamarcstrings::DNA; };
    bool HasSNPs() const { return m_dataType == lamarcstrings::SNP; };

    string getXML(unsigned int numTabs) const;   //  From DataSourceIf
};

#endif // CONVERTER_SEQUENCE_H

//____________________________________________________________________________________
