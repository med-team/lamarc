// $Id: nomenuglobals.h,v 1.10 2018/01/03 21:32:57 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


// This file defines the behaviour of the converter when compiled with the JSIM flag

#ifndef CONV_JSIMGLOBALS_H
#define CONV_JSIMGLOBALS_H

#ifdef JSIM
#include <string>

using std::string;

class convstr
{
  public:
    // the following are mandatory--they must exist
    static string GENETICDATAFILENAME;
    static string OUTFILENAME;
    // end mandatory

    static const string    JSIMPARMFILE; // empty or filename

    static string    HAPFILENAME;          // empty or filename
    static string    MAPFILENAME;          // empty or filename
    static string    GENETICDATATYPE;
    static string    GENETICDATAFORMAT;    // Phylip or Migrate
    static bool      GENETICDATAINTERLEAVED;
    static long      REGIONLENGTH;
    static long      REGIONOFFSET;
    static long      LOCUSMAPPOS;          // what's the locus' map-position?
    // empty = no mapping
    static bool      MICROREGIONS;         // are microsats unlinked
    // (and therefore analyzed as separate regions)?
};

#endif  // JSIM

#endif // CONV_JSIMGLOBALS_H

//____________________________________________________________________________________
