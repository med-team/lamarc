// $Id: definitions.h,v 1.22 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <cmath>                        // for exp() or log()
#include <limits>

// DEFINED stuff
//
// Peter Beerli
//
#ifndef DBL_MAX
//#define DBL_MAX (static_cast<double>(1.7976931348623157e308))
const double DBL_MAX = std::numeric_limits<double>::max();
#endif

#ifndef EPSILON
#define EPSILON         0.000001
#endif

const double GROWTH_EPSILON = 0.0001; // trigger special code when |g| is less than this
const double LOGISTIC_SELECTION_COEFFICIENT_EPSILON = 0.0001; // ditto

#ifndef DBL_EPSILON
#define DBL_EPSILON 2.2204460492503131e-14
#endif

#ifndef NEGMAX
//#define NEGMAX -DBL_MAX
const double NEGMAX = -DBL_MAX;
#endif

#ifndef DBL_BIG
//#define DBL_BIG (static_cast<double>(1.0e300))
const double DBL_BIG = pow(10.0, floor(0.97*log10(DBL_MAX)));
#endif

#ifndef EXPMAX
const double EXPMAX = log(DBL_BIG); //about 688 on our machines
#endif

#ifndef EXPMIN //We want roughly -EXPMAX; -687 on our machines.
//We actually only want a different definition for Microsoft VC++, but the only
// way to get that is by testing _MSC_VER, which Metrowerks also defines
// sometimes.  So we have to define EXPMIN in three places; twice the same way.
// (we need a different version for VC++ because there is no min())
#ifdef __MWERKS__
const double EXPMIN = 0.97*log(std::numeric_limits<double>::min());
#elif defined _MSC_VER
const double EXPMIN = -0.97*EXPMAX;
#else
const double EXPMIN = 0.97*log(std::numeric_limits<double>::min());
#endif
#endif

const double EXP_OF_EXPMAX = exp(EXPMAX);
const double EXP_OF_EXPMIN = exp(EXPMIN);

#ifndef MAXLONG
//#define MAXLONG (static_cast<long>(32000))
const long MAXLONG = std::numeric_limits<long>::max();
#endif

#include "conf.h"

#define MAX(A,B) (((A) > (B)) ? (A) : (B))

#endif // DEFINITONS_H

//____________________________________________________________________________________
