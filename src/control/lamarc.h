// $Id: lamarc.h,v 1.26 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/***************************************************************
 This file (not a class at this time) contains the main driver
 routines for LAMARC.  It does the following:

 (1) Set up registry (main)
 (2) Accept user input (DoUserInput)
 (3) Finalize registry (FinishRegistry)
 (4) Launch chain manager (main)

 Mary Kuhner (based on work by Jon Yamato) April 2001
***************************************************************/

#ifndef LAMARC_H
#define LAMARC_H

class UIInterface;

extern void GetCloseConfirmation();
extern void ParseOptions(long argc, char** argv, bool* isBatch, bool* explicitExit, bool* hasFile, std::string & infileName);
extern bool DoUserInput(long argc, char** argv);
extern void FinishRegistry(UIInterface&);

#if defined(LAMARC_COMPILE_LINUX) || defined(LAMARC_COMPILE_MACOSX)
extern void CatchFileTooBigSignal(int signal);
#endif

extern int main(int, char **);

//------------------------------------------------------------------------------------

#endif // LAMARC_H

//____________________________________________________________________________________
