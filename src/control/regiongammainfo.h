// $Id: regiongammainfo.h,v 1.11 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef REGIONGAMMAINFO_H
#define REGIONGAMMAINFO_H

#include "constants.h"
#include "plotstat.h"
#include "paramstat.h"

class RegionGammaForce;

class RegionGammaInfo
{
  private:
    double m_startValue;
    ParamStatus m_pstatus;
    bool m_doProfile;
    proftype m_proftype;
    std::string m_proftypeSummaryDescription;
    bool m_CurrentlyPerformingAnalysisOverRegions;
    double m_MLE;
    bool m_HaveMLE;
    ProfileStruct m_OverallProfile;
    bool m_HaveProfile;
    RegionGammaForce *m_pRegionGammaForce;
    RegionGammaInfo();
    RegionGammaInfo(const RegionGammaInfo& src);

  public:
    RegionGammaInfo(double startValue, ParamStatus paramStatus,
                    bool doProfile, proftype profType,
                    std::string profTypeSummaryDescription,
                    RegionGammaForce *pRegionGammaForce);
    ~RegionGammaInfo();
    double GetStartValue(void) const { return m_startValue; };
    ParamStatus GetParamStatus(void) const { return m_pstatus; };
    bool DoProfile(void) const { return m_doProfile; };
    proftype GetProfType(void) const;
    std::string GetProftypeSummaryDescription(void) const
    { return m_proftypeSummaryDescription; };
    void Activate(void) { m_CurrentlyPerformingAnalysisOverRegions = true; };
    void Deactivate(void) { m_CurrentlyPerformingAnalysisOverRegions = false; };
    bool CurrentlyPerformingAnalysisOverRegions(void) const
    { return m_CurrentlyPerformingAnalysisOverRegions; };
    void SetMLE(double value) { m_MLE = value; m_HaveMLE = true; };
    bool HaveMLE(void) const { return m_HaveMLE; };
    double GetMLE(void) const { return m_MLE; };
    double GetLowValue(void) const;
    double GetHighValue(void) const;
    double GetLowMultiplier(void) const;
    double GetHighMultiplier(void) const;
    double GetMaxValue(void) const;
    double GetMinValue(void) const;
    void AddProfile(const ProfileStruct& profile)
    { m_OverallProfile = profile; m_HaveProfile = true; };
    bool HaveProfile(void) const { return m_HaveProfile; };
    const ProfileStruct& GetProfile(void) const { return m_OverallProfile; };
    void ConstrainToMax();
    StringVec1d ToXML(long nspaces) const;
};

#endif // REGIONGAMMAINFO_H

//____________________________________________________________________________________
