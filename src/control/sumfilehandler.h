// $Id: sumfilehandler.h,v 1.8 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// The Sumfile Handler is here to handle reading and writing of summary files.
// Earlier versions of the functions here can be found in chainmanager directly,
// since it was pulled out of there due to this sort of thing taking up too
// much space.

// In general, it is owned by and called from the chainmanager object, and
// modifies (by reference) other chainmanager member variables when called
// upon to read a summary file, and reads (by reference) other chainmanager
// member variables when called upon to write a summary file.

#ifndef SUMFILEHANDLER_H
#define SUMFILEHANDLER_H

#include <fstream>
#include "vectorx.h"

class ForceParameters;
class ChainOut;
class ChainPack;
class CollectionManager;

class SumFileHandler
{
  private:
    std::ifstream m_sumin;        // use when reading from a summary file
    std::ofstream m_sumout;       // use when writing to a summary file

    long int m_lastRegion;
    long int m_lastReplicate;
    long int m_lastChain;
    long int m_lastRegionChainSum;
    long int m_lastReplicateChainSum;
    long int m_lastReplicateSummary;
    bool m_regionSummary;

  public:
    SumFileHandler();
    ~SumFileHandler() {};
    SumFileHandler(const SumFileHandler& src);            // undefined
    SumFileHandler& operator=(const SumFileHandler& src); // undefined

    long int GetLastRegion() {return m_lastRegion;};
    long int GetLastReplicate() {return m_lastReplicate;};
    long int GetLastChain() {return m_lastChain;};
    long int GetLastRegionChainSum() {return m_lastRegionChainSum;};
    long int GetLastReplicateChainSum() {return m_lastReplicateChainSum;};
    long int GetLastReplicateSummary() {return m_lastReplicateSummary;};
    bool GetRegionSummary() {return m_regionSummary;};

    void ReadInChainPack          (ChainPack& chainpack);
    void ReadInChainSum           (ChainPack& chainpack,
                                   CollectionManager& collectionmanager, long int numchains);
    void SkipComments             ();
    void ReadInChainOut           (ChainOut &c );
    void ReadInSumFile            (ChainPack& chainpack,
                                   CollectionManager& collectionmanager, long int numchains);
    void ReadInReplicateSummary   (ChainPack& chainpack);
    void ReadInRegionSummary      (ChainPack& chainpack);
    bool ReadInVec1D              (DoubleVec1d& vd, string endtag );
    bool ReadInVec1D              (LongVec1d& vd, string endtag );
    void ReadInEndRegion          (ChainPack& chainpack);
    void ReadInRecover            (ChainPack& chainpack);
    // public only so TreeSummary::ReadInTreeSummary() can use, change if merge
    void ReadInForceParameters    (ForceParameters& fp );
    static void HandleSumOutFileFormatError( string callingfxn );
    static void ReadInCheckFileFormat( string callingfxn, string expectedtag, string readtag );

    void WriteSumFileStart        ();
    void WriteSumFileEnd          (const ChainPack& chainpack);
    void WriteChainSumStart       (long int whichregion, long int whichreplicate,
                                   const CollectionManager& collectionmanager);
    void WriteChainSumEnd         (const CollectionManager& collectionmanager);
    void WriteChainPack           (ChainOut& chainout,const ChainPack& chainpack);
    void WriteLastChain           (const ChainPack& chainpack);
    void WriteRegionSummary       (ForceParameters& fp, double maxlike);
    void WriteReplicateSummary    (ForceParameters& fp, double maxlike,
                                   const ChainPack& chainpack);
    void WriteWhatWasRead         (bool recoversumfile,
                                   long int recover_region,
                                   long int recover_replicate,
                                   long int recover_chaintype,
                                   long int recover_chain,
                                   bool recover_redochain,
                                   bool recover_redomaximization,
                                   long int nregions,
                                   long int nreplicates,
                                   const ChainPack& chainpack,
                                   const CollectionManager& collectionmanager);
    static void WriteVec1D ( std::ofstream& out, vector<double>& vd );
    static void WriteVec1D ( std::ofstream& out, vector<long int>& vd );
    void CloseSumOut();

};

#endif // SUMFILEHANDLER_H

//____________________________________________________________________________________
