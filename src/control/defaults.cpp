// $Id: defaults.cpp,v 1.72 2012/06/30 01:32:39 bobgian Exp $

#include <string>
#include "local_build.h"
#include "defaults.h"

using std::string;

const bool defaults::convert_output_to_eliminate_zeroes = true;

// minimal mutation rate -- used in XpartitionEvent::ThrowIfPopSizeTiny
const double defaults::minMuRate          = 1e-10;

// parameters -- default to these when resonable value not provided or
// calculable
const double defaults::theta              = 0.01;
const double defaults::migration          = 100.0;
const double defaults::divMigration       = 100.0;
const double defaults::disease            = 1.0;
const double defaults::recombinationRate  = 0.01;
const double defaults::growth             = 1.0;
const double defaults::gammaOverRegions   = 1.0; // = exponential dist.
const double defaults::logisticSelection  = 1.0/defaults::theta;
const double defaults::epochtime          = defaults::theta;

// disease location
const long defaults::diseaseLocation    = 1L;

// growth approximation type
const growth_type defaults::growType    = growth_CURVE;

// selection approximation type
const selection_type defaults::selectionType = selection_DETERMINISTIC;

// maximum events
const long defaults::coalEvents           = 100000;
const long defaults::migEvents            = 10000;
const long defaults::diseaseEvents        = 1000;
const long defaults::recEvents            = 1000;
const long defaults::growEvents           = 0; // no such "event" possible
const long defaults::lselectEvents        = 0; // no such "event" possible
const long defaults::epochEvents          = 1000;  // might be a bit small!

// methods
const method_type defaults::thetaMethod        = method_PROGRAMDEFAULT;
const method_type defaults::migMethod          = method_PROGRAMDEFAULT;
const method_type defaults::divMigMethod       = method_PROGRAMDEFAULT;
const method_type defaults::diseaseMethod      = method_PROGRAMDEFAULT;
const method_type defaults::recMethod          = method_PROGRAMDEFAULT;
const method_type defaults::growMethod         = method_PROGRAMDEFAULT;
const method_type defaults::lselectMethod      = method_PROGRAMDEFAULT;
const method_type defaults::divMethod          = method_PROGRAMDEFAULT;

// default boundaries for bayesian prior
const double defaults::lowerboundTheta    = 0.00001;
const double defaults::upperboundTheta    = 10.0;
const double defaults::lowerboundMig      = 0.01;
const double defaults::upperboundMig      = 1000.0;
const double defaults::lowerboundDivMig   = 0.01;
const double defaults::upperboundDivMig   = 1000.0;
const double defaults::lowerboundDisease  = 0.0001;
const double defaults::upperboundDisease  = 1000.0;
const double defaults::lowerboundRec      = 0.00001;
const double defaults::upperboundRec      = 10.0;
const double defaults::lowerboundGrowth   = -500.0;
const double defaults::upperboundGrowth   = 1000.0;
const double defaults::lowerboundLSelect  = -1000.0;
const double defaults::upperboundLSelect  = 5000.0;
const double defaults::lowerboundEpoch    = 0.0001;
const double defaults::upperboundEpoch    = 10.0;

#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
const long   defaults::samplingRate       = 1;
#endif

// default prior types
const priortype defaults::priortypeTheta   = LOGARITHMIC;
const priortype defaults::priortypeMig     = LOGARITHMIC;
const priortype defaults::priortypeDivMig  = LOGARITHMIC;
const priortype defaults::priortypeDisease = LOGARITHMIC;
const priortype defaults::priortypeRec     = LOGARITHMIC;
const priortype defaults::priortypeGrowth  = LINEAR;
const priortype defaults::priortypeLSelect = LINEAR;
const priortype defaults::priortypeEpoch   = LINEAR;

// Min/Max allowable values for bayesian priors.  Used both in
//  prior_interface.cpp and ui_vars_prior.cpp
const double defaults::minboundTheta       = 1e-10;
const double defaults::maxboundTheta       = 100;
const double defaults::minboundMig         = 1e-10;
const double defaults::maxboundMig         = 10000;
const double defaults::minboundDivMig      = 1e-10;
const double defaults::maxboundDivMig      = 10000;
const double defaults::minboundDisease     = 1e-10;
const double defaults::maxboundDisease     = 10000;
const double defaults::minboundRec         = 1e-10;
const double defaults::maxboundRec         = 100;
const double defaults::minboundGrowth      = -5000;
const double defaults::maxboundGrowth      = 15000;
const double defaults::minboundLSelect     = -5000;
const double defaults::maxboundLSelect     = 15000;
const double defaults::minboundEpoch       = 1e-10;
const double defaults::maxboundEpoch       = 1000;

// maximum allowable values for initial parameter estimates
// for any chain.
const double defaults::maxTheta           = 100.0;
const double defaults::minTheta           =   1e-10;
const double defaults::maxMigRate         = 10000.0;
const double defaults::minMigRate         =     0.0;
const double defaults::maxDivMigRate      = 10000.0;
const double defaults::minDivMigRate      =     0.0;
const double defaults::maxDiseaseRate     = 10000.0;
const double defaults::minDiseaseRate     =     0.0;
const double defaults::maxRecRate         = 100.0;
const double defaults::minRecRate         =   0.0;
const double defaults::maxGrowRate        = 15000.0;
const double defaults::minGrowRate        = -5000.0;
const double defaults::maxLSelectCoeff    = 15000.0;
const double defaults::minLSelectCoeff    = -5000.0;
const double defaults::maxGammaOverRegions = 100.0;
const double defaults::minGammaOverRegions =   1e-4;
const double defaults::maxEpoch           = 1000.0;
const double defaults::minEpoch           =   1e-10;

// maximum allowable values during parameter estimation (maximization)
const double defaults::maximization_maxTheta           = 1500.0;
const double defaults::maximization_minTheta           =   1e-12;
const double defaults::maximization_maxMigRate         = 10000.0;
const double defaults::maximization_minMigRate         = 0.0;
const double defaults::maximization_maxDiseaseRate     = 10000.0;
const double defaults::maximization_minDiseaseRate     = 0.0;
const double defaults::maximization_maxRecRate         = 500.0;
const double defaults::maximization_minRecRate         = 0.0;
const double defaults::maximization_maxGrowRate        = 25000.0;
const double defaults::maximization_minGrowRate        = -5000.0;
const double defaults::maximization_maxLSelectCoeff    = 25000.0;
const double defaults::maximization_minLSelectCoeff    = -5000.0;
const double defaults::maximization_maxGammaOverRegions = 100.0;
const double defaults::maximization_minGammaOverRegions =   1e-4;
const double defaults::maximization_maxEpoch           = 1500.0;
const double defaults::maximization_minEpoch           =   1e-12;

// arrangers
const double defaults::dropArrangerTiming         = 1.0;
const double defaults::sizeArrangerTiming         = 0.2;
const double defaults::haplotypeArrangerTiming    = 0.0;
const double defaults::probhapArrangerTiming      = 0.2;
const double defaults::bayesianArrangerTiming     = 0.0;
const bool   defaults::useBayesianAnalysis        = false;

// temperatures
const long defaults::temperatureInterval                  = 10;
const bool defaults::useAdaptiveTemperatures              = false;
const double defaults::minTemperature                     = 1.0;
const double defaults::secondTemperature                  = 1.1;

// chains
const long defaults::initial              = 0;
const long defaults::initNChains          = 10;
const long defaults::initNSamples         = 500;
const long defaults::initInterval         = 20;
const long defaults::initDiscard          = 1000;

const long defaults::final                = 1;
const long defaults::finalNChains         = 2;
const long defaults::finalNSamples        = 10000;
const long defaults::finalInterval        = 20;
const long defaults::finalDiscard         = 1000;

// replicates
const long defaults::replicates           = 1;
const long defaults::geyeriters           = 100000;

//Max num heated chains.
const long defaults::maxNumHeatedChains   = 30;

// error conditions
const long defaults::tooManyDenovo        = 20;

// default user parameters for profiling
const bool defaults::doProfile = true;
const proftype defaults::profileType = profile_PERCENTILE;

// default group parameter status
const pstatus defaults::groupPstat = pstat_identical;

//Key parameters for profiling estimation algorithms.
const double defaults::highvalTheta     = 10;
const double defaults::lowvalTheta      = .0001;
const double defaults::highmultTheta    = 10;
const double defaults::lowmultTheta     = .1;

const double defaults::highvalMig       = 1000;
const double defaults::lowvalMig        = 1;
const double defaults::highmultMig      = 10;
const double defaults::lowmultMig       = .1;

const double defaults::highvalDisease   = 100;
const double defaults::lowvalDisease    = 1;
const double defaults::highmultDisease  = 10;
const double defaults::lowmultDisease   = .1;

const double defaults::highvalRec       = .1;
const double defaults::lowvalRec        = .0001;
const double defaults::highmultRec      = 5;
const double defaults::lowmultRec       = .1;

const double defaults::highvalGrowth    = 1000;
const double defaults::lowvalGrowth     = -10;
const double defaults::highmultGrowth   = 10;
const double defaults::lowmultGrowth    = 10;

const double defaults::highvalLSelect   = 1000;
const double defaults::lowvalLSelect    = -100;
const double defaults::highmultLSelect  = 10;
const double defaults::lowmultLSelect   = 10;

const double defaults::highvalGammaOverRegions  = 100;
const double defaults::lowvalGammaOverRegions   = .0001;
const double defaults::highmultGammaOverRegions = 1;
const double defaults::lowmultGammaOverRegions  = .1;

const double defaults::highvalEpoch     = 100;
const double defaults::lowvalEpoch      = .0001;
const double defaults::highmultEpoch    = 10;
const double defaults::lowmultEpoch     = .1;

// defaults for regions
const double defaults::effpopsize       = 1.0;

// defaults for datamodels
const long defaults::nucleotideBins     = 4;

// default number of bins for allelic and microsat stepwise models
const long defaults::bins               = 100;

// default number of bins for brownian model
const long defaults::brownianBins       = 3; //mean, variance, cumulative total

// default maximum separation (functionally) allowable between any two alleles
const long defaults::threshhold           = 20;

//Default external ranges for the model to explore.  In the stepwise
// model, we want a wider range than the mixedks model, since in the
// former we can allow some wandering at the edges, but in the latter,
// you don't want to spend a lot of time bouncing around in unused space.
//
// These may eventually be settable in the UI, but we need to find a clear
// way to explain to the user what it is they're setting if so.
const long defaults::step_allowance       = 5;
const long defaults::mixedks_allowance    = 1;

const string defaults::startMethod        = "user";

// default values for userparams

const string defaults::curvefileprefix      = "curvefile";
const string defaults::mapfileprefix        = "mapfile";
const string defaults::reclocfileprefix     = "reclocfile";
const string defaults::tracefileprefix      = "tracefile";
const string defaults::newicktreefileprefix = "newick";
#ifdef LAMARC_QA_TREE_DUMP
const string defaults::argfileprefix        = "ARGs";
#endif
const string defaults::datafilename         = "infile.xml";
const string defaults::resultsfilename      = "outfile.txt";
const string defaults::treesuminfilename    = "insumfile.xml";
const string defaults::treesumoutfilename   = "outsumfile.xml";
const string defaults::xmloutfilename       = "menusettings_infile.xml";
const string defaults::xmlreportfilename    = "report.xml";
const string defaults::profileprefix        = "profile";

const verbosity_type defaults::verbosity    = NORMAL;
const verbosity_type defaults::progress     = NORMAL;

const time_t defaults::programstarttime     = -1;
const long   defaults::randomseed           = -1L;      // bogus value
const bool   defaults::hasoldrandomseed     = false;

const bool defaults::plotpost               = false;
const bool defaults::readsumfile            = false;
const bool defaults::useoldrandomseed       = true;
const bool defaults::usesystemclock         = true;
const bool defaults::writecurvefiles        = true;
const bool defaults::writereclocfiles       = false;
const bool defaults::writetracefiles        = true;
const bool defaults::writenewicktreefiles   = false;
#ifdef LAMARC_QA_TREE_DUMP
const bool defaults::writeargfiles          = false;
const bool defaults::writemanyargs          = false;
#endif //  LAMARC_QA_TREE_DUMP
const bool defaults::writesumfile           = false;

// default values for data models
const double            defaults::minLegalFrequency     = 0.00001;

const ModelTypeVec1d    defaults::allDataModels()
{
    static ModelTypeVec1d possibleModels;
    //Models for DNA/SNP data:
    possibleModels.push_back(F84);
    possibleModels.push_back(GTR);
    //Models for microsat/eletrophoretic/phenotype data:
    possibleModels.push_back(KAllele);
    //Models for microsat-only data:
    possibleModels.push_back(Stepwise);
    possibleModels.push_back(MixedKS);
    possibleModels.push_back(Brownian);
    return possibleModels;
}

const model_type    defaults::dataModelType     = F84;
const bool          defaults::doNormalize       = false;
const double        defaults::autoCorrelation   = 1.0;
const double        defaults::ttratio           = 2.0;
const bool          defaults::calcFreqsFromData = true;
const double        defaults::categoryProbability = 1.0;

const DoubleVec1d   defaults::categoryProbabilities()
{
    static const DoubleVec1d catProbs(1,defaults::categoryProbability);
    return catProbs;
}

const double        defaults::categoryRate      = 1.0;
const double        defaults::categoryRateMultiple = 2.0;

const DoubleVec1d   defaults::categoryRates()
{
    static const DoubleVec1d catRates(1,defaults::categoryRate);
    return catRates;
}

const DoubleVec1d   defaults::chainTemperatures()
{
    static const DoubleVec1d chainTemps(1,defaults::minTemperature);
    return chainTemps;
}

const double        defaults::baseFrequencyA    = 0.25;
const double        defaults::baseFrequencyC    = 0.25;
const double        defaults::baseFrequencyG    = 0.25;
const double        defaults::baseFrequencyT    = 0.25;
const double        defaults::gtrRateAC         = 0.1;
const double        defaults::gtrRateAG         = 0.1;
const double        defaults::gtrRateAT         = 0.1;
const double        defaults::gtrRateCG         = 0.1;
const double        defaults::gtrRateCT         = 0.1;
const double        defaults::gtrRateGT         = 0.1;
const double        defaults::relativeMuRate    = 1.0;
const double        defaults::KS_alpha          = 0.5;
const bool          defaults::optimize_KS_alpha = false;
const long          defaults::numCategories     = 1;

const long          defaults::maxNumCategories  = 10;

const double        defaults::per_base_error_rate= 0.0;

const double        defaults::perThetaChange = 0.01;

const long          defaults::numSemiUniqueBranches = 1000000;

#ifdef LAMARC_QA_SINGLE_DENOVOS
const long          defaults::numDenovos = 500000;
#endif // LAMARC_QA_SINGLE_DENOVOS

//____________________________________________________________________________________
