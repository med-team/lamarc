// $Id: constants.cpp,v 1.29 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <string>
#include "constants.h"

const int lamarccodes::cleanReturn      = 0;
const int lamarccodes::badAllocation    = 1;
const int lamarccodes::fileError        = 2;
const int lamarccodes::optionError      = 4;
const int lamarccodes::denovoCompileError = 8;
const int lamarccodes::unknownError     = 16;

const std::string lamarcstrings::COAL = "coalesce";
const std::string lamarcstrings::MIG = "migrate";
const std::string lamarcstrings::DISEASE = "disease";
const std::string lamarcstrings::REC = "recombine";
const std::string lamarcstrings::GROW = "grow";
const std::string lamarcstrings::REGION_GAMMA = "gamma over regions";
const std::string lamarcstrings::INVALID = "invalid";
const std::string lamarcstrings::EXPGROWSTICK = "stick grow exponential";
const std::string lamarcstrings::LOGISTICSELECTION = "logistic selection";
const std::string lamarcstrings::LOGSELECTSTICK = "stick logistic selection";
const std::string lamarcstrings::DIVERGENCE = "divergence";
const std::string lamarcstrings::DIVMIG = "divmigrate";

const std::string lamarcstrings::STICK = "stick";
const std::string lamarcstrings::TIP = "tip";
const std::string lamarcstrings::BASE = "base";

const std::string lamarcstrings::SNP = "SNP";
const std::string lamarcstrings::DNA = "DNA";
const std::string lamarcstrings::NUC = "NUC";
const std::string lamarcstrings::MICROSAT = "MICROSAT";

const std::string lamarcstrings::PANEL = "Panel";
const std::string lamarcstrings::STUDY = "Study";
const std::string lamarcstrings::EMPTY = "";

const std::string lamarcstrings::F84 = "F84";
const std::string lamarcstrings::GTR = "GTR";
const std::string lamarcstrings::STEPWISE = "Stepwise";
const std::string lamarcstrings::BROWNIAN = "Brownian";
const std::string lamarcstrings::KALLELE = "KAllele";
const std::string lamarcstrings::MIXEDKS = "MixedKS";

const std::string lamarcstrings::ELECTRO = "ELECTRO";

const std::string lamarcstrings::longNameUSER = "USER";
const std::string lamarcstrings::longNamePROGRAMDEFAULT = "PROGRAMDEFAULT";
const std::string lamarcstrings::longNameFST = "FST";
const std::string lamarcstrings::longNameWATTERSON = "WATTERSON";

const std::string lamarcstrings::shortNameUSER = "USR";
const std::string lamarcstrings::shortNamePROGRAMDEFAULT = "DEF";
const std::string lamarcstrings::shortNameFST = "FST";
const std::string lamarcstrings::shortNameWATTERSON = "WAT";

const std::string lamarcstrings::longBrownianName = "Brownian";
const std::string lamarcstrings::longF84Name = "Felsenstein '84";
const std::string lamarcstrings::longGTRName = "General Time Reversible";
const std::string lamarcstrings::longKAlleleName = "K Allele";
const std::string lamarcstrings::longStepwiseName = "Stepwise";
const std::string lamarcstrings::longMixedKSName = "Mixed KAllele-Stepwise";

const std::string lamarcstrings::shortBrownianName = "Brownian";
const std::string lamarcstrings::shortF84Name = "F84";
const std::string lamarcstrings::shortGTRName = "GTR";
const std::string lamarcstrings::shortKAlleleName = "KAllele";
const std::string lamarcstrings::shortStepwiseName = "Stepwise";
const std::string lamarcstrings::shortMixedKSName = "MixedKS";

const std::string lamarcstrings::shortCurveName =   "CURVE";

// ignoring case, lamarcstrings::shortStickExpName must be the same as
// xmlstr::XML_ATTRVALUE_STICK for use in
// stringx.cpp::StringMatchesGrowthType() used by
// stringx.cpp::ProduceGrowthTypeOrBarf()
const std::string lamarcstrings::shortStickExpName = "STICK-EXP";
const std::string lamarcstrings::shortStickName = "STICK";
const std::string lamarcstrings::longCurveName  =   "Analytical (Curve) Growth Approx.";
const std::string lamarcstrings::longStickExpName  = "Constant (Stick) Exponential Growth Approx.";
const std::string lamarcstrings::longStickName  = "Linear (Stick) Growth Approx.";

const std::string lamarcstrings::longExpName = "Exponential";
const std::string lamarcstrings::shortExpName = "Exponential";
const std::string lamarcstrings::longStairStepName = "Stair Step";
const std::string lamarcstrings::shortStairStepName = "StairStep";

const std::string lamarcstrings::longDSelectionName = "Log-Deterministic Selection";
const std::string lamarcstrings::longSSelectionName = "Stochastic (Stick) Selection";
const std::string lamarcstrings::shortDSelectionName = "Deterministic Selection";
const std::string lamarcstrings::shortSSelectionName = "Stochastic Selection";

//____________________________________________________________________________________

bool IsMigrationLike(force_type f)
{
  return (f==force_MIG || f==force_DIVMIG);
}

bool IsLocalPartForce(force_type f)
{ 
   return (f==force_DISEASE); 
}
