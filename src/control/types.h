// $Id: types.h,v 1.26 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef TYPES_H
#define TYPES_H

#include <list>
#include <string>
#include <map>
#include <memory>

#include "vectorx.h"
#include "shared_ptr.hpp"

class Branch;
class DataType;
class DataModel;
class Cell;
struct triplet;
class DLCalculator;

typedef boost::shared_ptr<Cell> Cell_ptr;
typedef std::pair<long,long>   wakestats;

typedef std::pair<std::string,long> TagLine;

typedef std::map<std::string,DoubleVec2d> percentmap;     // DoubleVec2d dim:
// pop X reg
typedef std::map<std::string,DoubleVec1d> overpercentmap; // DoubleVec1d dim:
// dim: by pop

typedef std::map<std::string, std::pair<long, long> > ratemap; // acceptance rate per arranger

typedef boost::shared_ptr<Branch>  Branch_ptr;
typedef boost::weak_ptr<Branch>    weakBranch_ptr;
typedef std::list<Branch_ptr>      Branchlist;
typedef Branchlist::iterator       Branchiter;
typedef Branchlist::const_iterator Branchconstiter;

typedef double*** cellarray;
typedef std::multimap<triplet, cellarray> FreeStore;

typedef boost::shared_ptr<DataType> DataType_ptr;
typedef boost::shared_ptr<DataModel> DataModel_ptr;

typedef std::pair<double, double> centilepair;

typedef boost::shared_ptr<DLCalculator> DLCalc_ptr;

typedef std::map<force_type, long>  FPartMap;
typedef std::map<force_type, long>::iterator  FPartMapiter;

typedef short xpart_t;  // type representing a number of partitions or
                        // cross-partitions.  Change this if you expect to
                        // have too many population x disease state x whatever
                        // combos to fit in the current size.
typedef std::vector<xpart_t> XPartVec1d;
typedef std::vector<std::vector<xpart_t> > XPartVec2d;

#endif // TYPES_H

//____________________________________________________________________________________
