// $Id: chainout.cpp,v 1.14 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include "chainout.h"

#ifdef DMALLOC_FUNC_CHECK
#include "/usr/local/include/dmalloc.h"
#endif

ChainOut::ChainOut()
    : m_badtrees(FLAGLONG),
      m_tinypoptrees(FLAGLONG),
      m_zerodltrees(FLAGLONG),
      m_stretchedtrees(FLAGLONG),
      m_accrate(FLAGDOUBLE),
      m_numtemps(FLAGLONG),
      m_swaprates(),
      m_tempaccrates(),
      m_temperatures(),
      m_bayesunique(),
      m_starttime(0),
      m_endtime(0),
      m_estimates(unknown_region),
      m_llikemle(FLAGDOUBLE),
      m_llikedata(NEG_MAX)
{
    // deliberately blank
} // ChainOut

//------------------------------------------------------------------------------------

void ChainOut::SetStarttime()
{
#ifdef NOTIME_FUNC
    // The system does not provide a clock.  All times are 0.
    m_starttime = 0;
#else
    // Return "real" time.
    m_starttime = time(NULL);
#endif
}

//------------------------------------------------------------------------------------

void ChainOut::SetEndtime()
{
#ifdef NOTIME_FUNC
    // The system does not provide a clock.  All times are 0.
    m_endtime = 0;
#else
    // Return "real" time.
    m_endtime = time(NULL);
#endif
}

//____________________________________________________________________________________
