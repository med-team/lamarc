// $Id: xmlsum_strings.cpp,v 1.21 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


// strings assigned here should be defined in constants.h as
// public static const string xmlsum::members of class xmlsum

//------------------------------------------------------------------------------------
// xml tags for sumfile
//
//

#include "xmlsum_strings.h"
#include <string>

using std::string;

const string xmlsum::COMMENT_START              ="<!--";
const string xmlsum::COMMENT_END                ="-->";

// chainsum related xml tags
const string xmlsum::SUMFILE_START              ="<XML-summary-file>";
const string xmlsum::SUMFILE_END                ="</XML-summary-file>";
const string xmlsum::CHAINSUM_START             ="<chainsum>";
const string xmlsum::CHAINSUM_END               ="</chainsum>";
const string xmlsum::REG_REP_START              ="<reg_rep>";
const string xmlsum::REG_REP_END                ="</reg_rep>";
const string xmlsum::END_REGION_START           ="<end-region>";
const string xmlsum::END_REGION_END             ="</end-region>";

// chainpack related xml tags, includes all chainouts
const string xmlsum::CHAINPACK_START            ="<chainpack>";
const string xmlsum::CHAINPACK_END              ="</chainpack>";

// chainout related xml tags, includes estimate tags
const string xmlsum::ALPHA_END                  ="</alpha>";
const string xmlsum::ALPHA_START1               ="<alpha";
const string xmlsum::ALPHA_START2               ="locus=";
const string xmlsum::ALPHA_START3               =">";
const string xmlsum::ACCRATE_END                ="</accrate>";
const string xmlsum::ACCRATE_START              ="<accrate>";
const string xmlsum::BADTREES_END               ="</badtrees>";
const string xmlsum::BADTREES_START             ="<badtrees>";
const string xmlsum::BAYESUNIQUE_END            ="</bayes_unique>";
const string xmlsum::BAYESUNIQUE_START          ="<bayes_unique>";
const string xmlsum::CHAINOUT_END               ="</chainout>";
const string xmlsum::CHAINOUT_START             ="<chainout>";
const string xmlsum::ENDTIME_END                ="</endtime>";
const string xmlsum::ENDTIME_START              ="<endtime>";
const string xmlsum::LLIKEDATA_END              ="</llikedata>";
const string xmlsum::LLIKEDATA_START            ="<llikedata>";
const string xmlsum::LLIKEMLE_END               ="</llikemle>";
const string xmlsum::LLIKEMLE_START             ="<llikemle>";
const string xmlsum::MAP_END                    ="</map>";
const string xmlsum::MAP_START                  ="<map>";
const string xmlsum::NUMBER_END                 ="</number>";
const string xmlsum::NUMBER_START               ="<number>";
const string xmlsum::RATES_END                  ="</rates>";
const string xmlsum::RATES_START                ="<rates>";
const string xmlsum::STARTTIME_END              ="</starttime>";
const string xmlsum::STARTTIME_START            ="<starttime>";
const string xmlsum::STRETCHEDTREES_END         ="</stretchedtrees>";
const string xmlsum::STRETCHEDTREES_START       ="<stretchedtrees>";
const string xmlsum::SWAPRATES_END              ="</swaprates>";
const string xmlsum::SWAPRATES_START            ="<swaprates>";
const string xmlsum::TEMPERATURES_END           ="</temperatures>";
const string xmlsum::TEMPERATURES_START         ="<temperatures>";
const string xmlsum::TINYTREES_END              ="</tinytrees>";
const string xmlsum::TINYTREES_START            ="<tinytrees>";
const string xmlsum::ZERODLTREES_END            ="</zerodltrees>";
const string xmlsum::ZERODLTREES_START          ="<zerodltrees>";

// estimate specific xml tags
const string xmlsum::EPOCHTIMES_START           ="<epochtimes>";
const string xmlsum::EPOCHTIMES_END             ="</epochtimes>";
const string xmlsum::ESTIMATES_START            ="<estimates>";
const string xmlsum::ESTIMATES_END              ="</estimates>";
const string xmlsum::THETAS_START               ="<thetas>";
const string xmlsum::THETAS_END                 ="</thetas>";
const string xmlsum::MIGRATES_START             ="<migrates>";
const string xmlsum::MIGRATES_END               ="</migrates>";
const string xmlsum::DIVMIGRATES_START          ="<divmigrates>";
const string xmlsum::DIVMIGRATES_END            ="</divmigrates>";
const string xmlsum::RECRATES_START             ="<recrates>";
const string xmlsum::RECRATES_END               ="</recrates>";
const string xmlsum::GROWTHRATES_START          ="<growthrates>";
const string xmlsum::GROWTHRATES_END            ="</growthrates>";
const string xmlsum::LOGISTICSELECTION_START = "<logisitic-selection-coefficient>";
const string xmlsum::LOGISTICSELECTION_END = "</logisitic-selection-coefficient>";
const string xmlsum::DISEASERATES_START         ="<diseasethrates>";
const string xmlsum::DISEASERATES_END           ="</diseasethrates>";
const string xmlsum::GAMMAOVERREGIONS_START     ="<ShapeParameterForGammaOverRegions>";
const string xmlsum::GAMMAOVERREGIONS_END       ="</ShapeParameterForGammaOverRegions>";

// tree smmary xml tags
const string xmlsum::TREESUM_START              ="<treesum>";
const string xmlsum::TREESUM_END                ="</treesum>";

const string xmlsum::NCOPY_START                ="<ncopy>";
const string xmlsum::NCOPY_END                  ="</ncopy>";

const string xmlsum::SHORTFORCE_START           ="<shortforce>";
const string xmlsum::SHORTFORCE_END             ="</shortforce>";
const string xmlsum::INTERVALS_START            ="<intervals>";
const string xmlsum::INTERVALS_END              ="</intervals>";

const string xmlsum::SHORTPOINT_START           ="<shortpoint>";
const string xmlsum::SHORTPOINT_END             ="</shortpoint>";
const string xmlsum::SHORTWAIT_START            ="<shortwait>";
const string xmlsum::SHORTWAIT_END              ="</shortwait>";
const string xmlsum::SHORTPICK_START            ="<shortpick>";
const string xmlsum::SHORTPICK_END              ="</shortpick>";
const string xmlsum::SHORTPICK_FORCE_START      ="<shortpickforce>";
const string xmlsum::SHORTPICK_FORCE_END        ="</shortpickforce>";

// tree summary full-interval xml tags
const string xmlsum::FORCE_START                ="<force>";
const string xmlsum::FORCE_END                  ="</force>";
const string xmlsum::XPARTLINES_START           ="<xpartlines>";
const string xmlsum::XPARTLINES_END             ="</xpartlines>";
const string xmlsum::PARTLINES_START            ="<partlines>";
const string xmlsum::PARTLINES_END              ="</partlines>";
const string xmlsum::RECWEIGHT_START            ="<recweight>";
const string xmlsum::RECWEIGHT_END              ="</recweight>";
const string xmlsum::OLDSTATUS_START            ="<oldstatus>";
const string xmlsum::OLDSTATUS_END              ="</oldstatus>";
const string xmlsum::NEWSTATUS_START            ="<newstatus>";
const string xmlsum::NEWSTATUS_END              ="</newstatus>";
const string xmlsum::RECPOINT_START             ="<recpoint>";
const string xmlsum::RECPOINT_END               ="</recpoint>";
const string xmlsum::PARTNERPICKS_START         ="<partnerpicks>";
const string xmlsum::PARTNERPICKS_END           ="</partnerpicks>";

//Parameter summary tags
const string xmlsum::PARAM_SUMMARY_START        ="<param-summary>";
const string xmlsum::PARAM_SUMMARY_END          ="</param-summary>";

//Map summary tags
const string xmlsum::MAP_SUMMARY_START          ="<map-summary>";
const string xmlsum::MAP_SUMMARY_END            ="</map-summary>";
const string xmlsum::MAP_SUMMARIES_START        ="<map-summaries>";
const string xmlsum::MAP_SUMMARIES_END          ="</map-summaries>";

// region and replicate summary xml tags

const string xmlsum::REGION_SUMMARY_START       ="<region-summary>";
const string xmlsum::REGION_SUMMARY_END         ="</region-summary>";
const string xmlsum::REPLICATE_SUMMARY_START    ="<replicate-summary>";
const string xmlsum::REPLICATE_SUMMARY_END      ="</replicate-summary>";
const string xmlsum::MAXLIKE_START              ="<maxlike>";
const string xmlsum::MAXLIKE_END                ="</maxlike>";

//____________________________________________________________________________________
