// $Id: regiongammainfo.cpp,v 1.9 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, Eric Rynes and Joseph Felsenstein */


#include <cassert>

#include "regiongammainfo.h"
#include "force.h"
#include "stringx.h"
#include "xml_strings.h"

//------------------------------------------------------------------------------------

RegionGammaInfo::RegionGammaInfo(double startValue, ParamStatus paramStatus,
                                 bool doProfile, proftype profType,
                                 std::string profTypeSummaryDescription,
                                 RegionGammaForce *pRegionGammaForce)
    : m_startValue(startValue),
      m_pstatus(paramStatus),
      m_doProfile(doProfile),
      m_proftype(profType),
      m_proftypeSummaryDescription(profTypeSummaryDescription),
      m_CurrentlyPerformingAnalysisOverRegions(false),
      m_HaveMLE(false),
      m_HaveProfile(false),
      m_pRegionGammaForce(pRegionGammaForce)
{
}

RegionGammaInfo::~RegionGammaInfo()
{
    delete m_pRegionGammaForce;
}

proftype RegionGammaInfo::GetProfType(void) const
{
    if (m_doProfile)
    {
        return m_proftype;
    }
    return profile_NONE;
}

double RegionGammaInfo::GetLowValue(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetLowVal();
}

double RegionGammaInfo::GetHighValue(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetHighVal();
}

double RegionGammaInfo::GetLowMultiplier(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetLowMult();
}

double RegionGammaInfo::GetHighMultiplier(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetHighMult();
}

double RegionGammaInfo::GetMaxValue(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetMaximizerMaxVal();
}

double RegionGammaInfo::GetMinValue(void) const
{
    if (!m_pRegionGammaForce)
        throw implementation_error("RegionGammaInfo is missing m_pRegionGammaForce");
    return m_pRegionGammaForce->GetMaximizerMinVal();
}

void RegionGammaInfo::ConstrainToMax()
{
    if (m_pstatus.Status()==pstat_constant)
    {
        assert(false);
        throw implementation_error("Tried to constrain alpha to its maximum value when it was already constrained.");
    }
    m_pstatus = ParamStatus(pstat_constant);
    m_doProfile = false;
    m_HaveProfile = false; // possibly unnecessary
    m_startValue = GetMaxValue();
}

StringVec1d RegionGammaInfo::ToXML(long nspaces) const
{
    StringVec1d xmllines;
    string line = MakeIndent(MakeTag(xmlstr::XML_TAG_REGION_GAMMA),nspaces);
    xmllines.push_back(line);

    nspaces += INDENT_DEPTH;
    string mytag(MakeTag(xmlstr::XML_TAG_START_VALUES));
    DoubleVec1d startvalues;
    startvalues.push_back(m_startValue);
    line = MakeIndent(mytag,nspaces) + ToString(startvalues,5)
        + " " + MakeCloseTag(mytag);
    xmllines.push_back(line);

    mytag = MakeTag(xmlstr::XML_TAG_PROFILES);
    string profile_st;
    if (m_doProfile)
    {
        profile_st = ToString(m_proftype);
    }
    else
    {
        profile_st = ToString(profile_NONE);
    }
    line = MakeIndent(mytag,nspaces) + " " + profile_st
        + " " + MakeCloseTag(mytag);
    xmllines.push_back(line);

    mytag = MakeTag(xmlstr::XML_TAG_CONSTRAINTS);
    line = MakeIndent(mytag,nspaces) + " " + ToString(m_pstatus)
        + " " + MakeCloseTag(mytag);
    xmllines.push_back(line);

    //The gamma doesn't have groups or a prior.  Change this if we give it some!

    nspaces -= INDENT_DEPTH;
    line = MakeIndent(MakeCloseTag(xmlstr::XML_TAG_REGION_GAMMA),nspaces);
    xmllines.push_back(line);
    return xmllines;
}

//____________________________________________________________________________________
