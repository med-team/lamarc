// $Id: chainout.h,v 1.18 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/********************************************************************
 Class ChainOut contains summary information about the results of
 a chain, such as the acceptance rate, parameter estimates, and
 start/end times.  It does *not* contain the summary trees, which
 are stored separately in a Collector object.

 ChainOut objects are normally stored and organized in the ChainPack.
 They can reasonably be passed by value as they are fairly small,
 simple objects.

 ChainOut has ChainPack as a friend, since the two classes are
 tightly coupled.  This would be fairly easy to change if desired.

 Written by Mary Kuhner
*********************************************************************/

#ifndef CHAINOUT_H
#define CHAINOUT_H

#include <ctime>
#include <map>
#include <string>

#include "types.h"
#include "forceparam.h"

class ChainOut
{
  private:
    // chain information
    long m_badtrees;                // number of trees discarded due to limits
    long m_tinypoptrees;            // number of trees discarded due to small popsizes
    long m_zerodltrees;             // number of trees discarded due to 0 data likelihood
    long m_stretchedtrees;          // number of trees discarded due to long branches
    double m_accrate;               // overall (cold) acceptance rate
    ratemap m_rates;                // (cold) acceptance rate per arranger
    long m_numtemps;                // number of temperatures
    DoubleVec1d m_swaprates;        // heating swap rates between pairs
    DoubleVec1d m_tempaccrates;     // acceptance rates per temperature
    DoubleVec1d m_temperatures;     // average temperatures [adaptive/static]
    LongVec1d   m_bayesunique;      // per-parameter bayesian acceptances

    // Timing information
    // Type "time_t" is a C library type which holds time information
    // (in seconds since 1970)
    time_t m_starttime;
    time_t m_endtime;

    // chain MLE's
    ForceParameters m_estimates;    // Maximum likelihood estimates
    double m_llikemle;              // posterior lnL at maximum
    double m_llikedata;             // data lnL of last generated tree in chain

  public:
    // we accept defaults for copy constructor, operator=, and destructor
    ChainOut();

    // friendship to allow direct access to member variables
    // for these two tightly coupled classes
    friend class ChainPack;

    // Inspector functions
    long            GetNumBadTrees()   const {return m_badtrees;};
    long            GetTinyPopTrees()  const {return m_tinypoptrees;};
    long            GetStretchedTrees() const {return m_stretchedtrees;};
    long            GetZeroDLTrees()   const {return m_zerodltrees;};
    double          GetAccrate()       const {return m_accrate;};
    ratemap         GetAllAccrates()   const {return m_rates;};
    long            GetNumtemps()      const {return m_numtemps;};
    DoubleVec1d     GetSwaprates()     const {return m_swaprates;};
    DoubleVec1d     GetTemperatures()  const {return m_temperatures;};
    DoubleVec1d     GetTempAccrates()  const {return m_tempaccrates;};
    double          GetLlikemle()      const {return m_llikemle;};
    double          GetLlikedata()     const {return m_llikedata;};
    ForceParameters GetEstimates()     const {return m_estimates;};

    time_t          GetStarttime()     const {return m_starttime;};
    time_t          GetEndtime()       const {return m_endtime;};
    vector<long>    GetBayesUnique()   const {return m_bayesunique;};

    // Mutator functions
    void SetNumBadTrees(const long &nbad)           {m_badtrees = nbad;};
    void SetNumTinyPopTrees(const long &ntiny)      {m_tinypoptrees = ntiny;};
    void SetNumStretchedTrees(const long &nstretch) {m_stretchedtrees = nstretch;};
    void SetNumZeroDLTrees(const long &nzero)       {m_zerodltrees = nzero;};
    void SetAccrate(const double &r)                {m_accrate = r;};
    void SetAllAccrates(const ratemap &r)           {m_rates = r;};
    void SetNumtemps(const long &ntemps)            {m_numtemps = ntemps;};
    void SetSwaprates(const DoubleVec1d &r)         {m_swaprates = r;};
    void SetTemperatures(const DoubleVec1d &temps)  {m_temperatures = temps;};
    void SetTempAccrates(const DoubleVec1d &r)      {m_tempaccrates = r;};
    void SetLlikemle(const double &src)             {m_llikemle = src;};
    void SetLlikedata(const double &src)            {m_llikedata = src;};
    void SetEstimates(const ForceParameters &src)   {m_estimates = src;};

    void SetStarttime(const time_t &src)            {m_starttime = src;};
    void SetEndtime(const time_t &src)              {m_endtime = src;};

    void SetRates(const ratemap &r)                 {m_rates = r;};
    void SetBayesUnique(const LongVec1d& b)         {m_bayesunique = b;};

    // The following two overloads set the time to the current time, gotten from the system clock.
    void SetStarttime();
    void SetEndtime();

};

#endif // CHAINOUT_H

//____________________________________________________________________________________
