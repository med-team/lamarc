// $Id: xmlsum_strings.h,v 1.22 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef XMLSUMSTRINGS_H
#define XMLSUMSTRINGS_H

#include <string>

//------------------------------------------------------------------------------------
// xml tags for sumfile
//
//
class xmlsum
{
  public:
    static const std::string COMMENT_START;
    static const std::string COMMENT_END;

    // chainsum related xml tags
    static const std::string SUMFILE_START;
    static const std::string SUMFILE_END;
    static const std::string CHAINSUM_START;
    static const std::string CHAINSUM_END;
    static const std::string REG_REP_START;
    static const std::string REG_REP_END;
    static const std::string END_REGION_START;
    static const std::string END_REGION_END;

    // chainpack related xml tags, includes all chainouts
    static const std::string CHAINPACK_START;
    static const std::string CHAINPACK_END;

    // chainout related xml tags, includes estimate tags
    static const std::string ALPHA_END;
    static const std::string ALPHA_START1;
    static const std::string ALPHA_START2;
    static const std::string ALPHA_START3;
    static const std::string ACCRATE_END;
    static const std::string ACCRATE_START;
    static const std::string BADTREES_END;
    static const std::string BADTREES_START;
    static const std::string BAYESUNIQUE_END;
    static const std::string BAYESUNIQUE_START;
    static const std::string CHAINOUT_END;
    static const std::string CHAINOUT_START;
    static const std::string ENDTIME_END;
    static const std::string ENDTIME_START;
    static const std::string LLIKEDATA_END;
    static const std::string LLIKEDATA_START;
    static const std::string LLIKEMLE_END;
    static const std::string LLIKEMLE_START;
    static const std::string MAP_END;
    static const std::string MAP_START;
    static const std::string NUMBER_END;
    static const std::string NUMBER_START;
    static const std::string RATES_END;
    static const std::string RATES_START;
    static const std::string STARTTIME_END;
    static const std::string STARTTIME_START;
    static const std::string STRETCHEDTREES_END;
    static const std::string STRETCHEDTREES_START;
    static const std::string SWAPRATES_END;
    static const std::string SWAPRATES_START;
    static const std::string TEMPERATURES_END;
    static const std::string TEMPERATURES_START;
    static const std::string TINYTREES_END;
    static const std::string TINYTREES_START;
    static const std::string ZERODLTREES_END;
    static const std::string ZERODLTREES_START;

    // estimate-specific xml tags
    static const std::string EPOCHTIMES_START;
    static const std::string EPOCHTIMES_END;
    static const std::string ESTIMATES_START;
    static const std::string ESTIMATES_END;
    static const std::string THETAS_START;
    static const std::string THETAS_END;
    static const std::string MIGRATES_START;
    static const std::string MIGRATES_END;
    static const std::string DIVMIGRATES_START;
    static const std::string DIVMIGRATES_END;
    static const std::string RECRATES_START;
    static const std::string RECRATES_END;
    static const std::string GROWTHRATES_START;
    static const std::string GROWTHRATES_END;
    static const std::string LOGISTICSELECTION_START;
    static const std::string LOGISTICSELECTION_END;
    static const std::string DISEASERATES_START;
    static const std::string DISEASERATES_END;
    static const std::string GAMMAOVERREGIONS_START;
    static const std::string GAMMAOVERREGIONS_END;

    // tree summary xml tags
    static const std::string TREESUM_START;
    static const std::string TREESUM_END;

    static const std::string NCOPY_START;
    static const std::string NCOPY_END;

    static const std::string SHORTFORCE_START;
    static const std::string SHORTFORCE_END;
    static const std::string INTERVALS_START;
    static const std::string INTERVALS_END;

    static const std::string SHORTPOINT_START;
    static const std::string SHORTPOINT_END;
    static const std::string SHORTWAIT_START;
    static const std::string SHORTWAIT_END;
    static const std::string SHORTPICK_START;
    static const std::string SHORTPICK_END;
    static const std::string SHORTPICK_FORCE_START;
    static const std::string SHORTPICK_FORCE_END;

    // tree summary full-interval xml tags
    static const std::string FORCE_START;
    static const std::string FORCE_END;
    static const std::string XPARTLINES_START;
    static const std::string XPARTLINES_END;
    static const std::string PARTLINES_START;
    static const std::string PARTLINES_END;
    static const std::string RECWEIGHT_START;
    static const std::string RECWEIGHT_END;
    static const std::string OLDSTATUS_START;
    static const std::string OLDSTATUS_END;
    static const std::string NEWSTATUS_START;
    static const std::string NEWSTATUS_END;
    static const std::string RECPOINT_START;
    static const std::string RECPOINT_END;
    static const std::string PARTNERPICKS_START;
    static const std::string PARTNERPICKS_END;

    //Parameter summary tags
    static const std::string PARAM_SUMMARY_START;
    static const std::string PARAM_SUMMARY_END;

    //Map summary tags
    static const std::string MAP_SUMMARY_START;
    static const std::string MAP_SUMMARY_END;
    static const std::string MAP_SUMMARIES_START;
    static const std::string MAP_SUMMARIES_END;

    // region and replicate summary xml tags
    static const std::string REGION_SUMMARY_START;
    static const std::string REGION_SUMMARY_END;
    static const std::string REPLICATE_SUMMARY_START;
    static const std::string REPLICATE_SUMMARY_END;
    static const std::string MAXLIKE_START;
    static const std::string MAXLIKE_END;
};

#endif // XMLSUMSTRINGS_H

//____________________________________________________________________________________
