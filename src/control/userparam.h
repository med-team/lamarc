// $Id: userparam.h,v 1.43 2018/01/03 21:32:54 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


/********************************************************************
 UserParameters is a collection class used for internal communication
 throughout Lamarc.

 UserParameters is a grab-bag of information provided by the user that
 doesn't seem to belong anywhere else, including file names and
 output format options, and the random number seed.

 Copy semantics are provided for menu roll-back purposes; otherwise
 there should only be one copy, registered in Registry.

 Written by Jim Sloan, revised by Mary Kuhner

********************************************************************/

#ifndef USERPARAMETERS_H
#define USERPARAMETERS_H

#include <string>
#include <vector>
#include <set>

#include "local_build.h"

#include "constants.h"


class Registry;

class UserParameters
{
  private:
    const std::string m_curvefileprefix;
    const std::string m_mapfileprefix;
    const std::string m_reclocfileprefix;
    const std::string m_tracefileprefix;
    const std::string m_newicktreefileprefix;
#ifdef LAMARC_QA_TREE_DUMP
    const std::string m_argfileprefix;
#endif // LAMARC_QA_TREE_DUMP
    std::string m_datafilename;
    std::string m_profileprefix;
    std::string m_resultsfilename;
    std::string m_treesuminfilename;
    std::string m_treesumoutfilename;
    std::string m_xmloutfilename;
    std::string m_xmlreportfilename;

    verbosity_type   m_verbosity;
    verbosity_type   m_progress;
    bool   m_plotPost;
    bool   m_usesystemclock;
    bool   m_readsumfile;   // if true read in tree summaries from a file
    bool   m_writesumfile;  // if true write tree summaries to file.
    // Note:  Both could be 'true'; reading
    //  and writing to a file.
    bool   m_writecurvefiles;
    bool   m_writereclocfiles; // if true write Recloc output to file.
    // following variable only applies in Bayesian runs
    bool   m_writetracefiles; // if true write Tracer output to file.
    bool   m_writenewicktreefiles;
#ifdef LAMARC_QA_TREE_DUMP
    bool   m_writeargfiles;
    bool   m_writemanyargs;
#endif // LAMARC_QA_TREE_DUMP
    long   m_randomSeed;

    time_t m_programstarttime;

    //LS DEBUG:  The following member variables are being stored here for now,
    // but should eventually move to an 'output manager' of some sort.
    std::vector<std::string> m_curvefilenames;
    std::vector<std::string> m_mapfilenames;
    std::vector<std::string> m_profilenames;
    std::set<std::string> m_reclocfilenames; // RSGNOTE: Never used.
    std::set<std::string> m_tracefilenames;
    std::set<std::string> m_newicktreefilenames;
#ifdef LAMARC_QA_TREE_DUMP
    std::set<std::string> m_argfilenames;
#endif // LAMARC_QA_TREE_DUMP
    std::string m_currentReclocFileName;
    std::string m_currentTraceFileName;
    std::string m_currentNewickTreeFileName;
#ifdef LAMARC_QA_TREE_DUMP
    std::string m_currentArgFileName;
#endif // LAMARC_QA_TREE_DUMP
    double m_currentBestLike;
    long m_currentStep;

    UserParameters();       // undefined

  protected:
    std::string BuildFileName(std::string prefix, std::string regname, long repCount, std::string extension) const;

  public:
    UserParameters(
        std::string curvefileprefix,
        std::string mapfileprefix,
        std::string reclocfileprefix,
        std::string tracefileprefix,
        std::string newicktreefileprefix,
#ifdef LAMARC_QA_TREE_DUMP
        std::string argfileprefix,
#endif // LAMARC_QA_TREE_DUMP
        std::string datafilename,
        std::string profileprefix,
        std::string resultsfilename,
        std::string treesuminfilename,
        std::string treesumoutfilename,
        std::string xmloutfilename,
        std::string xmlreportfilename,
        verbosity_type   verbosity,
        verbosity_type   progress,
        bool   plotPost,
        bool   usesystemclock,
        bool   readsumfile,
        bool   writesumfile,
        bool   writecurvefiles,
        bool   writereclocfile,         // RSGNOTE: Never used.
        bool   writetracefile,
        bool   writenewicktreefile,     // RSGNOTE: Never used.
#ifdef LAMARC_QA_TREE_DUMP
        bool   writeargfiles,
        bool   writemanyargs,
#endif // LAMARC_QA_TREE_DUMP
        long   randomSeed,
        time_t programstarttime
        );

    ~UserParameters() {};
    // accepting default copy constructor and assignment operator

    // Get Functions
    std::vector < std::string > ToXML(unsigned long nspaces) const;

    std::string GetCurveFilePrefix()     const { return m_curvefileprefix; };
    std::string GetMapFilePrefix()       const { return m_mapfileprefix; };
    std::string GetReclocFilePrefix()    const { return m_reclocfileprefix; };
    std::string GetTraceFilePrefix()     const { return m_tracefileprefix; };
    std::string GetNewickTreeFilePrefix()    const { return m_newicktreefileprefix;};
#ifdef LAMARC_QA_TREE_DUMP
    std::string GetArgFilePrefix()       const { return m_argfileprefix;};
#endif // LAMARC_QA_TREE_DUMP
    std::string GetDataFileName()        const { return m_datafilename; };
    std::string GetProfilePrefix()       const { return m_profileprefix; };
    std::string GetResultsFileName()     const { return m_resultsfilename; };
    std::string GetTreeSumInFileName()   const { return m_treesuminfilename; };
    std::string GetTreeSumOutFileName()  const { return m_treesumoutfilename; };
    std::string GetXMLOutFileName()      const { return m_xmloutfilename; };
    std::string GetXMLReportFileName()   const { return m_xmlreportfilename; };
    std::vector<std::string> GetCurveFileNames() const { return m_curvefilenames; };
    std::vector<std::string> GetMapFileNames() const { return m_mapfilenames; };
    std::vector<std::string> GetProfileNames() const { return m_profilenames; };
    std::set<std::string> GetReclocFileNames() const { return m_reclocfilenames; }; // RSGNOTE: Never used.
    std::set<std::string> GetTraceFileNames() const { return m_tracefilenames; };
    std::set<std::string> GetNewickTreeFileNames() const { return m_newicktreefilenames; };

    verbosity_type   GetVerbosity() const { return m_verbosity; };
    verbosity_type   GetProgress()  const { return m_progress; };
    bool   GetPlotPost()            const { return m_plotPost; };
    bool   GetReadSumFile()         const { return m_readsumfile; };
    bool   GetWriteSumFile()        const { return m_writesumfile; };
    bool   GetWriteCurveFiles()     const { return m_writecurvefiles; };
    bool   GetWriteReclocFiles()    const { return m_writereclocfiles; };
    bool   GetWriteTraceFiles()     const { return m_writetracefiles; };
    bool   GetWriteNewickTreeFiles()const { return m_writenewicktreefiles; };
#ifdef LAMARC_QA_TREE_DUMP
    bool   GetWriteArgFiles()       const { return m_writeargfiles; };
    bool   GetWriteManyArgs()       const { return m_writemanyargs; };
#endif // LAMARC_QA_TREE_DUMP
    bool   GetUseSystemClock()      const { return m_usesystemclock; };

    long   GetRandomSeed()          const { return m_randomSeed; };
    time_t GetProgramStartTime()    const { return m_programstarttime; };

    // These Set methods are public because lamarc may have to set them
    // to "false" if an error occurs. This makes them not quite "user"
    // parameters, but it is the best place for this logic.
    void   SetWriteSumFile  (const bool a)     { m_writesumfile = a; };
    void   SetReadSumFile   (const bool a)     { m_readsumfile  = a; };

    //LS DEBUG:  These are the functions that would need to move to an output
    // manager if we make one:
    void AddCurveFileName (const std::string name);
    void AddMapFileName   (const std::string name);
    void AddProfileName   (const std::string name);
    void AddReclocFileName(const std::string name);
    void AddTraceFileName (const std::string name);
    void AddNewickTreeFileName (const std::string name);
#ifdef LAMARC_QA_TREE_DUMP
    void AddArgFileName   (const std::string name);
#endif // LAMARC_QA_TREE_DUMP
    void UpdateFileNamesAndSteps(long region, long replicate, bool readsumfile);
    void UpdateWriteReclocFile(long region, long replicate); // RSGNOTE: Never used.
    void UpdateWriteTraceFile(long region, long replicate);
    string GetCurrentReclocFileName() {return m_currentReclocFileName;};
    string GetCurrentTraceFileName() {return m_currentTraceFileName;};
    string GetCurrentNewickTreeFileName() {return m_currentNewickTreeFileName;};
#ifdef LAMARC_QA_TREE_DUMP
    string GetCurrentArgFileName() {return m_currentArgFileName;};
#endif // LAMARC_QA_TREE_DUMP
    double GetCurrentBestLike() {return m_currentBestLike;};
    void SetCurrentBestLike(double like) {m_currentBestLike = like;};
    void ClearCurrentBestLike() {m_currentBestLike = -DBL_MAX;};
    long GetNextStep(long initialOrFinal);  // EWFIX.CHAINTYPE
};

#endif // USERPARAMETERS_H

//____________________________________________________________________________________
