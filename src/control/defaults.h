// $Id: defaults.h,v 1.63 2012/06/30 01:32:39 bobgian Exp $

#ifndef DEFAULTS_H
#define DEFAULTS_H

#include <string>
#include "local_build.h"
#include "constants.h"
#include "vectorx.h"

using std::string;

class defaults
{
    // Default values -- you can change these, but it is easier to
    // code your defaults into your input data file.

  public:

    // Whether or not the user expects 'site 0' in their output
    static const bool convert_output_to_eliminate_zeroes;

    // minimal mutation rate
    static const double minMuRate;

    // parameters
    static const double theta;
    static const double migration;
    static const double divMigration;
    static const double disease;
    static const double recombinationRate;
    static const double growth;
    static const double logisticSelection;
    static const double gammaOverRegions; // equals alpha, which equals the
    // scaled shape parameter of a gamma distribution over regions
    // of the background mutation rate
    static const double epochtime;

    // disease location
    static const long diseaseLocation;

    // growth approximation type
    static const growth_type growType;
    // selection approximation type
    static const selection_type selectionType;

    // maximum events
    static const long coalEvents;
    static const long migEvents;
    static const long divMigEvents;
    static const long diseaseEvents;
    static const long recEvents;
    static const long growEvents;
    static const long lselectEvents;
    static const long epochEvents;

    // methods
    static const method_type thetaMethod;
    static const method_type migMethod;
    static const method_type divMigMethod;
    static const method_type diseaseMethod;
    static const method_type recMethod;
    static const method_type growMethod;
    static const method_type lselectMethod;
    static const method_type divMethod;

    // Do a bayesian analysis
    static const bool   useBayesianAnalysis;

    // bayesian prior default boundaries
    static const double lowerboundTheta;
    static const double upperboundTheta;
    static const double lowerboundMig;
    static const double upperboundMig;
    static const double lowerboundDivMig;
    static const double upperboundDivMig;
    static const double lowerboundDisease;
    static const double upperboundDisease;
    static const double lowerboundRec;
    static const double upperboundRec;
    static const double lowerboundGrowth;
    static const double upperboundGrowth;
    static const double lowerboundLSelect;
    static const double upperboundLSelect;
    static const double lowerboundEpoch;
    static const double upperboundEpoch;

#ifdef LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
    // default relative sampling rate
    static const long   samplingRate;
#endif

    // bayesian prior default types
    static const priortype priortypeTheta;
    static const priortype priortypeMig;
    static const priortype priortypeDivMig;
    static const priortype priortypeDisease;
    static const priortype priortypeRec;
    static const priortype priortypeGrowth;
    static const priortype priortypeLSelect;
    static const priortype priortypeEpoch;

    // Min/Max allowable values for bayesian priors.  Used both in
    //  prior_interface.cpp and ui_vars_prior.cpp
    static const double minboundTheta;
    static const double maxboundTheta;
    static const double minboundMig;
    static const double maxboundMig;
    static const double minboundDivMig;
    static const double maxboundDivMig;
    static const double minboundDisease;
    static const double maxboundDisease;
    static const double minboundRec;
    static const double maxboundRec;
    static const double minboundGrowth;
    static const double maxboundGrowth;
    static const double minboundLSelect;
    static const double maxboundLSelect;
    static const double minboundEpoch;
    static const double maxboundEpoch;

    // maximum allowable values for initial parameter estimates
    static const double maxTheta;
    static const double minTheta;
    static const double maxMigRate;
    static const double minMigRate;
    static const double maxDivMigRate;
    static const double minDivMigRate;
    static const double maxDiseaseRate;
    static const double minDiseaseRate;
    static const double maxRecRate;
    static const double minRecRate;
    static const double maxGrowRate;
    static const double minGrowRate;
    static const double maxLSelectCoeff;
    static const double minLSelectCoeff;
    static const double minGammaOverRegions;
    static const double maxGammaOverRegions; // equals alpha, which equals
    // the shape parameter of a gamma distribution over regions
    // of the background mutation rate
    static const double minEpoch;
    static const double maxEpoch;

    // maximum allowable values during parameter estimation (maximization)
    static const double maximization_maxTheta;
    static const double maximization_minTheta;
    static const double maximization_maxMigRate;
    static const double maximization_minMigRate;
    static const double maximization_maxDivMigRate;
    static const double maximization_minDivMigRate;
    static const double maximization_maxDiseaseRate;
    static const double maximization_minDiseaseRate;
    static const double maximization_maxRecRate;
    static const double maximization_minRecRate;
    static const double maximization_maxGrowRate;
    static const double maximization_minGrowRate;
    static const double maximization_maxLSelectCoeff;
    static const double maximization_minLSelectCoeff;
    static const double maximization_minGammaOverRegions;
    static const double maximization_maxGammaOverRegions; // equals alpha,
    // which equals the shape parameter of a gamma distribution over regions
    // of the background mutation rate
    static const double maximization_minEpoch;
    static const double maximization_maxEpoch;

    // arrangers
    static const double dropArrangerTiming;
    static const double sizeArrangerTiming;
    static const double haplotypeArrangerTiming;
    static const double probhapArrangerTiming;
    static const double bayesianArrangerTiming;

    // temperature
    static const long temperatureInterval;
    static const bool useAdaptiveTemperatures;
    static const double minTemperature;
    static const double secondTemperature;

    // chains
    static const long initial;
    static const long initNChains;
    static const long initNSamples;
    static const long initInterval;
    static const long initDiscard;

    static const long final;
    static const long finalNChains;
    static const long finalNSamples;
    static const long finalInterval;
    static const long finalDiscard;
    // replicates
    static const long replicates;
    static const long geyeriters;

    //Max num heated chains.
    static const long maxNumHeatedChains;

    // error conditions
    static const long tooManyDenovo;  // we give up; we can't make a denovo tree

    // default user params for profiling
    static const bool doProfile;
    static const proftype profileType;

    // default group parameter status
    static const pstatus groupPstat;

    //Parameters for profiling estimation
    static const double highvalTheta;
    static const double lowvalTheta;
    static const double highmultTheta;
    static const double lowmultTheta;

    static const double highvalMig;
    static const double lowvalMig;
    static const double highmultMig;
    static const double lowmultMig;

    static const double highvalDisease;
    static const double lowvalDisease;
    static const double highmultDisease;
    static const double lowmultDisease;

    static const double highvalRec;
    static const double lowvalRec;
    static const double highmultRec;
    static const double lowmultRec;

    static const double highvalGrowth;
    static const double lowvalGrowth;
    static const double highmultGrowth;
    static const double lowmultGrowth;

    static const double highvalLSelect;
    static const double lowvalLSelect;
    static const double highmultLSelect;
    static const double lowmultLSelect;

    static const double highvalGammaOverRegions;
    static const double lowvalGammaOverRegions;
    static const double highmultGammaOverRegions;
    static const double lowmultGammaOverRegions; // equals alpha, which equals
    // the shape parameter of a gamma distribution over regions
    // of the background mutation rate

    static const double highvalEpoch;
    static const double lowvalEpoch;
    static const double highmultEpoch;
    static const double lowmultEpoch;

    // defaults for datamodels
    static const long nucleotideBins;
    // default number of bins for allelic and microsat stepwise models
    static const long bins;
    // default number of bins for brownian model
    static const long brownianBins;
    // default maximum seperation (functionally) allowed between any
    // two alleles in a microsat stepwise model
    static const long threshhold;
    static const long step_allowance;
    static const long mixedks_allowance;

    static const string startMethod;

    // default values for userparams
    static const string curvefileprefix;
    static const string mapfileprefix;
    static const string reclocfileprefix;
    static const string tracefileprefix;
    static const string newicktreefileprefix;
#ifdef LAMARC_QA_TREE_DUMP
    static const string argfileprefix;
#endif

    static const string datafilename;
    static const string profileprefix;
    static const string resultsfilename;
    static const string treesuminfilename;
    static const string treesumoutfilename;
    static const string xmloutfilename;
    static const string xmlreportfilename;

    static const verbosity_type verbosity;
    static const verbosity_type progress;

    static const time_t programstarttime;
    static const long randomseed;
    static const bool hasoldrandomseed;

    static const bool plotpost;
    static const bool readsumfile;
    static const bool useoldrandomseed;
    static const bool usesystemclock;
    static const bool writecurvefiles;
    static const bool writereclocfiles;
    static const bool writetracefiles;
    static const bool writenewicktreefiles;
#ifdef LAMARC_QA_TREE_DUMP
    static const bool writeargfiles;
    static const bool writemanyargs;
#endif // LAMARC_QA_TREE_DUMP
    static const bool writesumfile;

    // default values for regions
    static const double effpopsize;

    // default values for data models

    static const double       minLegalFrequency;

    static const ModelTypeVec1d allDataModels();
    static const model_type   dataModelType;
    static const bool         doNormalize;
    static const double       autoCorrelation;
    static const double       ttratio;
    static const bool         calcFreqsFromData;
    static const double       categoryProbability;
    static const DoubleVec1d  categoryProbabilities();
    static const double       categoryRate;
    static const double       categoryRateMultiple;
    static const DoubleVec1d  categoryRates();
    static const DoubleVec1d  chainTemperatures();
    static const double       baseFrequencyA;
    static const double       baseFrequencyC;
    static const double       baseFrequencyG;
    static const double       baseFrequencyT;
    static const double       gtrRateAC;
    static const double       gtrRateAG;
    static const double       gtrRateAT;
    static const double       gtrRateCG;
    static const double       gtrRateCT;
    static const double       gtrRateGT;
    static const double       relativeMuRate;
    static const double       KS_alpha;
    static const bool         optimize_KS_alpha;
    static const long         numCategories;

    static const long         maxNumCategories;

    static const double       per_base_error_rate;

    // default stick-joint length
    static const double       perThetaChange;

    // default maximum number of "semi-unique" concurrent branches in
    // all trees
    static const long         numSemiUniqueBranches;
#ifdef LAMARC_QA_SINGLE_DENOVOS
    static const long         numDenovos;
#endif // LAMARC_QA_SINGLE_DENOVOS
};

#endif // DEFAULTS_H

//____________________________________________________________________________________
