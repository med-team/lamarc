// $Id: maximizer_strings.cpp,v 1.8 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <string>
#include "maximizer_strings.h"

using std::string;

//------------------------------------------------------------------------------------
// xml tags for lamarc input file

const string maxstr::MAX_BAD_ALPHA_0 = "The data seem to be well fit by a mutation rate that is nearly constant among these unlinked regions. A perfectly uniform mutation rate corresponds to a gamma distribution whose shape parameter, alpha, is infinite.  It is being constrained to equal ";
const string maxstr::MAX_BAD_ALPHA_1 = ", and we're maximizing over regions once more.  If this also doesn't work, it might be worthwhile to re-run your analysis with the gamma distribution of mutation rates over regions turned off.";
const string maxstr::MAX_BAD_LNL_0  = "Warning!  Calculated a log-likelihood of ";
const string maxstr::MAX_BAD_LNL_1  = " for the parameter vector p = (";
const string maxstr::MAX_BAD_LNL_2A1 = "), but could not calculate a log-likelihood at a point ";
const string maxstr::MAX_BAD_LNL_2A2 = " units away from it.  This implies that your data may be difficult to model, or that there is a problem with lamarc.\n";
const string maxstr::MAX_BAD_LNL_2B = "), and determined that a greater log-likelihood could be found in a certain direction, but no greater log-likelihood was found in that direction.  This implies that your data may be difficult to model, or that there is a problem with lamarc.\n";
const string maxstr::MAX_CLIFF_EDGE_0 = "Warning!  Calculated a log-likelihood of ";
const string maxstr::MAX_CLIFF_EDGE_1 =  " for the parameter vector p = (";
const string maxstr::MAX_CLIFF_EDGE_2 =  "), and determined that a greater log-likelihood could be found in a certain direction, but no greater log-likelihood was found in that direction.  This implies that your data may be difficult to model, or that there is a problem with lamarc.";
const string maxstr::MAX_FAILED_BRACKET_0 = "Warning!  During one search for the maximum log-likelihood for parameter \"";
const string maxstr::MAX_FAILED_BRACKET_1 = "\", we failed to find a well-defined peak.  This might only be worrisome if you receive several such warning messages.  The search concluded at a value of ";
const string maxstr::MAX_FAILED_BRACKET_2 = " for this parameter.  The slope at this point is ";
const string maxstr::MAX_FAILED_BRACKET_3 = ".";
const string maxstr::MAX_HIGH_ALPHA_0 = "The alpha parameter got high enough that further maximization is pointless.";
const string maxstr::MAX_NO_CONVERGENCE_0 = "Warning:  Convergence to the maximum cannot be guaranteed.  ";
const string maxstr::MAX_NO_CONVERGENCE_1 = "Maximization terminated after ";
const string maxstr::MAX_NO_CONVERGENCE_2 = " iterations.  ";
const string maxstr::MAX_NO_CONVERGENCE_3 = "(|gradient| = ";
const string maxstr::MAX_NO_CONVERGENCE_4 = ")";
const string maxstr::MAX_NO_MULTI_MAX = "Unable to find a multi-region maximum-likelihood estimate; tried searching the surface starting from each single region\'s peak, and from the average of these peaks.  The multi-region MLE will be set to the average of the single-region MLE values.\n";
const string maxstr::MAX_NO_UPPER_BOUND_0 = "Warning!  Encountered a region of the log-likelihood surface in which the log-likelihood increases steadily, seemingly without an upper bound.  This implies that your data is difficult to model.  The problematic parameter is ";
const string maxstr::MAX_NO_UPPER_BOUND_1 = "; it has been increased or decreased to a value of ";
const string maxstr::MAX_NO_UPPER_BOUND_2 = ", and the maximum lnL, if one exists, seems to lie beyond this value.\n";
const string maxstr::MAX_UNDEFINED_BORDER_0 = "Warning!  While searching for the maximum log-likelihood for parameter \"";
const string maxstr::MAX_UNDEFINED_BORDER_1 = "\", we were able to calculate the log-likelihood at a particular point (";
const string maxstr::MAX_UNDEFINED_BORDER_2 = "), but we were unable to calculate the log-likelihood at any point beyond this point.  This should never happen.  This implies that at least one of the genealogies that LAMARC produced is infeasible.  This might imply that your data is difficult to model.  If you encounter this error during an early stage of your run (for example, during the second of ten initial Markov chains), and the results in the final stage of your run look consistent and reasonable, then it might be safe to ignore this message.  Alternatively, if you encounter this message while calculating profile likelihoods or estimates over multiple regions, and your results look reasonable and consistent, then it might be safe to ignore this message.  Otherwise, the final results reported by LAMARC should not be trusted, and we ask that you contact the LAMARC support team and share this result with them.";

//____________________________________________________________________________________
