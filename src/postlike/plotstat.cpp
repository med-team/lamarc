// $Id: plotstat.cpp,v 1.11 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#include <cassert>
#include <vector>
#include "plotstat.h"

//------------------------------------------------------------------------------------

ProfileLineStruct::ProfileLineStruct()
    : loglikelihood(0.0),
      percentile(0.0),
      profilevalue(0.0),
      profparam(),
      isExtremeHigh(false),
      isExtremeLow(false),
      maximizerWarning(false)
{
}

ProfileLineStruct::~ProfileLineStruct()
{
}

//------------------------------------------------------------------------------------

const ProfileLineStruct& ProfileStruct::GetProfileLine(double percentile) const
{
    vector<ProfileLineStruct>::const_iterator prof;
    for(prof = profilelines.begin(); prof != profilelines.end(); ++prof)
        if (percentile == prof->percentile)
            return *prof;

    assert(false);
    return(*prof);
}

//____________________________________________________________________________________
