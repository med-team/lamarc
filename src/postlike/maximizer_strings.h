// $Id: maximizer_strings.h,v 1.5 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef MAXSTR_H
#define MAXSTR_H

#include <string>
#include "stringx.h"

class maxstr
{
  public:
    static const string MAX_BAD_ALPHA_0;
    static const string MAX_BAD_ALPHA_1;
    static const string MAX_BAD_LNL_0;
    static const string MAX_BAD_LNL_1;
    static const string MAX_BAD_LNL_2A1;
    static const string MAX_BAD_LNL_2A2;
    static const string MAX_BAD_LNL_2B;
    static const string MAX_CLIFF_EDGE_0;
    static const string MAX_CLIFF_EDGE_1;
    static const string MAX_CLIFF_EDGE_2;
    static const string MAX_FAILED_BRACKET_0;
    static const string MAX_FAILED_BRACKET_1;
    static const string MAX_FAILED_BRACKET_2;
    static const string MAX_FAILED_BRACKET_3;
    static const string MAX_HIGH_ALPHA_0;
    static const string MAX_NO_CONVERGENCE_0;
    static const string MAX_NO_CONVERGENCE_1;
    static const string MAX_NO_CONVERGENCE_2;
    static const string MAX_NO_CONVERGENCE_3;
    static const string MAX_NO_CONVERGENCE_4;
    static const string MAX_NO_MULTI_MAX;
    static const string MAX_NO_UPPER_BOUND_0;
    static const string MAX_NO_UPPER_BOUND_1;
    static const string MAX_NO_UPPER_BOUND_2;
    static const string MAX_UNDEFINED_BORDER_0;
    static const string MAX_UNDEFINED_BORDER_1;
    static const string MAX_UNDEFINED_BORDER_2;
};

#endif // MAXSTR_H

//____________________________________________________________________________________
