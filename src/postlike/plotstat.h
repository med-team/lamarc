// $Id: plotstat.h,v 1.13 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


#ifndef PLOTSTAT_H
#define PLOTSTAT_H

#include <cmath>
#include <string>
#include <vector>

#include "vectorx.h"

class ForceSummary;
class Parameter;

enum plottype { log_ten, linear };
enum analysistype { mle, profile };

struct PlotStruct
{
  public:Parameter * xaxis;
    Parameter *yaxis;
    DoubleVec2d plane;
};

class ProfileLineStruct
{
  public:
    // class variables
    ProfileLineStruct();
    ~ProfileLineStruct();
    double loglikelihood;
    double percentile;
    double profilevalue;
    DoubleVec1d profparam;
    bool isExtremeHigh;
    bool isExtremeLow;
    bool maximizerWarning;
};

class ProfileStruct
{
  public:
    // class variables
    vector < ProfileLineStruct > profilelines;

    // class methods
    const ProfileLineStruct& GetProfileLine(double percentile) const;
};

class LikeGraphs
{
  public:
    LikeGraphs () {};
    ~LikeGraphs () {};

    string MakeBorder (long points, long breaks = 4);
    vector < string > MakeInnards (const DoubleVec2d & likes);
    vector < string > MakeLikePlot (const StringVec1d & innards,
                                    const Parameter & paramX,
                                    const Parameter & paramY, long breaks = 4);
    DoubleVec2d AddGraphs (const DoubleVec2d & a, const DoubleVec2d & b);

  private:
    bool Divides (long x, long y);
};

#endif // PLOTSTAT_H

//____________________________________________________________________________________
