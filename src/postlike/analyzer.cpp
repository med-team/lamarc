// $Id: analyzer.cpp,v 1.34 2018/01/03 21:33:02 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Peter Beerli, Mary Kuhner, Jon Yamato and Joseph Felsenstein */


// analyzer.cpp
//
// the analyzer generates
// - profiles  [the code is in profile.cpp]
// - likelihood surfaces
// - likelihood ratio tests
//
// Peter Beerli November 2000

#include <iostream>

#include "analyzer.h"
#include "forcesummary.h"
#include "mathx.h"                      // for probchi
#include "registry.h"
#include "runreport.h"
#include "stringx.h"

using namespace std;

//------------------------------------------------------------------------------------
// Instantiate the Analyzer
// using the "last" postlike object, the Analyzer assumes that the
// Prob(G|parameter_0) are already computed
// [in likelihood.h:....PostLike::Setup()

Analyzer::Analyzer (ForceSummary &fs, const ParamVector& params, Maximizer * thismaximizer):
    m_maximizer(thismaximizer),
    m_forcesummary (fs)
{
    m_newparams = CreateVec1d(params.size(), static_cast<double>(0.0));
}

//------------------------------------------------------------------------------------

Analyzer::~Analyzer ()
{
    // intentionally blank
}

void Analyzer::SetMLEs(DoubleVec1d newMLEs)
{
    m_MLEparams = newMLEs;
    m_MLElparams = m_MLEparams;
    LogVec0 (m_MLEparams, m_MLElparams);
}

//------------------------------------------------------------------------------------

void Analyzer::PrintMLEs()
{
    cout << "Current analyzer MLE's:" << endl;
    for (unsigned long int i = 0; i < m_MLEparams.size(); ++i)
        cout << m_MLEparams[i] << endl;
}

//____________________________________________________________________________________
