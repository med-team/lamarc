// $Id: gc_parse_locus.cpp,v 1.14 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_file.h"
#include "gc_parse.h"
#include "gc_parse_locus.h"
#include "gc_strings_parse_locus.h"

//------------------------------------------------------------------------------------

GCParseLocus::GCParseLocus( const GCParse *     parse,
                            size_t              indexInParse,
                            size_t              lineNumber,
                            size_t              numMarkers)
    :
    m_parse(parse),
    m_indexInParse(indexInParse),
    m_lineNumber(lineNumber),
    m_numMarkers(numMarkers)
{
}

GCParseLocus::~GCParseLocus()
{
}

const GCParse &
GCParseLocus::GetParse() const
{
    return *m_parse;
}

size_t
GCParseLocus::GetIndexInParse() const
{
    return m_indexInParse;
}

size_t
GCParseLocus::GetLineNumber() const
{
    return m_lineNumber;
}

size_t
GCParseLocus::GetNumMarkers() const
{
    return m_numMarkers;
}

gcGeneralDataType
GCParseLocus::GetDataType() const
{
    assert(m_parse != NULL);
    return m_parse->GetDataType();
}

#if 0
gcSpecificDataType
GCParseLocus::GetSpecificDataType() const
{
    assert(m_parse != NULL);
    return m_parse->GetDataTypeSpecFromFile();
}
#endif

wxString
GCParseLocus::GetName() const
{
    long segCount = 1 + (long)m_indexInParse;
    wxString fileName = m_parse->GetFileRef().GetName();
    return wxString::Format(gcstr_parselocus::nameShort,segCount,fileName.c_str());
}

wxString
GCParseLocus::GetLongName() const
{
    long segCount = 1 + (long)m_indexInParse;
    wxString fileName = m_parse->GetFileRef().GetName();
    return wxString::Format(gcstr_parselocus::nameLong,segCount,fileName.c_str());
}

//____________________________________________________________________________________
