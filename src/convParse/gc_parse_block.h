// $Id: gc_parse_block.h,v 1.11 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARSE_BLOCK_H
#define GC_PARSE_BLOCK_H

#include <vector>

#include "gc_quantum.h"
#include "wx/string.h"

class GCParse;
class GCParseLocus;
class GCParsePop;
class GCParser;
class GCParseSample;

class GCParseSamples : public std::vector<GCParseSample*>
{
  public:
    GCParseSamples();
    virtual ~GCParseSamples();
    void DebugDump(wxString prefix=wxEmptyString) const;
};

class GCParseBlock : public GCQuantum
{
    friend class GCParser;
  private:
    GCParse *         m_parse;
    size_t                  m_indexInParse;
    size_t                  m_expectedNumSequences;
    const GCParsePop *      m_popPointer;
    const GCParseLocus *    m_locusPointer;
    GCParseSamples          m_samples;

  protected:
    GCParseSample & FindSample(size_t indexInBlock);
    GCParse &       GetParse();

  public:
    GCParseBlock(   GCParse *               parseParent,
                    size_t                  indexInParse,
                    size_t                  expectedNumSequences,
                    const GCParsePop &      popRef,
                    const GCParseLocus &    locusRef);
    virtual ~GCParseBlock();

    const GCParseSample &   FindSample(size_t indexInBlock) const;

    size_t                  GetExpectedNumSequences()       const;
    size_t                  GetIndexInParse()               const;
    const GCParseLocus &    GetLocusRef()                   const;
    const GCParse &         GetParse()                      const;
    const GCParsePop &      GetPopRef()                     const;
    const GCParseSamples &  GetSamples()                    const;

    bool                    HasIncompleteSequences()        const;
    void                    DebugDump(wxString prefix=wxEmptyString)   const;

    void                    SetCannotBeMsat();
};

#endif  // GC_PARSE_BLOCK_H

//____________________________________________________________________________________
