// $Id: gc_population.h,v 1.16 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_POPULATION_H
#define GC_POPULATION_H

#include "gc_quantum.h"
#include "wx/string.h"

class GCStructures;

class gcPopulation : public GCQuantum
{
    friend class GCStructures;

  private:
    bool    m_blessed;
    size_t  m_parentID;      // used by divergence
    int     m_displayOrder;  // used by divergence
    int     m_index;         // used by lam_conv

    void    SetBlessed(bool blessed);

  public:
    gcPopulation();
    virtual ~gcPopulation();
    bool    GetBlessed() const ;

    int     GetDispIndex()          const;
    void    SetDispIndex(int index);

    void    SetParentId(size_t id);
    void    ClearParent();
    size_t  GetParentId() const;
    bool    HasParent() const;

    void    SetDispOrder(int order);
    int     GetDispOrder() const;
    bool    HasDispOrder() const;

    void    DebugDump(wxString prefix=wxEmptyString) const;
};

#endif  // GC_POPULATION_H

//____________________________________________________________________________________
