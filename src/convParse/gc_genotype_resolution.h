// $Id: gc_genotype_resolution.h,v 1.9 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_GENOTYPE_RESOLUTION_H
#define GC_GENOTYPE_RESOLUTION_H

#include <set>
#include <vector>
#include "wx/arrstr.h"
#include "wx/string.h"

class GCHaplotypeProbability
{
  private:
    double          m_penetrance;
    wxArrayString   m_alleleNames;

  public:
    GCHaplotypeProbability();       // shoult only be used by stl containers
    GCHaplotypeProbability(double penetrance, wxArrayString alleleNames);
    ~GCHaplotypeProbability();

    double      GetPenetrance() const;
    wxString    GetAllelesAsString() const;
    void    DebugDump(wxString prefix=wxEmptyString) const;
};

class GCGenotypeResolution
{
  private:
    wxString                               m_traitName;
    std::vector<GCHaplotypeProbability>    m_probabilities;

  public:
    GCGenotypeResolution();                 // should only be used by stl containers
    GCGenotypeResolution(wxString traitName);
    void foo() const;
    virtual ~GCGenotypeResolution();

    void AppendHap(double penetrance, wxArrayString alleleNames);
    wxString GetTraitName() const;
    const std::vector<GCHaplotypeProbability> & GetHapProbs() const;

    void    DebugDump(wxString prefix=wxEmptyString) const;
};

typedef std::set<const GCGenotypeResolution*>   gcTraitSet;

#endif  // GC_GENOTYPE_RESOLUTION_H

//____________________________________________________________________________________
