// $Id: gc_parser.h,v 1.15 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARSER_H
#define GC_PARSER_H

#include "gc_types.h"
#include "wx/string.h"

class wxFileInputStream;
class wxTextInputStream;
class GCParseBlock;
class GCParseSample;

class GCParser
{
  private:
    GCParser();         // undefined

  protected:
    const GCDataStore &                 m_dataStore;
    size_t                              m_linesRead;
    wxFileInputStream *                 m_fileStreamPointer;
    wxTextInputStream *                 m_textStreamPointer;

    void SetUpStreams(wxString fileName);
    bool HasContent(const wxString&) const;
    wxString ReadLine(bool skipBlankLines=true);

    void CheckNoExtraData();
    virtual bool CompleteParse(GCParse&) = 0;

    void FillData              (GCParse&, size_t popIndex, size_t locIndex, GCInterleaving, size_t expectedSequences);
    void FillDataInterleaved   (GCParseBlock &,  GCParseLocus &, size_t expectedSequences);
    void FillDataNonInterleaved(GCParseBlock &,  GCParseLocus &, size_t expectedSequences);

    GCParseBlock &  AddBlock(   GCParse & , const GCParsePop &, const GCParseLocus &, size_t expectedSequences);
    GCParseLocus &  AddLocus(   GCParse & , size_t expectedIndex, size_t locusLength);
    GCParsePop &    AddPop(     GCParse & , size_t expectedIndex, wxString comment);
    GCParse &       MakeParse(  GCFile &            fileRef,
                                GCFileFormat        format,
                                gcGeneralDataType   dataType,
                                GCInterleaving      interleaving,
                                wxString            delimiter=wxEmptyString);
    GCParseSample & MakeSample( GCParseBlock &      block,
                                size_t              indexInBlock,
                                size_t              lineInFile,
                                wxString            label);
    void AddDataToSample(GCParseSample &, GCParseLocus &, wxString data);
    void AddNucDataToSample(GCParseSample &, GCParseLocus &, wxString data);
    void AddAllelicDataToSample(GCParseSample &, GCParseLocus &, wxString data);
    wxArrayString makeKalleleTokens(wxString tokensTogether, wxString delim);

    void SetDataTypeFromFile(GCParse & parse, gcSpecificDataType type);

  public:
    GCParser(const GCDataStore&);
    virtual ~GCParser();
};

#endif  // GC_PARSER_H

//____________________________________________________________________________________
