// $Id: gc_region.h,v 1.16 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_REGION_H
#define GC_REGION_H

#include <map>
#include <set>

#include "gc_quantum.h"
#include "gc_types.h"
#include "wx/string.h"

class gcLocus;
class GCStructures;

class GCMapPosition
{
  private:
    bool        m_hasPosition;
    long        m_position;
  public:
    GCMapPosition();
    ~GCMapPosition();

    long        GetPosition() const;
    bool        HasPosition() const;
    void        SetPosition(long position);
    void        UnsetPosition();
    void        DebugDump(wxString prefix=wxEmptyString) const;
    wxString    AsString() const;
};

class GCLocusInfoMap : public std::map<size_t, GCMapPosition>
{
  public:
    GCLocusInfoMap() ;
    ~GCLocusInfoMap() ;
    wxString AsString() const;
};

class GCTraitInfoSet : public std::set<size_t>
{
  public:
    GCTraitInfoSet() ;
    ~GCTraitInfoSet() ;
    wxString AsString() const;
};

class gcRegion : public GCQuantum
{
    friend class GCStructures;

  private:
    wxString                            m_name;
    bool                                m_blessed;
    bool                                m_hasEffectivePopulationSize;
    double                              m_effectivePopulationSize;

    GCLocusInfoMap                      m_loci;
    GCTraitInfoSet                      m_traits;

    void        AddLocus(gcLocus &);
    void        AddLocus(gcLocus &, long mapPosition);
    void        AddTraitId(size_t traitClassId);
    void        RemoveLocusId(size_t locusId);
    void        RemoveTraitId(size_t traitClassId);
    void        SetName(wxString newName);
    void        SetBlessed(bool blessed);

  protected:

    const GCLocusInfoMap &  GetLocusInfoMap() const ;

  public:
    gcRegion();
    ~gcRegion();

    bool        GetBlessed()                const ;
    wxString    GetName()                   const ;
    double      GetEffectivePopulationSize()const ;
    bool        HasEffectivePopulationSize()const ;
    size_t      GetLocusCount()             const ;

    const GCTraitInfoSet &  GetTraitInfoSet() const ;

    void    DebugDump(wxString=wxEmptyString) const;

    void    SetEffectivePopulationSize(double effectivePopulationSize);
    bool    CanMergeWith(const gcRegion&) const;
};

#endif  // GC_REGION_H

//____________________________________________________________________________________
