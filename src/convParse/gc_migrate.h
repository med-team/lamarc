// $Id: gc_migrate.h,v 1.18 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_MIGRATE_H
#define GC_MIGRATE_H

#include "gc_parser.h"
#include "gc_types.h"

class GCFile;

class GCMigrateParser : public GCParser
{
  private:
    GCMigrateParser();  // undefined

  protected:
    void    ParseMigrateFirstLine(
        gcSpecificDataType& dataTypeSpecInFile,
        size_t &            numPops,
        size_t &            numLoci,
        wxString &          populationName);
    void    ParseMigrateFirstLine(
        gcSpecificDataType& dataTypeSpecInFile,
        size_t &            numPops,
        size_t &            numLoci,
        wxString &          delimiter,
        wxString &          populationName);

    std::vector<size_t> ParseMigratePopulationInfo(wxString & popName, size_t numLoci);
    std::vector<size_t> ParseMigrateLocusLengths();

    GCParse * NucParse(     GCFile &        fileRef,
                            gcGeneralDataType   dataType,
                            GCInterleaving  interleaving);
    GCParse * AlleleParse(  GCFile &            fileRef,
                            gcGeneralDataType   dataType,
                            GCInterleaving      interleaving);

    bool            IsLegalDelimiter(wxString delimCandidate);
    bool            CompleteParse(GCParse&);

  public:
    GCMigrateParser(const GCDataStore& dataStore);
    virtual ~GCMigrateParser();

    GCParse * Parse(GCFile &            fileRef,
                    gcGeneralDataType   dataType,
                    GCInterleaving      interleaving);
};

#endif  // GC_MIGRATE_H

//____________________________________________________________________________________
