// $Id: gc_locus.h,v 1.22 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOCUS_H
#define GC_LOCUS_H

#include "wx/string.h"
#include "gc_creation_info.h"
#include "gc_phase.h"
#include "gc_quantum.h"
#include "gc_types.h"

class GCStructures;

class gcLocus : public GCQuantum
{
    friend class GCStructures;

  private:

    bool                        m_blessed;

    bool                        m_hasRegion;
    size_t                      m_regionId;

    bool                        m_inducedByFile;
    size_t                      m_fileId;

    gcSpecificDataType          m_dataType;

    bool                        m_hasNumMarkers;
    size_t                      m_numMarkers;

    bool                        m_hasTotalLength;
    size_t                      m_totalLength;

    bool                        m_hasLinkedUserValue;
    bool                        m_linkedUserValue;

    bool                        m_hasOffset;
    long                        m_offset;

    bool                        m_hasMapPosition;
    long                        m_mapPosition;

    bool                        m_hasUnphasedMarkers;
    gcUnphasedMarkers           m_unphasedMarkers;

    gcCreationInfo              m_creationInfo;

    std::vector<long>           m_locations;

    void    SetBlessed(bool blessed);
    void    SetCreationInfo(const gcCreationInfo &);
    void    SetFileId(size_t id);
    void    UnsetFileId();
    void    SetRegionId(size_t id);
    void    UnsetRegionId();

  protected:
    void    LocusMergeLogic(bool doSettings, gcLocus & locus);

  public:
    gcLocus();
    ~gcLocus();

    bool                GetBlessed()            const;
    gcSpecificDataType  GetDataType()           const;
    wxString            GetDataTypeString()     const;
    size_t              GetLength()             const;
    wxString            GetLengthString()       const;
    bool                GetLinked()             const;
    wxString            GetLinkedString()       const;
    bool                GetLinkedUserValue()    const;
    wxString            GetLinkedUserValueString()const;
    std::vector<long>   GetLocations()          const;
    wxString            GetLocationsAsString()  const;
    wxString            GetLongName()           const;
    long                GetMapPosition()        const;
    wxString            GetMapPositionString()  const;
    size_t              GetNumMarkers()         const;
    wxString            GetNumMarkersString()   const;
    long                GetOffset()             const;
    wxString            GetOffsetString()       const;
    size_t              GetRegionId()           const;
    size_t              GetTotalLength()        const;
    wxString            GetTotalLengthString()  const;
    const gcUnphasedMarkers * GetUnphasedMarkers()    const;
    wxString            GetUnphasedMarkersAsString()    const;

    bool        HasLength()         const ;
    bool        HasLocations()      const ;
    bool        HasLocationZero()   const ;
    bool        HasLinkedUserValue()const ;
    bool        HasMapPosition()    const ;
    bool        HasNumMarkers()     const ;
    bool        HasOffset()         const ;
    bool        HasRegion()         const ;
    bool        HasTotalLength()    const ;
    bool        HasUnphasedMarkers()const ;

    void    SetDataType(gcSpecificDataType type);
    void    SetLinkedUserValue(bool linked);
    void    SetLocations(wxString locationString);
    void    SetMapPosition(long position);
    void    SetNumMarkers(size_t numMarkers);
    void    SetOffset(long offset);
    void    SetTotalLength(size_t length);
    void    SetUnphasedMarkers(gcUnphasedMarkers);

    void    UnsetNumMarkers();
    void    UnsetMapPosition();
    void    UnsetTotalLength();

    void    DebugDump(wxString prefix=wxEmptyString) const;

    bool    CanMergeWith(gcLocus & locus) ;
    void       MergeWith(gcLocus & locus) ;
};

#endif  // GC_LOCUS_H

//____________________________________________________________________________________
