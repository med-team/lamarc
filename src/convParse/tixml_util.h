// $Id: tixml_util.h,v 1.10 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef TIXML_UTIL_H
#define TIXML_UTIL_H

#include <string>
#include <vector>

#include "errhandling.h"
#include "wx/string.h"

class TiXmlElement;

TiXmlElement *          tiwx_optionalChild(TiXmlElement* ancestor, wxString nodeName);
TiXmlElement *          tiwx_requiredChild(TiXmlElement* ancestor, wxString nodeName);
wxString                tiwx_nodeText(TiXmlElement *);
wxString                tiwx_attributeValue(TiXmlElement*,wxString attributeName);

double                  tiwx_double_from_text(TiXmlElement *);
long                    tiwx_long_from_text(TiXmlElement *) throw (incorrect_xml);
size_t                  tiwx_size_t_from_text(TiXmlElement *) throw (incorrect_xml);

std::vector<TiXmlElement *>  tiwx_optionalChildren(TiXmlElement* ancestor, wxString nodeName);
std::vector<TiXmlElement *>  tiwx_requiredChildren(TiXmlElement* ancestor, wxString nodeName);

#endif  // TIXML_UTIL_H

//____________________________________________________________________________________
