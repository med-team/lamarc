// $Id: tixml_util.cpp,v 1.11 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "errhandling.h"
#include "tinyxml.h"
#include "tixml_base.h"
#include "tixml_util.h"
#include "cnv_strings.h"

//------------------------------------------------------------------------------------

#if 0
TiXmlElement *
tiwx_singleElement(TiXmlElement* ancestor, wxString nodeName,bool required)
{
    TiXmlElement * elem = NULL;
    try
    {
        elem = ti_singleElement(ancestor,nodeName,required);
    }
    catch(incorrect_xml_missing_tag f)
    {
        assert(required);
        wxString msg = wxString::Format(cnvstr::ERR_MISSING_TAG,nodeName.c_str());
        incorrect_xml e(msg.c_str());
        throw e;
        return elem;
    }
    catch(incorrect_xml_extra_tag g)
    {
        wxString msg = wxString::Format(cnvstr::ERR_EXTRA_TAG,nodeName.c_str());
        incorrect_xml e(msg.c_str());
        throw e;
        return NULL;
    }
}
#endif

TiXmlElement *
tiwx_optionalChild(TiXmlElement* ancestor, wxString nodeName)
{
    try
    {
        //TiXmlElement * elem = ti_optionalChild(ancestor,nodeName.c_str());
        TiXmlElement * elem = ti_optionalChild(ancestor,(const char *)nodeName.mb_str());// JRM hack
        return elem;
    }
    catch(incorrect_xml_extra_tag g)
    {
        //wxString msg = wxString::Format(cnvstr::ERR_EXTRA_TAG,nodeName.c_str());
        //incorrect_xml e(msg.c_str());
        wxString msg = wxString::Format(cnvstr::ERR_EXTRA_TAG,(const char *)nodeName.mb_str());// JRM hack
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
        return NULL;
    }
}

TiXmlElement *
tiwx_requiredChild(TiXmlElement* ancestor, wxString nodeName)
{
    try
    {
        //TiXmlElement * elem = ti_requiredChild(ancestor,nodeName.c_str());
        TiXmlElement * elem = ti_requiredChild(ancestor,(const char *)nodeName.mb_str());// JRM hack
        return elem;
    }
    catch(incorrect_xml_missing_tag f)
    {
        //wxString msg = wxString::Format(cnvstr::ERR_MISSING_TAG,nodeName.c_str());
        //incorrect_xml e(msg.c_str());
        wxString msg = wxString::Format(cnvstr::ERR_MISSING_TAG,(const char *)nodeName.mb_str());// JRM hack
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
        return NULL;
    }
    catch(incorrect_xml_extra_tag g)
    {
        //wxString msg = wxString::Format(cnvstr::ERR_EXTRA_TAG,nodeName.c_str());
        //incorrect_xml e(msg.c_str());
        wxString msg = wxString::Format(cnvstr::ERR_EXTRA_TAG,(const char *)nodeName.mb_str());// JRM hack
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
        return NULL;
    }
}

wxString
tiwx_nodeText(TiXmlElement * node)
{
    return wxString(ti_nodeText(node).c_str());
}

wxString
tiwx_attributeValue(TiXmlElement * node, wxString attrName)
{
    //return wxString(ti_attributeValue(node,attrName.c_str()).c_str());
    return wxString(ti_attributeValue(node,(const char *)attrName.mb_str()).c_str());// JRM hack
}

double
tiwx_double_from_text(TiXmlElement * node)
{
    double value;
    try
    {
        value = ti_double_from_text(node);
    }
    catch (incorrect_xml_not_double f)
    {
        wxString msg = wxString::Format(cnvstr::ERR_NOT_DOUBLE,f.text().c_str());
        //incorrect_xml e(msg.c_str());
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
    }
    return value;
}

long
tiwx_long_from_text(TiXmlElement * node) throw (incorrect_xml)
{
    long value;
    try
    {
        value = ti_long_from_text(node);
    }
    catch (incorrect_xml_not_long f)
    {
        wxString msg = wxString::Format(cnvstr::ERR_NOT_LONG,f.text().c_str());
        //incorrect_xml e(msg.c_str());
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
    }
    return value;
}

size_t
tiwx_size_t_from_text(TiXmlElement * node) throw (incorrect_xml)
{
    size_t value;
    try
    {
        value = ti_size_t_from_text(node);
    }
    catch (incorrect_xml_not_size_t f)
    {
        wxString msg = wxString::Format(cnvstr::ERR_NOT_SIZE_T,f.text().c_str());
        //incorrect_xml e(msg.c_str());
        incorrect_xml e((const char *)msg.mb_str());// JRM hack
        throw e;
    }
    return value;
}

std::vector<TiXmlElement *>
tiwx_optionalChildren(TiXmlElement* ancestor, wxString nodeName)
{
    //return ti_optionalChildren(ancestor,nodeName.c_str());
    return ti_optionalChildren(ancestor,(const char *)nodeName.mb_str());// JRM hack
}

std::vector<TiXmlElement *>
tiwx_requiredChildren(TiXmlElement* ancestor, wxString nodeName)
{
    //return ti_requiredChildren(ancestor,nodeName.c_str());
    return ti_requiredChildren(ancestor,(const char *)nodeName.mb_str());// JRM hack
}

//____________________________________________________________________________________
