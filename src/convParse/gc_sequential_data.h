// $Id: gc_sequential_data.h,v 1.17 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_SEQUENTIAL_DATA_H
#define GC_SEQUENTIAL_DATA_H

#include "gc_types.h"
#include "wx/arrstr.h"
#include "wx/string.h"

class GCParseBlock;
class GCParser;

class GCSequentialData
{
  private:
    GCParseBlock &          m_block;

  protected:
    GCSequentialData();     // undefined
    GCSequentialData(GCParseBlock &);
    GCParseBlock &    GetParseBlock();

  public:
    virtual ~GCSequentialData();

    virtual void        AddMarker(wxString data) = 0;
    void        CheckMarkerCount();
    virtual size_t      GetNumMarkers()  const = 0;
    virtual wxString    GetData() const = 0;
    virtual wxString    GetData(size_t siteIndex) const = 0;

    void    DebugDump(wxString prefix=wxEmptyString) const;
    const GCParseBlock &    GetParseBlock() const;
};

class GCAllelicData : public GCSequentialData
{
  private:
    wxArrayString           m_data;

  public:
    GCAllelicData();        // undefined
    GCAllelicData(GCParseBlock&);
    virtual ~GCAllelicData();

    void        AddMarker(wxString data);
    size_t      GetNumMarkers() const;
    wxString    GetData() const;
    wxString    GetData(size_t siteIndex) const;
};

class GCNucData : public GCSequentialData
{
  private:
    wxString                m_data;

  public:
    GCNucData();            // undefined
    GCNucData(GCParseBlock&);
    virtual ~GCNucData();

    void        AddMarker(wxString data);
    size_t      GetNumMarkers() const;
    wxString    GetData() const;
    wxString    GetData(size_t siteIndex) const;
};

#endif  // GC_SEQUENTIAL_DATA_H

//____________________________________________________________________________________
