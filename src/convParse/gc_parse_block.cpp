// $Id: gc_parse_block.cpp,v 1.9 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_parse.h"
#include "gc_parse_block.h"
#include "gc_parse_locus.h"
#include "gc_parse_pop.h"
#include "gc_parse_sample.h"
#include "gc_strings.h"
#include "wx/log.h"

//------------------------------------------------------------------------------------

GCParseSamples::GCParseSamples()
{
}

GCParseSamples::~GCParseSamples()
{
}

void
GCParseSamples::DebugDump(wxString prefix) const
{
    for(GCParseSamples::const_iterator i=begin(); i != end(); i++)
    {
        (**i).DebugDump(prefix);
    }
}

#if 0
GCParseBlock::GCParseBlock()
    :
    m_parse(NULL),
    m_indexInParse(gcdefault::badIndex),
    m_popPointer(NULL),
    m_locusPointer(NULL)
{
}
#endif

GCParseBlock::GCParseBlock( GCParse *               parseParent,
                            size_t                  indexInParse,
                            size_t                  expectedNumSequences,
                            const GCParsePop &      popRef,
                            const GCParseLocus &    locusRef)
    :
    m_parse(parseParent),
    m_indexInParse(indexInParse),
    m_expectedNumSequences(expectedNumSequences),
    m_popPointer(&popRef),
    m_locusPointer(&locusRef)
{
}

GCParseBlock::~GCParseBlock()
{
    for(GCParseSamples::iterator i = m_samples.begin(); i != m_samples.end(); i++)
    {
        delete *i;
    }
}

size_t
GCParseBlock::GetExpectedNumSequences() const
{
    return m_expectedNumSequences;
}

size_t
GCParseBlock::GetIndexInParse() const
{
    return m_indexInParse;
}

GCParse &
GCParseBlock::GetParse()
{
    return *m_parse;
}

const GCParse &
GCParseBlock::GetParse() const
{
    return *m_parse;
}

const GCParseLocus &
GCParseBlock::GetLocusRef() const
{
    return *m_locusPointer;
}

const GCParsePop &
GCParseBlock::GetPopRef() const
{
    return *m_popPointer;
}

GCParseSample &
GCParseBlock::FindSample(size_t indexInBlock)
{
    assert(indexInBlock < m_samples.size());
    return *(m_samples[indexInBlock]);
}

const GCParseSample &
GCParseBlock::FindSample(size_t indexInBlock) const
{
    assert(indexInBlock < m_samples.size());
    return *(m_samples[indexInBlock]);
}

const GCParseSamples &
GCParseBlock::GetSamples() const
{
    return m_samples;
}

bool
GCParseBlock::HasIncompleteSequences() const
{
    for(size_t i=0; i < m_samples.size(); i++)
    {
        const GCParseSample & parseSample = *(m_samples[i]);
        if(parseSample.GetLength() < m_locusPointer->GetNumMarkers())
        {
            return true;
        }
    }
    return false;
}

void
GCParseBlock::DebugDump(wxString prefix) const
{
    wxLogDebug("%sid %5d; pop %5d; locus %5d",  // EWDUMPOK
               prefix.c_str(),
               (int)(GetIndexInParse()),
               (int)(GetPopRef().GetIndexInParse()),
               (int)(GetLocusRef().GetIndexInParse()));
    for(size_t i=0; i < m_samples.size(); i++)
    {
        const GCParseSample & sampleRef = *(m_samples[i]);
        sampleRef.DebugDump(prefix+gcstr::indent);
    }
}

void
GCParseBlock::SetCannotBeMsat()
{
    assert(m_parse != NULL);
    m_parse->SetCannotBeMsat();
}

//____________________________________________________________________________________
