// $Id: gc_population.cpp,v 1.14 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_population.h"
#include "gc_default.h"
#include "gc_strings.h"
#include "gc_strings_pop.h"
#include "gc_errhandling.h"
#include "wx/log.h"

gcPopulation::gcPopulation()
    :
    m_blessed(false),
    m_parentID(gcdefault::badIndex),
    m_displayOrder(0),
    m_index(0)
{
    SetName(wxString::Format(gcstr_pop::internalName,(long)GetId()));
}

gcPopulation::~gcPopulation()
{
}

bool
gcPopulation::GetBlessed() const
{
    return m_blessed;
}

void
gcPopulation::SetBlessed(bool blessed)
{
    m_blessed = blessed;
}

int
gcPopulation::GetDispIndex() const
{
    return m_index;
}

void
gcPopulation::SetDispIndex(int index)
{
    m_index = index;
}

void
gcPopulation::SetParentId(size_t id)
{
    m_parentID = id;
}

size_t
gcPopulation::GetParentId() const
{
    if(!HasParent())
    {
        wxString msg = wxString::Format(gcerr::unsetParentId,GetName().c_str());
        throw gc_implementation_error(msg.c_str());
    }
    return(m_parentID);
}

bool
gcPopulation::HasParent() const
{
    if (m_parentID == gcdefault::badIndex)
    {
        return false;
    }
    return true;
}

void
gcPopulation::ClearParent()
{
    m_parentID = gcdefault::badIndex;
}

void
gcPopulation::SetDispOrder(int order)
{
    m_displayOrder = order;
}

int
gcPopulation::GetDispOrder() const
{
    return m_displayOrder;
}

bool
gcPopulation::HasDispOrder() const
{
    if (m_displayOrder > 0)
    {
        return true;
    }
    return false;
}

void
gcPopulation::DebugDump(wxString prefix) const
{
    wxLogDebug("%spopulation %s (pop id %ld)",  // EWDUMPOK
               prefix.c_str(),
               GetName().c_str(),
               (long)GetId());
}

//____________________________________________________________________________________
