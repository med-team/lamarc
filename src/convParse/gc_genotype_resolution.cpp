// $Id: gc_genotype_resolution.cpp,v 1.10 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include "gc_genotype_resolution.h"
#include "gc_strings.h"
#include "gc_trait_err.h"
#include "wx/log.h"

GCHaplotypeProbability::GCHaplotypeProbability()
    :
    m_penetrance(0) // EWFIX.P3
{
}

GCHaplotypeProbability::GCHaplotypeProbability(double penetrance, wxArrayString alleleNames)
    :
    m_penetrance(penetrance)
{
    if(penetrance < 0)
    {
        throw gc_haplotype_probability_negative(penetrance);
    }
    // EWFIX.P4 -- we take all allele names, even if they
    // are misspellings. Perhaps we should require a definition ?
    m_alleleNames = alleleNames;
}

GCHaplotypeProbability::~GCHaplotypeProbability()
{
}

double
GCHaplotypeProbability::GetPenetrance() const
{
    return m_penetrance;
}

wxString
GCHaplotypeProbability::GetAllelesAsString() const
{
    wxString asString = " ";
    for(size_t i=0; i < m_alleleNames.GetCount(); i++)
    {
        asString += m_alleleNames[i];
        asString += " ";
    }

    return asString;
}

void
GCHaplotypeProbability::DebugDump(wxString prefix) const
{
    wxLogDebug("%sprobability %f for alleles %s",prefix.c_str(),GetPenetrance(),GetAllelesAsString().c_str());
}

//------------------------------------------------------------------------------------

GCGenotypeResolution::GCGenotypeResolution()
    :
    m_traitName(wxEmptyString)
{
}

GCGenotypeResolution::GCGenotypeResolution(wxString traitName)
    :
    m_traitName(traitName)
{
}

void
GCGenotypeResolution::foo() const
{
    wxLogDebug("EWFIX -- deleting %p",this);
}

GCGenotypeResolution::~GCGenotypeResolution()
{
    foo();
}

void
GCGenotypeResolution::AppendHap(double penetrance, wxArrayString alleles)
{
    m_probabilities.push_back(GCHaplotypeProbability(penetrance,alleles));
}

wxString
GCGenotypeResolution::GetTraitName() const
{
    return m_traitName;
}

const std::vector<GCHaplotypeProbability> &
GCGenotypeResolution::GetHapProbs() const
{
    return m_probabilities;
}

void
GCGenotypeResolution::DebugDump(wxString prefix) const
{
    wxLogDebug("%s:resolution for trait %s",prefix.c_str(),GetTraitName().c_str());
    for(size_t i=0; i < m_probabilities.size(); i++)
    {
        m_probabilities[i].DebugDump(prefix+gcstr::indent);
    }
}

//____________________________________________________________________________________
