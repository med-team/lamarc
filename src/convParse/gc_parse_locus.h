// $Id: gc_parse_locus.h,v 1.14 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARSE_LOCUS_H
#define GC_PARSE_LOCUS_H

#include "gc_types.h"
#include "wx/string.h"

class GCParse;

class GCParseLocus
{
  private:
    GCParseLocus();     // undefined

    const GCParse *     m_parse;
    size_t              m_indexInParse;
    size_t              m_lineNumber;
    size_t              m_numMarkers;

  public:
    GCParseLocus(   const GCParse *     parse,
                    size_t              indexInParse,
                    size_t              lineNumber,
                    size_t              numMarkers);
    ~GCParseLocus();

    const GCParse &     GetParse()              const ;
    size_t              GetIndexInParse()       const ;
    size_t              GetLineNumber()         const ;
    size_t              GetNumMarkers()         const ;
    gcGeneralDataType   GetDataType()           const ;

#if 0
    gcSpecificDataType  GetSpecificDataType()   const ;
#endif

    wxString            GetName()               const ;
    wxString            GetLongName()           const ;
};

#endif  // GC_PARSE_LOCUS_H

//____________________________________________________________________________________
