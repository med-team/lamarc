// $Id: gc_parse_sample.h,v 1.10 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARSE_SAMPLE_H
#define GC_PARSE_SAMPLE_H

#include <vector>

#include "gc_types.h"
#include "wx/string.h"

class GCParseBlock;
class GCParser;
class GCSequentialData;

class GCParseSample
{
    friend class GCParser;
  private:
    GCParseSample();    // undefined

  protected:
    GCParseBlock * const            m_block;
    size_t                          m_lineInFile;
    wxString                        m_label;
    std::vector<GCSequentialData*>  m_data;

    bool allLengthsEqual()  const;

  public:
    GCParseSample(GCParseBlock &, size_t lineInFile, wxString label);
    virtual ~GCParseSample();

    GCParseBlock &              GetBlock();
    const GCParseBlock &        GetBlock()                      const;
    const GCSequentialData &    GetData(size_t index)           const;
    wxString                    GetLabel()                      const;
    size_t                      GetLine()                       const;
    size_t                      GetLength()                     const;
    size_t                      GetSequencesPerLabel()          const;

    void                        DebugDump(wxString prefix=wxEmptyString)   const;
};

#endif  // GC_PARSE_SAMPLE_H

//____________________________________________________________________________________
