// $Id: gc_loci_match.h,v 1.12 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_LOCI_MATCH_H
#define GC_LOCI_MATCH_H

#include "gc_types.h"
#include "wx/arrstr.h"

class GCParse;

class GCLocusSpec
{
  private:
    bool            m_blessedLocus;
    bool            m_blessedRegion;
    wxString        m_locusName;
    wxString        m_regionName;
    GCLocusSpec();    // undefined

  public:
    GCLocusSpec(bool blessedLocus, bool blessedRegion, wxString locusName, wxString regionName);
    virtual ~GCLocusSpec();

    bool        GetBlessedLocus()   const ;
    bool        GetBlessedRegion()  const ;
    wxString    GetLocusName()      const ;
    wxString    GetRegionName()     const ;
};

class GCLocusMatcher
{
  protected:
    loc_match       m_locMatchType;
    wxArrayString   m_locNames;

  public:
    GCLocusMatcher();
    GCLocusMatcher(loc_match locMatchType);
    GCLocusMatcher(loc_match locMatchType, wxString name);
    GCLocusMatcher(loc_match locMatchType, wxArrayString names);
    ~GCLocusMatcher();
    GCLocusSpec GetLocSpec(size_t index, const GCParse & parse) const ;
    bool        HandlesThisManyLoci(size_t count) const ;
    loc_match   GetLocMatchType() const ;

    const wxArrayString &   GetLociNames()  const;
};

#endif  // GC_LOCI_MATCH_H

//____________________________________________________________________________________
