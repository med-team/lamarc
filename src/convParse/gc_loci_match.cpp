// $Id: gc_loci_match.cpp,v 1.17 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_datastore.h"
#include "gc_default.h"
#include "gc_errhandling.h"
#include "gc_loci_match.h"
#include "gc_parse.h"
#include "gc_strings.h"
#include "wx/filename.h"

//------------------------------------------------------------------------------------

GCLocusSpec::GCLocusSpec(bool blessedLocus, bool blessedRegion, wxString locusName, wxString regionName)
    :
    m_blessedLocus(blessedLocus),
    m_blessedRegion(blessedRegion),
    m_locusName(locusName),
    m_regionName(regionName)
{
}

GCLocusSpec::~GCLocusSpec()
{
}

bool
GCLocusSpec::GetBlessedLocus() const
{
    return m_blessedLocus;
}

bool
GCLocusSpec::GetBlessedRegion() const
{
    return m_blessedRegion;
}

wxString
GCLocusSpec::GetLocusName() const
{
    return m_locusName;
}

wxString
GCLocusSpec::GetRegionName() const
{
    return m_regionName;
}

//------------------------------------------------------------------------------------

GCLocusMatcher::GCLocusMatcher()
    :
    m_locMatchType(locmatch_DEFAULT)
{
    m_locNames.Empty();
}

GCLocusMatcher::GCLocusMatcher(loc_match locMatchType)
    :
    m_locMatchType(locMatchType)
{
    assert(m_locMatchType == locmatch_DEFAULT || m_locMatchType == locmatch_LINKED);
    m_locNames.Empty();
}

GCLocusMatcher::GCLocusMatcher(loc_match locMatchType, wxString name)
    :
    m_locMatchType(locMatchType)
{
    assert(m_locMatchType == locmatch_SINGLE);
    m_locNames.Empty();
    m_locNames.Add(name);
}

GCLocusMatcher::GCLocusMatcher(loc_match locMatchType, wxArrayString locNames)
    :
    m_locMatchType(locMatchType),
    m_locNames(locNames)
{
    assert(m_locMatchType == locmatch_VECTOR);
}

GCLocusMatcher::~GCLocusMatcher()
{
}

GCLocusSpec
GCLocusMatcher::GetLocSpec(size_t index, const GCParse & parse) const
{
    if(!HandlesThisManyLoci(index))
    {
        wxString msg = wxString::Format(gcerr::tooFewLociInSpec,(int)index);
        gc_implementation_error e(msg.c_str());
        throw e;
    }
    wxString shortName = parse.GetFileRef().GetShortName();
    wxString locName = wxString::Format(gcstr::locusNameFromFile,
                                        (int)index+1,
                                        shortName.c_str());
    wxString regName = wxString::Format(gcstr::regionNameFromFile,
                                        shortName.c_str());

    switch(m_locMatchType)
    {
        case locmatch_DEFAULT:
            regName = wxString::Format("%s_%d",regName.c_str(),(int)index+1); // EWFIX.STRING
            return GCLocusSpec(false,false,locName,regName);
            break;
        case locmatch_SINGLE:
            return GCLocusSpec(true,false,m_locNames[0],gcdefault::regionName);
            break;
        case locmatch_LINKED:
            return GCLocusSpec(false,false,locName,regName);
            break;
        case locmatch_VECTOR:
            return GCLocusSpec(true,false,m_locNames[index],gcdefault::regionName);
            break;
    }
    assert(false);
    return GCLocusSpec(false,false,gcerr::emptyName,gcerr::emptyName);
}

bool
GCLocusMatcher::HandlesThisManyLoci(size_t count) const
{
    switch(m_locMatchType)
    {
        case locmatch_DEFAULT:
        case locmatch_SINGLE:
        case locmatch_LINKED:
            return true;
            break;
        case locmatch_VECTOR:
            return (count <= m_locNames.size());
            break;
    }
    assert(false);
    return false;

}

loc_match
GCLocusMatcher::GetLocMatchType() const
{
    return m_locMatchType;
}

const wxArrayString &
GCLocusMatcher::GetLociNames() const
{
    return m_locNames;
}

//____________________________________________________________________________________
