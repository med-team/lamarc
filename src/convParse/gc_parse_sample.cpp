// $Id: gc_parse_sample.cpp,v 1.14 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_errhandling.h"
#include "gc_parse_block.h"
#include "gc_parse_locus.h"
#include "gc_parse_sample.h"
#include "gc_sequential_data.h"
#include "gc_strings.h"
#include "wx/log.h"
#include "wx/tokenzr.h"

//------------------------------------------------------------------------------------

GCParseSample::GCParseSample(GCParseBlock & block, size_t line, wxString label)
    :
    m_block(&block),
    m_lineInFile(line),
    m_label(label)
{
}

GCParseSample::~GCParseSample()
{
    for(std::vector<GCSequentialData*>::iterator i = m_data.begin(); i != m_data.end(); i++)
    {
        delete *i;
    }
}

GCParseBlock &
GCParseSample::GetBlock()
{
    assert(m_block != NULL);
    return *m_block;
}

const GCParseBlock &
GCParseSample::GetBlock() const
{
    assert(m_block != NULL);
    return *m_block;
}

const GCSequentialData &
GCParseSample::GetData(size_t index) const
{
    if(index >= m_data.size())
    {
        wxString msg = wxString::Format(gcerr::tooBigDataIndex,
                                        (int)index,
                                        (int)(m_data.size()));
        gc_implementation_error e(msg.c_str());
        throw e;
    }
    assert(index < m_data.size());
    return *(m_data[index]);
}

wxString
GCParseSample::GetLabel() const
{
    return m_label;
}

size_t
GCParseSample::GetLine() const
{
    return m_lineInFile;
}

bool
GCParseSample::allLengthsEqual() const
{
    if(m_data.empty()) return true;
    size_t firstLength = (m_data[0])->GetNumMarkers();
    std::vector<GCSequentialData*>::const_iterator iter;
    for(iter = m_data.begin(); iter != m_data.end(); iter++)
    {
        const GCSequentialData & seq = **iter;
        if(seq.GetNumMarkers() != firstLength) return false;
    }
    return true;
}

size_t
GCParseSample::GetLength() const
{
    if(m_data.empty()) return 0;
    assert(allLengthsEqual());
    const GCSequentialData & seq = *(m_data[0]);
    return seq.GetNumMarkers();
}

size_t
GCParseSample::GetSequencesPerLabel() const
{
    return m_data.size();
}

void
GCParseSample::DebugDump(wxString prefix) const
{
    wxLogDebug("%s%10s:", prefix.c_str(), m_label.c_str()); // EWDUMPOK
    for(size_t i=0; i < m_data.size(); i++)
    {
        const GCSequentialData & seq = *(m_data[i]);
        seq.DebugDump(prefix+gcstr::indent);
    }
}

//____________________________________________________________________________________
