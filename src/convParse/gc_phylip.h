// $Id: gc_phylip.h,v 1.17 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PHYLIP_H
#define GC_PHYLIP_H

#include "gc_parser.h"
#include "gc_types.h"

class GCFile;
class GCParse;

class GCPhylipParser : public GCParser
{
  private:
    GCPhylipParser();           // undefined

  protected:
    void ParseTopPhylipLine(size_t              *   numSequences,
                            size_t              *   numSites,
                            bool                *   hasWeights);
    bool ParsePhylipWeightsLine(size_t numSites, wxString fileName);
    bool CompleteParse  (GCParse&);

  public:
    GCPhylipParser(const GCDataStore&);
    virtual ~GCPhylipParser();

    GCParse * Parse(GCFile &            fileRef,
                    gcGeneralDataType   dataType,
                    GCInterleaving      interleaving);

    void BadFirstToken  (wxString token) const;
    void BadSecondToken (wxString token) const;
};

#endif  // GC_PHYLIP_H

//____________________________________________________________________________________
