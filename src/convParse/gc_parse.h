// $Id: gc_parse.h,v 1.18 2018/01/03 21:32:55 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_PARSE_H
#define GC_PARSE_H

#include <vector>

#include "gc_phase_info.h"
#include "gc_quantum.h"
#include "gc_structure_maps.h"
#include "gc_types.h"
#include "wx/string.h"

class GCDataStore;
class GCFile;
class GCParseBlock;
class GCParseLocus;
class GCParsePop;
class GCParser;

typedef std::vector<GCParseBlock*>  GCParseBlocks;
typedef std::vector<GCParsePop*>    GCParsePops;
typedef std::vector<GCParseLocus*>  GCParseLoci;

class GCParse : public GCQuantum
{
    friend class GCParser;
    friend class GCParseVec;

  private:
    const GCFile *      m_filePointer;      // we don't own this
    GCFileFormat        m_format;
    gcGeneralDataType   m_dataType;
    GCInterleaving      m_interleaving;
    wxString            m_delimiter;
    bool                m_multiLineSeenInFile;
    bool                m_hasSpacesInNames;
    GCParsePops         m_pops;             // we own the contents
    GCParseLoci         m_loci;             // we own the contents
    GCParseBlocks       m_blocks;           // we own the contents

    GCParse();      // undefined

  protected:
    GCParseLocus &      GetParseLocus(size_t locusIndex) ;
    void                SetDataTypeFromFile(gcSpecificDataType dtype);
    void                SetHasSpacesInNames();

  public:
    GCParse(    GCFile &            fileRef,
                GCFileFormat        format,
                gcGeneralDataType   dataType,
                GCInterleaving      interleaving,
                wxString            delim=wxEmptyString);
    ~GCParse();

    wxString                GetSettings()   const;
    gcGeneralDataType       GetDataType()   const ;
    GCFileFormat            GetFormat()     const ;
    GCInterleaving          GetInterleaving() const ;
    wxString                GetDelimiter()  const ;
    bool                    GetMultiLineSeenInFile() const ;
    virtual wxString        GetName() const;
    const GCParseLocus &    GetParseLocus(size_t locusIndex) const ;
    const GCParsePop   &    GetParsePop  (size_t popIndex)   const;

    constBlockVector        GetBlocks() const;
    const GCParseBlock &    GetBlock(size_t popId, size_t locusId) const;

    const GCFile &          GetFileRef()    const ;
    size_t                  GetPopCount()   const ;
    size_t                  GetLociCount()  const ;
    bool                    GetHasSpacesInNames() const;

    void DebugDump(wxString prefix=wxEmptyString) const;

    gcIdSet     IdsOfAllBlocks() const;

    wxString    GetFormatString() const;
    wxString    GetDataTypeString() const;
    wxString    GetInterleavingString() const;

    gcPhaseInfo *   GetDefaultPhaseRecords() const;
    gcPhaseInfo *   GetPhaseRecordsForAdjacency(size_t adj) const;

    void    SetCannotBeMsat();
};

class GCParseVec : public std::vector<GCParse*>
{
  private:
    bool MungeParses(GCParseVec::iterator,GCParseVec::iterator);

  public:
    GCParseVec();
    virtual ~GCParseVec();
    bool MungeParses();
    void NukeContents();
};

#endif  // GC_PARSE_H

//____________________________________________________________________________________
