// $Id: gc_parse_pop.cpp,v 1.6 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_default.h"
#include "gc_parse_pop.h"

//------------------------------------------------------------------------------------

GCParsePop::GCParsePop( const GCParse * parse,
                        size_t          indexInParse,
                        wxString        name)
    :
    m_parse(parse),
    m_indexInParse(indexInParse),
    m_name(name)
{
    assert(m_parse != NULL);
}

GCParsePop::~GCParsePop()
{
}

const GCParse &
GCParsePop::GetParse() const
{
    return *m_parse;
}

size_t
GCParsePop::GetIndexInParse() const
{
    return m_indexInParse;
}

wxString
GCParsePop::GetName() const
{
    return m_name;
}

//____________________________________________________________________________________
