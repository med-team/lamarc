// $Id: gc_pop_match.h,v 1.11 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#ifndef GC_POP_MATCH_H
#define GC_POP_MATCH_H

#include "gc_types.h"
#include "wx/arrstr.h"

class GCFile;
class GCParse;

class GCPopSpec
{
  private:
    bool            m_blessed;
    wxString        m_name;
    GCPopSpec();    // undefined

  public:
    GCPopSpec(bool blessed, wxString name);
    ~GCPopSpec();

    bool        GetBlessed()    const ;
    wxString    GetName()       const ;
};

class GCPopMatcher
{
  protected:
    pop_match       m_popMatchType;
    wxArrayString   m_popNames;

  public:
    GCPopMatcher();
    GCPopMatcher(pop_match popMatchType);
    GCPopMatcher(pop_match popMatchType, wxString name);
    GCPopMatcher(pop_match popMatchType, wxArrayString names);
    ~GCPopMatcher();
    GCPopSpec   GetPopSpec(size_t index, const GCParse &) const;
    bool        HandlesThisManyPops(size_t count) const;
    pop_match   GetPopMatchType() const ;

    const wxArrayString &   GetPopNames() const;
};

#endif  // GC_POP_MATCH_H

//____________________________________________________________________________________
