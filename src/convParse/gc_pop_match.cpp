// $Id: gc_pop_match.cpp,v 1.20 2018/01/03 21:32:56 mkkuhner Exp $

/*
Copyright 2002-2005 Mary K. Kuhner, Peter Beerli, and Joseph Felsenstein

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

/* Authors: Mary Kuhner, Jon Yamato, and Joseph Felsenstein */


#include <cassert>

#include "gc_errhandling.h"
#include "gc_file.h"
#include "gc_parse.h"
#include "gc_parse_pop.h"
#include "gc_pop_match.h"
#include "gc_strings.h"
#include "wx/filename.h"
#include "wx/string.h"

//------------------------------------------------------------------------------------

GCPopSpec::GCPopSpec(bool blessed, wxString name)
    :
    m_blessed(blessed),
    m_name(name)
{
}

GCPopSpec::~GCPopSpec()
{
}

bool
GCPopSpec::GetBlessed() const
{
    return m_blessed;
}

wxString
GCPopSpec::GetName() const
{
    return m_name;
}

//------------------------------------------------------------------------------------

GCPopMatcher::GCPopMatcher()
    :
    m_popMatchType(popmatch_DEFAULT)
{
    m_popNames.Empty();
}

GCPopMatcher::GCPopMatcher(pop_match pMatchType)
    :
    m_popMatchType(pMatchType)
{
    assert(m_popMatchType == popmatch_DEFAULT || m_popMatchType == popmatch_NAME);
    m_popNames.Empty();
}

GCPopMatcher::GCPopMatcher(pop_match pMatchType, wxString name)
    :
    m_popMatchType(pMatchType)
{
    assert(m_popMatchType == popmatch_SINGLE);
    m_popNames.Empty();
    m_popNames.Add(name);
}

GCPopMatcher::GCPopMatcher(pop_match pMatchType, wxArrayString names)
    :
    m_popMatchType(pMatchType),
    m_popNames(names)
{
    assert(m_popMatchType == popmatch_VECTOR);
    assert(!(m_popNames.IsEmpty()));
}

GCPopMatcher::~GCPopMatcher()
{
}

GCPopSpec
GCPopMatcher::GetPopSpec(size_t index, const GCParse& parse) const
{
    if(!HandlesThisManyPops(index))
    {
        wxString msg = wxString::Format(gcerr::tooFewPopsInSpec,(int)index);
        gc_implementation_error e(msg.c_str());
        throw e;
    }

    wxString shortName = parse.GetFileRef().GetShortName();
    wxString popName = parse.GetParsePop(index).GetName();
    if(popName.IsEmpty())
    {
        popName = wxString::Format(gcstr::populationNameFromFile,
                                   (int)index+1,
                                   shortName.c_str());
    }

    switch(m_popMatchType)
    {
        case popmatch_DEFAULT:
            return GCPopSpec(false,popName);
            break;
        case popmatch_NAME:
            return GCPopSpec(false,parse.GetParsePop(index).GetName());
            break;
        case popmatch_SINGLE:
            assert(m_popNames.size() == 1);
            return GCPopSpec(true,m_popNames[0]);
            break;
        case popmatch_VECTOR:
            return GCPopSpec(true,m_popNames[index]);
            break;
    };
    assert(false);
    return GCPopSpec(false,gcerr::emptyName);
}

bool
GCPopMatcher::HandlesThisManyPops(size_t count) const
{
    switch(m_popMatchType)
    {
        case popmatch_DEFAULT:
            return true;
            break;
        case popmatch_NAME:
            return true;
            break;
        case popmatch_SINGLE:
            return true;
            break;
        case popmatch_VECTOR:
            return(count <= m_popNames.size());
            break;
    };
    assert(false);
    return false;
}

pop_match
GCPopMatcher::GetPopMatchType() const
{
    return m_popMatchType;
}

const wxArrayString &
GCPopMatcher::GetPopNames() const
{
    return m_popNames;
}

//____________________________________________________________________________________
