#!/bin/bash

#This script requires that you have 'wx-config' in your path somewhere.  For
# example, if you're on darwin and want to use sanity's version, you can
# put ~sanity/wxLibs/bin/ in your path.

if [ ! -r gcov/Makefile ]; then
    mkdir -p gcov
    cd gcov
    ../configure --enable-debug --enable-gcov --enable-gui
    cd ..
fi
cd gcov
make && echo 'Executables updated in the gcov directory--cd gcov to run.'
