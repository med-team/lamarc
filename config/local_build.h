// $Id: local_build.h,v 1.20 2012/06/30 01:32:25 bobgian Exp $

/*
 * ~/lamarc/config/local_build.h
 *
 *  Regular (non-DENOVO) arrangers with Data, Final-Coalescence ON, Non-Dynameter version
 *
 *  Created on: Nov 25, 2009
 *      Author: bobgian
 */

#ifndef LOCAL_BUILD_H
#define LOCAL_BUILD_H

//------------------------------------------------------------------------------------
//
// This file is intended to be customized for individual builds (DEBUG vs not, FC ON vs OFF, Dynameter ON vs OFF, etc)
// and placed into each build directory in which a customized build is done (actually, in the "config" subdirectory of the
// build directory).  THIS one, the version stored in the CVS repository, is intended as the default for PRODUCTION builds.
//
// The copy in the "config" subdirectory in which a build is running (a DEBUG or test build, say) takes precedence over any
// other copy (THIS copy, in the default PRODUCTION "config" subdirectory, for example).  The reason is that the "make" process
// INCLUDE directory precedence order prioritizes the CURRENT directory's "config" subdirectory over any other directory
// containing a file of same name.
//
// Note that this file does NOT define DEBUG or NDEBUG.  Either add it to a customized version or let the CONFIGURE script do it.
// That way, this file can work both for DEBUG and Non-DEBUG builds.
//
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// Implementation for Biglink optimization and associated debugging printout.

// When this is defined, the Biglink optimization runs natively (that is, the optimization takes effect).
// If NOT defined, the Littlelink system runs natively.
#define RUN_BIGLINKS

// When this is defined, the Biglink optimization runs natively but it EMULATES the Littlelink functionality.
// That is, all Biglink functionality is enabled, but the Biglink Map defines each Littlelink as its own Biglink.
// The intended effect should be exactly the same (modulo possible floating-point roundoff) as the native
// Littlelink implementation.  This is for testing purposes.
// #define EMULATE_LITTLELINKS

#ifndef NDEBUG // In Debug mode, the following compile flags all may-or-may-not take effect.
//
// When defined, information is printed about Region data as parsed for the Biglink Map construction.
// This works with either Biglink (including Littlelink Emulation) or Littlelink systems enabled.
// #define ENABLE_REGION_DUMP
//
// When defined, information is printed about construction and contents of the Biglink Map.
// This works only when the Biglink (including Littlelink Emulation) systems are enabled.
// #define ENABLE_BIGLINKMAP_DUMP
//
#endif // NDEBUG

//------------------------------------------------------------------------------------
// For testing of Final Coalescence optimization.

#define FINAL_COALESCENCE_ON          true

//------------------------------------------------------------------------------------
// Data versus Stationaries, Denovo testing.
// Make sure this file is consistent with the Makefile either by never defining these or by #undef-ing them.

// Test for stationary distributions.
// #define STATIONARIES

// Use DENOVO arrangers, ignoring input data.
// #define DENOVO

// Stationaries with denovo arranger in ALL cases.
// If not defined, run denovo arranger only in Bayesian case.
// #define ALL_ARRANGERS_DENOVO

//------------------------------------------------------------------------------------
// Dynameter

// NB: DYNAMETER_LEVEL is NOT defined.  The presence or absence of the pre-processor symbol DYNAMETER_LEVEL determines whether
// the tracing/metering facility is activated.  If the symbol IS defined, various symbols and macros defined in header file
// "dynatracer.h" expand so as to activate (compile in) the metering tool.  Also, the actual value of the symbol (an integer
// greater than zero) determines the set of metering/tracing features that are activated (higher numbers activate more features).
// If DYNAMETER_LEVEL is NOT defined (as is the case here), these other symbols are NOT defined and the macros expand into whitespace,
// making the apparent function calls (actually calls to these macros) "disappear" from the source files during compilation.
//
// Activate basic Dynameter functionality (metering of function call times and statistics printed at end) by defining value as 1.
// #define DYNAMETER_LEVEL               1
//
// Add to above the accumulation of tree-search statistics (printed at end) by defining value as 2.
// #define DYNAMETER_LEVEL               2
//
// Add to above the dynamic function-call tracing (printout continuously while running) by defining value as 3.
// #define DYNAMETER_LEVEL               3
//
#ifdef DYNAMETER_LEVEL
// If the Dynameter is in use (above symbol defined), then the symbol DYNAMETER_FILE_INDEX must be defined before this file is
// included with an integer value starting from 0 and incrementing by 1 for each file that includes calls to StartDynameter().
// DYNAMETER_FILE_INDEX's value is an index into a compile-time SWITCH statement held in "dynacount.h", whose output provides
// the starting value for the Dynameter array's index values, incremented automatically by the macro __COUNTER__ at each call
// of StartDynameter() within that file.
#include "dynacount.h"
// Each file using the Dynameter will, by virtue of declaring such intent by defining DYNAMETER_LEVEL in this file, be guaranteed
// to have the environment for such use set up automatically.  The file "dynacount.h" (in directory "~/lamarc/config") is generated
// by the script "~/lamarc/config/dynacounter.bash".
#endif // DYNAMETER_LEVEL

//------------------------------------------------------------------------------------
// LAMARC_QA tests -- here are flags you can turn on (by removing the leading comment
// characters and space) to do special-purpose Quality Assurance testing.
//
// Turn off aliasing -- this makes the code run much more slowly, but allows you
// to check that you haven't screwed up aliasing.
// #define LAMARC_QA_NOALIAS
//
// To generate single denovo trees and retrieve their parameter values, uncomment both of these.
// #define LAMARC_QA_SINGLE_DENOVOS
// #define LAMARC_QA_SINGLE_DENOVOS_COUNT 10000
//
// To dump out GraphML trees.  Not released to the general public.  If you are
// using this, please be aware that it is not tested well enough to be trusted.
// #define LAMARC_QA_TREE_DUMP
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// LAMARC_NEW_FEATURE -- here are flags you can turn on (by removing the leading comment
// characters and space) to turn on a new feature that is not yet released.
// #define LAMARC_NEW_FEATURE_RELATIVE_SAMPLING
//------------------------------------------------------------------------------------

#endif // LOCAL_BUILD_H

//____________________________________________________________________________________
