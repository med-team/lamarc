#!/bin/bash

#This script requires that you have 'wx-config' in your path somewhere.  For
# example, if you're on darwin and want to use sanity's version, you can
# put ~sanity/wxLibs/bin/ in your path.
#
# Also note that if your makefiles are not working, you may need to start 
# from scratch, with:
#
# aclocal
# autoconf
# automake
#
# (in this directory) before running this script.

if [ ! -r debug/Makefile ]; then
    mkdir -p debug
    cd debug
    ../configure --enable-debug --enable-gui
    cd ..
fi
cd debug
make lamarc && cp -f lamarc .. && echo 'lamarc updated to the debug version.'
make lam_conv && cp -f lam_conv .. && echo 'lam_conv updated to the debug version.'
make old_lam_conv && cp -f old_lam_conv .. && echo 'old_lam_conv updated to the debug version.'

