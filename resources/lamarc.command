#!/bin/sh

# Directory we run from: blah/lamarc.app/Contents/MacOS
LAMARC_BOTTOMDIR=`dirname "$0"`
LAMARC_TOPDIR="$LAMARC_BOTTOMDIR/../../.."
LAMARC_OUTPUT="$(/usr/bin/osascript -e "tell application \"System Events\" to activate" -e "tell application \"System Events\" to set thefile to choose folder with prompt \"Select your LAMARC output directory\"" -e "do shell script (\"echo \"&(quoted form of POSIX path of thefile as Unicode text)&\"\")")"
LAMARC_INFILE="$(/usr/bin/osascript -e "tell application \"System Events\" to activate" -e "tell application \"System Events\" to set thefile to choose file with prompt \"Select your LAMARC infile\"" -e "do shell script (\"echo \"&(quoted form of POSIX path of thefile as Unicode text)&\"\")")"


# Extra quoting required because $dirname may have spaces and double-quotes get
# eaten below.

scriptcmd="cd \\\"${mydir}/../../..\\\" ; \\\"${mydir}/lamarc -x\\\" ; exit"
osascript <<EOF
tell application "Terminal"
	activate
	do script "cd $LAMARC_OUTPUT; $LAMARC_BOTTOMDIR/lamarc -x $LAMARC_INFILE; exit"
	set background color of window 1 to {52224, 65535, 65535}
	set normal text color of window 1 to "black"
	set cursor color of window 1 to "black"
	set custom title of window 1 to "lamarc"
end tell
EOF

