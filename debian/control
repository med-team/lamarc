Source: lamarc
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libboost-dev,
               libtinyxml-dev,
               libwxgtk3.2-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/lamarc
Vcs-Git: https://salsa.debian.org/med-team/lamarc.git
Homepage: https://evolution.gs.washington.edu/lamarc/
Rules-Requires-Root: no

Package: lamarc
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Likelihood Analysis with Metropolis Algorithm using Random Coalescence
 LAMARC is a program which estimates population-genetic parameters such
 as population size, population growth rate, recombination rate, and
 migration rates. It approximates a summation over all possible
 genealogies that could explain the observed sample, which may be
 sequence, SNP, microsatellite, or electrophoretic data. LAMARC and its
 sister program Migrate are successor programs to the older programs
 Coalesce, Fluctuate, and Recombine, which are no longer being supported.
 The programs are memory-intensive but can run effectively on
 workstations.
