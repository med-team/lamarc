#!/usr/bin/perl

use File::Basename;

## prints out one file name when within a list of sources
sub printOneFileToHandle
{
    my($fileName) = @_;

    ## printf "\t\t\t<File\r\n";
    ## printf "\t\t\t\tRelativePath=\"$fromVcprojToTop$fileName\">\r\n";
    ## printf "\t\t\t</File>\r\n";
    printf "\t\t\t<File RelativePath=\"$fromVcprojToTop$fileName\"/>\r\n";
}

## constant strings, not expected to change 
$templateFile              = dirname($0)."/vcproj_template";
$fromVcprojToTop           = "..\\..\\";
$fromVcprojToInclude       = "..\\";

$matchesHeaderFiles     = "\\\.h\$";
$matchesSourceFiles     = "\\\.cpp\$";
$matchesResourceFiles   = "\\\.rc\$|\\\.ico\$";

$heapSize               = "100000000";
$execPath               = "\$(ProjectDir)\\$fromVcprojToTop";

$originalProjectName    = "PROJECTNAME";
$originalHeapSize       = "HEAPSIZE";
$originalExecName       = "EXECNAME";


## gonna slurp a bunch of file names into these
@headers = ();
@sources = ();
@resources = ();
@includeDirs = ();

########################################################
## first argument is the project name, it could be lamarc 
## or lam_conv, for example
$projectName = shift;

########################################################
## divide up remaining arguments into files and include
## path items
$nextIsInclude = "";    ## true when we just saw -I
foreach $item (@ARGV)
{
    $item =~ s/\//\\/g;

    if($nextIsInclude)
    {
        push @includeDirs, $item ;
        $nextIsInclude = "";
    }
    elsif ($item =~ /$matchesHeaderFiles/)
    {
        push @headers, $item ;
    }
    elsif ($item =~ /$matchesSourceFiles/)
    {
        push @sources, $item ;
    }
    elsif ($item =~ /$matchesResourceFiles/)
    {
        push @resources, $item ;
    }
    elsif ($item =~ /^-I$/)
    {
        $nextIsInclude = "yes";
    }
}

open(TEMPLATEFILE,$templateFile);       ## modify content of this file

$inFiles = $inFilter = $filterType = "";
while(<TEMPLATEFILE>)
{
    ## #################################################
    ## replacing project name, etc
    ## #################################################
    $_ =~ s/$originalProjectName/$projectName/g;
    $_ =~ s/$originalHeapSize/$heapSize/g;
    $_ =~ s/$originalExecName/$execPath\\$projectName\.exe/g;

    ## #################################################
    ## in this section, setting state but not outputting
    ## #################################################
    if($_ =~ /<Files>/)
    {
        $inFiles = "yes";
    }
    if ($_ =~ /<\/Files>/)
    {
        $inFiles = "";
    }
    if($_ =~ /<Filter/)
    {
        $inFilter = "yes";
    }
    if ($_ =~ /<\/Filter>/)
    {
        $inFilter = "";
    }
    if($_ =~ /Name=\"Header Files\"/)
    {
        $filterType = "h";
    }
    if($_ =~ /Name=\"Source Files\"/)
    {
        $filterType = "cpp";
    }
    if($_ =~ /Name=\"Resource Files\"/)
    {
        $filterType = "rc";
    }


    ## #################################################
    ## in this section, producing output
    ## #################################################
    if($_ =~ /AdditionalIncludeDirectories/)
    {
        printf "\t\t\t\tAdditionalIncludeDirectories=\"";
        foreach $item (@includeDirs)
        {
            if ($item =~ /\\config$/) {
                printf "&quot;\$(ProjectDir)\\$fromVcprojToTop$item&quot;;";
            } else {
                printf "&quot;\$(ProjectDir)\\$fromVcprojToInclude$item&quot;;";
            }
        }
        printf "\"\r\n";
    }
    elsif($_ =~ /<File\/>/)
    {
        ## dump out file names based on fiter type
        if($inFiles && $inFilter)
        {
            if($filterType eq "h")
            {
                foreach $item (@headers)
                {
                    printOneFileToHandle($item);
                }
            }
            if($filterType eq "cpp")
            {
                foreach $item (@sources)
                {
                    printOneFileToHandle($item);
                }
            }
            if($filterType eq "rc")
            {
                foreach $item (@resources)
                {
                    printOneFileToHandle($item);
                }
            }
        }
        else
        {
        }
    }
    else
    {
        printf $_;
    }
}
close(TEMPLATEFILE);


