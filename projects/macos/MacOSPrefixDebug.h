



/*

 *	MacOSPrefix.h		(c) 2000 Zero-Knowledge Systems, Inc.

 *	

 *	$Id: MacOSPrefixDebug.h,v 1.2 2004/08/04 21:57:41 lpsmith Exp $

 *	

 *	General-purpose CodeWarrior prefix file, for release builds

 *	

 */



#define TIXML_USE_STL



#if !defined(Rez)

#	define FREEDOM_PRODUCTION_BUILD	"FREEDOM BUILD ("__DATE__", "__TIME__")"

#endif



#if defined(__MWERKS__) && !defined(Rez)

	/* CodeWarrior */

#	if __POWERPC__

		/* PowerPC compiler */

#		pragma traceback off 

#	elif __MC68K__

		/* Motorola 680x0 compiler */

#		pragma macsbug off 

#	endif

#endif



/* we want the <stdint.h> macros, even with C++ */

#define __STDC_LIMIT_MACROS	1



/* strip out patented stuff in OpenSSL */

#define NO_IDEA

#define NO_RC5

