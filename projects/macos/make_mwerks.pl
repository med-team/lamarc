#!/usr/bin/perl

## constant strings, not expected to change 
$templateFile           = "projects/macos/mwerks_xml_template";
$fromXmlToTop           = "../../";
$cr                     = "\n";

$matchesHeaderFiles     = "\\\.h\$";
$matchesSourceFiles     = "\\\.cpp\$";
$matchesResourceFiles   = "\\\.ico\$";

## prints out one user search path
sub makeSearchPath
{
    my($localPath) = @_;

    my($path) = $fromXmlToTop.$localPath;

    printf "                    <SETTING>$cr";
    printf "                        <SETTING><NAME>SearchPath</NAME>$cr";
    printf "                            <SETTING><NAME>Path</NAME><VALUE>$path</VALUE></SETTING>$cr";
    printf "                            <SETTING><NAME>PathFormat</NAME><VALUE>Unix</VALUE></SETTING>$cr";
    printf "                            <SETTING><NAME>PathRoot</NAME><VALUE>Project</VALUE></SETTING>$cr";
    printf "                        </SETTING>$cr";
    printf "                        <SETTING><NAME>Recursive</NAME><VALUE>true</VALUE></SETTING>$cr";
    printf "                        <SETTING><NAME>FrameworkPath</NAME><VALUE>false</VALUE></SETTING>$cr";
    printf "                        <SETTING><NAME>HostFlags</NAME><VALUE>All</VALUE></SETTING>$cr";
    printf "                    </SETTING>$cr";

}


## prints out one file name when within a list of sources
sub makeFileEntry
{
    my($fileName) = @_;

    my(@exploded) = split /\// , $fileName;

    my($fname) = pop @exploded;
    my($fpath) = $fromXmlToTop . (join ('/',@exploded));


    printf "                <FILE>$cr";
    printf "                    <PATHTYPE>Name</PATHTYPE>$cr";
    printf "                    <PATH>$fname</PATH>$cr";
    printf "                    <PATHFORMAT>Unix</PATHFORMAT>$cr";
    printf "                </FILE>$cr";
}

sub makeGroupEntry
{
    my($fileName) = @_;

    my(@exploded) = split /\// , $fileName;

    my($fname) = pop @exploded;
    my($fpath) = $fromXmlToTop . (join ('/',@exploded));

    printf "            <FILEREF>$cr";
    printf "                <TARGETNAME>$lamarcTargetName</TARGETNAME>$cr";
    printf "                <PATHTYPE>Name</PATHTYPE>$cr";
    printf "                <PATH>$fname</PATH>$cr";
    printf "                <PATHFORMAT>Unix</PATHFORMAT>$cr";
    printf "            </FILEREF>$cr";
}


## gonna slurp a bunch of file names into these
@headers = ();
@sources = ();
@resources = ();
@includeDirs = ();

########################################################
## first argument is the project name, it could be lamarc 
## or lam_conv, for example
$projectName = shift;

$lamarcTargetName = $projectName."-mac";

########################################################
## divide up remaining arguments into files and include
## path items
$nextIsInclude = "";    ## true when we just saw -I
foreach $item (@ARGV)
{

    if($nextIsInclude)
    {
        push @includeDirs, $item ;
        $nextIsInclude = "";
    }
    elsif ($item =~ /$matchesHeaderFiles/)
    {
        push @headers, $item ;
    }
    elsif ($item =~ /$matchesSourceFiles/)
    {
        push @sources, $item ;
    }
    elsif ($item =~ /$matchesResourceFiles/)
    {
        push @resources, $item ;
    }
    elsif ($item =~ /^-I$/)
    {
        $nextIsInclude = "yes";
    }
}

open(TEMPLATEFILE,$templateFile);       ## modify content of this file

$inFiles = $inFilter = $filterType = "";
while(<TEMPLATEFILE>)
{

    ## #################################################
    ## in this section, producing output
    ## #################################################

    s/LAMARCTARGETNAME/$lamarcTargetName/g;

    if($_ =~ /LAMARC: SEARCHPATHS/)
    {
        printf;
        foreach $item (@includeDirs)
        {
            makeSearchPath($item);
        }
    }
    elsif($_ =~ /LAMARC: FILELIST/)
    {
        printf;
        foreach $item (@headers)
        {
            makeFileEntry($item);
        }
        foreach $item (@sources)
        {
            makeFileEntry($item);
        }
        foreach $item (@resources)
        {
            makeFileEntry($item);
        }
    }
    elsif($_ =~ /LAMARC: GROUPLIST/)
    {
        printf;
        foreach $item (@headers)
        {
            makeGroupEntry($item);
        }
        foreach $item (@sources)
        {
            makeGroupEntry($item);
        }
        foreach $item (@resources)
        {
            makeGroupEntry($item);
        }
    }
    else
    {
        printf;
    }
}
close(TEMPLATEFILE);


