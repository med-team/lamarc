#!/bin/bash

#This script requires that you have 'wx-config' in your path somewhere.  For
# example, if you're on darwin and want to use sanity's version, you can
# put ~sanity/wxLibs/bin/ in your path.

if [ ! -r opt/Makefile ]; then
    mkdir -p opt
    cd opt
    ../configure --enable-gui
    cd ..
fi
cd opt
make lamarc && cp -f lamarc .. && echo 'lamarc updated to the optimized version.'
make lam_conv && cp -f lam_conv .. && echo 'lam_conv updated to the optimized version.'
make old_lam_conv && cp -f old_lam_conv .. && echo 'old_lam_conv updated to the optimized version.'

